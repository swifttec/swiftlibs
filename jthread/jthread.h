#ifndef __jthread_jthread_h__
#define __jthread_jthread_h__

#include <jrtplib/jrtplib.h>

#include <swift/SWThread.h>

namespace jthread
{

class JRTPLIB_DLL_EXPORT JThread : public SWThread
		{
public:
		JThread() : SWThread(SW_THREAD_TYPE_NORMAL)	{ }

		bool	IsRunning()							{ return running(); }
		int		Start()								{ SWThread::start(); return 0; }
		void	Kill()								{ stop(); }

protected:	// Virtual Overrides

		/**
		***	@brief	The run method used by derived classes.
		***
		*** The run method should be overridden by anyone deriving a thread class
		*** from XThread. The derived method should periodically check the run status
		*** by calling the runnable() method and should exit the run method if runnable()
		*** returns false. This mechanism is used to help ensure that all thread resources
		*** are properly returned to the system (e.g. memory, file handles, etc).
		***
		*** The thread should return an integer which will be used as the exit code for
		*** the thread which any other thread can check after the thread has exited.
		***
		*** @return		The thread exit code.
		**/
		virtual int		run()						{ Thread(); return 0; }

protected:
		virtual void *Thread() = 0;

		void	ThreadStarted()	{ }
		};

} // end namespace jthread

#endif // __jthread_jthread_h__
