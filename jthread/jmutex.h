#ifndef __jthread_jmutex_h__
#define __jthread_jmutex_h__

#include <jrtplib/jrtplib.h>

#include <swift/SWThreadMutex.h>

namespace jthread
{

class JRTPLIB_DLL_EXPORT JMutex : public SWThreadMutex
		{
public:
		JMutex() : SWThreadMutex(true)	{ }

		bool	IsInitialized()			{ return true; }
		int		Init()					{ return 0; }
		void	Lock()					{ acquire(); }
		void	Unlock()				{ release(); }
		};

} // end namespace jthread

#endif // __jthread_jmutex_h__
