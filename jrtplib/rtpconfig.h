/*

  This file is a part of JRTPLIB
  Copyright (c) 1999-2011 Jori Liesenborgs

  Contact: jori.liesenborgs@gmail.com

  This library was developed at the Expertise Centre for Digital Media
  (http://www.edm.uhasselt.be), a research center of the Hasselt University
  (http://www.uhasselt.be). The library is based upon work done for 
  my thesis at the School for Knowledge Technology (Belgium/The Netherlands).

  Permission is hereby granted, free of charge, to any person obtaining a
  copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, including without limitation
  the rights to use, copy, modify, merge, publish, distribute, sublicense,
  and/or sell copies of the Software, and to permit persons to whom the
  Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.

*/

#ifndef RTPCONFIG_UNIX_H
#define RTPCONFIG_UNIX_H

#include <jrtplib/jrtplib.h>
	
#define JRTPLIB_IMPORTEXPORT JRTPLIB_DLL_EXPORT

// By default we support threading
#define RTP_SUPPORT_THREAD

#undef RTP_SUPPORT_IPV4MULTICAST
#undef RTP_SUPPORT_IPV6
#undef RTP_SUPPORT_IPV6MULTICAST

#undef RTP_HAVE_SYS_FILIO
#undef RTP_HAVE_SYS_SOCKIO
#undef RTP_BIG_ENDIAN
#undef RTP_SOCKLENTYPE_UINT
#undef RTP_SOCKLENTYPE_SIZE_T
#undef RTP_SOCKLENTYPE_SOCKLEN_T
#undef RTP_HAVE_SOCKADDR_LEN
#undef RTP_SUPPORT_SDESPRIV
#undef RTP_SUPPORT_PROBATION
#undef RTP_SUPPORT_GETLOGINR
#undef RTP_SUPPORT_IFADDRS
#undef RTP_SUPPORT_SENDAPP
#undef RTP_SUPPORT_MEMORYMANAGEMENT
#undef RTP_SUPPORT_RTCPUNKNOWN

#if defined(SW_PLATFORM_LINUX)
	#define RTP_SOCKLENTYPE_SOCKLEN_T
	#define RTP_SUPPORT_IPV4MULTICAST
	#define RTP_SUPPORT_IFADDRS
	#define RTP_SUPPORT_IPV6
	#define RTP_SUPPORT_IPV6MULTICAST
#endif

#endif // RTPCONFIG_UNIX_H

