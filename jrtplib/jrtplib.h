
#ifndef __jrtplib_h__
#define __jrtplib_h__

#include <xplatform/xplatform.h>

#if defined(SW_PLATFORM_WINDOWS)

	// Determine the import/export type for a DLL build
	#if defined(JRTPLIB_BUILD_DLL)
		#define JRTPLIB_DLL_EXPORT __declspec(dllexport)
	#else
		#define JRTPLIB_DLL_EXPORT __declspec(dllimport)
	#endif // defined(UTIL_BUILD_DLL)

	#pragma warning(disable: 4267)

#else // defined(SW_PLATFORM_WINDOWS)

	// On non-windows platforms we just define this to be nothing
	#define JRTPLIB_DLL_EXPORT

#endif // defined(SW_PLATFORM_WINDOWS)

#endif // __jrtplib_h__
