#ifndef __jtrplib_rtptypes_h__
#define __jtrplib_rtptypes_h__

#include <jrtplib/jrtplib.h>

#include <xplatform/sw_types.h>
#include <swift/sw_net.h>

#include <stdint.h>

#if defined(SW_PLATFORM_WINDOWS)
	#pragma comment(lib, "Ws2_32.lib")

	#pragma warning(disable: 4996)
#endif

#endif // __jtrplib_rtptypes_h__
