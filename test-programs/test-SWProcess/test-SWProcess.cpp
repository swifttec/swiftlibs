#include <swift/sw_main.h>
#include <swift/SWProcess.h>

int
sw_main(const SWString &prog, const SWStringArray &args)
 		{
		printf("PID = %d\n", SWProcess::self());

		SWProcess		p;
		SWStringArray	sa;
		SWString		exe="FakeProgram";

		if (args.size() > 0) exe = args.get(0);

		printf("PID = %d creating process for %s\n", SWProcess::self(), exe.c_str());
		if (p.create(exe, sa) == SW_STATUS_SUCCESS)
			{
			printf("PID = %d successfully created process for %s with PID %d\n", SWProcess::self(), exe.c_str(), p.getProcessId());
			}
		else
			{
			printf("PID = %d error %s creating process for %s\n", SWProcess::self(), sw_status_getLastErrorString(), exe.c_str());
			}

		printf("Exiting PID = %d\n", SWProcess::self());

		return 0;
		}

