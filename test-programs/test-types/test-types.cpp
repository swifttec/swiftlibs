#include <swift/sw_main.h>
#include <swift/sw_printf.h>

/*
**	testtypes.cpp	Simple program to test the sizes and condition of various types
*/

#include <swift/SWTime.h>
#include <string.h>
#include <stdint.h>

bool	verbose=false;

static void
check(const char *text, int result, int expected)
		{
		if (result != expected)
			{
			sw_printf("%s %s (got %d, expected %d)\n", text, result==expected?"OK":"Failed", result, expected);
			}
		}


#define SHOW(x)		sw_printf("sizeof("#x") = %d\n", sizeof(x))
#define TESTSIZE(x, n)	check(#x, sizeof(x), n)

static void
testtypes()
		{
		if (verbose)
			{
			SHOW(bool);
			//SHOW(boolean);
			SHOW(char);
			SHOW(unsigned char);
			SHOW(sw_tchar_t);
			SHOW(short);
			SHOW(unsigned short);
			SHOW(int);
			SHOW(unsigned int);
			SHOW(long);
			SHOW(unsigned long);
	#ifndef	_WIN32
			SHOW(long long);
			SHOW(unsigned long long);
	#endif
			SHOW(float);
			SHOW(double);
			SHOW(long double);
			SHOW(void *);
			SHOW(wchar_t);
			}

		SHOW(void *);
		SHOW(sw_size_t);
		SHOW(sw_intptr_t);
		SHOW(sw_uintptr_t);
		SHOW(sw_int8_t);
		SHOW(sw_int16_t);
		SHOW(sw_int32_t);
		SHOW(sw_int64_t);

		/* Unsigned int */
		SHOW(sw_uint8_t);
		SHOW(sw_uint16_t);
		SHOW(sw_uint32_t);
		SHOW(sw_uint64_t);

		TESTSIZE(long, SW_SIZEOF_LONG);

		/* Pointers */
		TESTSIZE(void *, SW_SIZEOF_POINTER);
		TESTSIZE(void *, sizeof(sw_intptr_t));
		TESTSIZE(void *, sizeof(sw_uintptr_t));

		/* Signed int */
		TESTSIZE(sw_int8_t, 1);
		TESTSIZE(sw_int16_t, 2);
		TESTSIZE(sw_int32_t, 4);
		TESTSIZE(sw_int64_t, 8);

		/* Unsigned int */
		TESTSIZE(sw_uint8_t, 1);
		TESTSIZE(sw_uint16_t, 2);
		TESTSIZE(sw_uint32_t, 4);
		TESTSIZE(sw_uint64_t, 8);


		/* boolean */
		TESTSIZE(bool, 1);

	// Some platforms have bigger wchars than others!
		/* chars */
		TESTSIZE(char, 1);
		TESTSIZE(wchar_t, SW_SIZEOF_WCHAR);

	#ifdef UNICODE
		TESTSIZE(sw_tchar_t, SW_SIZEOF_WCHAR);
	#else
		TESTSIZE(sw_tchar_t, 1);
	#endif
		}


static void
testconstvalues()
		{

#define TEST(t, c, v) \
		{ \
		t	v1=c; \
		t	v2=v; \
		\
		sw_printf("%-13s = %d\n", #c, v1); \
		if (v1 != v2) sw_printf("%s (%s) returns %d instead of %d\n", #c, #t, v1, v2); \
		}

		TEST(sw_int8_t, SW_INT8_MIN, -128);
		TEST(sw_int8_t, SW_INT8_MAX, 127);
		TEST(sw_int16_t, SW_INT16_MIN, -32768);
		TEST(sw_int16_t, SW_INT16_MAX, 32767);
		TEST(sw_int32_t, SW_INT32_MIN, -2147483647 - 1);
		TEST(sw_int32_t, SW_INT32_MAX, 2147483647);

#undef TEST
#define TEST(t, c, v) \
		{ \
		t	v1=c; \
		t	v2=v; \
		\
		sw_printf("%-13s = %u\n", #c, v1); \
		if (v1 != v2) sw_printf("%s (%s) returns %u instead of %u\n", #c, #t, v1, v2); \
		}

		TEST(sw_uint8_t, SW_UINT8_MAX, 255);
		TEST(sw_uint16_t, SW_UINT16_MAX, 65535);
		TEST(sw_uint32_t, SW_UINT32_MAX, 0xffffffff);

#undef TEST
#define TEST(t, c, v) \
		{ \
		t	v1=c; \
		t	v2=v; \
		\
		sw_printf("%-13s = %lld\n", #c, v1); \
		if (v1 != v2) sw_printf("%s (%s) returns %lld instead of %lld\n", #c, #t, v1, v2); \
		}

#if defined(_MSC_VER) && _MSC_VER < 1300
		TEST(sw_int64_t, SW_INT64_MIN, -9223372036854775807L - 1L);
		TEST(sw_int64_t, SW_INT64_MAX, 9223372036854775807L);
#else // defined(_MSC_VER) && _MSC_VER < 1300
		TEST(sw_int64_t, SW_INT64_MIN, -9223372036854775807LL - 1LL);
		TEST(sw_int64_t, SW_INT64_MAX, 9223372036854775807LL);
#endif // defined(_MSC_VER) && _MSC_VER < 1300

#undef TEST
#define TEST(t, c, v) \
		{ \
		t	v1=c; \
		t	v2=v; \
		\
		sw_printf("%-13s = %llu\n", #c, v1); \
		if (v1 != v2) sw_printf("%s (%s) returns %llu instead of %llu\n", #c, #t, v1, v2); \
		}

#if defined(_MSC_VER) && _MSC_VER < 1300
		TEST(sw_uint64_t, SW_UINT64_MAX, 0xffffffffffffffffUL);
#else // defined(_MSC_VER) && _MSC_VER < 1300
		TEST(sw_uint64_t, SW_UINT64_MAX, 0xffffffffffffffffULL);
#endif // defined(_MSC_VER) && _MSC_VER < 1300

/*
#define SW_INT64_MAX	SW_CONST_INT64(9223372036854775807)		///< maximum signed int64 value
#define SW_INT64_MIN	SW_CONST_INT64(-9223372036854775808)	///< minimum signed int64 value
#define SW_UINT64_MAX	SW_CONST_UINT64(0xffffffffffffffff)		///< maximum unsigned int64 value
*/
		}


		
static void
testminmaxvalues()
		{


		{
		sw_uint64_t	v;

		v = SW_UINT64_MAX;
		if (++v != 0) sw_printf("++SW_UINT64_MAX returns %llu instead of zero.", v);

		v = 0;
		if (--v != SW_UINT64_MAX) sw_printf("--SW_UINT64_MAX returns %llu instead of SW_UINT64_MAX.", v);
		}

		{
		sw_int64_t	v;

		v = SW_INT64_MAX;
		if (++v != SW_INT64_MIN) sw_printf("++SW_INT64_MAX returns %llu instead of SW_INT64_MIN.", v);

		v = SW_INT64_MIN;
		if (--v != SW_INT64_MAX) sw_printf("--SW_INT64_MAX returns %llu instead of SW_INT64_MAX.", v);
		}

		{
		sw_uint32_t	v;

		v = SW_UINT32_MAX;
		if (++v != 0) sw_printf("++SW_UINT32_MAX returns %llu instead of zero.", v);

		v = 0;
		if (--v != SW_UINT32_MAX) sw_printf("--SW_UINT32_MAX returns %llu instead of SW_UINT32_MAX.", v);
		}

		{
		sw_int32_t	v;

		v = SW_INT32_MAX;
		if (++v != SW_INT32_MIN) sw_printf("++SW_INT32_MAX returns %llu instead of SW_INT32_MIN.", v);

		v = SW_INT32_MIN;
		if (--v != SW_INT32_MAX) sw_printf("--SW_INT32_MAX returns %llu instead of SW_INT32_MAX.", v);
		}


		{
		sw_uint16_t	v;

		v = SW_UINT16_MAX;
		if (++v != 0) sw_printf("++SW_UINT16_MAX returns %llu instead of zero.", v);

		v = 0;
		if (--v != SW_UINT16_MAX) sw_printf("--SW_UINT16_MAX returns %llu instead of SW_UINT16_MAX.", v);
		}

		{
		sw_int16_t	v;

		v = SW_INT16_MAX;
		if (++v != SW_INT16_MIN) sw_printf("++SW_INT16_MAX returns %llu instead of SW_INT16_MIN.", v);

		v = SW_INT16_MIN;
		if (--v != SW_INT16_MAX) sw_printf("--SW_INT16_MAX returns %llu instead of SW_INT16_MAX.", v);
		}


		{
		sw_uint8_t	v;

		v = SW_UINT8_MAX;
		if (++v != 0) sw_printf("++SW_UINT8_MAX returns %llu instead of zero.", v);

		v = 0;
		if (--v != SW_UINT8_MAX) sw_printf("--SW_UINT8_MAX returns %llu instead of SW_UINT8_MAX.", v);
		}

		{
		sw_int8_t	v;

		v = SW_INT8_MAX;
		if (++v != SW_INT8_MIN) sw_printf("++SW_INT8_MAX returns %llu instead of SW_INT8_MIN.", v);

		v = SW_INT8_MIN;
		if (--v != SW_INT8_MAX) sw_printf("--SW_INT8_MAX returns %llu instead of SW_INT8_MAX.", v);
		}


		{
		unsigned long	v;

		v = SW_ULONG_MAX;
		if (++v != 0) sw_printf("++SW_ULONG_MAX returns %llu instead of zero.", v);

		v = 0;
		if (--v != SW_ULONG_MAX) sw_printf("--SW_ULONG_MAX returns %llu instead of SW_ULONG_MAX.", v);
		}

		{
		long	v;

		v = SW_LONG_MAX;
		if (++v != SW_LONG_MIN) sw_printf("++SW_LONG_MAX returns %ld instead of SW_LONG_MIN.", v);

		v = SW_LONG_MIN;
		if (--v != SW_LONG_MAX) sw_printf("--SW_LONG_MAX returns %ld instead of SW_LONG_MAX.", v);
		}

		}


void
runtest_types()
		{
		bool	b;

		testtypes();
		testminmaxvalues();
		testconstvalues();

		if (verbose)
			{
			b=true;		sw_printf("b(true) = %d\n", b);
			b=false;	sw_printf("b(false) = %d\n", b);
			b=0;		sw_printf("b(0) = %d\n", b);
			b=1;		sw_printf("b(1) = %d\n", b);
			//b=2;		sw_printf("b(2) = %d\n", b);
			//b=-1;		sw_printf("b(-1) = %d\n", b);
			b=!b;		sw_printf("b(!b) = %d\n", b);
			b=TRUE;		sw_printf("b(TRUE) = %d\n", b);
			b=FALSE;	sw_printf("b(FALSE) = %d\n", b);

			sw_printf("TRUE=%d\n", TRUE);
			sw_printf("FALSE=%d\n", FALSE);
			sw_printf("NULL=%d\n", NULL);
			sw_printf("true=%d\n", true);
			sw_printf("false=%d\n", false);
			//sw_printf("null=%d\n", null);
			}

		b = TRUE;
		if (b)
			{
			if (verbose) sw_printf("TRUE OK\n");
			}
		else sw_printf("TRUE FAILED");

		b = FALSE;
		if (b) sw_printf("FALSE FAILED\n");
		else if (verbose) sw_printf("FALSE OK\n");

		b = true;
		if (b)
			{
			if (verbose) sw_printf("true OK\n");
			}
		else sw_printf("true FAILED");

		b = false;
		if (b) sw_printf("false FAILED");
		else if (verbose) sw_printf("false OK\n");


		// Test SW_SPLIT_UINT64 byte order
		{
		SW_SPLIT_UINT64	v;
		
		v.u.high32 = 0;
		v.u.low32 = 1;

		if (v.ui64 != 1) sw_printf("SW_SPLIT_UINT64 low/high split failure");
		}

		// Test FILETIME byte order
		{
		FILETIME	v;
		
		v.dwHighDateTime = 0;
		v.dwLowDateTime = 1;

		if (*((sw_uint64_t *)&v) != 1) sw_printf("FILETIME low/high split failure");
		}
		}



int
sw_main(const SWString &prog, const SWStringArray &args)
 		{
		runtest_types();

		return 0;
		}

