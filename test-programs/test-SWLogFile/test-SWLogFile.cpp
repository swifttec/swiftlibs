#include <stdio.h>

#include <swift/SWLogFile.h>
#include <swift/sw_main.h>
#include <xplatform/sw_clock.h>

SWLogFile	theAppLog;


void
doLogMessages(int nsets)
		{
		double	start, finish;

		start = sw_clock_getCurrentTimeAsDouble();
		for (int i=0;i<nsets;i++)
			{
			theAppLog.writeLine(SWString().format("This is message %d of %d", i+1, nsets));
			}
		finish = sw_clock_getCurrentTimeAsDouble();

		printf("Log %d sets of messages to = %.3fs\n", nsets, finish-start);
		}

int
sw_main(const SWString &prog, const SWStringArray &args)
		{
		int			nbackups=4;
		int			maxsize=65536;
		int			flags=0;
		int			count=100000;
		SWString	filename="test-SWLogFile.log";

		for (int i=0;i<(int)args.size();i++)
			{
			SWString	arg=args.get(i);

			if (arg == "--backups")
				{
				if (++i < (int)args.size()) nbackups=args.get(i).toInt32(10);
				}
			else if (arg == "--max-size")
				{
				if (++i < (int)args.size()) maxsize=args.get(i).toInt32(10);
				}
			else if (arg == "--keep-open")
				{
				flags = flags | SWLogFile::KeepOpen;
				}
			else if (arg == "--auto-flush")
				{
				flags = flags | SWLogFile::AutoFlush;
				}
			else
				{
				count = arg.toInt32(10);
				}
			}

		
		printf("Generating %d entries in %s (maxsize=%d, nbackups=%d, flags=0x%x)\n", count, filename.c_str(), maxsize, nbackups, flags);

		double	start, finish;

		start = sw_clock_getCurrentTimeAsDouble();


		theAppLog.maxFileSize(maxsize);
		theAppLog.backupCount(nbackups);
		theAppLog.flags(flags);
		theAppLog.open(filename);

		doLogMessages(count);

		theAppLog.close();

		finish = sw_clock_getCurrentTimeAsDouble();

		printf("Total elasped time = %.3fs\n", finish-start);

		return 0;
		}

