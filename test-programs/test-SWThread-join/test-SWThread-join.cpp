#include <swift/sw_main.h>
#include <swift/SWThread.h>


class MyThread : public SWThread
		{
public:
		MyThread(int waitms);

		virtual int	run();

protected:
		int		m_waitms;
		};


MyThread::MyThread(int waitms) :
	m_waitms(waitms)
		{
		}

int
MyThread::run()
		{
		printf("Thread %08x: Sleeping %dms\n", getThreadId(), m_waitms);
		sleep(m_waitms);
		printf("Thread %08x: Stopping\n", getThreadId());

		return m_waitms;
		}


void
doThreadTest(int threadSleepMS, int mainSleepMS)
		{
		MyThread	*pThread;

		printf("Creating thread to sleep for %dms\n", threadSleepMS);
		pThread = new MyThread(threadSleepMS);
		pThread->start();
		
		printf("Sleeping %dms\n", mainSleepMS);
		sw_thread_sleep(mainSleepMS);

		printf("Waiting for thread\n");
		pThread->waitUntilStopped();

		printf("Thread exited with code %d\n\n", pThread->getExitCode());
		}


int
sw_main(const SWString &prog, const SWStringArray &args)
 		{
		doThreadTest(1000, 500);
		doThreadTest(500, 1000);

		return 0;
		}

