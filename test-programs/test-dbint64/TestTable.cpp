#include "TestTable.h"


static DBFieldDef	table_fieldlist[] =
{
	{	"id",			DBF_INT64,		4,	0	},
	{	"created",		DBF_DATETIME,	8,	0	},
	{	"updated",		DBF_DATETIME,	8,	0	},
	{	"vtext",		DBF_TEXT,		64,	0	},

	// End of list marker
	{	NULL,			DBF_MAX,		0,	0	}
};

static DBIndexDef table_indexlist[] =
{
	{	"primary",		"id",	true	},

	// End of list marker
	{	NULL,			NULL	}
};

#define TABLE_NAME		"testtable"
#define TABLE_VERSION	1

DB_IMPLEMENT_TABLE_METHODS(TestTable, TABLE_NAME, TABLE_VERSION, table_fieldlist, table_indexlist, "id")


TestTable::TestTable()
		{
		}


TestTable::TestTable(const TestTable &other)
		{
		*this = other;
		}
		

TestTable::~TestTable()
		{
		}


TestTable &
TestTable::operator=(const TestTable &other)
		{
		DBDataObject::operator=(other);

#define COPY(x)	x = other.x
		COPY(m_oldid);
#undef COPY

		return *this;
		}


bool
TestTable::operator==(const TestTable &other)
		{
		return (id() == other.id());
		}		



void
TestTable::clear()
		{
		DBDataObject::clear();
		m_oldid = 0;
		}


bool
TestTable::load(DBDatabase &db, int id)
		{
		SWString	where;
		
		where.format("where id = %lld", id);
		m_loaded = (db.getRecord(*this, where) == SW_STATUS_SUCCESS);
		if (m_loaded) saveIdentifier();
		
		return m_loaded;
		}


bool
TestTable::remove(DBDatabase &db)
		{
		bool	res=false;

		if (m_loaded)
			{
			SWString	sql;
		
			sql.format("delete from " TABLE_NAME "where id = %lld", m_oldid);
			res = (db.execute(sql) == SW_STATUS_SUCCESS);
			}
		
		return res;
		}


bool
TestTable::save(DBDatabase &db, bool forceinsert)
		{
		SWLocalTime	now;
		SWString	sql;
		bool		res;
		
		now.update();
		updated(now);
		if (isNewRecord() || forceinsert)
			{
			created(now);
			res = (db.insertRecord(*this) == SW_STATUS_SUCCESS);
			}
		else
			{
			sql.format("where id = %lld", m_oldid);
			res = (db.updateRecord(*this, sql) == SW_STATUS_SUCCESS);
			}
		
		if (res) saveIdentifier();

		return res;
		}


void
TestTable::saveIdentifier()
		{
		m_oldid = id();
		}
