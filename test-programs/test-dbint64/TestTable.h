#pragma once

#include <db/DBDataObject.h>
#include <db/DBGeneralDatabase.h>


class TestTable : public DBDataObject
		{
public:
		TestTable();
		virtual ~TestTable();

		TestTable(const TestTable &other);
		TestTable &	operator=(const TestTable &other);
		bool	operator==(const TestTable &other);
		
		// Save this record into the database
		virtual bool		load(DBDatabase &db, int id);
		
		// Save this record into the database
		virtual bool		save(DBDatabase &db, bool forceinsert=false);

		// Clear this record
		virtual void		clear();

		// Remove this record
		virtual bool		remove(DBDatabase &db);

		virtual void		saveIdentifier();

		sw_int64_t			id() const						{ return getInt64("id"); }
		void				id(sw_int64_t v)				{ set("id", v); }

		SWString			text() const					{ return getValue("vtext"); }
		void				text(const SWString &v)			{ set("vtext", v); }

		SWLocalTime			created() const					{ return getTime("created"); }
		void				created(const SWLocalTime &v)	{ set("created", v); }

		SWLocalTime			updated() const					{ return getTime("updated"); }
		void				updated(const SWLocalTime &v)	{ set("updated", v); }
		
public: // Overrides
		DB_DECLARE_TABLE_METHODS(TestTable);

private:
		sw_int64_t		m_oldid;
		};
