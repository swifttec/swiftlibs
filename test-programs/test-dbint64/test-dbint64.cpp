#include <swift/SWFilename.h>
#include <swift/sw_main.h>

#include "TestTable.h"


void
do_test(int dbtype, const SWString &dbparams)
		{
		DBGeneralDatabase	db;

		if (db.open(dbtype, dbparams) == SW_STATUS_SUCCESS)
			{
			printf("Successfully opened database(%d, %s)\n", dbtype, dbparams.c_str());

			TestTable	rec;
			sw_int64_t	v=1;
			SWString	s;

			db.checkTable(rec);
			db.execute("delete from " + rec.getTableName());

			for (int i=0;i<36;i++)
				{
				rec.clear();
				rec.id(v);
				rec.text(SWString().format("i=%d, v=%lld", i, v));
				rec.save(db);
				
				v <<= 1;
				}

			SWArray<TestTable>	ta;

			TestTable::loadRecordsFromDB(db, ta, "", "order by id");

			for (int i=0;i<(int)ta.size();i++)
				{
				TestTable	&t=ta[i];

				printf("Got %lld [%s]\n", t.id(), t.text().c_str());
				}

			db.close();
			printf("-------------------------------\n");
			}
		else
			{
			printf("Failed to open database(%d, %s)\n", dbtype, dbparams.c_str());
			}
		}


int
sw_main(const SWString &prog, const SWStringArray &args)
 		{
		// do_test(DBDatabase::DB_TYPE_DAO,	"filename=test.mdb&create=1");
		do_test(DBDatabase::DB_TYPE_SQLITE3, "filename=test.db&create=1");
		// do_test(DBDatabase::DB_TYPE_MYSQL, "host=localhost&port=3306&database=test&username=root&passwd=pass2db");

		return 0;
		}

