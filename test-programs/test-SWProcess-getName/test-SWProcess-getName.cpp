#include <swift/sw_main.h>
#include <swift/SWProcess.h>

void
showProcessInfo(int pid)
		{
		printf("PID %d = %s\n", pid, SWProcess::getName(pid).c_str());
		}



int
sw_main(const SWString &prog, const SWStringArray &args)
 		{
		if (args.size() == 0)
			{
			showProcessInfo(SWProcess::self());
			}
		else
			{
			for (int i=0;i<(int)args.size();i++)
				{
				showProcessInfo(args.get(i).toInt32(10));
				}
			}

		return 0;
		}

