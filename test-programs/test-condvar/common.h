/*
**	common.h	Common stuff for all test programs
*/

#include "TestObject.h"

#include <swift/swift.h>
#include <swift/SWLog.h>
#include <swift/SWGuard.h>
#include <stdio.h>
#include <stdarg.h>

extern int	stopOnTestFailure;
extern int	keepgoing;
extern int	verbose;
extern int	full;

int	compareIntNormal(const void *pData1, const void *pData2);
int compareIntReverse(const void *pData1, const void *pData2);
int compareIntCustom(const void *pData1, const void *pData2);

double		getDoubleTime();


/*
** This simple class is used for various tests
** especially ones involving collections.
*/
class IntRef
		{
public:
		// Basic constructor
		IntRef(int i=0)	: m_value(i)				{}

		// Copy constructor
		IntRef(const IntRef &other)					{ m_value = other.m_value; }

		// Assignment operator
		IntRef &	operator=(const IntRef &other)	{ m_value = other.m_value; return *this; }

		// Cast to int
		operator int() const						{ return m_value; }
		//operator int &()							{ return m_value; }

		// Assignment operator (from int)
		IntRef & operator=(int v)					{ m_value = v; return *this; }

		// Equality test
		bool	operator==(const IntRef &other)		{ return (other.m_value == m_value); }

private:
		int	m_value;
		};




void		commonSetTestObject(TestObject *pTestObject);
void		commonReport(sw_uint32_t flags, const char *file, int lineno, const char *fmt, ...);
void		commonReport(sw_uint32_t flags, const char *file, int lineno, const wchar_t *fmt, ...);
int			commonGetErrorCount();


#define SW_TEST_REGISTER_FUNCTION(group, name, function, description) \
		static int	rf_##function=registerTestFunction(group, AUTO, name, function, description)
	


#define SW_TEST_CHECK(expr) if (!(expr)) { SW_TEST_ERROR3("Check [%s] failed at line %d of %s", #expr, __LINE__, __FILE__); }


#define SW_TEST_GETERRORCOUNT()	commonGetErrorCount()

#define SW_TEST_INFO0(fmt)		commonReport(SW_LOG_MSG_INFO, __FILE__, __LINE__, "%s", fmt)
#define SW_TEST_WARNING0(fmt)	commonReport(SW_LOG_MSG_WARNING, __FILE__, __LINE__, "%s", fmt)
#define SW_TEST_ERROR0(fmt)		commonReport(SW_LOG_MSG_ERROR, __FILE__, __LINE__, "%s", fmt)
#define SW_TEST_DETAIL0(fmt)		commonReport(SW_LOG_MSG_DETAIL, __FILE__, __LINE__, "%s", fmt)

#define SW_TEST_INFO1(fmt, a)		commonReport(SW_LOG_MSG_INFO, __FILE__, __LINE__, fmt, a)
#define SW_TEST_WARNING1(fmt, a)	commonReport(SW_LOG_MSG_WARNING, __FILE__, __LINE__, fmt, a)
#define SW_TEST_ERROR1(fmt, a)		commonReport(SW_LOG_MSG_ERROR, __FILE__, __LINE__, fmt, a)
#define SW_TEST_DETAIL1(fmt, a)		commonReport(SW_LOG_MSG_DETAIL, __FILE__, __LINE__, fmt, a)

#define SW_TEST_INFO2(fmt, a, b)		commonReport(SW_LOG_MSG_INFO, __FILE__, __LINE__, fmt, a, b)
#define SW_TEST_WARNING2(fmt, a, b)		commonReport(SW_LOG_MSG_WARNING, __FILE__, __LINE__, fmt, a, b)
#define SW_TEST_ERROR2(fmt, a, b)		commonReport(SW_LOG_MSG_ERROR, __FILE__, __LINE__, fmt, a, b)
#define SW_TEST_DETAIL2(fmt, a, b)		commonReport(SW_LOG_MSG_DETAIL, __FILE__, __LINE__, fmt, a, b)

#define SW_TEST_INFO3(fmt, a, b, c)		commonReport(SW_LOG_MSG_INFO, __FILE__, __LINE__, fmt, a, b, c)
#define SW_TEST_WARNING3(fmt, a, b, c)	commonReport(SW_LOG_MSG_WARNING, __FILE__, __LINE__, fmt, a, b, c)
#define SW_TEST_ERROR3(fmt, a, b, c)	commonReport(SW_LOG_MSG_ERROR, __FILE__, __LINE__, fmt, a, b, c)
#define SW_TEST_DETAIL3(fmt, a, b, c)	commonReport(SW_LOG_MSG_DETAIL, __FILE__, __LINE__, fmt, a, b, c)

#define SW_TEST_INFO4(fmt, a, b, c, d)		commonReport(SW_LOG_MSG_INFO, __FILE__, __LINE__, fmt, a, b, c, d)
#define SW_TEST_WARNING4(fmt, a, b, c, d)	commonReport(SW_LOG_MSG_WARNING, __FILE__, __LINE__, fmt, a, b, c, d)
#define SW_TEST_ERROR4(fmt, a, b, c, d)		commonReport(SW_LOG_MSG_ERROR, __FILE__, __LINE__, fmt, a, b, c, d)
#define SW_TEST_DETAIL4(fmt, a, b, c, d)		commonReport(SW_LOG_MSG_DETAIL, __FILE__, __LINE__, fmt, a, b, c, d)

#define SW_TEST_INFO5(fmt, a, b, c, d, e)		commonReport(SW_LOG_MSG_INFO, __FILE__, __LINE__, fmt, a, b, c, d, e)
#define SW_TEST_WARNING5(fmt, a, b, c, d, e)	commonReport(SW_LOG_MSG_WARNING, __FILE__, __LINE__, fmt, a, b, c, d, e)
#define SW_TEST_ERROR5(fmt, a, b, c, d, e)		commonReport(SW_LOG_MSG_ERROR, __FILE__, __LINE__, fmt, a, b, c, d, e)
#define SW_TEST_DETAIL5(fmt, a, b, c, d, e)		commonReport(SW_LOG_MSG_DETAIL, __FILE__, __LINE__, fmt, a, b, c, d, e)

#define SW_TEST_INFO6(fmt, a, b, c, d, e, f)		commonReport(SW_LOG_MSG_INFO, __FILE__, __LINE__, fmt, a, b, c, d, e, f)
#define SW_TEST_WARNING6(fmt, a, b, c, d, e, f)		commonReport(SW_LOG_MSG_WARNING, __FILE__, __LINE__, fmt, a, b, c, d, e, f)
#define SW_TEST_ERROR6(fmt, a, b, c, d, e, f)		commonReport(SW_LOG_MSG_ERROR, __FILE__, __LINE__, fmt, a, b, c, d, e, f)
#define SW_TEST_DETAIL6(fmt, a, b, c, d, e, f)		commonReport(SW_LOG_MSG_DETAIL, __FILE__, __LINE__, fmt, a, b, c, d, e, f)

#define SW_TEST_INFO7(fmt, a, b, c, d, e, f, g)			commonReport(SW_LOG_MSG_INFO, __FILE__, __LINE__, fmt, a, b, c, d, e, f, g)
#define SW_TEST_WARNING7(fmt, a, b, c, d, e, f, g)		commonReport(SW_LOG_MSG_WARNING, __FILE__, __LINE__, fmt, a, b, c, d, e, f, g)
#define SW_TEST_ERROR7(fmt, a, b, c, d, e, f, g)		commonReport(SW_LOG_MSG_ERROR, __FILE__, __LINE__, fmt, a, b, c, d, e, f, g)
#define SW_TEST_DETAIL7(fmt, a, b, c, d, e, f, g)		commonReport(SW_LOG_MSG_DETAIL, __FILE__, __LINE__, fmt, a, b, c, d, e, f, g)

#define SW_TEST_INFO8(fmt, a, b, c, d, e, f, g, h)		commonReport(SW_LOG_MSG_INFO, __FILE__, __LINE__, fmt, a, b, c, d, e, f, g, h)
#define SW_TEST_WARNING8(fmt, a, b, c, d, e, f, g, h)	commonReport(SW_LOG_MSG_WARNING, __FILE__, __LINE__, fmt, a, b, c, d, e, f, g, h)
#define SW_TEST_ERROR8(fmt, a, b, c, d, e, f, g, h)		commonReport(SW_LOG_MSG_ERROR, __FILE__, __LINE__, fmt, a, b, c, d, e, f, g, h)
#define SW_TEST_DETAIL8(fmt, a, b, c, d, e, f, g, h)		commonReport(SW_LOG_MSG_DETAIL, __FILE__, __LINE__, fmt, a, b, c, d, e, f, g, h)

#define SW_TEST_INFO9(fmt, a, b, c, d, e, f, g, h, i)		commonReport(SW_LOG_MSG_INFO, __FILE__, __LINE__, fmt, a, b, c, d, e, f, g, h, i)
#define SW_TEST_WARNING9(fmt, a, b, c, d, e, f, g, h, i)	commonReport(SW_LOG_MSG_WARNING, __FILE__, __LINE__, fmt, a, b, c, d, e, f, g, h, i)
#define SW_TEST_ERROR9(fmt, a, b, c, d, e, f, g, h, i)		commonReport(SW_LOG_MSG_ERROR, __FILE__, __LINE__, fmt, a, b, c, d, e, f, g, h, i)
#define SW_TEST_DETAIL9(fmt, a, b, c, d, e, f, g, h, i)		commonReport(SW_LOG_MSG_DETAIL, __FILE__, __LINE__, fmt, a, b, c, d, e, f, g, h, i)

