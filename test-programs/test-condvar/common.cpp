/*
** runtest.c	Program to time and run tests
*/

#include "common.h"

#include <time.h>
#if !defined(WIN32) && !defined(_WIN32)
	#include <sys/time.h>
#endif
#include <string.h>

#include <swift/SWException.h>
#include <swift/SWThreadMutex.h>
#include <swift/SWGuard.h>
#include <swift/sw_printf.h>

int		full=0;
int		keepgoing=0;
int		verbose=1;
int		stopOnTestFailure=0;

static SWThreadMutex	g_mutex;
static SWLog			g_log("stdout:");
static int				g_errcount=0;
static int				g_wrncount=0;




static void
report(sw_uint32_t flags, const char *file, int lineno, const char *fmt, ...)
		{
		va_list	ap;

		va_start(ap, fmt);

		SWString	msg;
		SWString	istr;

		// for (int i=0;i<m_indent;i++) istr += " ";

		msg.formatv(fmt, ap);
		g_log.report(flags, 0, file, lineno, 0, 0, "%s%s",istr.c_str(), msg.c_str());

		va_end(ap);

		if ((flags & SW_LOG_TYPE_ERROR) != 0) g_errcount++;
		else if ((flags & SW_LOG_TYPE_WARNING) != 0) g_wrncount++;
		}


static void
report(sw_uint32_t flags, const char *file, int lineno, const wchar_t *fmt, ...)
		{
		va_list	ap;

		va_start(ap, fmt);

		SWString	msg;
		SWString	istr;

		// for (int i=0;i<m_indent;i++) istr += " ";

		msg.formatv(fmt, ap);
		g_log.report(flags, 0, file, lineno, 0, 0, L"%ls%ls", istr.w_str(), msg.w_str());

		va_end(ap);

		if ((flags & SW_LOG_TYPE_ERROR) != 0) g_errcount++;
		else if ((flags & SW_LOG_TYPE_WARNING) != 0) g_wrncount++;
		}




int
compareIntNormal(const void *pData1, const void *pData2)
		{
		const int	*pInt1=(const int *)pData1;
		const int	*pInt2=(const int *)pData2;

		return (*pInt1==*pInt2?0:(*pInt1<*pInt2?-1:1));
		}


int
compareIntReverse(const void *pData1, const void *pData2)
		{
		const int	*pInt1=(const int *)pData1;
		const int	*pInt2=(const int *)pData2;

		return (*pInt1==*pInt2?0:(*pInt1<*pInt2?1:-1));
		}


int
compareIntCustom(const void *pData1, const void *pData2)
		{
		const int	*pInt1=(const int *)pData1;
		const int	*pInt2=(const int *)pData2;
		int			r=0;

		int	v1=*pInt1 / 10;
		int	v2=*pInt2 / 10;

		if (v1 < v2) r = -1;
		else if (v1 > v2) r = 1;
		else
			{
			v1=*pInt1 % 10;
			v2=*pInt2 % 10;
			if (v1 < v2) r = 1;
			else if (v1 > v2) r = -1;
			else r = 0;
			}

		return r;
		}


int
commonGetErrorCount()
		{
		int		res=0;

		res = g_errcount;

		return res;
		}


void
commonReport(sw_uint32_t flags, const char *file, int lineno, const char *fmt, ...)
		{
		SWMutexGuard	guard(g_mutex);

		va_list	ap;

		va_start(ap, fmt);

		SWString	msg;

		msg.formatv(fmt, ap);
		report(flags, file, lineno, "%s", msg.c_str());

		va_end(ap);
		}


void
commonReport(sw_uint32_t flags, const char *file, int lineno, const wchar_t *fmt, ...)
		{
		SWMutexGuard	guard(g_mutex);

		va_list	ap;

		va_start(ap, fmt);

		SWString	msg;

		msg.formatv(fmt, ap);
		report(flags, file, lineno, L"%ls", msg.w_str());

		va_end(ap);
		}
