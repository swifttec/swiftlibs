#include "TestObject.h"

#include <swift/sw_printf.h>
#include <swift/SWTime.h>

#if !defined(SW_PLATFORM_WINDOWS)
	#include <sys/time.h>
#endif


TestObject::~TestObject()
		{
		}


TestObject::TestObject(const TestObject &other)
		{
		*this = other;
		}


TestObject &
TestObject::operator=(const TestObject &other)
		{
#define COPY(x)	x = other.x
		COPY(m_runtype);
		COPY(m_rungroup);
		COPY(m_name);
		COPY(m_description);
		COPY(m_testfunc);
		COPY(m_errcount);
		COPY(m_wrncount);
		COPY(m_starttime);
		COPY(m_endtime);
		COPY(m_deleteFlag);
		COPY(m_pLog);
		COPY(m_indent);
#undef COPY

		return *this;
		}


bool
TestObject::operator==(const TestObject &other)
		{
		return (m_name == other.m_name);
		}


TestObject::TestObject(int type, int group, const SWString &name, const SWString &desc) :
	m_runtype(type),
	m_rungroup(group),
	m_name(name),
	m_description(desc),
	m_testfunc(NULL),
	m_errcount(0),
	m_wrncount(0),
	m_starttime(0.0),
	m_endtime(0.0),
	m_deleteFlag(false),
	m_pLog(0),
	m_indent(0)
		{
		}


static bool
ends(const char *s1, const char *s2)
		{
		bool	res=false;
		size_t	l1=strlen(s1);
		size_t	l2=strlen(s2);

		if (l1 >= l2)
			{
			res = (strcmp(s1 + l1 - l2, s2) == 0);
			}

		return res;
		}


static bool
ends(const wchar_t *s1, const wchar_t *s2)
		{
		bool	res=false;
		size_t	l1=wcslen(s1);
		size_t	l2=wcslen(s2);

		if (l1 >= l2)
			{
			res = (wcscmp(s1 + l1 - l2, s2) == 0);
			}

		return res;
		}


void
TestObject::report(sw_uint32_t flags, const char *file, int lineno, const char *fmt, ...)
		{
		va_list	ap;

		va_start(ap, fmt);

		if (m_pLog == NULL)
			{
			const char	*prefix="INF";

			if ((flags & SW_LOG_TYPE_ERROR) != 0) prefix = "ERR";
			else if ((flags & SW_LOG_TYPE_WARNING) != 0) prefix = "WRN";

			sw_printf("%s: ", prefix);
			sw_vprintf(fmt, ap);
			
			if (!ends(fmt, "\n")) sw_printf("\n");
			}
		else
			{
			SWString	msg;
			SWString	istr;

			for (int i=0;i<m_indent;i++) istr += " ";

			msg.formatv(fmt, ap);
			m_pLog->report(flags, 0, file, lineno, 0, 0, "%s%s",istr.c_str(), msg.c_str());
			}

		va_end(ap);

		if ((flags & SW_LOG_TYPE_ERROR) != 0) m_errcount++;
		else if ((flags & SW_LOG_TYPE_WARNING) != 0) m_wrncount++;
		}


void
TestObject::report(sw_uint32_t flags, const char *file, int lineno, const wchar_t *fmt, ...)
		{
		va_list	ap;

		va_start(ap, fmt);

		if (m_pLog == NULL)
			{
			const wchar_t	*prefix=L"INF";

			if ((flags & SW_LOG_TYPE_ERROR) != 0) prefix = L"ERR";
			else if ((flags & SW_LOG_TYPE_WARNING) != 0) prefix = L"WRN";

			sw_wprintf(L"%ls: ", prefix);
			sw_vwprintf(fmt, ap);

			if (!ends(fmt, L"\n")) sw_wprintf(L"\n");
			}
		else
			{
			SWString	msg;
			SWString	istr;

			for (int i=0;i<m_indent;i++) istr += " ";

			msg.formatv(fmt, ap);
			m_pLog->report(flags, 0, file, lineno, 0, 0, L"%ls%ls", istr.w_str(), msg.w_str());
			}

		va_end(ap);

		if ((flags & SW_LOG_TYPE_ERROR) != 0) m_errcount++;
		else if ((flags & SW_LOG_TYPE_WARNING) != 0) m_wrncount++;
		}


double
TestObject::getDoubleTime()
		{
		SWTime	now(true);

		return double(now);
		}


void
TestObject::runtest()
		{
		m_errcount = m_wrncount = 0;

		report(SW_LOG_MSG_INFO, __FILE__, __LINE__, "\nStarting %s test....\n", m_name.c_str());

		m_indent += 2;
		m_starttime = getDoubleTime();
		try
			{
			if (m_testfunc != NULL)
				{
				m_testfunc();
				}
			else
				{
				report(SW_LOG_MSG_ERROR, __FILE__, __LINE__, "NULL test function");
				}
			}
		catch (SWException &e)
			{
			report(SW_LOG_MSG_ERROR, __FILE__, __LINE__, "Got unexpected exception - %s", (const char *)e);
			}
		catch (...)
			{
			report(SW_LOG_MSG_ERROR, __FILE__, __LINE__, "Got unexpected unknown exception");
			}

		m_endtime = getDoubleTime();
		m_indent -= 2;
		report(SW_LOG_MSG_INFO, __FILE__, __LINE__, "Completed %s test (%.3f seconds, %d error%s, %d warning%s)\n", 
			m_name.c_str(),
			(m_endtime - m_starttime),
			m_errcount,
			m_errcount==1?"":"s",
			m_wrncount,
			m_wrncount==1?"":"s"
			);

		if (m_pLog == NULL)
			{
			m_pLog->flush();
			fflush(stdout);
			}
		}
