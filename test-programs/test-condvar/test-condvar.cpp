#include <stdio.h>

#include "common.h"

#include <swift/SWThread.h>
#include <swift/SWGuard.h>
#include <swift/SWThreadMutex.h>
#include <swift/SWThreadCondVar.h>

#define MAXTHREADS 100

static sw_uint64_t			waitms=(sw_uint64_t)SW_TIMEOUT_INFINITE;

typedef struct cvinfo
		{
		SWCondVar	*pcv;	// Pointer to SWCondVar
		volatile bool		awake;
		volatile bool		waiting;
		volatile bool		timeout;
		volatile bool		closed;
		} CVINFO;


int
cvwait_thread(SWThread *pThread, void *param)
		{
		CVINFO		*pci=(CVINFO *)param;
		SWCondVar	*pCV=pci->pcv;
		sw_status_t	r;

		if (verbose) SW_TEST_INFO1("  [%d] Started\n", SWThread::self());

		while (pThread->runnable())
			{
			try
				{
				pci->waiting = true;
				pci->timeout = false;
				pci->awake = false;
				if (verbose)
					{
					if (waitms != SW_TIMEOUT_INFINITE) SW_TEST_INFO2("    [%d] Waiting (%lldms timeout)\n", SWThread::self(), waitms);
					else SW_TEST_INFO1("    [%d] Waiting\n", SWThread::self());
					}
				r = pCV->wait(waitms);
				pci->waiting = false;

				if (r == SW_STATUS_TIMEOUT)
					{
					if (verbose) SW_TEST_INFO1("    [%d] TIMEOUT\n", SWThread::self());
					pci->timeout = true;
					SWThread::sleep(500);
					}
				else if (r == SW_STATUS_SUCCESS)
					{
					if (verbose) SW_TEST_INFO1("    [%d] AWAKE!\n", SWThread::self());
					pci->awake = true;
					}
				else
					{
					SW_TEST_ERROR2("Got error %d (0x%x) in wait", r, r);
					break;
					}
				}
			catch (SWCondVarException &)
				{
				if (verbose) SW_TEST_INFO1("    [%d] CLOSED\n", SWThread::self());
				pci->closed = true;
				break;
				}

			// Sleep a short amount to allow the main thread to check us
			SWThread::sleep(200);
			}

		if (verbose) SW_TEST_INFO1("  [%d] Stopped\n", SWThread::self());

		return 0;
		}


typedef struct _ti_
		{
		SWCondVar		*cv;
		sw_thread_id_t	tid;
		int				count;
		} TINFO;

typedef struct
		{
		int		id;
		TINFO	*ti;
		} TI;


int
cvsignal_thread(SWThread *, void *param)
		{
		TI			*pti = (TI *)param;
		TINFO		*pt = pti->ti;
		int			width= 4; // + (pti->id * 2);

		if (verbose) SW_TEST_INFO3("%-*s[%d] Sleeping\n", width, "", SWThread::self());

		SWThread::sleep(1000);

		if (verbose) SW_TEST_INFO3("%-*s[%d] Waiting for mutex\n", width, "", SWThread::self());
		pt->cv->acquire();

		if (verbose) SW_TEST_INFO3("%-*s[%d] Updating common info.\n", width, "", SWThread::self());
		pt->tid = SWThread::self();
		--(pt->count);

		if (verbose) SW_TEST_INFO3("%-*s[%d] Signalling condvar\n", width, "", SWThread::self());
		pt->cv->signal();

		if (verbose) SW_TEST_INFO3("%-*s[%d] Unlocking mutex\n", width, "", SWThread::self());
		pt->cv->release();

		if (verbose) SW_TEST_INFO3("%-*s[%d] Exiting\n", width, "", SWThread::self());

		return 0;
		}




/**
*** The signal tests 2 areas of functionality.
***
*** 1.	It tests the use of condvar with an externally provided mutex.
*** 2.	It tests the operation of the condvar in a scenario when the
***		mutex has already be acquired before signal, wait, etc are 
***		called.
**/
int
run_signal_test(int nthreads)
		{
		SWThreadMutex	mu;
		int				res=0;

		SW_TEST_INFO1("  Running signal test with mutex and %d threads\n", nthreads);

		SWThreadCondVar	cv(mu);
		SWThread				*ta[MAXTHREADS];
		TI						ti[MAXTHREADS];
		int						i;
		TINFO					tinfo;

		tinfo.cv = &cv;
		tinfo.count = 0;
		tinfo.tid = 0;

		mu.acquire();

		for (i=0;i<nthreads;i++)
			{
			ti[i].id = i;
			ti[i].ti = &tinfo;

			ta[i] = new SWThread(cvsignal_thread, &ti[i]);
			ta[i]->start();
			tinfo.count++;
			}

		if (verbose) SW_TEST_INFO1("    Started %d threads\n", nthreads);
		while (tinfo.count > 0)
			{
			sw_status_t r = cv.wait(5000);
			if (r != SW_STATUS_SUCCESS) 
				{
				if (r == SW_STATUS_TIMEOUT) SW_TEST_ERROR0("Got timeout in wait");
				else SW_TEST_ERROR2("Got error %d (0x%x) in wait", r, r);
				break;
				}

			if (verbose) SW_TEST_INFO2("    Last signal from thread %d, count=%d\n", tinfo.tid, tinfo.count);
			}
		
		mu.release();

		for (i=0;i<nthreads;i++)
			{
			delete ta[i];
			}

		return res;
		}

#undef nthreads

typedef struct cvstats
		{
		volatile int	aCount;
		volatile int	wCount;
		volatile int	tCount;
		volatile int	cCount;
		} CVSTATS;

void
getstats(CVINFO *cvi, int count, CVSTATS &stats)
		{
		memset(&stats, 0, sizeof(stats));

		for (int i=0;i<count;i++)
			{
			if (cvi[i].waiting)		stats.wCount++;
			if (cvi[i].awake)		stats.aCount++;
			if (cvi[i].timeout)		stats.tCount++;
			if (cvi[i].closed)		stats.cCount++;
			}
		}


/**
*** this test creates a condvar using it's own mutex and then fires off 
*** 4 threads to wait on the condvar.
***
*** The test code then alternates signal and broadcasts to ensure that 
*** both signals and broadcasts are picked up correctly. Finally the close
*** should force a close event.
***
*** The wait thread can work in 2 modes, with or without timeouts.
**/
int
run_wait_test(int nthreads)
		{
		SWThreadCondVar	cv;
		int				i, j, res=0;
		CVSTATS			stats;

		SW_TEST_INFO1("  Running condvar signal/broadcast test with %d threads\n", nthreads);

		{ // BEGIN THREAD SECTION
		CVINFO		cvi[MAXTHREADS];
		SWThread	*pt[MAXTHREADS];

		for (i=0;i<nthreads;i++)
			{
			memset(&cvi[i], 0, sizeof(cvi[0]));
			cvi[i].pcv = &cv;
			pt[i] = new SWThread(cvwait_thread, &cvi[i]);
			pt[i]->start();
			}

		if (verbose) SW_TEST_INFO1("    Started %d threads\n", nthreads);
		SWThread::sleep(500);

		for (j=0;j<10;j++)
			{
			// Check all the threads are in the wait state.
			getstats(cvi, nthreads, stats);

			if (stats.wCount != nthreads)
				{
				SW_TEST_ERROR1("Incorrect number of threads (%d) are waiting before signal", stats.wCount);
				break;
				}

			// Do a signal - should wake only 1 thread
			if (verbose) SW_TEST_INFO0("Signal\n");
			cv.signal();

			// Sleep to allow the other threads to wake up
			SWThread::sleep(100);

			getstats(cvi, nthreads, stats);

			if (stats.aCount != 1)
				{
				SW_TEST_ERROR1("Incorrect number of threads (%d) are awake after signal", stats.aCount);
				break;
				}

			if (stats.wCount != (nthreads - 1))
				{
				SW_TEST_ERROR1("Incorrect number of threads (%d) are waiting after signal", stats.wCount);
				break;
				}

			// Sleep to allow all threads back to a waiting state
			SWThread::sleep(500);

			// Check all the threads are in the wait state.
			getstats(cvi, nthreads, stats);

			if (stats.wCount != nthreads)
				{
				SW_TEST_ERROR1("Incorrect number of threads (%d) are waiting before broadcast", stats.wCount);
				break;
				}

			// Before we do a broadcast set the wait timeout to set the thread into timed
			// mode for the next test.
			waitms = 200;

			// Do a broadcast - should wake all threads
			if (verbose) SW_TEST_INFO0("Broadcast\n");
			cv.broadcast();

			// Sleep to allow the other threads to wake up
			SWThread::sleep(100);

			getstats(cvi, nthreads, stats);

			if (stats.aCount != nthreads)
				{
				SW_TEST_ERROR1("Incorrect number of threads (%d) are awake after broadcast", stats.aCount);
				break;
				}

			if (stats.wCount != 0)
				{
				SW_TEST_ERROR1("Incorrect number of threads (%d) are waiting after broadcast", stats.wCount);
				break;
				}

			// Sleep to allow all threads back to a waiting state
			SWThread::sleep(400);


			// Get the stats before the threads timeout and reset the
			// wait period to infinite.
			waitms = (sw_uint64_t)SW_TIMEOUT_INFINITE;
			getstats(cvi, nthreads, stats);

			if (stats.wCount != 0)
				{
				SW_TEST_ERROR1("Incorrect number of threads (%d) are waiting (timeout test)", stats.wCount);
				break;
				}

			if (stats.tCount != nthreads)
				{
				SW_TEST_ERROR1("Incorrect number of threads (%d) are timed-out (timeout test)", stats.tCount);
				break;
				}

			// Sleep to allow all threads back to a waiting state
			SWThread::sleep(1000);
			}
		
		if (verbose) SW_TEST_INFO0("Close\n");
		cv.close();

		// Sleep to allow the other threads to wake up
		SWThread::sleep(1000);

		getstats(cvi, nthreads, stats);

		if (stats.cCount != nthreads)
			{
			SW_TEST_ERROR1("Incorrect number of threads (%d) are closed", stats.cCount);
			}

		SWThread::sleep(100);

		for (i=0;i<nthreads;i++) delete pt[i];

		} // END THREAD SECTION

		SW_TEST_INFO1("  condvar signal/broadcast test with %d threads - COMPLETE\n", nthreads);

		return res;
		}


/**
*** The signal tests 2 areas of functionality.
***
*** 1.	It tests the use of condvar with an externally provided mutex.
*** 2.	It tests the operation of the condvar in a scenario when the
***		mutex has already be acquired before signal, wait, etc are 
***		called.
**/
int
run_signal_test_external_mutex(int nthreads)
		{
		SWThreadMutex	mu;
		int				res=0;

		SW_TEST_INFO1("  Running signal test with external mutex and %d threads and an external mutex\n", nthreads);

		SWThreadCondVar	cv(mu);
		SWThread				*ta[MAXTHREADS];
		TI						ti[MAXTHREADS];
		int						i;
		TINFO					tinfo;

		tinfo.cv = &cv;
		tinfo.count = 0;
		tinfo.tid = 0;

		mu.acquire();

		for (i=0;i<nthreads;i++)
			{
			ti[i].id = i;
			ti[i].ti = &tinfo;

			ta[i] = new SWThread(cvsignal_thread, &ti[i]);
			ta[i]->start();
			tinfo.count++;
			}

		if (verbose) SW_TEST_INFO1("    Started %d threads\n", nthreads);
		while (tinfo.count > 0)
			{
			sw_status_t r = cv.wait(5000);
			if (r != SW_STATUS_SUCCESS) 
				{
				if (r == SW_STATUS_TIMEOUT) SW_TEST_ERROR0("Got timeout in wait");
				else SW_TEST_ERROR2("Got error %d (0x%x) in wait", r, r);
				break;
				}

			if (verbose) SW_TEST_INFO2("    Last signal from thread %d, count=%d\n", tinfo.tid, tinfo.count);
			}
		
		mu.release();

		for (i=0;i<nthreads;i++)
			{
			delete ta[i];
			}

		return res;
		}

#undef nthreads


/**
*** this test creates a condvar using it's own mutex and then fires off 
*** 4 threads to wait on the condvar.
***
*** The test code then alternates signal and broadcasts to ensure that 
*** both signals and broadcasts are picked up correctly. Finally the close
*** should force a close event.
***
*** The wait thread can work in 2 modes, with or without timeouts.
**/
int
run_wait_test_external_mutex(int nthreads)
		{
		SWThreadMutex	mu;
		int				res=0;

		{ // BEGIN MUTEX SECTION
		SWThreadCondVar	cv(mu);
		int				i, j;
		CVSTATS			stats;

		SW_TEST_INFO1("  Running condvar signal/broadcast test with %d threads and an external mutex\n", nthreads);

		{ // BEGIN THREAD SECTION
		CVINFO		cvi[MAXTHREADS];
		SWThread	*pt[MAXTHREADS];

		for (i=0;i<nthreads;i++)
			{
			memset(&cvi[i], 0, sizeof(cvi[0]));
			cvi[i].pcv = &cv;
			pt[i] = new SWThread(cvwait_thread, &cvi[i]);
			pt[i]->start();
			}

		if (verbose) SW_TEST_INFO1("    Started %d threads\n", nthreads);
		SWThread::sleep(500);

		for (j=0;j<10;j++)
			{
			// Check all the threads are in the wait state.
			getstats(cvi, nthreads, stats);

			if (stats.wCount != nthreads)
				{
				SW_TEST_ERROR1("Incorrect number of threads (%d) are waiting before signal", stats.wCount);
				break;
				}

			// Do a signal - should wake only 1 thread
			if (verbose) SW_TEST_INFO0("Signal\n");
			mu.acquire();
			cv.signal();
			mu.release();

			// Sleep to allow the other threads to wake up
			SWThread::sleep(100);

			getstats(cvi, nthreads, stats);

			if (stats.aCount != 1)
				{
				SW_TEST_ERROR1("Incorrect number of threads (%d) are awake after signal", stats.aCount);
				break;
				}

			if (stats.wCount != (nthreads - 1))
				{
				SW_TEST_ERROR1("Incorrect number of threads (%d) are waiting after signal", stats.wCount);
				break;
				}

			// Sleep to allow all threads back to a waiting state
			SWThread::sleep(500);

			// Check all the threads are in the wait state.
			getstats(cvi, nthreads, stats);

			if (stats.wCount != nthreads)
				{
				SW_TEST_ERROR1("Incorrect number of threads (%d) are waiting before broadcast", stats.wCount);
				break;
				}

			// Before we do a broadcast set the wait timeout to set the thread into timed
			// mode for the next test.
			waitms = 200;

			// Do a broadcast - should wake all threads
			if (verbose) SW_TEST_INFO0("Broadcast\n");
			mu.acquire();
			cv.broadcast();
			mu.release();

			// Sleep to allow the other threads to wake up
			SWThread::sleep(100);

			getstats(cvi, nthreads, stats);

			if (stats.aCount != nthreads)
				{
				SW_TEST_ERROR1("Incorrect number of threads (%d) are awake after broadcast", stats.aCount);
				break;
				}

			if (stats.wCount != 0)
				{
				SW_TEST_ERROR1("Incorrect number of threads (%d) are waiting after broadcast", stats.wCount);
				break;
				}

			// Sleep to allow all threads back to a waiting state
			SWThread::sleep(400);


			// Get the stats before the threads timeout and reset the
			// wait period to infinite.
			waitms = (sw_uint64_t)SW_TIMEOUT_INFINITE;
			getstats(cvi, nthreads, stats);

			if (stats.wCount != 0)
				{
				SW_TEST_ERROR1("Incorrect number of threads (%d) are waiting (timeout test)", stats.wCount);
				break;
				}

			if (stats.tCount != nthreads)
				{
				SW_TEST_ERROR1("Incorrect number of threads (%d) are timed-out (timeout test)", stats.tCount);
				break;
				}

			// Sleep to allow all threads back to a waiting state
			SWThread::sleep(1000);
			}
		
		if (verbose) SW_TEST_INFO0("Close\n");
		mu.acquire();
		cv.close();
		mu.release();

		// Sleep to allow the other threads to wake up
		SWThread::sleep(1000);

		getstats(cvi, nthreads, stats);

		if (stats.cCount != nthreads)
			{
			SW_TEST_ERROR1("Incorrect number of threads (%d) are closed", stats.cCount);
			}

		SWThread::sleep(100);

		for (i=0;i<nthreads;i++) delete pt[i];

		} // END THREAD SECTION
		} // END MUTEX SECTION

		return res;
		}


int
main(int , char **)
		{
		int	r=0;

		try
			{
			SWThread::setConcurrency(MAXTHREADS);

			if (r == 0) r = run_wait_test(verbose?10:MAXTHREADS);
			if (r == 0) r = run_signal_test(verbose?10:MAXTHREADS);
			if (r == 0) r = run_wait_test_external_mutex(verbose?10:MAXTHREADS);
			if (r == 0) r = run_signal_test_external_mutex(verbose?10:MAXTHREADS);
			}
		catch (SWException &e)
			{
			printf("got exception %s at line %d of %s\n", e.toString(), e.line(), e.file());
			}

		return 0;
		}
