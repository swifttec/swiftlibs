#ifndef __TestObject_h__
#define __TestObject_h__

#include <swift/SWString.h>
#include <swift/SWLog.h>

typedef void (*TestFunction) (void);

class TestObject
		{
public:
		TestObject(const TestObject &other);
		TestObject(int type, int group, const SWString &name, const SWString &desc);
		virtual ~TestObject();

		TestObject &		operator=(const TestObject &other);
		bool				operator==(const TestObject &other);

		void				report(sw_uint32_t flags, const char *file, int lineno, const char *fmt, ...);
		void				report(sw_uint32_t flags, const char *file, int lineno, const wchar_t *fmt, ...);

		double				getDoubleTime();

		void				setLog(SWLog *pLog)					{ m_pLog = pLog; }
		void				setTestFunction(TestFunction func)	{ m_testfunc = func; }
		void				setDeleteFlag(bool v)				{ m_deleteFlag = v; }

		const SWString &	name() const			{ return m_name; }
		const SWString &	description() const		{ return m_description; }
		int					runtype() const			{ return m_runtype; }
		int					rungroup() const		{ return m_rungroup; }
		int					errcount() const		{ return m_errcount; }
		int					wrncount() const		{ return m_wrncount; }
		bool				deleteFlag() const		{ return m_deleteFlag; }

public:
		virtual void	runtest();

private:
		int				m_runtype;		///< The test run type (auto, manual, etc)
		int				m_rungroup;		///< The test group
		SWString		m_name;			///< The name of the test
		SWString		m_description;	///< The test description
		unsigned int	m_errcount;		///< The number of errors encountered during the test run
		unsigned int	m_wrncount;		///< The number of warnings encountered during the test run
		TestFunction	m_testfunc;		///< The test function (if a function-style test)
		double			m_starttime;	///< The time the test started
		double			m_endtime;		///< The time the test started
		bool			m_deleteFlag;	///< A flag to indicate that the list should delete the object
		SWLog			*m_pLog;		///< The logger for all messages
		int				m_indent;		///< A flag indicating that the test is currently running.
		};


#endif // __TestObject_h__
