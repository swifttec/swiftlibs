#include <swift/SWTime.h>
#include <swift/sw_main.h>


void
dotest(const SWTime &t, double adjust)
		{
		SWTime	t2;

		t2 = t;
		// t2.adjust(adjust);
		t2 += adjust;

		printf("%s adjust by %.2f = %s (%.2f)\n", t.toString("%d/%m/%Y %H:%M:%S.%f").c_str(), adjust, t2.toString("%d/%m/%Y %H:%M:%S.%f").c_str(), t2.toDouble()-t.toDouble());
		}


int
sw_main(const SWString &prog, const SWStringArray &args)
 		{
		SWTime	t;

		t.update();
		printf("%s\n", t.toString("%d/%m/%Y %H:%M:%S.%f").c_str());
				
		return 0;
		}

