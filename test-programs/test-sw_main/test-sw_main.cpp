#include <swift/sw_main.h>

int
sw_main(const SWString &prog, const SWStringArray &args)
 		{
		printf("Prog: %s\n", prog.c_str());
		printf("Args: %d\n", (int)args.size());

		for (int i=0;i<(int)args.size();i++)
			{
			printf("%4d: %s\n", i, args.get(i).c_str());
			}

		return 0;
		}

