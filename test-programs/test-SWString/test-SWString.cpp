// test-VeristoreDataBlock.cpp : Defines the entry point for the console application.
//

#include <stdio.h>

#include <swift/SWString.h>

static void show(const char *msg, const SWString &v)
		{
		printf("%s: %s (cs=%d)\n", msg, v.c_str(), (int)v.charSize());
		}


int
main()
		{
		SWString		s, t, u;
		SWString		a("aaa");
		SWString		w(L"www");
		const char		*cs="char string";
		const wchar_t	*ws=L"wchar_t string";

		s = "fred";
		show("Assign fred", s);

		s = a + "bbb";
		show("Assign a + \"bbb\"", s);

		s = "bbb" + a;
		show("Assign \"bbb\" + a", s);

		s = a + 'X';
		show("Assign a + 'X'", s);

		s = 'X' + a;
		show("Assign 'X' + a", s);

		s = cs + a;
		show("Assign cs + a", s);

		s = ws + a;
		show("Assign ws + a", s);

		s = a + ws;
		show("Assign a + ws", s);

		s = "ABC";
		t = "def";
		u = s + t;
		show("Assign s + t", u);

		return 0;
		}

