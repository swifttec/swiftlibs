#include <swift/SWAppConfig.h>
#include <swift/sw_main.h>

int
sw_main(const SWString &prog, const SWStringArray &args)
 		{
		SWAppConfig	theAppConfig;


		theAppConfig.set("PRODUCT_NAME", "SwiftLibs");
		theAppConfig.set("PROGRAM_NAME", "testSWAppConfig");
		theAppConfig.init(prog, args);

		#undef SHOW
		#define SHOW(x)	printf("%-16s = %s\n", #x, theAppConfig.getString(#x).c_str())

		printf("------------------------------------------------------------------\n");
		SWIterator<SWString>	it=theAppConfig.keys();

		while (it.hasNext())
			{
			SWString	param=*it.next();

			printf("%-16s = %s\n", param.c_str(), theAppConfig.getString(param).c_str());
			}

		printf("------------------------------------------------------------------\n");

		#undef SHOW
		#define SHOW(x)	printf("%-24s = %s\n", #x, theAppConfig.getFolder(SWAppConfig::x).c_str())

		for (int i=0;i<(int)SWAppConfig::MaxFolderID;i++)
			{
			printf("%-24s = %s\n", theAppConfig.getFolderName(i).c_str(), theAppConfig.getFolder(i).c_str());
			}

		printf("------------------------------------------------------------------\n");


		return 0;
		}

