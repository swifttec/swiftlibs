#include <swift/SWFilename.h>
#include <swift/sw_main.h>
#include <swift/SWLocalTime.h>
#include <swift/sw_registry.h>
#include <xplatform/sw_clock.h>


void
check(SWRegistryHive hive, const SWString &key, const SWString &param)
		{
		if (sw_registry_isValue(hive, key, param))
			{
			SWValue	v;

			v = sw_registry_getValue(hive, key, param);
			printf("%s/%s/%s = %s\n", SWRegistryKey::hiveAsString(hive).c_str(), key.c_str(), param.c_str(), v.toString().c_str());
			}
		else
			{
			printf("Software/SwiftTec/test-registry/Count is not a value\n");
			}
		}


void
show(SWRegistryHive hive, const SWString &key)
		{
		SWRegistryKey	rk;

		if (rk.open(hive, key) == SW_STATUS_SUCCESS)
			{
			SWStringArray	sa;
			SWRegistryValue	v;

			rk.valueNames(sa);

			printf("%d values:\n", (int)sa.size());
			for (int i=0;i<(int)sa.size();i++)
				{
				rk.getValue(sa[i], v);
				printf("  %d: %s = %s\n", i, v.name().c_str(), v.toString().c_str());
				}
			}
		}


int
sw_main(const SWString &prog, const SWStringArray &args)
 		{
		int		iv=0;

		check(SW_REGISTRY_CURRENT_USER, "Software/SwiftTec/test-registry", "Count");
		check(SW_REGISTRY_CURRENT_USER, "SOFTWARE/SwiftTec/test-registry", "Count");
		check(SW_REGISTRY_CURRENT_USER, "SOFTWARE/SwiftTec/test-registry", "LastUpdated");

		iv = sw_registry_getInteger(SW_REGISTRY_CURRENT_USER, "SOFTWARE/SwiftTec/test-registry", "Count");
		sw_registry_setInteger(SW_REGISTRY_CURRENT_USER, "SOFTWARE/SwiftTec/test-registry", "Count", iv+1);
		
		SWLocalTime	now;

		now.update();
		sw_registry_setString(SW_REGISTRY_CURRENT_USER, "SOFTWARE/SwiftTec/test-registry", "LastUpdated", now.toString());

		check(SW_REGISTRY_CURRENT_USER, "Software/SwiftTec/test-registry", "Count");
		check(SW_REGISTRY_CURRENT_USER, "SOFTWARE/SwiftTec/test-registry", "Count");
		check(SW_REGISTRY_CURRENT_USER, "SOFTWARE/SwiftTec/test-registry", "LastUpdated");


		int			nupdates=10000;
		int			nparams=100;
		SWString	param;

		double		c1=sw_clock_getCurrentTimeAsDouble();

		for (int i=0;i<nupdates;i++)
			{
			param.format("iValue%d", i % nparams);
			iv = sw_registry_setInteger(SW_REGISTRY_CURRENT_USER, "SOFTWARE/SwiftTec/test-registry", param, i);
			}

		double		c2=sw_clock_getCurrentTimeAsDouble();

		printf("%d registry updates took = %.3f\n", nupdates, c2-c1);


		show(SW_REGISTRY_CURRENT_USER, "SOFTWARE/SwiftTec/test-registry");

		return 0;
		}

