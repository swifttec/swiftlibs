#include <swift/SWFilename.h>
#include <swift/sw_main.h>

int
sw_main(const SWString &prog, const SWStringArray &args)
 		{
		SWString	path, result;

		path = L"aaa/bbb/ccc/ddd/eee/fff/hhh/../../../XXX";

		result = SWFilename::optimizePath(path, SWFilename::PathTypeUnix);

		printf("Path  : %s\n", path.c_str());
		printf("Result: %s\n", result.c_str());

		return 0;
		}

