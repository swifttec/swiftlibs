// test-VeristoreDataBlock.cpp : Defines the entry point for the console application.
//

#include <stdio.h>

#include <swift/SWString.h>
#include <swift/SWArray.h>

class TestValue
		{
public:
		TestValue();
		~TestValue();
		};



TestValue::TestValue()
		{
		printf("TestValue constructor for %p\n", this);
		}


TestValue::~TestValue()
		{
		printf("TestValue destructor for %p\n", this);
		}


class TestObject
		{
public:
		TestObject();
		TestObject(const TestObject &other);
		~TestObject();

		TestObject &	operator=(const TestObject &other);

public:
		SWString	text;
		TestValue	value;
		};


TestObject::TestObject()
		{
		printf("TestObject constructor for %p\n", this);
		}


TestObject::TestObject(const TestObject &other)
		{
		printf("TestObject copy constructor for %p\n", this);
		*this = other;
		}

TestObject &
TestObject::operator=(const TestObject &other)
		{
		text = other.text;
		value = other.value;

		return *this;
		}

TestObject::~TestObject()
		{
		printf("TestObject destructor for %p\n", this);
		}

void
show(SWArray<TestObject> &list)
		{
		printf("---- BEGIN LIST ----\n");
		for (int i=0;i<(int)list.size();i++)
			{
			printf("list[%d] @ %p = %s\n", i, &list[i], list[i].text.c_str());
			}
		printf("---- END LIST ----\n");
		}


int
main()
		{
		printf("Generate obj\n");
		TestObject	obj;

		obj.text = "This is a test";

		printf("Generate tlist\n");
		SWArray<TestObject>	tlist;

		printf("Add obj to tlist\n");
		tlist.add(obj);

		show(tlist);

		printf("Add via reference\n");
		TestObject	&objref=tlist[(int)tlist.size()];
		objref.text = "This is a reference";

		show(tlist);

		return 0;
		}

