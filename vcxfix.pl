#!perl

sub randhex
		{
		my $res="";

		$n = rand(16);
		if ($n < 10) { $res = chr(48+$n); }
		else { $res = chr(65+$n-10); }

		return $res;
		}


sub uuid
		{
		# {49401A6E-2A60-44BB-A6FA-73AC20190DB7}

		my $res = "{";

		for (my $i=0;$i<8;$i++) { $res .= randhex(); }
		$res .= "-";
		for (my $i=0;$i<4;$i++) { $res .= randhex(); }
		$res .= "-";
		for (my $i=0;$i<4;$i++) { $res .= randhex(); }
		$res .= "-";
		for (my $i=0;$i<4;$i++) { $res .= randhex(); }
		$res .= "-";
		for (my $i=0;$i<12;$i++) { $res .= randhex(); }

		$res .= "}";

		return $res;
		}


sub fix
		{
		my ($filename) = @_;

		if (-f $filename)
			{
			my $oldfilename="$filename.bak";
			my $newfilename=$filename;
			my $overwrite=0;

			print "Fixing $filename\n";

			my $oldfilename="$filename.bak";
			my $newfilename=$filename;

			if (-f "$filename.bak") 
				{
				unlink("$filename.bak");
				}

			rename($filename, "$filename.bak");

			if (-f $newfilename)
				{
				print "$newfilename already exists\n";
				return;
				}

			open(IN, "$oldfilename");
			open(OUT, ">$newfilename");

			while ($line = <IN>)
				{
				chomp($line);
				if ($line =~ /^(.*<ProjectGuid>).*(<\/ProjectGuid>.*)$/)
					{
					$line = $1.uuid().$2;
					}

				print OUT "$line\n";
				}

			close(OUT);
			close(IN);

			unlink($oldfilename);
			}
		else
			{
			print "$file is not a file\n";
			}
		}


foreach $file (glob "*.vcxproj *.csproj *.vbproj")
	{
	fix($file);
	}
