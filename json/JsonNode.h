/**
*** @file		JsonNode.h
*** @brief		An object to represent an XML node
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the JsonNode class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __JsonNode_h__
#define __JsonNode_h__

#include <json/json.h>

#include <swift/SWValue.h>
#include <swift/SWList.h>
#include <swift/SWTextFile.h>

class JSON_DLL_EXPORT JsonNode
		{
friend class JsonFile;

public:
		enum JsonValueType
			{
			Null=0,
			Value,
			Array,
			Object
			};

protected:
		JsonNode(int type);

public:
		virtual ~JsonNode();

		int					type() const	{ return m_type; }

public: // Overrides
		virtual void		clear();
		virtual void		write(SWTextFile &f, int indent)=0;
		virtual SWString	toString() const;

private:
		int			m_type;
		};

#endif
