/**
*** @file		JsonValue.h
*** @brief		An object to represent an XML node
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the JsonValue class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __JsonValue_h__
#define __JsonValue_h__

#include <json/JsonNode.h>

#include <swift/SWValue.h>
#include <swift/SWList.h>
#include <swift/SWTextFile.h>


class JSON_DLL_EXPORT JsonValue : public JsonNode
		{
public:
		JsonValue();
		JsonValue(const SWValue &v);
		JsonValue(const JsonValue &other);
		virtual ~JsonValue();

		JsonValue &	operator=(const SWValue &other);
		JsonValue &	operator=(const JsonValue &other);

		virtual void		set(const SWValue &v);
		virtual SWString	toString() const;

public: // Overrides
		virtual void		clear();
		virtual void		write(SWTextFile &f, int indent);

private:
		SWValue			m_value;
		};

#endif
