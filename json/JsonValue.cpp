/**
*** @file		JsonValue.cpp
*** @brief		An object to represent an XML node
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWJsonValue class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <json/JsonValue.h>
#include <swift/sw_printf.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


JsonValue::JsonValue() :
	JsonNode(Value)
		{
		clear();
		}


JsonValue::~JsonValue()
		{
		clear();
		}


// Copy constructor
JsonValue::JsonValue(const JsonValue &v) :
	JsonNode(Value)
		{
		*this = v;
		}


JsonValue::JsonValue(const SWValue &v) :
	JsonNode(Value)
		{
		*this = v;
		}


// Assignment operator
JsonValue &
JsonValue::operator=(const JsonValue &v)
		{
#define COPY(x)	x = v.x
		COPY(m_value);
#undef COPY

		return *this;
		}


// Assignment operator
JsonValue &
JsonValue::operator=(const SWValue &v)
		{
		m_value = v;

		return *this;
		}


void
JsonValue::clear()
		{
		m_value.clear();
		}


void
JsonValue::set(const SWValue &v)
		{
		m_value = v;
		}


void
JsonValue::write(SWTextFile &f, int /* indent */)
		{
		// This is a standard value
		switch (m_value.type())
			{
			case SWValue::VtNone:
				f.writeString("null");
				break;

			case SWValue::VtBoolean:
				f.writeString(m_value.toBoolean()?"true":"false");
				break;

			case SWValue::VtInt8:
			case SWValue::VtInt16:
			case SWValue::VtInt32:
			case SWValue::VtInt64:
			case SWValue::VtUInt8:
			case SWValue::VtUInt16:
			case SWValue::VtUInt32:
			case SWValue::VtUInt64:
			case SWValue::VtFloat:
			case SWValue::VtDouble:
			case SWValue::VtMoney:
			case SWValue::VtCurrency:
				f.writeString(m_value.toString());
				break;

			case SWValue::VtCChar:
			case SWValue::VtWChar:
			case SWValue::VtCString:
			case SWValue::VtWString:
			case SWValue::VtDate:
			case SWValue::VtTime:
			case SWValue::VtPointer:
			case SWValue::VtData:
				f.writeString(SWString::quote(m_value.toString(), '"', false, '\\'));
				break;
			}
		}


SWString
JsonValue::toString() const
		{
		return m_value.toString();
		}
