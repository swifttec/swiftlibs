/**
*** @file		JsonObject.cpp
*** @brief		An object to represent an JSON node
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the JsonObject class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"

#include <json/JsonObject.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


JsonObject::JsonObject() :
	JsonNode(Object)
		{
		}


JsonObject::JsonObject(const JsonObject &v) :
	JsonNode(Object)
		{
		*this = v;
		}


JsonObject::~JsonObject()
		{
		clear();
		}


// Assignment operator
JsonObject &
JsonObject::operator=(const JsonObject &v)
		{
		clear();

#define COPY(x)	x = v.x
#undef COPY

		/*
		SWIterator<SWString>	it=v.m_values.keys();

		while (it.hasNext())
			{
			SWString	param=*it.next();
			JsonNode	*pNode;

			m_values.get(param, pNode);

			add(param, pNode);
			}
		*/


		return *this;
		}


void
JsonObject::clear()
		{
		SWIterator< SWHashMapEntry<SWString,JsonNode *> >	it=m_values.iterator();

		while (it.hasNext())
			{
			SWHashMapEntry<SWString,JsonNode *>	*pEntry=it.next();

			delete pEntry->value();
			}
			
		m_values.clear();
		}


void
JsonObject::write(SWTextFile &f, int indent)
		{
		SWString	space;

		for (int i=0;i<indent;i++) space += _T("\t");

		f.writeString(_T("\n"));
		f.writeString(space);
		f.writeString(_T("{"));

		SWIterator<SWString>	it=m_values.keys();
		bool					first=true;

		while (it.hasNext())
			{
			if (first)
				{
				first = false;
				}
			else
				{
				f.writeString(_T(","));
				}

			f.writeString(_T("\n"));
			f.writeString(space);

			SWString	param=*it.next();
			JsonNode	*pNode=NULL;
			
			m_values.get(param, pNode);

			f.writeString(SWString::quote(param, '"', false, '\\'));
			f.writeString(_T(": "));

			if (pNode != NULL)
				{
				pNode->write(f, indent+1);
				}
			else
				{
				f.writeString(_T("null"));
				}
			}

		f.writeString(_T("\n"));
		f.writeString(space);
		f.writeString(_T("}"));
		}


void
JsonObject::set(const SWString &param, const SWValue &v)
		{
		set(param, new JsonValue(v));
		}


void
JsonObject::set(const SWString &param, JsonNode *pNode)
		{
		JsonNode	*pOldNode=NULL;
		
		m_values.put(param, pNode, &pOldNode);
		if (pOldNode != NULL)
			{
			delete pOldNode;
			}
		}


void
JsonObject::remove(const SWString &param)
		{
		JsonNode	*pOldNode=NULL;
		
		m_values.remove(param, &pOldNode);
		if (pOldNode != NULL)
			{
			delete pOldNode;
			}
		}


JsonNode *		
JsonObject::get(const SWString &param) const
		{
		JsonNode	*pNode=NULL;

		m_values.get(param, pNode);

		return pNode;
		}


SWStringArray
JsonObject::keys() const
		{
		SWIterator<SWString>	it=m_values.keys();
		SWStringArray			ka;

		while (it.hasNext())
			{
			ka.add(*it.next());
			}

		return ka;
		}

