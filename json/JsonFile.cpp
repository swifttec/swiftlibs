/**
*** @file		JsonFile.cpp
*** @brief		An object to represent an JSON node
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the JsonFile class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"

#include <json/JsonFile.h>
#include <json/JsonValue.h>
#include <json/JsonObject.h>
#include <json/JsonArray.h>

#include <swift/SWArray.h>
#include <swift/SWTextFile.h>
#include <swift/sw_printf.h>



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
JsonFile::~JsonFile()
		{
		clear();
		}



JsonFile::JsonFile(const SWString &filename) :
	m_pRootNode(NULL)
		{
		if (!filename.isEmpty())
			{
			load(filename);
			}
		}



void
JsonFile::clear()
		{
		delete m_pRootNode;
		m_pRootNode = NULL;
		}

#define	SEPCHARS	" \t\r\n"


static bool skipWhiteSpace(const sw_tchar_t *&sp, sw_tchar_t expected)
		{
		bool	res=false;

		while (*sp && (*sp == ' ' || *sp == '\r' || *sp == '\t' || *sp == '\n'))
			{
			sp++;
			}
		
		if (expected)
			{
			if (*sp == expected)
				{
				sp++;
				res = true;
				}
			}
		else
			{
			res = true;
			}

		return res;
		}


static bool readQuotedString(const sw_tchar_t *&sp, SWString &s, bool checkInitialQuote)
		{
		bool	res=true;

		if (checkInitialQuote)
			{
			if (*sp != '"')
				{
				res = false;
				}
			else
				{
				sp++;
				}
			}

		if (res)
			{
			while (*sp && *sp != '"')
				{
				s += *sp++;
				}

			if (*sp != '"') res = false;
			else sp++;
			}

		return res;
		}


static bool readUnquotedString(const sw_tchar_t *&sp, SWString &s)
		{
		bool	res=true;

		while (*sp && *sp != ' ' && *sp != '\r' && *sp != '\t' && *sp != '\n' && *sp != ',' && *sp != '}')
			{
			s += *sp++;
			}

		return res;
		}


bool
JsonFile::processLine(const SWString &input)
		{
		const sw_tchar_t	*sp;
		bool				res=false;

		sp = input.t_str();

		// Skip white space
		skipWhiteSpace(sp, 0);

		if (*sp == '[')
			{
			// Array
			JsonArray	*pArray=new JsonArray();

			m_pRootNode = pArray;
			res = processArray(sp, pArray);
			}
		else if (*sp == '{')
			{
			JsonObject	*pObject=new JsonObject();

			m_pRootNode = pObject;
			res = processObject(sp, pObject);
			}

		return res;
		}


bool
JsonFile::processValue(const sw_tchar_t *&sp, JsonNode *&pNode)
		{
		bool			res=true;

		// Skip white space
		skipWhiteSpace(sp, 0);

		// Now the start of value, array or object
		if (*sp == '[')
			{
			JsonArray	*pArray=new JsonArray();

			res = processArray(sp, pArray);
			if (!res)
				{
				delete pArray;
				}
			else
				{
				pNode = pArray;
				}
			}
		else if (*sp == '{')
			{
			JsonObject	*pObject=new JsonObject();

			res = processObject(sp, pObject);
			if (!res)
				{
				delete pObject;
				}
			else
				{
				pNode = pObject;
				}
			}
		else if (*sp == '"')
			{
			// String
			SWString	value;

			res = readQuotedString(sp, value, true);

			if (res)
				{
				JsonValue	*pValue;

				pNode = pValue = new JsonValue();
				pValue->set(value);
				}
			}
		else
			{
			// Other value
			SWString	value;

			res = readUnquotedString(sp, value);
			if (res)
				{
				JsonValue	*pValue;

				pNode = pValue = new JsonValue();

				if (value.compareNoCase("true") == 0) pValue->set(true);
				else if (value.compareNoCase("false") == 0) pValue->set(false);
				else if (value.compareNoCase("null") == 0) pValue->set(SWValue());
				else pValue->set(value);
				}
			}

		return res;
		}


bool
JsonFile::processObject(const sw_tchar_t *&sp, JsonObject *pObject)
		{
		bool			res=true;

		// Skip white space
		res = skipWhiteSpace(sp, '{');
		if (res)
			{
			while (res && *sp)
				{
				// Skip white space
				res = skipWhiteSpace(sp, '"');
				if (!res) break;

				SWString	name;

				res = readQuotedString(sp, name, false);
				if (!res) break;

				// Skip white space
				res = skipWhiteSpace(sp, ':');
				if (!res) break;

				// Now process the value
				JsonNode	*pNode=NULL;

				res = processValue(sp, pNode);
				if (!res) break;

				// Add the value
				pObject->set(name, pNode);

				// Skip white space
				skipWhiteSpace(sp, 0);

				if (*sp == '}')
					{
					// End of object
					sp++;
					break;
					}
				else if (*sp != ',')
					{
					res = false;
					break;
					}

				// Another name/value pair to come
				sp++;
				}
			}

		return res;
		}


bool
JsonFile::processArray(const sw_tchar_t *&sp, JsonArray *pArray)
		{
		bool			res=true;

		// Skip white space
		res = skipWhiteSpace(sp, '[');
		if (res)
			{
			if (*sp == ']')
				{
				// End of empty array
				sp++;
				res = true;
				}
			else
				{
				while (res && *sp)
					{
					// Now process the value
					JsonNode	*pNode=NULL;

					res = processValue(sp, pNode);
					if (!res) break;

					// Add the value
					pArray->add(pNode);

					// Skip white space
					skipWhiteSpace(sp, 0);

					if (*sp == ']')
						{
						// End of object
						sp++;
						break;
						}
					else if (*sp != ',')
						{
						res = false;
						break;
						}

					// Another value pair to come
					sp++;
					}
				}
			}

		return res;
		}


bool
JsonFile::loadFromString(const SWString &contents)
		{
		bool	res=true;

		clear();
		res = processLine(contents);

		return res;
		}


bool
JsonFile::load(const SWString &filename)
		{
		clear();

		bool			res=true;
		SWTextFile		f;

		if ((f.open(filename, TF_MODE_READ)) == SW_STATUS_SUCCESS)
			{
			SWString	line, str;

			while (str.readLine(f))
				{
				line += str;
				}

			res = processLine(line);
			f.close();
			}
		else
			{
			res = false;
			}

		return res;
		}



bool
JsonFile::save(const SWString &filename)
		{
		bool	res=true;
		SWTextFile	f;

		if (f.open(filename, TF_MODE_WRITE|TF_ENCODING_UTF8) == SW_STATUS_SUCCESS)
			{
			if (m_pRootNode == NULL)
				{
				f.writeString(_T("{\n}\n"));
				}
			else
				{
				m_pRootNode->write(f, 0);
				f.writeString(_T("\n"));
				}
			f.close();
			}
		else
			{
			res = false;
			}

		return res;
		}

