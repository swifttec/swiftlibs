/**
*** @file		JsonArray.h
*** @brief		An object to represent a single JSON array
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the JsonArray class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __JsonArray_h__
#define __JsonArray_h__

#include <json/JsonNode.h>

#include <swift/SWArray.h>
#include <swift/SWString.h>

class JSON_DLL_EXPORT JsonArray : public JsonNode
		{
public:
		JsonArray();
		JsonArray(const JsonArray &other);
		virtual ~JsonArray();

		JsonArray &	operator=(const JsonArray &other);

		void			add(const SWValue &v);
		void			add(JsonNode *pNode);
		void			set(int idx, const SWValue &v);
		void			set(int idx, JsonNode *pNode);
		void			remove(int idx);
		JsonNode *		get(int idx) const;

		sw_size_t		size()		{ return m_values.size(); }

public: // Overrides
		virtual void		clear();
		virtual void		write(SWTextFile &f, int indent);

private:
		SWArray<JsonNode *>	m_values;
		};

#endif
