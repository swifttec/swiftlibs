/**
*** @file		JsonFile.h
*** @brief		An object to represent an JSON file
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the JsonFile class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#ifndef __JsonFile_h__
#define __JsonFile_h__

#include <json/JsonObject.h>
#include <json/JsonArray.h>

#include <swift/SWList.h>
#include <swift/SWArray.h>


class JSON_DLL_EXPORT JsonFile
		{
public:
		JsonFile(const SWString &filename="");
		virtual ~JsonFile();


		bool				loadFromString(const SWString &contents);
		bool				load(const SWString &filename);
		bool				save(const SWString &filename);

		JsonNode *			getRootNode() const		{ return m_pRootNode; }

public: // Overrides
		virtual void		clear();

protected:
		bool				processLine(const SWString &line);
		bool				processValue(const sw_tchar_t *&sp, JsonNode *&pNode);
		bool				processObject(const sw_tchar_t *&sp, JsonObject *pObject);
		bool				processArray(const sw_tchar_t *&sp, JsonArray *pArray);

protected:
		JsonNode	*m_pRootNode;
		};

#endif
