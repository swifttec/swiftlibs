/**
*** @file		JsonArray.cpp
*** @brief		An object to represent a JSON array
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the JsonArray class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"

#include <json/JsonArray.h>
#include <json/JsonValue.h>


JsonArray::JsonArray() :
	JsonNode(Array)
		{
		}


// Copy constructor
JsonArray::JsonArray(const JsonArray &v) :
	JsonNode(Array)
		{
		*this = v;
		}


JsonArray::~JsonArray()
		{
		clear();
		}


// Assignment operator
JsonArray &
JsonArray::operator=(const JsonArray &v)
		{
		clear();

#define COPY(x)	x = v.x
#undef COPY

		return *this;
		}


void
JsonArray::clear()
		{
		for (int i=0;i<(int)m_values.size();i++)
			{
			delete m_values[i];
			m_values[i] = NULL;
			}

		m_values.clear();
		}


void
JsonArray::write(SWTextFile &f, int indent)
		{
		SWString	space;

		for (int i=0;i<indent;i++) space += _T("\t");

		f.writeString(_T("\n"));
		f.writeString(space);
		f.writeString(_T("["));

		for (int i=0;i<(int)m_values.size();i++)
			{
			if (i) f.writeString(_T(","));
			JsonNode	*pNode=m_values[i];
			
			f.writeString(_T("\n"));
			f.writeString(space);
			if (pNode != NULL)
				{
				pNode->write(f, indent+1);
				}
			else
				{
				f.writeString(_T("null"));
				}
			}

		f.writeString(_T("\n"));
		f.writeString(space);
		f.writeString(_T("]"));
		}


void
JsonArray::add(const SWValue &v)
		{
		add(new JsonValue(v));
		}


void
JsonArray::add(JsonNode *pNode)
		{
		m_values.add(pNode);
		}


void			
JsonArray::set(int idx, const SWValue &v)
		{
		if (idx >= 0)
			{
			set(idx, new JsonValue(v));
			}
		}


void			
JsonArray::set(int idx, JsonNode *pNode)
		{
		if (idx >= 0)
			{
			int		oldsize=(int)m_values.size();

			if (idx < oldsize)
				{
				JsonNode	*pOldNode=m_values[idx];
				
				delete pOldNode;
				}

			m_values[idx] = pNode;

			// Put NULL in all intervening indexes
			for (int i=oldsize;i<(idx-1);i++)
				{
				m_values[i] = NULL;
				}
			}
		}


void			
JsonArray::remove(int idx)
		{
		if (idx >= 0 && idx <(int)m_values.size())
			{
			JsonNode	*pOldNode=m_values[idx];
				
			delete pOldNode;

			m_values.remove(idx);
			}
		}


JsonNode *		
JsonArray::get(int idx) const
		{
		JsonNode	*pNode=NULL;

		if (idx >= 0 && idx <(int)m_values.size())
			{
			pNode = m_values.get(idx);
			}

		return pNode;
		}

