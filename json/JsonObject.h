/**
*** @file		JsonObject.h
*** @brief		An abstract object to represent an JSON object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the JsonObject class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __JsonObject_h__
#define __JsonObject_h__

#include <json/json.h>
#include <json/JsonNode.h>
#include <json/JsonValue.h>

#include <swift/swift.h>
#include <swift/SWValue.h>
#include <swift/SWTextFile.h>
#include <swift/SWHashMap.h>

class JSON_DLL_EXPORT JsonObject : public JsonNode
		{
friend class JsonFile;

public:
		JsonObject();
		JsonObject(const JsonObject &other);
		virtual ~JsonObject();

		JsonObject &	operator=(const JsonObject &v);

		void			set(const SWString &name, const SWValue &v);
		void			set(const SWString &name, JsonNode *pNode);
		void			remove(const SWString &name);
		JsonNode *		get(const SWString &name) const;
		SWStringArray	keys() const;


public: // Overrides
		virtual void			clear();
		virtual void			write(SWTextFile &f, int indent);

protected:
		SWHashMap<SWString,JsonNode *>	m_values;
		};


#endif
