/**
*** @file		Base64.h
*** @brief		A class for handling base 64 encoding and decoding
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the Base64 class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __crypt_Base64_h__
#define __crypt_Base64_h__

#include <swift/SWString.h>
#include <swift/SWDataBuffer.h>
#include <crypt/crypt.h>


class CRYPT_DLL_EXPORT Base64
		{
public:
		// Encode a block of data as a base64 string
		static bool		encode(const void *pData, sw_size_t nbytes, SWString &result);

		// Decode a base64 string and return as data
		static bool		decode(const SWString &s, SWDataBuffer &result);
		};


#endif
