/**
*** @file		Hash.cpp
*** @brief		A class for handling a Secure Hash Algorithms
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the Hash class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <crypt/Hash.h>

#include "sha2.h"
#include "hmac_sha2.h"


void
Hash::bufferToString(SWDataBuffer &data, SWString &result)
		{
		sw_size_t		len=data.size();
		unsigned char	*dp=data;

		result.clear();
		char *output = (char *)(result.lockBuffer((2*data.size()) + 1, sizeof(char)));
		for (sw_size_t i=0;i<len;i++)
			{
#if defined(SW_PLATFORM_WINDOWS)
			sprintf_s(output, len, "%02x", *dp);
#else
			sprintf(output, "%02x", *dp);
#endif

			dp++;
			output += 2;
			}
		result.unlockBuffer();
		}


void
Hash::sha224(const void *pData, sw_size_t nbytes, SWString &result)
		{
		SWDataBuffer	data;

		Hash::sha224(pData, nbytes, data);
		bufferToString(data, result);
		}


void
Hash::sha256(const void *pData, sw_size_t nbytes, SWString &result)
		{
		SWDataBuffer	data;

		Hash::sha256(pData, nbytes, data);
		bufferToString(data, result);
		}


void
Hash::sha384(const void *pData, sw_size_t nbytes, SWString &result)
		{
		SWDataBuffer	data;

		Hash::sha384(pData, nbytes, data);
		bufferToString(data, result);
		}


void
Hash::sha512(const void *pData, sw_size_t nbytes, SWString &result)
		{
		SWDataBuffer	data;

		Hash::sha512(pData, nbytes, data);
		bufferToString(data, result);
		}



void
Hash::sha224(const void *pData, sw_size_t nbytes, SWDataBuffer &result)
		{
		result.clear();
		result.capacity(SHA224_DIGEST_SIZE);

		::sha224((const unsigned char *)pData, (unsigned int)nbytes, result);

		result.size(SHA224_DIGEST_SIZE);
		}


void
Hash::sha256(const void *pData, sw_size_t nbytes, SWDataBuffer &result)
		{
		result.clear();
		result.capacity(SHA256_DIGEST_SIZE);

		::sha256((const unsigned char *)pData, (unsigned int)nbytes, result);

		result.size(SHA256_DIGEST_SIZE);
		}


void
Hash::sha384(const void *pData, sw_size_t nbytes, SWDataBuffer &result)
		{
		result.clear();
		result.capacity(SHA384_DIGEST_SIZE);

		::sha384((const unsigned char *)pData, (unsigned int)nbytes, result);

		result.size(SHA384_DIGEST_SIZE);
		}


void
Hash::sha512(const void *pData, sw_size_t nbytes, SWDataBuffer &result)
		{
		result.clear();
		result.capacity(SHA512_DIGEST_SIZE);

		::sha512((const unsigned char *)pData, (unsigned int)nbytes, result);

		result.size(SHA512_DIGEST_SIZE);
		}









void
Hash::hmac_sha224(const void *pKey, sw_size_t keysize, const void *pData, sw_size_t datasize, SWString &result)
		{
		SWDataBuffer	data;

		Hash::hmac_sha224(pKey, keysize, pData, datasize, data);
		bufferToString(data, result);
		}


void
Hash::hmac_sha256(const void *pKey, sw_size_t keysize, const void *pData, sw_size_t datasize, SWString &result)
		{
		SWDataBuffer	data;

		Hash::hmac_sha256(pKey, keysize, pData, datasize, data);
		bufferToString(data, result);
		}


void
Hash::hmac_sha384(const void *pKey, sw_size_t keysize, const void *pData, sw_size_t datasize, SWString &result)
		{
		SWDataBuffer	data;

		Hash::hmac_sha384(pKey, keysize, pData, datasize, data);
		bufferToString(data, result);
		}


void
Hash::hmac_sha512(const void *pKey, sw_size_t keysize, const void *pData, sw_size_t datasize, SWString &result)
		{
		SWDataBuffer	data;

		Hash::hmac_sha512(pKey, keysize, pData, datasize, data);
		bufferToString(data, result);
		}



void
Hash::hmac_sha224(const void *pKey, sw_size_t keysize, const void *pData, sw_size_t datasize, SWDataBuffer &result)
		{
		result.clear();
		result.capacity(SHA224_DIGEST_SIZE);

		::hmac_sha224((const unsigned char *)pKey, keysize, (const unsigned char *)pData, datasize, result, SHA224_DIGEST_SIZE);

		result.size(SHA224_DIGEST_SIZE);
		}


void
Hash::hmac_sha256(const void *pKey, sw_size_t keysize, const void *pData, sw_size_t datasize, SWDataBuffer &result)
		{
		result.clear();
		result.capacity(SHA256_DIGEST_SIZE);

		::hmac_sha256((const unsigned char *)pKey, keysize, (const unsigned char *)pData, datasize, result, SHA256_DIGEST_SIZE);

		result.size(SHA256_DIGEST_SIZE);
		}


void
Hash::hmac_sha384(const void *pKey, sw_size_t keysize, const void *pData, sw_size_t datasize, SWDataBuffer &result)
		{
		result.clear();
		result.capacity(SHA384_DIGEST_SIZE);

		::hmac_sha384((const unsigned char *)pKey, keysize, (const unsigned char *)pData, datasize, result, SHA384_DIGEST_SIZE);

		result.size(SHA384_DIGEST_SIZE);
		}


void
Hash::hmac_sha512(const void *pKey, sw_size_t keysize, const void *pData, sw_size_t datasize, SWDataBuffer &result)
		{
		result.clear();
		result.capacity(SHA512_DIGEST_SIZE);

		::hmac_sha512((const unsigned char *)pKey, keysize, (const unsigned char *)pData, datasize, result, SHA512_DIGEST_SIZE);

		result.size(SHA512_DIGEST_SIZE);
		}
