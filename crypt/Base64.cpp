/**
*** @file		Base64.cpp
*** @brief		A class for handling a web connection
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the Base64 class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <crypt/Base64.h>

#include "base64_encode.h"
#include "base64_decode.h"

bool		
Base64::encode(const void *pData, sw_size_t nbytes, SWString &result)
		{
		bool		res=false;
		sw_size_t	buflen=0;
		int			len;
		char		*output, *op;

		// This is the maximum we will need
		buflen = 32 + (2 * nbytes);
		op = output = new char[buflen];

		if (output != NULL)
			{
			// Encode the data
			base64_encodestate	es;

			base64_init_encodestate(&es, 0);

			len = base64_encode_block((const char *)pData, (int)nbytes, op, &es);
			op += len;

			len = base64_encode_blockend(op, &es);
			op += len;
			*op = 0;

			result = output;
			res = true;
			
			delete [] output;
			}

		return res;
		}


bool
Base64::decode(const SWString &s, SWDataBuffer &result)
		{
		bool		res=true;
		const char	*code=s.c_str();
		sw_size_t	codelength=strlen(code);

		// Encode the steing
		base64_decodestate	es;

		base64_init_decodestate(&es);
		result.capacity(codelength);
		result.size(base64_decode_block(code, (int)codelength, (char *)result.data(), &es));

		return res;
		}

