/**
*** @file		Hash.h
*** @brief		A class for handling Secure Hash Algorithms
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the Hash class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __crypt_Hash_h__
#define __crypt_Hash_h__

#include <swift/SWString.h>
#include <swift/SWDataBuffer.h>
#include <crypt/crypt.h>


class CRYPT_DLL_EXPORT Hash
		{
public:
		static void		sha224(const void *pData, sw_size_t nbytes, SWDataBuffer &result);
		static void		sha224(const void *pData, sw_size_t nbytes, SWString &result);

		static void		sha256(const void *pData, sw_size_t nbytes, SWDataBuffer &result);
		static void		sha256(const void *pData, sw_size_t nbytes, SWString &result);

		static void		sha384(const void *pData, sw_size_t nbytes, SWDataBuffer &result);
		static void		sha384(const void *pData, sw_size_t nbytes, SWString &result);

		static void		sha512(const void *pData, sw_size_t nbytes, SWDataBuffer &result);
		static void		sha512(const void *pData, sw_size_t nbytes, SWString &result);

		static void		hmac_sha224(const void *pKey, sw_size_t keysize, const void *pData, sw_size_t datasize, SWDataBuffer &result);
		static void		hmac_sha224(const void *pKey, sw_size_t keysize, const void *pData, sw_size_t datasize, SWString &result);

		static void		hmac_sha256(const void *pKey, sw_size_t keysize, const void *pData, sw_size_t datasize, SWDataBuffer &result);
		static void		hmac_sha256(const void *pKey, sw_size_t keysize, const void *pData, sw_size_t datasize, SWString &result);

		static void		hmac_sha384(const void *pKey, sw_size_t keysize, const void *pData, sw_size_t datasize, SWDataBuffer &result);
		static void		hmac_sha384(const void *pKey, sw_size_t keysize, const void *pData, sw_size_t datasize, SWString &result);

		static void		hmac_sha512(const void *pKey, sw_size_t keysize, const void *pData, sw_size_t datasize, SWDataBuffer &result);
		static void		hmac_sha512(const void *pKey, sw_size_t keysize, const void *pData, sw_size_t datasize, SWString &result);

		static void		bufferToString(SWDataBuffer &data, SWString &result);
		};


#endif
