/**
*** @file		GeodeticMeasurement.cpp
*** @brief		An object to an angle in degrees
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the JsonArray class.
**/

/*********************************************************************************
Converted to C++ from Gavaghan.Geodesy C# code by Mike Gavaghan

http://www.gavaghan.org/blog/free-source-code/geodesy-library-vincentys-formula/

This code may be freely used and modified on any personal or professional
project.  It comes with no warranty.
*********************************************************************************/

#include "stdafx.h"

#include <geodesy/GeodeticMeasurement.h>
#include <swift/sw_misc.h>


GeodeticMeasurement::GeodeticMeasurement()
		{
		clear();
		}


GeodeticMeasurement::GeodeticMeasurement(const GeodeticCurve &averageCurve, double elevationChange)
		{
		double ellDist = averageCurve.ellipsoidalDistance();

		m_curve = averageCurve;
		m_elevationChange = elevationChange;
		m_p2p = sw_math_sqrt((ellDist * ellDist) + (m_elevationChange * m_elevationChange));
		}


GeodeticMeasurement::GeodeticMeasurement(const GeodeticMeasurement &v)
		{
		*this = v;
		}


GeodeticMeasurement &
GeodeticMeasurement::operator=(const GeodeticMeasurement &v)
		{
		clear();

#define COPY(x)	x = v.x
		COPY(m_curve);
		COPY(m_elevationChange);
		COPY(m_p2p);
#undef COPY

		return *this;
		}


// Assignment operator
bool
GeodeticMeasurement::operator==(const GeodeticMeasurement &v)
		{
		return (compareTo(v) == 0);
		}


int
GeodeticMeasurement::compareTo(const GeodeticMeasurement &other)
		{
		int		res=0;

		if (res == 0) res = m_curve.compareTo(other.m_curve);
		if (res == 0) res = sw_misc_compare(m_elevationChange, other.m_elevationChange);
		if (res == 0) res = sw_misc_compare(m_p2p, other.m_p2p);

		return res;
		}


void
GeodeticMeasurement::clear()
		{
		sw_misc_clear(m_curve);
		sw_misc_clear(m_elevationChange);
		sw_misc_clear(m_p2p);
		}
