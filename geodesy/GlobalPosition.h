/**
*** @file		GlobalPosition.h
*** @brief		An object to an angle in degrees
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the JsonArray class.
**/

/*********************************************************************************
Converted to C++ from Gavaghan.Geodesy C# code by Mike Gavaghan

http://www.gavaghan.org/blog/free-source-code/geodesy-library-vincentys-formula/

This code may be freely used and modified on any personal or professional
project.  It comes with no warranty.
*********************************************************************************/



#ifndef __geodesy_GlobalPosition_h__
#define __geodesy_GlobalPosition_h__

#include <geodesy/geodesy_defs.h>
#include <geodesy/GlobalCoordinates.h>

#include <swift/SWArray.h>
#include <swift/SWString.h>

#include <xplatform/sw_math.h>

/**
*** Encapsulates a three dimensional location on a globe (GlobalCoordinates combined with
*** an elevation in meters above a reference ellipsoid).
**/
class GEODESY_DLL_EXPORT GlobalPosition : public SWObject
		{
public:
		/// Default constructor
		GlobalPosition();
		
		/// Construct a new GlobalPosition from a degree measurement.
		GlobalPosition(const GlobalCoordinates &coord, double elevation);

		/// Copy constructor
		GlobalPosition(const GlobalPosition &other);

		/// Assignment operator
		GlobalPosition &			operator=(const GlobalPosition &other);

		/// Equality operator
		bool						operator==(const GlobalPosition &other);

		const GlobalCoordinates &	coordinates() const							{ return m_coordinates; }
		void						coordinates(const GlobalCoordinates & v)	{ m_coordinates = v; }

		double						elevation() const							{ return m_elevation; }
		void						elevation(double v)							{ m_elevation = v; }

		double						latitude() const							{ return m_coordinates.latitude(); }
		void						latitude(double v)							{ m_coordinates.latitude(v); }

		double						longitude() const							{ return m_coordinates.longitude(); }
		void						longitude(double v)							{ m_coordinates.longitude(v); }

		int							compareTo(const GlobalPosition &other);


public: // Overrides
		virtual void		clear();

private:
		GlobalCoordinates	m_coordinates;
		double				m_elevation;
		};

#endif // __geodesy_GlobalPosition_h__
