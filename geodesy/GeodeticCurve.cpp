/**
*** @file		GeodeticCurve.cpp
*** @brief		An object to an angle in degrees
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the JsonArray class.
**/

/*********************************************************************************
Converted to C++ from Gavaghan.Geodesy C# code by Mike Gavaghan

http://www.gavaghan.org/blog/free-source-code/geodesy-library-vincentys-formula/

This code may be freely used and modified on any personal or professional
project.  It comes with no warranty.
*********************************************************************************/

#include "stdafx.h"

#include <geodesy/GeodeticCurve.h>


GeodeticCurve::GeodeticCurve() :
	m_ellipsoidalDistance(0.0),
	m_azimuth(0.0),
	m_reverseAzimuth(0.0)
		{
		}


GeodeticCurve::GeodeticCurve(double ellipsoidalDistance, double azimuth, double reverseAzimuth) :
	m_ellipsoidalDistance(ellipsoidalDistance),
	m_azimuth(azimuth),
	m_reverseAzimuth(reverseAzimuth)
		{
		}


GeodeticCurve::GeodeticCurve(const GeodeticCurve &v)
		{
		*this = v;
		}


GeodeticCurve &
GeodeticCurve::operator=(const GeodeticCurve &v)
		{
		clear();

#define COPY(x)	x = v.x
		COPY(m_ellipsoidalDistance);
		COPY(m_azimuth);
		COPY(m_reverseAzimuth);
#undef COPY

		return *this;
		}


// Assignment operator
bool
GeodeticCurve::operator==(const GeodeticCurve &v)
		{
		return (compareTo(v) == 0);
		}


int
GeodeticCurve::compareTo(const GeodeticCurve &other)
		{
		int		res=0;

		if (m_ellipsoidalDistance < other.m_ellipsoidalDistance) res = -1;
		else if (m_ellipsoidalDistance > other.m_ellipsoidalDistance) res = 1;

		return res;
		}

void
GeodeticCurve::clear()
		{
		m_ellipsoidalDistance = m_azimuth = m_reverseAzimuth = 0.0;
		}
