/**
*** @file		geodesy.h
*** @brief		General header for the GEODESY library.
*** @version	1.0
*** @author		Simon Sparkes
***
*** This file is the general header used by all of the components of the
*** GEODESY library.
**/

/*********************************************************************************
Copyright (c) 2006-2017 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __geodesy_defs_h__
#define __geodesy_defs_h__

#include <swift/swift.h>

#if defined(SW_PLATFORM_WINDOWS)

	// Determine the import/export type for a DLL build
	#if defined(GEODESY_BUILD_DLL)
		#define GEODESY_DLL_EXPORT __declspec(dllexport)
	#else
		#define GEODESY_DLL_EXPORT __declspec(dllimport)
	#endif // defined(GEODESY_BUILD_DLL)

#else // defined(SW_PLATFORM_WINDOWS)

	// On non-windows platforms we just define this to be nothing
	#define GEODESY_DLL_EXPORT

#endif // defined(SW_PLATFORM_WINDOWS)

#endif // __geodesy_defs_h__
