/**
*** @file		Ellipsoid.h
*** @brief		An object to an angle in degrees
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the JsonArray class.
**/

/*********************************************************************************
Converted to C++ from Gavaghan.Geodesy C# code by Mike Gavaghan

http://www.gavaghan.org/blog/free-source-code/geodesy-library-vincentys-formula/

This code may be freely used and modified on any personal or professional
project.  It comes with no warranty.
*********************************************************************************/



#ifndef __geodesy_Ellipsoid_h__
#define __geodesy_Ellipsoid_h__

#include <geodesy/geodesy_defs.h>

#include <swift/SWArray.h>
#include <swift/SWString.h>

#include <xplatform/sw_math.h>

class GEODESY_DLL_EXPORT Ellipsoid
		{
public:
		enum EllipsoidType
			{
			WGS84=0,
			GRS80,
			GRS67,
			ANS,
			WGS72,
			Clarke1858,
			Clarke1880,
			Sphere
			};

private:
		void	init(double semiMajor, double semiMinor, double flattening, double inverseFlattening);
		void	initFromAAndF(double semiMajor, double flattening);
		void	initFromAAndInverseF(double semiMajor, double inverseFlattening);

		Ellipsoid();

public:
		/// Constructor
		Ellipsoid(EllipsoidType type);
				
		double semiMajorAxis() const
			{
			return m_semiMajorAxis;
			}

		/// <summary>Get semi minor axis (meters).</summary>
		double semiMinorAxis() const
			{
			return m_semiMinorAxis;
			}

		/// <summary>Get flattening.</summary>
		double flattening() const
			{
			return m_flattening;
			}

		/// <summary>Get inverse flattening.</summary>
		double inverseFlattening() const
			{
			return m_inverseFlattening;
			}

public:
		static Ellipsoid fromAAndF(double semiMajor, double flattening);
		static Ellipsoid fromAAndInverseF(double semiMajor, double inverseFlattening);

private:
		double m_semiMajorAxis;		///< Semi major axis (meters)
		double m_semiMinorAxis;		///< Semi minor axis (meters)
		double m_flattening;		///< Flattening
		double m_inverseFlattening;	///< Inverse flattening
		};

#endif // __geodesy_Ellipsoid_h__
