/**
*** @file		Angle.h
*** @brief		An object to an angle in degrees
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the JsonArray class.
**/

/*********************************************************************************
Converted to C++ from Gavaghan.Geodesy C# code by Mike Gavaghan

http://www.gavaghan.org/blog/free-source-code/geodesy-library-vincentys-formula/

This code may be freely used and modified on any personal or professional
project.  It comes with no warranty.
*********************************************************************************/



#ifndef __geodesy_Angle_h__
#define __geodesy_Angle_h__

#include <geodesy/geodesy_defs.h>

#include <swift/SWArray.h>
#include <swift/SWString.h>

#include <xplatform/sw_math.h>

class GEODESY_DLL_EXPORT Angle : public SWObject
		{
public:
		/// Default constructor
		Angle();
		
		/// Construct a new Angle from a degree measurement.
		Angle(double degrees);
		
		/// Construct a new Angle from degrees and minutes.
		Angle(int degrees, double minutes);

		/// Construct a new Angle from degrees and minutes.
		Angle(int degrees, int minutes, double seconds);

		/// Copy constructor
		Angle(const Angle &other);

		/// Assignment operator
		Angle &	operator=(const Angle &other);

		/// Equality operator
		bool	operator==(const Angle &other);

		double	degrees() const		{ return m_degrees; }
		void	degrees(double v)	{ m_degrees = v; }
		double	radians() const		{ return m_degrees * PI / 180.0; }
		void	radians(double v)	{ m_degrees = v * 180.0 / PI; }

		operator double() const		{ return m_degrees; }
		operator double &()			{ return m_degrees; }

		Angle &	operator=(double v)	{ m_degrees = v; return *this; }

public: // Overrides
		virtual void		clear();

private:
		double	m_degrees;
		};

#endif // __geodesy_Angle_h__
