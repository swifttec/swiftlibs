/**
*** @file		GlobalCoordinates.h
*** @brief		An object to an angle in degrees
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the JsonArray class.
**/

/*********************************************************************************
Converted to C++ from Gavaghan.Geodesy C# code by Mike Gavaghan

http://www.gavaghan.org/blog/free-source-code/geodesy-library-vincentys-formula/

This code may be freely used and modified on any personal or professional
project.  It comes with no warranty.
*********************************************************************************/



#ifndef __geodesy_GlobalCoordinates_h__
#define __geodesy_GlobalCoordinates_h__

#include <geodesy/geodesy_defs.h>
#include <geodesy/Angle.h>

#include <swift/SWArray.h>
#include <swift/SWString.h>

#include <xplatform/sw_math.h>

class GEODESY_DLL_EXPORT GlobalCoordinates : public SWObject
		{
public:
		/// Default constructor
		GlobalCoordinates();
		
		/// Construct a new GlobalCoordinates from a degree measurement.
		GlobalCoordinates(const Angle &latitude, const Angle &longitude);

		/// Copy constructor
		GlobalCoordinates(const GlobalCoordinates &other);

		/// Assignment operator
		GlobalCoordinates &	operator=(const GlobalCoordinates &other);

		/// Equality operator
		bool	operator==(const GlobalCoordinates &other);

		void			set(const Angle &latitude, const Angle &longitude);

		const Angle &	latitude() const			{ return m_latitude; }
		void			latitude(const Angle &v)	{ m_latitude = v; canonicalize(); }

		const Angle &	longitude() const			{ return m_longitude; }
		void			longitude(const Angle &v)	{ m_longitude = v; canonicalize(); }

		int				compareTo(const GlobalCoordinates &other);


public: // Overrides
		virtual void		clear();

private:
		void	canonicalize();

private:
		Angle	m_latitude;
		Angle	m_longitude;
		};

#endif // __geodesy_GlobalCoordinates_h__
