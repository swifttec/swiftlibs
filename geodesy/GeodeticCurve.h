/**
*** @file		GeodeticCurve.h
*** @brief		An object to an angle in degrees
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the JsonArray class.
**/

/*********************************************************************************
Converted to C++ from Gavaghan.Geodesy C# code by Mike Gavaghan

http://www.gavaghan.org/blog/free-source-code/geodesy-library-vincentys-formula/

This code may be freely used and modified on any personal or professional
project.  It comes with no warranty.
*********************************************************************************/



#ifndef __geodesy_GeodeticCurve_h__
#define __geodesy_GeodeticCurve_h__

#include <geodesy/geodesy_defs.h>
#include <geodesy/Angle.h>

#include <swift/SWArray.h>
#include <swift/SWString.h>

#include <xplatform/sw_math.h>


/// This is the outcome of a geodetic calculation.  It represents the path and
/// ellipsoidal distance between two GlobalCoordinates for a specified reference
/// ellipsoid.
class GEODESY_DLL_EXPORT GeodeticCurve : public SWObject
		{
public:
		/// Default constructor
		GeodeticCurve();
		
		/// Construct a new GeodeticCurve from a degree measurement.
		GeodeticCurve(double ellipsoidalDistance, double azimuth, double reverseAzimuth);

		/// Copy constructor
		GeodeticCurve(const GeodeticCurve &other);

		/// Assignment operator
		GeodeticCurve &	operator=(const GeodeticCurve &other);

		/// Equality operator
		bool	operator==(const GeodeticCurve &other);

		double			ellipsoidalDistance() const				{ return m_ellipsoidalDistance; }
		void			ellipsoidalDistance(double v)			{ m_ellipsoidalDistance = v; }

		const Angle &	azimuth() const					{ return m_azimuth; }
		void			azimuth(const Angle &v)			{ m_azimuth = v; }

		const Angle &	reverseAzimuth() const			{ return m_reverseAzimuth; }
		void			reverseAzimuth(const Angle &v)	{ m_reverseAzimuth = v; }

		int				compareTo(const GeodeticCurve &other);


public: // Overrides
		virtual void		clear();

private:
		double	m_ellipsoidalDistance;	///<Ellipsoidal distance (in meters).
		Angle	m_azimuth;				///< Azimuth (degrees from north).
		Angle	m_reverseAzimuth;		///< Reverse azimuth (degrees from north).
		};

#endif // __geodesy_GeodeticCurve_h__
