/**
*** @file		GlobalCoordinates.cpp
*** @brief		An object to an angle in degrees
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the JsonArray class.
**/

/*********************************************************************************
Converted to C++ from Gavaghan.Geodesy C# code by Mike Gavaghan

http://www.gavaghan.org/blog/free-source-code/geodesy-library-vincentys-formula/

This code may be freely used and modified on any personal or professional
project.  It comes with no warranty.
*********************************************************************************/

#include "stdafx.h"

#include <geodesy/GlobalCoordinates.h>


GlobalCoordinates::GlobalCoordinates() :
	m_latitude(0.0),
	m_longitude(0.0)
		{
		}


GlobalCoordinates::GlobalCoordinates(const Angle &latitude, const Angle &longitude) :
	m_latitude(latitude),
	m_longitude(longitude)
		{
		canonicalize();
		}


GlobalCoordinates::GlobalCoordinates(const GlobalCoordinates &v)
		{
		*this = v;
		}


GlobalCoordinates &
GlobalCoordinates::operator=(const GlobalCoordinates &v)
		{
		clear();

#define COPY(x)	x = v.x
		COPY(m_latitude);
		COPY(m_longitude);
#undef COPY

		return *this;
		}


// Assignment operator
bool
GlobalCoordinates::operator==(const GlobalCoordinates &v)
		{
		return (compareTo(v) == 0);
		}


int
GlobalCoordinates::compareTo(const GlobalCoordinates &other)
		{
		int		res=0;

		if (m_longitude < other.m_longitude) res = -1;
		else if (m_longitude > other.m_longitude) res = 1;
		else if (m_latitude < other.m_latitude) res = -1;
		else if (m_latitude > other.m_latitude) res = 1;

		return res;
		}

void
GlobalCoordinates::clear()
		{
		m_latitude = m_longitude = 0.0;
		}

void
GlobalCoordinates::set(const Angle &latitude, const Angle &longitude)
		{
		m_latitude = latitude;
		m_longitude = longitude;
		}


void
GlobalCoordinates::canonicalize()
		{
		double latitude = m_latitude.degrees();
		double longitude = m_longitude.degrees();

		latitude = fmod(latitude + 180.0, 360.0);
		if (latitude < 0) latitude += 360.0;
		latitude -= 180.0;

		if (latitude > 90.0)
			{
			latitude = 180.0 - latitude;
			longitude += 180.0;
			}
		else if (latitude < -90)
			{
			latitude = -180.0 - latitude;
			longitude += 180.0;
			}

		longitude = fmod(longitude + 180.0, 360.0);
		if (longitude <= 0) longitude += 360.0;
		longitude -= 180.0;

		m_latitude = latitude;
		m_longitude = longitude;
		}