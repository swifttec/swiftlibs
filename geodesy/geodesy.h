/**
*** @file		geodesy.h
*** @brief		General header for the GEODESY library.
*** @version	1.0
*** @author		Simon Sparkes
***
*** This file is the general header used by all of the components of the
*** GEODESY library.
**/

/*********************************************************************************
Copyright (c) 2006-2017 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __geodesy_h__
#define __geodesy_h__

#include <geodesy/geodesy_defs.h>
#include <geodesy/Ellipsoid.h>
#include <geodesy/Angle.h>
#include <geodesy/GlobalCoordinates.h>
#include <geodesy/GeodeticCurve.h>
#include <geodesy/GlobalPosition.h>
#include <geodesy/GeodeticMeasurement.h>


/// @brief
/// Calculate the destination and final bearing after traveling a specified
/// distance, and a specified starting bearing, for an initial location.
/// This is the solution to the direct geodetic problem.
/// 
/// @param	ellipsoid		reference ellipsoid to use
/// @param	start			starting location
/// @param	startBearing	starting bearing (degrees)
/// @param	distance		distance to travel (meters)
/// @param	endBearing		bearing at destination (degrees)
void GEODESY_DLL_EXPORT	geodesy_calculateEndingGlobalCoordinates(const Ellipsoid &ellipsoid, const GlobalCoordinates &start, const Angle &startBearing, double distance, Angle &endBearing, GlobalCoordinates &coordinates);

/// @brief
/// Calculate the destination after traveling a specified distance, and a
/// specified starting bearing, for an initial location. This is the
/// solution to the direct geodetic problem.
///
/// @param	ellipsoid		reference ellipsoid to use
/// @param	start			starting location
/// @param	startBearing	starting bearing (degrees)
/// @param	distance		distance to travel (meters)
void GEODESY_DLL_EXPORT	geodesy_calculateEndingGlobalCoordinates(const Ellipsoid &ellipsoid, const GlobalCoordinates &start, const Angle &startBearing, double distance, GlobalCoordinates &coordinates);

/// @brief
/// Calculate the geodetic curve between two points on a specified reference ellipsoid.
/// This is the solution to the inverse geodetic problem.
/// 
/// @param	ellipsoid	reference ellipsoid to use
/// @param	start		starting coordinates
/// @param	end			ending coordinates 
void GEODESY_DLL_EXPORT	geodesy_calculateGeodeticCurve(const Ellipsoid &ellipsoid, const GlobalCoordinates &start, const GlobalCoordinates &end, GeodeticCurve &curve);

/// @brief
/// Calculate the three dimensional geodetic measurement between two positions
/// measured in reference to a specified ellipsoid.
/// 
/// This calculation is performed by first computing a new ellipsoid by expanding or contracting
/// the reference ellipsoid such that the new ellipsoid passes through the average elevation
/// of the two positions.  A geodetic curve across the new ellisoid is calculated.  The
/// point-to-point distance is calculated as the hypotenuse of a right triangle where the length
/// of one side is the ellipsoidal distance and the other is the difference in elevation.
/// 
/// @param	refEllipsoid	reference ellipsoid to use
/// @param	start			starting position
/// @param	end				ending position
void GEODESY_DLL_EXPORT	geodesy_calculateGeodeticMeasurement(const Ellipsoid &refEllipsoid, const GlobalPosition &start, const GlobalPosition &end, GeodeticMeasurement &measurement);

#endif // __geodesy_h__
