/**
*** @file		Ellipsoid.cpp
*** @brief		An object to an angle in degrees
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the JsonArray class.
**/

/*********************************************************************************
Converted to C++ from Gavaghan.Geodesy C# code by Mike Gavaghan

http://www.gavaghan.org/blog/free-source-code/geodesy-library-vincentys-formula/

This code may be freely used and modified on any personal or professional
project.  It comes with no warranty.
*********************************************************************************/

#include "stdafx.h"

#include <geodesy/Ellipsoid.h>

#if defined(SW_PLATFORM_WINDOWS)
	#pragma warning(disable: 4723)
#endif

Ellipsoid::Ellipsoid()
		{
		}


Ellipsoid::Ellipsoid(EllipsoidType type)
		{
		switch (type)
			{
			case WGS84:			initFromAAndInverseF(6378137.0, 298.257223563);	break;
			case GRS80:			initFromAAndInverseF(6378137.0, 298.257222101);	break;
			case GRS67:			initFromAAndInverseF(6378160.0, 298.25);		break;
			case ANS:			initFromAAndInverseF(6378160.0, 298.25);		break;
			case WGS72:			initFromAAndInverseF(6378135.0, 298.26);		break;
			case Clarke1858:	initFromAAndInverseF(6378293.645, 294.26);		break;
			case Clarke1880:	initFromAAndInverseF(6378249.145, 293.465);		break;
			default:			initFromAAndF(6371000, 0.0);					break;
			}
		}

void
Ellipsoid::init(double semiMajor, double semiMinor, double flattening, double inverseFlattening)
		{
		m_semiMajorAxis = semiMajor;
		m_semiMinorAxis = semiMinor;
		m_flattening = flattening;
		m_inverseFlattening = inverseFlattening;
		}


void
Ellipsoid::initFromAAndInverseF(double semiMajor, double inverseFlattening)
		{
		double f = 1.0 / inverseFlattening;
		double b = (1.0 - f) * semiMajor;

		init(semiMajor, b, f, inverseFlattening);
		}


void
Ellipsoid::initFromAAndF(double semiMajor, double flattening)
		{
		double inverseF = 1.0 / flattening;
		double b = (1.0 - flattening) * semiMajor;

		init(semiMajor, b, flattening, inverseF);
		}


Ellipsoid
Ellipsoid::fromAAndF(double semiMajor, double flattening)
		{
		Ellipsoid	v;

		v.initFromAAndF(semiMajor, flattening);

		return v;
		}


Ellipsoid
Ellipsoid::fromAAndInverseF(double semiMajor, double inverseFlattening)
		{
		Ellipsoid	v;

		v.initFromAAndInverseF(semiMajor, inverseFlattening);

		return v;
		}
