/**
*** @file		GlobalPosition.cpp
*** @brief		An object to an angle in degrees
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the JsonArray class.
**/

/*********************************************************************************
Converted to C++ from Gavaghan.Geodesy C# code by Mike Gavaghan

http://www.gavaghan.org/blog/free-source-code/geodesy-library-vincentys-formula/

This code may be freely used and modified on any personal or professional
project.  It comes with no warranty.
*********************************************************************************/


#include "stdafx.h"

#include <geodesy/GlobalPosition.h>


GlobalPosition::GlobalPosition() :
	m_elevation(0.0)
		{
		}


GlobalPosition::GlobalPosition(const GlobalCoordinates &coord, double elevation) :
	m_coordinates(coord),
	m_elevation(elevation)
		{
		}


GlobalPosition::GlobalPosition(const GlobalPosition &v)
		{
		*this = v;
		}


GlobalPosition &
GlobalPosition::operator=(const GlobalPosition &v)
		{
		clear();

#define COPY(x)	x = v.x
		COPY(m_coordinates);
		COPY(m_elevation);
#undef COPY

		return *this;
		}


// Assignment operator
bool
GlobalPosition::operator==(const GlobalPosition &v)
		{
		return (compareTo(v) == 0);
		}


int
GlobalPosition::compareTo(const GlobalPosition &other)
		{
		int		res=m_coordinates.compareTo(other.m_coordinates);

		if (res == 0)
			{
			if (m_elevation < other.m_elevation) res = -1;
			else if (m_elevation > other.m_elevation) res = +1;
			}

		return res;
		}

void
GlobalPosition::clear()
		{
		m_coordinates.clear();
		m_elevation = 0.0;
		}

