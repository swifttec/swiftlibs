/**
*** @file		Angle.cpp
*** @brief		An object to an angle in degrees
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the JsonArray class.
**/

/*********************************************************************************
Converted to C++ from Gavaghan.Geodesy C# code by Mike Gavaghan

http://www.gavaghan.org/blog/free-source-code/geodesy-library-vincentys-formula/

This code may be freely used and modified on any personal or professional
project.  It comes with no warranty.
*********************************************************************************/

#include "stdafx.h"

#include <geodesy/Angle.h>


Angle::Angle() :
	m_degrees(0.0)
		{
		}


Angle::Angle(double degrees) :
	m_degrees(degrees)
		{
		}


Angle::Angle(int degrees, double minutes) :
	m_degrees(0.0)
		{
		m_degrees = minutes / 60.0;
		m_degrees = (degrees < 0) ? (degrees - m_degrees) : (degrees + m_degrees);
		}


Angle::Angle(int degrees, int minutes, double seconds) :
	m_degrees(0.0)
		{
		m_degrees = (seconds / 3600.0) + (minutes / 60.0);
		m_degrees = (degrees < 0) ? (degrees - m_degrees) : (degrees + m_degrees);
		}		


Angle::Angle(const Angle &v)
		{
		*this = v;
		}



// Assignment operator
Angle &
Angle::operator=(const Angle &v)
		{
		clear();

#define COPY(x)	x = v.x
		COPY(m_degrees);
#undef COPY

		return *this;
		}


// Assignment operator
bool
Angle::operator==(const Angle &v)
		{
		return (v.m_degrees == m_degrees);
		}

void
Angle::clear()
		{
		m_degrees = 0.0;
		}
