/**
*** @file		GeodeticMeasurement.h
*** @brief		An object to an angle in degrees
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the JsonArray class.
**/

/*********************************************************************************
Converted to C++ from Gavaghan.Geodesy C# code by Mike Gavaghan

http://www.gavaghan.org/blog/free-source-code/geodesy-library-vincentys-formula/

This code may be freely used and modified on any personal or professional
project.  It comes with no warranty.
*********************************************************************************/



#ifndef __geodesy_GeodeticMeasurement_h__
#define __geodesy_GeodeticMeasurement_h__

#include <geodesy/geodesy_defs.h>
#include <geodesy/GeodeticCurve.h>

#include <swift/SWArray.h>
#include <swift/SWString.h>

#include <xplatform/sw_math.h>

class GEODESY_DLL_EXPORT GeodeticMeasurement : public SWObject
		{
public:
		/// Default constructor
		GeodeticMeasurement();
		
		/// Construct a new GeodeticMeasurement from a degree measurement.
		GeodeticMeasurement(const GeodeticCurve &averageCurve, double elevationChange);

		/// Copy constructor
		GeodeticMeasurement(const GeodeticMeasurement &other);

		/// Assignment operator
		GeodeticMeasurement &	operator=(const GeodeticMeasurement &other);

		/// Equality operator
		bool	operator==(const GeodeticMeasurement &other);

		const GeodeticCurve &	averageCurve() const			{ return m_curve; }
		double					azimuth() const					{ return m_curve.azimuth(); }
		double					reverseAzimuth() const			{ return m_curve.reverseAzimuth(); }
		double					elevationChange() const			{ return m_elevationChange; }
		double					pointToPointDistance() const	{ return m_p2p; }

		int						compareTo(const GeodeticMeasurement &other);


public: // Overrides
		virtual void		clear();

private:
		GeodeticCurve	m_curve;			///< The average geodetic curve.
		double			m_elevationChange;	///< The elevation change, in meters, going from the starting to the ending point.
		double			m_p2p;				///< The distance travelled, in meters, going from one point to the next.
		};

#endif // __geodesy_GeodeticMeasurement_h__
