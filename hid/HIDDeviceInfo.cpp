/**
*** @file
*** @brief		General header for the HID library.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the HIDDeviceInfo class.
**/

/*********************************************************************************
Copyright (c) 2006-2017 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <hid/HIDDeviceInfo.h>
#include <hid/HIDApi.h>


HIDDeviceInfo::~HIDDeviceInfo()
		{
		clear();
		}


HIDDeviceInfo::HIDDeviceInfo()
		{
		clear();
		}


// Copy constructor
HIDDeviceInfo::HIDDeviceInfo(const HIDDeviceInfo &v)
		{
		*this = v;
		}


void
HIDDeviceInfo::clear()
		{
		m_path.clear();
		m_vendorID = 0;
		m_productID = 0;
		m_serialNo.clear();
		m_releaseNo = 0;
		m_manufacturer.clear();
		m_product.clear();
		m_usagePage = 0;
		m_usage = 0;
		m_interfaceNo = 0;
		}



bool
HIDDeviceInfo::enumerate(SWArray<HIDDeviceInfo> &data, sw_uint16_t vendor, sw_uint16_t product)
		{
		return theHIDAPI.enumerate(data, vendor, product);
		}

