/**
*** @file
*** @brief		General header for the HID library.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the HIDDevice class.
**/

/*********************************************************************************
Copyright (c) 2006-2017 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"

#include <hid/HIDDevice.h>
#include <hid/HIDApi.h>


HIDDevice::HIDDevice() :
	m_hDevice(0),
	m_inputReportLength(0),
	m_pReadBuffer(NULL),
	m_readPending(false)
		{
#if defined(SW_PLATFORM_WINDOWS)
		// Use the Windows-style interface
		m_hDevice = INVALID_HANDLE_VALUE;
#else
		// Use the Unix-style interface
		m_hDevice = -1;
#endif // defined(SW_FILE_USE_NATIVE_FILEIO)
		}


HIDDevice::~HIDDevice()
		{
		close();
		}




bool
HIDDevice::isOpen() const
		{
#if defined(SW_USE_UNIX_FILES)
		return (m_hDevice >= 0);
#elif defined(SW_PLATFORM_WINDOWS)
		return (m_hDevice != INVALID_HANDLE_VALUE);
#endif
		}



sw_status_t
HIDDevice::open(sw_uint16_t vid, sw_uint16_t pid, const SWString &serial)
		{
		sw_status_t				res = SW_STATUS_NOT_FOUND;
		SWArray<HIDDeviceInfo>	data;
		SWString				path;

		theHIDAPI.enumerate(data, vid, pid);

		for (int i=0;i<(int)data.size();i++)
			{
			HIDDeviceInfo	&info=data[i];

			if (info.vendorID() == vid && info.productID() == pid)
				{
				if (!serial.isEmpty())
					{
					if (info.serialNo() == serial)
						{
						path = info.path();
						break;
						}
					}
				else
					{
					path = info.path();
					break;
					}
				}
			}

		if (!path.isEmpty())
			{
			res = open(path);
			}

		if (res == SW_STATUS_SUCCESS)
			{
			m_pReadBuffer = new sw_byte_t[m_inputReportLength];
			memset(&m_overlappedInfo, 0, sizeof(m_overlappedInfo));
			m_overlappedInfo.hEvent = CreateEvent(NULL, FALSE, FALSE /*inital state f=nonsignaled*/, NULL);
			}

		return res;
		}


sw_status_t
HIDDevice::open(const SWString &path)
		{
		// We do this inside the API so we don't have direct access to the HID internals
		// in this class
		return theHIDAPI.open(path, *this);
		}


sw_status_t
HIDDevice::close()
		{
		sw_status_t	res = SW_STATUS_INVALID_HANDLE;

		if (isOpen())
			{
			CloseHandle(m_overlappedInfo.hEvent);
			CloseHandle(m_hDevice);

			res = SW_STATUS_SUCCESS;
			m_inputReportLength = 0;
			delete m_pReadBuffer;
			m_pReadBuffer = NULL;
			m_readPending = false;
			memset(&m_overlappedInfo, 0, sizeof(m_overlappedInfo));

#if defined(SW_PLATFORM_WINDOWS)
			// Use the Windows-style interface
			m_hDevice = INVALID_HANDLE_VALUE;
#else
			// Use the Unix-style interface
			m_hDevice = -1;
#endif // defined(SW_FILE_USE_NATIVE_FILEIO)
			}

		return res;
		}


sw_status_t
HIDDevice::read(void *data, sw_size_t nbytes, sw_size_t *pBytesRead, sw_uint64_t waitms)
		{
		sw_status_t	result=SW_STATUS_SUCCESS;
		DWORD		count=0;

		if (isOpen())
			{
			BOOL	r;


			if (!m_readPending)
				{
				// Start an Overlapped I/O read.
				m_readPending = true;
				ResetEvent(m_overlappedInfo.hEvent);
				r = ReadFile(m_hDevice, m_pReadBuffer, (DWORD)m_inputReportLength, &count, &m_overlappedInfo);

				if (!r)
					{
					if (GetLastError() != ERROR_IO_PENDING)
						{
						// ReadFile() has failed.
						// Clean up and return error.
						CancelIo(m_hDevice);
						m_readPending = false;
						result = GetLastError();
						}
					}
				}

			if (waitms != 0 && waitms != SW_TIMEOUT_INFINITE)
				{
				// See if there is any data yet.
				r = WaitForSingleObject(m_overlappedInfo.hEvent, (DWORD)waitms);

				if (r != WAIT_OBJECT_0)
					{
					// There was no data this time. Return zero bytes available,
					// but leave the Overlapped I/O running.
					count = 0;
					result = SW_STATUS_TIMEOUT;
					}
				}


			if (result == SW_STATUS_SUCCESS)
				{
				// Either WaitForSingleObject() told us that ReadFile has completed, or
				// we are in non-blocking mode. Get the number of bytes read. The actual
				// data has been copied to the data[] array which was passed to ReadFile().
				r = GetOverlappedResult(m_hDevice, &m_overlappedInfo, &count, TRUE/*wait*/);
		
				// Set pending back to false, even if GetOverlappedResult() returned error.
				m_readPending = false;

				if (r && count > 0)
					{
					if (m_pReadBuffer[0] == 0x0)
						{
						/* If report numbers aren't being used, but Windows sticks a report
						   number (0x0) on the beginning of the report anyway. To make this
						   work like the other platforms, and to make it work more like the
						   HID spec, we'll skip over this byte. */
						count--;
						if (nbytes > count) nbytes = (sw_size_t)count;
						else if (nbytes < count) count = (DWORD)nbytes;
						memcpy(data, m_pReadBuffer+1, nbytes);
						}
					else
						{
						/* Copy the whole buffer, report number and all. */
						if (nbytes > count) nbytes = (sw_size_t)count;
						else if (nbytes < count) count = (DWORD)nbytes;
						memcpy(data, m_pReadBuffer, nbytes);
						}
					}
				}
			}
		else
			{
			result = SW_STATUS_INVALID_HANDLE;
			}

		if (pBytesRead != NULL) *pBytesRead = count;

		return result;
		}


sw_status_t
HIDDevice::write(const void *data, sw_size_t nbytes, sw_size_t *pBytesWritten, sw_uint64_t waitms)
		{
		sw_status_t	result=SW_STATUS_SUCCESS;

		if (isOpen())
			{
			result = SW_STATUS_UNSUPPORTED_OPERATION;
			}
		else
			{
			result = SW_STATUS_INVALID_HANDLE;
			}

		return result;
		}

