/**
*** @file
*** @brief		General header for the HID library.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the HIDApi class.
**/

/*********************************************************************************
Copyright (c) 2006-2017 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"

#include <hid/HIDApi.h>
#include <swift/SWLoadableModule.h>
#include <swift/SWInterlockedInt32.h>


#ifdef __cplusplus
extern "C" {
#endif
	#include <setupapi.h>
	#include <winioctl.h>
	#include <bcrypt.h>

	// Copied from inc/ddk/hidclass.h, part of the Windows DDK.
	#define HID_OUT_CTL_CODE(id)  \
		CTL_CODE(FILE_DEVICE_KEYBOARD, (id), METHOD_OUT_DIRECT, FILE_ANY_ACCESS)
	#define IOCTL_HID_GET_FEATURE                   HID_OUT_CTL_CODE(100)


	// Since we're not building with the DDK, and the HID header
	// files aren't part of the SDK, we have to define all this
	// stuff here. In lookup_functions(), the function pointers
	// defined below are set.
	typedef struct _HIDD_ATTRIBUTES{
		ULONG Size;
		USHORT VendorID;
		USHORT ProductID;
		USHORT VersionNumber;
	} HIDD_ATTRIBUTES, *PHIDD_ATTRIBUTES;

	typedef USHORT USAGE;
	typedef struct _HIDP_CAPS {
		USAGE Usage;
		USAGE UsagePage;
		USHORT InputReportByteLength;
		USHORT OutputReportByteLength;
		USHORT FeatureReportByteLength;
		USHORT Reserved[17];
		USHORT fields_not_used_by_hidapi[10];
	} HIDP_CAPS, *PHIDP_CAPS;
	typedef char* HIDP_PREPARSED_DATA;
	#define HIDP_STATUS_SUCCESS 0x0

	typedef BOOLEAN (__stdcall *HidD_GetAttributes_)(HANDLE device, PHIDD_ATTRIBUTES attrib);
	typedef BOOLEAN (__stdcall *HidD_GetSerialNumberString_)(HANDLE device, PVOID buffer, ULONG buffer_len);
	typedef BOOLEAN (__stdcall *HidD_GetManufacturerString_)(HANDLE handle, PVOID buffer, ULONG buffer_len);
	typedef BOOLEAN (__stdcall *HidD_GetProductString_)(HANDLE handle, PVOID buffer, ULONG buffer_len);
	typedef BOOLEAN (__stdcall *HidD_SetFeature_)(HANDLE handle, PVOID data, ULONG length);
	typedef BOOLEAN (__stdcall *HidD_GetFeature_)(HANDLE handle, PVOID data, ULONG length);
	typedef BOOLEAN (__stdcall *HidD_GetIndexedString_)(HANDLE handle, ULONG string_index, PVOID buffer, ULONG buffer_len);
	typedef BOOLEAN (__stdcall *HidD_GetPreparsedData_)(HANDLE handle, HIDP_PREPARSED_DATA **preparsed_data);
	typedef BOOLEAN (__stdcall *HidD_FreePreparsedData_)(HIDP_PREPARSED_DATA *preparsed_data);
	typedef BOOLEAN (__stdcall *HidP_GetCaps_)(HIDP_PREPARSED_DATA *preparsed_data, HIDP_CAPS *caps);

	static HidD_GetAttributes_ HidD_GetAttributes;
	static HidD_GetSerialNumberString_ HidD_GetSerialNumberString;
	static HidD_GetManufacturerString_ HidD_GetManufacturerString;
	static HidD_GetProductString_ HidD_GetProductString;
	static HidD_SetFeature_ HidD_SetFeature;
	static HidD_GetFeature_ HidD_GetFeature;
	static HidD_GetIndexedString_ HidD_GetIndexedString;
	static HidD_GetPreparsedData_ HidD_GetPreparsedData;
	static HidD_FreePreparsedData_ HidD_FreePreparsedData;
	static HidP_GetCaps_ HidP_GetCaps;


#ifdef __cplusplus
} // extern "C"
#endif


static SWLoadableModule		dll;
static SWInterlockedInt32	hidInitCount(0);

// The global instance
HIDApi	theHIDAPI;

HIDApi::HIDApi()
		{
		SWGuard	guard(hidInitCount);

		if (hidInitCount == 0)
			{
			startup();
			}

		hidInitCount++;
		}


HIDApi::~HIDApi()
		{
		SWGuard	guard(hidInitCount);

		if (--hidInitCount == 0)
			{
			shutdown();
			}
		}



bool
HIDApi::startup()
		{
		bool	res=false;

		if (!res)
			{
			res = (dll.open("hid") == SW_STATUS_SUCCESS);
			}

#define RESOLVE(x) if (res) { x = (x##_)dll.symbol(#x); if (!x) res = false; }
		RESOLVE(HidD_GetAttributes);
		RESOLVE(HidD_GetSerialNumberString);
		RESOLVE(HidD_GetManufacturerString);
		RESOLVE(HidD_GetProductString);
		RESOLVE(HidD_SetFeature);
		RESOLVE(HidD_GetFeature);
		RESOLVE(HidD_GetIndexedString);
		RESOLVE(HidD_GetPreparsedData);
		RESOLVE(HidD_FreePreparsedData);
		RESOLVE(HidP_GetCaps);
#undef RESOLVE

		return res;
		}


bool
HIDApi::shutdown()
		{
		bool	res=true;

		return res;
		}


sw_handle_t
HIDApi::open(const SWString &path)
		{
		HANDLE handle;

		/* First, try to open with sharing mode turned off. This will make it so
		   that a HID device can only be opened once. This is to be consistent
		   with the behavior on the other platforms. */
		handle = CreateFile(path,
			GENERIC_WRITE |GENERIC_READ,
			0, /*share mode*/
			NULL,
			OPEN_EXISTING,
			FILE_FLAG_OVERLAPPED,
			0);

		if (handle == INVALID_HANDLE_VALUE) 
			{
			/* Couldn't open the device. Some devices must be opened
			   with sharing enabled (even though they are only opened once),
			   so try it here. */
			handle = CreateFile(path,
				GENERIC_WRITE |GENERIC_READ,
				FILE_SHARE_READ|FILE_SHARE_WRITE, /*share mode*/
				NULL,
				OPEN_EXISTING,
				FILE_FLAG_OVERLAPPED,
				0);
			}

		return handle;
		}


sw_status_t
HIDApi::open(const SWString &path, HIDDevice &device)
		{
		sw_status_t				res = SW_STATUS_NOT_FOUND;

		if (device.isOpen())
			{
			res = SW_STATUS_ALREADY_OPEN;
			}
		else
			{
			device.m_hDevice = open(path);

			if (device.isOpen())
				{
				HIDP_PREPARSED_DATA *pp_data = NULL;
				HIDP_CAPS			caps;
				NTSTATUS			nt_res;

				// Get the Input Report length for the device.
				if (!HidD_GetPreparsedData(device.m_hDevice, &pp_data))
					{
					// register_error(dev, "HidD_GetPreparsedData");
					res = SW_STATUS_INVALID_PARAMETER;
					}
				else
					{
					nt_res = HidP_GetCaps(pp_data, &caps);
					if (nt_res != HIDP_STATUS_SUCCESS)
						{
						// register_error(dev, "HidP_GetCaps");	
						res = SW_STATUS_INVALID_PARAMETER;
						}
					else
						{
						device.m_inputReportLength = caps.InputReportByteLength;
						res = SW_STATUS_SUCCESS;
						}

					HidD_FreePreparsedData(pp_data);
					}
				}
			}

		return res;
		}


bool
HIDApi::enumerate(SWArray<HIDDeviceInfo> &data, sw_uint16_t vid, sw_uint16_t pid)
		{
		BOOL		res=TRUE;

		// Windows objects for interacting with the driver.
		GUID								InterfaceClassGuid = {0x4d1e55b2, 0xf16f, 0x11cf, {0x88, 0xcb, 0x00, 0x11, 0x11, 0x00, 0x00, 0x30} };
		SP_DEVINFO_DATA						devinfo_data;
		SP_DEVICE_INTERFACE_DATA			device_interface_data;
		SP_DEVICE_INTERFACE_DETAIL_DATA_A	*device_interface_detail_data = NULL;
		HDEVINFO							device_info_set = INVALID_HANDLE_VALUE;
		int									dataidx=0, device_index = 0;

		data.clear();

		// Initialize the Windows objects.
		devinfo_data.cbSize = sizeof(SP_DEVINFO_DATA);
		device_interface_data.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);

		// Get information for all the devices belonging to the HID class.
		device_info_set = SetupDiGetClassDevsA(&InterfaceClassGuid, NULL, NULL, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);
		
		
		for (;;)
			{
			HANDLE			write_handle = INVALID_HANDLE_VALUE;
			DWORD			required_size = 0;
			HIDD_ATTRIBUTES	attrib;

			res = SetupDiEnumDeviceInterfaces(device_info_set, NULL, &InterfaceClassGuid, device_index, &device_interface_data);
			
			if (!res)
				{
				// A return of FALSE from this function means that
				// there are no more devices.
				break;
				}

			// Call with 0-sized detail size, and let the function
			// tell us how long the detail struct needs to be. The
			// size is put in &required_size.
			res = SetupDiGetDeviceInterfaceDetailA(device_info_set, &device_interface_data, NULL, 0, &required_size, NULL);

			// Allocate a long enough structure for device_interface_detail_data.
			device_interface_detail_data = (SP_DEVICE_INTERFACE_DETAIL_DATA_A*) malloc(required_size);
			device_interface_detail_data->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA_A);

			// Get the detailed data for this device. The detail data gives us
			// the device path for this device, which is then passed into
			// CreateFile() to get a handle to the device.
			res = SetupDiGetDeviceInterfaceDetailA(device_info_set, &device_interface_data, device_interface_detail_data, required_size, NULL, NULL);

			if (!res)
				{
				//register_error(dev, "Unable to call SetupDiGetDeviceInterfaceDetail");
				// Continue to the next device.
				goto cont;
				}

			//wprintf(L"HandleName: %s\n", device_interface_detail_data->DevicePath);

			// Open a handle to the device
			write_handle = open(device_interface_detail_data->DevicePath);

			// Check validity of write_handle.
			if (write_handle == INVALID_HANDLE_VALUE)
				{
				// Unable to open the device.
				//register_error(dev, "CreateFile");
				goto cont_close;
				}		


			// Get the Vendor ID and Product ID for this device.
			attrib.Size = sizeof(HIDD_ATTRIBUTES);
			HidD_GetAttributes(write_handle, &attrib);
			//wprintf(L"Product/Vendor: %x %x\n", attrib.ProductID, attrib.VendorID);

			// Check the VID/PID to see if we should add this
			// device to the enumeration list.
			if ((vid == 0x0 && pid == 0x0) || 
				(attrib.VendorID == vid && attrib.ProductID == pid))
				{
				#define WSTR_LEN 512
				const char *str;
				HIDP_PREPARSED_DATA *pp_data = NULL;
				HIDP_CAPS caps;
				BOOLEAN res;
				NTSTATUS nt_res;
				wchar_t wstr[WSTR_LEN]; // TODO: Determine Size

				/* VID/PID match. Create the record. */
				HIDDeviceInfo	&devinfo=data[dataidx++];

				// Get the Usage Page and Usage for this device.
				res = HidD_GetPreparsedData(write_handle, &pp_data);
				if (res)
					{
					nt_res = HidP_GetCaps(pp_data, &caps);
					if (nt_res == HIDP_STATUS_SUCCESS)
						{
						devinfo.usagePage(caps.UsagePage);
						devinfo.usage(caps.Usage);
						}

					HidD_FreePreparsedData(pp_data);
					}
				
				/* Fill out the record */
				str = device_interface_detail_data->DevicePath;
				if (str)
					{
					devinfo.path(str);
					}

				/* Serial Number */
				res = HidD_GetSerialNumberString(write_handle, wstr, sizeof(wstr));
				wstr[WSTR_LEN-1] = 0x0000;
				if (res)
					{
					devinfo.serialNo(wstr);
					}

				/* Manufacturer String */
				res = HidD_GetManufacturerString(write_handle, wstr, sizeof(wstr));
				wstr[WSTR_LEN-1] = 0x0000;
				if (res)
					{
					devinfo.manufacturer(wstr);
					}

				/* Product String */
				res = HidD_GetProductString(write_handle, wstr, sizeof(wstr));
				wstr[WSTR_LEN-1] = 0x0000;
				if (res)
					{
					devinfo.product(wstr);
					}

				/* VID/PID */
				devinfo.vendorID(attrib.VendorID);
				devinfo.productID(attrib.ProductID);

				/* Release Number */
				devinfo.releaseNo(attrib.VersionNumber);

				/* Interface Number. It can sometimes be parsed out of the path
				   on Windows if a device has multiple interfaces. See
				   http://msdn.microsoft.com/en-us/windows/hardware/gg487473 or
				   search for "Hardware IDs for HID Devices" at MSDN. If it's not
				   in the path, it's set to -1. */
				devinfo.interfaceNo(-1);

				if (!devinfo.path().isEmpty())
					{
					/*
					char *interface_component = strstr(cur_dev->path, "&mi_");
					if (interface_component) {
						char *hex_str = interface_component + 4;
						char *endptr = NULL;
						cur_dev->interface_number = strtol(hex_str, &endptr, 16);
						if (endptr == hex_str)
							{
							// The parsing failed. Set interface_number to -1.
							cur_dev->interface_number = -1;
							}
						}
					*/
					}
				}

	cont_close:
			CloseHandle(write_handle);
	cont:
			// We no longer need the detail data. It can be freed
			free(device_interface_detail_data);

			device_index++;
			}

		// Close the device information handle.
		SetupDiDestroyDeviceInfoList(device_info_set);

		return true;
		}

