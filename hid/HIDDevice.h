/**
*** @file
*** @brief		General header for the HID library.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the HIDDevice class.
**/

/*********************************************************************************
Copyright (c) 2006-2017 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __HIDDevice_h__
#define __HIDDevice_h__

#include <hid/hid.h>

#include <swift/SWObject.h>
#include <swift/SWString.h>

class HID_DLL_EXPORT HIDDevice
		{
friend class HIDApi;

public:
		HIDDevice();
		~HIDDevice();

		sw_status_t			open(const SWString &path);
		sw_status_t			open(sw_uint16_t vid, sw_uint16_t pid, const SWString &serial="");
		sw_status_t			close();
		bool				isOpen() const;

		/**
		*** @brief	Read nbytes of data from the file into the specified memory location.
		***
		*** @return the number of bytes actually read or -1 on failure.
		**/
		sw_status_t			read(void *data, sw_size_t nbytes, sw_size_t *pBytesRead=NULL, sw_uint64_t waitms=SW_TIMEOUT_INFINITE);

		/**
		*** @brief	Write nbytes of data to the file from the specified memory location.
		***
		*** @return the number of bytes actually written or -1 on failure.
		**/
		sw_status_t			write(const void *data, sw_size_t nbytes, sw_size_t *pBytesWritten=NULL, sw_uint64_t waitms=SW_TIMEOUT_INFINITE);


protected:
		sw_filehandle_t	m_hDevice;
		sw_size_t		m_inputReportLength;
		sw_byte_t		*m_pReadBuffer;
		bool			m_readPending;
		OVERLAPPED		m_overlappedInfo;
		};

#endif
