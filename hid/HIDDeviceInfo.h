/**
*** @file
*** @brief		General header for the HID library.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the HIDDeviceInfo class.
**/

/*********************************************************************************
Copyright (c) 2006-2017 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#ifndef __HIDDeviceInfo_h__
#define __HIDDeviceInfo_h__

#include <hid/hid.h>

#include <swift/SWObject.h>
#include <swift/SWString.h>
#include <swift/SWArray.h>

class HID_DLL_EXPORT HIDDeviceInfo
		{
public:
		HIDDeviceInfo();
		HIDDeviceInfo(const HIDDeviceInfo &other);
		virtual ~HIDDeviceInfo();

		void				path(const SWString &v)				{ m_path = v; }
		const SWString &	path() const						{ return m_path; }

		void				serialNo(const SWString &v)			{ m_serialNo = v; }
		const SWString &	serialNo() const					{ return m_serialNo; }

		void				manufacturer(const SWString &v)		{ m_manufacturer = v; }
		const SWString &	manufacturer() const				{ return m_manufacturer; }

		void				product(const SWString &v)			{ m_product = v; }
		const SWString &	product() const						{ return m_product; }

		void				vendorID(sw_uint16_t v)				{ m_vendorID = v; }
		sw_uint16_t			vendorID() const					{ return m_vendorID; }

		void				productID(sw_uint16_t v)			{ m_productID = v; }
		sw_uint16_t			productID() const					{ return m_productID; }

		void				releaseNo(sw_uint16_t v)			{ m_releaseNo = v; }
		sw_uint16_t			releaseNo() const					{ return m_releaseNo; }

		void				usagePage(sw_uint16_t v)			{ m_usagePage = v; }
		sw_uint16_t			usagePage() const					{ return m_usagePage; }

		void				usage(sw_uint16_t v)				{ m_usage = v; }
		sw_uint16_t			usage() const						{ return m_usage; }

		void				interfaceNo(int v)					{ m_interfaceNo = v; }
		int					interfaceNo() const					{ return m_interfaceNo; }


public: // Overrides
		virtual void		clear();


public: // Static methods
		static bool			enumerate(SWArray<HIDDeviceInfo> &data, sw_uint16_t vendor=0, sw_uint16_t product=0);


private:
		SWString	m_path;				// Platform-specific device path
		sw_uint16_t	m_vendorID;			// Device Vendor ID
		sw_uint16_t	m_productID;			// Device Product ID
		SWString	m_serialNo;			// Serial Number
		sw_uint16_t	m_releaseNo;		// Device Release Number in binary-coded decimal, also known as Device Version Number
		SWString	m_manufacturer;		// Manufacturer String
		SWString	m_product;			// Product string
		sw_uint16_t	m_usagePage;		// Usage Page for this Device/Interface (Windows/Mac only).
		sw_uint16_t	m_usage;			// Usage for this Device/Interface (Windows/Mac only).
		int			m_interfaceNo;		// The USB interface which this logical device represents. Valid on both Linux implementations in all cases, and valid on the Windows implementation only if the device contains more than one interface.
		};

#endif
