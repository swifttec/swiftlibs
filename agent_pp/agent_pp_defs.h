/**
*** @file		agent_defs.h
*** @brief		General header for the AGENT_PP library.
*** @version	1.0
*** @author		Simon Sparkes
***
*** This file is the general header used by all of the components of the
*** AGENT_PP library.
**/

/*********************************************************************************
Copyright (c) 2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __agent_defs_h__
#define __agent_defs_h__

#include <swift/swift.h>

#if defined(SW_PLATFORM_WINDOWS)

	// Determine the import/export type for a DLL build
	#if defined(AGENT_PP_BUILD_DLL)
		#define AGENT_PP_DLL_EXPORT __declspec(dllexport)
		#define AGENT_PP_DLL_TEMPL
	#else
		#define AGENT_PP_DLL_EXPORT __declspec(dllimport)
		#define AGENT_PP_DLL_TEMPL extern
	#endif // defined(AGENT_PP_BUILD_DLL)

#else // defined(SW_PLATFORM_WINDOWS)

	// On non-windows platforms we just define this to be nothing
	#define AGENT_PP_DLL_EXPORT
	#define AGENT_PP_DLL_TEMPL

#endif // defined(SW_PLATFORM_WINDOWS)

#endif // __agent_defs_h__
