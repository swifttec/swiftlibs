/**
*** @file		XmlString.cpp
*** @brief		An object to represent a single XML node attribute
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the XmlString class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <xml/XmlString.h>



XmlString::~XmlString()
		{
		clear();
		}


XmlString::XmlString(const SWString &value) :
	XmlObject(String),
	m_value(value)
		{
		}


// Copy constructor
XmlString::XmlString(const XmlString &v) :
	XmlObject(String)
		{
		*this = v;
		}


// Assignment operator
XmlString &
XmlString::operator=(const XmlString &v)
		{
#define COPY(x)	x = v.x
		COPY(m_value);
#undef COPY

		return *this;
		}


void
XmlString::clear()
		{
		m_value.clear();
		XmlObject::clear();
		}


void
XmlString::write(SWTextFile &f)
		{
		f.writeString(m_value);
		}


SWString
XmlString::encode(const SWString &v)
		{
		SWString	in, out;
		int			pos=0, len=(int)v.length();

		while (pos < len)
			{
			int		nc=(int)v.count("&<>\"", SWString::excluding, pos, false);

			if (nc > 0)
				{
				out += v.mid(pos, nc);
				pos += nc;
				}

			if (pos < len)
				{
				// Must have a special character
				int		c=v.getCharAt(pos);

				switch (c)
					{
					case '&':	out += "&amp;";		break;
					case '<':	out += "&lt;";		break;
					case '>':	out += "&gt;";		break;
					case '"':	out += "&quot;";	break;
					case '\'':	out += "&apos;";	break;

					// Should never get here - but just in case!
					default:	out += c;			break;
					}

				pos++;
				}
			}

		return out;
		}


SWString
XmlString::decode(const SWString &v)
		{
		SWString	in, out;
		int			pos=0, len=(int)v.length();

		while (pos < len)
			{
			int		nc=(int)v.count("&", SWString::excluding, pos, false);

			if (nc > 0)
				{
				out += v.mid(pos, nc);
				pos += nc;
				}

			if (pos < len)
				{
				// Must have a special character
				SWString	s=v.mid(pos, 6);

				if (s.startsWith("&amp;"))
					{
					out += "&";
					pos += 5;
					}
				else if (s.startsWith("&lt;"))
					{
					out += "<";
					pos += 4;
					}
				else if (s.startsWith("&gt;"))
					{
					out += ">";
					pos += 4;
					}
				else if (s.startsWith("&quot;"))
					{
					out += "\"";
					pos += 6;
					}
				else if (s.startsWith("&apos;"))
					{
					out += "'";
					pos += 6;
					}
				else
					{
					// In case we have badly formed XML
					int		c=v.getCharAt(pos);
					
					out += c;
					pos++;
					}
				}
			}

		return out;
		}


