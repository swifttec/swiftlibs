/**
*** @file		XmlNode.h
*** @brief		An object to represent an XML node
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the XmlNode class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __XmlNode_h__
#define __XmlNode_h__

#include <xml/XmlAttr.h>
#include <xml/XmlObject.h>

#include <swift/SWList.h>
#include <swift/SWTextFile.h>



class XML_DLL_EXPORT XmlNode : public XmlObject
		{
friend class XmlFile;

public:
		XmlNode(const SWString &name="");
		XmlNode(const XmlNode &other);
		virtual ~XmlNode();

		XmlNode &	operator=(const XmlNode &other);

		/// Set the name of this tag
		void					name(const SWString &v)		{ m_name = v; }

		/// Get the name of this tag
		const SWString &		name() const				{ return m_name; }

		/// Check to see if the tag contains the specified attribute
		bool					containsAttr(const SWString &attr) const;

		/// Get the specified attribute
		bool					getAttr(const SWString &attr, SWValue &value) const;

		/// Set the specified attribute
		void					setAttr(const SWString &attr, const SWValue &value);

		/// Find the specifiled attibute and return a pointer to it, or NULL if not found
		XmlAttr *				findAttr(const SWString &attr) const;

		/// Add the string to this node
		void					addString(const SWString &v);

		/// Add the string to this node
		void					addValue(const SWValue &v)		{ addString(v.toString()); }

		// Add a node to this node
		XmlNode *				addNode(const SWString &name);
 
		// Find a node of the type specified or NULL if not found
		XmlNode *				findNode(const SWString &name) const;

		// Remove a node from this node
		bool					removeNode(const SWString &name);
 

		SWIterator<XmlAttr *>	attributes() const			{ return m_attrlist.iterator(); }
		SWIterator<XmlObject *>	elements() const			{ return m_objlist.iterator(); }
		SWString				toString() const;
		virtual sw_int32_t		toInt32() const					{ return toString().toInt32(10); }
		virtual sw_int32_t		toInt32(int base) const			{ return toString().toInt32(base); }
		double					toDouble() const				{ return toString().toDouble(); }
		bool					toBoolean() const;

		SWValue					getNodeValue(const SWString &name) const;
		SWString				getNodeString(const SWString &name) const	{ return getNodeValue(name).toString(); }
		sw_int32_t				getNodeInt32(const SWString &name) const	{ return getNodeValue(name).toInt32(); }
		double					getNodeDouble(const SWString &name) const	{ return getNodeValue(name).toDouble(); }
		bool					getNodeBoolean(const SWString &name) const	{ return getNodeValue(name).toBoolean(); }

		void					clearAttributes();
		void					clearElements();

public: // Overrides
		virtual void		clear();
		virtual void		write(SWTextFile &f);

protected:
		void				add(XmlObject *pObj);

private:
		SWString			m_name;			///< The node name
		SWList<XmlAttr *>	m_attrlist;		///< A collection of attributes
		SWList<XmlObject *>	m_objlist;		///< A collection of attributes
		};


#endif
