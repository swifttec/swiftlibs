/**
*** @file		XmlString.h
*** @brief		An object to represent a single XML node attribute
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the XmlString class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __XmlString_h__
#define __XmlString_h__

#include <xml/XmlObject.h>

#include <swift/SWObject.h>
#include <swift/SWString.h>



class XML_DLL_EXPORT XmlString : public XmlObject
		{
public:
		XmlString(const SWString &value="");
		XmlString(const XmlString &other);
		virtual ~XmlString();

		XmlString &	operator=(const XmlString &other);

		/// Set the value of this tag
		void				value(const SWString &v)	{ m_value = v; }

		/// Get the value of this tag
		const SWString &	value() const				{ return m_value; }


public: // Overrides
		virtual void		clear();
		virtual void		write(SWTextFile &f);

public: // Static methods
		static SWString		encode(const SWString &v);
		static SWString		decode(const SWString &v);

private:
		SWString	m_value;
		};

#endif
