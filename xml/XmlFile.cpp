/**
*** @file		SWXmlFile.cpp
*** @brief		An object to represent an XML node
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWXmlFile class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/






#include "stdafx.h"

#include <xml/XmlFile.h>
#include <xml/XmlNode.h>
#include <xml/XmlString.h>

#include <swift/SWArray.h>
#include <swift/SWTextFile.h>
#include <swift/sw_printf.h>
#include <swift/SWFileInfo.h>





//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
XmlFile::~XmlFile()
		{
		clear();
		}



XmlFile::XmlFile(const SWString &filename) :
	m_trim(true)
		{
		if (!filename.isEmpty())
			{
			load(filename);
			}
		}



void
XmlFile::clear()
		{
		m_filename.clear();
		XmlNode::clear();
		m_pCurrentNode = this;
		m_nodestack.clear();
		m_string.clear();
		}

#define	SEPCHARS	" \t\r\n"


sw_status_t
XmlFile::processLine(const SWString &line, int pos)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		int			len=(int)line.length();

		while (res == SW_STATUS_SUCCESS && pos < len)
			{
			int		p, q;

			if ((p = line.indexOf('<', pos)) >= 0)
				{
				if (p > pos)
					{
					m_string += line.substring(pos, p-1);
					pos = p;
					}

//					m_string.strip(SEPCHARS, SWString::both);
				if (!m_string.isEmpty())
					{
					//printf("Adding string: [%s]\n", m_string.c_str());
					m_pCurrentNode->add(new XmlString(XmlString::decode(m_string)));
					m_string.clear();
					}

				// Check for a comment
				if (line.substring(pos, pos+3) == "<!--")
					{
					if ((p = line.indexOf("-->", pos+4)) >= 0)
						{
						pos = p+1;
						}
					else
						{
						// May need more data - break out of loop
						break;
						}

					continue;
					}

				// Now look for the matching > char
				if ((p = line.indexOf('>', pos)) < 0)
					{
					// May need more data - break out of loop
					break;
					}

				// At this point line should contain "<....>....."
				// with p pointing to the > char.
				SWString	tag = line.substring(pos+1, p-1);
				if (tag.length() < 1)
					{
					res = SW_STATUS_XML_PARSE_ERROR;
					break;
					}

				pos = p+1;

				if (tag.startsWith("/"))
					{
					// This is a end of node marker and should match the current node
					tag.remove(0, 1);
					if (tag.compareNoCase(m_pCurrentNode->name()) == 0)
						{
						//printf("End node: %s\n", m_pCurrentNode->name().c_str());
						if (m_nodestack.size() == 0)
							{
							res = SW_STATUS_XML_PARSE_ERROR;
							break;
							}
						else
							{
							m_pCurrentNode = m_nodestack.pop();
							}
						}
					else
						{
						res = SW_STATUS_XML_PARSE_ERROR;
						break;
						}
					}
				else
					{
					XmlNode		*pNewNode;

					// This is a new node
					m_nodestack.push(m_pCurrentNode);
					pNewNode = new XmlNode();
					m_pCurrentNode->add(pNewNode);
					m_pCurrentNode = pNewNode;

					// First find the node name
					p = tag.firstOf(SEPCHARS "/");
					if (p < 0)
						{
						// No attributes!
						m_pCurrentNode->name(tag);
						tag.clear();
						}
					else
						{
						m_pCurrentNode->name(tag.left(p));
						tag.remove(0, p);
						}

					//printf("Start node: %s\n", m_pCurrentNode->name().c_str());

					while (!tag.isEmpty())
						{
						if (tag == "/")
							{
							// We have completed an empty node with only attributes
							tag.empty();
							m_pCurrentNode = m_nodestack.pop();
							break;
							}

						p = (int)tag.count(SEPCHARS);
						if (p > 0)
							{
							tag.remove(0, p);
							continue;
							}

						// Find the next space
						q = tag.firstOf(SEPCHARS);
						p = tag.first('=');

						if (p >= 0 && q > 0 && q < p)
							{
							tag.remove(q, 1);
							continue;
							}

						SWString	name;
						SWString	value;

						if (p >= 0 && ((q >= 0 && p < q) || (q < 0)))
							{
							name = tag.left(p);
							tag.remove(0, p+1);

							while (tag.length() > 0 && 
								(
								tag[0] == ' ' ||
								tag[0] == '\t' ||
								tag[0] == '\r' ||
								tag[0] == '\n')
								)
								{
								tag.remove(0, 1);
								}

							if (tag[0] == '"')
								{
								tag.remove(0, 1);
								p = tag.first('"');
								if (p < 0)
									{
									res = SW_STATUS_XML_PARSE_ERROR;
									break;
									}

								value = tag.left(p);
								tag.remove(0, p+1);
								}
							else
								{
								p = tag.firstOf(SEPCHARS);

								if (p < 0)
									{
									value = tag;
									tag.clear();
									}
								else
									{
									value = tag.left(p);
									tag.remove(0, p);
									}
								}
							
							if (!name.isEmpty())
								{
								m_pCurrentNode->add(new XmlAttr(name, XmlString::decode(value)));
								}
							}
						else if (p < 0 && q < 0)
							{
							res = SW_STATUS_XML_PARSE_ERROR;
							break;
							}
						}
					}
				}
			else
				{
				m_string += line.substring(pos, len);
				pos = len;
				}
			}

		return res;
		}


sw_status_t
XmlFile::loadFromString(const SWString &contents, bool requireheader)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		clear();

		SWString	line=contents;
		int			p;

		if (!line.startsWith("<?xml"))
			{
			if (requireheader)
				{
				res = SW_STATUS_XML_PARSE_ERROR;
				}
			else
				{
				res = processLine(line, 0);
				}
			}
		else
			{
			p = line.first('>');
			if (p > 0)
				{
				res = processLine(line, p+1);
				}
			else res = SW_STATUS_XML_PARSE_ERROR;
			}

		return res;
		}


sw_status_t
XmlFile::load(const SWString &filename, bool requireheader)
		{
		clear();

		sw_status_t	res=SW_STATUS_SUCCESS;
		SWTextFile		f;

		if ((f.open(filename, TF_MODE_READ)) == SW_STATUS_SUCCESS)
			{
			SWString	line, contents;

			// Read the entire file into a single string
			contents.capacity((sw_size_t)SWFileInfo::size(filename));

			while (line.readLine(f, false))
				{
				contents += line;
				}

			res = loadFromString(contents, requireheader);

			f.close();
			}
		else
			{
			res = SW_STATUS_NOT_FOUND;
			}

		return res;
		}



sw_status_t
XmlFile::save(const SWString &filename)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		SWTextFile	f;

		if (f.open(filename, TF_MODE_WRITE|TF_ENCODING_UTF8) == SW_STATUS_SUCCESS)
			{
			sw_fprintf(f, "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");

			SWIterator<XmlObject *>	it;

			it = elements();
			while (it.hasNext())
				{
				(*it.next())->write(f);
				}

			f.close();
			}

		return res;
		}


