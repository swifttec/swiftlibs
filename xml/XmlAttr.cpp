/**
*** @file		XmlAttr.cpp
*** @brief		An object to represent a single XML node attribute
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the XmlAttr class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <xml/XmlAttr.h>



XmlAttr::~XmlAttr()
		{
		clear();
		}


XmlAttr::XmlAttr(const SWString &name, const SWString &value) :
	XmlObject(Attribute),
	m_name(name),
	m_value(value)
		{
		}


// Copy constructor
XmlAttr::XmlAttr(const XmlAttr &v) :
	XmlObject(Attribute)
		{
		*this = v;
		}


// Assignment operator
XmlAttr &
XmlAttr::operator=(const XmlAttr &v)
		{
#define COPY(x)	x = v.x
		COPY(m_name);
		COPY(m_value);
#undef COPY

		return *this;
		}


void
XmlAttr::clear()
		{
		m_name.clear();
		m_value.clear();
		XmlObject::clear();
		}



