/**
*** @file		SWXmlNode.cpp
*** @brief		An object to represent an XML node
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWXmlNode class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"

#include <xml/XmlAttr.h>
#include <xml/XmlNode.h>
#include <xml/XmlString.h>
#include <swift/sw_printf.h>
#include <swift/SWLocale.h>




//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


XmlNode::~XmlNode()
		{
		clear();
		}


XmlNode::XmlNode(const SWString &name) :
	XmlObject(Node),
	m_name(name)
		{
		}


// Copy constructor
XmlNode::XmlNode(const XmlNode &v) :
	XmlObject(Node)
		{
		*this = v;
		}


// Assignment operator
XmlNode &
XmlNode::operator=(const XmlNode &v)
		{
#define COPY(x)	x = v.x
		COPY(m_name);
#undef COPY

		return *this;
		}


void
XmlNode::clear()
		{
		m_name.clear();
		clearAttributes();
		clearElements();
		}


void
XmlNode::clearAttributes()
		{
		XmlAttr		*pAttr;

		while (m_attrlist.remove(m_attrlist.ListHead, &pAttr))
			{
			delete pAttr;
			}
		}


void
XmlNode::clearElements()
		{
		XmlObject	*pObj;

		while (m_objlist.remove(m_objlist.ListHead, &pObj))
			{
			delete pObj;
			}
		}



bool
XmlNode::containsAttr(const SWString &attr) const
		{
		return (findAttr(attr) != NULL);
		}


XmlNode *
XmlNode::findNode(const SWString &attr) const
		{
		SWIterator<XmlObject *>	it;
		XmlObject				*pObj=NULL;
		XmlNode					*pNode=NULL;

		// Nodeibutes are only at the beginning of the list
		it = m_objlist.iterator();
		while (it.hasNext())
			{
			pObj = *it.next();

			if (pObj->objtype() == Node)
				{
				XmlNode	*pTmpNode=dynamic_cast<XmlNode *>(pObj);

				if (pTmpNode->name().compareNoCase(attr) == 0)
					{
					pNode = pTmpNode;
					break;
					}
				}
			}

		return pNode;
		}


bool
XmlNode::removeNode(const SWString &name)
		{
		SWIterator<XmlObject *>	it;
		XmlObject				*pObj=NULL;
		bool					res=false;

		// Nodeibutes are only at the beginning of the list
		it = m_objlist.iterator();
		while (it.hasNext())
			{
			pObj = *it.next();

			if (pObj->objtype() == Node)
				{
				XmlNode	*pTmpNode=dynamic_cast<XmlNode *>(pObj);

				if (pTmpNode->name().compareNoCase(name) == 0)
					{
					it.remove();
					safe_delete(pTmpNode);
					res = true;
					break;
					}
				}
			}

		return res;
		}


SWValue
XmlNode::getNodeValue(const SWString &name) const
		{
		SWValue	v;
		XmlNode	*pNode=findNode(name);

		if (pNode != NULL)
			{
			v = pNode->toString();
			}

		return v;
		}


XmlAttr *
XmlNode::findAttr(const SWString &attr) const
		{
		SWIterator<XmlAttr *>	it;
		XmlAttr					*pObj=NULL;
		XmlAttr					*pAttr=NULL;

		// Attributes are only at the beginning of the list
		it = m_attrlist.iterator();
		while (it.hasNext())
			{
			pObj = *it.next();

			if (pObj->name().compareNoCase(attr) == 0)
				{
				pAttr = pObj;
				break;
				}
			}

		return pAttr;
		}



bool
XmlNode::getAttr(const SWString &attr, SWValue &value) const
		{
		SWIterator<XmlAttr *>	it;
		XmlAttr					*pObj;
		bool					found=false;

		// Attributes are only at the beginning of the list
		it = m_attrlist.iterator();
		while (it.hasNext())
			{
			pObj = *it.next();

			if (pObj->name().compareNoCase(attr) == 0)
				{
				found = true;
				value = pObj->value();
				break;
				}
			}

		return found;
		}



void
XmlNode::setAttr(const SWString &attr, const SWValue &value)
		{
		XmlAttr		*pAttr;

		pAttr = findAttr(attr);
		if (pAttr == NULL)
			{
			pAttr = new XmlAttr(attr);
			add(pAttr);
			}

		pAttr->value(value.toString());
		}



void
XmlNode::addString(const SWString &s)
		{
		if (!s.isEmpty())
			{
			add(new XmlString(s));
			}
		}


XmlNode *
XmlNode::addNode(const SWString &name)
		{
		XmlNode	*pNode=NULL;

		if (!name.isEmpty())
			{
			pNode = new XmlNode(name);
			add(pNode);
			}

		return pNode;
		}


void
XmlNode::add(XmlObject *pObj)
		{
		if (pObj->objtype() == Attribute)
			{
			m_attrlist.add(dynamic_cast<XmlAttr *>(pObj));
			}
		else
			{
			m_objlist.add(pObj);
			}
		}



bool
XmlNode::toBoolean() const
		{
		SWString	s=toString();
		bool		res=false;

		if (s.compareNoCase(SWLocale::getCurrentLocale().booleanChars(true)) == 0) res = true;
		else if (s.compareNoCase(SWLocale::getCurrentLocale().booleanChars(false)) == 0) res = false;
		else if (s.compareNoCase(SWLocale::getCurrentLocale().booleanString(true)) == 0) res = true;
		else if (s.compareNoCase(SWLocale::getCurrentLocale().booleanString(false)) == 0) res = false;
		else if (s.startsWith("t", SWString::ignoreCase)) res = true;
		else if (s.startsWith("f", SWString::ignoreCase)) res = false;
		else if (s.startsWith("y", SWString::ignoreCase)) res = true;
		else if (s.startsWith("n", SWString::ignoreCase)) res = false;
		else res = (s.toInt32(10) != 0);

		return res;
		}


SWString
XmlNode::toString() const
		{
		SWIterator<XmlObject *>	it;
		XmlObject				*pObj;
		SWString				res;
		XmlString				*pStr;

		// Attributes are only at the beginning of the list
		it = elements();
		while (it.hasNext())
			{
			pObj = *it.next();

			pStr = dynamic_cast<XmlString *>(pObj);
			if (pStr != NULL)
				{
				res += pStr->value();
				}
			}

		return res;
		}


void
XmlNode::write(SWTextFile &f)
		{
		sw_fprintf(f, "<%s", m_name.c_str());

		SWIterator<XmlAttr *>	itAttribs=attributes();
		SWIterator<XmlObject *>	itElements=elements();

		while (itAttribs.hasNext())
			{
			XmlAttr	*pAttrib=*itAttribs.next();

			sw_fprintf(f, " %s=\"%s\"", pAttrib->name().c_str(), pAttrib->value().c_str());
			}

		if (itElements.hasNext())
			{
			sw_fprintf(f, ">");

			while (itElements.hasNext())
				{
				XmlObject	*pObject=*itElements.next();

				pObject->write(f);
				}

			sw_fprintf(f, "</%s>", m_name.c_str());
			}
		else
			{
			sw_fprintf(f, " />");
			}
		}


