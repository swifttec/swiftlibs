/**
*** @file		XmlAttr.h
*** @brief		An object to represent a single XML node attribute
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the XmlAttr class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




#ifndef __XmlAttr_h__
#define __XmlAttr_h__

#include <xml/XmlObject.h>

#include <swift/SWObject.h>
#include <swift/SWString.h>


class XML_DLL_EXPORT XmlAttr : public XmlObject
		{
public:
		XmlAttr(const SWString &name="", const SWString &value="");
		XmlAttr(const XmlAttr &other);
		virtual ~XmlAttr();

		XmlAttr &	operator=(const XmlAttr &other);

		/// Set the name of this tag
		void				name(const SWString &v)		{ m_name = v; }

		/// Get the name of this tag
		const SWString &	name() const				{ return m_name; }

		/// Set the value of this tag
		void				value(const SWString &v)	{ m_value = v; }

		/// Get the value of this tag
		const SWString &	value() const				{ return m_value; }


public: // Overrides
		virtual void		clear();


private:
		SWString	m_name;
		SWString	m_value;
		};

#endif
