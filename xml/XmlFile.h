/**
*** @file		XmlFile.h
*** @brief		An object to represent an XML node
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the XmlFile class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




#ifndef __XmlFile_h__
#define __XmlFile_h__

#include <xml/XmlNode.h>

#include <swift/SWList.h>
#include <swift/SWArray.h>


class XML_DLL_EXPORT XmlFile : public XmlNode
		{
public:
		XmlFile(const SWString &filename="");
		virtual ~XmlFile();


		sw_status_t			loadFromString(const SWString &contents, bool requireheader);
		sw_status_t			load(const SWString &filename, bool requireheader=true);
		sw_status_t			save(const SWString &filename);
		sw_status_t			save();

public: // Overrides
		virtual void		clear();

private:
		sw_status_t			processLine(const SWString &line, int pos);

private:
		SWString			m_filename;
		bool				m_trim;
		SWArray<XmlNode *>	m_nodestack;
		XmlNode 			*m_pCurrentNode;
		SWString			m_string;
		};


#endif
