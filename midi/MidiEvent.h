/**
*** @file		SWInt32.h
*** @brief		A class to represent a date
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWInt32 class.
***
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __midi_MidiEvent_h__
#define __midi_MidiEvent_h__

#include <midi/midi.h>

#include <swift/swift.h>
#include <swift/SWObject.h>
#include <swift/SWException.h>
#include <swift/SWString.h>

/**
*** @brief	A class to represent an single MIDI event
*** @author		Simon Sparkes
*/
class MIDI_DLL_EXPORT MidiEvent
		{
public:
		enum EventTypes
			{
			NoteOff					= 0x80,
			NoteOn					= 0x90,
			PolyphonicKeyPressure 	= 0xa0,
			ControlChange			= 0xb0,
			ProgramChange			= 0xc0,
			ChannelPressure			= 0xd0,
			PitchBend				= 0xe0,
			SystemExclusive			= 0xf0,
			TimeCodeQuarterFrame	= 0xf1,
			SongPositionPointer		= 0xf2,
			SongSelect				= 0xf3,
			Reserved1				= 0xf4,
			Reserved2				= 0xf5,
			TuneRequest				= 0xf6,
			EndOfExclusive			= 0xf7,
			TimingClock				= 0xf8,
			Reserved3				= 0xf9,
			SequenceStart			= 0xfa,
			SequenceContinue		= 0xfb,
			SequenceStop			= 0xfc,
			Reserved4				= 0xfd,
			ActiveSensing			= 0xfe,
			Reset					= 0xff
			};

public:
		// Default constructor
		MidiEvent();

		// Default constructor
		MidiEvent(const sw_byte_t *buf);

		/// Return the event type for this event
		int			type() const;

		/// Return the channel for this event or zero if no channel or system message
		int			channel() const;

		/// Return the status byte (the first byte in the message sequence)
		sw_byte_t	status() const;

		bool		isSystemMessage() const	{ return ((status() & 0xf0) == 0xf0); }
		bool		isValid() const	{ return (status() != 0); }

		bool		operator==(const MidiEvent &other);

		SWString	toString();

		sw_byte_t	param(int idx) const;

protected:
		sw_byte_t	m_data[4];
		};

#endif // __midi_MidiEvent_h__
