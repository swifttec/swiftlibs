#include "stdafx.h"
#include "MidiEvent.h"


MidiEvent::MidiEvent()
		{
		memset(m_data, 0, sizeof(m_data));
		}


MidiEvent::MidiEvent(const sw_byte_t *buf)
		{
		int		nbytes=3;

		memset(m_data, 0, sizeof(m_data));

		switch (buf[0] & 0xf0)
			{
			case 0xc0:
			case 0xd0:
				nbytes = 2;
				break;

			case 0xf0:
				switch (buf[0])
					{
					case 0xf0:
						nbytes = 0;
						break;

					case 0xf1:
					case 0xf2:
						nbytes = 2;
						break;

					case 0xf3:
					case 0xf4:
					case 0xf5:
					case 0xf6:
					case 0xf7:
					case 0xf8:
					case 0xf9:
					case 0xfa:
					case 0xfb:
					case 0xfc:
					case 0xfd:
					case 0xfe:
					case 0xff:
						nbytes = 1;
						break;
					}
				break;
			}

		if (nbytes > 0)
			{
			memcpy(m_data, buf, nbytes);
			/*
			printf("%d bytes: ", nbytes);
			for (int i=0;i<nbytes;i++)
				{
				printf(" %02X", m_data[i]);
				}
			printf("\n");
			*/
			}
		}


bool
MidiEvent::operator==(const MidiEvent &other)
		{
		return (memcmp(m_data, other.m_data, 4) == 0);
		}


int
MidiEvent::type() const
		{
		int		res=0;

		if ((m_data[0] & 0x80) != 0)
			{
			if ((m_data[0] & 0xf0) == 0xf0)
				{
				res = (m_data[0] & 0xff);
				}
			else
				{
				res = (m_data[0] & 0xf0);
				}
			}
		
		return res;
		}


int
MidiEvent::channel() const
		{
		int		res=0;

		if ((m_data[0] & 0x80) != 0 && (m_data[0] & 0xf0) != 0xf0)
			{
			res = (m_data[0] & 0xf) + 1;
			}
		
		return res;
		}



sw_byte_t
MidiEvent::status() const
		{
		return m_data[0];
		}


sw_byte_t
MidiEvent::param(int idx) const
		{
		sw_byte_t	res=0;

		if (idx >= 0 && idx <= 2) res = m_data[idx+1];

		return res;
		}


SWString
MidiEvent::toString()
		{
		SWString	res;

		switch (type())
			{
			case NoteOff:				res.format("NoteOff               channel=%d note=%d velocity=%d",		channel(), m_data[1], m_data[2]);	break;
			case NoteOn:				res.format("NoteOn                channel=%d note=%d, velocity=%d",		channel(), m_data[1], m_data[2]);	break;
			case PolyphonicKeyPressure:	res.format("PolyphonicKeyPressure channel=%d note=%d, value=%d",		channel(), m_data[1], m_data[2]);	break;
			case ControlChange:			res.format("ControlChange         channel=%d controller=%d, value=%d",	channel(), m_data[1], m_data[2]);	break;
			case ProgramChange:			res.format("ProgramChange         channel=%d patch=%d",					channel(), m_data[1]);				break;
			case ChannelPressure:		res.format("ChannelPressure       channel=%d value=%d",					channel(), m_data[1]);				break;
			case PitchBend:				res.format("PitchBend             channel=%d low=%d, high=%d",			channel(), m_data[1], m_data[2]);	break;
			case SystemExclusive:		res = "SystemExclusive";														break;
			case TimeCodeQuarterFrame:	res = "TimeCodeQuarterFrame";													break;
			case SongPositionPointer:	res = "SongPositionPointer";													break;
			case SongSelect:			res.format("SongSelect            song=%d",					m_data[1]);			break;
			case Reserved1:				res = "Reserved1";																break;
			case Reserved2:				res = "Reserved2";																break;
			case TuneRequest:			res = "TuneRequest";															break;
			case EndOfExclusive:		res = "EndOfExclusive";															break;
			case TimingClock:			res = "TimingClock";															break;
			case Reserved3:				res = "Reserved3";																break;
			case SequenceStart:			res = "SequenceStart";															break;
			case SequenceContinue:		res = "SequenceContinue";														break;
			case SequenceStop:			res = "SequenceStop";															break;
			case Reserved4:				res = "Reserved4";																break;
			case ActiveSensing:			res = "ActiveSensing";															break;
			case Reset:					res = "Reset";																	break;
			default:					res.format("Type %02X", type() & 0xff);											break;
			}
					
		return res;
		}
