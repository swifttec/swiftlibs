/**
*** @file		MidiInput.h
*** @brief		A class to represent a MIDI input device
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWInt32 class.
***
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __midi_MidiInput_h__
#define __midi_MidiInput_h__

#include <midi/MidiEvent.h>

#include <xplatform/sw_status.h>

#include <swift/SWCircularBuffer.h>

class MIDI_DLL_EXPORT MidiInput : public SWLockableObject
		{
public:
		MidiInput();
		~MidiInput();

		sw_status_t		open(const SWString &devname);
		sw_status_t		close();

		bool			hasEvent();
		bool			readEvent(MidiEvent &data);
		
		// Used by callback
		bool			addEvent(const MidiEvent &v);

public:
		static int		getDeviceCount();
		static SWString	getDeviceName(int i);

protected:
		SWList<MidiEvent>	m_data;
		sw_handle_t			m_hMidiDevice;
		};


#endif // __midi_MidiInput_h__
