#include "stdafx.h"

#include "MidiInput.h"

#include <config/targetver.h>

#include <windows.h>
#include <mmsystem.h>

#ifdef SW_PLATFORM_WINDOWS

static void CALLBACK myMidiInProc(
   HMIDIIN   hMidiIn,
   UINT      wMsg,
   DWORD_PTR dwInstance,
   DWORD_PTR dwParam1,
   DWORD_PTR dwParam2)
		{
		MidiInput	*pMidiInput=(MidiInput *)dwInstance;


		switch (wMsg)
			{
			case MM_MIM_DATA:
				{
				MidiEvent	mevent((sw_byte_t *)&dwParam1);

				if (mevent.isValid()) pMidiInput->addEvent(mevent);
				}
				break;

/*
			default:
				printf("myMidiInProc - wMsg = %d (0x%x)\n", wMsg, wMsg);
				break;
*/
			}
		}


#endif


MidiInput::MidiInput() :
	m_hMidiDevice(0)
		{
		synchronize();
		}


MidiInput::~MidiInput()
		{
		}


sw_status_t
MidiInput::open(const SWString &devname)
		{
		SWGuard			guard(this);
		sw_status_t		res=SW_STATUS_SUCCESS;

#ifdef SW_PLATFORM_WINDOWS
		int			devid=-1;
		int			ndevs=(int)midiInGetNumDevs();
		MIDIINCAPS	di;

		for (int i=0;i<ndevs;i++)
			{
			memset(&di, 0, sizeof(di));
			midiInGetDevCaps(i, &di, sizeof(di));

//			printf("%d = %ls\n", i, di.szPname);

			if (devname == di.szPname)
				{
				devid = i;
				break;
				}
			}

		if (devid < 0) devid = devname.toInt32(10);

		HMIDIIN		h;
		MMRESULT	mmr=midiInOpen(&h, devid, (DWORD_PTR)myMidiInProc, (DWORD_PTR)this, CALLBACK_FUNCTION);

		if (mmr == MMSYSERR_NOERROR)
			{
			m_hMidiDevice = h;
			midiInStart(h);
			}
		else
			{
			res = mmr;
			}

#else // SW_PLATFORM_WINDOWS
		res = SW_STATUS_ILLEGAL_OPERATION;
#endif // SW_PLATFORM_WINDOWS

		return res;
		}


sw_status_t
MidiInput::close()
		{
		SWGuard			guard(this);
		sw_status_t		res=SW_STATUS_SUCCESS;

#ifdef SW_PLATFORM_WINDOWS
		if (m_hMidiDevice != 0)
			{
			HMIDIIN		h=(HMIDIIN)m_hMidiDevice;

			midiInStop(h);
			midiInClose(h);
			m_hMidiDevice = 0;
			}
#else
		res = SW_STATUS_ILLEGAL_OPERATION;
#endif
		return res;
		}

int
MidiInput::getDeviceCount()
		{
		int	res=0;
		
		res = (int)midiInGetNumDevs();

		return res;
		}

SWString
MidiInput::getDeviceName(int i)
		{
		SWString	res;
		MIDIINCAPS	di;

		memset(&di, 0, sizeof(di));
		if (midiInGetDevCaps(i, &di, sizeof(di)) == 0) res = di.szPname;

		return res;
		}


bool
MidiInput::hasEvent()
		{
		SWGuard	guard(this);

		return (m_data.size() > 0);
		}


bool
MidiInput::readEvent(MidiEvent &data)
		{
		SWGuard	guard(this);
		bool	res=hasEvent();

		if (res)
			{
			m_data.removeHead(&data);
			}

		return res;
		}



bool
MidiInput::addEvent(const MidiEvent &v)
		{
		SWGuard	guard(this);

		m_data.addTail(v);
		
		return true;
		}
