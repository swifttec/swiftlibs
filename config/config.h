/**
*** @file
*** @brief		Local defines to determine support
*** @version	1.0
*** @author		Simon Sparkes
***
*** This file is used to define config of the SwiftTec libraries and what
*** is and is not supported. It consists only of #defines or #undefs to determine
*** what features are supported by the swift libraries
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#ifndef __config_config_h__
#define __config_config_h__

// define this to include MySQL support, undef to exclude it
#define SW_CONFIG_USE_MYSQL

// define this to override the shared memory file prefix under windows
// #define SW_CONFIG_WINDOWS_SHMFILE_PREFIX	"SwiftTec_SWSharedMemory"

// Set the default company name for SWAppConfig
#define SW_CONFIG_DEFAULT_COMPANY			"SwiftTec"

#endif // __config_config_h__
