/**
*** @file		slib.h
*** @brief		General header for the SLIB library.
*** @version	1.0
*** @author		Simon Sparkes
***
*** This file is the general header used by all of the components of the
*** SLIB library.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#ifndef __slib_slib_h__
#define __slib_slib_h__

#include <slib/sw_platform.h>
#include <slib/sw_build.h>
#include <slib/sw_types.h>
#include <slib/sw_status_codes.h>

#if defined(SW_PLATFORM_WINDOWS)
	// Determine the import/export type for a DLL build
	#if defined(SLIB_BUILD_DLL)
		#define SLIB_DLL_EXPORT __declspec(dllexport)
	#else
		#if defined(SW_BUILD_STATIC)
			#define SLIB_DLL_EXPORT
		#else
			#define SLIB_DLL_EXPORT __declspec(dllimport)
		#endif
	#endif // defined(SLIB_BUILD_DLL)

#else // defined(SW_PLATFORM_WINDOWS)

	// On non-windows platforms we just define this to be nothing
	#define SLIB_DLL_EXPORT

#endif // defined(SW_PLATFORM_WINDOWS)

#endif // __slib_slib_h__
