/**
*** @file		WebClient.h
*** @brief		A class for handling a web connection
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the WebClient class.
**/
/****************************************************************************
** $Date: 2012/08/07 13:09:29 $
** $Author: simon $
** $Revision: 1.2 $
*****************************************************************************
** $Log: WebClient.h,v $
** Revision 1.2  2012/08/07 13:09:29  simon
** Added http post support
**
** Revision 1.1  2012/05/18 21:40:39  simon
** Initial version
**
****************************************************************************/


#ifndef __web_WebClient_h__
#define __web_WebClient_h__

#include <web/web.h>
#include <swift/SWString.h>
#include <swift/SWThreadMutex.h>
#include <swift/SWFile.h>
#include <swift/SWLog.h>
#include <swift/SWStringAssocArray.h>
#include <swift/SWDataBuffer.h>

enum HttpPostType
		{
		HTTP_POST_REGULAR=0,
		HTTP_POST_MULTIPART,
		};


class WEB_DLL_EXPORT SWWebClient
		{
public:
		SWWebClient(SWLog *pLog=NULL);
		virtual ~SWWebClient();

		// Perform the request with output directed to the 
		sw_status_t		requestToFile(const SWString &url, const SWString &filename);
		sw_status_t		requestToString(const SWString &url, SWString &result);
		sw_status_t		requestToBuffer(const SWString &url, SWDataBuffer &result);

		void			addHttpHeader(const SWString &param, const SWString &value);
		void			resetHttpHeaders();

		void			setHttpPostType(int type);
		void			addHttpPost(const SWString &param, const SWString &value);
		void			resetHttpPost();

		SWString		getResponseHeader() const	{ return m_responseHeader; }

		void			setLogHandler(SWLog *pLog);

public:
		// Handle a callback from CURL
		size_t			handleDataCallback(char *ptr, size_t size, size_t nmemb);
		size_t			handleHeaderCallback(char *ptr, size_t size, size_t nmemb);

public:
		static SWString	fixPostData(const SWString &text);

protected:
		sw_status_t		doRequest(const SWString &url);

protected:
		SWThreadMutex		m_mutex;
		void				*m_pCurl;
		int					m_mode;
		SWLog				*m_pLog;

		// Custom Headers
		SWStringAssocArray	m_httpHeader;
		SWStringAssocArray	m_httpPost;
		SWString			m_httpPostData;
		int					m_httpPostType;

		// Output
		SWString			m_responseHeader;
		SWString			m_outputString;
		SWFile				m_outputFile;
		SWDataBuffer		*m_pBuf;
		};


#endif
