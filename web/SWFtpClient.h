/**
*** @file		SWFtpClient.h
*** @brief		A class for handling a web connection
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWFtpClient class.
**/
/****************************************************************************
** $Date: 2012/08/07 13:09:29 $
** $Author: simon $
** $Revision: 1.2 $
*****************************************************************************
** $Log: SWFtpClient.h,v $
** Revision 1.2  2012/08/07 13:09:29  simon
** Added http post support
**
** Revision 1.1  2012/05/18 21:40:39  simon
** Initial version
**
****************************************************************************/


#ifndef __web_SWFtpClient_h__
#define __web_SWFtpClient_h__

#include <web/web.h>

#include <swift/SWString.h>
#include <swift/SWThreadMutex.h>
#include <swift/SWFile.h>
#include <swift/SWLog.h>
#include <swift/SWStringAssocArray.h>

// Forward declarations
class ftplib;

class WEB_DLL_EXPORT SWFtpClient
		{
public:
		enum TransferMode
			{
			ascii = 'A',
			image = 'I'
			};

public:
		SWFtpClient(SWLog *pLog=NULL);
		virtual ~SWFtpClient();

		sw_status_t		connect(const SWString &host);
		sw_status_t		disconnect();
		bool			isConnected();

		sw_status_t		login(const SWString &user, const SWString &pass);

		sw_status_t		folderCreate(const SWString &path);
		sw_status_t		folderRemove(const SWString &path);

		sw_status_t		fileGet(const SWString &localfile, const SWString &remotepath, TransferMode mode, sw_int64_t offset=0);
		sw_status_t		filePut(const SWString &localfile, const SWString &remotepath, TransferMode mode, sw_int64_t offset=0);
		sw_status_t		fileRemove(const SWString &path);

		/*
		sw_status_t		site(const SWString &cmd);
		sw_status_t		raw(const SWString &cmd);
		sw_status_t		sysType(SWString &buf, int max);
		sw_status_t		mkdir(const SWString &path);
		sw_status_t		chdir(const SWString &path);
		sw_status_t		cdup();
		sw_status_t		pwd(SWString &path, int max);
		sw_status_t		nlst(const SWString &outputfile, const SWString &path);
		sw_status_t		dir(const SWString &outputfile, const SWString &path);
		sw_status_t		size(const SWString &path, int *size, transfermode mode);
		sw_status_t		modDate(const SWString &path, SWString &dt, int max);
		sw_status_t		rename(const SWString &src, const SWString &dst);
		*/

		void			setLogHandler(SWLog *pLog);

public:
		// Handle a callback from CURL
		size_t			handleDataCallback(char *ptr, size_t size, size_t nmemb);
		size_t			handleHeaderCallback(char *ptr, size_t size, size_t nmemb);

public:
		static SWString	fixPostData(const SWString &text);

protected:
		sw_status_t		doRequest(const SWString &url);

protected:
		SWThreadMutex		m_mutex;
		SWLog				*m_pLog;
		ftplib				*m_pFtpHandle;
		};

#endif
