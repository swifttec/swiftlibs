/**
*** @file		SWWebClient.cpp
*** @brief		A class for handling a web connection
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWWebClient class.
**/
/****************************************************************************
** $Date: 2012/08/07 13:09:29 $
** $Author: simon $
** $Revision: 1.2 $
*****************************************************************************
** $Log: SWWebClient.cpp,v $
** Revision 1.2  2012/08/07 13:09:29  simon
** Added http post support
**
** Revision 1.1  2012/05/18 21:40:39  simon
** Initial version
**
****************************************************************************/

#include "stdafx.h"

#include <swift/SWGuard.h>
#include <web/SWWebClient.h>
#include <curl/curl.h>


static size_t SWWebClientDataCallback( char *ptr, size_t size, size_t nmemb, void *userdata)
		{
		SWWebClient	*pClient=(SWWebClient *)userdata;
		size_t		res=0;

		if (pClient != NULL)
			{
			res = pClient->handleDataCallback(ptr, size, nmemb);
			}

		return res;
		}

static size_t SWWebClientHeaderCallback( char *ptr, size_t size, size_t nmemb, void *userdata)
		{
		SWWebClient	*pClient=(SWWebClient *)userdata;
		size_t		res=0;

		if (pClient != NULL)
			{
			res = pClient->handleHeaderCallback(ptr, size, nmemb);
			}

		return res;
		}

enum SWWebClientMode
		{
		MODE_NONE=0,
		MODE_STRING,
		MODE_FILE,
		MODE_BUFFER
		};

SWWebClient::SWWebClient(SWLog *pLog) :
	m_mutex(true),
	m_mode(MODE_NONE),
	m_pLog(pLog),
	m_httpPostType(HTTP_POST_MULTIPART)
		{
		m_pCurl = curl_easy_init();
		curl_easy_setopt(m_pCurl, CURLOPT_WRITEFUNCTION, SWWebClientDataCallback);
		curl_easy_setopt(m_pCurl, CURLOPT_WRITEDATA, this);
		curl_easy_setopt(m_pCurl, CURLOPT_HEADERFUNCTION, SWWebClientHeaderCallback);
		curl_easy_setopt(m_pCurl, CURLOPT_HEADERDATA, this);
		curl_easy_setopt(m_pCurl, CURLOPT_SSL_VERIFYPEER, 0L);
		curl_easy_setopt(m_pCurl, CURLOPT_SSL_VERIFYHOST, 0L);
		}


SWWebClient::~SWWebClient(void)
		{
		curl_easy_cleanup(m_pCurl);
		}


void
SWWebClient::setLogHandler(SWLog *pLog)
		{
		SWMutexGuard	guard(m_mutex);
		
		m_pLog = pLog;
		}


size_t
SWWebClient::handleDataCallback(char *ptr, size_t size, size_t nmemb)
		{
		SWMutexGuard	guard(m_mutex);
		size_t			res=0;
		size_t			nbytes=size * nmemb;

		if (m_pLog != NULL) m_pLog->debug("SWWebClient::handleDataCallback - mode=%d  ptr=%x  size=%d  nmemb=%d", m_mode, ptr, size, nmemb);

		switch (m_mode)
			{
			case MODE_STRING:
				{
				SWString	s(ptr, nbytes);

				m_outputString += s;
				res = nbytes;
				}
				break;

			case MODE_FILE:
				if (nbytes) m_outputFile.write(ptr, nbytes);
				res = nbytes;
				break;

			case MODE_BUFFER:
				if (nbytes) m_pBuf->write(ptr, nbytes, m_pBuf->size());
				res = nbytes;
				break;
			}

		if (m_pLog != NULL) m_pLog->debug("SWWebClient::handleDataCallback - returning %d", res);

		return res;
		}


size_t
SWWebClient::handleHeaderCallback(char *ptr, size_t size, size_t nmemb)
		{
		SWMutexGuard	guard(m_mutex);
		size_t			res=0;
		size_t			nbytes=size * nmemb;

		if (m_pLog != NULL) m_pLog->debug("SWWebClient::handleHeaderCallback - mode=%d  size=%d  nmemb=%d", m_mode, size, nmemb);

		SWString	s(ptr, nbytes);

		m_responseHeader += s;
		res = nbytes;

		return res;
		}


sw_status_t
SWWebClient::requestToFile(const SWString &url, const SWString &filename)
		{
		SWMutexGuard	guard(m_mutex);
		sw_status_t		res=SW_STATUS_SUCCESS;

		m_mode = MODE_FILE;
		res = m_outputFile.open(filename, SWFile::ModeWriteOnly|SWFile::ModeCreate|SWFile::ModeTruncate|SWFile::ModeBinary);
		if (res == SW_STATUS_SUCCESS)
			{
			res = doRequest(url);
			m_outputFile.close();
			}

		m_mode = MODE_NONE;

		return res;
		}


sw_status_t
SWWebClient::requestToString(const SWString &url, SWString &output)
		{
		SWMutexGuard	guard(m_mutex);
		sw_status_t		res=SW_STATUS_SUCCESS;

		m_mode = MODE_STRING;
		m_outputString.empty();

		res = doRequest(url);

		if (res == SW_STATUS_SUCCESS) output = m_outputString;
		m_outputString.empty();

		return res;
		}


sw_status_t
SWWebClient::requestToBuffer(const SWString &url, SWDataBuffer &output)
		{
		SWMutexGuard	guard(m_mutex);
		sw_status_t		res=SW_STATUS_SUCCESS;

		m_mode = MODE_BUFFER;
		m_pBuf = &output;
		output.clear();

		res = doRequest(url);

		if (res != SW_STATUS_SUCCESS) output.clear();

		return res;
		}


SWString
SWWebClient::fixPostData(const SWString &text)
		{
		SWString	res;
		int			c;
		int			len=(int)text.length();

		for (int i=0;i<len;i++)
			{
			c = text.getCharAt(i);
			if (c <= 32 || c >= 127)
				{
				SWString	s;

				s.format("%%%02X", c&0xff);
				res += s;
				}
			else
				{
				res += c;
				}
			}

		return res;
		}


sw_status_t
SWWebClient::doRequest(const SWString &url)
		{
		SWMutexGuard			guard(m_mutex);
		sw_status_t				res=SW_STATUS_SUCCESS;
		struct curl_slist		*slist=NULL;
		struct curl_httppost	*formpost=NULL;
		struct curl_httppost	*lastpost=NULL;

		m_responseHeader.empty();

		if (m_httpPost.size())
			{
			SWIterator<SWString>	it=m_httpPost.keys();

			if (m_httpPostType == HTTP_POST_REGULAR)
				{
				m_httpPostData.empty();

				while (it.hasNext())
					{
					SWString	key=*it.next();

					if (!m_httpPostData.isEmpty())
						{
						m_httpPostData += "&";
						}

					m_httpPostData += key;
					m_httpPostData += "=";
					m_httpPostData += fixPostData(m_httpPost[key]);
					}

				curl_easy_setopt(m_pCurl, CURLOPT_POST, 1);
				curl_easy_setopt(m_pCurl, CURLOPT_HTTPPOST, 0);
				curl_easy_setopt(m_pCurl, CURLOPT_POSTFIELDS, m_httpPostData.c_str()); 
				}
			else
				{
				while (it.hasNext())
					{
					SWString	key=*it.next();

					curl_formadd(&formpost, &lastpost, CURLFORM_COPYNAME, key.c_str(), CURLFORM_COPYCONTENTS, m_httpPost[key].c_str(), CURLFORM_END);
					}

				curl_easy_setopt(m_pCurl, CURLOPT_HTTPPOST, formpost);
				}
			}

		if (m_httpHeader.size())
			{
			SWIterator<SWString>	it=m_httpHeader.keys();

			while (it.hasNext())
				{
				SWString	key=*it.next();
				SWString	entry;

				entry = key;
				entry += ": ";
				entry += m_httpHeader[key];
				slist = curl_slist_append(slist, entry.c_str());  
				}

			curl_easy_setopt(m_pCurl, CURLOPT_HTTPHEADER, slist);
			}

		curl_easy_setopt(m_pCurl, CURLOPT_URL, url.c_str());

		if (m_pLog != NULL) m_pLog->debug("SWWebClient::doRequest calling curl_easy_perform on %s", url.c_str());
		res = curl_easy_perform(m_pCurl);
		if (m_pLog != NULL) m_pLog->debug("SWWebClient::doRequest curl_easy_perform res = %d", res);
		m_mode = MODE_NONE;

		if (slist != NULL)
			{
			curl_easy_setopt(m_pCurl, CURLOPT_HTTPHEADER, NULL);
			curl_slist_free_all(slist); /* free the list again */ 
			}

		return res;
		}


void
SWWebClient::addHttpHeader(const SWString &param, const SWString &value)
		{
		m_httpHeader[param] = value;
		}


void
SWWebClient::resetHttpHeaders()
		{
		m_httpHeader.clear();
		}


void
SWWebClient::addHttpPost(const SWString &param, const SWString &value)
		{
		m_httpPost[param] = value;
		}


void
SWWebClient::resetHttpPost()
		{
		m_httpPost.clear();
		}


void
SWWebClient::setHttpPostType(int type)
		{
		m_httpPostType = type;
		m_httpPostData.empty();
		}
