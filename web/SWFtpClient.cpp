/**
*** @file		SWFtpClient.cpp
*** @brief		A class for handling a web connection
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWFtpClient class.
**/
/****************************************************************************
** $Date: 2012/08/07 13:09:29 $
** $Author: simon $
** $Revision: 1.2 $
*****************************************************************************
** $Log: SWFtpClient.cpp,v $
** Revision 1.2  2012/08/07 13:09:29  simon
** Added http post support
**
** Revision 1.1  2012/05/18 21:40:39  simon
** Initial version
**
****************************************************************************/

#include "stdafx.h"

#include <web/SWFtpClient.h>
#include <swift/SWGuard.h>

#include "ftplib.h"

SWFtpClient::SWFtpClient(SWLog *pLog) :
	m_mutex(true),
	m_pFtpHandle(NULL),
	m_pLog(pLog)
		{
		}


SWFtpClient::~SWFtpClient()
		{
		disconnect();
		}


void
SWFtpClient::setLogHandler(SWLog *pLog)
		{
		SWMutexGuard	guard(m_mutex);
		
		m_pLog = pLog;
		}


bool
SWFtpClient::isConnected()
		{
		SWMutexGuard	guard(m_mutex);
		
		return (m_pFtpHandle != NULL);
		}



sw_status_t
SWFtpClient::connect(const SWString &host)
		{
		SWMutexGuard	guard(m_mutex);
		sw_status_t		res=SW_STATUS_SUCCESS;

		if (m_pFtpHandle == NULL)
			{
			m_pFtpHandle = new ftplib();
			if (!m_pFtpHandle->Connect(host))
				{
				res = SW_STATUS_NOT_FOUND;
				}
			}
		else
			{
			res = SW_STATUS_ALREADY_OPEN;
			}

		return res;
		}


sw_status_t
SWFtpClient::disconnect()
		{
		SWMutexGuard	guard(m_mutex);
		sw_status_t		res=SW_STATUS_SUCCESS;

		if (m_pFtpHandle != NULL)
			{
			delete m_pFtpHandle;
			m_pFtpHandle = NULL;
			}

		return res;
		}


sw_status_t
SWFtpClient::login(const SWString &user, const SWString &pass)
		{
		SWMutexGuard	guard(m_mutex);
		sw_status_t		res=SW_STATUS_INVALID_HANDLE;

		if (isConnected())
			{
			int		r;

			r = m_pFtpHandle->Login(user, pass);
			if (r == 1) res = SW_STATUS_SUCCESS;
			else res = SW_STATUS_INVALID_PARAMETER;
			}

		return res;
		}


sw_status_t
SWFtpClient::folderCreate(const SWString &path)
		{
		SWMutexGuard	guard(m_mutex);
		sw_status_t		res=SW_STATUS_INVALID_HANDLE;

		if (isConnected())
			{
			int		r;

			r = m_pFtpHandle->Mkdir(path);
			if (r == 1) res = SW_STATUS_SUCCESS;
			else res = SW_STATUS_INVALID_PARAMETER;
			}

		return res;
		}


sw_status_t
SWFtpClient::folderRemove(const SWString &path)
		{
		SWMutexGuard	guard(m_mutex);
		sw_status_t		res=SW_STATUS_INVALID_HANDLE;

		if (isConnected())
			{
			int		r;

			r = m_pFtpHandle->Rmdir(path);
			if (r == 1) res = SW_STATUS_SUCCESS;
			else res = SW_STATUS_INVALID_PARAMETER;
			}

		return res;
		}


sw_status_t
SWFtpClient::fileGet(const SWString &localfile, const SWString &remotepath, TransferMode mode, sw_int64_t offset)
		{
		SWMutexGuard	guard(m_mutex);
		sw_status_t		res=SW_STATUS_INVALID_HANDLE;

		if (isConnected())
			{
			ftplib::transfermode	tmode;
			int						r;

			if (mode == ascii) tmode = ftplib::ascii;
			else tmode = ftplib::image;

			r = m_pFtpHandle->Get(localfile, remotepath, tmode, offset);
			if (r == 1) res = SW_STATUS_SUCCESS;
			else res = SW_STATUS_INVALID_PARAMETER;
			}

		return res;
		}


sw_status_t
SWFtpClient::filePut(const SWString &localfile, const SWString &remotepath, TransferMode mode, sw_int64_t offset)
		{
		SWMutexGuard	guard(m_mutex);
		sw_status_t		res=SW_STATUS_INVALID_HANDLE;

		if (isConnected())
			{
			ftplib::transfermode	tmode;
			int						r;

			if (mode == ascii) tmode = ftplib::ascii;
			else tmode = ftplib::image;

			r = m_pFtpHandle->Put(localfile, remotepath, tmode, offset);
			if (r == 1) res = SW_STATUS_SUCCESS;
			else res = SW_STATUS_INVALID_PARAMETER;
			}

		return res;
		}


sw_status_t
SWFtpClient::fileRemove(const SWString &path)
		{
		SWMutexGuard	guard(m_mutex);
		sw_status_t		res=SW_STATUS_INVALID_HANDLE;

		if (isConnected())
			{
			int						r;

			r = m_pFtpHandle->Delete(path);
			if (r == 1) res = SW_STATUS_SUCCESS;
			else res = SW_STATUS_INVALID_PARAMETER;
			}

		return res;
		}
