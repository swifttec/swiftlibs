#ifndef __LIBSNMP_H_INCLUDED__
#define __LIBSNMP_H_INCLUDED__

#include <snmp_pp/snmp_pp_defs.h>
#include <swift/SWString.h>

typedef SWString	SNMPString;

#include <stdint.h>

#if !defined(SW_PLATFORM_WINDOWS)
	#include <ctype.h>
#endif

#endif // __LIBSNMP_H_INCLUDED__
