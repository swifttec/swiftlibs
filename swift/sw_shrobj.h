/**
*** @file		sw_shrobj.h
*** @brief		Functions for maintaining a global (shared) object list.
*** @version	1.0
*** @author		Simon Sparkes
***
*** This module is intended for internal use for simulating named objects
*** on platforms where they are not available.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#ifndef __sw_shrobj_h__
#define __sw_shrobj_h__





#define SW_SHROBJ_TYPE_PROCESS_MUTEX		0x0001
#define SW_SHROBJ_TYPE_PROCESS_SEMAPHORE	0x0002
#define SW_SHROBJ_TYPE_PROCESS_RWLOCK		0x0003


/**
*** @brief	Create a new shared object.
**/
SWIFT_DLL_EXPORT sw_status_t	sw_shrobj_create(const sw_tchar_t *name, sw_uint16_t type, sw_uint16_t size, sw_handle_t &hobj);

/**
*** @brief	Attach to an existing shared object.
**/
SWIFT_DLL_EXPORT sw_status_t	sw_shrobj_attach(const sw_tchar_t *name, sw_uint16_t type, sw_handle_t &hobj);

/**
*** @brief	Detach from a shared object.
**/
SWIFT_DLL_EXPORT sw_status_t	sw_shrobj_detach(sw_handle_t hobj, bool autodelete=true);

/**
*** @brief	Delete a shared object.
**/
SWIFT_DLL_EXPORT sw_status_t	sw_shrobj_delete(const sw_tchar_t *name);

/**
*** @brief	Get a pointer to the data part of the shared object.
**/
SWIFT_DLL_EXPORT void *			sw_shrobj_data(sw_handle_t hobj);

/**
*** @brief	Get a pointer to the data part of the shared object.
**/
SWIFT_DLL_EXPORT sw_tchar_t *	sw_shrobj_name(sw_handle_t hobj);

/**
*** @brief	Get the length of the data part of the shared object.
**/
SWIFT_DLL_EXPORT sw_uint16_t	sw_shrobj_datalen(sw_handle_t hobj);

/**
*** @brief	Get a data type of the shared object.
**/
SWIFT_DLL_EXPORT sw_uint16_t	sw_shrobj_datatype(sw_handle_t hobj);

/**
*** @brief	Lock the shared object database.
**/
SWIFT_DLL_EXPORT void			sw_shrobj_lock();

/**
*** @brief	Unlock the shared object database.
**/
SWIFT_DLL_EXPORT void			sw_shrobj_unlock();


/**
*** @brief	Dump the shared object database to stdout.
**/
SWIFT_DLL_EXPORT void			sw_shrobj_dump();





#endif // __SWbase_shrobj_h__
