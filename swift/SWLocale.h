/**
*** @file		SWLocale.h
*** @brief		A class representing locale information
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWLocale class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#ifndef __SWLocale_h__
#define __SWLocale_h__

#include <swift/swift.h>
#include <swift/SWObject.h>
#include <swift/SWString.h>
#include <swift/SWArray.h>


/**
*** @brief	A class representing locale information.
***
*** Ths class represents locale information in a platform independent manner.
*** Currently the only information held by this is class is related to
*** times and timezones. However this will be expanded to include other areas
*** as needed by the SWIFT.
**/
class SWIFT_DLL_EXPORT SWLocale : public SWObject
		{
public:
		enum FormatType
			{
			FMT_SWDate,
			FMT_SWTime
			};

public:
		/// Default constructor
		SWLocale();

		/// Virtual destructor
		virtual ~SWLocale();

		/// Copy constructor
		SWLocale(const SWLocale &other);

		/// Assignment operator
		SWLocale &	operator=(const SWLocale &other);

		/// Test to see if this locale object is valid
		bool				isValid() const			{ return m_valid; }

		// Load the locale for the given LCID (a number)
		sw_status_t			load(sw_uint32_t lcid);
		
		// Load the locale for the given LCID string (in the form en or en-gb)
		sw_status_t			load(const SWString &lcname);


		sw_uint32_t			id() const				{ return m_lcid; }
		const SWString &	languageName() const	{ return m_langName; }
		const SWString &	languageCode() const	{ return m_langCode; }
		const SWString &	sublanguageName() const	{ return m_sublangName; }
		const SWString &	sublanguageCode() const	{ return m_sublangCode; }

		SWString 			name() const;
		SWString 			code() const;

		/**
		*** Return the name for the given day in abbreviated or full format
		***
		*** @param	day				The day number (0-6) where 0=Sunday
		*** @param	abbreviated		If true the abbreviated form of the day is returned (e.g. Sun).
		***							If false the full form of the day is returned (e.g. Sunday).
		**/
		SWString			dayName(int day, bool abbreviated) const;

		/**
		*** Return the name for the given month in abbreviated or full format
		***
		*** @param	month			The month number (0-11) where 0=January
		*** @param	abbreviated		If true the abbreviated form of the day is returned (e.g. Jan).
		***							If false the full form of the day is returned (e.g. January).
		**/
		SWString			monthName(int month, bool abbreviated) const;


		/// Return the separator used for dates (e.g. / for dd/mm/yyyy)
		const SWString &	dateSeparator() const			{ return m_dateSeparator; }

		/// Return the separator used for times (e.g. : for hh:mm:ss)
		const SWString &	timeSeparator() const			{ return m_timeSeparator; }

		/// Return the string for AM in upper or lower case.
		const SWString &	amString(bool uppercase) const	{ return uppercase?m_amStringUpper:m_amStringLower; }

		/// Return the string for PM in upper or lower case.
		const SWString &	pmString(bool uppercase) const	{ return uppercase?m_pmStringUpper:m_pmStringLower; }

		/// Return the string of possible boolean chars for the given boolean value.
		const SWString &	booleanChars(bool v) const		{ return v?m_trueChars:m_falseChars; }

		/// Return the string representing the given boolean value.
		const SWString &	booleanString(bool v) const		{ return v?m_trueString:m_falseString; }

		/// Return the currency symbol
		const SWString &	currencySymbol() const			{ return m_currencySymbol; }

		/// Get the date format string (overrides locale)
		SWString			dateFormat(FormatType style=FMT_SWDate) const;

		/// Get the time format string (overrides locale)
		SWString			timeFormat(FormatType style=FMT_SWDate) const;

		/// Get the timestamp format string (overrides locale)
		SWString			timestampFormat(FormatType style=FMT_SWDate) const;

public: // Static methods

		/// Get a reference to the current locale
		static SWLocale &	getCurrentLocale();

		/// Get a reference to the default user locale
		static SWLocale &	getUserLocale();

		/// Get a reference to the default system locale
		static SWLocale &	getSystemLocale();

private: // Static Members
		static SWLocale		sm_currlocale;		///< The current locale
		static SWLocale		sm_userlocale;		///< The user locale
		static SWLocale		sm_systemlocale;	///< The system locale

private: // Members
		bool				m_valid;				///< A flag to indicate if this is a valid Locale object
		sw_uint32_t			m_lcid;					///< The locale identifier
		SWString			m_langName;				///< The language name (e.g. English)
		SWString			m_langCode;				///< The language code (e.g. en)
		SWString			m_sublangName;			///< The sub-language name (e.g. British)
		SWString			m_sublangCode;			///< The sub-language code (e.g. gb)
		SWArray<SWString>	m_shortDayName;			///< An array of day names
		SWArray<SWString>	m_fullDayName;			///< An array of day names
		SWArray<SWString>	m_shortMonthName;		///< An array of month names
		SWArray<SWString>	m_fullMonthName;		///< An array of month names
		SWString			m_dateSeparator;		///< The separator for dates
		SWString			m_dateFormat;			///< The format for dates
		SWString			m_timeSeparator;		///< The separator for times
		SWString			m_timeFormat;			///< The format for times
		SWString			m_amStringUpper;		///< The string for "AM"
		SWString			m_amStringLower;		///< The string for "am"
		SWString			m_pmStringUpper;		///< The string for "PM"
		SWString			m_pmStringLower;		///< The string for "pm"
		SWString			m_trueChars;			///< The set of characters that represent true (e.g. TtYy or perhaps Jj for german jah)
		SWString			m_falseChars;			///< The set of characters that represent false
		SWString			m_trueString;			///< The word for true
		SWString			m_falseString;			///< The word for false
		SWString			m_currencySymbol;		///< The currency symbol
		};




#endif
