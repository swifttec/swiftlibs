/**
*** A collection of internal functions
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <swift/sw_registry.h>
#include <swift/SWFilename.h>


#if defined(SW_PLATFORM_WINDOWS)
static SWRegistryHive
hkeyToHive(HKEY hk)
		{
		SWRegistryHive	hive;

		if (hk == HKEY_CLASSES_ROOT)
			{
			hive = SW_REGISTRY_CLASSES_ROOT;
			}
		else if (hk == HKEY_CURRENT_USER)
			{
			hive = SW_REGISTRY_CURRENT_USER;
			}
		else if (hk == HKEY_LOCAL_MACHINE)
			{
			hive = SW_REGISTRY_LOCAL_MACHINE;
			}
		else
			{
			throw SW_EXCEPTION(SWIllegalArgumentException);
			}

		return hive;
		}


SWIFT_DLL_EXPORT SWString
sw_registry_getString(HKEY hk, const SWString &key, const SWString &param, const SWString &defval)
		{
		return sw_registry_getString(hkeyToHive(hk), key, param, defval);
		}


SWIFT_DLL_EXPORT int
sw_registry_getInteger(HKEY hk, const SWString &key, const SWString &param, int defval)
		{
		return sw_registry_getInteger(hkeyToHive(hk), key, param, defval);
		}


SWIFT_DLL_EXPORT double
sw_registry_getDouble(HKEY hk, const SWString &key, const SWString &param, double defval)
		{
		return sw_registry_getDouble(hkeyToHive(hk), key, param, defval);
		}


SWIFT_DLL_EXPORT sw_status_t
sw_registry_setString(HKEY hk, const SWString &key, const SWString &param, const SWString &val)
		{
		return sw_registry_setString(hkeyToHive(hk), key, param, val);
		}


SWIFT_DLL_EXPORT sw_status_t
sw_registry_setInteger(HKEY hk, const SWString &key, const SWString &param, int val)
		{
		return sw_registry_setInteger(hkeyToHive(hk), key, param, val);
		}


SWIFT_DLL_EXPORT sw_status_t
sw_registry_setDouble(HKEY hk, const SWString &key, const SWString &param, double val)
		{
		return sw_registry_setDouble(hkeyToHive(hk), key, param, val);
		}


SWIFT_DLL_EXPORT bool
sw_registry_isKey(HKEY hk, const SWString &key)
		{
		return sw_registry_isKey(hkeyToHive(hk), key);
		}


SWIFT_DLL_EXPORT bool
sw_registry_isValue(HKEY hk, const SWString &key, const SWString &param)
		{
		return sw_registry_isValue(hkeyToHive(hk), key, param);
		}


SWIFT_DLL_EXPORT sw_status_t
sw_registry_createKey(HKEY hk, const SWString &key)
		{
		return sw_registry_createKey(hkeyToHive(hk), key);
		}


SWIFT_DLL_EXPORT sw_status_t
sw_registry_removeKey(HKEY hk, const SWString &key)
		{
		return sw_registry_removeKey(hkeyToHive(hk), key);
		}


SWIFT_DLL_EXPORT sw_status_t
sw_registry_removeValue(HKEY hk, const SWString &key, const SWString &param)
		{
		return sw_registry_removeValue(hkeyToHive(hk), key, param);
		}


SWIFT_DLL_EXPORT sw_status_t
sw_registry_renameKey(HKEY hk, const SWString &oldkey, const SWString &newkey)
		{
		return sw_registry_renameKey(hkeyToHive(hk), oldkey, newkey);
		}


SWIFT_DLL_EXPORT sw_status_t
sw_registry_renameValue(HKEY hk, const SWString &key, const SWString &oldparam, const SWString &newparam)
		{
		return sw_registry_renameValue(hkeyToHive(hk), key, oldparam, newparam);
		}
#endif // defined(SW_PLATFORM_WINDOWS)





SWIFT_DLL_EXPORT SWString
sw_registry_getString(SWRegistryHive pdk, const SWString &key, const SWString &param, const SWString &defval)
		{
		SWString		res=defval;
		SWRegistryKey	rk(pdk, key, false);
		SWRegistryValue	rv;

		if (rk.getValue(param, rv) == SW_STATUS_SUCCESS)
			{
			res = rv.toString();
			}
		
		return res;
		}


SWIFT_DLL_EXPORT int
sw_registry_getInteger(SWRegistryHive pdk, const SWString &key, const SWString &param, int defval)
		{
		int				res=defval;
		SWRegistryKey	rk(pdk, key, false);
		SWRegistryValue	rv;

		if (rk.getValue(param, rv) == SW_STATUS_SUCCESS)
			{
			res = rv.toInt32();
			}
		
		return res;
		}


SWIFT_DLL_EXPORT bool
sw_registry_getBoolean(SWRegistryHive pdk, const SWString &key, const SWString &param, bool defval)
		{
		bool			res=defval;
		SWRegistryKey	rk(pdk, key, false);
		SWRegistryValue	rv;

		if (rk.getValue(param, rv) == SW_STATUS_SUCCESS)
			{
			res = rv.toBoolean();
			}
		
		return res;
		}


SWIFT_DLL_EXPORT double
sw_registry_getDouble(SWRegistryHive pdk, const SWString &key, const SWString &param, double defval)
		{
		double			res=defval;
		SWRegistryKey	rk(pdk, key, false);
		SWRegistryValue	rv;

		if (rk.getValue(param, rv) == SW_STATUS_SUCCESS)
			{
			res = rv.toDouble();
			}
		
		return res;
		}


SWIFT_DLL_EXPORT SWValue
sw_registry_getValue(SWRegistryHive pdk, const SWString &key, const SWString &param, const SWValue &defval)
		{
		SWValue			res=defval;
		SWRegistryKey	rk(pdk, key, false);
		SWRegistryValue	rv;

		if (rk.getValue(param, rv) == SW_STATUS_SUCCESS)
			{
			res = rv;
			}
		
		return res;
		}

SWIFT_DLL_EXPORT sw_status_t
sw_registry_setString(SWRegistryHive pdk, const SWString &key, const SWString &param, const SWString &val)
		{
		SWRegistryKey	rk(pdk, key, true);

		return rk.setValue(SWRegistryValue(param, val.t_str()));
		}


SWIFT_DLL_EXPORT sw_status_t
sw_registry_setInteger(SWRegistryHive pdk, const SWString &key, const SWString &param, int val)
		{
		SWRegistryKey	rk(pdk, key, true);

		return rk.setValue(SWRegistryValue(param, val));
		}


SWIFT_DLL_EXPORT sw_status_t
sw_registry_setBoolean(SWRegistryHive pdk, const SWString &key, const SWString &param, bool val)
		{
		SWRegistryKey	rk(pdk, key, true);

		return rk.setValue(SWRegistryValue(param, val));
		}


SWIFT_DLL_EXPORT sw_status_t
sw_registry_setDouble(SWRegistryHive pdk, const SWString &key, const SWString &param, double val)
		{
		SWString		vs;

		vs.format("%f", val);

		SWRegistryKey	rk(pdk, key, true);

		return rk.setValue(SWRegistryValue(param, vs.c_str()));
		}


SWIFT_DLL_EXPORT sw_status_t
sw_registry_setValue(SWRegistryHive pdk, const SWString &key, const SWString &param, const SWValue &val)
		{
		SWRegistryKey	rk(pdk, key, true);
		sw_status_t		res;

		if (val.type() == SWValue::VtFloat || val.type() == SWValue::VtDouble)
			{
			SWString		vs;
			
			vs.format("%f", val.toDouble());

			res = rk.setValue(SWRegistryValue(param, SWRegistryValue::RtString, vs.c_str()));
			}
		else
			{
			res = rk.setValue(SWRegistryValue(param, SWRegistryValue::getRegValueTypeFromValueType(val.type()), val));
			}

		return res;
		}

SWIFT_DLL_EXPORT bool
sw_registry_isKey(SWRegistryHive pdk, const SWString &key)
		{
		SWRegistryKey	rk(pdk, key, false);

		return rk.isOpen();
		}


SWIFT_DLL_EXPORT bool
sw_registry_isValue(SWRegistryHive pdk, const SWString &key, const SWString &param)
		{
		SWRegistryKey	rk(pdk, key, false);

		return rk.valueExists(param);
		}


SWIFT_DLL_EXPORT sw_status_t
sw_registry_createKey(SWRegistryHive pdk, const SWString &key)
		{
		SWRegistryKey	rk;
		
		return rk.open(pdk, key, true);
		}


SWIFT_DLL_EXPORT sw_status_t
sw_registry_removeKey(SWRegistryHive pdk, const SWString &key)
		{
		SWRegistryKey	rk(pdk, SWFilename::dirname(key), false);

		return rk.deleteKey(SWFilename::basename(key));
		}


SWIFT_DLL_EXPORT sw_status_t
sw_registry_removeValue(SWRegistryHive pdk, const SWString &key, const SWString &param)
		{
		SWRegistryKey	rk(pdk, key, false);

		return rk.deleteValue(param);
		}


SWIFT_DLL_EXPORT sw_status_t
sw_registry_renameKey(SWRegistryHive pdk, const SWString &oldkey, const SWString &newkey)
		{
		return SW_STATUS_UNSUPPORTED_OPERATION;
		}


SWIFT_DLL_EXPORT sw_status_t
sw_registry_renameValue(SWRegistryHive pdk, const SWString &key, const SWString &oldparam, const SWString &newparam)
		{
		SWRegistryKey	rk(pdk, key, false);

		return rk.renameValue(oldparam, newparam);
		}
