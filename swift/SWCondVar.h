/**
*** @file		SWCondVar.h
*** @brief		A class to deal with conditional variables for thread synchronisation.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWCondVar class.
***
*** This module contains a class to deal with conditional variables for thread synchronisation.
*** Condional Variables are those on which a thread/process can wait and be woken up (signalled)
*** by another thread/process.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWCondVar_h__
#define __SWCondVar_h__

#include <swift/SWObject.h>
#include <swift/SWException.h>
#include <swift/SWMutex.h>

#include <xplatform/sw_condvar.h>




/**
*** @brief A condition variable implementation which allows a thread to block until shared data changes.
***
*** A condition variable enables threads to atomically block and test the condition under the
*** protection of a mutual exclusion lock (mutex) until the condition is satisfied.
*** That is, the mutex must have been held by the thread before calling wait or signal on the condition.
*** If the condition is false, a thread blocks on a condition variable and atomically releases
*** the mutex that is waiting for the condition to change. If another thread changes the condition,
*** it may wake up waiting threads by signaling the associated condition variable.
*** The waiting threads, upon awakening, reacquire the mutex and re-evaluate the condition. 
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Safe</td>
*** </tr>
*** </table>
***
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWCondVar
		{
protected:
		SWCondVar(int type, sw_mutex_t hMutex);

public:
		virtual ~SWCondVar();

		/**
		*** Wait for a condition to occur on our variable
		***
		*** @param	waitms	The time in millisecons to wait before timing out.
		***
		*** @return		SW_STATUS_SUCCESS if successfully acquired
		***				SW_STATUS_TIMEOUT if the operation timed-out
		***				or an error code indicating the error.
		**/
		virtual sw_status_t	wait(sw_uint64_t waitms=SW_TIMEOUT_INFINITE);

		/// Signal the condition variable (wakeup one waiting thread only)
		virtual void		signal();

		/// Broadcast to the condition variable (wakeup all waiting threads)
		virtual void		broadcast();

		/// Close the condition variable (causes any waiters to get an exception)
		virtual void		close();

		/// Lock/Acquire the mutex associated with this condition variable
		void				acquire();

		/// Unlock/Release the mutex associated with this condition variable
		void				release();

		/// Check to see if the mutex associated with this condition variable
		/// is already locked.
		bool				hasLock();

		/// Return a pointer to the mutex used for thread locking
		// SWMutex *			getMutex() const	{ return m_pMutex; }

protected:
		sw_condvar_t	m_hcv;		///< A handle to the underlying condvar
		// SWMutex		*m_pMutex;	///< A pointer the mutex associated with this condvar
		// bool		m_ownMutex;	///< A flag indicating if we own the mutex

private:
		// void		checkState(bool dorelease);		// Check our internal state
		};

SW_DEFINE_EXCEPTION(SWCondVarException, SWException);					///< Generic SWCondVar exception
SW_DEFINE_EXCEPTION(SWCondVarInactiveException, SWCondVarException);	///< Operation attempted on inactive/disabled queue




#endif
