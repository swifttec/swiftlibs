/**
*** @file		SWPair.h
*** @brief		Templated data pair.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWPair class.
**/

/*********************************************************************************
Copyright (c) 2006-2016 SwiftTec Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWPair_h__
#define __SWPair_h__

#include <swift/SWObject.h>

SWIFT_NAMESPACE_BEGIN

/**
*** @brief	SWPair is a template that stores a pair of values.
***
*** SWPair is a template that stores a pair of values. It provides access to
*** the values by reference via methods first() and second() and provides tests
*** for equality and inequality via operators.
***
*** The stored data types T1 and T2 need to define the = and == operators and the 
*** copy constructor.
*** 
*** @note
*** Where ordering operators (\<, \<=, \>, \>=) are required the SWOrderedPair class should be used.
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>No built-in thread safeness, but dependent on the data types T1 and T2</td>
*** </tr>
*** </table>
***
***
*** @see		SWOrderedPair
*** @author		Simon Sparkes
**/
template<class T1, class T2>
class SWPair : public SWObject
		{
public:
		/**
		*** @brief	Construct a SWPair object.
		**/
		SWPair();


		/**
		*** @brief	Construct a SWPair object.
		**/
		SWPair(const T1 &v1, const T2 &v2);


		/**
		*** @brief	Copy constructor
		**/
		SWPair(const SWPair<T1, T2> &other);


		/**
		*** @brief	Assignment operator
		**/
		SWPair<T1, T2> &	operator=(const SWPair<T1, T2> &other);


		/**
		*** @brief	Return a reference to the first value.
		**/
		T1 &				first() const	{ return (T1 &)_value1; }


		/**
		*** @brief	Return a reference to the first value.
		**/
		T2 &				second() const	{ return (T2 &)_value2; }


		/**
		*** @brief	Check for equality
		**/
		bool				operator==(const SWPair<T1, T2> &other) const;


		/**
		*** @brief	Check for inequality
		**/
		bool				operator!=(const SWPair<T1, T2> &other) const;

private:
		T1		_value1;	///< The first value of type T1
		T2		_value2;	///< The second value of type T2
		};


SWIFT_NAMESPACE_END

// Include the implementation
#if defined(SW_TEMPLATES_REQUIRE_SOURCE)
	#include <swift/SWPair.cpp>
#elif defined(SW_TEMPLATES_REQUIRE_PRAGMA)
	#pragma implementation("swift/SWPair.cpp")
#else
	#error No implementation
#endif

#endif
