/**
*** @file		SWHardwareInfo.cpp
*** @brief		A generic value object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWHardwareInfo class.
**/

/*********************************************************************************
Copyright (c) 2006-2016 SwiftTec Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWHardwareInfo.h>
#include <swift/SWString.h>
#include <swift/SWTokenizer.h>

#if !defined(SW_PLATFORM_WINDOWS)
	#if defined(SW_PLATFORM_SOLARIS)
		#include <sys/systeminfo.h>
	#endif
	#include <sys/utsname.h>
	#include <unistd.h>
#endif


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif

SWIFT_NAMESPACE_BEGIN

SWHardwareInfo::SWHardwareInfo()
		{
		}


SWHardwareInfo::~SWHardwareInfo()
		{
		}


SWHardwareInfo::SWHardwareInfo(const SWHardwareInfo &other) :
	SWInfoObject()
		{
		*this = other;
		}


SWHardwareInfo &
SWHardwareInfo::operator=(const SWHardwareInfo &other)
		{
		m_values = other.m_values;

		return *this;
		}


sw_status_t
SWHardwareInfo::getMachineInfo(SWHardwareInfo &info)
		{
		bool		workstation=false;
		bool		server=false;
		bool		domaincontroller=false;
		int			pfamily=HW_PROCESSOR_FAMILY_UNKNOWN;
		SWString	processor, architecture, isalist, platform, hw_provider, hw_serial;

#if defined(WIN32) || defined(_WIN32)
		OSVERSIONINFOEX	ver;
		bool			ok=false;
		bool			extended=false;

		ver.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
		if (GetVersionEx((OSVERSIONINFO *)&ver))
			{
			ok = true;
			extended = true;
			}
		else
			{
			// Failed - try getting non-extended info
			ver.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
			if (GetVersionEx((OSVERSIONINFO *)&ver))
				{
				ok = true;
				extended = false;
				}
			}

		if (ok)
			{
			switch (ver.dwPlatformId)
				{
				case VER_PLATFORM_WIN32s:
				case VER_PLATFORM_WIN32_WINDOWS:
					// Win 3.1
					// Win95/98/Me
					workstation = true;
					break;

				case VER_PLATFORM_WIN32_NT:
					if (extended)
						{
						switch (ver.wProductType)
							{
							case VER_NT_WORKSTATION:
								workstation = true;
								break;

							case VER_NT_DOMAIN_CONTROLLER:
								domaincontroller = true;
								// fall into server

							case VER_NT_SERVER:
								server = true;
								break;
							}
						}
					else
						{
						/*
						XString	tmp;

						tmp = sw_registry_getString(HKEY_LOCAL_MACHINE, "SYSTEM\\CurrentControlSet\\Control\\ProductOptions", "ProductType");

						if (tmp.CompareNoCase("WINNT") == 0)
							{
							workstation = true;
							}
						else if (tmp.CompareNoCase("LANMANNT") == 0)
							{
							server = true;
							}
						else if (tmp.CompareNoCase("SERVERNT") == 0)
							{
							server = true;
							}
						*/
						}
					break;
				}
			}

#if defined(_M_ALPHA)
		pfamily = HW_PROCESSOR_FAMILY_ALPHA;
#elif defined(_M_IX86)
		pfamily = HW_PROCESSOR_FAMILY_X86;
#elif defined(_M_IA64)
		pfamily = HW_PROCESSOR_FAMILY_IA64;
#elif defined(_M_MPPC)
		pfamily = HW_PROCESSOR_FAMILY_POWERMAC;
#elif defined(_M_MRX000)
		pfamily = HW_PROCESSOR_FAMILY_MIPS;
#elif defined(_M_PPC)
		pfamily = HW_PROCESSOR_FAMILY_POWERPC;
#endif

		SYSTEM_INFO	si;

		GetSystemInfo(&si);
		switch (si.wProcessorArchitecture)
			{
			case PROCESSOR_ARCHITECTURE_UNKNOWN:
				pfamily = HW_PROCESSOR_FAMILY_UNKNOWN;
				break;

			case PROCESSOR_ARCHITECTURE_INTEL:
				pfamily = HW_PROCESSOR_FAMILY_X86;

				switch (si.wProcessorLevel)
					{
					case 3: processor = "i386";			break;
					case 4: processor = "i486";			break;
					case 5: processor = "i586";			break;
					case 6: processor = "i586pro";	break; // Pentium pro
					default: processor = "i686";			break;
					}

				break;

			case PROCESSOR_ARCHITECTURE_MIPS:
				pfamily = HW_PROCESSOR_FAMILY_MIPS;
				switch (si.wProcessorLevel)
					{
					case 4: 	processor = "MIPS_R4000";	break;
					default:	processor = "MIPS";		break;
					}
				break;

			case PROCESSOR_ARCHITECTURE_ALPHA:
				pfamily = HW_PROCESSOR_FAMILY_ALPHA;
				architecture = "alpha";
				switch (si.wProcessorLevel)
					{
					case 21064: processor = "Alpha_21064";	break;
					case 21066: processor = "Alpha_21066";	break;
					case 21164: processor = "Alpha_21164";	break;
					default:	processor = "Alpha";		break;
					}
				break;

			case PROCESSOR_ARCHITECTURE_PPC:
				pfamily = HW_PROCESSOR_FAMILY_POWERPC;
				architecture = "ppc";
				switch (si.wProcessorLevel)
					{
					case 1: processor = "PPC_601";	break;
					case 3: processor = "PPC_603";	break;
					case 4: processor = "PPC_604";	break;
					case 6: processor = "PPC_603+";	break;
					case 9: processor = "PPC_604+";	break;
					case 20: processor = "PPC_620";	break;
					default: processor = "PPC";	break;
					}
				break;

			case PROCESSOR_ARCHITECTURE_IA64:
				pfamily = HW_PROCESSOR_FAMILY_IA64;
				processor = "IA64";
				break;

			case PROCESSOR_ARCHITECTURE_IA32_ON_WIN64:
				pfamily = HW_PROCESSOR_FAMILY_X86;
				processor = "IA64";
				break;

			case PROCESSOR_ARCHITECTURE_AMD64:
				pfamily = HW_PROCESSOR_FAMILY_AMD64;
				processor = "AMD64";
				break;
			}
#else
		struct utsname	u;

		server = true;
		workstation = true;

		if (uname(&u) >= 0)
			{
			processor = u.machine;
			}

	#if defined(i386) || defined(__i386) || defined(__i386__)
		pfamily = HW_PROCESSOR_FAMILY_X86;
		architecture = "x86";
		processor = "X86";
	#elif defined(__alpha)
		pfamily = HW_PROCESSOR_FAMILY_ALPHA;
		architecture = "alpha";
		processor = "Alpha";
	#elif defined(_POWER)
		pfamily = HW_PROCESSOR_FAMILY_POWERPC;
		architecture = "ppc";
		processor = "PPC";
	#elif defined(sparc)
		pfamily = HW_PROCESSOR_FAMILY_SPARC;
		architecture = "sparc";
		processor = "sparc";
	#endif

	#if defined(SW_PLATFORM_SOLARIS)
		char	buf[1024];

		if (sysinfo(SI_MACHINE, buf, sizeof(buf)) > 0) processor = buf;
		if (sysinfo(SI_ARCHITECTURE, buf, sizeof(buf)) > 0) architecture = buf;
		if (sysinfo(SI_ISALIST, buf, sizeof(buf)) > 0) isalist = buf;
		if (sysinfo(SI_PLATFORM, buf, sizeof(buf)) > 0) platform = buf;
		if (sysinfo(SI_HW_PROVIDER, buf, sizeof(buf)) > 0) hw_provider = buf;
		if (sysinfo(SI_HW_SERIAL, buf, sizeof(buf)) > 0) hw_serial = buf;
	#endif // defined(SW_PLATFORM_SOLARIS)

#endif

		switch (pfamily)
			{
			case HW_PROCESSOR_FAMILY_ALPHA:		architecture = "alpha";		break;
			case HW_PROCESSOR_FAMILY_X86:		architecture = "x86";		break;
			case HW_PROCESSOR_FAMILY_IA64:		architecture = "ia64";		break;
			case HW_PROCESSOR_FAMILY_POWERMAC:	architecture = "PowerMac";	break;
			case HW_PROCESSOR_FAMILY_MIPS:		architecture = "MIPS";		break;
			case HW_PROCESSOR_FAMILY_POWERPC:	architecture = "PowerPC";	break;
			case HW_PROCESSOR_FAMILY_AMD64:		architecture = "amd64";		break;
			case HW_PROCESSOR_FAMILY_SPARC:		architecture = "sparc";		break;
			}

		if (processor.isEmpty()) processor = architecture;
		if (isalist.isEmpty()) isalist = processor;

		info.setValue(IsWorkstation, workstation);
		info.setValue(IsServer, server);
		info.setValue(IsDomainController, domaincontroller);

		info.setValue(ProcessorFamily, pfamily);
		info.setValue(ProcessorArchitecture, architecture);
		info.setValue(ProcessorName, processor);
		info.setValue(ProcessorInstructionSetList, isalist);
		info.setValue(Platform, platform);
		info.setValue(Provider, hw_provider);
		info.setValue(SerialNo, hw_serial);

		return SW_STATUS_SUCCESS;
		}


SWIFT_NAMESPACE_END
