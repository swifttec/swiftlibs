/**
*** @file		SWDate.h
*** @brief		A class to represent a date
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWDate class.
***
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWDate_h__
#define __SWDate_h__

#include <swift/swift.h>
#include <swift/SWObject.h>
#include <swift/SWException.h>
#include <swift/SWString.h>




// Forward declarations
class SWTime;

/**
*** @brief	A generic date class based on Julian Day Numbers.
***
***	This is a generic date class that represents dates as Julian Day Numbers.
*** This gives a enormous range of dates that are stored in a 32-bit integer value.
***
*** The class stores date information only and has no time component. However it
*** can easily be converted both to and from a SWTime value. Where both date and
*** time information are required developers should use the SWTime class.
***
*** @author		Simon Sparkes
*/
class SWIFT_DLL_EXPORT SWDate : public SWObject
		{
public:
		/// An enumerator for days
		enum DayNumber
			{
			Sunday=0,
			Monday=1,
			Tuesday=2,
			Wednesday=3,
			Thursday=4,
			Friday=5,
			Saturday=6
			};

		/// An enumerator for months
		enum MonthNumber
			{
			January=1,
			February=2,
			March=3,
			April=4,
			May=5,
			June=6,
			July=7,
			August=8,
			September=9,
			October=10,
			November=11,
			December=12
			};

		enum AdjustmentUnit
			{
			Days=0,
			Weeks=1,
			Months=2,
			Years=3
			};

public: 
		/// Construct a SWDate object a julian day number of zero.
		SWDate();

		/// Virtual destructor
		virtual ~SWDate();

		/**
		*** Construct a SWDate object with the date based on the given day, month and year.
		***
		*** @param	day					The day (1-31)
		*** @param	month				The month (1-12)
		*** @param	year				The year (1-9999)
		*** @param	throwOnInvalidDMY	If true and the date is invalid throw a SWDateInvalidException
		*** @return						true if the conversion was successful, false otherwise.
		*** @throws SWDateInvalidException
		**/
		SWDate(int day, int month, int year, bool throwOnInvalidDMY=true);

		/**
		*** Construct a date value from a string value and format.
		***
		*** The string value is parsed using the format string as a guide and the time value is set accordingly.
		*** The time value is assumed to be in GMT format and no offset is applied.
		***
		*** @param	value				The time value string. This value is interpreted according to the format parameter.
		*** @param	format				The format string which specifies how the string value is interpreted. The format is
		***								a sequence of characters built up from the following:
		***
		***								- YYYY - A 4-digit year
		***								- YY - A 2-digit year (00-99)
		***								- MM - A 2-digit month (01-12)
		***								- DD - A 2-digit day number (01-31)
		***
		***								Other characters in the format string are matched exactly, so a format of "DD/MM/YY" will
		***								only match a number sequence with slashes (i.e. 10/12/32) and not (101232).
		***
		*** @param	throwOnInvalidDMY	If true and the date is invalid throw a SWDateInvalidException
		***
		*** @return						true if the conversion was successful, false otherwise.
		***
		*** @throws SWDateInvalidException
		**/
		SWDate(const SWString &value, const SWString &format="YYYYMMDD", bool throwOnInvalidDMY=true);

		/// Copy constructor
		SWDate(const SWDate &other);

		/**
		*** @brief	Construct a SWDate object with the date based on the given time.
		***
		*** This method constructs a SWDate object using the parameters supplied.
		*** The t parameter specifies either a time_t value (as in unix time()) or
		*** a julian number. The isJulian parameter indicates how t is to be interpreted.
		***
		*** If isJulian is true then t is expected to be a julian day number,
		*** otherwise it is used as a time_t.
		***
		*** @note
		*** The constructor which took only a time_t value has been removed to avoid confusion
		*** for the compiler and accidental construction of a SWDate object with the wrong value.
		***
		*** @param[in]	t			The time value to construct the date from
		*** @param[in]	isJulian	Indicates is the parameter t is to be interpreted as
		***							a time_t (isJulian=false) or a julian day number
		***							(isJulian=true).
		**/
		SWDate(sw_int32_t t, bool isJulian);

		/// Construct a SWDate object with the date based on the given Time object
		SWDate(const SWTime &ut);

		/// Return true if this object contains a valid date
		bool	isValid() const										{ return m_valid; }

		/// Return true if this object represents a leap year
		bool	isLeapYear() const									{ return isLeapYear(m_year); }

		/// Assignment operator from SWTime object
		SWDate &		operator=(const SWTime &ut);

		/// Assignment operator from SWDate object
		SWDate &		operator=(const SWDate &other);

		/**
		*** Set this date from the given day, month and year.
		***
		*** If an invalid date is specified this method will return false and
		*** the object will remain unchanged.
		***
		*** @param	day					The day (1-31)
		*** @param	month				The month (1-12)
		*** @param	year				The year (1-9999)
		*** @param	throwOnInvalidDMY	If true and the date is invalid throw a SWDateInvalidException
		***
		*** @return						true if the conversion was successful, false otherwise.
		***
		*** @throws SWDateInvalidException
		**/
		bool			set(int day, int month, int year, bool throwOnInvalidDMY=false);

		/**
		*** Set the date value from a string value and format.
		***
		*** The string value is parsed using the format string as a guide and the time value is set accordingly.
		*** The time value is assumed to be in GMT format and no offset is applied.
		***
		*** @param	value				The time value string. This value is interpreted according to the format parameter.
		*** @param	format				The format string which specifies how the string value is interpreted. The format is
		***								a sequence of characters built up from the following:
		***
		***								- YYYY - A 4-digit year
		***								- YY - A 2-digit year (00-99)
		***								- MM - A 2-digit month (01-12)
		***								- DD - A 2-digit day number (01-31)
		***
		***								Other characters in the format string are matched exactly, so a format of "DD/MM/YY" will
		***								only match a number sequence with slashes (i.e. 10/12/32) and not (101232).
		***
		*** @param	throwOnInvalidDMY	If true and the date is invalid throw a SWDateInvalidException
		***
		*** @throws SWDateInvalidException
		***
		*** @return						true if the conversion was successful, false otherwise.
		**/
		bool			set(const SWString &value, const SWString &format="YYYYMMDD", bool throwOnInvalidDMY=false);

		/// Get the Julian Day Number
		int				julian() const								{ return m_julian; }

		/// Return the day of month (1-31)
		int				day() const									{ return m_day; }

		/// Return the month (1=Jan - 12=Dec)
		int				month() const								{ return m_month; }

		/// Get the year
		int				year() const								{ return m_year; }

		/// Return the day of week (0=Sunday - 6=Saturday)
		int				dayOfWeek() const							{ return (m_julian+1)%7; }

		/// Return the day of month (1-31)
		int				dayOfMonth() const							{ return m_day; }

		/// Return the day of year (1-366 if dayOneIsZero is false) or (0-365 if dayOneIsZero is true)
		int				dayOfYear(bool dayOneIsZero=false) const	{ return m_julian - m_julianYearStart + (dayOneIsZero?0:1); }

		/**
		*** Get the week number of year as a decimal number (0-53),
		***
		*** Returns the week number of the year taking into account the day
		*** which is considered the first day of the week. This defines when
		*** week 1 starts. Week 0 is the first few days of the year before the first
		*** day of the week. Week 53 is the last few days of the year. Both week 0 and week
		*** 53 will be less than 7 days.
		***
		*** @param	firstDayOfWeek1		Specifies which day is considered the first day of week 1 (default=Sunday)
		**/
		int				weekOfYear(DayNumber firstDayOfWeek1=Sunday) const;


		/// Update this object to hold the current date
		void			update();


		/**
		*** @brief	Convert to a string.
		**/
		SWString		toString(const SWString &format="YYYYMMDD") const;


		/**
		*** Adjust the date by the specified number of units
		**/
		void			adjust(int amount, int unit);

public: // Comparison operators

		/**
		*** @brief	Compare this object with another.
		***
		*** This method compares this object with the one specified and returns a
		*** result indicating if the other object is less than, equal to, or greater
		*** than this object.
		***
		*** - If this object is less than the specified object then a negative result is returned.
		*** - If this object is greater than the specified object then a positive result is returned.
		*** - If this object is equal to the specified object then a zero result is returned.
		***
		*** @note
		*** The compareTo method is not required to return -1 and +1 for the negative and positive
		*** values although this is typically the value returned. Callers should test for \< 0 and \> 0
		*** rather than for specific values of -1 and +1.
		**/
		int			compareTo(const SWDate &other) const;

		/**
		*** @brief	Test for equality
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is equal to the other object, false otherwise.
		**/
		bool	operator==(const SWDate &other) const	{ return (compareTo(other) == 0); }

		/**
		*** @brief	Test for inequality
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is not equal to the other object, false otherwise.
		**/
		bool	operator!=(const SWDate &other) const	{ return (compareTo(other) != 0); }

		/**
		*** @brief	Test for less than.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is less than the other object, false otherwise.
		**/
		bool	operator< (const SWDate &other) const	{ return (compareTo(other) <  0); }

		/**
		*** @brief	Test for greater than.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is greater than the other object, false otherwise.
		**/
		bool	operator> (const SWDate &other) const	{ return (compareTo(other) >  0); }

		/**
		*** @brief	Test for less than or equal to.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is less than or equal to the other object, false otherwise.
		**/
		bool	operator<=(const SWDate &other) const	{ return (compareTo(other) <= 0); }

		/**
		*** @brief	Test for greater than or equal to.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is greater than or equal to the other object, false otherwise.
		**/
		bool	operator>=(const SWDate &other) const	{ return (compareTo(other) >= 0); }


public: // Other operators

		/// Prefix increment operator.
		const SWDate &	operator++();

		/// Postfix increment operator.
		SWDate			operator++(int);

		/// Prefix decrement operator.
		const SWDate &	operator--();

		/// Postfix decrement operator.
		SWDate			operator--(int);

		/// += operator (int)
		SWDate &		operator+=(int v);

		/// -= operator (int)
		SWDate &		operator-=(int v);


public: // Friend operators and methods

		/**
		*** @brief	Return the sum of 2 dates
		***
		*** Adds the 2 specified dates together and returns a new date object
		*** containing that date.
		***
		*** @param[in]	a	The first date to add together.
		*** @param[in]	b	The second date to add together.
		***
		*** @return			The sum of a + b.
		**/
		friend SWIFT_DLL_EXPORT SWDate	operator+(const SWDate &a, const SWDate &b);

		/**
		*** @brief	Return the difference of 2 dates
		***
		*** Subtracts the second specified date from the first and returns a new date object
		*** containing that date.
		***
		*** @param[in]	a	The date to subtract from.
		*** @param[in]	b	The date to subtract from a.
		***
		*** @return			The sum of a - b.
		**/
		friend SWIFT_DLL_EXPORT SWDate	operator-(const SWDate &a, const SWDate &b);


public: // Virtual overrides

		/// Override for SWObject writeToStream
		virtual sw_status_t		writeToStream(SWOutputStream &stream) const;

		/// Override for SWObject readFromStream
		virtual sw_status_t		readFromStream(SWInputStream &stream);


public: // STATIC

		/// Convert a day/month/year to a julian day number
		static void		dayMonthYearToJulian(int d, int m, int y, int &j);

		/// Convert a  julian day number to a day/month/year
		static void		julianToDayMonthYear(int j, int &d, int &m, int &y);

		/// Check to see if the given year is a leap year
		static bool		isLeapYear(int year);

		/// Return a SWDate object containing today's date.
		static SWDate	today();

		/// Compare 2 dates and return the difference in years, months and days, returns 0 if the same, -1 if dt1 < dt2, 1 if dt1 > dt2
		static int		compareDates(const SWDate &dt1, const SWDate &dt2, int &years, int &months, int &days);

private:
		/// Internal function to calculate the value of the non-julian fields
		void			calculateFields();

private:
		bool		m_valid;			///< Set to true if the calculated fields are valid
		sw_int32_t	m_julian;			///< The julian day number
		sw_int32_t	m_julianYearStart;	///< Calculated field: The julian number for the start of the year
		sw_uint8_t	m_day;				///< Calculated field: The day (1-31)
		sw_uint8_t	m_month;			///< Calculated field: The month (1-12)
		sw_int16_t	m_year;				///< Calculated field: The year
		};


SW_DEFINE_EXCEPTION(SWInvalidDateException, SWException);




#endif
