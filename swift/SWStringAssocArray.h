/**
*** @file
*** @brief		A template map class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWHashMap class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWStringAssocArray_h__
#define __SWStringAssocArray_h__


#include <swift/swift.h>
#include <swift/SWHashMap.h>
#include <swift/SWStringArray.h>



/**
*** Perl-like assoc. arrays for strings
**/
class SWIFT_DLL_EXPORT SWStringAssocArray : public SWObject
		{
public:
		/// Default constructor
		SWStringAssocArray(bool ignoreCase=false);

		// Copy constructor and assignment operator
		SWStringAssocArray(SWStringAssocArray &other);
		SWStringAssocArray & operator=(const SWStringAssocArray &other);

		// Array operator
		SWString &	operator[](const SWString &key);

		// Get an entry
		SWString		get(const SWString &key) const;

		// Check to see if an element is defined
		bool			defined(const SWString &key) const;

		// Remove a entry
		bool			remove(const SWString &key);

		// Remove all entries
		sw_size_t		size() const;

		// Remove all entries
		void			clear();

		// Returns an iterator of MapEntry objects
		SWIterator< SWHashMapEntry<SWString,SWString> >	iterator() const;

		// Returns an array of values
		SWIterator<SWString>							values() const;

		// Returns an array of keys
		SWIterator<SWString>							keys() const;

		// Returns an array of the keys contained in this map.
		SWStringArray	keysArray() const;

		// Returns an array of the values contained in this map.
		SWStringArray	valuesArray() const;

		bool			lookup(const SWString &key, SWString &value) const;

		void			set(const SWString &key, const SWString &value);

private:
		SWHashMap<SWString,SWString>	m_strings;	// The array of strings
		bool							m_ignoreCase;
		};




#endif
