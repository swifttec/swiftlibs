/**
*** A collection of O/S emulation functions
***
*** All non-C++ specific functions have been moved to the xplatform library.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#ifndef __swift_h__
#include <swift/swift.h>
#endif

#include <xplatform/sw_osfunc.h>

#ifndef __swift_sw_functions_h__
#define __swift_sw_functions_h__




SWIFT_DLL_EXPORT char *		strnew(const char *);
SWIFT_DLL_EXPORT char *		strnew(const wchar_t *, int codepage=CP_UTF8);
SWIFT_DLL_EXPORT wchar_t *	wcsnew(const char *, int codepage=CP_ACP);
SWIFT_DLL_EXPORT wchar_t *	wcsnew(const wchar_t *);

// The SwiftTec equivalent to AFX_MANAGE_STATE
class SWResourceModule;
SWIFT_DLL_EXPORT SWResourceModule * sw_resource_push_state();
SWIFT_DLL_EXPORT void sw_resource_pop_state(SWResourceModule *p);
class SWIFT_DLL_EXPORT SWResourceGuard
		{
public:
		SWResourceGuard()			{ m_pModule = sw_resource_push_state(); }
		~SWResourceGuard()			{ sw_resource_pop_state(m_pModule); }

private:
		SWResourceModule	*m_pModule;
		};
	


#endif // __swift_sw_osfunc_h__
