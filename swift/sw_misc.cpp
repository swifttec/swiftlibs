/**
*** A collection of internal functions
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <swift/sw_misc.h>
#include <swift/SWStringAssocArray.h>
#include "sw_internal_printf.h"

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif


#if defined(linux)
#include <wctype.h>
#endif


/**
*** Get the value of an environment variable.
***
*** @param	var	The variable to get from the environment
*** @param	value	Given the value of var if found
*** @return	true if found, false otherwise
**/
SWIFT_DLL_EXPORT bool
sw_misc_getEnvironmentVariable(const SWString &var, SWString &value)
		{
#if defined(SW_PLATFORM_WINDOWS)
		DWORD	dw, size;

		size = 128;
		dw = GetEnvironmentVariable(var, (TCHAR *)value.lockBuffer(size, sizeof(TCHAR)), size);

		if (dw > size)
			{
			value.unlockBuffer();
			size = dw;
			dw = GetEnvironmentVariable(var, (TCHAR *)value.lockBuffer(size, sizeof(TCHAR)), size);
			}

		value.unlockBuffer();

		return (dw != 0);

#else
		sw_tchar_t	*vp = t_getenv(var);

		if (vp != NULL) value = vp;
		else value.empty();

		return (vp != NULL);
#endif
		}


/**
*** Set the value of an environment variable.
***
*** @param	var	The variable to set in the environment
*** @param	value	The value to set var to
*** @return	true if succeed, false otherwise
**/
SWIFT_DLL_EXPORT bool
sw_misc_setEnvironmentVariable(const SWString &var, const SWString &value)
		{
		bool				r;

#if defined(SW_PLATFORM_WINDOWS)
		r = (SetEnvironmentVariable(var, value) != FALSE);
#else
		static SWStringAssocArray	sa;
		
		sa[var].format(_T("%s=%s"), var.c_str(), value.c_str());
		putenv((char *)(sa[var].c_str()));
		r = true;
#endif

		return r;
		}


SWIFT_DLL_EXPORT void	
sw_misc_getDefaultBytesIndexAndPrecision(sw_uint64_t v, int &idx, int &precision)
		{
		double		d;

#if defined(SW_PLATFORM_WINDOWS)
		d = (double)(sw_int64_t)v;
#else
		d = v;
#endif

		idx = 0;

		while (klist[idx+1].shortname && (v / 1000) != 0)
			{
			idx++;
			v /= 1024;
			d /= 1024.0;
			}

		precision = klist[idx].defaultprecision;
		}


SWIFT_DLL_EXPORT SWString	
sw_misc_convertNumberToBytesString(sw_uint64_t num, int idx, int precision, int flags)
		{
		SWString	result;
		bool		useDefaultPrecision = (precision < 0);

		if (idx < 0)
			{
			int			defprc;

			// Get the defaults
			sw_misc_getDefaultBytesIndexAndPrecision(num, idx, defprc);
			}

		if (precision < 0) precision = klist[idx].defaultprecision;

		double		d;

#if defined(WIN32) || defined(_WIN32)
		d = (double)(sw_int64_t)num;
#else
		d = num;
#endif

		for (int i=0;i<idx;i++)
			{
			num /= 1024;
			d /= 1024.0;
			}

		char	tmp[128], *tp;

		sw_snprintf(tmp, sizeof(tmp), "%.*f", precision, d);

		// If # format is NOT specified anything ending in 0 after the . is removed (i.e. 64.0KB -> 64KB, 64.120KB -> 64.12KB)
		if ((useDefaultPrecision && !(flags & SW_CONVERTBYTES_COMPACT_FORMAT)) && (tp = strchr(tmp, '.')) != NULL)
			{
			if (atoi(tp+1) == 0) *tp = 0;
			else
				{
				tp++;
				while (*tp)
					{
					if (atoi(tp) == 0)
						{
						*tp = 0;
						break;
						}
					tp++;
					}
				}
			}

		// Add the size specifier
		if (!(flags&SW_CONVERTBYTES_USE_NO_NAME))
			{
			const char	*name;

			if ((flags & SW_CONVERTBYTES_USE_PREFERRED_NAME) != 0) name = klist[idx].preferredname;
			else if ((flags & SW_CONVERTBYTES_USE_LONG_NAME) != 0) name = klist[idx].longname;
			else name = klist[idx].shortname;

			// If _ flag is specified we don't want a space between the number and the size specifier
			if (!(flags&SW_CONVERTBYTES_NO_INSERT_SPACE)) strcat(tmp, " ");

			strcat(tmp, name);
			}

		result = tmp;

		return result;
		}


/*
** sw_fatal makes a call to the sw_log_fatal function to make an entry
** in the application log and then calls exit with an exit code of 1
**
** @brief Log a fatal error and exit
SWIFT_DLL_EXPORT void
^sw_fatal(const TCHAR *fmt, ...)
	{
	va_list ap;
	va_start(ap, fmt);
	sw_log_internal_ap(SW_LOG_LEVEL_FATAL,  _T("unknown"), -1, 0, 1, fmt, ap);
	va_end(ap);

	exit(1);
	}
**/


/*
** Run the program and optionally wait for it to close
SWIFT_DLL_EXPORT void
^sw_misc_runProgram(String cmdline, bool waitForProcess, int *exitcode)
	{
#if defined(WIN32) || defined(_WIN32)
	PROCESS_INFORMATION	pinfo;
	STARTUPINFO		sinfo;
	TCHAR			*cp = cmdline.GetBuffer(cmdline.length()+1);

	memset(&sinfo, 0, sizeof(sinfo));
	sinfo.cb = sizeof(sinfo); 
	sinfo.lpTitle = cp;
	sinfo.dwFlags = 0;

	CreateProcess(NULL, cp, NULL, NULL, FALSE, 0, NULL, NULL, &sinfo, &pinfo);

	cmdline.ReleaseBuffer();
	
	if (waitForProcess)
	    {
	    DWORD	r;
	    
	    for (;;)
		{
		r = MsgWaitForMultipleObjectsEx(1, &pinfo.hProcess, INFINITE, QS_ALLEVENTS|QS_ALLINPUT|QS_ALLPOSTMESSAGE, MWMO_INPUTAVAILABLE);
		if (r == WAIT_OBJECT_0) break;

		MSG msg;
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		    {
		    if (msg.message == WM_QUIT) break;
		    else
			{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			}
		    }
		}
	    }
#endif
	}
**/

SWIFT_DLL_EXPORT bool
sw_misc_isWindows2000OrGreater()
		{
#if defined(SW_PLATFORM_WINDOWS)
		OSVERSIONINFO osinfo;
		osinfo.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

		GetVersionEx(&osinfo);

		DWORD dwPlatformId   = osinfo.dwPlatformId;
		DWORD dwMajorVersion = osinfo.dwMajorVersion;

		return ((dwPlatformId == 2) && 
				(dwMajorVersion >= 5));
#else
		return false;
#endif
		}

SWIFT_DLL_EXPORT bool
sw_misc_isWindows2000()
		{
#if defined(SW_PLATFORM_WINDOWS)
		OSVERSIONINFO osinfo;
		osinfo.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

		GetVersionEx(&osinfo);

		DWORD dwPlatformId   = osinfo.dwPlatformId;
		DWORD dwMajorVersion = osinfo.dwMajorVersion;
		DWORD dwMinorVersion = osinfo.dwMinorVersion;

		return ((dwPlatformId == 2) && 
				(dwMajorVersion == 5) && 
				(dwMinorVersion == 0));
#else
		return false;
#endif
		}

SWIFT_DLL_EXPORT bool
sw_misc_isWindowsXP()
		{
#if defined(SW_PLATFORM_WINDOWS)
		OSVERSIONINFO osinfo;
		osinfo.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

		GetVersionEx(&osinfo);

		DWORD dwPlatformId   = osinfo.dwPlatformId;
		DWORD dwMajorVersion = osinfo.dwMajorVersion;
		DWORD dwMinorVersion = osinfo.dwMinorVersion;

		return ((dwPlatformId == 2) && 
				(dwMajorVersion == 5) && 
				(dwMinorVersion >= 1));
#else
		return false;
#endif
		}


SWIFT_DLL_EXPORT bool
sw_misc_isWindowsVista()
		{
#if defined(SW_PLATFORM_WINDOWS)
		OSVERSIONINFO osinfo;
		osinfo.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

		GetVersionEx(&osinfo);

		DWORD dwPlatformId   = osinfo.dwPlatformId;
		DWORD dwMajorVersion = osinfo.dwMajorVersion;

		return ((dwPlatformId == 2) && 
				(dwMajorVersion >= 6));
#else
		return false;
#endif
		}





SWIFT_DLL_EXPORT int
sw_misc_expandSize(int nbytes, int align)
		{
		if (align < 1) align = 1;
		return (((nbytes + (align - 1)) / align) * align);
		}


SWIFT_DLL_EXPORT void
sw_misc_setFlag(sw_uint32_t &value, sw_uint32_t flags, bool b)
		{
		if (b) value |= flags;
		else value &= ~flags;
		}


SWIFT_DLL_EXPORT bool
sw_misc_isFlagSet(const sw_uint32_t &value, sw_uint32_t flags, bool allflags)
		{
		sw_uint32_t	res=value & flags;

		return (allflags?(res == flags):(res != 0));
		}


SWIFT_DLL_EXPORT void		sw_misc_clear(bool &v)			{ v = false; }

SWIFT_DLL_EXPORT void		sw_misc_clear(sw_int8_t &v)		{ v = 0; }
SWIFT_DLL_EXPORT void		sw_misc_clear(sw_int16_t &v)	{ v = 0; }
SWIFT_DLL_EXPORT void		sw_misc_clear(sw_int32_t &v)	{ v = 0; }
SWIFT_DLL_EXPORT void		sw_misc_clear(sw_int64_t &v)	{ v = 0; }

SWIFT_DLL_EXPORT void		sw_misc_clear(sw_uint8_t &v)	{ v = 0; }
SWIFT_DLL_EXPORT void		sw_misc_clear(sw_uint16_t &v)	{ v = 0; }
SWIFT_DLL_EXPORT void		sw_misc_clear(sw_uint32_t &v)	{ v = 0; }
SWIFT_DLL_EXPORT void		sw_misc_clear(sw_uint64_t &v)	{ v = 0; }

SWIFT_DLL_EXPORT void		sw_misc_clear(float &v)			{ v = 0.0; }
SWIFT_DLL_EXPORT void		sw_misc_clear(double &v)		{ v = 0.0; }

SWIFT_DLL_EXPORT void		sw_misc_clear(SWObject &v)		{ v.clear(); }
SWIFT_DLL_EXPORT void		sw_misc_clear(SWTime &v)		{ v = SWTime(); }


SWIFT_DLL_EXPORT int		sw_misc_compare(sw_int8_t v1, sw_int8_t v2)		{ return ((v1 < v2)?-1:((v2 > v1)?1:0)); }
SWIFT_DLL_EXPORT int		sw_misc_compare(sw_int16_t v1, sw_int16_t v2)	{ return ((v1 < v2)?-1:((v2 > v1)?1:0)); }
SWIFT_DLL_EXPORT int		sw_misc_compare(sw_int32_t v1, sw_int32_t v2)	{ return ((v1 < v2)?-1:((v2 > v1)?1:0)); }
SWIFT_DLL_EXPORT int		sw_misc_compare(sw_int64_t v1, sw_int64_t v2)	{ return ((v1 < v2)?-1:((v2 > v1)?1:0)); }

SWIFT_DLL_EXPORT int		sw_misc_compare(sw_uint8_t v1, sw_uint8_t v2)	{ return ((v1 < v2)?-1:((v2 > v1)?1:0)); }
SWIFT_DLL_EXPORT int		sw_misc_compare(sw_uint16_t v1, sw_uint16_t v2)	{ return ((v1 < v2)?-1:((v2 > v1)?1:0)); }
SWIFT_DLL_EXPORT int		sw_misc_compare(sw_uint32_t v1, sw_uint32_t v2)	{ return ((v1 < v2)?-1:((v2 > v1)?1:0)); }
SWIFT_DLL_EXPORT int		sw_misc_compare(sw_uint64_t v1, sw_uint64_t v2)	{ return ((v1 < v2)?-1:((v2 > v1)?1:0)); }

SWIFT_DLL_EXPORT int		sw_misc_compare(float v1, float v2)				{ return ((v1 < v2)?-1:((v2 > v1)?1:0)); }
SWIFT_DLL_EXPORT int		sw_misc_compare(double v1, double v2)			{ return ((v1 < v2)?-1:((v2 > v1)?1:0)); }
