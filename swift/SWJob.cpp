/**
*** @file		SWJob.cpp
*** @brief		A class to represent an abstract job.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWJob class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWJob.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif



class SWJobFunction : public SWJob
		{
public:
		SWJobFunction(JobProc fp, void *param);

		/// Override for SWJob method
		virtual sw_status_t	run();

private:
		JobProc		m_jobfunc;		///< The job function to call
		void		*m_jobparam;	///< The parameter to pass to the job function.
		};


SWJobFunction::SWJobFunction(JobProc fp, void *param) :
	m_jobfunc(fp),
	m_jobparam(param)
		{
		}


sw_status_t
SWJobFunction::run()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (m_jobfunc != NULL)
			{
			res = m_jobfunc(m_jobparam);
			}

		return res;
		}







SWJob::SWJob(NotificationProc notify) :
	m_pJobList(NULL),
	m_flags(0),
	m_notify(notify),
	m_status(SW_STATUS_JOB_NOT_EXECUTED)
		{
		}


SWJob::SWJob(JobProc fp, void *param, NotificationProc notify) :
	m_pJobList(NULL),
	m_flags(SW_JOB_FUNCTION),
	m_notify(notify),
	m_status(SW_STATUS_JOB_NOT_EXECUTED)
		{
		add(new SWJobFunction(fp, param), true);
		}


SWJob::~SWJob()
		{
		// Cleanup
		if (m_pJobList != NULL)
			{
			delete m_pJobList;
			}
		}


void
SWJob::add(SWJob *pJob, bool delflag)
		{
		SWGuard	guard(this);

		if (delflag) pJob->m_flags |= SW_JOB_DELETE;
		else pJob->m_flags &= ~SW_JOB_DELETE;

		if (m_pJobList == NULL)
			{
			m_pJobList = new SWList<SWJob *>;
			}

		if (m_pJobList != NULL)
			{
			m_pJobList->add(pJob, SWList<SWJob *>::ListTail);
			}
		}


sw_status_t
SWJob::run()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if ((m_flags & SW_JOB_FUNCTION) != 0 && m_pJobList != NULL)
			{
			SWJob	*pJob=NULL;

			m_pJobList->get(SWList<SWJob *>::ListHead, pJob);
			if (pJob != NULL)
				{
				res = pJob->execute();
				}
			}

		return res;
		}


sw_status_t
SWJob::execute()
		{
		SWGuard		guard(this);
		sw_status_t	res;
		bool		ignoreerrors=((m_flags & SW_JOB_IGNORE_ERRORS) != 0);

		m_status = SW_STATUS_SUCCESS;
		m_flags |= (SW_JOB_RUNNING|SW_JOB_STARTED);

		notify(JobStarted);

		if ((m_flags & SW_JOB_EXECUTE_SUBJOBS_FIRST) == 0)
			{
			// First of all call run.
			m_status = run();
			if (m_status != SW_STATUS_SUCCESS) m_flags |= SW_JOB_ERROR;
			}

		// If there are any subjobs - run them now.
		if ((m_status == SW_STATUS_SUCCESS || ignoreerrors != 0) && m_pJobList != NULL)
			{
			SWIterator<SWJob *>	it=m_pJobList->iterator();
			SWJob				**ppJob;
			bool				seenFunction=false;

			while ((m_status == SW_STATUS_SUCCESS || (m_flags & SW_JOB_IGNORE_ERRORS) != 0) && it.hasNext())
				{
				ppJob = it.next();

				if ((m_flags & SW_JOB_FUNCTION) != 0 && !seenFunction)
					{
					// The first job in the list is the main function, not a real sub-job
					seenFunction = true;
					continue;
					}

				res = (*ppJob)->execute();
				if (res != SW_STATUS_SUCCESS)
					{
					m_flags |= SW_JOB_SUBJOB_ERROR;
					if (!ignoreerrors)
						{
						// Record the status and stop
						m_status = res;
						break;
						}
					else
						{
						if (m_status == SW_STATUS_SUCCESS || ((m_flags & SW_JOB_RETURN_FIRST_ERROR) == 0))
							{
							// Record the error code
							m_status = res;
							}
						}
					}
				}

			while (it.hasNext())
				{
				ppJob = it.next();
				(*ppJob)->reset();
				}
			}

		if ((m_status == SW_STATUS_SUCCESS || (m_flags & SW_JOB_IGNORE_ERRORS) != 0) && (m_flags & SW_JOB_EXECUTE_SUBJOBS_FIRST) != 0)
			{
			// Last of all call run.
			res = run();
			if (res != SW_STATUS_SUCCESS)
				{
				m_flags |= SW_JOB_ERROR;
				if (!ignoreerrors)
					{
					// Record the status and stop
					m_status = res;
					}
				else
					{
					if (m_status == SW_STATUS_SUCCESS || ((m_flags & SW_JOB_RETURN_FIRST_ERROR) == 0))
						{
						// Record the error code
						m_status = res;
						}
					}
				}
			}

		m_flags &= ~SW_JOB_RUNNING;

		notify(JobStopped);

		return m_status;
		}


sw_status_t
SWJob::reset()
		{
		SWGuard		guard(this);
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (!isRunning())
			{
			// Reset this job
			m_status = SW_STATUS_JOB_NOT_EXECUTED;
			m_flags &= ~(SW_JOB_RUNNING|SW_JOB_STARTED);

			// Reset all sub-jobs
			if (m_pJobList != NULL)
				{
				SWIterator<SWJob *>	it=m_pJobList->iterator();
				SWJob				**ppJob;

				while (res == SW_STATUS_SUCCESS && it.hasNext())
					{
					ppJob = it.next();
					res = (*ppJob)->reset();
					}
				}
			}
		else
			{
			res = SW_STATUS_JOB_RUNNING;
			}

		return res;
		}


void
SWJob::notify(NotificationCode code)
		{
		if (m_notify != NULL)
			{
			m_notify(this, code);
			}
		}


void
SWJob::setNotificationFunction(NotificationProc notify)
		{
		m_notify = notify;
		}


void
SWJob::setDeleteFlag(bool v)
		{
		if (v) m_flags |= SW_JOB_DELETE;
		else m_flags &= ~SW_JOB_DELETE;
		}


bool
SWJob::getDeleteFlag() const
		{
		return ((m_flags & SW_JOB_DELETE) != 0);
		}


void
SWJob::setExecutionOrder(bool v)
		{
		if (v) m_flags |= SW_JOB_EXECUTE_SUBJOBS_FIRST;
		else  m_flags &= ~SW_JOB_EXECUTE_SUBJOBS_FIRST;
		}


void
SWJob::setIgnoreErrors(bool v)
		{
		if (v) m_flags |= SW_JOB_IGNORE_ERRORS;
		else  m_flags &= ~SW_JOB_IGNORE_ERRORS;
		}


void
SWJob::setErrorReturn(bool v)
		{
		if (v) m_flags |= SW_JOB_RETURN_FIRST_ERROR;
		else  m_flags &= ~SW_JOB_RETURN_FIRST_ERROR;
		}


sw_status_t
SWJob::status() const
		{
		sw_status_t	res=m_status;

		if (isRunning()) res = SW_STATUS_JOB_RUNNING;

		return res;
		}

