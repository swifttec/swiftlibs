/**
*** @file		SWPointerArray.cpp
*** @brief		An array of Pointers
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWPointerArray class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <swift/SWPointer.h>
#include <swift/SWException.h>
#include <swift/SWInputStream.h>
#include <swift/SWOutputStream.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




SWPointer::SWPointer(void *ptr) :
    m_ptr(ptr)
		{
		}


SWPointer::~SWPointer()
		{
		// Just in case
		m_ptr = NULL;
		}



sw_status_t
SWPointer::writeToStream(SWOutputStream &stream) const
		{
		sw_status_t	res;
		sw_uintptr_t	p;

		p = (sw_uintptr_t)m_ptr;

		res = stream.write((sw_int8_t)sizeof(m_ptr));
		if (res == SW_STATUS_SUCCESS) res = stream.write(p);

		return res;
		}


sw_status_t
SWPointer::readFromStream(SWInputStream &stream)
		{
		sw_status_t		res;
		sw_uint8_t		s;
		sw_uintptr_t	p;

		res = stream.read(s);
		if (res == SW_STATUS_SUCCESS)
			{
			if (s != sizeof(m_ptr))
				{
				throw SW_EXCEPTION(SWIllegalStateException);
				}

			res = stream.read(p);
			if (res == SW_STATUS_SUCCESS)
				{
				m_ptr = (void *)p;
				}
			}
		
		return res;
		}




