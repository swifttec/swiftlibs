/****************************************************************************
**	SWApp.cpp
****************************************************************************/

#include "stdafx.h"

#include <swift/swift.h>
#include <swift/SWFilename.h>
#include <swift/SWFileInfo.h>
#include <swift/SWApp.h>


SWApp	*SWApp::m_pMainApp=NULL;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

SWApp::SWApp(SWAppConfig *pAppConfig, int appflags, bool mainapp) :
	m_exitcode(0),
	m_pAppConfig(pAppConfig),
	m_mainApp(mainapp),
	m_appHelpFileType(0),
	m_appFlags(appflags),
	m_pAppConfigDelFlag(false)
		{
		synchronize();

		if (m_mainApp)
			{
			if (m_pMainApp == NULL)
				{
				m_pMainApp = this;
				}
			else
				{
				m_mainApp = false;
				}
			}

		if (m_pAppConfig == NULL)
			{
			m_pAppConfig = new SWAppConfig();
			m_pAppConfigDelFlag = true;
			}
		}


SWApp::~SWApp()
		{
		if (m_mainApp)
			{
			m_pMainApp = NULL;
			}

		if (m_pAppConfigDelFlag)
			{
			safe_delete(m_pAppConfig);
			}
		}
		
	
void
SWApp::setAppID(const SWString &s)
		{
		m_appID = s;
		}
		
	
void
SWApp::setAppTitle(const SWString &s)
		{
		m_appTitle = s;
		}
		
	
void
SWApp::setRegistryKey(const SWString &s)
		{
		m_appRegistryKey = s;
		}
 


int
SWApp::execute(SWApp &app, const SWString &prog, const SWStringArray &args)
		{
		int		exitcode=0;

		if (app.initInstance(prog, args))
			{
			app.runInstance();
			}
			
		app.exitInstance();

		return app.exitcode();
		}


bool
SWApp::initInstance(const SWString &prog, const SWStringArray &args)
		{
		m_appExecutable = prog;
		m_appArgs = args;

		if (m_pAppConfig == NULL)
			{
			return false;
			}

		m_pAppConfig->init(prog, args);

		SWString	s;

		s = m_pAppConfig->getString("PRODUCT_COMPANY");
		s += ".";
		s += m_pAppConfig->getString("PRODUCT_TITLE");
		s += ".";
		s += m_pAppConfig->getString("PROGRAM_NAME");
		s += ".";
		s += m_pAppConfig->getVersionString("PROGRAM");
		setAppID(s);

		s = m_pAppConfig->getString("PRODUCT_TITLE");
		if (s.isEmpty())
			{
			s = m_pAppConfig->getString("PRODUCT_COMPANY");
			s +=m_pAppConfig->getString("PRODUCT_NAME");
			s += m_pAppConfig->getString("PROGRAM_NAME");
			}
		
		setAppTitle(s);
		
		// Standard initialization
		// If you are not using these features and wish to reduce the size
		// of your final executable, you should remove from the following
		// the specific initialization routines you do not need
		// Change the registry key under which our settings are stored
		// TODO: You should modify this string to be something appropriate
		// such as the name of your company or organization
		setRegistryKey(m_pAppConfig->getCompanyName());
		
		return true;
		}


int
SWApp::exitInstance()
		{
		return m_exitcode;
		}


void
SWApp::runInstance()
		{
		m_exitcode = 0;
		}


void
SWApp::setHelpFile(const SWString &filename, int type)
		{
		m_appHelpFile = filename;
		m_appHelpFileType = type;
		}


/****************************************************************************
*                             createExclusionName
* Inputs:
*       LPCTSTR GUID: The GUID for the exclusion
*       UINT kind: Kind of exclusion
*               UNIQUE_TO_SYSTEM
*               UNIQUE_TO_DESKTOP
*               UNIQUE_TO_SESSION
*               UNIQUE_TO_TRUSTEE
* Result: CString
*       A name to use for the exclusion mutex
* Effect: 
*       Creates the exclusion mutex name
* Notes:
*       The GUID is created by a declaration such as
*               #define UNIQUE _T("MyAppName-{44E678F7-DA79-11d3-9FE9-006067718D04}")
****************************************************************************/
SWString				
SWApp::createExclusionName(const SWString &guid, ExclusionType kind)
		{
		SWGuard		guard(this);
		SWString	res=guid;

#if defined(SW_PLATFORM_WINDOWS)
		switch (kind)
			{
			case UNIQUE_TO_SYSTEM:
				res = guid;
				break;

			case UNIQUE_TO_DESKTOP:
				{ /* desktop */
				SWString s = guid;
				DWORD len;
				HDESK desktop = GetThreadDesktop(GetCurrentThreadId());
				BOOL result = GetUserObjectInformation(desktop, UOI_NAME, NULL, 0, &len);
				DWORD err = ::GetLastError();
				if (!result && err == ERROR_INSUFFICIENT_BUFFER)
					{ /* NT/2000 */
					LPBYTE data = new BYTE[len];
					result = GetUserObjectInformation(desktop, UOI_NAME, data, len, &len);
					s += _T("-");
					s += (LPCTSTR)data;
					delete [ ] data;
					} /* NT/2000 */
				else
					{ /* Win9x */
					s += _T("-Win9x");
					} /* Win9x */
				
				res = s;
				} /* desktop */
				break;

			case UNIQUE_TO_SESSION:
				{ /* session */
				SWString s = guid;
				HANDLE token;
				DWORD len;
				BOOL result = OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY, &token);
				if (result)
					{ /* NT */
					GetTokenInformation(token, TokenStatistics, NULL, 0, &len);
					LPBYTE data = new BYTE[len];
					GetTokenInformation(token, TokenStatistics, data, len, &len);
					LUID uid = ((PTOKEN_STATISTICS)data)->AuthenticationId;
					delete [ ] data;
					SWString t;
					t.format(_T("-%08x%08x"), uid.HighPart, uid.LowPart);
					
					res = s;
					res += t;
					} /* NT */
				else
					{ /* 16-bit OS */
					res = s;
					} /* 16-bit OS */
				} /* session */
				break;

			case UNIQUE_TO_TRUSTEE:
				{ /* trustee */
				SWString s = guid;
				#define NAMELENGTH 64
				TCHAR userName[NAMELENGTH];
				DWORD userNameLength = NAMELENGTH;
				TCHAR domainName[NAMELENGTH];
				DWORD domainNameLength = NAMELENGTH;

				if (GetUserName(userName, &userNameLength))
					{ /* get network name */
					// The NetApi calls are very time consuming
					// This technique gets the domain name via an
					// environment variable
					domainNameLength = ExpandEnvironmentStrings(_T("%USERDOMAIN%"),
										domainName,
										NAMELENGTH);
					SWString t;

					t.format(_T("-%s-%s"), domainName, userName);
					s += t;
					} /* get network name */

				res = s;
				} /* trustee */
				break;
			} /* kind */
#endif

		return res;
		}


void
SWApp::onAppHelp()
		{
		if (m_pAppConfig != NULL)
			{
			SWString	filename="index.html";
			SWString	package=m_pAppConfig->getString("PRODUCT_PACKAGE");
			SWString	program=m_pAppConfig->getString("PROGRAM_NAME");
			SWString	htmlfile;

			// Try program/index.html
			filename = m_pAppConfig->getHelpPath(SWFilename::concatPaths(program, "index.html"));
		
			// Try program.html
			htmlfile = program;
			htmlfile += ".html";
			if (!SWFileInfo::isFile(filename)) filename = m_pAppConfig->getHelpPath(htmlfile);

			// Try product/index.html
			if (!SWFileInfo::isFile(filename)) filename = m_pAppConfig->getHelpPath(SWFilename::concatPaths(package, "index.html"));
			htmlfile = package;
			htmlfile += ".html";

			// Try product.html
			if (!SWFileInfo::isFile(filename)) filename = m_pAppConfig->getHelpPath(htmlfile);

			// try index.html
			if (!SWFileInfo::isFile(filename)) filename = "index.html";

			if (SWFileInfo::isFile(filename))
				{
				showHelp(filename);
				}
			}
		}


bool
SWApp::showHelp(const SWString &filename)
		{
		// By default do nothing for now
		return false;
		}
