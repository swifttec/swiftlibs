/**
*** @file		SWThread.cpp
*** @brief		A thread controlling class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWThread class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWThread.h>
#include <swift/SWThreadMutex.h>
#include <swift/SWGuard.h>
#include <xplatform/sw_clock.h>

#include <errno.h>


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif






// Protected Constructor
SWThread::SWThread(sw_uint32_t createflags) :
	m_hThread(0),
	m_flags(0),
    m_tid(0),
	m_exitcode(0),
	m_param(0),
	m_tfunc(0),
	m_priority(0)
		{
		// Create an object mutex
		synchronize(new SWThreadMutex(true), true);

		// Set the runflag
		m_flags = (createflags & SW_THREAD_TYPE_MASK) | (SW_THREAD_FLAG_RUN | SW_THREAD_FLAG_DERIVED);
		m_hThread = 0;
		}


// Public Constructor
SWThread::SWThread(SWThreadObjProc func, void *param, sw_uint32_t createflags) :
	m_hThread(0),
	m_flags(0),
    m_tid(0),
	m_exitcode(0),
	m_param(param),
	m_tfunc(func),
	m_priority(0)
		{
		// Create an object mutex
		synchronize(new SWThreadMutex(true), true);

		// Set the runflag
		m_flags = (createflags & SW_THREAD_TYPE_MASK) | (SW_THREAD_FLAG_RUN | SW_THREAD_FLAG_FUNCTION);
		m_hThread = 0;
		}


// Destructor
SWThread::~SWThread()
		{
		// Force the stop flag on so the the thread will exit gracefully
		// Of course if a thread does no obey this protocol we will wait a very long time.
		stop(true);

		if (getFlags(SW_THREAD_FLAG_VALID) != 0)
			{
			waitUntilStopped();
			}

		if (m_hThread != 0) sw_thread_destroy(m_hThread);

		//sw_log_trace1(_T("Thread object destroyed (%x)"), this);
		}


/**
*** Set the stop flag for this thread and optionally wait until
*** the thread has actually exited
***
*** @param waitFlag	If true wait until the thread exits before returning otherwise return immediately.
**/
void
SWThread::stop(bool waitFlag)
		{
		if (getFlags(SW_THREAD_FLAG_VALID) != 0)
			{
			setStopFlag();
			if (waitFlag) waitUntilStopped();
			}
		}


/**
*** Waits until the thread has exited until returning. If possible this method
*** calls a system function that will suspend the calling thread until this 
*** thread has exited. If this is not possible then the thread is polled at
*** the specified interval (default 250ms) until the thread is flagged as no
*** longer running.
***
*** <b>Note:</b> under Solaris detached and daemon threads cannot be waited
*** on and the poll method will be used. Other threads types can be waited for
*** without polling.
***
*** @brief		Waits until the thread has exited until returning.
***
*** @param pollMs	The time in milliseconds to sleep between tests
***			to determine if the thread is still running.
**/
sw_status_t
SWThread::waitUntilStopped(int /*pollMs*/)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (getFlags(SW_THREAD_FLAG_VALID) != 0)
			{
			// We must not leave here until the thread is stopped
			res = sw_thread_waitUntilStopped(m_hThread);
			if (m_tid > 0)
				{
				while (!stopped() || !getFlags(SW_THREAD_FLAG_JOINED))
					{
					// We cannot do a join of detached threads,
					// so we must wait until they die
					while (!stopped())
						{
						yield();
						}

					// Indicate that we have done a join (we pretend since we can't really do
					// this for a detached thread)
					setFlags(SW_THREAD_FLAG_JOINED);

					res = SW_STATUS_SUCCESS;
					}
				}
			else
				{
				// The thread was never valid so it's stopped and we return
				// a success code.
				res = SW_STATUS_SUCCESS;
				}
			}

		return res;
		}


/**
*** Waits until the thread has started until returning. If possible this method
*** calls a system function that will suspend the calling thread until this 
*** thread has started. If this is not possible then the thread is polled at
*** the specified interval (default 250ms) until the thread is flagged as no
*** longer running.
***
*** <b>Note:</b> Most O/S will require the polling method to be used.
***
*** @brief		Waits until the thread has started until returning.
***
*** @param pollMs	The time in milliseconds to sleep between tests
***			to determine if the thread has started.
**/
sw_status_t
SWThread::waitUntilStarted(int /*pollMs*/)
		{
		if (m_hThread == NULL) throw SW_EXCEPTION(SWThreadInvalidHandleException);

		return sw_thread_waitUntilStarted(m_hThread);
		}



// The form required for the CreateThread function
int
internal_thread_proc(void *param)
		{
		return (unsigned int)SWThread::irun(param);
		}

/*
The following are used for _beginthreadex and AfxBeginThread

unsigned int
#if defined(_AFX_H_)
// MFC threads
__cdecl
#else // !defined(_AFXDLL) || defined(AFXAPI)
// Non-MFC threads
__stdcall
#endif // !defined(_AFXDLL) || defined(AFXAPI)
internal_thread_proc(void *param)
		{
		return (unsigned int)SWThread::irun(param);
		}


#if (defined(WIN32) || defined(_WIN32)) && defined(_AFX_H_)

// Special internal function for starting an MFC thread
unsigned int __cdecl
mfc_thread_proc(void *param)
		{
		SWInternalThread	*pThread=(SWInternalThread *)param;

		m_flags &=~ SW_THREAD_FLAG = false;
		pThread->started = true;

		pThread->exitcode = pThread->tproc(pThread->tparam);

		pThread->stopped = true;

		return pThread->exitcode;
		}
#endif


*/




// Start the thread running
void
SWThread::start()
		{
		if (getFlags(SW_THREAD_FLAG_VALID) != 0 && running())
			{
			// We should not create the same thread twice!
			throw SW_EXCEPTION(SWThreadRunningException);
			}

		// Make sure we are thread-safe
		SWGuard	guard(this);

		if (m_hThread != 0) sw_thread_destroy(m_hThread);

		// Reset the runflag
		setFlags(SW_THREAD_FLAG_RUN);

		m_hThread = sw_thread_create(internal_thread_proc, this);
		if (m_hThread == NULL)
			{
			m_tid = 0;
			throw SW_EXCEPTION(SWThreadCreationException);
			}
		else
			{
			setFlags(SW_THREAD_FLAG_VALID);
			sw_thread_getThreadId(m_hThread, &m_tid);

			// Now the thread has started set it's priority
			setPriority(m_priority);
			}
		}



// Get the thread ID for this thread.
sw_thread_id_t
SWThread::getThreadId() const
		{
		if (getFlags(SW_THREAD_FLAG_VALID) == 0) throw SW_EXCEPTION(SWThreadInvalidHandleException);

		return m_tid;
		}


// Test to see if the thread is runnable.
bool
SWThread::runnable() const
		{
		//if (getFlags(SW_THREAD_FLAG_VALID) == 0) throw SW_EXCEPTION(SWThreadInvalidHandleException);

		return (getFlags(SW_THREAD_FLAG_VALID) != 0 && getFlags(SW_THREAD_FLAG_RUN) != 0);
		}


// Test to see if the thread has stopped.
bool
SWThread::stopped() const
		{
		//if (getFlags(SW_THREAD_FLAG_VALID) == 0) throw SW_EXCEPTION(SWThreadInvalidHandleException);

		return (getFlags(SW_THREAD_FLAG_VALID) != 0 && getFlags(SW_THREAD_FLAG_STOPPED) != 0);
		}


// Test to see if the thread has started.
bool
SWThread::started() const
		{
		//if (getFlags(SW_THREAD_FLAG_VALID) == 0) throw SW_EXCEPTION(SWThreadInvalidHandleException);

		return (getFlags(SW_THREAD_FLAG_VALID) != 0 && getFlags(SW_THREAD_FLAG_STARTED) != 0);
		}


/**
***	Test to see if the thread is running
**/
bool
SWThread::running() const
		{
		//if (getFlags(SW_THREAD_FLAG_VALID) == 0) throw SW_EXCEPTION(SWThreadInvalidHandleException);

		return (getFlags(SW_THREAD_FLAG_VALID) != 0 && m_tid > 0 && (getFlags(SW_THREAD_FLAG_STOPPED) == 0));
		}


/**
*** Suspend the execution of this thread until it is resumed
**/
sw_status_t
SWThread::suspend()
		{
		if (getFlags(SW_THREAD_FLAG_VALID) == 0) throw SW_EXCEPTION(SWThreadInvalidHandleException);

		return sw_thread_suspend(m_hThread);
		}


/**
*** Resume the execution of this thread.
**/
sw_status_t
SWThread::resume()
		{
		if (getFlags(SW_THREAD_FLAG_VALID) == 0) throw SW_EXCEPTION(SWThreadInvalidHandleException);

		return sw_thread_resume(m_hThread);
		}



sw_status_t
SWThread::setPriority(int priority)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		m_priority = priority;
		if (getFlags(SW_THREAD_FLAG_VALID) != 0)
			{
			res = sw_thread_setPriority(m_hThread, priority);
			}

		return res;
		}



/**
*** Gets the priority of this thread
**/
int
SWThread::getPriority()
		{
		if (getFlags(SW_THREAD_FLAG_VALID) != 0)
			{
			sw_thread_getPriority(m_hThread, &m_priority);
			}

		return m_priority;
		}


/**
***	Return the exit code for the thread.
**/
int
SWThread::getExitCode()
		{
		// Check for a valid handle
		if (getFlags(SW_THREAD_FLAG_VALID) == 0) throw SW_EXCEPTION(SWThreadInvalidHandleException);

		// Check to make sure the thread actually stopped
		if (!stopped()) throw SW_EXCEPTION(SWThreadRunningException);

		return m_exitcode;
		}


// The internal run command called from thr_create
int
SWThread::irun(void *data)
		{
		SWThread	*rpt = (SWThread *)data;

		//sw_log_trace1(_T("Thread %d started"), rpt->_tid);

		rpt->lock();
		rpt->m_flags &= ~SW_THREAD_FLAG_STOPPED;
		rpt->m_flags |= SW_THREAD_FLAG_STARTED;

		// We check the run flag just before starting
		// just in case we've been stopped by another thread
		// before we could get going.
		if (rpt->runnable()) 
			{
			rpt->unlock();
			rpt->m_exitcode = rpt->run();
			rpt->lock();
			}
		rpt->m_flags |= SW_THREAD_FLAG_STOPPED;
		rpt->unlock();

		//sw_log_trace1(_T("Thread %d stopped"), rpt->_tid);

		return rpt->m_exitcode;
		}


void
SWThread::sleep(sw_uint32_t ms)
		{
#ifdef sun
		// Solaris
		poll(0, 0, ms);
#elif defined(WIN32) || defined(_WIN32)
		// Windows
		Sleep(ms);
#else
		// Other Unix
		struct timeval	tv;

		tv.tv_sec = ms / 1000;
		tv.tv_usec = (ms % 1000) * 1000;
		select(0, 0, 0, 0, &tv);
#endif
		}



bool
SWThread::waitFor(int ms, int maxsleepms)
		{
		sw_clockms_t	now;

		if (maxsleepms < 1) maxsleepms = 1;

		now = sw_clock_getCurrentTimeMS();

		while (runnable() && ms > 0)
			{
			if (ms > maxsleepms)
				{
				SWThread::sleep(maxsleepms);
				ms -= maxsleepms;
				}
			else
				{
				SWThread::sleep(ms);
				ms = 0;
				}
			}

		return runnable();
		}


sw_thread_id_t
SWThread::self()
		{
		return sw_thread_self();
		}


void
SWThread::yield()
		{
		sw_thread_yield();
		}


// Set the thread concurrency value
sw_status_t
SWThread::setConcurrency(int v)
		{
		sw_thread_setConcurrency(v);

		return SW_STATUS_SUCCESS;
		}


// Get the thread concurrency value
int		
SWThread::getConcurrency()
		{
		return sw_thread_getConcurrency();
		}


// Clear the thread run flag (as returned by the runnable method)
void
SWThread::setStopFlag()
		{
		clearFlags(SW_THREAD_FLAG_RUN);
		}


// Default run method
int
SWThread::run()
		{
		if (getFlags(SW_THREAD_FLAG_FUNCTION) == 0 || m_tfunc == 0) throw SW_EXCEPTION(SWThreadNotFunctionException);
		return m_tfunc(this, m_param);
		}


// Set internal flags
void
SWThread::setFlags(sw_uint32_t flags)
		{
		SWGuard	guard(this);

		m_flags |= flags;
		}


// Clear internal flags
void
SWThread::clearFlags(sw_uint32_t flags)
		{
		SWGuard	guard(this);

		m_flags &= ~flags;
		}


// test internal flags
sw_uint32_t
SWThread::getFlags(sw_uint32_t flags) const
		{
		SWGuard	guard(this);

		return (m_flags & flags);
		}



