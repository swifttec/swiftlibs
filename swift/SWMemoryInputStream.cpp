/**
*** @file		SWMemoryInputStream.cpp
*** @brief		A memory based input stream.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWMemoryInputStream class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWMemoryInputStream.h>
#include <swift/SWException.h>

#include <memory.h>


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





SWMemoryInputStream::SWMemoryInputStream(const void *pBuffer)
		{
		setBuffer(pBuffer);
		}


SWMemoryInputStream::SWMemoryInputStream(const void *pBuffer, sw_size_t buflen)
		{
		setBuffer(pBuffer, buflen);
		}


void
SWMemoryInputStream::setBuffer(const void *buf)
		{
		_bp = _buf = (const sw_byte_t *)buf;
		_datalen = 0;
		_limitedSize = false;
		}


void
SWMemoryInputStream::setBuffer(const void *buf, sw_size_t len)
		{
		_bp = _buf = (const sw_byte_t *)buf;
		_datalen = len;
		_limitedSize = true;
		}



sw_status_t
SWMemoryInputStream::readData(void *pData, sw_size_t nbytes)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (!_buf)
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}
		else if (!_limitedSize)
			{
			memcpy(pData, _bp, nbytes);
			_bp += nbytes;
			}
		else
			{
			sw_size_t	pos=(sw_size_t)(_bp-_buf);

			if (pos + nbytes <= _datalen)
				{
				memcpy(pData, _bp, nbytes);
				_bp += nbytes;
				}
			else
				{
				memcpy(pData, _bp, _datalen - pos);
				_bp += _datalen - pos;
				nbytes -= _datalen - pos;
				res = SW_STATUS_INSUFFICIENT_DATA;

				// Zero out any remainder
				//if (nbytes > 0) memset(pData, 0, nbytes);
				}
			}

		return res;
		}



sw_status_t
SWMemoryInputStream::rewindStream()
		{
		_bp = _buf;
		return SW_STATUS_SUCCESS;
		}



