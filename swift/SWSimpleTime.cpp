/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"

#include "SWSimpleTime.h"

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif



SWSimpleTime::SWSimpleTime(sw_uint16_t t) :
	m_time(t)
		{
		}


SWSimpleTime::~SWSimpleTime()
		{
		}



SWSimpleTime::operator SWString() const
		{
		SWString	s;
		
		s.format("%02d:%02d", m_time/60, m_time%60);
		
		return s;
		}



#if defined(SW_BUILD_MFC)
SWSimpleTime::operator CString() const
		{
		CString	s;
		
		s.Format(_T("%02d:%02d"), m_time/60, m_time%60);
		
		return s;
		}
#endif


SWSimpleTime &
SWSimpleTime::operator=(const SWString &s)
		{
		int		h=0, m=0;
		
		if (sscanf(s, "%d:%d", &h, &m) == 2)
			{
			if (h < 0) h = 0;
			if (h > 23) h = 23;
			if (m < 0) m = 0;
			if (m > 59) m = 59;
			}
		
		m_time = (h * 60) + m;
		
		return *this;
		}


