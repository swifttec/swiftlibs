/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#pragma once

#include <swift/SWString.h>




class SWIFT_DLL_EXPORT SWSimpleTime
		{
public:
		SWSimpleTime(sw_uint16_t t=0);
		~SWSimpleTime();

		operator sw_uint16_t &()		{ return m_time; }
		operator sw_uint16_t() const	{ return m_time; }
		
		operator SWString() const;

#if defined(SW_BUILD_MFC)
		operator CString() const;
#endif

		SWSimpleTime &		operator=(const SWString &s);
		SWSimpleTime &		operator=(sw_uint16_t v)		{ m_time = v; return *this; }

private:
		sw_uint16_t	m_time;
		};


