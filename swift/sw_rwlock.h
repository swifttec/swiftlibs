/**
*** @file		sw_rwlock.h
*** @brief		Functions for reader/writer mutexes.
*** @version	1.0
*** @author		Simon Sparkes
***
*** This module is intended for internal use for simulating reader/writer
*** mutexes on platforms where they are not available. The interface is
*** modelled on the Solaris one which provides these as part of the O/S.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#ifndef __sw_rwlock_h__
#define __sw_rwlock_h__

#include <swift/swift.h>

#if !defined(SW_PLATFORM_WINDOWS)
	#include <unistd.h>
	#include <sys/time.h>
#endif


#if defined(SW_USE_SOLARIS_RWLOCKS)
	#include <thread.h>
	#include <synch.h>
#elif defined(SW_USE_POSIX_RWLOCKS)
	#include <pthread.h>
#elif !defined(SW_USE_EMULATED_RWLOCKS)
	#error Unsupported platform - please update this file and/or sw_platform.h
#endif






#define SW_RWLOCK_THREAD			1
#define SW_RWLOCK_PROCESS			2

#define SW_RWLOCK_READ				1
#define SW_RWLOCK_READ_NBLOCK		2
#define SW_RWLOCK_WRITE				3
#define SW_RWLOCK_WRITE_NBLOCK		4
#define SW_RWLOCK_WRITE_UPGRADE		5
#define SW_RWLOCK_READ_DOWNGRADE	6


#if defined(SW_USE_EMULATED_RWLOCKS)
	typedef struct sw_rwlock_data
		{
	#if !defined(SW_PLATFORM_WINDOWS)
		pthread_mutex_t		mutex;	///< The mutex
		pthread_cond_t		cond;	///< The condvar
	#endif
		int				nreaders;	///< The number of active readers
		int				rwaiting;	///< The number of threads waiting to become readers
		int				wwaiting;	///< The number of threads waiting to become writers
		int				wtaken;		///< The number of writes taken since the last readlock was acquired
		sw_uint32_t		writerid;	///< The ID of the current writer (if any)
		} sw_rwlock_data_t;
#elif defined(SW_USE_SOLARIS_RWLOCKS)
	typedef rwlock_t			sw_rwlock_data_t;
#elif defined(SW_USE_POSIX_RWLOCKS)
	typedef pthread_rwlock_t	sw_rwlock_data_t;
#endif // defined(SW_USE_EMULATED_RWLOCKS)

typedef struct sw_rwlock
	{
	int					type;	///< The type of the rwlock (thread or process level)
	sw_handle_t			hData;	///< The local handle to the global (named) shared data
	sw_rwlock_data_t	*pData;	///< The pointer to the data
#if defined(SW_PLATFORM_WINDOWS)
	HANDLE				hMutex;	///< The local handle to the global (named) mutex
	HANDLE				hEvent;	///< The local handle to the global (named) event
#endif
	} sw_rwlock_t;


/**
*** @brief	Create a read/writer mutex.
**/
SWIFT_DLL_EXPORT sw_status_t		sw_rwlock_create(sw_rwlock_t *&lock, int type, const sw_tchar_t *name);

/**
*** @brief	Destroy a read/writer mutex.
**/
SWIFT_DLL_EXPORT sw_status_t		sw_rwlock_destroy(sw_rwlock_t *lock);

/**
*** @brief	Acquire a read/writer mutex of the specified type.
**/
SWIFT_DLL_EXPORT sw_status_t		sw_rwlock_acquire(sw_rwlock_t *lock, int type, sw_uint64_t waitms);

/**
*** @brief	Release a read/writer mutex.
**/
SWIFT_DLL_EXPORT sw_status_t		sw_rwlock_release(sw_rwlock_t *lock);





#endif // __SWbase_rwlock_h__
