/**
*** @file		SWStringPtr.h
*** @brief		A string pointer class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWStringPtr class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWStringPtr_h__
#define __SWStringPtr_h__

#include <swift/swift.h>





// Forward declaration
class SWString;

class SWIFT_DLL_EXPORT SWStringPtr
		{
public:
		SWStringPtr(const void *p, int cs);
		SWStringPtr(const SWString &s);
		SWStringPtr(const char *s);
		SWStringPtr(const wchar_t *s);

		const SWStringPtr &	operator++();		//< Prefix increment operator.
		SWStringPtr			operator++(int);	//< Postfix increment operator.
		const SWStringPtr &	operator--();		//< Prefix decrement operator.
		SWStringPtr			operator--(int);	//< Postfix decrement operator.
		SWStringPtr &		operator+=(int v);	//< += operator (int)
		SWStringPtr &		operator-=(int v);	//< -= operator (int)

		int					operator[](int idx) const;
		int					operator*() const;

private:
		const char *	m_ptr;
		int				m_charsize;
		};



#endif
