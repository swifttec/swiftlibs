/**
*** @file		SWFileInputStream.h
*** @brief		A generic socket implementation
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWFileInputStream class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWFileInputStream_h__
#define __SWFileInputStream_h__

#include <swift/SWFile.h>
#include <swift/SWInputStream.h>

/**
*** @brief	A file input stream
***
*** This class takes data from a file and uses it as the input to
*** the input stream.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWFileInputStream : public SWInputStream
		{
public:
		/// Default constructor
		SWFileInputStream();

		/// Constructor
		SWFileInputStream(SWFile *pFile, bool delflag);

		/// Constructor
		SWFileInputStream(SWFile &file);

		/// Constructor
		SWFileInputStream(SWFile &file, sw_filepos_t seekpos);

		/// Virtual destructor
		virtual ~SWFileInputStream();

		/// Returns a pointer to the socket implementation
		SWFile *			getFile()	{ return m_pFile; }

		/// Set the socket implementation
		void				setFile(SWFile *pFile, bool delflag);

		/// Move to a specific position in the file
		sw_status_t			seek(sw_filepos_t pos, SWFile::SeekMethod whence);

protected:
		virtual sw_status_t		rewindStream();							///< Rewind back to the beginning of the file.
		virtual sw_status_t		readData(void *pData, sw_size_t len);	///< Write binary data to the output stream.
		
protected:
		SWFile	*m_pFile;	///< A pointer to the actual socket implementation
		bool	m_delFlag;	///< A flag to indicate if the socket implementator should be delete on destruction
		};




#endif
