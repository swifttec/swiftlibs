/**
*** @file		SWQueue.cpp
*** @brief		Generic synchronised queue based on the SWList class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWQueue class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <swift/SWQueue.h>
#include <swift/SWGuard.h>
#include <errno.h>


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




// PRIVATE: Initialise
void
SWQueue::init(SWThreadMutex *pMutex, bool ownMutex)
		{
		m_pMutex = pMutex;
		m_ownMutex = ownMutex;
		m_flags = SW_QUEUE_OPERATION_ALL;
		m_pcv = new SWThreadCondVar(pMutex);
		synchronize(m_pMutex, false);
		}


SWQueue::SWQueue()
    	{
		init(new SWThreadMutex(true), true);
		}


SWQueue::SWQueue(SWThreadMutex *mu)
    	{
    	init(mu, false);
		}



SWQueue::SWQueue(SWThreadMutex &mu)
    	{
    	init(&mu, false);
		}


SWQueue::~SWQueue()
		{
		shutdown();

		// Unsync this object before we delete any mutexes
		synchronize(NULL, false);

#if !defined(SW_BUILD_DEBUG)
		// We only do this in non-debug mode as there is a very wierd which I've never been
		// able to find despite hours of searching and debugging which causes a crash/exception
		// when running outside of the debugger.
		delete m_pcv;
#endif
		m_pcv = NULL;

		if (m_ownMutex)
			{
			delete m_pMutex;
			m_pMutex = NULL;
			m_ownMutex = false;
			}
		}


void
SWQueue::enable(int flags)
		{
		SWGuard	guard(this);

		m_flags |= (flags & SW_QUEUE_OPERATION_MASK);
		m_flags &= ~(SW_QUEUE_INTERRUPT_WHEN_EMPTY|SW_QUEUE_DISABLE_WHEN_EMPTY);
		}


void
SWQueue::disable(int flags)
		{
		SWGuard	guard(this);

		m_flags &= ~((flags & SW_QUEUE_OPERATION_MASK)|SW_QUEUE_INTERRUPT_WHEN_EMPTY|SW_QUEUE_DISABLE_WHEN_EMPTY);
		if ((flags & SW_QUEUE_OPERATION_GET) != 0) interrupt();
		}

void
SWQueue::interrupt(bool interruptReaderWhenEmpty)
		{
		SWGuard	guard(this);

		if (!interruptReaderWhenEmpty || m_list.size() == 0) 
			{
			//m_flags |= SW_QUEUE_INTERRUPT;
			m_interruptCount = m_waitingCount;
			m_pcv->broadcast();
			}
		else m_flags |= SW_QUEUE_INTERRUPT_WHEN_EMPTY;
		}


bool
SWQueue::add(void *obj)
		{
		if (!isEnabled(SW_QUEUE_OPERATION_ADD)) throw SW_EXCEPTION(SWQueueInactiveException);

		SWGuard	guard(this);

		m_list.add(obj, SWList<void *>::ListTail);
		m_pcv->signal();

		return true;
		}


void *
SWQueue::get(sw_uint64_t waitms, bool peekflag)
		{
		void	*obj=NULL;
		int		r=-1;
		bool	interrupted=false;

		// We use lock/unlock here rather than a guard because
		// we can throw exceptions which don't unlock the mutex
		lock();

		if (!isEnabled(peekflag?SW_QUEUE_OPERATION_PEEK:SW_QUEUE_OPERATION_GET))
			{
			unlock();
			throw SW_EXCEPTION(SWQueueInactiveException);
			}

		while (m_interruptCount > 0)
			{
			unlock();
			SWThread::yield();
			lock();
			}

		while (isEnabled(peekflag?SW_QUEUE_OPERATION_PEEK:SW_QUEUE_OPERATION_GET) /* && !m_flags.isSet(SW_QUEUE_INTERRUPT) */)
			{
			if (m_list.size() != 0)
				{
				r = 1;
				break;
				}
			else
				{
				bool	retry = false;

				if ((m_flags & SW_QUEUE_DISABLE_WHEN_EMPTY) != 0)
					{
					m_flags &= ~(SW_QUEUE_OPERATION_ALL|SW_QUEUE_DISABLE_WHEN_EMPTY);
					retry = true;
					}

				if ((m_flags & SW_QUEUE_INTERRUPT_WHEN_EMPTY) != 0)
					{
					m_flags &= ~(SW_QUEUE_INTERRUPT_WHEN_EMPTY);
					m_interruptCount = m_waitingCount;
					interrupted = true;
					break;
					}

				if (retry) continue;
				}

			sw_status_t	rs;

			m_waitingCount++;
			while ((rs = m_pcv->wait(waitms)) != SW_STATUS_SUCCESS)
				{
				if (errno == EINTR) continue;
				--m_waitingCount;
				unlock();
				
				if (!isEnabled(peekflag?SW_QUEUE_OPERATION_PEEK:SW_QUEUE_OPERATION_GET))
					{
					// The queue has been shutdown.
					throw SW_EXCEPTION(SWQueueInactiveException);
					}
				else if (rs == SW_STATUS_TIMEOUT)
					{
					throw SW_EXCEPTION(SWQueueTimeoutException);
					}
				else
					{
					// Something more serious gone wrong.
					throw SW_EXCEPTION(SWQueueException);
					}
				}
			--m_waitingCount;

			if (m_interruptCount > 0)
				{
				interrupted = true;
				--m_interruptCount;
				break;
				}
			}

		if ((m_flags & (peekflag?SW_QUEUE_OPERATION_PEEK:SW_QUEUE_OPERATION_GET)) == 0)
			{
			unlock();
			throw SW_EXCEPTION(SWQueueInactiveException);
			}

		if (interrupted)
			{
			unlock();
			throw SW_EXCEPTION(SWQueueInterruptedException);
			}

		if (r > 0)
			{
			if (peekflag) m_list.get(SWList<void *>::ListHead, obj);
			else m_list.remove(SWList<void *>::ListHead, &obj);
			}

		unlock();

		return obj;
		}


void *
SWQueue::peek(sw_uint64_t waitms)
		{
		return get(waitms, true);
		}


sw_size_t
SWQueue::size() const
		{
		SWGuard	guard(this);

		return m_list.size();
		}


void
SWQueue::clear(bool emptyInactiveQueue)
		{
		if (!isActive() && !emptyInactiveQueue) throw SW_EXCEPTION(SWQueueInactiveException);

		SWGuard		guard(this);

		m_list.clear();
		}


void
SWQueue::shutdown(bool disableReaderWhenEmpty)	
		{
		SWGuard	guard(this);

		if (!disableReaderWhenEmpty || m_list.size() == 0) disable();
		else m_flags |= SW_QUEUE_DISABLE_WHEN_EMPTY;
		interrupt();
		}



