/**
*** @file		SWThreadEvent.cpp
*** @brief		Windows-style events.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWThreadEvent class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <swift/SWThreadEvent.h>
#include <swift/SWException.h>
#include <swift/SWGuard.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





// Protected Constructor
SWThreadEvent::SWThreadEvent(EventType type, bool initialstate) :
	SWEvent(type),
#if defined(WIN32) || defined(_WIN32)
	m_hEvent(0),
#else
	m_signalled(initialstate),
#endif
	m_waiting(0),
	m_closed(false)
		{
#if defined(WIN32) || defined(_WIN32)
		m_hMutex = sw_mutex_create(SW_MUTEX_TYPE_RECURSIVE_THREAD, NULL);
		m_hEvent = CreateEvent(NULL, (type==ManualReset)?TRUE:FALSE, initialstate, NULL);
#else
		m_hCondVar = sw_condvar_create(SW_CONDVAR_TYPE_THREAD);
#endif
		}


SWThreadEvent::~SWThreadEvent()
		{
		close();

#if defined(WIN32) || defined(_WIN32)
		::CloseHandle(m_hEvent);
		m_hEvent = NULL;
#else
		sw_condvar_destroy(m_hCondVar);
		m_hCondVar = NULL;
#endif
		}


void
SWThreadEvent::close()
		{
		sw_mutex_t	hMutex;

#if defined(WIN32) || defined(_WIN32)
		hMutex = m_hMutex;
#else
		hMutex = sw_condvar_getMutex(m_hCondVar);
#endif

		sw_mutex_acquire(hMutex);

		if (!m_closed)
			{
			m_closed = true;

#if defined(WIN32) || defined(_WIN32)
			// Wake up any waiting threads so they given an exception
			while (m_waiting > 0)
				{
				sw_mutex_release(hMutex);
				::SetEvent(m_hEvent);
				SWThread::yield();
				sw_mutex_acquire(hMutex);
				}
#else
			sw_condvar_close(m_hCondVar);
#endif
			}

		sw_mutex_release(hMutex);

#if defined(WIN32) || defined(_WIN32)
		sw_mutex_destroy(m_hMutex);
		m_hMutex = NULL;
#endif

		}


// Wait for the event to become signalled. The event will auto-reset to non-signalled.
sw_status_t
SWThreadEvent::wait(sw_uint64_t waitms)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		sw_mutex_t	hMutex;

#if defined(WIN32) || defined(_WIN32)
		hMutex = m_hMutex;
#else
		hMutex = sw_condvar_getMutex(m_hCondVar);
#endif

		sw_mutex_acquire(hMutex);
		if (m_closed)
			{
			sw_mutex_release(hMutex);
			throw SW_EXCEPTION(SWEventException);
			}

		m_waiting++;

#if defined(WIN32) || defined(_WIN32)
		DWORD	ms, dw;

		if (waitms == SW_TIMEOUT_INFINITE) ms = INFINITE;
		else ms = (DWORD)waitms;

		// Release the mutex whilst we wait
		sw_mutex_release(hMutex);

		dw = ::WaitForSingleObject(m_hEvent, ms);
		if (dw == WAIT_TIMEOUT) res = SW_STATUS_TIMEOUT;
		else if (dw == WAIT_OBJECT_0) res = SW_STATUS_SUCCESS;
		else res = ::GetLastError();

		// Re-acquire the mutex
		sw_mutex_acquire(hMutex);

#else // defined(WIN32) || defined(_WIN32)
		// We must wait until we become signalled
		if (!m_signalled)
			{
			try
				{
				res = sw_condvar_timedWait(m_hCondVar, waitms);
				}
			catch (SWCondVarException &)
				{
				}
			}

		// We are now in a signalled state
		if (m_type == AutoReset)
			{
			m_signalled = false;
			}

#endif // defined(WIN32) || defined(_WIN32)

		--m_waiting;
		sw_mutex_release(hMutex);

		if (m_closed)
			{
			sw_mutex_release(hMutex);
			throw SW_EXCEPTION(SWEventException);
			}

		return res;
		}



// Reset the event if no thread is waiting, set to signaled state if thread(s) are waiting, wake up one waiting thread and reset event.
void
SWThreadEvent::set()
		{
		sw_mutex_t	hMutex;

#if defined(WIN32) || defined(_WIN32)
		hMutex = m_hMutex;
#else
		hMutex = sw_condvar_getMutex(m_hCondVar);
#endif

		sw_mutex_acquire(hMutex);

		if (m_closed)
			{
			sw_mutex_release(hMutex);
			throw SW_EXCEPTION(SWEventException);
			}

#if defined(WIN32) || defined(_WIN32)
		::SetEvent(m_hEvent);
#else
		try
			{
			m_signalled = true;
			if (m_type == AutoReset)
				{
				// Wake up just one thread
				sw_condvar_signal(m_hCondVar);
				}
			else
				{
				// Wake up all threads
				sw_condvar_broadcast(m_hCondVar);
				}
			}
		catch (SWCondVarException &)
			{
			sw_mutex_release(hMutex);
			throw SW_EXCEPTION(SWEventException);
			}

		if (m_closed)
			{
			sw_mutex_release(hMutex);
			throw SW_EXCEPTION(SWEventException);
			}
#endif

		sw_mutex_release(hMutex);
		}



// Wake up one thread (if present) and reset the event.
void
SWThreadEvent::pulse()
		{
		sw_mutex_t	hMutex;

#if defined(WIN32) || defined(_WIN32)
		hMutex = m_hMutex;
#else
		hMutex = sw_condvar_getMutex(m_hCondVar);
#endif

		sw_mutex_acquire(hMutex);

		if (m_closed)
			{
			sw_mutex_release(hMutex);
			throw SW_EXCEPTION(SWEventException);
			}

#if defined(WIN32) || defined(_WIN32)
		::PulseEvent(m_hEvent);
#else
		try
			{
			if (m_type == AutoReset)
				{
				// Wake up just one thread
				sw_condvar_signal(m_hCondVar);
				}
			else
				{
				// Wake up all threads
				sw_condvar_broadcast(m_hCondVar);
				}
			}
		catch (SWCondVarException &)
			{
			sw_mutex_release(hMutex);
			throw SW_EXCEPTION(SWEventException);
			}

		if (m_closed)
			{
			sw_mutex_release(hMutex);
			throw SW_EXCEPTION(SWEventException);
			}
#endif

		sw_mutex_release(hMutex);
		}



// Reset the event to a non-signalled state
void
SWThreadEvent::reset()
		{
		sw_mutex_t	hMutex;

#if defined(WIN32) || defined(_WIN32)
		hMutex = m_hMutex;
#else
		hMutex = sw_condvar_getMutex(m_hCondVar);
#endif

		sw_mutex_acquire(hMutex);

		if (m_closed)
			{
			sw_mutex_release(hMutex);
			throw SW_EXCEPTION(SWEventException);
			}

#if defined(WIN32) || defined(_WIN32)
		::ResetEvent(m_hEvent);
#else
		m_signalled = false;
#endif

		sw_mutex_release(hMutex);
		}





