/**
*** @file		SWBufferReference.h
*** @brief		A general purpose memory buffer
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWBufferReference class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWBufferReference_h__
#define __SWBufferReference_h__

#include <swift/SWAbstractBuffer.h>





/**
*** @brief General purpose memory buffer reference class.
***
*** This is a variation on the SWBuffer class that can be used to
*** reference an external buffer. There are 2 forms of reference
*** available, a read-only reference which will prevent operations
*** that modify the buffer, and a read-write reference which will
*** allow buffer modification but will not resize the buffer or allow
*** the buffer capacity to be modified.
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Safe when synchronized</td>
*** </tr>
*** </table>
***
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWBufferReference : public SWAbstractBuffer
		{
public:
		/**
		*** @brief	Construct a read-write buffer reference.
		***
		*** This form of SWBufferReference constructor consturcts a buffer
		*** object that is writeable. The buffer can be written up to the
		*** capacity specified. The data changes will be stored in the memory
		*** location specified in the constructor. Any operation that would cause
		*** the buffer capacity to be exceeded will throw an exception.
		***
		*** @param[in]	ptr			A pointer to the buffer to reference.
		*** @param[in]	capacity	The capacity (maximum size) of the buffer.
		**/
		SWBufferReference(void *ptr, sw_size_t capacity, bool readonly=false);


		// Read-only reference
		SWBufferReference(const void *ptr, sw_size_t capacity);

		/// Virtual destructor
		virtual ~SWBufferReference();
		};





#endif
