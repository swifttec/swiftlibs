/**
*** @file		SWFlags.h
*** @brief		A class to represent a set of flags.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWFlags class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#ifndef __SWFlags_h__
#define __SWFlags_h__

#include <swift/swift.h>
#include <swift/SWBitSet.h>
#include <swift/SWIntegerArray.h>
#include <swift/SWString.h>


/**
*** @brief	A class to represent a set of boolean flags where the number of flags is variable
***			or too big to fit into a single integer.
***
*** This class to represents a set of boolean flags where the number of flags
*** can vary at runtime or there are too many flags to fit into a single integer,
*** i.e. more than 64 bits for most platforms.
***
*** The underlying storage uses an array of integers to store the boolean flags.
*** one bit for each flag.
***
*** This class is particularly useful where
*** the bits represent some kind of map (e.g. 1 bit per disk sector on a disk).
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWFlags : public SWBitSet
		{
public:
		/// Default constructor (specifiying number of bits and optional fixed size)
		SWFlags(int nbits=0, bool fixedSize=false);

		/// String constructor (specifiying initial value and optional fixed size)
		SWFlags(const SWString &s, bool fixedSize=false);

		/// Copy constructor
		SWFlags(const SWFlags &bs);

		/// Destructor
		virtual ~SWFlags()	{ }

		/**
		*** @brief	Clear the bit set.
		***
		*** This has slightly different effects
		*** depending on whether or not the bit set has a fixed length.
		***
		*** If the bit set is of a fixed length then all the bits are
		*** set to zero but the length is not changed.
		***
		*** If the bit set is not of a fixed length then the length is
		*** set to 0
		**/
		void				clear();

		/**
		*** @brief	Set all the bits in this set to 0. The length of the set is not changed.
		**/
		void				clearAllBits();

		/**
		*** @brief	Set all the bits in this set to 1. The length of the set is not changed.
		**/
		void				setAllBits();

		/**
		*** @brief	Toggle all the bits in this set to (1->0 or 0->1). The length of the set is not changed.
		**/
		void				toggleAllBits();

		/**
		*** @brief			Set the specified bit to the specified value (default true)
		***
		*** @param[in]	n	The index of the bit to set
		*** @param[in]	v	The value (true or false) to set the specified bit to.
		**/
		void				setBit(int n, bool v=true);

		/**
		*** @brief			Clear the specified bit
		***
		*** @param[in]	n	The index of the bit to clear
		**/
		void				clearBit(int n)		{ setBit(n, false); }

		/**
		*** @brief			Toggle the specified bit (1->0 or 0->1)
		***
		*** @param[in]	n	The index of the bit to toggle
		**/
		void				toggleBit(int n)	{ setBit(n, !getBit(n)); }

		/**
		*** @brief			Get the specified bit
		***
		*** @param[in]	n	The index of the bit to toggle
		*** @return			The value (true or false) of the specified bit
		**/
		bool				getBit(int n) const;

		/**
		*** @brief	Get the number of bits
		**/
		int					size() const		{ return _nbits; }

		/**
		*** @brief	Return the fixed size flag
		**/
		bool				isFixedSize() const	{ return _fixedSize; }

		/**
		*** @brief	Index operator
		**/
		bool				operator[](int i)	{ return getBit(i); }

		/**
		*** @brief	Assignment operator
		**/
		const SWFlags &	operator=(const SWFlags &);

		/**
		*** @brief	String Assignment operator
		**/
		const SWFlags &	operator=(const SWString &);

		/**
		*** @brief	Assigns the bit set directly from the integer passed.
		***
		*** If the bit set is not of a fixed size it assumes the number of bits
		*** held by a native integer (which is the underlying storage unit).
		***
		*** If the bit set has a fixed size then only the relavent bits are set.
		**/
		const SWFlags &	operator=(int v);


		/// AND operator
		SWFlags &	operator&=(const SWFlags &);

		/// OR operator
		SWFlags &	operator|=(const SWFlags &);

		/// XOR operator
		SWFlags &	operator^=(const SWFlags &);


		/// AND operator (string)
		SWFlags &	operator&=(const SWString &str)	{ SWFlags	v(str); *this &= v; return *this; }

		/// OR operator (string)
		SWFlags &	operator|=(const SWString &str)	{ SWFlags	v(str); *this |= v; return *this; }

		/// XOR operator (string)
		SWFlags &	operator^=(const SWString &str)	{ SWFlags	v(str); *this ^= v; return *this; }


		/// AND operator (int)
		SWFlags &	operator&=(int i)		{ SWFlags v;	v=i; *this &= v; return *this; }

		/// OR operator (int)
		SWFlags &	operator|=(int i)		{ SWFlags v;	v=i; *this |= v; return *this; }

		/// XOR operator (int)
		SWFlags &	operator^=(int i)		{ SWFlags v;	v=i; *this ^= v; return *this; }


		/**
		*** @brief	Return a representation of this bit set as a string
		**/
		virtual SWString	toString();

		/// Dump this object
		virtual void		dump();


public: // virtual overrides

		/// Override for SWObject writeToStream
		//virtual sw_status_t		writeToStream(SWOutputStream &stream) const;

		/// Override for SWObject readFromStream
		//virtual sw_status_t		readFromStream(SWInputStream &stream);


private: // Methods

		/// Private method for getting the index into the arry for the given bit
		void			calcIndexAndOffset(int n, int &index, int &offset) const;
		void			getIndexAndOffset(int n, int &index, int &offset);

private:
		SWIntegerArray	_array;		///< The array of bits (represented by integers)
		int				_nbits;		///< The number of bits represented by this object
		bool			_fixedSize;	///< Flag to indicate if this bit set can grow or not
		};




#endif
