/**
*** @file		SWFileOutputStream.cpp
*** @brief		A generic socket implementation
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWFileOutputStream class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <swift/SWFileOutputStream.h>
#include <swift/SWFile.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




/// Default constructor
SWFileOutputStream::SWFileOutputStream() :
	m_pFile(NULL),
	m_delFlag(false)
		{
		}

SWFileOutputStream::SWFileOutputStream(SWFile *pFile, bool delflag) :
	m_pFile(pFile),
	m_delFlag(delflag)
		{
		}


SWFileOutputStream::SWFileOutputStream(SWFile &file) :
	m_pFile(&file),
	m_delFlag(false)
		{
		}


SWFileOutputStream::SWFileOutputStream(SWFile &file, sw_filepos_t seekpos) :
	m_pFile(&file),
	m_delFlag(false)
		{
		m_pFile->lseek(seekpos, SWFile::SeekSet);
		}


/// Virtual destructor
SWFileOutputStream::~SWFileOutputStream()
		{
		if (m_delFlag)
			{
			delete m_pFile;
			m_pFile = 0;
			m_delFlag = false;
			}
		}


sw_status_t
SWFileOutputStream::flushData()
		{
		if (m_pFile == NULL)
			{
			throw SW_EXCEPTION(SWIllegalStateException);
			}

		return m_pFile->flush();
		}


sw_status_t
SWFileOutputStream::rewindStream()
		{
		if (m_pFile == NULL)
			{
			throw SW_EXCEPTION(SWIllegalStateException);
			}

		return m_pFile->lseek(0, SWFile::SeekSet);
		}


sw_status_t
SWFileOutputStream::writeData(const void *pData, sw_size_t len)
		{
		if (m_pFile == NULL)
			{
			throw SW_EXCEPTION(SWIllegalStateException);
			}

		sw_status_t	res=SW_STATUS_SUCCESS;
		sw_size_t	count;

		do
			{
			res = m_pFile->write(pData, len, &count);
			if (res == SW_STATUS_SUCCESS)
				{
				len -= (sw_uint32_t)count;
				pData = (char *)pData + count;
				}
			}
		while (res == SW_STATUS_SUCCESS && len > 0);

		return res;
		}



void
SWFileOutputStream::setFile(SWFile *pFile, bool delflag)
		{
		if (m_pFile != NULL && pFile != m_pFile && m_delFlag)
			{
			// Delete the previously held socket
			delete m_pFile;
			}

		m_pFile = pFile;
		m_delFlag = delflag;
		}




