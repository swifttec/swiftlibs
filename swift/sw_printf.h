/**
*** @file		sw_printf.h
*** @brief		A collection of extended printf functions
*** @version	1.0
*** @author		Simon Sparkes
***
*** This file contains a collection of extended printf functions.
*** The group of sw_printf functions attempt to use a consistent naming convention
*** to make it easy to pick the correct one. The printf keyword is prefixed by the
*** character size qualifier (d=double, q=quad, w=wide, t=TCHAR, none=char).
*** 
*** The format specifier for sw_printf is consistent across all platforms, unlike the
*** format for the platform-specific printf function. Where possible the format
*** string is consistent with that expected with the normal printf. Indeed the functionality
*** of sw_printf is verified using the standard printf as a comparision allowing for
*** the platform-specific variations in the test program.
*** 
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __sw_printf_h__
#define __sw_printf_h__

#include <swift/swift.h>
#include <swift/SWTextFile.h>

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <wchar.h>

#if defined(SW_PLATFORM_SOLARIS)
	#include <widec.h>
#endif





/**
*** @brief		Defines a custom output function
***
*** This typedef defines a callback function to bed used in conjunction
*** with sw_xprintf. The function is called to output a single
*** character to the custom output stream.
***
*** The first parameter (c) specifies the character to be output, the 
*** second parameter (p) is the data pointer passed to sw_xprintf.
***
*** @param	c	The character to output.
*** @param	p	The data pointer passed to sw_xprintf.
***
*** @see sw_xprintf
***
**/
typedef void (sw_printf_outputfunction)(int c, void *p);



/**
*** @brief	print using single-byte character string format to stdout.
***
*** @param	fmt		The format string (see <a href="../userguide/sw_printf.html">the sw_printf userguide</a> for more details).
**/
SWIFT_DLL_EXPORT int	sw_printf(const char *fmt, ...);

/**
*** @brief	print using single-byte character string format to a single-byte character string buffer with buffer size parameter.
***
*** @param	fmt		The format string (see <a href="../userguide/sw_printf.html">the sw_printf userguide</a> for more details).
*** @param	buf		The string buffer which the result is output to.
*** @param	size	The size of the buffer for output in characters (not bytes). The output will not exceed size characters
***					the nul terminate character.
**/
SWIFT_DLL_EXPORT int	sw_snprintf(char *buf, int size, const char *fmt, ...);

/**
*** @brief	print using single-byte character string format to the specified FILE handle.
***
*** @param	fmt		The format string (see <a href="../userguide/sw_printf.html">the sw_printf userguide</a> for more details).
*** @param	f		The stdio file handle which the result is output to.
**/
SWIFT_DLL_EXPORT int	sw_fprintf(FILE *f, const char *fmt, ...);
SWIFT_DLL_EXPORT int	sw_fprintf(SWTextFile &f, const char *fmt, ...);

/**
*** @brief	print using single-byte character string format to the specified output function.
***
*** @param	fmt		The format string (see <a href="../userguide/sw_printf.html">the sw_printf userguide</a> for more details).
*** @param	func	The custom output function to call for each character to be output.
*** @param	data	The void * data pointer to pass to the output function.
**/
SWIFT_DLL_EXPORT int	sw_xprintf(sw_printf_outputfunction *func, void *data, const char *fmt, ...);



/**
*** @brief	print using single-byte character string format and arglist with output to stdout.
***
*** @param	fmt		The format string (see <a href="../userguide/sw_printf.html">the sw_printf userguide</a> for more details).
*** @param	ap		The varargs parameter list.
**/
SWIFT_DLL_EXPORT int	sw_vprintf(const char *fmt, va_list ap);

/**
*** @brief	print using single-byte character string format and arglist with output to a single-byte character string buffer with buffer size parameter.
***
*** @param	fmt		The format string (see <a href="../userguide/sw_printf.html">the sw_printf userguide</a> for more details).
*** @param	buf		The string buffer which the result is output to.
*** @param	size	The size of the buffer for output in characters (not bytes). The output will not exceed size characters
***					the nul terminate character.
*** @param	ap		The varargs parameter list.
**/
SWIFT_DLL_EXPORT int	sw_vsnprintf(char *buf, int size, const char *fmt, va_list ap);

/**
*** @brief		print using single-byte character string format and arglist with output to the specified FILE handle.
***
*** @param	fmt		The format string (see <a href="../userguide/sw_printf.html">the sw_printf userguide</a> for more details).
*** @param	f		The stdio file handle which the result is output to.
*** @param	ap		The varargs parameter list.
**/
SWIFT_DLL_EXPORT int	sw_vfprintf(FILE *f, const char *fmt, va_list ap);
SWIFT_DLL_EXPORT int	sw_vfprintf(SWTextFile &f, const char *fmt, va_list ap);

/**
*** @brief	print using single-byte character string format and arglist with output to the specified output function.
***
*** @param	fmt		The format string (see <a href="../userguide/sw_printf.html">the sw_printf userguide</a> for more details).
*** @param	func	The custom output function to call for each character to be output.
*** @param	data	The void * data pointer to pass to the output function.
*** @param	ap		The varargs parameter list.
**/
SWIFT_DLL_EXPORT int	sw_vxprintf(sw_printf_outputfunction *func, void *data, const char *fmt, va_list ap);



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



/**
*** @brief	print using wide character string format to stdout.
***
*** @param	fmt		The format string (see <a href="../userguide/sw_printf.html">the sw_printf userguide</a> for more details).
**/
SWIFT_DLL_EXPORT int	sw_wprintf(const wchar_t *fmt, ...);

/**
*** @brief	print using wide character string format to a wide character string buffer with buffer size parameter.
***
*** @param	fmt		The format string (see <a href="../userguide/sw_printf.html">the sw_printf userguide</a> for more details).
*** @param	buf		The string buffer which the result is output to.
*** @param	size	The size of the buffer for output in characters (not bytes). The output will not exceed size characters
***					the nul terminate character.
**/
SWIFT_DLL_EXPORT int	sw_snwprintf(wchar_t *buf, int size, const wchar_t *fmt, ...);

/**
*** @brief	print using wide character string format to the specified FILE handle.
***
*** @param	fmt		The format string (see <a href="../userguide/sw_printf.html">the sw_printf userguide</a> for more details).
*** @param	f		The stdio file handle which the result is output to.
**/
SWIFT_DLL_EXPORT int	sw_fwprintf(FILE *f, const wchar_t *fmt, ...);
SWIFT_DLL_EXPORT int	sw_fwprintf(SWTextFile &f, const wchar_t *fmt, ...);

/**
*** @brief	print using wide character string format to the specified output function.
***
*** @param	fmt		The format string (see <a href="../userguide/sw_printf.html">the sw_printf userguide</a> for more details).
*** @param	func	The custom output function to call for each character to be output.
*** @param	data	The void * data pointer to pass to the output function.
**/
SWIFT_DLL_EXPORT int	sw_xwprintf(sw_printf_outputfunction *func, void *data, const wchar_t *fmt, ...);



/**
*** @brief	print using wide character string format and arglist with output to stdout.
***
*** @param	fmt		The format string (see <a href="../userguide/sw_printf.html">the sw_printf userguide</a> for more details).
*** @param	ap		The varargs parameter list.
**/
SWIFT_DLL_EXPORT int	sw_vwprintf(const wchar_t *fmt, va_list ap);

/**
*** @brief	print using wide character string format and arglist with output to a wide character string buffer with buffer size parameter.
***
*** @param	fmt		The format string (see <a href="../userguide/sw_printf.html">the sw_printf userguide</a> for more details).
*** @param	buf		The string buffer which the result is output to.
*** @param	size	The size of the buffer for output in characters (not bytes). The output will not exceed size characters
***					the nul terminate character.
*** @param	ap		The varargs parameter list.
**/
SWIFT_DLL_EXPORT int	sw_vsnwprintf(wchar_t *buf, int size, const wchar_t *fmt, va_list ap);

/**
*** @brief	print using wide character string format and arglist with output to the specified FILE handle.
***
*** @param	fmt		The format string (see <a href="../userguide/sw_printf.html">the sw_printf userguide</a> for more details).
*** @param	f		The stdio file handle which the result is output to.
*** @param	ap		The varargs parameter list.
**/
SWIFT_DLL_EXPORT int	sw_vfwprintf(FILE *f, const wchar_t *fmt, va_list ap);
SWIFT_DLL_EXPORT int	sw_vfwprintf(SWTextFile &f, const wchar_t *fmt, va_list ap);

/**
*** @brief	print using wide character string format and arglist with output to the specified output function.
***
*** @param	fmt		The format string (see <a href="../userguide/sw_printf.html">the sw_printf userguide</a> for more details).
*** @param	func	The custom output function to call for each character to be output.
*** @param	data	The void * data pointer to pass to the output function.
*** @param	ap		The varargs parameter list.
**/
SWIFT_DLL_EXPORT int	sw_vxwprintf(sw_printf_outputfunction *func, void *data, const wchar_t *fmt, va_list ap);



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////






#endif
