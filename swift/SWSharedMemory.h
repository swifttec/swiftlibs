/**
*** @file		SWSharedMemory.h
*** @brief		An object for handling shared memory segments.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWSharedMemory class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWSharedMemory_h__
#define __SWSharedMemory_h__

#include <swift/SWException.h>
#include <swift/SWLockableObject.h>

// Begin namespace



/**
*** @brief	A shared memory implementation.
***
*** The SWSharedMemory object provides a shared memory interface that
*** is consistent across platforms. On platforms where shared memory
*** is not available (e.g. Windows) then it is emulated.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWSharedMemory : public SWLockableObject
		{
public:
		/**
		*** @brief	Default constructor - no memory attached to or created.
		***
		*** No shared memory segment is attached to, nor is any shared memory
		*** segment created whewn using the default constructor. The caller should
		*** use the attach or create methods.
		**/
		SWSharedMemory();

		/// Virtual destructor
		virtual ~SWSharedMemory();

		/**
		*** @brief	Constructor for pre-existing segment.
		***
		*** Constructs a shared memory object and then calls the attach function.
		*** If the attach fails (normally because the segment specified does not
		*** exist) then this constructor will throw an exception.
		***
		*** @param[in]	id		The id of the shared memory segment to attach to.
		**/
		SWSharedMemory(sw_uint32_t id);

		/**
		*** @brief	Constructor for noe-existing segment.
		***
		*** Constructs a shared memory object and then calls the create function.
		*** If the create fails (normally because the segment specified already
		*** exist) then this constructor will throw an exception.
		***
		*** @param[in]	id		The id of the shared memory segment to create.
		*** @param[in]	size	The size in bytes of the shared memory to create.
		**/
		SWSharedMemory(sw_uint32_t id, sw_uint32_t size);

		/**
		*** @brief	Return the loaded address of the shared memory.
		***
		*** In order to access the shared memory developers need to call the 
		*** address method to determine the memory address that the shared memory
		*** is located at.
		***
		*** @note
		*** No guards are implemented to prevent a program writing beyond
		*** the scope of shared memory segment so developers should call the size method
		*** to determine the extent of the memory segment.
		**/
		void *			address() const	{ return _addr; }

		/**
		*** @brief	Return the ID of this shared memory segment.
		**/
		sw_uint32_t		id() const		{ return _id; }

		/**
		*** @brief	Return the size of this shared memory segment.
		**/
		sw_uint32_t		size() const	{ return _size; }

		/**
		*** @brief	Create a shared memory segment with the specified ID and size.
		***
		*** This method creates a shared memory segment with the specified ID and size
		*** and then attaches it to this object.
		***
		*** @param[in]	id		The id of the shared memory segment to create.
		*** @param[in]	size	The size in bytes of the shared memory to create.
		**/
		sw_status_t		create(sw_uint32_t id, sw_uint32_t size);

		/**
		*** @brief	Attach to the shared memory segment with the specified ID.
		***
		*** This method attaches the object to the shared memory segment with the specified ID.
		***
		*** @param[in]	id		The id of the shared memory segment to attach to.
		**/
		sw_status_t		attach(sw_uint32_t id);

		/**
		*** @brief	Detach the object from a shared memory segment.
		***
		*** This method detaches the object from the shared memory segment it
		*** is associated with.
		***
		*** @param	delflag		If true the segment is deleted after being detached.
		**/
		sw_status_t		detach(bool delflag=false);

		/// Verify if the object is currently attached to a shared memory segment
		bool			isAttached() const;

public: // STATIC
		/**
		*** @brief Remove the shared memory segment with the ID.
		**/
		static sw_status_t		remove(sw_uint32_t id);

protected:
		void				*_addr;			///< Attached address
		int					_id;			///< Shared memory identifier
		int					_size;			///< The size of the shared memory segment
#if defined(WIN32) || defined(_WIN32)
		HANDLE				_fileHandle;	///< A handle to the shared memory file
		HANDLE				_memHandle;		///< A handle to the mapped memory segment.
#else
		int					_handle;		///< A handle to the shared memory
#endif
		};

// Exceptions
SW_DEFINE_EXCEPTION(SWSharedMemoryException, SWException);

// End namespace


#endif
