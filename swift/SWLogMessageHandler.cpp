/**
*** @file		SWLogMessageHandler.cpp
*** @brief		A class to define an abstract class for handling log messages.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWLogMessageHandler class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWLogMessageHandler.h>
#include <swift/SWLog.h>



// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif



SWLogMessageHandler::SWLogMessageHandler() :
	m_pOwner(NULL),
	m_typeMask(SW_LOG_TYPE_MASK),
	m_featureMask(SW_CONST_UINT64(~0)),
#ifdef SW_BUILD_DEBUG
	m_logLevel(SW_LOG_LEVEL_DEBUG)
#else
	m_logLevel(SW_LOG_LEVEL_SUMMARY)
#endif	
		{
		}


SWLogMessageHandler::~SWLogMessageHandler()
		{
		}


void
SWLogMessageHandler::formatMsg(SWString &str, const SWLogMessage &msg) const
		{
		// First of all get the format string
		SWString	fmt=format();
		SWLog		*pLog=owner();

		while (fmt.isEmpty() && pLog != NULL)
			{
			fmt = pLog->format();
			pLog = pLog->owner();
			}

		if (fmt.isEmpty())
			{
			// Use a default value
			fmt = SW_LOG_FORMAT_DEFAULT;
			}

		// Now format the msg into a string using the fmt string
		msg.format(str, fmt);
		}




