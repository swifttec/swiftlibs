/**
*** @file		SWLogStdioFileHandler.h
*** @brief		A class write messages to a FILE.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWLogStdioFileHandler class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWLogStdioFileHandler_h__
#define __SWLogStdioFileHandler_h__

#include <swift/SWLogMessage.h>
#include <swift/SWLogMessageHandler.h>

#include <swift/SWString.h>






/**
*** @brief		A class write messages to a FILE.
***
*** This class handles messages from a SWLog object and writes them to a FILE
*** which can be specified by FILE handle or by file name. When used with a 
*** FILE handle this object can be requested to optionally close the FILE handle
*** when this object is destroyed.
***
*** When a filename is specified this handler can optionally limit the size of a
*** logfile and how many backup copies are made. This is described in detail in
*** the filename constructor.
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Unsafe</td>
*** </tr>
*** </table>
***
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWLogStdioFileHandler : public SWLogMessageHandler
		{
public:
		/**
		*** @brief	Constuct a log message handler that writes messages to the specified FILE handle.
		***
		*** @param	f			The FILE handle to write to.
		*** @param	closeFlag	If true the FILE handle specified is closed when this object is destroyed.
		**/
		SWLogStdioFileHandler(FILE *f, bool closeFlag);

		/**
		*** @brief	Constuct a log messagge handler that writes messages to the specified file.
		***
		*** This constructor creates a handler that will write to the specified filename. The
		*** caller can specify an optional maximum size and backup count. If both of these 
		*** parameters are specified with non-zero value they are used as follows:
		***
		*** When the current size of the file (in bytes) reaches the specified maximum file size
		*** (maxsize parameter) the file is closed and renamed. The renaming scheme is dependent
		*** on the backupcount parameter. If backupcount has the value 1 the file is renamed to 
		*** filename.old and a new file is created.
		***
		*** If the backupcount is greater than 1 then the file is renamed as filename.2 (filename.2
		*** renamed as filename.3 etc) thus ending with a maximum of backupcount files.
		**/
		SWLogStdioFileHandler(const SWString &filename, sw_size_t maxsize=0, int backupcount=0);

		/// Virtual destructor
		virtual ~SWLogStdioFileHandler();

		/// Set the max file size (in bytes)
		void			maxFileSize(sw_size_t v)		{ m_maxFileSize = v; }

		/// Get the max file size (in bytes)
		sw_size_t		maxFileSize() const				{ return m_maxFileSize; }

		/// Set the backup file count
		void			backupCount(int v)				{ m_backupFileCount = v; }

		/// Get the backup file count
		int				backupCount() const				{ return m_backupFileCount; }

public: // Virtual method overrides
		virtual sw_status_t	open();
		virtual sw_status_t	process(const SWLogMessage &lm);
		virtual sw_status_t	flush();
		virtual sw_status_t	close();

private:
		SWString	m_filename;			///< The file name
		FILE		*m_file;			///< The output stream for the file
		bool		m_closeFlag;		///< If true and in FILE handle mode the file is close on destruction
		sw_size_t	m_maxFileSize;		///< The maximum size of a single log file.
		int			m_backupFileCount;	///< The number of backup files to create
		};




#endif
