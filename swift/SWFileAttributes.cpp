/**
*** @file		SWFileAttributes.cpp
*** @brief		A class representing a single directory entry
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWFileAttributes class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <swift/SWFileAttributes.h>
#include <swift/SWFilename.h>
#include <swift/SWTokenizer.h>

#if defined(SW_PLATFORM_WINDOWS)
	#include <winioctl.h>
#else
	#include <sys/stat.h>
#endif


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


SWFileAttributes::SWFileAttributes() :
	m_type(FileTypeUnknown),
	m_attribs(0)
		{
		}


// Copy constructor
SWFileAttributes::SWFileAttributes(const SWFileAttributes &other) :
	m_type(FileTypeUnknown),
	m_attribs(0)
		{
		*this = other;
		}


// Assignment operator
SWFileAttributes &
SWFileAttributes::operator=(const SWFileAttributes &other)
		{
#define COPY(x)	x = other.x
		COPY(m_attribs);
#undef COPY

		return *this;
		}


SWFileAttributes::SWFileAttributes(FileType type, sw_fileattr_t attribs, SWFileSecurity *psd) :
	m_type(type),
	m_attribs(attribs)
		{
		}


/*
** Get the mode as an ls-like string
** This is a 10 or 11 character string
**
** First char
** d     the entry is a directory;
** D     the entry is a door;
** l     the entry is a symbolic link;
** b     the entry is a block special file;
** c     the entry is a character special file;
** p     the entry is a fifo (or "named pipe") special file;
** s     the entry is an AF_UNIX address family socket;
** -     the entry is an ordinary file;
**
** Next 3 groups of 3 are Read/Write/Execute permissions for
** Owner, Group, and World respectivly
**
** Finally a plus character is appended if there is an acl
** associated with the entry
*/
SWString
SWFileAttributes::getUnixModeString() const
		{
		SWString	result;
		char		perm[10] = "rwxrwxrwx";
		int			i, mask;

		if (isDirectory()) result = "d";
		else if (isDoor()) result = "D";
		else if (isSymbolicLink()) result = "l";
		else if (isBlockDevice()) result = "b";
		else if (isCharDevice()) result = "c";
		else if (isPipe()) result = "p";
		else if (isSocket()) result = "s";
		else result = "-";

		mask = 0400;
		for (i=0;i<9;i++)
			{
			if (!(m_attribs & mask)) perm[i] = '-';
			mask >>= 1;
			}
		
		if (isSetUserID()) perm[2] = isOwnerExecute()?'s':'S';
		if (isSetGroupID()) perm[5] = isGroupExecute()?'s':'S';
		if (isSticky()) perm[8] = isWorldExecute()?'t':'T';

		result += perm;

		return result;
		}


SWString
SWFileAttributes::getAttributesString() const
		{
		SWString	s;

		switch (m_type)
			{
			// Directory is the same as Folder
			// case FileTypeDirectory:			s = "Directory";		break;
			case FileTypeFile:				s = "File";				break;
			case FileTypeFolder:			s = "Folder";			break;
			case FileTypeDevice:			s = "Device";			break;
			case FileTypeCharDevice:		s = "CharDevice";		break;
			case FileTypeBlockDevice:		s = "BlockDevice";		break;
			case FileTypeSocket:			s = "Socket";			break;
			case FileTypeDoor:				s = "Door";				break;
			case FileTypeFifo:				s = "Fifo";				break;
			case FileTypeXenixNamedFile:	s = "XenixNamedFile";	break;
			case FileTypeNetworkSpecial:	s = "NetworkSpecial";	break;
			case FileTypePipe:				s = "Pipe";				break;
			case FileTypeSymbolicLink:		s = "SymbolicLink";		break;
			default:						s = "Unknown";			break;
			}

		if ((m_attribs & FileAttrArchive) != 0)
			{
			if (!s.isEmpty()) s += ",";
			s += "Archive";
			}
		if ((m_attribs & FileAttrCompressed) != 0)
			{
			if (!s.isEmpty()) s += ",";
			s += "Compressed";
			}
		if ((m_attribs & FileAttrEncrypted) != 0)
			{
			if (!s.isEmpty()) s += ",";
			s += "Encrypted";
			}
		if ((m_attribs & FileAttrHidden) != 0)
			{
			if (!s.isEmpty()) s += ",";
			s += "Hidden";
			}
		if ((m_attribs & FileAttrNormal) != 0)
			{
			if (!s.isEmpty()) s += ",";
			s += "Normal";
			}
		if ((m_attribs & FileAttrOffline) != 0)
			{
			if (!s.isEmpty()) s += ",";
			s += "Offline";
			}
		if ((m_attribs & FileAttrReadOnly) != 0)
			{
			if (!s.isEmpty()) s += ",";
			s += "ReadOnly";
			}
		if ((m_attribs & FileAttrReparsePoint) != 0)
			{
			if (!s.isEmpty()) s += ",";
			s += "ReparsePoint";
			}
		if ((m_attribs & FileAttrSparse) != 0)
			{
			if (!s.isEmpty()) s += ",";
			s += "Sparse";
			}
		if ((m_attribs & FileAttrSystem) != 0)
			{
			if (!s.isEmpty()) s += ",";
			s += "System";
			}
		if ((m_attribs & FileAttrTemporary) != 0)
			{
			if (!s.isEmpty()) s += ",";
			s += "Temporary";
			}
		
		return s;
		}


bool
SWFileAttributes::hasAttributes(sw_fileattr_t wanted, bool wantAny)
		{
		bool	result=false;

		if (m_attribs != 0 && m_type != FileTypeUnknown)
			{
			sw_fileattr_t	tmp = (m_attribs & wanted);
			
			if (tmp)
				{
				result = wantAny?true:(tmp==wanted);
				}
			}

		return result;
		}


// Protected methods to set the file attributes
void
SWFileAttributes::set(const SWString &name, sw_uint32_t unixmode, sw_uint32_t winattrs, sw_uint_t symlink)
		{
		m_type=FileTypeUnknown;
		m_attribs = 0;

#if defined(WIN32) || defined(_WIN32)

		if ((winattrs & FILE_ATTRIBUTE_READONLY) != 0)				m_attribs |= FileAttrReadOnly;
		if ((winattrs & FILE_ATTRIBUTE_HIDDEN) != 0)				m_attribs |= FileAttrHidden;
		if ((winattrs & FILE_ATTRIBUTE_SYSTEM) != 0)				m_attribs |= FileAttrSystem;
		if ((winattrs & FILE_ATTRIBUTE_ARCHIVE) != 0)				m_attribs |= FileAttrArchive;
		if ((winattrs & FILE_ATTRIBUTE_TEMPORARY) != 0)				m_attribs |= FileAttrTemporary;
		if ((winattrs & FILE_ATTRIBUTE_SPARSE_FILE) != 0)			m_attribs |= FileAttrSparse;
		if ((winattrs & FILE_ATTRIBUTE_COMPRESSED) != 0)			m_attribs |= FileAttrCompressed;
		if ((winattrs & FILE_ATTRIBUTE_OFFLINE) != 0)				m_attribs |= FileAttrOffline;
		if ((winattrs & FILE_ATTRIBUTE_NOT_CONTENT_INDEXED) != 0)	m_attribs |= FileAttrNotIndexed;
		if ((winattrs & FILE_ATTRIBUTE_ENCRYPTED) != 0)				m_attribs |= FileAttrEncrypted;
		if ((winattrs & FILE_ATTRIBUTE_NORMAL) != 0)				m_attribs |= FileAttrNormal;

		if ((winattrs & FILE_ATTRIBUTE_REPARSE_POINT) != 0)
			{
			// Can we be more specific and distinguish between various reparse points?
			m_attribs |= FileAttrReparsePoint | FileAttrSymbolicLink;
			}

		if ((winattrs & FILE_ATTRIBUTE_DIRECTORY) != 0) m_type = FileTypeDirectory;
		else if ((winattrs & FILE_ATTRIBUTE_DEVICE) != 0) m_type = FileTypeDevice;
		else m_type = FileTypeFile;

		// Check for executable files (should really use PATHEXT)
		SWString	pathext;

		if (getenv("PATHEXT") != NULL) pathext = getenv("PATHEXT");
		else pathext = ".COM;.EXE;.BAT;.CMD";

		SWTokenizer	tok(pathext, ":");
		bool		found=false;

		while (!found && tok.hasMoreTokens())
			{
			if (name.endsWith(tok.nextToken(), SWString::ignoreCase))
				{
				found = true;
				}
			}

		if (found)
			{
			m_attribs |= FileAttrOwnerExecute | FileAttrGroupExecute | FileAttrWorldExecute;
			}
#else
		// Dos/Windows attributes must be simulated
		m_attribs = unixmode & 0x1ff;

		switch (unixmode & S_IFMT)
			{
			case S_IFDIR:	 m_type = FileTypeDirectory;	break;
			case S_IFREG:	 m_type = FileTypeFile;	break;
	#ifdef S_IFBLK
			case S_IFBLK:	 m_type = FileTypeBlockDevice;	break;
	#endif
	#ifdef S_IFCHR
			case S_IFCHR:	 m_type = FileTypeCharDevice;	break;
	#endif
	#ifdef S_IFIFO
			case S_IFIFO:	 m_type = FileTypeFifo;	break;
	#endif
	#ifdef S_IFSOCK
			case S_IFSOCK:	 m_type = FileTypeSocket;	break;
	#endif
	#ifdef S_IFDOOR
			case S_IFDOOR:	 m_type = FileTypeDoor;	break;
	#endif
	#ifdef S_IFNAM
			case S_IFNAM:	
				{
				//#define	S_IFNAM		0x5000  /* XENIX special named file */
				//#define	S_INSEM		0x1	/* XENIX semaphore subtype of IFNAM */
				//#define	S_INSHD		0x2	/* XENIX shared data subtype of IFNAM */
				m_type = FileTypeXenixNamedFile;
				break;
				}
	#endif
	#ifdef S_IFLNK
			case S_IFLNK:	 m_type = FileTypeSymbolicLink;	break;
	#endif
			}

#ifdef S_ISUID
		if (unixmode & (S_ISUID)) m_attribs |= FileAttrSetUserID;
#endif
#ifdef S_ISGID
		if (unixmode & (S_ISGID)) m_attribs |= FileAttrSetGroupID;
#endif
#ifdef S_ISVTX
		if (unixmode & (S_ISVTX)) m_attribs |= FileAttrSticky;
#endif
#ifdef S_ENFMT
		if (unixmode & (S_ENFMT)) m_attribs |= FileAttrRecordLocking;
#endif

		if (symlink != 0)
			{
			// Can we be more specific and distinguish between various reparse points?
			m_attribs |= FileAttrReparsePoint | FileAttrSymbolicLink;
			}

		// "dot" files are pseudo hidden
		SWString	basename=SWFilename::basename(name);

		if (!basename.isEmpty() && basename[0] == '.') m_attribs |= FileAttrHidden;

#if defined(SW_BASE_USES_ACE)
		if (ACE_OS::access((const sw_tchar_t *)name, W_OK) != 0) m_attribs |= FileAttrReadOnly;
#else
		if (::access(name, W_OK) != 0) m_attribs |= FileAttrReadOnly;
#endif

		// We get this flag set only if no others are set
		if (m_type == FileTypeUnknown)
			{
			m_type = FileTypeFile;
			m_attribs |= FileAttrNormal;
			}
#endif
		}




