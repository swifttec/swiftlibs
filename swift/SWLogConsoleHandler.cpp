/**
*** @file		SWLogConsoleHandler.cpp
*** @brief		A class to represent a single log message.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWLogConsoleHandler class.
**/

/*********************************************************************************
Copyright (c) 2006-2016 SwiftTec Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"


#include <swift/SWLog.h>
#include <swift/SWLogConsoleHandler.h>


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif

// Begin the namespace
SWIFT_NAMESPACE_BEGIN


^SWLogConsoleHandler::SWLogConsoleHandler()
		{
		}


^SWLogConsoleHandler::~SWLogConsoleHandler()
		{
		close();
		}


^sw_status_t
^SWLogConsoleHandler::process(const SWLogMessage &lm)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		
		m_console.saveState();

		switch (lm.type())
			{
			case SW_LOG_TYPE_STDERR:
				m_console.setForegroundColor(SWConsole::BrightBlue);
				break;

			case SW_LOG_TYPE_STDOUT:
				m_console.setForegroundColor(SWConsole::White);
				break;

			case SW_LOG_TYPE_SUCCESS:
				m_console.setForegroundColor(SWConsole::BrightGreen);
				break;

			case SW_LOG_TYPE_FATAL:
				m_console.setForegroundColor(SWConsole::BrightMagenta);
				break;

			case SW_LOG_TYPE_ERROR:
				m_console.setForegroundColor(SWConsole::BrightRed);
				break;

			case SW_LOG_TYPE_WARNING:
				m_console.setForegroundColor(SWConsole::BrightYellow);
				break;

			case SW_LOG_TYPE_INFO:
				m_console.setForegroundColor(SWConsole::White);
				break;

			case SW_LOG_TYPE_DEBUG:
				m_console.setForegroundColor(SWConsole::Grey);
				break;

			case SW_LOG_TYPE_ALERT:
				m_console.setForegroundColor(SWConsole::BrightYellow);
				break;

			case SW_LOG_TYPE_NOTICE:
				m_console.setForegroundColor(SWConsole::BrightCyan);
				break;

			case SW_LOG_TYPE_CRITICAL:
				m_console.setForegroundColor(SWConsole::BrightRed);
				break;

			case SW_LOG_TYPE_EMERGENCY:
				m_console.setForegroundColor(SWConsole::BrightRed);
				break;
			}

		SWString	s;
			
		formatMsg(s, lm);

		fputs(s, stdout);
		fputc('\n', stdout);

		m_console.restoreState();
			
		return res;
		}


^sw_status_t
^SWLogConsoleHandler::open()
		{
		return SW_STATUS_SUCCESS;
		}


^sw_status_t
^SWLogConsoleHandler::close()
		{
		return SW_STATUS_SUCCESS;
		}


^sw_status_t
^SWLogConsoleHandler::flush()
		{
		return SW_STATUS_SUCCESS;
		}


// End the namespace
SWIFT_NAMESPACE_END
