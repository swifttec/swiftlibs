/**
*** @file		SWTimeZone.cpp
*** @brief		A class representing timezone information
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWTimeZone class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




// Needed for precompiled header support
#include "stdafx.h"

#include <swift/swift.h>

#include <swift/SWTimeZone.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




// Static Members
SWTimeZone		SWTimeZone::sm_currtimezone;

SWTimeZone &
SWTimeZone::getCurrentTimeZone()
		{
		if (!sm_currtimezone.isValid())
			{
			SWString	tzTitle;
			SWString	tzNames[2];
			int			tzAdjust=0, dstAdjust=0, dstAvailable=-1;

			// Get the local timezone info.
			// We must do this everytime in case the caller has changed the timezone information.
#if defined(WIN32) || defined(_WIN32)
			TIME_ZONE_INFORMATION	tzi;

			if (GetTimeZoneInformation(&tzi) != TIME_ZONE_ID_INVALID)
				{
				tzAdjust = (tzi.Bias + tzi.StandardBias) * 60;
				dstAdjust = -tzi.DaylightBias * 60;
				tzNames[0] = tzi.StandardName;
				tzNames[1] = tzi.DaylightName;

				/*
				char	tmp[16];

				sprintf(tmp, "%d", tzAdjust/3600);
				*/

				tzTitle = tzNames[0];
				tzTitle += SWString::valueOf(tzAdjust/3600);
				tzTitle += tzNames[1];

				dstAvailable = (dstAdjust != 0)?1:0;
				}

#else // defined(WIN32) || defined(_WIN32)
			tzset();
			tzAdjust = timezone;
			tzNames[0] = tzname[0];
			tzNames[1] = tzname[1];
			dstAvailable = daylight;

	#ifdef sun
			dstAdjust = timezone - altzone;
	#else
			// No altzone available, we must work it out
			// based on daylight and assume that the adjustment is 1 hour (3600 seconds)
			dstAdjust = (daylight?3600:0);
	#endif

			char	*p = getenv("TZ");

			if (p != NULL) tzTitle = p;
			else
				{
				char	tmp[16];

				sprintf(tmp, "%d", tzAdjust/3600);

				tzTitle = tzNames[0];
				tzTitle += tmp;
				tzTitle += tzNames[1];
				}
#endif // defined(WIN32) || defined(_WIN32)

			sm_currtimezone.set(tzTitle, tzAdjust, dstAdjust, dstAvailable, tzNames[0], tzNames[1]);
			}

		return sm_currtimezone;
		}




SWTimeZone::SWTimeZone() :
	m_valid(false),
    m_tzAdjustment(0),
	m_dstAdjustment(0),
	m_dstAvailable(0)
		{
		}


SWTimeZone::SWTimeZone(const SWString &name, int tzAdjustment, int dstAdjustment, int dstAvailable, const SWString &stdText, const SWString &dstText) :
	m_valid(false),
    m_tzAdjustment(0),
	m_dstAdjustment(0),
	m_dstAvailable(0)
		{
		set(name, tzAdjustment, dstAdjustment, dstAvailable, stdText, dstText);
		}


SWTimeZone::SWTimeZone(const SWTimeZone &other) :
	SWObject()
		{
		*this = other;
		}


SWTimeZone::~SWTimeZone()
		{
		}


SWTimeZone &
SWTimeZone::operator=(const SWTimeZone &other)
		{
		m_valid = other.m_valid;
		m_name = other.m_name;
		m_tzAdjustment = other.m_tzAdjustment;
		m_dstAdjustment = other.m_dstAdjustment;
		m_dstAvailable = other.m_dstAvailable;
		m_stdText = other.m_stdText;
		m_dstText = other.m_dstText;

		return *this;
		}


void
SWTimeZone::set(const SWString &name, int tzAdjustment, int dstAdjustment, int dstAvailable, const SWString &stdText, const SWString &dstText)
		{
		m_valid = true;
		m_name = name;
		m_tzAdjustment = tzAdjustment;
		m_dstAdjustment = dstAdjustment;
		m_dstAvailable = dstAvailable;
		m_stdText = stdText;
		m_dstText = dstText;
		}




