/****************************************************************************
**	SWMsgSocket.cpp	A TCP based socket for passing SWMsg objects
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWMsgSocket.h>

// Should be last include




SWMsgSocket::SWMsgSocket(SWTcpSocket *sock, bool delFlag) :
    m_pSocket(sock),
    m_is(sock, false),
    m_os(sock, false),
    m_delFlag(delFlag)
		{
		}


// Destructor
SWMsgSocket::~SWMsgSocket()
		{
		if (m_delFlag) 
			{
			delete m_pSocket;
			m_pSocket = 0;
			}
		}



/**
*** Sends a message down the socket.
***
*** @return	true on success, false otherwise
**/
sw_status_t
SWMsgSocket::send(const SWMsg &msg)
		{
		sw_status_t	res;

		if (m_pSocket == NULL) throw SW_EXCEPTION_TEXT(SWSocketException, "SWMsgSocket::send - no socket set");
		res = msg.writeToStream(m_os);
		if (res == SW_STATUS_SUCCESS) res = m_os.flush();

		return res;
		}


/**
*** Receives a message from the socket.
***
*** @return	true on success, false otherwise
**/
sw_status_t
SWMsgSocket::receive(SWMsg &msg)
		{
		if (m_pSocket == NULL) throw SW_EXCEPTION_TEXT(SWSocketException, "SWMsgSocket::receive - no socket set");
		return msg.readFromStream(m_is);
		}



/**
*** Sets/changes the socket we are associated with.
**/
void
SWMsgSocket::setSocket(SWTcpSocket *sock, bool delflag)
		{
		// No change? - do nowt!
		if (sock == m_pSocket) return;

		// Delete the previous socket 
		if (m_delFlag) 
			{
			delete m_pSocket;
			m_pSocket = 0;
			}

		m_pSocket = sock;
		m_delFlag = delflag;
		m_is.setSocket(m_pSocket, false);
		m_os.setSocket(m_pSocket, false);
		}


