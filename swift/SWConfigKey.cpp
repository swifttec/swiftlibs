/****************************************************************************
**	SWConfigKey.cpp	A class to that represents a single level of configuration.
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




#include "stdafx.h"

#include <swift/SWConfigKey.h>
#include <swift/SWGuard.h>
#include <swift/SWException.h>
#include <swift/SWConfigValue.h>



// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif

SWConfigKey::SWConfigKey(SWConfigKey *owner, const SWString &name, bool ignoreCase) :
    _owner(owner),
    _name(name),
    _changed(false),
	_ignoreCase(ignoreCase)
		{
		}


/**
*** Destructor
**/
SWConfigKey::~SWConfigKey()
		{
		// Free up all the resources
		clearKeysAndValues();
		}



/**
*** This protected method is used to free up all the resources
*** used. It is called either by the destructor or on reload
**/
void
SWConfigKey::clearKeysAndValues()
		{
		SWGuard	guard(this);
		int		i;

		for (i=0;i<(int)_keyVec.size();i++)
			{
			delete _keyVec[i];
			}
		_keyVec.clear();

		for (i=0;i<(int)_valueVec.size();i++)
			{
			delete _valueVec[i];
			}
		_valueVec.clear();
		}


/**
*** This method checks to see if the given sub-key already exists.
*** If it does it returns a pointer to it, if not the key is created
*** and returned.
***
*** @brief	Find or create a sub-key of the given name
**/
SWConfigKey *
SWConfigKey::findOrCreateKey(const SWString &name)
		{
		SWGuard		guard(this);
		SWConfigKey	*key = findKey(name);

		if (key == NULL)
			{
			key = new SWConfigKey(this, name, _ignoreCase);
			_keyVec.add(key);

			setChangedFlag(KeyCreated, key);
			}

		return key;
		}



/**
*** This method searches for the named key and returns it.
***
*** @brief	Find the named key
*** @return	A pointer to the key, or NULL if not found
**/
SWConfigKey *
SWConfigKey::findKey(const SWString &name) const
		{
		SWGuard		guard(this);
		SWConfigKey	*key;
		int		i;

		for (i=0;i<(int)_keyVec.size();i++)
			{
			key = _keyVec[i];
			if (key->name().compareTo(name, _ignoreCase?(SWString::ignoreCase):(SWString::exact)) == 0) return key;
			}

		return NULL;
		}



/**
*** This method returns the nth key or throws
*** an IndexOutOfBoundsException if the index
*** supplied is out of bounds
**/
SWConfigKey *
SWConfigKey::getKey(int n) const
		{
		if (n < 0 || n >= (int)_keyVec.size())
			{
			throw SW_EXCEPTION_TEXT(SWIndexOutOfBoundsException, "SWConfigKey::getKey - index out of range");
			}

		return _keyVec[n];
		}



/**
*** This method returns the name of the nth key or throws
*** an IndexOutOfBoundsException if the index
*** supplied is out of bounds
**/
SWString
SWConfigKey::keyName(int n) const
		{
		if (n < 0 || n >= (int)_keyVec.size())
			{
			throw SW_EXCEPTION_TEXT(SWIndexOutOfBoundsException, "SWConfigKey::keyName - index out of range");
			}

		return _keyVec[n]->name();
		}


// Get the index of the key, or -1 f not found
int
SWConfigKey::keyIndex(const SWString &name) const
		{
		int	i;

		for (i=0;i<(int)_keyVec.size();i++)
			{
			if (_keyVec[i]->name().compareTo(name, _ignoreCase?(SWString::ignoreCase):(SWString::exact)) == 0)
				{
				return i;
				}
			}
		
		// Not found
		return -1;
		}




/**
*** Remove the named key (if it exists). This will invalidate any pointers
*** to the corresponding SWConfigKey object.
***
*** @brief	Remove the named key
**/
void
SWConfigKey::removeKey(const SWString &name)
		{
		int		n = keyIndex(name);

		if (n >= 0 && n < (int)_keyVec.size()) removeKey(n);
		}


/**
*** Remove the nth key (if it exists). This will invalidate any pointers
*** to the corresponding SWConfigKey object.
***
*** @brief	Remove the nth key
**/
void
SWConfigKey::removeKey(int n)
		{
		if (n >= 0 && n < (int)_keyVec.size())
			{
			SWConfigKey	*kp;

			kp = _keyVec[n];

			//while (kp->valueCount() > 0) kp->removeValue(kp->valueCount()-1);
			//while (kp->keyCount() > 0) kp->removeKey(kp->keyCount()-1);

			// Remove the key from the vector, then call setChangedFlag
			// and finally delete the key.
			_keyVec.removeAt(n);
			setChangedFlag(KeyDeleted, kp);
			delete kp;
			}
		}



/**
*** This method checks to see if the given value already exists.
*** If it does it returns a pointer to it, if not the value is created
*** and returned.
***
*** @brief	Find or create a value of the given name
**/
SWConfigValue *
SWConfigKey::findOrCreateValue(const SWString &name)
		{
		SWGuard			guard(this);
		SWConfigValue	*value = findValue(name);

		if (value == NULL)
			{
			value = new SWConfigValue(this, name);
			_valueVec.add(value);

			// Notify parent of change and set changed flag
			setChangedFlag(ValueCreated, value);
			}

		return value;
		}



/**
*** This method searches for the named value and returns it.
***
*** @brief	Find the named value
*** @return	A pointer to the value, or NULL if not found
**/
SWConfigValue *
SWConfigKey::value(const SWString &name) const
		{
		SWGuard			guard(this);
		SWConfigValue	*value;
		int		i;

		for (i=0;i<(int)_valueVec.size();i++)
			{
			value = _valueVec[i];
			if (value->name().compareTo(name, _ignoreCase?(SWString::ignoreCase):(SWString::exact)) == 0) return value;
			}

		return NULL;
		}



/**
*** This method returns the nth value or throws
*** an IndexOutOfBoundsException if the index
*** supplied is out of bounds
**/
SWConfigValue *
SWConfigKey::getValue(int n) const
		{
		if (n < 0 || n >= (int)_valueVec.size())
			{
			throw SW_EXCEPTION_TEXT(SWIndexOutOfBoundsException, "SWConfigKey::getValue - index out of range");
			}

		return _valueVec[n];
		}



/**
*** This method returns the name of the nth value or throws
*** an IndexOutOfBoundsException if the index
*** supplied is out of bounds
**/
SWString
SWConfigKey::valueName(int n) const
		{
		if (n < 0 || n >= (int)_valueVec.size())
			{
			throw SW_EXCEPTION_TEXT(SWIndexOutOfBoundsException, "SWConfigKey::valueName - index out of range");
			}

		return _valueVec[n]->name();
		}



/**
*** Remove the named value (if it exists). This will invalidate any pointers
*** to the corresponding SWConfigValue object.
***
*** @brief	Remove the named value
**/
void
SWConfigKey::removeValue(const SWString &name)
		{
		int		n = valueIndex(name);

		if (n >= 0 && n < (int)_valueVec.size()) removeValue(n);
		}


/**
*** Remove the nth value (if it exists). This will invalidate any pointers
*** to the corresponding SWConfigValue object.
***
*** @brief	Remove the nth value
**/
void
SWConfigKey::removeValue(int n)
		{
		if (n >= 0 && n < (int)_valueVec.size())
			{
			SWConfigValue	*v;

			v = _valueVec[n];

			// Remove the value from the vector, call setChangedFlag
			// and finally remove the value object
			_valueVec.removeAt(n);
			setChangedFlag(ValueDeleted, v);
			delete v;
			}
		}


// Get the index of the name, or -1 f not found
int
SWConfigKey::valueIndex(const SWString &name) const
		{
		int	i;

		for (i=0;i<(int)_valueVec.size();i++)
			{
			if (_valueVec[i]->name().compareTo(name, _ignoreCase?(SWString::ignoreCase):(SWString::exact)) == 0)
				{
				return i;
				}
			}
		
		// Not found
		return -1;
		}


/**
*** Set the changed flag.
*** We must also set the changed flag of our owner
**/
void
SWConfigKey::setChangedFlag(ChangeEventCode code, SWObject *obj)
		{
		_changed = true;
		if (_owner) _owner->setChangedFlag(code, obj);
		}


/**
*** Clears the changed flag.
**/
void
SWConfigKey::clearChangedFlag()
		{
		_changed = false;
		}

