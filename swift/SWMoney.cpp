/**
*** @file		SWMoney.cpp
*** @brief		A representation of date based on Julian day numbers
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWMoney class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <swift/SWMoney.h>
#include <swift/sw_printf.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





// Construct with today's date
SWMoney::SWMoney(int amount) :
	m_amount((sw_money_t)amount)
		{
		}

SWMoney::SWMoney(sw_money_t amount) :
	m_amount(amount)
		{
		}


SWMoney::SWMoney(const char *s)
		{
		*this = SWString(s);
		}


SWMoney &
SWMoney::operator=(const char *s)
		{
		*this = SWString(s);
		return *this;
		}


SWMoney::SWMoney(const wchar_t *s)
		{
		*this = SWString(s);
		}


SWMoney &
SWMoney::operator=(const wchar_t *s)
		{
		*this = SWString(s);
		return *this;
		}


SWMoney::SWMoney(const SWString &s)
		{
		*this = s;
		}


SWMoney &
SWMoney::operator=(const SWString &str)
		{
		int			p;
		sw_money_t	w, f;
		SWString	s(str);
		SWString	t;
		bool		negative=false;
		
		s.strip(" \t\n\r", SWString::both);
		if (s.startsWith("-"))
			{
			negative = true;
			s.remove(0, 1);
			}
		else if (s.endsWith("-"))
			{
			negative = true;
			s.remove((int)s.length()-1, 1);
			}

		p = s.first('.');
		if (p < 0)
			{
			t = s;
			t.replaceAll(",", "");
			w = t.toUInt64(10);
			f = 0;
			}
		else
			{
			t = s.left(p);
			t.replaceAll(",", "");
			w = t.toUInt64(10);

			t = s.mid(p+1, 2);
			while (t.length() < 2) t += "0";
			
			f = t.toUInt64(10);
			}

		m_amount = (negative?-1:1) * ((w * 100) + f);

		return *this;
		}


SWMoney::SWMoney(sw_money_t w, sw_money_t f)
		{
		m_amount = ((sw_money_t)w * 100) + (sw_money_t)f;
		}


SWString
SWMoney::toString(int flags) const
		{
		SWString	s;
		const char	*nc="";
		sw_money_t	v=m_amount, w, f;
		
		if (v < 0)
			{
			nc = "-";
			v = -v;
			}
		else
			{
			if ((flags & MF_NOSIGNSPACE) != 0) nc = "";
			else nc = " ";
			}

		w = v / 100;
		f = v % 100;

		if ((flags & MF_NOCOMMA) != 0) s.format("%s%lld.%02llu", nc, w, f);
		else s.format("%s%,lld.%02llu", nc, w, f);

		return s;
		}


sw_money_t
SWMoney::whole() const
		{
		return (m_amount / 100);
		}


sw_money_t
SWMoney::fraction() const
		{
		sw_money_t	v=m_amount;

		if (v < 0) v = -v;
		return (v % 100);
		}



