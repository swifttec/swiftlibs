/**
*** @file		SWThreadIdCount.h
*** @brief		A class to represent a thread ID and associated count.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWThreadIdCount class.
***
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWThreadIdCount_h__
#define __SWThreadIdCount_h__

#include <swift/swift.h>



// Forward declarations

/**
*** @brief A class to represent a thread ID and associated count.
***
*** This class is used internally for various purposes when tracking
*** the number of resources used by an individual thread.
***
*** @author		Simon Sparkes
**/
class SWThreadIdCount
		{
public:
		/**
		*** @brief Construct a ThreadIdCount object with the specified thread ID and count.
		***
		*** @param[in]	tid		The thread ID.
		*** @param[in]	count	The initial count.
		**/
		SWThreadIdCount(sw_thread_id_t tid=0, sw_int32_t count=0) : m_tid(tid), m_count(count)	{ }

		/// Returns the value of the current thread ID
		sw_thread_id_t		id() const								{ return m_tid; }

		/// Returns the value of the count
		sw_int32_t		count() const							{ return m_count; }

		/// Sets the tid/count pair
		void			set(sw_thread_id_t tid, sw_int32_t count=0)	{ m_tid = tid ; m_count = count; }

		/// Clears the tid/count pair
		void			clear()										{ m_tid = 0 ; m_count = 0; }

		/// Increment the the count component
		sw_int32_t		increment()									{ return ++m_count; }

		/// Decrement the the count component
		sw_int32_t		decrement()									{ return --m_count; }

		/// Less-than operator
		bool			operator<(const SWThreadIdCount &other) const	{ return (m_tid < other.m_tid); }

		/// Equality operator
		bool			operator==(const SWThreadIdCount &other) const	{ return (m_tid == other.m_tid); }

		/// Prefix increment operator.
		sw_int32_t			operator++()						{ m_count++; return m_count; }

		/// Postfix increment operator.
		sw_int32_t			operator++(int)						{ sw_int32_t v=m_count++; return v; }

		/// Prefix decrement operator.
		const sw_int32_t &	operator--()						{ m_count--; return m_count; }

		/// Postfix decrement operator.
		sw_int32_t			operator--(int)						{ sw_int32_t v=m_count--; return v; }

private:
		sw_thread_id_t	m_tid;		///< The thread ID
		sw_int32_t	m_count;	///< The count for this thread ID.
		};




#endif
