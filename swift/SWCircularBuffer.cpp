#include "stdafx.h"

#include <swift/SWCircularBuffer.h>
#include <swift/SWGuard.h>
#include <xplatform/sw_math.h>


SWCircularBuffer::SWCircularBuffer(int initialSize, int stepSize) :
	m_pData(NULL),
	m_stepSize(stepSize),
	m_bufsize(initialSize),
	m_readpos(0),
	m_writepos(0),
	m_datasize(0)
		{
		// Set this up as a synchronised object
		synchronize();

		if (m_bufsize != 0)
			{
			m_pData = new sw_byte_t[m_bufsize];
			}
		}


SWCircularBuffer::~SWCircularBuffer()
		{
		if (m_pData != NULL)
			{
			delete [] m_pData;
			m_pData = NULL;
			}
		}

	
void
SWCircularBuffer::clear()
		{
		SWGuard		guard(this);

		m_readpos = m_writepos = m_datasize = 0;
		}

		
void
SWCircularBuffer::ensureCapacity(sw_size_t nbytes)
		{
		SWGuard		guard(this);

		if ((int)nbytes > m_bufsize)
			{
			SWDataBuffer	tmpdata;
			sw_size_t	tmpsize=m_datasize;

			// Read all the data into a temp buffer
			readData(tmpdata, tmpsize);

			sw_byte_t	*p=(sw_byte_t *)tmpdata.data();

			while (m_bufsize < (int)nbytes) m_bufsize += m_stepSize;

			delete m_pData;
			m_pData = new sw_byte_t[m_bufsize];
			m_readpos = m_writepos = 0;

			// Write the data back
			writeData(tmpdata, tmpsize);
			}
		}


bool
SWCircularBuffer::writeData(void *pSourceData, sw_size_t nbytes)
		{
		bool		res=false;

		if (nbytes > 0)
			{
			SWGuard		guard(this);
			
			ensureCapacity(m_datasize + nbytes);

			int			space=m_bufsize - m_datasize;

			// printf("SWCircularBuffer::writeData nbytes=%d, space=%d, readpos=%d, writepos=%d, bufsize=%d, datasize=%d\n", nbytes, space, m_readpos, m_writepos, m_bufsize, m_datasize);

			if ((int)nbytes <= space)
				{
				sw_byte_t	*pData=(sw_byte_t *)pSourceData;
				sw_byte_t	*bp=m_pData + m_writepos;
				sw_size_t	bytestowrite=nbytes;

				if ((m_writepos + (int)bytestowrite) > m_bufsize)
					{
					sw_size_t	bytestoend=m_bufsize-m_writepos;

					memcpy(bp, pData, bytestoend);
					m_writepos = 0;
					bp = m_pData;
					memcpy(bp, pData, nbytes-bytestoend);
					bytestowrite -= bytestoend;
					pData += bytestoend;
					}

				bp = m_pData + m_writepos;

				memcpy(bp, pData, bytestowrite);
				m_writepos += (int)bytestowrite;

				// Add the frame size to the queue
				m_datasize += (int)nbytes;
				res = true;
				}
			}
		else
			{
			res = true;
			}

		return res;
		}


// Read a single byte from the queue
bool
SWCircularBuffer::readByte(sw_byte_t &v, int mode)
		{
		SWGuard		guard(this);
		sw_size_t	nbytes=0;
		bool		res=false;

		if (datasize() > 0)
			{
			sw_byte_t	*bp=m_pData + m_readpos;
			sw_size_t	bytestoread=1;

			if (m_readpos >= m_bufsize)
				{
				m_readpos = 0;
				bp = m_pData;
				}

			int			space=m_bufsize - m_datasize;

			bp = m_pData + m_readpos;
			v = *bp;

			if (mode != Peek)
				{
				// We are not peeking at the data so update the read position and data size
				m_readpos += 1;
				m_datasize -= 1;
				}
						
			res = true;
			}

		return res;
		}


bool
SWCircularBuffer::readAllData(SWDataBuffer &data, int mode)
		{
		SWGuard		guard(this);

		return readData(data, datasize(), mode);
		}



bool
SWCircularBuffer::readData(void *buf, sw_size_t nbytes, int mode)
		{
		SWGuard		guard(this);
		bool		res=false;

		if (nbytes > 0 && nbytes <= datasize())
			{
			int			readpos=m_readpos;
			sw_byte_t	*pData=(sw_byte_t*)buf;
			sw_byte_t	*bp=m_pData + readpos;
			sw_size_t	bytestoread=nbytes;

			if ((readpos + (int)bytestoread) > m_bufsize)
				{
				sw_size_t	bytestoend=m_bufsize-readpos;

				// printf(">>> partial nbytes=%d from %d\n", bytestoend, readpos);

				memcpy(pData, bp, bytestoend);
				readpos = 0;
				bp = m_pData;
				bytestoread -= bytestoend;
				pData += bytestoend;

				// printf(">>> remaining nbytes=%d from %d\n", bytestoread, readpos);
				}

			bp = m_pData + readpos;

			memcpy(pData, bp, bytestoread);
			readpos += (int)bytestoread;
						
			res = true;

			if (mode != Peek)
				{
				// We are not peeking at the data so update the read position and data size
				m_readpos = readpos;
				m_datasize -= (int)nbytes;
				}
			}
		else if (nbytes == 0)
			{
			res = true;
			}

		return res;
		}


bool
SWCircularBuffer::readData(SWDataBuffer &data, sw_size_t nbytes, int mode)
		{
		bool		res;

		data.clear();

		res = readData(data.data(nbytes), nbytes, mode);
		if (res) data.size(nbytes);

		return res;
		}


bool
SWCircularBuffer::dropData(sw_size_t nbytes)
		{
		SWGuard		guard(this);
		bool		res=false;

		if (nbytes > 0 && nbytes <= datasize())
			{
			sw_size_t	bytestoread=nbytes;

			if ((m_readpos + (int)bytestoread) > m_bufsize)
				{
				sw_size_t	bytestoend=m_bufsize-m_readpos;

				m_readpos = 0;
				bytestoread -= bytestoend;
				}

			m_readpos += (int)bytestoread;
			m_datasize -= (int)nbytes;			
			res = true;
			}
		else if (nbytes == 0)
			{
			res = true;
			}

		return res;
		}
