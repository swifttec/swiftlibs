/**
*** @file		SWFilteredFolder.h
*** @brief		A class to represent a handle to a directory.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWFilteredFolder class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




#ifndef __SWFilteredFolder_h__
#define __SWFilteredFolder_h__

#include <swift/SWFolder.h>
#include <swift/SWRegularExpression.h>







/**
*** @brief	A class to represent a handle to a folder/directory.
***
*** This class represents a handle to a folder/directory which can
*** operate on a single folder/directory entry at a time. This makes it
*** efficient on folder/directories with very large numbers of files.
***
*** No filtering is done on folder entries so all entries in the folder
*** are returned including hidden/system entries. To pre-filter the 
*** entries returned use a SWFilteredFolder.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWFilteredFolder : public SWFolder
		{
public:
		/**
		*** @brief	Default constructor.
		***
		*** If the path is specified the directory will be opened by
		*** calling the open method.
		***
		*** @param	path		The directory to be opened
		*** @see	open
		**/
		SWFilteredFolder(const SWString &path=_T(""), const SWString &filter=_T(""));

		/**
		*** @brief	Copy constructor
		***
		*** The copy constructor opens a new handle to the directory
		*** represented by the specified folder so that the two SWFilteredFolder
		*** objects can operate independently.
		***
		*** @param[in]	other	The folder object to copy.
		**/
		SWFilteredFolder(const SWFilteredFolder &other);

		/**
		*** @brief	Assignment operator.
		***
		*** Opens a new handle to the directory represented by the specified SWFilteredFolder object.
		*** If the SWFilteredFolder object is currently open it will be closed.
		***
		*** @param[in]	other	The folder object to copy.
		**/
		SWFilteredFolder &	operator=(const SWFilteredFolder &other);

		/**
		*** @brief	Destructor
		***
		*** The destructor will close the directory handle if open.
		**/
		virtual ~SWFilteredFolder();

		// Set the filter for this directory handle
		void				setFilter(const SWString &filter);
		
		/// Returns the filter used to create the listing
		const SWString &	filter()					{ return m_filter; }

		/// Return the case sensitivity flag
		bool				isCaseSensitive() const		{ return m_caseSensitive; }

		/// Set the case sensitive flag
		void				setCaseSensitive(bool v)	{ m_caseSensitive = v; }


public: // Static members
		static SWRegularExpression *	createFilterRE(const SWString &filter, bool caseSensitive);


protected: // Methods

		/// Internal method for filename filtering.
		virtual bool		includeFile(const SWString &file);

		/// Internal method for filename filtering.
		virtual bool		excludeFile(const SWString &file);


protected: // Members
		SWRegularExpression	*m_pRegExpr;
		SWString			m_filter;
		bool				m_caseSensitive;
		};





#endif
