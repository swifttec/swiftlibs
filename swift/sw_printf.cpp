/**
*** @file		sw_printf.cpp
*** @brief		A collection of useful misc functions
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the sw_printf functions.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include "sw_internal_printf.h"

#if defined(sun)
#include <ieeefp.h>
#include <floatingpoint.h>
#endif

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





static void
out(outputinfo *info, int c)
		{
		info->count++;

		if ((info->flags & SW_PRINTF_FLAG_LIMIT) != 0)
			{
			if (info->limit <= (c?1:0)) return;
			info->limit--;
			}

		switch (info->outputtype)
			{
			case IO_TYPE_CHAR:
				if ((info->flags & SW_PRINTF_FLAG_REALLOC) != 0 && info->outpos >= info->limit)
					{
					info->limit += 32;

					char	*oldbp=(char *)(info->pBuffer);
					char	*newbp=(char *)(info->pMemoryManager->realloc(info->pBuffer, info->limit));

					if (newbp == NULL) throw SW_EXCEPTION(SWMemoryException);
					if (newbp != oldbp)
						{
						info->pBuffer = newbp;
						info->out.cp = newbp + (info->out.cp - oldbp);
						}
					}

				*info->out.cp++ = (char)c;
				info->outpos++;
				break;

			case IO_TYPE_WCHAR:
				if ((info->flags & SW_PRINTF_FLAG_REALLOC) != 0 && info->outpos >= info->limit)
					{
					info->limit += 32;

					wchar_t	*oldbp=(wchar_t *)(info->pBuffer);
					wchar_t	*newbp=(wchar_t *)(info->pMemoryManager->realloc(info->pBuffer, info->limit * sizeof(wchar_t)));

					if (newbp == NULL) throw SW_EXCEPTION(SWMemoryException);
					if (newbp != oldbp)
						{
						info->pBuffer = newbp;
						info->out.wp = newbp + (info->out.wp - oldbp);
						}
					}

				*info->out.wp++ = (wchar_t)c;
				info->outpos++;
				break;

			case IO_TYPE_FILE:
				fputc(c, info->out.fp);
				info->outpos++;
				break;

			case IO_TYPE_TEXTFILE:
				info->out.tp->writeChar(c);
				info->outpos++;
				break;

			case IO_TYPE_CUSTOM:
				info->out.custom.func(c, info->out.custom.data);
				info->outpos++;
				break;
			}
		}


#define	FLAG_MINUS	0x1
#define FLAG_PLUS	0x2
#define FLAG_SPACE	0x4
#define FLAG_HASH	0x8
#define FLAG_ZERO	0x10
#define FLAG_GROUP	0x20	// Use thousands grouping
#define FLAG_COMMA	0x40	// Use forced thousands grouping (comma is blank)
#define FLAG_UNDERSCORE	0x80	// For %k and %K no space before suffix

#define FLAG_VARIABLE_WIDTH	0x80000000
#define FLAG_VARIABLE_PRECISION	0x40000000

static const char	*numericCharSet[2]= { "0123456789abcdef", "0123456789ABCDEF" };

static void
outString(const void *p, int type, int precision, int width, int padchar, int padleft, int quoted, outputinfo *info, int slen)
		{
		int				c, plen;
		const char		*cp = (const char *)p;
		const wchar_t	*wp = (const wchar_t *)p;

		// Get the string length and adjust for the precsion
		if (slen < 0)
			{
			if (type == sizeof(wchar_t)) slen = (int)wcslen(wp);
			else slen = (int)strlen(cp);
			}

		if (precision >= 0 && precision < slen) slen = precision;

		// Now calculate the length allowinf
		plen = width - slen - (quoted?2:0);

		if (padleft)
			{
			while (plen > 0)
				{
				out(info, padchar);
				plen--;
				}
			}

		if (quoted) out(info, '"');

		if (type == info->outputtype)
			{
			// Input character size and output character size match
			if ((info->flags & SW_PRINTF_FLAG_LIMIT) != 0)
				{
				if ((info->limit - slen) <= 1)
					{
					// Too many chars to fit in the limit, so adjust slen
					slen = info->limit-1;
					}
				info->limit -= slen;
				}

			if (slen > 0)
				{
				switch (info->outputtype)
					{
					case IO_TYPE_CHAR:
						if ((info->flags & SW_PRINTF_FLAG_REALLOC) != 0 && (info->outpos + slen) >= info->limit)
							{
							info->limit += 32 + slen;

							char	*oldbp=(char *)(info->pBuffer);
							char	*newbp=(char *)(info->pMemoryManager->realloc(info->pBuffer, info->limit));

							if (newbp == NULL) throw SW_EXCEPTION(SWMemoryException);
							if (newbp != oldbp)
								{
								info->pBuffer = newbp;
								info->out.cp = newbp + (info->out.cp - oldbp);
								}
							}

						memcpy(info->out.cp, cp, slen * sizeof(char));
						info->out.cp += slen;
						break;

					case IO_TYPE_WCHAR:
						if ((info->flags & SW_PRINTF_FLAG_REALLOC) != 0 && (info->outpos + slen) >= info->limit)
							{
							info->limit += 32 + slen;

							wchar_t	*oldbp=(wchar_t *)(info->pBuffer);
							wchar_t	*newbp=(wchar_t *)(info->pMemoryManager->realloc(info->pBuffer, info->limit * sizeof(wchar_t)));

							if (newbp == NULL) throw SW_EXCEPTION(SWMemoryException);
							if (newbp != oldbp)
								{
								info->pBuffer = newbp;
								info->out.wp = newbp + (info->out.wp - oldbp);
								}
							}

						memcpy(info->out.wp, wp, slen * sizeof(wchar_t));
						info->out.wp += slen;
						break;
					}

				info->count += slen;
				info->outpos += slen;
				}
			}
		else
			{
			// Either we have a custom/file output type or the character sizes
			// of input and output do not match
			while (slen > 0)
				{
				if (type == sizeof(wchar_t)) c = *wp++;
				else c = *cp++;

				out(info, c);
				slen--;
				}
			}

		if (quoted) out(info, '"');

		if (!padleft)
			{
			while (plen > 0)
				{
				out(info, ' ');
				plen--;
				}
			}
		}


#define GET_NEXT_CHAR(x) \
	if (intype == sizeof(wchar_t)) x = *(const wchar_t *)inptr; \
	else x = *inptr; \
	if (x) inptr += intype;

#define CURR_CHAR_PTR()		((const char *)inptr)
#define CURR_WCHAR_PTR()	((const wchar_t *)inptr)

int
sw_internal_doprintf(outputinfo *info, va_list ap)
		{
		const void	*pFrom=0;
		int			count=0;
		int			c=0;
		int			intype=info->inputtype;

		const char	*inptr=info->in.cp;

		pFrom = CURR_CHAR_PTR();

		GET_NEXT_CHAR(c);
		while (c)
			{
			if (c == '%')
				{
				if (count > 0)
					{
					outString(pFrom, intype, count, -1, ' ', false, false, info, count);

					if ((info->flags & SW_PRINTF_FLAG_LIMIT) != 0)
						{
						if (info->limit <= 1) break;
						}
					}

				int		flags=0;
				int		width=0;
				int		precision=-1;
				int		size=0;
				union
					{
					const char *cp;
					const wchar_t *wp;
					} fpstart;

				if (intype == IO_TYPE_WCHAR)
					{
					fpstart.wp = CURR_WCHAR_PTR()-1;
					}
				else
					{
					fpstart.cp = CURR_CHAR_PTR()-1;
					}

				GET_NEXT_CHAR(c);

				if (!c)
					{
					// Do nothing
					}
				else if (c == '%')
					{
					out(info, c);
					GET_NEXT_CHAR(c);
					}
				else 
					{
					// Get the flags (if any)
					while (c)
						{
						if (c == '-') flags |= FLAG_MINUS;
						else if (c == '+') flags |= FLAG_PLUS;
						else if (c == ' ') flags |= FLAG_SPACE;
						else if (c == '#') flags |= FLAG_HASH;
						else if (c == '0') flags |= FLAG_ZERO;
						else if (c == '\'') flags |= FLAG_GROUP;
						else if (c == ',') flags |= FLAG_COMMA;
						else if (c == '_') flags |= FLAG_UNDERSCORE;
						else break;

						GET_NEXT_CHAR(c);
						}
					
					// Get the width (if any)
					if (c == '*')
						{
						width = va_arg(ap, int);
						flags |= FLAG_VARIABLE_WIDTH;
						GET_NEXT_CHAR(c);
						}
					else if (c >= '1' && c <= '9')
						{
						while (c >= '0' && c <= '9')
							{
							width = width * 10 + c - '0';
							GET_NEXT_CHAR(c);
							}
						}

					// Get the precision (if any)
					if (c == '.')
						{
						GET_NEXT_CHAR(c);

						if (c == '*')
							{
							precision = va_arg(ap, int);
							flags |= FLAG_VARIABLE_PRECISION;
							GET_NEXT_CHAR(c);
							}
						else if (c >= '0' && c <= '9')
							{
							precision = 0;
							while (c >= '0' && c <= '9')
							{
							precision = precision * 10 + c - '0';
							GET_NEXT_CHAR(c);
							}
							}
						}


					// Get the size modifier (if any)
					while (c)
						{
						if (c == 'h') size = sizeof(short);
						else if (c == 'z')
							{
							size = sizeof(size_t);
							}
						else if (c == 'l' || c == 'w')
							{
							if (!size) size = sizeof(long);
							else size = sizeof(sw_int64_t);
							}
						else if (c == 'I'
							&& 
								(
								(intype == IO_TYPE_CHAR && *(CURR_CHAR_PTR()) == '6' && *(CURR_CHAR_PTR()+1) == '4')
								|| (intype == IO_TYPE_WCHAR && *(CURR_WCHAR_PTR()) == '6' && *(CURR_WCHAR_PTR()+1) == '4')
								)
							)
							{
							// Microsoft I64
							size = sizeof(sw_int64_t);
							GET_NEXT_CHAR(c);
							GET_NEXT_CHAR(c);
							}
						else if (c == 'C' || c == 'D' || c == 'Q' || c == 'T' || c == 'W')
							{
							if (c == 'C')
								{
								int	nc=0;

								if (intype == IO_TYPE_CHAR) nc = *(CURR_CHAR_PTR());
								if (intype == IO_TYPE_WCHAR) nc = *(CURR_WCHAR_PTR());

								if (nc != 's' && nc != 'c') break;
								}

							// Special size modifiers for %c and %s
							size = c;
							}
						else break;

						GET_NEXT_CHAR(c);
						}

					// Get the type
					switch (c)
						{
						case 'C': // same as lc
							size = sizeof(long);
							// fall into ...

						case 'c':
							out(info, va_arg(ap, int));
							GET_NEXT_CHAR(c);
							break;

						case 'q': // Quoted string (if FLAG_HASH only quoted if contains space or tab)
						case 's':
							{
							int	charsize=0;

							// by this point size will either be a sizeof(short), sizeof(long)
							// or a special conversion char.
							if (size == 'C' || size == sizeof(short))
								{
								charsize = sizeof(char);
								}
							else if (size == 'W' || size == sizeof(long))
								{
								charsize = sizeof(wchar_t);
								}
							else if (size == 'T')
								{
								charsize = intype;
								}
							else
								{
								// We have been given something wierd so pretend it's a char string
								charsize = sizeof(char);
								}

							// Now charsize will be either 1, 2, or 4 if not we are in trouble.
							if (charsize == sizeof(wchar_t))
								{
								if (c == 'q')
									{
									// qchar string
									wchar_t	*sp = va_arg(ap, wchar_t *);
									bool	quote;

									if (flags & FLAG_HASH) quote = (wcschr(sp, ' ') || wcschr(sp, '\t'));
									else quote = true;

									outString(sp, sizeof(wchar_t), precision, width, ' ', !(flags&FLAG_MINUS)!=0, quote, info, -1);
									}
								else
									{
									outString(va_arg(ap, wchar_t *), sizeof(wchar_t), precision, width, ' ', !(flags&FLAG_MINUS)!=0, false, info, -1);
									}
								}
							else
								{
								if (c == 'q')
									{
									// char string
									char	*sp = va_arg(ap, char *);
									bool	quote;

									if (flags & FLAG_HASH) quote = (strchr(sp, ' ') || strchr(sp, '\t'));
									else quote = true;

									outString(sp, sizeof(char), precision, width, ' ', !(flags&FLAG_MINUS)!=0, quote, info, -1);
									}
								else
									{
									outString(va_arg(ap, char *), sizeof(char), precision, width, ' ', !(flags&FLAG_MINUS)!=0, false, info, -1);
									}
								}

							GET_NEXT_CHAR(c);
							}
							break;

						case 'k':
						case 'K':
							{
							const char	*name;
							sw_uint64_t		v;
							double		d;
							int			idx=0;
							bool		useDefaultPrecision;
							char		tmp[64], *tp;


							// An unsigned number is expected
							switch (size)
								{
								case sizeof(short):
				#if defined(SW_PLATFORM_WINDOWS)
									v = va_arg(ap, unsigned short);
				#else
									v = (unsigned short)va_arg(ap, unsigned int);
				#endif
									size = sizeof(short);
									break;
									
								case sizeof(long):
									v = va_arg(ap, unsigned long);
									size = sizeof(long);
									break;
									
				#if SW_SIZEOF_LONG != 8
								case sizeof(sw_int64_t):	// a 64-bit integer
									v = va_arg(ap, sw_uint64_t);
									size = sizeof(sw_int64_t);
									break;
				#endif
									
								default:	// An integer
									v = va_arg(ap, unsigned int);
									size = sizeof(int);
									break;
									
								}

			#if defined(SW_PLATFORM_WINDOWS)
							d = (double)(sw_int64_t)v;
			#else
							d = v;
			#endif
							while (klist[idx+1].shortname && (v / 1000) != 0)
								{
								idx++;
								v /= 1024;
								d /= 1024.0;
								}

							useDefaultPrecision = (precision < 0);

							if (precision < 0) precision = klist[idx].defaultprecision;

							sw_snprintf(tmp, sizeof(tmp), "%.*f", precision, d);

							// If # format is NOT specified anything ending in 0 after the . is removed (i.e. 64.0KB -> 64KB, 64.120KB -> 64.12KB)
							if ((useDefaultPrecision && !(flags & FLAG_HASH)) && (tp = strchr(tmp, '.')) != NULL)
								{
								if (atoi(tp+1) == 0) *tp = 0;
								else
									{
									tp++;
									while (*tp)
										{
										if (atoi(tp) == 0)
											{
											*tp = 0;
											break;
											}
										tp++;
										}
									}
								}

							// If _ flag is specified we don't want a space between the number and the size specifier
							if (!(flags&FLAG_UNDERSCORE)) strcat(tmp, " ");

							// Add the size specifier
							if (c == 'k')
								{
								if ((flags & FLAG_GROUP) != 0) name = klist[idx].preferredname;
								else name = klist[idx].shortname;
								}
							else name = klist[idx].longname; 

							strcat(tmp, name);

							outString(tmp, 1, -1, width, (flags&FLAG_ZERO)?'0':' ', !(flags&FLAG_MINUS)!=0, 0, info, -1);
							GET_NEXT_CHAR(c);
							}
							break;

						case 'b': // binary
						case 'p': // pointer as hex
						case 'i': // integer
						case 'o': // octal integer
						case 'd': // integer
						case 'u': // unsigned integer
						case 'x': // unsigned hex int
						case 'X': // unsigned hex int
							{
							sw_uint64_t	v=0;
							int		base=10, charset=0;
							bool	negative=false, issigned=false;
							char	tmp[128], *sp=0;
							int		commaChar=0;

							// Set the default precision
							if (precision < 0) precision = 1;

							if (c == 'd' || c == 'i')
								{
								// A signed number is expected
								sw_int64_t	iv;

								issigned = true;
								switch (size)
									{
									case sizeof(short):
					#if defined(SW_PLATFORM_WINDOWS)
										iv = va_arg(ap, short);
					#else
										iv = (short)va_arg(ap, int);
					#endif
										size = sizeof(short);
										break;
										
									case sizeof(long):
										iv = va_arg(ap, long);
										size = sizeof(long);
										break;
										
					#if SW_SIZEOF_LONG != 8
									case sizeof(sw_int64_t):	// a 64-bit integer
										iv = va_arg(ap, sw_int64_t);
										size = sizeof(sw_int64_t);
										break;
					#endif
										
									default:	// An integer
										iv = va_arg(ap, int);
										size = sizeof(int);
										break;
										
									}

								if (iv < 0)
									{
									negative = true;
									v = -iv;
									}
								else v = iv;
								}
							else
								{
								// An unsigned number is expected
								switch (size)
									{
									case sizeof(short):
					#if defined(SW_PLATFORM_WINDOWS)
										v = va_arg(ap, unsigned short);
					#else
										v = (unsigned short)va_arg(ap, unsigned int);
					#endif
										size = sizeof(short);
										break;
										
									case sizeof(long):
										v = va_arg(ap, unsigned long);
										size = sizeof(long);
										break;
										
					#if SW_SIZEOF_LONG != 8
									case sizeof(sw_int64_t):	// a 64-bit integer
										v = va_arg(ap, sw_uint64_t);
										size = sizeof(sw_int64_t);
										break;
					#endif
										
									default:	// An integer
										v = va_arg(ap, unsigned int);
										size = sizeof(int);
										break;
										
									}
								}


							if (c == 'b')
								{
								base = 2;
								}
							else if (c == 'o')
								{
								base = 8;
								}
							else if (c == 'x')
								{
								base = 16;
								}
							else if (c == 'X')
								{
								base = 16;
								charset = 1;
								}
							else if (c == 'p')
								{
								base = 16;
								}


							commaChar=0;

							if (flags & FLAG_COMMA) commaChar = ',';
							else if (flags & FLAG_GROUP) commaChar = 0;

							int			dcount=0, digit;
							const char	*ncs=numericCharSet[charset];

							sp = &tmp[sizeof(tmp)-1];
							*sp = 0;

							if (size > (int)sizeof(sw_uint32_t) || v < SW_UINT32_MAX)
								{
								sw_uint64_t number=v;
								int			ndigits=0;

								while (number != 0 || precision > 0)
									{
									digit = (int)(number % base);
									number /= base;
									
 									if (commaChar && ndigits && (ndigits % 3) == 0)
										{
										*--sp = (char)commaChar;
										dcount++;
										}
										
									*--sp = ncs[digit];
									dcount++;
									ndigits++;
									precision--;
									}
								}
							else
								{
								sw_uint32_t number=(sw_uint32_t)v;
								int			ndigits=0;

								while (number != 0 || precision > 0)
									{
									digit = (int)(number % base);
									number /= base;
									
									if (commaChar && ndigits && (ndigits % 3) == 0)
										{
										*--sp = (char)commaChar;
										dcount++;
										}
										
									*--sp = ncs[digit];
									dcount++;
									ndigits++;
									precision--;
									}
								}

							//////////////////
							if (issigned)
								{
								if (negative)
									{
									*--sp = '-';
									dcount++;
									}
								else if (flags & FLAG_PLUS)
									{
									*--sp = '+';
									dcount++;
									}
								else if (flags & FLAG_SPACE)
									{
									*--sp = ' ';
									dcount++;
									}
								}
							else if ((flags & FLAG_HASH) && (c == 'X' || c == 'x') && v != 0)
								{
								*--sp = (char)c;
								*--sp = '0';
								dcount += 2;
								}

							if (flags & FLAG_ZERO)
								{
								if (negative || flags & (FLAG_PLUS|FLAG_SPACE))
									{
									out(info, *sp++);
									width--;
									dcount--;
									}
								}

							outString(sp, 1, -1, width, (flags&FLAG_ZERO)?'0':' ', !(flags&FLAG_MINUS)!=0, 0, info, dcount);
							GET_NEXT_CHAR(c);
							}
							break;

						/*
						case 'g':
						case 'G':
							{
							double	v = va_arg(ap, double);
							char	*sp, *num, *np;
							int	sign=0;


							if (precision < 0) precision = 6;
							char	*tmp = (char *)pMemoryManager->malloc(width + precision + 8);
							gcvt(v, precision, tmp);
							num = strdup(tmp);
							sign = (*num == '-');

							if (c == 'G' && (sp = strchr(num, 'e'))) *sp = 'E';
							np = num;
							sp = tmp;
							if (sign) *sp++ = *np++;
							else if (flags & FLAG_PLUS) *sp++ = '+';
							else if (flags & FLAG_SPACE) *sp++ = ' ';
							while (*np) *sp++ = *np++;
							*sp = 0;

							pMemoryManager->free(num);

							sp = tmp;
							if (flags & FLAG_ZERO)
								{
								if (sign || flags & (FLAG_PLUS|FLAG_SPACE))
								{
								out(info, *sp++);
								width--;
								}
								}

							outString(sp, 0, -1, width, (flags&FLAG_ZERO)?'0':' ', !(flags&FLAG_MINUS)!=0, false, info, -1);
							GET_NEXT_CHAR(c);
							pMemoryManager->free(tmp);
							}
							break;
			*/

						case 'n':
							{
							int	*ip = va_arg(ap, int *);

							*ip = info->count;
							GET_NEXT_CHAR(c);
							}
							break;

						case 'e':
						case 'E':
						case 'f':
						case 'g':
						case 'G':
							{
							// Use the real sprintf for convienience
							double	v = va_arg(ap, double);
							char	tmp[512];	// Is this enough?
							char	fmtstr[64];

							if (intype == IO_TYPE_WCHAR)
								{
								int	flen=(int)(CURR_WCHAR_PTR() - fpstart.wp);

								if (flen >= (int)sizeof(fmtstr))
									{
									// Somethings's wrong here, but keep going as best we can.
									flen = sizeof(fmtstr)-1;
									}

								const wchar_t	*wp=fpstart.wp;
								char			*fp=fmtstr;

								while (flen-- > 0)
									{
									// We can safely ignore char conversions here since format strings can
									// only contain ascii chars in this portion.
									*fp++ = (char)(*wp++);
									}
								*fp = 0;
								}
							else // CHAR (default)
								{
								int	flen=(int)(CURR_CHAR_PTR() - fpstart.cp);

								if (flen >= (int)sizeof(fmtstr))
									{
									// Somethings's wrong here, but keep going as best we can.
									flen = sizeof(fmtstr)-1;
									}

								strncpy(fmtstr, fpstart.cp, flen);
								fmtstr[flen] = 0;
								}

							if ((flags & (FLAG_VARIABLE_WIDTH|FLAG_VARIABLE_PRECISION)) == (FLAG_VARIABLE_WIDTH|FLAG_VARIABLE_PRECISION)) sprintf(tmp, fmtstr, width, precision, v);
							else if (flags & (FLAG_VARIABLE_WIDTH)) sprintf(tmp, fmtstr, width, v);
							else if (flags & (FLAG_VARIABLE_PRECISION)) sprintf(tmp, fmtstr, precision, v);
							else sprintf(tmp, fmtstr, v);

							outString(tmp, 1, -1, 0, ' ', 0, false, info, -1);
							GET_NEXT_CHAR(c);
							}
							break;

						default:
							if (c) 
								{
								out(info, c);
								GET_NEXT_CHAR(c);
								}

							break;
						}

					}

				if ((info->flags & SW_PRINTF_FLAG_LIMIT) != 0)
					{
					if (info->limit <= 1) break;
					}

				// Reset input count and pointer
				count = 0;
				pFrom = CURR_CHAR_PTR() - intype;
				}
			else
				{
				//out(info, c);
				count++;
				GET_NEXT_CHAR(c);
				}
			}

		if (count > 0)
			{
			outString(pFrom, intype, count, -1, ' ', false, false, info, count);
			}

		// Terminate string
		switch (info->outputtype)
			{
			case IO_TYPE_CHAR:
			case IO_TYPE_WCHAR:
				out(info, c);
				info->outpos--;
				break;
			}

		return 0;
		}


SWIFT_DLL_EXPORT int
sw_printf(const char *fmt, ...)
		{
		int	n;
		va_list	ap;

		va_start(ap, fmt);
		n = sw_vfprintf(stdout, fmt, ap);
		va_end(ap);

		return n;
		}


SWIFT_DLL_EXPORT int
sw_snprintf(char *buf, int size, const char *fmt, ...)
		{
		int	n;
		va_list	ap;

		va_start(ap, fmt);
		n = sw_vsnprintf(buf, size, fmt, ap);
		va_end(ap);

		return n;
		}


SWIFT_DLL_EXPORT int
sw_fprintf(FILE *f, const char *fmt, ...)
		{
		int	n;
		va_list	ap;

		va_start(ap, fmt);
		n = sw_vfprintf(f, fmt, ap);
		va_end(ap);

		return n;
		}


SWIFT_DLL_EXPORT int
sw_fprintf(SWTextFile &f, const char *fmt, ...)
		{
		int	n;
		va_list	ap;

		va_start(ap, fmt);
		n = sw_vfprintf(f, fmt, ap);
		va_end(ap);

		return n;
		}



SWIFT_DLL_EXPORT int
sw_xprintf(sw_printf_outputfunction *func, void *data, const char *fmt, ...)
		{
		int	n;
		va_list	ap;

		va_start(ap, fmt);
		n = sw_vxprintf(func, data, fmt, ap);
		va_end(ap);

		return n;
		}


// char strings with arg lists
SWIFT_DLL_EXPORT int
sw_vprintf(const char *fmt, va_list ap)
		{
		outputinfo	info;

		memset(&info, 0, sizeof(info));
		info.inputtype = sizeof(*fmt);
		info.flags = 0;
		info.in.cp = fmt;
		info.out.fp = stdout;
		info.limit = -1;
		info.nulterminate = false;
		info.outputtype = IO_TYPE_FILE;

		return sw_internal_doprintf(&info, ap);
		}


SWIFT_DLL_EXPORT int
sw_vsnprintf(char *buf, int size, const char *fmt, va_list ap)
		{
		outputinfo	info;

		memset(&info, 0, sizeof(info));
		info.inputtype = sizeof(*fmt);
		info.flags = SW_PRINTF_FLAG_LIMIT;
		info.in.cp = fmt;
		info.out.cp = buf;
		info.limit = size;
		info.nulterminate = true;
		info.outputtype = sizeof(*buf);

		return sw_internal_doprintf(&info, ap);
		}


SWIFT_DLL_EXPORT int
sw_vfprintf(FILE *f, const char *fmt, va_list ap)
		{
		outputinfo	info;

		memset(&info, 0, sizeof(info));
		info.inputtype = sizeof(*fmt);
		info.flags = 0;
		info.in.cp = fmt;
		info.out.fp = f;
		info.limit = -1;
		info.nulterminate = false;
		info.outputtype = IO_TYPE_FILE;

		return sw_internal_doprintf(&info, ap);
		}


SWIFT_DLL_EXPORT int
sw_vfprintf(SWTextFile &f, const char *fmt, va_list ap)
		{
		outputinfo	info;

		memset(&info, 0, sizeof(info));
		info.inputtype = sizeof(*fmt);
		info.flags = 0;
		info.in.cp = fmt;
		info.out.tp = &f;
		info.limit = -1;
		info.nulterminate = false;
		info.outputtype = IO_TYPE_TEXTFILE;

		return sw_internal_doprintf(&info, ap);
		}



SWIFT_DLL_EXPORT int
sw_vxprintf(sw_printf_outputfunction *func, void *data, const char *fmt, va_list ap)
		{
		outputinfo	info;

		memset(&info, 0, sizeof(info));
		info.inputtype = sizeof(*fmt);
		info.flags = 0;
		info.in.cp = fmt;
		info.out.custom.func = func;
		info.out.custom.data = data;
		info.limit = -1;
		info.nulterminate = false;
		info.outputtype = IO_TYPE_CUSTOM;

		return sw_internal_doprintf(&info, ap);
		}




// wchar_t strings
SWIFT_DLL_EXPORT int
sw_wprintf(const wchar_t *fmt, ...)
		{
		int	n;
		va_list	ap;

		va_start(ap, fmt);
		n = sw_vfwprintf(stdout, fmt, ap);
		va_end(ap);

		return n;
		}


SWIFT_DLL_EXPORT int
sw_snwprintf(wchar_t *buf, int size, const wchar_t *fmt, ...)
		{
		int	n;
		va_list	ap;

		va_start(ap, fmt);
		n = sw_vsnwprintf(buf, size, fmt, ap);
		va_end(ap);

		return n;
		}


SWIFT_DLL_EXPORT int
sw_fwprintf(FILE *f, const wchar_t *fmt, ...)
		{
		int	n;
		va_list	ap;

		va_start(ap, fmt);
		n = sw_vfwprintf(f, fmt, ap);
		va_end(ap);

		return n;
		}


SWIFT_DLL_EXPORT int
sw_fwprintf(SWTextFile &f, const wchar_t *fmt, ...)
		{
		int	n;
		va_list	ap;

		va_start(ap, fmt);
		n = sw_vfwprintf(f, fmt, ap);
		va_end(ap);

		return n;
		}


SWIFT_DLL_EXPORT int
sw_xwprintf(sw_printf_outputfunction *func, void *data, const wchar_t *fmt, ...)
		{
		int	n;
		va_list	ap;

		va_start(ap, fmt);
		n = sw_vxwprintf(func, data, fmt, ap);
		va_end(ap);

		return n;
		}



//  sw_tchar_t strings with arg lists
SWIFT_DLL_EXPORT int
sw_vwprintf(const wchar_t *fmt, va_list ap)
		{
		outputinfo	info;

		memset(&info, 0, sizeof(info));
		info.inputtype = sizeof(*fmt);
		info.flags = 0;
		info.in.wp = (const wchar_t *)fmt;
		info.out.fp = stdout;
		info.limit = -1;
		info.nulterminate = false;
		info.outputtype = IO_TYPE_FILE;

		return sw_internal_doprintf(&info, ap);
		}


SWIFT_DLL_EXPORT int
sw_vsnwprintf(wchar_t *buf, int size, const wchar_t *fmt, va_list ap)
		{
		outputinfo	info;

		memset(&info, 0, sizeof(info));
		info.inputtype = sizeof(*fmt);
		info.flags = SW_PRINTF_FLAG_LIMIT;
		info.in.wp = (const wchar_t *)fmt;
		info.outputtype = sizeof(*buf);
		info.limit = size;
		info.nulterminate = true;

		return sw_internal_doprintf(&info, ap);
		}


SWIFT_DLL_EXPORT int
sw_vfwprintf(FILE *f, const wchar_t *fmt, va_list ap)
		{
		outputinfo	info;

		memset(&info, 0, sizeof(info));
		info.inputtype = sizeof(*fmt);
		info.flags = 0;
		info.in.wp = (const wchar_t *)fmt;
		info.out.fp = f;
		info.limit = -1;
		info.nulterminate = true;
		info.outputtype = IO_TYPE_FILE;

		return sw_internal_doprintf(&info, ap);
		}


SWIFT_DLL_EXPORT int
sw_vfwprintf(SWTextFile &f, const wchar_t *fmt, va_list ap)
		{
		outputinfo	info;

		memset(&info, 0, sizeof(info));
		info.inputtype = sizeof(*fmt);
		info.flags = 0;
		info.in.wp = (const wchar_t *)fmt;
		info.out.tp = &f;
		info.limit = -1;
		info.nulterminate = true;
		info.outputtype = IO_TYPE_TEXTFILE;

		return sw_internal_doprintf(&info, ap);
		}


SWIFT_DLL_EXPORT int
sw_vxwprintf(sw_printf_outputfunction *func, void *data, const wchar_t *fmt, va_list ap)
		{
		outputinfo	info;

		memset(&info, 0, sizeof(info));
		info.inputtype = sizeof(*fmt);
		info.flags = 0;
		info.in.wp = (const wchar_t *)fmt;
		info.out.custom.func = func;
		info.out.custom.data = data;
		info.limit = -1;
		info.nulterminate = true;
		info.outputtype = IO_TYPE_CUSTOM;

		return sw_internal_doprintf(&info, ap);
		}





