/**
*** @file		SWPoint.h
*** @brief		A class to represent a point or x/y coordinate
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWPoint class.
***
**/

/*********************************************************************************
Copyright (c) 2006-2016 SwiftTec Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <swift/SWPoint.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif

SWPoint::SWPoint(sw_int32_t x, sw_int32_t y) :
	m_xpos(x),
	m_ypos(y)
		{
		}

/// Copy constructor
SWPoint::SWPoint(const SWPoint &other) :
	m_xpos(other.m_xpos),
	m_ypos(other.m_ypos)
		{
		}

/// Assignment operator
SWPoint &
SWPoint::operator=(const SWPoint &other)
		{
		m_xpos = other.m_xpos;
		m_ypos = other.m_ypos;

		return *this;
		}

#if defined(SW_PLATFORM_WINDOWS)
/// Convert to a COORD - win32 only
SWPoint::operator COORD() const
		{
		COORD	coord;

		coord.X = (SHORT)m_xpos;
		coord.Y = (SHORT)m_ypos;

		return coord;
		}
#endif

sw_status_t
SWPoint::writeToStream(SWOutputStream &stream) const
		{
		sw_status_t	res;

		res = stream.write(m_xpos);
		if (res == SW_STATUS_SUCCESS) res = stream.write(m_ypos);

		return res;
		}


/// Override for SWObject readFromStream
sw_status_t
SWPoint::readFromStream(SWInputStream &stream)
		{
		sw_status_t	res;
		sw_int32_t	x=0, y=0;

		res = stream.read(x);
		if (res == SW_STATUS_SUCCESS) res = stream.read(y);

		if (res == SW_STATUS_SUCCESS)
			{
			m_xpos = x;
			m_ypos = y;
			}

		return res;
		}
