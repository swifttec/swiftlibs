/**
*** @file		SWAbstractArray.h
*** @brief		A generic array of anything common to all array objects
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWAbstractArray class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWAbstractArray_h__
#define __SWAbstractArray_h__

#include <swift/SWMemoryManager.h>
#include <swift/SWLockableObject.h>




/**
*** @brief An abstract array class.
***
*** A generic array class which is self sizing. If an element is accessed 
*** at an index out of range the array is automatically resized to incorporate
*** that index and it's reference is then returned.
*** 
*** All memory is automatically created and return to the system on destruction
***
*** @author		Simon Sparkes
***/
class SWIFT_DLL_EXPORT SWAbstractArray : public SWLockableObject
		{
protected:
		/// Protected constructor
		SWAbstractArray(SWMemoryManager *pMemoryManager, sw_size_t elemsize);

public:
		/// Virtual destructor
		~SWAbstractArray();

		/// Return the size of the array
		sw_size_t				size() const;

		/// Return the size of the array
		sw_size_t				count() const;

		/// Clear/empty array of all elements
		virtual void			clear();

		/**
		*** @brief	Remove the one or more elements
		***
		*** @param[in]	pos		The position to remove the elements from.
		***	@param[in]	count	The number of elements to remove (default=1).
		**/
		virtual void	remove(sw_index_t pos, sw_size_t count=1);

public: // Microsoft compatibility methods
		void		RemoveAll()				{ clear(); }
		sw_size_t	GetSize() const			{ return size(); }

protected:
		/**
		*** @brief	Insert one or more blank elements
		***
		*** @param[in]	pos		The position to insert the elements at.
		***	@param[in]	count	The number of elements to insert (default=1).
		**/
		virtual void	insertElements(sw_index_t pos, sw_size_t count=1);

		/**
		*** @brief	Remove the one or more elements
		***
		*** @param[in]	pos		The position to remove the elements from.
		***	@param[in]	count	The number of elements to remove (default=1).
		**/
		virtual void	removeElements(sw_index_t pos, sw_size_t count=1);

		/**
		*** @brief	Test to see if the element at the specified index is defined
		***
		*** This method tests to see if the element at the index specified by pos
		*** is defined or not. This is dependent on the type of array being tested.
		***
		*** If the array type is simple (e.g. SWIntegerArray) then this will return
		*** true if pos is a valid index, i.e. pos is >= 0 and less than the number
		*** of elements in the array. If the array type is backed by objects or more
		*** complex types then this method can determine if the element at the
		*** specified index has been set.
		***
		*** @param[in]	pos		The index of the element to check.
		***
		*** @return				true if the specifed element is defined, or false otherwise.
		**/
		virtual bool	defined(sw_index_t pos) const;

		/// Ensure that the array has sufficient spaces for the specified number of elements
		virtual void	ensureCapacity(sw_size_t nelems);

protected:
		//SWBuffer	m_data;		///< The memory buffer containing the array data
		SWMemoryManager	*m_pMemoryManager;	///< The memory manager
		char			*m_pData;			///< The data buffer
		sw_size_t		m_capacity;			///< The capacity of the data buffer
		sw_size_t		m_nelems;	///< The number of elements in the array
		sw_size_t		m_elemsize;	///< The size of each element
		};




#endif
