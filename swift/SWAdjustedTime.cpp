/**
*** @file		SWAdjustedTime.cpp
*** @brief		A class to represent a time in seconds and microseconds  since 1st Jan 1900
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWAdjustedTime class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <math.h>
#if !defined(WIN32) && !defined(_WIN32)
#include <sys/time.h>
#endif
#include <swift/SWAdjustedTime.h>
#include <swift/SWDate.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




// The number of seconds between 1/1/1900 and 1/1/1970
// Our time and Unix time


SWAdjustedTime::SWAdjustedTime() :
	m_dst(-1)
		{
		calculateDST();
		}


SWAdjustedTime::SWAdjustedTime(const SWTimeZone &timezone) :
	m_dst(-1),
	m_timezone(timezone)
		{
		calculateDST();
		}


SWAdjustedTime::SWAdjustedTime(const SWTime &other, const SWTimeZone &timezone) :
	SWTime(other),
	m_dst(-1),
	m_timezone(timezone)
		{
		calculateDST();
		}


// Copy constructor
SWAdjustedTime::SWAdjustedTime(const SWAdjustedTime &other) :
	SWTime(other)
		{
		m_dst = other.m_dst;
		m_timezone = other.m_timezone;
		}


// Assignment operator
SWAdjustedTime &
SWAdjustedTime::operator=(const SWAdjustedTime &other)
		{
		SWTime::operator=(other);

		m_dst = other.m_dst;
		m_timezone = other.m_timezone;

		return *this;
		}

// Assignmemt from a unix timespec
SWAdjustedTime &
SWAdjustedTime::operator=(const sw_timespec_t &v)
		{
		SWTime::operator=(v);
		return *this;
		}


// Assignmemt from a unix timeval
SWAdjustedTime &
SWAdjustedTime::operator=(const sw_timeval_t &v)
		{
		SWTime::operator=(v);
		return *this;
		}


// Assignment from a unix time
SWAdjustedTime &
SWAdjustedTime::operator=(time_t v)
		{
		SWTime::operator=(v);
		return *this;
		}


// Assignment from a double (SWTime format only)
SWAdjustedTime &
SWAdjustedTime::operator=(double v)
		{
		SWTime::operator=(v);
		return *this;
		}


// Assignment from a windows FILETIME
SWAdjustedTime &
SWAdjustedTime::operator=(const FILETIME &v)
		{
		SWTime::operator=(v);
		return *this;
		}


// Assignment from a windows SYSTEMTIME
SWAdjustedTime &
SWAdjustedTime::operator=(const SYSTEMTIME &v)
		{
		SWTime::operator=(v);
		return *this;
		}


// Assignment from a SWDate
SWAdjustedTime &
SWAdjustedTime::operator=(const SWDate &v)
		{
		SWTime::operator=(v);
		return *this;
		}


// Assignment from a sw_tm_t (unix)
SWAdjustedTime &
SWAdjustedTime::operator=(const sw_tm_t &v)
		{
		SWTime::operator=(v);
		return *this;
		}


void
SWAdjustedTime::setFromLocalVariantTime(double v)
		{
		// Adjust the variant to convert to GMT time
		SWTime::set(v, true);
		m_seconds = m_seconds + m_timezone.tzAdjustment() - (m_dst>0?m_timezone.dstAdjustment():0);
		}


bool
SWAdjustedTime::set(sw_int64_t secs, sw_uint32_t nsecs, TimeType tt)
		{
		bool	res;

		res = SWTime::set(secs, nsecs, tt);
		if (res)
			{
			m_seconds = m_seconds + m_timezone.tzAdjustment() - (m_dst>0?m_timezone.dstAdjustment():0);
			}

		return res;


		}


bool
SWAdjustedTime::set(const SWString &value, const SWString &format)
		{
		bool	res;

		// First call SWTime to do the conversion
		res = SWTime::set(value, format);
		if (res)
			{
			m_seconds = m_seconds + m_timezone.tzAdjustment() - (m_dst>0?m_timezone.dstAdjustment():0);
			}

		return res;
		}


bool
SWAdjustedTime::set(int nYear, int nMonth, int nDay, int nHour, int nMinute, int nSeconds, int nNanoseconds)
		{
		bool	res;

		// First call SWTime to do the conversion
		res = SWTime::set(nYear, nMonth, nDay, nHour, nMinute, nSeconds, nNanoseconds);
		if (res)
			{
			m_seconds = m_seconds + m_timezone.tzAdjustment() - (m_dst>0?m_timezone.dstAdjustment():0);
			}

		return res;
		}

void
SWAdjustedTime::set(double clock, bool isVariantTime)
		{
		// First call SWTime to do the conversion
		SWTime::set(clock, isVariantTime);
		m_seconds = m_seconds + m_timezone.tzAdjustment() - (m_dst>0?m_timezone.dstAdjustment():0);
		}


// Return the hour (0-23)
int
SWAdjustedTime::hour() const
		{
		return (int)((adjustedSeconds()/3600) % 24);
		}

// Return the minute (0-59)
int
SWAdjustedTime::minute() const
		{
		return (int)((adjustedSeconds()/60) % 60);
		}

// Return the second (0-59)
int
SWAdjustedTime::second() const
		{
		return (int)(adjustedSeconds() % 60);
		}


// Return the Julian Day Number for this time
int
SWAdjustedTime::julian() const
		{
		return (int)(adjustedSeconds() / SECONDS_IN_DAY) + JULIAN_DAY_31ST_DEC_1899;
		}



/// Return the name of the timezone
SWString
SWAdjustedTime::tzName(int dst) const
		{
		return (dst > 0)?m_timezone.dstText():m_timezone.stdText();
		}




/*
** PRIVATE
** Determine is we are in DST or not
** 
** If possible we use the local unix algorithm to do the calculation.
** otherwise we use the following rule:
**
** Daylight time is defined as starting at 2am on the first Sunday in April 
** (2am becomes 3am), and ending at 2am on the last Sunday in October (2am 
** becomes 1am). 
** 
** Non-standard stuff (pre 1970 calculations) based on perl code 
** written on 18 March 1998 by S. Edberg, UC Davis. 
**------------------------------------------------------------------------------ 
*/
void
SWAdjustedTime::calculateDST()
		{
		if (m_timezone.dstAvailable() > 0)
			{
			SWDate	ud(*this);

			m_dst = 0;
			if (ud.year() >= 1970 && ud.year() < 2038)
				{
				// Use the Unix localtime algorithm
				time_t		t = toUnixTime();
				struct tm	tm, *tp;

				tp = localtime_r(&t, &tm);
				m_dst = tp->tm_isdst;
				}
			else
				{
				// Standard not available, use the next best guess
				int	dom = ud.day(), dow = ud.dayOfWeek(), mon = ud.month(), hr=hour();

				if      (mon >  4  && mon <  10)									m_dst = true;	// May thru September 
				else if (mon == 4  && dom >  7)										m_dst = true;	// After first week in April 
				else if (mon == 4  && dom <= 7  && dow == 0 && hr >= 2)				m_dst = true;	// After 2am on first Sunday ($dow=0) in April 
				else if (mon == 4  && dom <= 7  && dow != 0 && (dom-dow > 0))		m_dst = true;	// After Sunday of first week in April 
				else if (mon == 10 && dom <  25)									m_dst = true;	// Before last week of October 
				else if (mon == 10 && dom >= 25 && dow == 0 && hr < 2)				m_dst = true;	// Before 2am on last Sunday in October 
				else if (mon == 10 && dom >= 25 && dow != 0 && (dom-24-dow < 1) )	m_dst = true;	// Before Sunday of last week in October 
				}
			}
		else m_dst = m_timezone.dstAvailable();
		}


int
SWAdjustedTime::dst() const
		{
		return m_dst;
		}


void
SWAdjustedTime::setTimeZone(const SWTimeZone &timezone)
		{
		m_timezone = timezone;
		}


sw_int64_t
SWAdjustedTime::adjustedSeconds() const
		{
		return m_seconds - m_timezone.tzAdjustment() + (m_dst>0?m_timezone.dstAdjustment():0);
		}




