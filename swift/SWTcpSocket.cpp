/**
*** @file		SWTcpSocket.cpp
*** @brief		A TCP socket
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWTcpSocket class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWGuard.h>

#include <swift/SWTcpSocket.h>
#include <swift/sw_net.h>


#if !defined(SW_PLATFORM_WINDOWS)
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>
#else
	#if defined(SW_BUILD_MFC)
		#include <winsock2.h>
	#endif
#endif


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




SWTcpSocket::SWTcpSocket()
		{
		}


SWTcpSocket::~SWTcpSocket()
		{
		close();
		}



sw_status_t
SWTcpSocket::close()
		{
		SWGuard		guard(this);
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (isListening() || isConnected())
			{
			res = SWSocket::shutdown(_fd, 2);
			}

		if (_fd != SW_NET_INVALID_SOCKET)
			{
			res = SWSocket::close(_fd);
			_fd = SW_NET_INVALID_SOCKET;
			_flags.clear();
			}
		
		return setLastError(res);
		}


/*
sw_status_t
SWTcpSocket::connect(const SWString &hostname, int port)
		{
		return SWSocket::connect(SWSocketAddress(hostname, port));
		}


sw_status_t
SWTcpSocket::listen(int port, int maxPending)
		{
		return listen(SWSocketAddress("", port), maxPending);
		}
*/


sw_status_t
SWTcpSocket::open()
		{
		SWGuard		guard(this);
		sw_status_t	res=SW_STATUS_SUCCESS;

		// Attempt to create the socket
		res = setLastError(SWSocket::open(AF_INET, SOCK_STREAM, 0, _fd));

		if (res == SW_STATUS_SUCCESS)
			{
			_flags.setBit(SW_SOCKFLAG_STREAM);
			}

		return res;
		}



sw_status_t		
SWTcpSocket::listen(const SWSocketAddress &addr, int maxPending)
		{
		SWGuard		guard(this);
		sw_status_t	res=SW_STATUS_SUCCESS;

		// Check to make sure we are not connected already
		if (isConnected() || isListening())
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}
		else if (addr.type() != SWSocketAddress::IP4)
			{
			res = SW_STATUS_INVALID_PARAMETER;
			}

		if (res == SW_STATUS_SUCCESS && !isOpen())
			{
			// Auto-open the socket
			res = open();
			}

		if (res == SW_STATUS_SUCCESS)
			{
			res = bind(addr);
			if (res == SW_STATUS_SUCCESS)
				{
				res = SWSocket::listen(_fd, maxPending);
				}

			if (res != SW_STATUS_SUCCESS)
				{
				SWSocket::close(_fd);
				_fd = SW_NET_INVALID_SOCKET;
				}
			else
				{
				_flags.setBit(SW_SOCKFLAG_LISTENING);
				_addr = addr;
				}
			}

		return setLastError(res);
		}


sw_status_t		
SWTcpSocket::accept(SWSocket *&pSocket, sw_uint64_t waitms)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		SWGuard	guard(this);

		if (!isListening())
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}
		else
			{
			res = SWSocket::waitForInput(_fd, waitms);
			if (res == SW_STATUS_SUCCESS)
				{
				sw_sockhandle_t	sock;
				SWSocketAddress	addr;

				res = SWSocket::accept(_fd, sock, addr);
				if (res == SW_STATUS_SUCCESS)
					{
					SWTcpSocket	*pNewSocket = new SWTcpSocket();

					pNewSocket->_fd = sock;
					pNewSocket->_flags.setBit(SW_SOCKFLAG_CONNECTED);
					pNewSocket->_addr = addr;
					pSocket = pNewSocket;
					}
				}
			}

		return setLastError(res);
		}




