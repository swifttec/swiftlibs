/**
*** @file		SWCollectionIterator.h
*** @brief		An abstract internal collection iterator.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWCollectionIterator class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWCollectionIterator_h__
#define __SWCollectionIterator_h__

#include <swift/swift.h>
#include <swift/SWObject.h>
#include <swift/SWAbstractIterator.h>



class SWAbstractIterator;
class SWCollection;

/**
*** @internal
***
*** @brief An abstract internal collection iterator.
***
*** SWCollectionIterator is a special class designed to be used by developers
*** which are implementing new collections.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWCollectionIterator : public SWObject
		{
// Forward Declarations
friend class SWCollection;
friend class SWAbstractIterator;

protected:
		SWCollectionIterator(SWCollection *c);		// Constructor

public:
		/// Returns true if the iteration has a valid current element.
		virtual bool	hasCurrent()=0;

		/// Returns true if the iteration has more elements after the current one.
		virtual bool	hasNext()=0;

		/// Returns true if the iteration has more elements before the current one.
		virtual bool	hasPrevious()=0;

		/// Returns the current element in the interation.
		virtual void *	current()=0;

		/// Returns the next element in the interation.
		virtual void *	next()=0;

		/// Returns the next element in the interation.
		virtual void *	previous()=0;

		/// Removes the current object from the collection.
		virtual bool	remove(void *pOldData)=0;

		/// Sets/updates the current object in underlying collection.
		virtual bool	set(void *pNewData, void *pOldData)=0;

		/// Adds the object to the underlying collection
		virtual bool	add(void *data, SWAbstractIterator::AddLocation where)=0;

protected:
		/// Increment the refcount and return the new value
		sw_size_t		addRef();

		/// Decrement the refcount and return the new value
		sw_size_t		dropRef();

		/// Return the current refcount value
		sw_size_t		refCount();

		/// Check the collection modCount with the iterator one and throw an exception if not the same
		void			checkForComodification();

public:
		/// Create a SWIterator object which references the given SWCollectionIterator
		//static SWIterator	iterator(SWCollectionIterator *it);

protected:
		SWCollection	*m_pCollection;	///< The collection being iterated over
		sw_size_t		m_refcount;		///< The number of references to this iterator from SWIterator objects
		sw_size_t		m_modcount;		///< The modCount value from the underlying collection
		};




#endif
