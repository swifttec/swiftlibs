/**
*** @file		SWSharedMemoryManager.cpp
*** @brief		An memory allocator that sub-allocates from	a shared memory segment.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWSharedMemoryManager class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




#include "stdafx.h"

#include <swift/SWSharedMemoryManager.h>
#include <swift/SWGuard.h>
#include <swift/SWProcessMutex.h>
#include <swift/sw_printf.h>


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





// The magic number that marks the segment (at offset 0)
#define SHM_LIST_MAGIC	0x1a2b3c4d

// The magic number that marks the each memory block (at offset 0)
#define SHM_BLOCK_MAGIC	0x1a2b3c4d

// The minimum size of memory to allocate.
// We define this as the size of header PLUS the minimum data size per block
//
// Shared memory will be a relatively limited resource so we want to keep waste
// to a minimum so we define the minimum data size to be 4 integers which
// is probabbly the smallest data element we are likely to alloc.
#define SHM_MIN_ALLOC_SIZE	(sizeof(ShmMemoryHeader)+(4*sizeof(int)))

SWSharedMemoryManager::SWSharedMemoryManager() :
	_pMutex(0),
	_ownMutex(false),
	_reserved(0)
		{
		}

SWSharedMemoryManager::~SWSharedMemoryManager()
		{
		detach();
		}


sw_status_t
SWSharedMemoryManager::attach(sw_uint32_t id, SWMutex *pMutex, sw_uint32_t reserved)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		_reserved = reserved;

		if (pMutex == NULL)
			{
			SWString	name;

			name.format("SWSharedMemoryManager:%d", id);

			_pMutex = new SWProcessMutex(name);
			_ownMutex = true;
			}
		else
			{
			_pMutex = pMutex;
			_ownMutex = false;
			}

		_pMutex->acquire();

		res = _shmem.attach(id);
		if (res == SW_STATUS_SUCCESS)
			{
			if (getFreeList()->magic != SHM_LIST_MAGIC || getInUseList()->magic != SHM_LIST_MAGIC)
				{
				// This is not a valid segment.
				res = SW_STATUS_INVALID_SHARED_MEMORY_SEGMENT;
				}
			}

		_pMutex->release();

		if (res != SW_STATUS_SUCCESS)
			{
			detach();
			}

		return res;
		}


sw_status_t
SWSharedMemoryManager::create(sw_uint32_t id, sw_uint32_t size, SWMutex *pMutex, sw_uint32_t reserved)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		_reserved = reserved;
		if (pMutex == NULL)
			{
			_pMutex = new SWProcessMutex(id);
			_ownMutex = true;
			}
		else
			{
			_pMutex = pMutex;
			_ownMutex = false;
			}

		_pMutex->acquire();

		res = _shmem.create(id, size);
		if (res == SW_STATUS_SUCCESS)
			{
			// We must initialise the shared-memory structures

			// The free list is located at offset 0
			ShmMemoryList	*pList=getFreeList();

			pList = getFreeList();
			pList->magic = SHM_LIST_MAGIC;
			pList->head = pList->tail = 0;
			pList->nelems = 0;
			pList->size = 0;
			pList->ordered = 1;

			pList = getInUseList();
			pList->magic = SHM_LIST_MAGIC;
			pList->head = pList->tail = 0;
			pList->nelems = 0;
			pList->size = 0;
			pList->ordered = 0;

			// The first free block is located at location 2*sizeof(ShmMemoryList)
			ShmMemoryHeader	*pHdr=(ShmMemoryHeader *)address(_reserved + 2*sizeof(ShmMemoryList));
			pHdr->magic = SHM_BLOCK_MAGIC;
			pHdr->next = 0;
			pHdr->prev = 0;
			pHdr->actualSize = size - (2*sizeof(ShmMemoryList)) - _reserved;
			pHdr->refcount = 0;

			addToList(getFreeList(), pHdr);
			}

		_pMutex->release();

		if (res != SW_STATUS_SUCCESS)
			{
			detach();
			}

		return res;
		}


sw_status_t
SWSharedMemoryManager::detach()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (_ownMutex)
			{
			delete _pMutex;
			_pMutex = NULL;
			_ownMutex = false;
			}

		res = _shmem.detach();

		return res;
		}



SWSharedMemoryManager::SWSharedMemoryManager(sw_uint32_t id, sw_uint32_t size, SWMutex *pMutex, sw_uint32_t reserved) :
	_pMutex(0),
	_ownMutex(false),
	_reserved(0)
		{
		sw_status_t	status;

		status = attach(id, pMutex, reserved);
		if (status == SW_STATUS_INVALID_SHARED_MEMORY_SEGMENT)
			{
			// This is not a valid segment. We must not overwrite it so we must throw an exception.
			throw SW_EXCEPTION_TEXT(SWMemoryManagerException, "Existing memory segment has invalid magic number");
			}
		else if (status != SW_STATUS_SUCCESS)
			{
			status = create(id, size, pMutex);
			if (status != SW_STATUS_SUCCESS)
				{
				throw SW_EXCEPTION_TEXT(SWMemoryManagerException, "Can't create shared memory segment");
				}
			}
		}


/*
** Insert the memory block into the memory list.
** If the memory list is marked as ordered we insert the
** block in space order (least first) so we can do fast
** memory allocation. Memory free is the slow operation.
*/
void
SWSharedMemoryManager::addToList(ShmMemoryList *pList, ShmMemoryHeader *pHdr)
		{
		if (pList->head == 0)
			{
			// Empty list
			pHdr->next = 0;
			pHdr->prev = 0;
			pList->head = pList->tail = offset(pHdr);
			}
		else
			{
			int	done=0;

			if (pList->ordered)
				{
				ShmMemoryHeader	*p=NULL;
				sw_uint32_t		off;

				// Insert in size order
				off = pList->head;
				while (off != 0)
					{
					p = getHeaderAtOffset(off);

					if (p->actualSize >= pHdr->actualSize) break;
					off = p->next;
					}
				/*
				for (p=(ShmMemoryHeader *)address(pList->head);p!=NULL&&p->actualSize<pHdr->actualSize;)
					{
					p = (ShmMemoryHeader *)address(p->next);
					}
				*/

				if (off != 0)
					{
					// p is still valid here so don't re-assign using address(off)

					if (p == getHeaderAtOffset(pList->head))
						{
						// Add to head of pList
						pHdr->prev = 0;
						pHdr->next = pList->head;
						getHeaderAtOffset(pList->head)->prev = offset(pHdr);
						pList->head = offset(pHdr);
						done = 1;
						}
					else
						{
						// Insert in front of current element
						pHdr->next = offset(p);
						pHdr->prev = p->prev;
						if (p->prev) getHeaderAtOffset(p->prev)->next = offset(pHdr);
						p->prev = offset(pHdr);
						done = 1;
						}
					}
				}

			if (!done)
				{
				// Add to end of list
				pHdr->next = 0;
				pHdr->prev = pList->tail;
				((ShmMemoryHeader *)address(pList->tail))->next = offset(pHdr);
				pList->tail = offset(pHdr);
				}
			}

		pList->nelems += 1;
		pList->size += pHdr->actualSize;
		}


// Remove the state from our internal list
void
SWSharedMemoryManager::removeFromList(ShmMemoryList *pList, ShmMemoryHeader *pHdr)
		{
		if (pHdr->next != 0) getHeaderAtOffset(pHdr->next)->prev = pHdr->prev;
		if (pHdr->prev != 0) getHeaderAtOffset(pHdr->prev)->next = pHdr->next;

		if (pList->head == offset(pHdr)) pList->head = pHdr->next;
		if (pList->tail == offset(pHdr)) pList->tail = pHdr->prev;

		pList->nelems -= 1;
		pList->size -= pHdr->actualSize;

		// For safety!
		pHdr->next = 0;
		pHdr->prev = 0;
		}


/*
** insertIntoFreeList inserts the memory back into the free list
** by attempting to combine the block with one already on the free list.
** If one is found the space is merged and the result is recursively inserted
** into the free list again. Why? well we want to keep space use to a minimum
** so we can use this in the kernel, and the overhead should not be too great
** at least I hope not. Another advantage of this method is it helps to keep
** free list size down too and will allow us to give back memory to the system
*/
void
SWSharedMemoryManager::insertIntoFreeList(ShmMemoryHeader *pHdr)
		{
		ShmMemoryHeader	*hp=0;
		sw_uint32_t		off=0;

		// check the freelist to see if we can join it to an existing block
		ShmMemoryList	*pList=getFreeList();
		bool			doit=true;

		while (doit)
			{
			doit = false;

			for (off=pList->head;off!=0;off=hp->next)
				{
				hp = getHeaderAtOffset(off);

				if ((ShmMemoryHeader *)((char *)hp + hp->actualSize) == pHdr)
					{
					// Join to end of existing block in free list
					//printf("Appending space to freelist block %p\n", hp);

					// Remove and re-insert into list to keep order
					removeFromList(pList, hp);
					hp->actualSize += pHdr->actualSize;

					// The recursive insert replaced to avoid potential stack problems
					// WAS: insertIntoFreeList(hp);
					doit = true;
					pHdr = hp;
					break;
					}
				else if ((ShmMemoryHeader *)((char *)pHdr + pHdr->actualSize) == hp)
					{
					//printf("Appending freelist block space %p to hdr and re-inserting\n", hp);
					pHdr->actualSize += hp->actualSize;
					removeFromList(pList, hp);

					// The recursive insert replaced to avoid potential stack problems
					// WAS: insertIntoFreeList(pHdr);
					doit = true;
					break;
					}
				}
			}

		if (off == 0)
			{
			// We couldn't do a join so simply add it.
			addToList(pList, pHdr);
			}
		}




void
SWSharedMemoryManager::dump(FILE *f)
		{
		if (!_shmem.isAttached()) throw SW_EXCEPTION_TEXT(SWMemoryManagerException, "Shared memory not attached");

		SWMutexGuard	guard(_pMutex);

		sw_uint32_t		off;

		sw_fprintf(f, "\n------------ BEGIN ShmMemoryManager Dump-------------\n");
		sw_fprintf(f, "Shared Memory Segment : ID %d loaded at address %p\n", _shmem.id(), _shmem.address());

		sw_fprintf(f, "------------ FREE LIST -------------\n");
		sw_fprintf(f, "Elements : %u\n", getFreeList()->nelems);
		sw_fprintf(f, "Bytes    : %u\n", getFreeList()->size);

		off = getFreeList()->head;
		while (off != 0)
			{
			ShmMemoryHeader	*p=getHeaderAtOffset(off);

			sw_fprintf(f, "  Block of size %u at offset %u\n", p->actualSize, off);
			off = p->next;
			}

		sw_fprintf(f, "----------- IN-USE LIST ------------\n");
		sw_fprintf(f, "Elements : %u\n", getInUseList()->nelems);
		sw_fprintf(f, "Bytes    : %u\n", getInUseList()->size);

		off = getInUseList()->head;
		while (off != 0)
			{
			ShmMemoryHeader	*p=getHeaderAtOffset(off);

			sw_fprintf(f, "  Block of size %u (%u data) at offset %u with refcount %u - data at address %p\n", p->actualSize, p->actualSize - sizeof(ShmMemoryHeader), off, p->refcount, (char *)p + sizeof(ShmMemoryHeader));
			off = p->next;
			}

		sw_fprintf(f, "------------------------------------\n\n");
		}













/*********************************************************************************
** The following methods form the primary public interface:
** malloc, realloc, free.
*********************************************************************************/


void *
SWSharedMemoryManager::malloc(sw_size_t nbytes)
		{
		if (!_shmem.isAttached()) throw SW_EXCEPTION_TEXT(SWMemoryManagerException, "Shared memory not attached");

		if (nbytes == 0) return NULL;

		ShmMemoryHeader	*p=NULL, *pHdr=NULL;
		sw_size_t			wanted = nbytes+sizeof(ShmMemoryHeader);
		
#ifdef DEBUG_SHM_ALLOCATOR
		// Go check all the in-use blocks
		checkInUseBlocks();

		// Add space for magic tail
		wanted += sizeof(int);
#endif

		// From this point onwards we want to be locked
		SWMutexGuard	guard(_pMutex);


		ShmMemoryList	*pList=getFreeList();

		if (pList->head)
			{
			// Search the free list for the most suitable block (i.e. the smallest)
			sw_uint32_t	off;

			for (off=pList->head;off!=0;off=p->next)
				{
				p = (ShmMemoryHeader *)address(off);
				if (p->actualSize > wanted)
					{
					// Since the list is ordered the first find will be the smallest.
					pHdr = p;
					break;
					}
				}
			}

		if (pHdr == NULL)
			{
			// We've not enough memory for this operation - return NULL
			return NULL;
			}

		// Remove from the free list and add to the used list
		removeFromList(pList, pHdr);

		// By now we have a block of memory in pHdr, but do we want all of it?
		if (pHdr->actualSize - wanted >= SHM_MIN_ALLOC_SIZE)
			{
			// Too much left over so split it up into 2 blocks
			sw_size_t		modwanted;

			modwanted = (1+(wanted / sizeof(void *))) * sizeof(void *);
			p = (ShmMemoryHeader *)((char *)pHdr + modwanted);
			p->actualSize = pHdr->actualSize - modwanted;
			p->next = p->prev = 0;
			p->magic = SHM_BLOCK_MAGIC;
			pHdr->actualSize = modwanted;
			pHdr->refcount = 0;

			// Add the new block to the free list
			addToList(getFreeList(), p);
			}

		pHdr->refcount = 1;
		addToList(getInUseList(), pHdr);

		//m_allocSize += nbytes;
		//m_usedBytes += pHdr->actualSize;
		//m_availableBytes -= pHdr->actualSize;

		return (char *)pHdr+sizeof(ShmMemoryHeader);
		}


void *
SWSharedMemoryManager::realloc(void *pOldData, sw_size_t nbytes)
		{
		if (!_shmem.isAttached()) throw SW_EXCEPTION_TEXT(SWMemoryManagerException, "Shared memory not attached");

		ShmMemoryHeader	*pHdr=NULL;
		void			*newptr;

		// The simple cases
		if (pOldData == NULL) return this->malloc(nbytes);
		if (nbytes == 0)
			{
			this->free(pOldData);
			return NULL;
			}
		
		pHdr = (ShmMemoryHeader *)((char *)pOldData - sizeof(ShmMemoryHeader));

		sw_size_t	dataspace=pHdr->actualSize - sizeof(ShmMemoryHeader);

		if (dataspace >= nbytes)
			{
			// We can continue to use the same block as it already has enough space.
			sw_size_t	wanted=nbytes + sizeof(ShmMemoryHeader);

			if (pHdr->actualSize - wanted >= SHM_MIN_ALLOC_SIZE)
				{
				// Too much left over so split it up into 2 blocks
				sw_size_t				modwanted = (1+(wanted / sizeof(void *))) * sizeof(void *);
				ShmMemoryHeader *p = (ShmMemoryHeader *)((char *)pHdr + modwanted);

				p->actualSize = pHdr->actualSize - modwanted;
				p->next = p->prev = 0;
				p->magic = SHM_BLOCK_MAGIC;
				pHdr->actualSize = modwanted;
				pHdr->refcount = 0;

				// Add the new block to the free list
				SWMutexGuard	guard(_pMutex);

				addToList(getFreeList(), p);
				}

			return pOldData;
			}

		// We must copy the info to a new block
		if ((newptr = this->malloc(nbytes)) == NULL) return NULL;

		memcpy(newptr, pOldData, dataspace);
		this->free(pOldData);

		return newptr;
		}



void
SWSharedMemoryManager::free(void *pData)
		{
		if (!_shmem.isAttached()) throw SW_EXCEPTION_TEXT(SWMemoryManagerException, "Shared memory not attached");

		if (pData == NULL)
			{
			// Simply return as we have nopthing to do!
			return;
			}

		ShmMemoryHeader	*pHdr=NULL;
		
		// for now just move from the in-use list to the free list
		pHdr = (ShmMemoryHeader *)((char *)pData - sizeof(ShmMemoryHeader));

		if (pHdr->magic != SHM_BLOCK_MAGIC)
			{
			throw SW_EXCEPTION_TEXT(SWMemoryManagerException, "Attempt to free memory not allocated by this allocator");
			}

		// From this point onwards we must be locked.
		SWMutexGuard			guard(_pMutex);

#ifdef DEBUG_SHM_ALLOCATOR
		// Check it's own our in-use list
		for (hp=m_inuseList.head;hp&&hp!=pHdr;)
			{
			hp = hp->next;
			}

		if (hp == NULL)
			{
			throw SW_EXCEPTION_TEXT(SWMemoryManagerException, "Memory block not found in in-use list.");
			}
#endif

		if (pHdr->refcount == 0)
			{
			throw SW_EXCEPTION_TEXT(SWMemoryManagerException, "Memory block already free.");
			}

		if (--(pHdr->refcount) == 0)
			{
			removeFromList(getInUseList(), pHdr);

			//m_usedBytes -= pHdr->actualSize;
			//m_availableBytes += pHdr->actualSize;

			// Insert the memory back into the free list
			insertIntoFreeList(pHdr);
			}
		}


