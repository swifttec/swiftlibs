/**
*** @file		SWLocalTime.cpp
*** @brief		A class to represent a local time.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWLocalTime class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <math.h>
#if !defined(WIN32) && !defined(_WIN32)
#include <sys/time.h>
#endif
#include <swift/SWLocalTime.h>
#include <swift/SWDate.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





// Construct with today's date
SWLocalTime::SWLocalTime() :
	SWAdjustedTime(SWTimeZone::getCurrentTimeZone())
		{
	    }

// SWTime constructor
SWLocalTime::SWLocalTime(const SWTime &other) :
	SWAdjustedTime(other, SWTimeZone::getCurrentTimeZone())
		{
		}


// Copy constructor
SWLocalTime::SWLocalTime(const SWLocalTime &other) :
	SWAdjustedTime(other)
		{
		}


// Assignment operator
SWLocalTime &
SWLocalTime::operator=(const SWLocalTime &other)
		{
		SWAdjustedTime::operator=(other);

		return *this;
		}

// Assignment operator
SWLocalTime &
SWLocalTime::operator=(const SWTime &other)
		{
		SWTime::operator=(other);

		return *this;
		}

// Assignmemt from a unix timespec
SWLocalTime &
SWLocalTime::operator=(const sw_timespec_t &v)
		{
		SWAdjustedTime::operator=(v);
		return *this;
		}


// Assignmemt from a unix timeval
SWLocalTime &
SWLocalTime::operator=(const sw_timeval_t &v)
		{
		SWAdjustedTime::operator=(v);
		return *this;
		}


// Assignment from a unix time
SWLocalTime &
SWLocalTime::operator=(time_t v)
		{
		SWAdjustedTime::operator=(v);
		return *this;
		}


// Assignment from a double (SWTime format only)
SWLocalTime &
SWLocalTime::operator=(double v)
		{
		SWAdjustedTime::operator=(v);
		return *this;
		}


// Assignment from a windows FILETIME
SWLocalTime &
SWLocalTime::operator=(const FILETIME &v)
		{
		SWAdjustedTime::operator=(v);
		return *this;
		}


// Assignment from a windows SYSTEMTIME
SWLocalTime &
SWLocalTime::operator=(const SYSTEMTIME &v)
		{
		SWAdjustedTime::operator=(v);
		return *this;
		}


// Assignment from a SWDate
SWLocalTime &
SWLocalTime::operator=(const SWDate &v)
		{
		SWAdjustedTime::operator=(v);
		return *this;
		}


// Assignment from a struct tm (unix)
SWLocalTime &
SWLocalTime::operator=(const sw_tm_t &v)
		{
		SWAdjustedTime::operator=(v);
		return *this;
		}




