/**
*** @file		SWLogSyslogHandler.h
*** @brief		A class to write log messages to the system logging facility.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWLog class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#ifndef __SWLogSyslogHandler_h__
#define __SWLogSyslogHandler_h__

#include <swift/SWLogMessage.h>
#include <swift/SWLogMessageHandler.h>






/**
*** @brief		A class to write log messages to the system logging facility.
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Unsafe</td>
*** </tr>
*** </table>
***
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWLogSyslogHandler : public SWLogMessageHandler
		{
public:
		/// Default constructor
		SWLogSyslogHandler();

		/// Virtual destructor
		virtual ~SWLogSyslogHandler();

protected: // Virtual method overrides
		virtual sw_status_t	open();
		virtual sw_status_t	process(const SWLogMessage &lm);
		virtual sw_status_t	flush();
		virtual sw_status_t	close();

private:
		SWString	m_appname;	// The last appname seen
#if defined(SW_PLATFORM_WINDOWS)
		HANDLE		m_eventHandle;	// A handle to the event handle for reporting events to the system
#endif
		};





#endif
