/****************************************************************************
**	SWMsgField.cpp	A class for holding a single field of a SWMsg message
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWMsgField.h>
#include <swift/SWMemoryInputStream.h>
#include <swift/SWMemoryOutputStream.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




SWMsgField::SWMsgField(sw_uint32_t id, const SWValue &v) :
	SWValue(v),
	m_id(id)
		{
		}


SWMsgField::SWMsgField(sw_uint32_t id, bool v) :
	SWValue(v),
	m_id(id)
		{
		}


SWMsgField::SWMsgField(sw_uint32_t id, sw_int8_t v) :
	SWValue(v),
	m_id(id)
		{
		}


SWMsgField::SWMsgField(sw_uint32_t id, sw_int16_t v) :
	SWValue(v),
	m_id(id)
		{
		}


SWMsgField::SWMsgField(sw_uint32_t id, sw_int32_t v) :
	SWValue(v),
	m_id(id)
		{
		}


SWMsgField::SWMsgField(sw_uint32_t id, sw_int64_t v) :
	SWValue(v),
	m_id(id)
		{
		}


SWMsgField::SWMsgField(sw_uint32_t id, sw_uint8_t v) :
	SWValue(v),
	m_id(id)
		{
		}


SWMsgField::SWMsgField(sw_uint32_t id, sw_uint16_t v) :
	SWValue(v),
	m_id(id)
		{
		}


SWMsgField::SWMsgField(sw_uint32_t id, sw_uint32_t v) :
	SWValue(v),
	m_id(id)
		{
		}


SWMsgField::SWMsgField(sw_uint32_t id, sw_uint64_t v) :
	SWValue(v),
	m_id(id)
		{
		}


SWMsgField::SWMsgField(sw_uint32_t id, float v) :
	SWValue(v),
	m_id(id)
		{
		}


SWMsgField::SWMsgField(sw_uint32_t id, double v) :
	SWValue(v),
	m_id(id)
		{
		}


SWMsgField::SWMsgField(sw_uint32_t id, const char *v) :
	SWValue(v),
	m_id(id)
		{
		}


SWMsgField::SWMsgField(sw_uint32_t id, const wchar_t *v) :
	SWValue(v),
	m_id(id)
		{
		}


SWMsgField::SWMsgField(sw_uint32_t id, const void *pData, size_t len) :
	SWValue(pData, len, true),
	m_id(id)
		{
		}


SWMsgField::SWMsgField(sw_uint32_t id, const SWBuffer &v) :
	SWValue(v),
	m_id(id)
		{
		}


SWMsgField::SWMsgField(sw_uint32_t id, const SWString &v) :
	SWValue(v),
	m_id(id)
		{
		}


SWMsgField::SWMsgField(sw_uint32_t id, const SWDate &v) :
	SWValue(v),
	m_id(id)
		{
		}


SWMsgField::SWMsgField(sw_uint32_t id, const SWTime &v) :
	SWValue(v),
	m_id(id)
		{
		}


SWMsgField::SWMsgField(sw_uint32_t id, const SWMoney &v) :
	SWValue(v),
	m_id(id)
		{
		}



#if defined(SW_BUILD_MFC)
SWMsgField::SWMsgField(sw_uint32_t id, const COleVariant &v) :
	SWValue(v),
	m_id(id)
		{
		}


SWMsgField::SWMsgField(sw_uint32_t id, const CString &v) :
	SWValue(v),
	m_id(id)
		{
		}
#endif


SWMsgField::SWMsgField(const SWMsgField &other) :
	SWValue(other)
		{
		*this = other;
		}


SWMsgField::SWMsgField() :
	m_id(0)
		{
		}


SWMsgField::~SWMsgField()
		{
		}

/**
*** Work out and return the length of the field when written to memory
***
*** The data is written to a stream (and so the length is worked out the same way)
*** as follows:
***	Type info		1 byte
***	Field ID		4 bytes
***	Data Length		4 bytes (variable length types only)
***	Data			1-n bytes
***
**/
sw_size_t
SWMsgField::fieldLengthSize() const			
		{
		ValueType	ft = type();
		sw_size_t	lensize=0;

		// Work out the correct lensize for STRING, WSTRING and DATA
		if (ft == VtCString || ft == VtWString || ft == VtData)
			{
			lensize = 4;
			}

		return lensize;
		}



sw_size_t
SWMsgField::fieldLength() const			
		{
		sw_size_t		lensize=fieldLengthSize(), datalen=length();

		return 5 + datalen + lensize;
		}



// Set the ID and flag
void
SWMsgField::id(sw_uint32_t id)
		{
		if (m_id != id)
			{
			m_id = id;
			setChangedFlag(true);
			}
		}


/**
*** Read the contents of the memory supplied and initialise this field
*** from it.
***
*** @return	The length of the data.
**/
sw_status_t
SWMsgField::readFromBuffer(char *mem)
		{
		SWMemoryInputStream	mis(mem);

		return readFromStream(mis);
		}


/**
*** Initialise this field from the given memory input stream
***
*** @return	The length of the data.
**/
sw_status_t
SWMsgField::readFromStream(SWInputStream &is)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		sw_uint32_t	id;

		// Clear the field
		clearValue(true);

		// Read the field id
		res = is.read(id);
		if (res == SW_STATUS_SUCCESS)
			{
			SWValue::readFromStream(is);
			m_id = id;
			}

		return res;
		}


/**
*** Write this field into the given buffer.
***
*** @return	The length of the data written
**/
sw_status_t
SWMsgField::writeToBuffer(SWBuffer &buf, sw_size_t offset) const
		{
		SWMemoryOutputStream	os(buf, offset);

		return writeToStream(os);
		}


/**
*** Write this field into the given output stream.
**/
sw_status_t
SWMsgField::writeToStream(SWOutputStream &os) const
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		res = os.write(m_id);
		if (res == SW_STATUS_SUCCESS)
			{
			res = SWValue::writeToStream(os);
			}

		return res;
		}




/**
*** Assignment operator
**/
SWMsgField &
SWMsgField::operator=(const SWMsgField &other)
		{
		SWValue::operator=(other);

		m_id = other.m_id;

		return *this;
		}



/**
*** clear the ID and value of this field
**/
void
SWMsgField::clear()	
		{
		id(0);
		SWValue::clearValue(true);
		}


/**
*** Protected member (virtual) to clear the value of this field
**/
bool
SWMsgField::clearValue(bool updateChanged)	
		{
		return SWValue::clearValue(updateChanged);
		}


/**
*** Dumps the field to stdout for debug purposes
**/
void
SWMsgField::dump()
		{
		printf("******************** Field ID = %d ********************\n", m_id);

		SWValue::dump();
		}



