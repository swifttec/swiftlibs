/**
*** @file		SWThreadEvent.h
*** @brief		A thread event class.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWThreadEvent class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWThreadEvent_h__
#define __SWThreadEvent_h__



#include <swift/SWEvent.h>
#include <swift/SWThreadMutex.h>
#include <swift/SWThreadCondVar.h>




/**
*** @brief A thread-level auto-event synchronisation object.
***
*** A thread-level auto-event synchronisation object.
*** SWThreadEvent objects are based around the Windows concept of events for synchonisation.
*** They will be emulated on platforms where native support is not available.
***
***  events are events that will
*** 
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Safe</td>
*** </tr>
*** </table>
***
***
*** @author		Simon Sparkes
*** @see		SWManualThreadEvent, SWThreadEvent
**/
class SWIFT_DLL_EXPORT SWThreadEvent : public SWEvent
		{
public:
		/// Construct a auto-reset event with it's initial state
		SWThreadEvent(EventType type, bool initialstate=false);

		virtual ~SWThreadEvent();

		/// Wait for the event to become signalled. The event will auto-reset to non-signalled.
		virtual sw_status_t		wait(sw_uint64_t waitms=SW_TIMEOUT_INFINITE);

		/// Reset the event if no thread is waiting, set to signaled state if thread(s) are waiting, wake up one waiting thread and reset event.
		virtual void			set();

		/// Wake up one thread (if present) and reset the event.
		virtual void			pulse();

		/// Reset the event to a non-signalled state
		virtual void			reset();

		/// Close the event
		virtual void			close();

private:
#if defined(WIN32) || defined(_WIN32)
		// SWThreadMutex	m_mutex;		///< A mutex for thread-syncronisation
		sw_mutex_t		m_hMutex;		///< A mutex for thread-syncronisation
		HANDLE			m_hEvent;		///< A handle to the actual event
#else
		sw_condvar_t	m_hCondVar;		///< A condition variable used to simulate an event
		bool			m_signalled;	///< A flag indicating the signalled state of this event
#endif
		int				m_waiting;		///< The number of waiting threads
		bool			m_closed;		///< A flag to indicate that this event has been closed
		};





#endif
