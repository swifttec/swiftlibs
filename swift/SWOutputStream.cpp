/**
*** @file		SWOutputStream.cpp
*** @brief		A generic output stream.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWOutputStream class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




#include "stdafx.h"

#include <swift/SWOutputStream.h>
#include <swift/SWString.h>


#if defined(SW_PLATFORM_WINDOWS)
	#include <float.h>
	#include <math.h>
	#define isnand(x)	_isnan(x)
	#define finite(x)	_finite(x)
	#define	IsNegNAN(d)	(d < 0.0)
#else
	#include <unistd.h>

	#if defined(SW_PLATFORM_SOLARIS)
		#include <ieeefp.h>
		#include <nan.h>
	#endif // linux
#endif // WIN32



// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif



sw_int64_t
SWOutputStream::doubleToLongBits(double d)
		{
		sw_int64_t	bits=0;

#ifdef SW_PLATFORM_NATIVE_DOUBLE_IN_IEEE_FORMAT
		bits = *((sw_int64_t *)&d);
#else
		sw_int64_t	m;
		double	md;
		int	e;

		// If the argument is positive infinity, the result is 0x7ff0000000000000L.
		// If the argument is negative infinity, the result is 0xfff0000000000000L.
		// If the argument is NaN, the result is 0x7ff8000000000000L.

		if (isnand(d)) bits = 0x7ff8000000000000L;
		else if (!finite(d))
			{
			if (d < 0.0) bits = 0xfff0000000000000L;
			else bits = 0x7ff0000000000000L;
			}
		else if (d == 0.0) 
			{
			if (IsNegNAN(d)) bits = 0x8000000000000000L;
			else bits = 0;
			}
		else
			{
			if (d < 0.0)
				{
				bits |= ((sw_int64_t)1<<63);
				d = -d;
				}

			md = frexp(d, &e);
			m = (sw_int64_t)md;

			while ((m & 0x10000000000000) == 0 && md != 0.0)
				{
				md *= 2.0;
				m = (sw_int64_t)md;
				e--;

				// printf("%f -> exp=%d  mantissa=%f (%016llx)\n", d, e, md, m);
				}

			bits |= ((sw_int64_t)(e+1075)) << 52;
			bits |= m & 0xfffffffffffffL;
			}

		// printf("-> %016llx\n", bits);
#endif

		return bits;
		}


sw_int32_t
SWOutputStream::floatToIntBits(float d)
		{
		sw_int32_t	bits=0;

#ifdef SW_PLATFORM_NATIVE_FLOAT_IN_IEEE_FORMAT
		bits = *((int *)&d);
#else
		int	m;
		double	md;
		int	e;

		// If the argument is positive infinity, the result is 0x7f800000.
		// If the argument is negative infinity, the result is 0xff800000.
		// If the argument is NaN, the result is 0x7fc00000.

		if (isnand(d)) bits = 0x7fc00000;
		else if (!finite(d))
			{
			if (d < 0.0) bits = 0xff800000;
			else bits = 0x7f800000;
			}
		else if (d == 0.0) 
			{
			if (IsNegNAN(d)) bits = 0x80000000L;
			else bits = 0;
			}
		else
			{
			if (d < 0.0)
				{
				bits |= ((int)1<<31);
				d = -d;
				}

			md = frexp(d, &e);
			m = (int)md;

			while ((m & 0x800000) == 0 && md != 0.0)
				{
				md *= 2.0;
				m = (int)md;
				e--;

				// printf("%f -> exp=%d  mantissa=%f (%08x)\n", d, e, md, m);
				}


			bits |= ((int)(e+150) & 0xff) << 23;
			bits |= m & 0x7fffff;
			}

		// printf("-> %08x\n", bits);
#endif

		return bits;
		}



sw_status_t
SWOutputStream::writeSignedInteger(sw_int64_t v, int nbytes)
		{
		sw_byte_t	buf[32], *bp=buf;
		int			count=nbytes;

		while (count-- > 0)
			{
			*bp++ = (sw_byte_t)(v & 0xff);
			v >>= 8;
			}

		return writeData(buf, nbytes);
		}


sw_status_t
SWOutputStream::writeUnsignedInteger(sw_uint64_t v, int nbytes)
		{
		sw_byte_t	buf[32], *bp=buf;
		int			count=nbytes;

		while (count-- > 0)
			{
			*bp++ = (sw_byte_t)(v & 0xff);
			v >>= 8;
			}

		return writeData(buf, nbytes);
		}


sw_status_t
SWOutputStream::writeDouble(double v)
		{
		// Convert the double to a portable binary format and transmit as a 64-bit int.
		return writeSignedInteger(doubleToLongBits(v), sizeof(sw_int64_t));
		}


sw_status_t
SWOutputStream::writeFloat(float v)
		{
		// Convert the float to a portable binary format and transmit as a 32-bit int.
		return writeSignedInteger(floatToIntBits(v), sizeof(sw_int32_t));
		}


/*
sw_status_t
SWOutputStream::writeString(const SWString &v)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		// We write a string as follows:
		// CharSize (1 byte)
		// String Length (4 bytes)
		// String Length (4 bytes)
		res = writeSignedInteger(v.getCharSize(), 1);
		if (res == SW_STATUS_SUCCESS) res = writeSignedInteger(v.getDataLength(), 4);
		if (res == SW_STATUS_SUCCESS) res = writeData(v, len * sizeof(*v));

		return res;
		}
*/

sw_status_t
SWOutputStream::writeBoolean(bool v)
		{
		// Default is to write this as a single byte value
		return writeUnsignedInteger(v?1:0, 1);
		}


sw_status_t
SWOutputStream::writeBinaryData(const void *pData, sw_size_t nbytes)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		sw_uint32_t	len=(sw_uint32_t)nbytes;

		res = writeUnsignedInteger(len, sizeof(len));
		if (res == SW_STATUS_SUCCESS) res = writeData(pData, len);

		return res;
		}


sw_status_t
SWOutputStream::writeRawData(const void *pData, sw_size_t nbytes)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		sw_uint32_t	len=(sw_uint32_t)nbytes;

		res = writeData(pData, len);

		return res;
		}


sw_status_t
SWOutputStream::writeData(const void * /*pData*/, sw_size_t /*len*/)
		{
		// The default behaviour is to return an error as we cannot perform this operation.
		return SW_STATUS_UNSUPPORTED_OPERATION;
		}



sw_status_t
SWOutputStream::flushData()
		{
		// The default behaviour is to return success - i.e. a NOP.
		return SW_STATUS_SUCCESS;
		}


sw_status_t
SWOutputStream::rewindStream()
		{
		// The default behaviour is to return an error as we cannot perform this operation.
		return SW_STATUS_UNSUPPORTED_OPERATION;
		}





