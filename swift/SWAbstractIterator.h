/**
*** @file		SWAbstractIterator.h
*** @brief		Iterator class for walking collections
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWAbstractIterator class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWAbstractIterator_h__
#define __SWAbstractIterator_h__

#include <swift/swift.h>
#include <swift/SWObject.h>



// Forward declarations
class SWCollection;
class SWCollectionIterator;


/**
*** @brief	A collection iterator object.
***
*** SWAbstractIterator is the class returned by the iterator function of a SWCollection object
*** and is used to "walk" the collection it is obtained from.
*** An iterator can interact with any collection dervied from the abstract SWCollection class.
***
*** Not all operations are supported on all collection types. When an operation is not supported
*** an exception will be thrown.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWAbstractIterator : public SWObject
		{
		friend class SWCollection;
		friend class SWCollectionIterator;

public:
		/// An enumeration of possible add locations for use with the add method.
		enum AddLocation
			{
			AddBeforeOrAfter,	///< Add the data before or after the current element.
			AddAfter,			///< Add the data after the current element.
			AddBefore			///< Add the data before the current element.
			};

protected:
		/// Default constructor
		SWAbstractIterator();

		/// Copy constructor
		SWAbstractIterator(const SWAbstractIterator &other);

		/// Assignment operator
		SWAbstractIterator &       operator=(const SWAbstractIterator &);

public:
		/// Destructor
		virtual ~SWAbstractIterator();

		/**
		*** @brief	Check to see if there is a valid current element.
		***
		*** This method checks to see if the current() method can be called on the iterator.
		*** This will not cause an exception if no more data is available.
		***
		*** @return		true if current() can be called without causing an exception,
		***				false otherwise.
		**/
		bool		hasCurrent();


		/**
		*** @brief	Check to see if there is another element to retrieve by calling next.
		***
		*** This method checks to see if the next() method can be called on the iterator.
		*** This will not cause an exception if no more data is available.
		***
		*** @return		true if there is more data available and the next() method can
		***				be called, false otherwise.
		**/
		bool		hasNext();


		/**
		*** @brief	Check to see if there is another element to retrieve by calling previous.
		***
		*** This method checks to see if the previous() method can be called on the iterator.
		*** This will not cause an exception if no more data is available.
		***
		*** @return		true if there is more data available and the previous() method can
		***				be called, false otherwise.
		**/
		bool		hasPrevious();


		/**
		*** Returns a pointer to the collection currently being iterated.
		**/
		SWCollection *	collection() const;

		/**
		*** Clears the iterator
		**/
		void		clear()						{ setIterator(NULL); }

protected:
		/// Set the actual collection iterator to associate with this iterator
		void	setIterator(SWCollectionIterator *pCollectionIterator);

protected:
		SWCollectionIterator	*m_pIterator;	///< A link to the underlying (real) iterator
		};




#endif
