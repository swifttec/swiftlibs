/**
*** @file		SWSocketInputStream.h
*** @brief		A generic socket implementation
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWSocketInputStream class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWSocketInputStream_h__
#define __SWSocketInputStream_h__

#include <swift/swift.h>
#include <swift/SWSocket.h>

#include <swift/SWInputStream.h>




/**
*** @brief	A socket input stream
***
*** This class takes data read from a socket and uses it as the input to
*** the input stream.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWSocketInputStream : public SWInputStream
		{
public:
		/// Default constructor
		SWSocketInputStream();

		/// Constructor with specific socket implementation
		SWSocketInputStream(SWSocket *pSocket, bool delflag);

		/// Virtual destructor
		virtual ~SWSocketInputStream();

		/// Returns a pointer to the socket implementation
		SWSocket *			getSocket()	{ return _pSocket; }

		/// Set the socket implementation
		void				setSocket(SWSocket *pSocket, bool delflag);

protected:
		virtual sw_status_t		readData(void *pData, sw_size_t len);	///< Write binary data to the output stream.
		
protected:
		SWSocket	*_pSocket;	///< A pointer to the actual socket implementation
		bool		_delFlag;	///< A flag to indicate if the socket implementator should be delete on destruction
		};




#endif
