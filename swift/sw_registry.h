/**
*** A collection of internal functions
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __sw_registry_h__
#define __sw_registry_h__

#include <swift/swift.h>
#include <swift/SWString.h>
#include <swift/SWRegistryKey.h>
#include <swift/SWRegistryValue.h>


SWIFT_DLL_EXPORT SWString		sw_registry_getString(SWRegistryHive pdk, const SWString &key, const SWString &param, const SWString &defval="");
SWIFT_DLL_EXPORT SWValue		sw_registry_getValue(SWRegistryHive pdk, const SWString &key, const SWString &param, const SWValue &defval="");
SWIFT_DLL_EXPORT int			sw_registry_getInteger(SWRegistryHive pdk, const SWString &key, const SWString &param, int defval=0);
SWIFT_DLL_EXPORT bool			sw_registry_getBoolean(SWRegistryHive pdk, const SWString &key, const SWString &param, bool defval=false);
SWIFT_DLL_EXPORT double			sw_registry_getDouble(SWRegistryHive pdk, const SWString &key, const SWString &param, double defval=0.0);
SWIFT_DLL_EXPORT sw_status_t	sw_registry_setValue(SWRegistryHive pdk, const SWString &key, const SWString &param, const SWValue &val);
SWIFT_DLL_EXPORT sw_status_t	sw_registry_setString(SWRegistryHive pdk, const SWString &key, const SWString &param, const SWString &val);
SWIFT_DLL_EXPORT sw_status_t	sw_registry_setInteger(SWRegistryHive pdk, const SWString &key, const SWString &param, int val);
SWIFT_DLL_EXPORT sw_status_t	sw_registry_setBoolean(SWRegistryHive pdk, const SWString &key, const SWString &param, bool val);
SWIFT_DLL_EXPORT sw_status_t	sw_registry_setDouble(SWRegistryHive pdk, const SWString &key, const SWString &param, double val);
SWIFT_DLL_EXPORT bool			sw_registry_isKey(SWRegistryHive pdk, const SWString &key);
SWIFT_DLL_EXPORT bool			sw_registry_isValue(SWRegistryHive pdk, const SWString &key, const SWString &param);
SWIFT_DLL_EXPORT sw_status_t	sw_registry_createKey(SWRegistryHive pdk, const SWString &key);
SWIFT_DLL_EXPORT sw_status_t	sw_registry_removeKey(SWRegistryHive pdk, const SWString &key);
SWIFT_DLL_EXPORT sw_status_t	sw_registry_removeValue(SWRegistryHive pdk, const SWString &key, const SWString &param);
SWIFT_DLL_EXPORT sw_status_t	sw_registry_renameKey(SWRegistryHive pdk, const SWString &oldkey, const SWString &newkey);
SWIFT_DLL_EXPORT sw_status_t	sw_registry_renameValue(SWRegistryHive pdk, const SWString &key, const SWString &oldparam, const SWString &newparam);


#if defined(SW_PLATFORM_WINDOWS)
SWIFT_DLL_EXPORT SWString		sw_registry_getString(HKEY hk, const SWString &key, const SWString &param, const SWString &defval="");
SWIFT_DLL_EXPORT int			sw_registry_getInteger(HKEY hk, const SWString &key, const SWString &param, int defval=0);
SWIFT_DLL_EXPORT double			sw_registry_getDouble(HKEY hk, const SWString &key, const SWString &param, double defval=0.0);
SWIFT_DLL_EXPORT sw_status_t	sw_registry_setString(HKEY hk, const SWString &key, const SWString &param, const SWString &val);
SWIFT_DLL_EXPORT sw_status_t	sw_registry_setInteger(HKEY hk, const SWString &key, const SWString &param, int val);
SWIFT_DLL_EXPORT sw_status_t	sw_registry_setDouble(HKEY hk, const SWString &key, const SWString &param, double val);
SWIFT_DLL_EXPORT bool			sw_registry_isKey(HKEY hk, const SWString &key);
SWIFT_DLL_EXPORT bool			sw_registry_isValue(HKEY hk, const SWString &key, const SWString &param);
SWIFT_DLL_EXPORT sw_status_t	sw_registry_createKey(HKEY hk, const SWString &key);
SWIFT_DLL_EXPORT sw_status_t	sw_registry_removeKey(HKEY hk, const SWString &key);
SWIFT_DLL_EXPORT sw_status_t	sw_registry_removeValue(HKEY hk, const SWString &key, const SWString &param);
SWIFT_DLL_EXPORT sw_status_t	sw_registry_renameKey(HKEY hk, const SWString &oldkey, const SWString &newkey);
SWIFT_DLL_EXPORT sw_status_t	sw_registry_renameValue(HKEY hk, const SWString &key, const SWString &oldparam, const SWString &newparam);
#endif

#endif // __sw_registry_h__
