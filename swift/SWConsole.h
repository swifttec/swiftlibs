/**
*** @file		SWConsole.h
*** @brief		A class for controlling console (terminal) I/O.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWConsole class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWConsole_h__
#define __SWConsole_h__

#include <swift/SWObject.h>
#include <swift/SWIntegerArray.h>
#include <swift/SWSize.h>
#include <swift/SWPoint.h>

// Forward declarations

/**
*** @brief	A class for controlling console/terminal I/O
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWConsole : public SWObject
		{
public:
		static SWString	getString();
		static SWString	getStringNoEcho();

#ifdef NOT_YET_IMPLEMENTED
public:
		/// A enumeration of possible colour values.
		enum ConsoleColor
			{
			Black=0,
			Red=1,
			Green=2,
			Yellow=3,
			Blue=4,
			Magenta=5,
			Cyan=6,
			White=7,
			Grey=8,
			BrightRed=9,
			BrightGreen=10,
			BrightYellow=11,
			BrightBlue=12,
			BrightMagenta=13,
			BrightCyan=14,
			BrightWhite=15
			};

public:
		/// Constructor
		SWConsole();

		/// Destructor
		virtual ~SWConsole();

		/**
		*** @brief	Clear the screen/window
		***
		*** @param	home	If true the cursor position is set to the home position
		***					(top-left corner). If false the cursor remains at the 
		***					current position.
		**/
		void	clear(bool home=true);

		/**
		*** @brief		Set the bold attribute on the output
		*** @param	v	If true output is in bold type, otherwise output is in non-bold type.
		**/
		void	bold(bool v);

		/**
		*** @brief		Set the concealed attribute on the output
		*** @param	v	If true output is in concealed type, otherwise output is in non-concealed type.
		**/
		void	concealed(bool v);

		/**
		*** @brief		Set the underline attribute on the output
		*** @param	v	If true output is in underlined type, otherwise output is in non-underlined type.
		**/
		void	underline(bool v);

		/**
		*** @brief			Sets the foreground colour of the text to the specified colour
		*** @param	color	The color value to use.
		**/
		void	setForegroundColor(int color);

		/**
		*** @brief			Sets the background colour of the text to the specified colour
		*** @param	color	The color value to use.
		**/
		void	setBackgroundColor(int color);

		/**
		*** @brief	Reverses the foreground and backgroun colours.
		**/
		void	reverseColors();

		/**
		*** @brief	Saves the current state, so that it can be restored by calling restoreState.
		**/
		void	saveState();

		/**
		*** @brief	Restores a previously stored state.
		**/
		void	restoreState();

		/**
		*** @brief	Returns the width of the window in characters (rows).
		**/
		int		width()		{ return size().width(); }

		/**
		*** @brief	Returns the height of the window in characters (lines).
		**/
		int		height()	{ return size().height(); }

		/**
		*** @brief	Returns the dimensions of the windows (in characters) as a SWSize object.
		**/
		SWSize	size();

		/**
		*** @brief	Returns the current cursor location (in characters) as a SWPoint object.
		**/
		SWPoint	getCursorPos();

		/**
		*** @brief	Sets the current cursor location (in characters).
		**/
		void	setCursorPos(SWPoint p);

		/**
		*** @brief	Sets the current cursor location (in characters).
		**/
		void	setCursorPos(int x, int y);

		/**
		*** @brief	Gets a password from the console without displaying it
		**/
		void	getPassword(SWString &pass);

private:
#if defined(SW_PLATFORM_WINDOWS)
		SWIntegerArray	m_stateArray;	///< Saved states
		WORD			m_savedAttr;	///< The handle we saved at startup
		WORD			m_currentAttr;	///< The current text attribute
		HANDLE			m_handle;		///< Handle to the console
#endif
#endif // NOT_YET_IMPLEMENTED
		};

#endif
