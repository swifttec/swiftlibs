/**
*** @file		SWEvent.h
*** @brief		An event class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWEvent class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWEvent_h__
#define __SWEvent_h__

#include <swift/SWException.h>





/**
*** @brief	An abstract portable event class.
***
*** SWEvent is an abstract event class that forms the base of all event classes.
*** SWEvent objects are based around the Windows concept of events for synchonisation.
*** They will be emulated on platforms where native support is not available.
*** 
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Safe</td>
*** </tr>
*** </table>
***
***
*** @see		SWManualEvent, SWAutoEvent
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWEvent
		{
public:
		/// Defines the type of event object
		enum EventType
			{
			AutoReset,
			ManualReset
			};
protected:
		/// Protected constructor - to allow construction only by derived classes
		SWEvent(EventType type);

public:
		virtual ~SWEvent();

		/// Wait for the event
		virtual sw_status_t		wait(sw_uint64_t waitms=SW_TIMEOUT_INFINITE)=0;

		/// Signal the event
		virtual void			set()=0;

		/// Pulse the event
		virtual void			pulse()=0;

		/// Reset the event
		virtual void			reset()=0;

		/// Close the event
		virtual void			close()=0;

		/// Returns the type of this event object (auto or manual reset).
		EventType				type() const	{ return m_type; }

protected:
		EventType	m_type;		///< The event type
		};


/// General event exception
SW_DEFINE_EXCEPTION(SWEventException, SWException);




#endif
