/****************************************************************************
**	sw_file.cpp		Various file utilities
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"


#if defined(SW_PLATFORM_WINDOWS)
#include <fcntl.h>
#include <io.h>
#include <direct.h>
#endif

#include <swift/sw_misc.h>
#include <swift/sw_file.h>
#include <swift/SWString.h>
#include <swift/SWArray.h>
#include <swift/SWTokenizer.h>
#include <swift/SWTempFile.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif

#if defined(SW_PLATFORM_WINDOWS)
	#include <Accctrl.h>
	#include <Aclapi.h>
	#include <fcntl.h>
	#include <io.h>
	#include <direct.h>
	#include <Shlobj.h>
	
	#pragma warning(disable: 4996)
#else
	#include <sys/stat.h>
	#include <sys/statvfs.h>
#endif




/**
*** Platform independent version of mkdir system call to create a directory.
***
*** @param path		The pathname of the directory to be created.
*** @param mode		The file mode to be used (default=0777). Mode is ignored by windows
*** @param createParent	This flag indicates if parent directories should be created to
***			accomodate the directory creation.
**/
SWIFT_DLL_EXPORT sw_status_t
sw_file_mkdir(const SWString &path, bool createParent, SWFileAttributes *pfa)
		{
		return SWFolder::create(path, createParent, pfa);
		}


/*************************************************************
** Various Local File Tests
*************************************************************/

SWIFT_DLL_EXPORT bool
sw_file_isRelativePath(const SWString &path)
		{
		return SWFilename::isRelativePath(path);
		}


SWIFT_DLL_EXPORT bool
sw_file_isAbsolutePath(const SWString &path)
		{
		return SWFilename::isAbsolutePath(path);
		}


/**
*** Test to see if the given filename is a file
**/
SWIFT_DLL_EXPORT bool
sw_file_isFile(const SWString &filename)
		{
		return SWFileInfo::isFile(filename);
		}


/**
*** Test to see if the given filename is a directory
**/
SWIFT_DLL_EXPORT bool
sw_file_isDirectory(const SWString &filename)
		{
		return SWFileInfo::isDirectory(filename);
		}



/**
*** Test to see if the given filename is a symbolic link
**/
SWIFT_DLL_EXPORT bool
sw_file_isSymlink(const SWString &filename)
		{
		return SWFileInfo::isSymbolicLink(filename);
		}


/**
*** Test to see if the given file is readable
***
*** @return true if readable, false otherwise
**/
SWIFT_DLL_EXPORT bool
sw_file_isReadable(const SWString &filename)
		{
		return SWFileInfo::isReadable(filename);
		}


/**
*** Test to see if the given file is writeable
***
*** @return true if writeable, false otherwise
**/
SWIFT_DLL_EXPORT bool
sw_file_isWriteable(const SWString &filename)
		{
		return SWFileInfo::isWriteable(filename);
		}


/**
*** Test to see if the given file is executable
***
*** @return true if executable, false otherwise
**/
SWIFT_DLL_EXPORT bool
sw_file_isExecutable(const SWString &filename)
		{
		return SWFileInfo::isExecutable(filename);
		}



/**
*** Returns the size in bytes of the file specified.
***
*** @return the size in bytes of the file specified or 0 if the file does not exist
**/
SWIFT_DLL_EXPORT sw_filesize_t
sw_file_size(const SWString &filename)
		{
		return SWFileInfo::size(filename);
		}


/**
*** Returns the last modification time of the file specified.
**/
SWIFT_DLL_EXPORT SWTime
sw_file_time(const SWString &filename)
		{
		SWTime	t;

		SWFileInfo::getFileTimes(filename, &t, NULL, NULL);

		return t;
		}



/*************************************************************
** Filename Maniuplation
*************************************************************/

SWIFT_DLL_EXPORT SWString
sw_file_basename(const SWString &filename)
		{
		return SWFilename::basename(filename);
		}


SWIFT_DLL_EXPORT SWString
sw_file_dirname(const SWString &filename)
		{
		return SWFilename::dirname(filename);
	    }


#if defined(NEVER)
// Returns the server component of a path
// In windows the notation is \\server\path
// in unix the notation is /net/server/path
SWIFT_DLL_EXPORT SWString
sw_file_servername(const SWString &filename)
		{
		SWString	v;
		SWString	path=filename;

		if (path.GetLength() > 2 && path[0] == _T('\\') && path[1] == _T('\\'))
			{
			int	p;

			v = path;
			v.Delete(0, 2);

			if ((p = v.Find(_T('\\'))) >= 0) v.Delete(p, v.GetLength()-p);
			}
		else if (path.length() > 5 && path.left(5) == _T("/net/")) // Unix path using /net
			{
			int	p;

			v = path;
			v.remove(0, 5);

			// Now expect server/path
			if ((p = v.first(_T('/'))) >= 0) v.remove(p);
			}
		
		return v;
		}



/**
*** This function returns the drive component of a path.
*** This only makes sense in windows/dos, but left for compatibiliy reasons
*** and will return an empty string under Unix.
**/
SWIFT_DLL_EXPORT SWString
sw_file_drivename(const SWString &filename)
		{
		SWString	v;
		SWString	path=filename;

		// Case 1: \\server\share\...
		if (path.GetLength() > 2 && path[0] == _T('\\') && path[1] == _T('\\'))
			{
			SWString	tmp;

			tmp = sw_file_sharename(path);
			if (tmp.GetLength() == 2 && tmp[1] == _T('$')) v = tmp[0];
			}
		// Case 2: X:...
		else if (path.GetLength() > 1 && path[1] == _T(':'))
			{
			v = path[0];
			}

		return v;
		}


/**
*** This function returns the share component of a path
*** This only makes sense in windows/dos, but left for compatibiliy reasons
*** and returns a blank in a non-dos environment.
**/
SWIFT_DLL_EXPORT SWString
sw_file_sharename(const SWString &filename)
		{
		SWString	v;
		SWString	path=filename;

		// Case 1: \\server\share\...
		if (path.GetLength() > 2 && path[0] == _T('\\') && path[1] == _T('\\'))
			{
			SWString	tmp;
				int		p;

			tmp = path;
			tmp.Delete(0, 2);

			// NOW server\share\....
			if ((p = tmp.Find(_T('\\'))) >= 0)
				{
				// NOW share\....
				tmp.Delete(0, p+1);

				if ((p = tmp.Find(_T('\\'))) >= 0) tmp.Delete(p, tmp.GetLength()-p);

				// NOW share
				v = tmp;
				}
			}

		return v;
		}


/**
*** This function splits a network path into it's component parts (server,
*** share and directory).
***
*** Under windows a path \\\\server\\share\\dir is split into the 3 parts
*** as expected.
***
*** Under Unix the path is expected to be of the form /net/server/path
**/
SWIFT_DLL_EXPORT void
sw_file_splitRemotePath(const SWString &path, SWString &server, SWString &share, SWString &rest)
		{
		SWString	v = path;

		server = share = rest = _T("");

		if (v.left(5) == _T("/net/"))
			{
			// Case 1: Unix path using /net
			int	p;

			v.remove(0, 5);

			// Now expect server/path
			if ((p = v.first(_T('/'))) >= 0)
			{
			server = v.left(p);
			v.remove(0, p);
			}
			}
		else if (v.GetLength() > 2 && v[0] == _T('\\') && v[1] == _T('\\'))
			{
			// Case 2: \\server\share\...
				int		p;

			// Remove initial slashes
			v.Delete(0, 2);

			// NOW server\share\....
			if ((p = v.Find(_T('\\'))) >= 0)
			{
			server = v.Left(p);
			v.Delete(0, p+1);

			// NOW share\....
			if ((p = v.Find(_T('\\'))) >= 0)
				{
				share = v.Left(p);
				v.Delete(0, p);
				}
			else

				{

				share = v;

				v = _T("");

				}

			// If we have got a real share (not x$) then we make the rest
			// a relative file name by removing the first \ character from v
			if (!(share.GetLength() == 2 && share[1] == _T('$')) && !v.isEmpty()) v.Delete(0, 1);

			/*
			if (share.GetLength() == 2 && share[1] == _T('$')) drive = share[0];
			else
				{
				SWString tmp;

				tmp = getServerSharePath(server, share);

				if (!tmp.IsEmpty())
				{
				// Now have drive:dir
				drive = tmp[0];
				tmp.Delete(0, 3);
				v.Insert(1, _T('\\'));
				v.Insert(1, tmp);
				}
				}
			*/
			}
			}
		/*
		else if (v.GetLength() > 2 && v[1] == _T(':'))
			{
			// Case 3:  Drive:Directory
			drive = v[0];
			v.Delete(0, 2);
			}
		*/

		// Whatever's left we put into rest	
		rest = v;
		}
#endif // defined(NEVER)


SWIFT_DLL_EXPORT SWString
sw_file_extension(const SWString &file)
		{
		return SWFilename::extension(file);
		}


SWIFT_DLL_EXPORT void
sw_file_splitFilename(const SWString &file, SWString &base, SWString &extension)
		{
		SWFilename::split(file, base, extension);
		}


/**
*** Creates a full path name for the given server/file combination
*** for the unix platform
SWIFT_DLL_EXPORT SWString
sw_file_createRemoteUnixPath(const SWString &server, const SWString &path)
	{
	return sw_file_concatPaths(sw_file_concatPaths(_T("/net"), server, SW_FILE_UNIX_PATH), path, SW_FILE_UNIX_PATH);
	}
**/



/**
*** Creates a full path name for the given server/file combination
*** for the Dos/Windows platform
***
*** Path MUST be of the form drive:directory otherwise the function
*** returns an empty string
SWIFT_DLL_EXPORT SWString
sw_file_createRemoteWindowsPath(const SWString &server, const SWString &path)
	{
	SWString	result;
	SWString	tmp, drive, rest;

	rest = path;
	tmp = _T("\\\\");
	tmp += server;

	if (rest.length() >= 2 && rest[1] == _T(':'))
	    {
	    drive = rest[0];
	    drive += _T("$");
	    rest.remove(0, 2);
	    }

	// Can't do it without a drive
	if (drive.isEmpty()) return _T("");

	return sw_file_concatPaths(sw_file_concatPaths(tmp, drive, SW_FILE_DOS_PATH), rest, SW_FILE_DOS_PATH);
	}
**/


/**
*** Concatenate 2 paths
*** Assumes that 2nd path is relative, if not results are unpredicatable!
***
*** This function will handle both dos and unix paths. Specify a type
*** if a specific concatenation is required, otherwise the default
*** is to use the platform default. (Unix=/, Dos=\)
***
*** @param path		The first path
*** @param path2	The path to append
*** @param type		The path type (Default: 0 - platform dependent, 1 - Unix, 2 - Dos/Windows)
**/
SWIFT_DLL_EXPORT SWString
sw_file_concatPaths(const SWString &path, const SWString &path2, SWFilename::PathType type)
		{
		return SWFilename::concatPaths(path, path2, type);
		}


/**
*** Attempt to map the share on the given server to it's full (local) path
SWIFT_DLL_EXPORT SWString
sw_file_getServerSharePath(const SWString &server, const SWString &share)
	{
	SWString	result;
#if defined(WIN32) || defined(_WIN32)
	if (share.GetLength() == 2 && share[1] == _T('$'))
	    {
	    result = share[0];
	    result += _T(":\\");
	    }

	SWRegistryKey	rrk(server, HKEY_LOCAL_MACHINE, _T("SYSTEM\\CurrentControlSet\\Services\\lanmanserver\\Shares"));

	if (rrk.valueExists(share))
	    {
	    SWRegistryValue   rv = rrk.getValue(share);
	    TCHAR	    *p;

	    // This is a multipart string and we want the third entry
	    p = (TCHAR *)rv.data();

	    while (*p)
		{
		if (t_strncasecmp(p, _T("Path="), 5) == 0)
		    {
		    result = p+5;
		    break;
		    }

		// Skip to the end of the string
		while (*p) p = p++;
		p++;
		}
	    }
#endif

	return result;
	}
**/



/**
*** Convert a Unix filename to a Dos one / -> \ 
**/
SWIFT_DLL_EXPORT SWString
sw_file_unixToDosName(const SWString &filename)
		{
		SWString	file=filename;

		file.replaceAll(_T('/'), _T('\\'));

		return file;
		}




/**
*** Convert a Unix filename to a Dos one \ -> / 
**/
SWIFT_DLL_EXPORT SWString
sw_file_dosToUnixName(const SWString &filename)
		{
		SWString	file = filename;

		file.replaceAll(_T('\\'), _T('/'));

		return file;
		}




/**
*** This function gets the file times for the specified path.
*** The time parameters are optional and if they are specifed
*** as NULL then that time element is not retrieved.
***
*** Note that Unix does not have the concept of creation time
*** so it is simulated using last modification time.
***
*** @brief	Retrieves the various file times from the file
***
*** @param path			The filename to set the times on.
*** @param lastModTime		If not null a pointer to a SWTime object to receive
***				the last modification time of the file.
*** @param createTime		If not null a pointer to a SWTime object to receive
***				the creation time of the file.
*** @param lastAccessTime	If not null a pointer to a SWTime object to receive
***				the last access time of the file.
**/
SWIFT_DLL_EXPORT sw_status_t
sw_file_getTimes(const SWString &path, SWTime *lastModTime, SWTime *createTime, SWTime *lastAccessTime)
		{
		return SWFileInfo::getFileTimes(path, lastModTime, createTime, lastAccessTime);
		}


/**
*** This function sets the file times for the specified path.
*** The time parameters are optional and if they are specifed
*** as NULL then that time element is not modified.
***
*** Note that Unix does not have the concept of creation time
*** so it is ignored.
***
*** @brief	Retrieves the various file times from the file
***
*** @param path			The filename to set the times on.
*** @param lastModTime		If not null a pointer to a SWTime object to containing
***				the last modification time of the file.
*** @param createTime		If not null a pointer to a SWTime object to containing
***				the creation time of the file.
*** @param lastAccessTime	If not null a pointer to a SWTime object to containing
***				the last access time of the file.
**/
SWIFT_DLL_EXPORT sw_status_t
sw_file_setTimes(const SWString &path, SWTime *lastModTime, SWTime *createTime, SWTime *lastAccessTime)
		{
		return SWFileInfo::setFileTimes(path, lastModTime, createTime, lastAccessTime);
		}



/**
*** Returns the name of a directory in which temporary files can be created.
***
*** @param dir	The preferred directory in which to store the file. If specified
***		and is a real directory this value will be returned. If NULL the system
***		tries to work out an appropriate value according to the platform.
**/
SWIFT_DLL_EXPORT SWString
sw_file_getTempDirectory(const SWString &dir)
		{
		return SWTempFile::getTempDirectory(dir);
		}



/**
*** Creates a temporary file name
***
*** @param prefix	The prefix for the file part of the temporary filename. If NULL
***			the prefix tmp is used
*** @param ext		The filename extension to use.
*** @param dir		The directory name in which to store the file. If NULL the system
***			tries to work out an appropriate value
**/
SWIFT_DLL_EXPORT SWString
sw_file_getTempFile(const SWString &prefix, const SWString &ext, const SWString &dir)
		{
		return SWTempFile::getTempFileName(prefix, ext, dir);
		}



/**
*** Fixes a filename to ensure that it is valid and contains no extra components.
***
*** For the windows platform:
***	C:    		-> C:\\<br>
***	C:\\xxx\\	-> C:\\xxx
***
*** For the Unix platforms
***	/a/	-> /a<br>
***	/a//b	-> /a/b
**/
SWIFT_DLL_EXPORT SWString
sw_file_fixname(const SWString &filename)
		{
		return SWFilename::fix(filename);
		}




/**
*** Perform a "stat" of the given filename on the given server
***
*** Requires access to the server via /net (Unix) or a Windows share (Win32)
SWIFT_DLL_EXPORT int
sw_file_statServer(const SWString &server, const SWString &path, sw_stat_t *sp)
	{
	return t_stat(sw_file_createRemotePath(server, path), sp);
	}
**/


/**
*** Test if the given filename is a valid directory on the given server
***
*** Requires access to the server via /net (Unix) or a Windows share (Win32)
SWIFT_DLL_EXPORT bool
sw_file_isServerDirectory(const SWString &server, const SWString &path)
	{
	sw_stat_t sb;

	if (sw_file_statServer(server, path, &sb) != 0) return false;
	
	return ((sb.st_mode & S_IFDIR) == S_IFDIR);
	}
**/


/**
*** Test if the given filename is a valid file on the given server
***
*** Requires access to the server via /net (Unix) or a Windows share (Win32)
SWIFT_DLL_EXPORT bool
sw_file_isServerFile(const SWString &server, const SWString &path)
	{
	sw_stat_t sb;

	if (sw_file_statServer(server, path, &sb) != 0) return false;
	
	return ((sb.st_mode & S_IFREG) == S_IFREG);
	}
**/


/*
** Get the system root for the specified server
SWIFT_DLL_EXPORT SWString
sw_file_getServerSystemRoot(const SWString &server)
	{
	SWString    v;

	::GetSystemDirectory(v.GetBuffer(512), 512);
	
#if defined(WIN32) || defined(_WIN32)
	v = sw_registry_getServerString(server, HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion"),_T("SystemRoot"));
#else
	v = _T("/");
#endif
	
	return v;
	}
*/


/*
** Get the system drive for the specified server
SWIFT_DLL_EXPORT TCHAR
sw_file_getServerSystemDrive(const SWString &server)
	{
#if defined(WIN32) || defined(_WIN32)
	SWString	root;

	root = sw_file_getServerSystemRoot(server);

	return root.IsEmpty()?0:root[0];
#else
	// Unix doesn't have a drive concept!
	return 0;
#endif
	}
*/




/**
*** Split a path into it's component parts.
**/
SWIFT_DLL_EXPORT sw_size_t
sw_file_splitPath(const SWString &path, SWStringArray &sa)
		{
		return SWFilename::splitPath(path, sa);
		}


/**
*** Optimize a path by reducing duplicate path separators
*** . and .. where possible
**/
SWIFT_DLL_EXPORT SWString	
sw_file_optimizePath(const SWString &path, sw_file_pathtype type)
		{
		SWStringArray	sa;

		sw_file_splitPath(path, sa);
		return sw_file_joinPath(sa, type);
		}


/**
*** Joins path components to make a path
**/
SWIFT_DLL_EXPORT SWString	
sw_file_joinPath(SWStringArray &sa, sw_file_pathtype type)
		{
		return SWFilename::joinPath(sa, type);
		}


/**
*** Returns the absolute path name for the given path
*** in relation to the specified directory (default current)
***
*** If the neither the directory nor the path passed to this function are absolute
*** it is impossible to generate an absolute path and the result will be the same
*** as calling sw_file_concatPaths.
***
*** @param path		The path name
*** @param dir		The directory name used to generate a relative path
*** @param type		The path type (Default: 0 - platform dependent, 1 - Unix, 2 - Dos/Windows)
**/
SWIFT_DLL_EXPORT SWString	
sw_file_absolutePath(const SWString &path, const SWString &dir, sw_file_pathtype type)
		{
		SWString	result;

		if (sw_file_isAbsolutePath(path)) result = path;
		else result = sw_file_concatPaths(dir, path, type);

		return sw_file_optimizePath(result, type);
		}


/**
*** Returns the relative path name for the given path
*** in relation to the specified directory (default .)
***
*** There are certain conditions when it is impossible to calculate a relative
*** path. These are described below:
***
*** When the path is absolute and the directory is relative it is impossible
*** to determine the relative path between them. Under these circumstances the
*** orginal path is returned.
***
*** When the path and the directory are on different physical drives (e.g. c: and d:)
*** there is no relative path between them. Under these circumstances the
*** orginal path is returned.
***
*** @param p		The path name (can be absolute)
*** @param d		The directory name used to generate an absolute path
*** @param type		The path type (Default: 0 - platform dependent, 1 - Unix, 2 - Dos/Windows)
**/
SWIFT_DLL_EXPORT SWString		
sw_file_relativePath(const SWString &p, const SWString &d, sw_file_pathtype type)
		{
		// Check for impossible situations which require special handling
		if (sw_file_isAbsolutePath(p) && sw_file_isRelativePath(d)) return p;
		if (p.length() > 1 && d.length() > 1 && p[1] == ':' && d[1] == ':' && p[0] != d[0]) return p;

		SWString	dir=d;
		SWString	path=p;

		// Now path AND dir are both absolute
		SWStringArray	pa, da, ra;
		int		i, j, npa, nda;

		npa = (int)sw_file_splitPath(path, pa);
		nda = (int)sw_file_splitPath(dir, da);

		i = 0;
		while (i < npa && i < nda)
			{
			// Ignore case on dos paths
			if (pa[i].compareTo(da[i], type==SW_FILE_DOS_PATH?SWString::ignoreCase:SWString::exact) != 0) break;
			i++;
			}

		for (j=i;j<nda;j++)
			{
			if (!sw_file_isAbsolutePath(da[j])) ra.add(_T(".."));
			}

		for (j=i;j<npa;j++) 
			{
			if (!sw_file_isAbsolutePath(pa[j])) ra.add(pa[j]);
			}

		if (ra.size() == 0) return _T(".");

		return sw_file_joinPath(ra, type);
		}


/**
*** Test to see if the given program is in the current path
**/
SWIFT_DLL_EXPORT bool
sw_file_isProgramInPath(const SWString &prog)
		{
		SWString	path = sw_file_pathForProgram(prog);

		return !path.isEmpty();
		}


/**
*** Tests to see if the given program is in the directory.
***
*** For unix systems this function will detect if the given program is a file
*** and is executeable.
***
*** For windows systems this function will detect if the program exists by appending
*** various extensions to the program name and checking if the file exists.
***
*** On all systems the filename parameter is filled in with the full pathname of the
*** discovered file.
***
*** @param	prog		The program name to look for (e.g. ls)
*** @param	dir		The directory in which to check
*** @param	filename	Receives the full program path if found.
*** @result			true if the program is found, false otherwise.
**/
SWIFT_DLL_EXPORT bool
sw_file_isProgramInDirectory(const SWString &prog, const SWString &dir, SWString &filename)
		{
		SWString	s;
		bool		found=false;

		s = sw_file_concatPaths(dir, prog);

	#if defined(WIN32) || defined(_WIN32)
		SWString	pathext, ext, path;

		// Windows NT defines the prog extensions in the PATHEXT environment var.
		if (!sw_misc_getEnvironmentVariable(_T("PATHEXT"), pathext)) pathext = _T(".com;.exe;.bat;.cmd");

		SWTokenizer	tok(pathext, _T(";"));

		while (tok.hasMoreTokens())
			{
			ext = tok.nextToken();
			if (prog.endsWith(ext, SWString::ignoreCase)) path = s;
			else path = sw_file_concatPaths(s, ext);

			if (sw_file_isFile(s) && t_access(path, 0) == 0)
				{
				found = true;
				filename = path;
				break;
				}
			}

	#else
		if (sw_file_isFile(s) && t_access(s, X_OK) == 0)
			{
			found = true;
			filename = s;
			}
	#endif

		return found;
		}



/**
*** Find the path for the given program.
**/
SWIFT_DLL_EXPORT SWString
sw_file_pathForProgram(const SWString &prog)
		{
		bool		found=false;
		SWString	filename;

	#if defined(WIN32) || defined(_WIN32)
		// Under windows "." is always in the path
		found = sw_file_isProgramInDirectory(prog, _T("."), filename);
	#endif

		if (!found)
			{
			SWString	path;

			sw_misc_getEnvironmentVariable(_T("PATH"), path);

	#if defined(WIN32) || defined(_WIN32)
			SWTokenizer	tok(path, _T(";"));
	#else
			SWTokenizer	tok(path, _T(":"));
	#endif

			while (tok.hasMoreTokens())
				{
				path = tok.nextToken();
				found = sw_file_isProgramInDirectory(prog, path, filename);
				if (found) break;
				}
			}

		if (!found) filename.empty();

		return filename;
		}



/**
*** Pushes the current directory onto an internal stack
*** and attempts to change to the specified directory.
***
*** If the setCurrentDirectory fails the current directory
*** is NOT pushed onto the stack.
***
*** @return	The result of calling sw_file_getCurrentDirectory
**/
SWIFT_DLL_EXPORT sw_status_t
sw_file_pushCurrentDirectory(const SWString &newdir)
		{
		return SWFolder::pushCurrentDirectory(newdir);
		}


/**
*** Pops the a previously remembered directory name from the internal
*** stack and changes to that directory.
***
*** If the setCurrentDirectory fails the current directory
*** is NOT pushed onto the stack.
***
*** @return	The result of calling sw_file_getCurrentDirectory
**/
SWIFT_DLL_EXPORT sw_status_t
sw_file_popCurrentDirectory()
		{
		return SWFolder::popCurrentDirectory();
		}


/**
*** Set the current working directory
***
*** @return	true is successful, false otherwise.
**/
SWIFT_DLL_EXPORT sw_status_t
sw_file_setCurrentDirectory(const SWString &newdir)
		{
		return SWFolder::setCurrentDirectory(newdir);
		}



/**
*** Gets the current working directory
**/
SWIFT_DLL_EXPORT SWString
sw_file_getCurrentDirectory()
		{
		return SWFolder::getCurrentDirectory();
		}



/**
*** Gets file information for the given filename.
***
*** This is similar to the unix stat function, but the information is 
*** more comprehensive. This function also takes care of the differences
*** between Unix and Windows and is preferrable to using one of the platform
*** specific function calls.
***
*** @param	filename	The filename to get the information for.
*** @param	fileinfo	A SWFileInfo object to receive the info.
***
*** @return	true is successful, false otherwise.
**/
SWIFT_DLL_EXPORT sw_status_t
sw_file_getFileInfo(const SWString &filename, SWFileInfo &fileinfo)
		{
		return fileinfo.getInfoForFile(filename);
		}


SWIFT_DLL_EXPORT void
sw_file_grantEverybodyAccessToFile(const SWString &FileName)
		{
#if defined(SW_PLATFORM_WINDOWS)
		PSID pEveryoneSID = NULL;
		PACL pACL = NULL;
		EXPLICIT_ACCESS ea[1];
		SID_IDENTIFIER_AUTHORITY SIDAuthWorld = SECURITY_WORLD_SID_AUTHORITY;

		// Create a well-known SID for the Everyone group.
		AllocateAndInitializeSid(&SIDAuthWorld, 1,
							SECURITY_WORLD_RID,
							0, 0, 0, 0, 0, 0, 0,
							&pEveryoneSID);

		// Initialize an EXPLICIT_ACCESS structure for an ACE.
		// The ACE will allow Everyone read access to the key.
		ZeroMemory(&ea, 1 * sizeof(EXPLICIT_ACCESS));
		ea[0].grfAccessPermissions = 0xFFFFFFFF;
		ea[0].grfAccessMode = GRANT_ACCESS;
		ea[0].grfInheritance= SUB_CONTAINERS_AND_OBJECTS_INHERIT;
		ea[0].Trustee.TrusteeForm = TRUSTEE_IS_SID;
		ea[0].Trustee.TrusteeType = TRUSTEE_IS_WELL_KNOWN_GROUP;
		ea[0].Trustee.ptstrName  = (LPTSTR) pEveryoneSID;

		// Create a new ACL that contains the new ACEs.
		SetEntriesInAcl(1, ea, NULL, &pACL);

		// Initialize a security descriptor.  
		PSECURITY_DESCRIPTOR pSD = (PSECURITY_DESCRIPTOR) LocalAlloc(LPTR, 
									SECURITY_DESCRIPTOR_MIN_LENGTH); 

		InitializeSecurityDescriptor(pSD,SECURITY_DESCRIPTOR_REVISION);

		// Add the ACL to the security descriptor. 
		SetSecurityDescriptorDacl(pSD, 
				TRUE,     // bDaclPresent flag   
				pACL, 
				FALSE);   // not a default DACL 


		//Change the security attributes
		SetFileSecurity(FileName, DACL_SECURITY_INFORMATION, pSD);

		if (pEveryoneSID) 
			FreeSid(pEveryoneSID);
		if (pACL) 
			LocalFree(pACL);
		if (pSD) 
			LocalFree(pSD);
#else
		SWFileInfo			fi(FileName);
		SWFileAttributes	fa=fi.getAttributes();

		if (fa.isDirectory() || fa.isOwnerExecute() || fa.isGroupExecute() || fa.isWorldExecute())
			{
			chmod(FileName, fa.attributes()|0777);
			}
		else
			{
			chmod(FileName, fa.attributes()|0666);
			}
#endif
		}




SWIFT_DLL_EXPORT sw_status_t
sw_file_copyFile(const SWString &fromfile, const SWString &tofile)
		{
		SWTime		fromWriteTime;
		SWTime		fromCreateTime;
		SWTime		fromAccessTime;
		sw_int64_t	nbytes=0;
		int			keptdate=0;

		SWFileInfo	frominfo(fromfile);

		if (!frominfo.isFile(fromfile))
			{
			return SW_STATUS_NOT_FOUND;
			}

		SWFileInfo::getFileTimes(fromfile, &fromWriteTime, &fromCreateTime, &fromAccessTime);

		// Need to make a copy
		SWFile		in, out;
		sw_status_t	res;

		if ((res = in.open(fromfile, SWFile::ModeBinary|SWFile::ModeReadOnly)) == SW_STATUS_SUCCESS)
			{
			if ((res = out.open(tofile, SWFile::ModeBinary|SWFile::ModeWriteOnly|SWFile::ModeCreate|SWFile::ModeTruncate)) != SW_STATUS_SUCCESS)
				{
				if (SWFileInfo::isFile(tofile))
					{
					SWFile::remove(tofile);
					}
				res = out.open(tofile, SWFile::ModeBinary|SWFile::ModeWriteOnly|SWFile::ModeCreate|SWFile::ModeTruncate);
				}

			if (res == SW_STATUS_SUCCESS)
				{
				// OK now do the copy
				#define COPYBUFSIZE 0x8000

				sw_byte_t	*buf;
				sw_size_t	bytesread, byteswritten;

				buf = new sw_byte_t[COPYBUFSIZE];

				while (res == SW_STATUS_SUCCESS)
					{
					res = in.read(buf, COPYBUFSIZE, &bytesread);
					if (res == SW_STATUS_SUCCESS)
						{
						if (bytesread == 0)
							{
							// We are finished
							break;
							}
						
						res = out.write(buf, bytesread, &byteswritten);
						if (res == SW_STATUS_SUCCESS && bytesread != byteswritten)
							{
							res = SW_STATUS_SYSTEM_ERROR;
							break;
							}
						}
					}

				delete buf;
				}

			in.close();
			}

		if (res == SW_STATUS_SUCCESS)
			{
			SWFileInfo::setFileTimes(tofile, &fromWriteTime, &fromCreateTime, &fromAccessTime);
			}
		
		return res;
		}


// Get the size of a complete folder
SWIFT_DLL_EXPORT sw_filesize_t
sw_file_getFolderSize(const SWString &path)
		{
		sw_filesize_t	res=0;
		SWFolder		folder;

		if (folder.open(path) == SW_STATUS_SUCCESS)
			{
			SWFileInfo	entry;
			SWString	name;

			while (folder.read(entry) == SW_STATUS_SUCCESS)
				{
				name = entry.getName();

				if (name == "." || name == "..") continue;

				if (entry.isFile())
					{
					res += entry.getSize();
					}
				else if (entry.isFolder())
					{
					res += sw_file_getFolderSize(entry.getFullPathname());
					}
				}
			}

		return res;
		}



SWIFT_DLL_EXPORT sw_filesize_t
sw_filesystem_getFreeSpace(const SWString &path)
		{
		sw_filesize_t	res=0;

#if defined(SW_PLATFORM_WINDOWS)
		ULARGE_INTEGER	callerAvailable, totalBytes, freeBytes;

		if (GetDiskFreeSpaceEx(path, &callerAvailable, &totalBytes, &freeBytes))
			{
			res = freeBytes.QuadPart;
			}
#else
		struct statvfs	info;
		
		if (statvfs(path, &info) == 0)
			{
			res = info.f_bfree * info.f_bsize;
			}
#endif

		return res;
		}


SWIFT_DLL_EXPORT sw_filesize_t
sw_filesystem_getAvailableSpace(const SWString &path)
		{
		sw_filesize_t	res=0;

#if defined(SW_PLATFORM_WINDOWS)
		ULARGE_INTEGER	callerAvailable, totalBytes, freeBytes;

		if (GetDiskFreeSpaceEx(path, &callerAvailable, &totalBytes, &freeBytes))
			{
			res = callerAvailable.QuadPart;
			}
#else
		struct statvfs	info;
		
		if (statvfs(path, &info) == 0)
			{
			res = info.f_bavail * info.f_bsize;
			}
#endif
		
		return res;
		}
