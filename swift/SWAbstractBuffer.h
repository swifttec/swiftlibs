/**
*** @file		SWAbstractBuffer.h
*** @brief		A general purpose memory buffer
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWAbstractBuffer class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#ifndef __SWAbstractBuffer_h__
#define __SWAbstractBuffer_h__

#include <swift/SWObject.h>
#include <swift/SWMemoryManager.h>




/**
*** @brief General purpose memory buffer class.
***
*** SWAbstractBuffer forms the basis of the SWBuffer and SWBufferReference
*** classes. It contains most of what was originally in the SWBuffer class
*** but has removed from it the methods that are "dangerous" when used in
*** the context of a referenced external buffer as used by the SWBufferReference.
*** 
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWAbstractBuffer : public SWObject
		{
public:
		/// Various internal buffer flags
		enum
			{
			ReadOnly=1,
			FixedSize=2,
			Reference=4,
			AllocateExactAmount=8,
			AllocateDoubleCapacity=16
			} SWAbstractBufferFlags;

protected:
		/// Internal constructor
		SWAbstractBuffer(SWMemoryManager *pManager, sw_byte_t *ptr, sw_size_t capacity, sw_uint32_t flags);

public:

		/// Virtual destructor
		virtual ~SWAbstractBuffer();

		/// Copy constructor - makes a copy of the SWAbstractBuffer specified.
		SWAbstractBuffer(const SWAbstractBuffer &other);

		/**
		*** @brief	Assignment operator - copies data from the other buffer
		***
		*** This assignment operator copies data from the SWAbstractBuffer object specified
		*** into the current object. The buffer is expanded as required to accomodate
		*** the memory. If it is not possible to expand the buffer an exception will be thrown.
		***
		*** Developers should note that only data is copied, other features like the
		*** memory manager are not modified. This is useful when two buffers are in use
		*** one using a local heap and one in shared memory. In this case assigning from
		*** one buffer to the other will copy data to/from shared memory.
		***
		*** @throws	SWMemoryException
		***/
		SWAbstractBuffer &	operator=(const SWAbstractBuffer &other);

		/**
		*** Clear the buffer and set the size to zero
		**/
		virtual void		clear();

		/**
		*** Clear the buffer contents (reset to zero bytes), but keep the size and capacity the same
		**/
		virtual void		clearContents()		{ setContents(0); }

		/**
		*** Set the buffer contents to contain all bytes as v
		**/
		void				setContents(sw_byte_t v);

		/**
		*** Write into the buffer from the specified memory location adjusting size if required.
		***
		*** @param	from	The memory location to copy data from.
		*** @param	nbytes	The number of bytes of data to copy.
		*** @param	offset	The offset into this buffer at which to start copying data to.
		***
		*** @return 		The number of bytes copied
		**/
		virtual sw_size_t	write(const void *from, sw_size_t nbytes, sw_size_t offset);


		/**
		*** Read data from the buffer into the specified memory location.
		***
		*** @param	to		The memory location to copy data to. <b>This buffer must contain
		***					sufficient space to copy nbytes of data to.</b>
		*** @param	nbytes	The number of bytes of data to copy.
		*** @param	offset	The offset into this buffer at which to start copying data from.
		***
		*** @return 		The number of bytes copied.
		**/
		virtual sw_size_t	read(void *to, sw_size_t nbytes, sw_size_t offset) const;

		/**
		*** @brief	Return a bool indicating if this is a read-only buffer.
		**/
		bool				isReadOnly() const		{ return ((m_flags & ReadOnly) != 0); }

		/**
		*** @brief	Return a bool indicating if this is a fixed-size buffer.
		**/
		bool				isFixedSize() const		{ return ((m_flags & FixedSize) != 0); }

		/**
		*** @brief	Return a bool indicating if this is a referenced buffer (external buffer).
		**/
		bool				isReference() const		{ return ((m_flags & Reference) != 0); }

		/// Return the capacity of the buffer in bytes
		virtual sw_size_t	size() const;

		/// Ensure the size is sufficient

		/// Return the capacity of the buffer in bytes
		virtual sw_size_t	capacity() const;

		/** 
		*** @brief Ensure the capacity is sufficient
		***
		*** Sets the data size of the buffer (as returned by size()) to the size specified.
		*** The capacity of the buffer will be adjusted to ensure that it is possible to store
		*** the specified number of bytes.
		***
		*** @param size				The new size required.
		*** @param preserveData		If true preserve the old data, otherwise the old data may be discarded.
		*** @param clearNewData		If true any new part of the buffer will be intialised to zero.
		***							If both keep and zero flags are true the entire buffer will be intialised
		***							to zero. If zero is false no initialisation of the buffer is done.
		*** @param freeExtraMemory	If true set the buffer capacity to the exact size.
		***
		*** @throws	SWMemoryException
		**/
		virtual void		capacity(sw_size_t size, bool preserveData=true, bool clearNewData=true, bool freeExtraMemory=false);

		/** 
		*** @brief Set/change the size of the buffer.
		***
		*** Sets the data size of the buffer (as returned by size()) to the size specified.
		*** The capacity of the buffer will be adjusted to ensure that it is possible to store
		*** the specified number of bytes.
		***
		*** @param size	The new size required.
		*** @param keep	If true preserve the old data, otherwise the old data may be discarded.
		*** @param zero	If true any new part of the buffer will be intialised to zero.
		***				If both keep and zero flags are true the entire buffer will be intialised
		***				to zero. If zero is false no initialisation of the buffer is done.
		*** @param trim	If true set the buffer capacity to the exact size.
		***
		*** @throws	SWMemoryException
		**/
		virtual void		size(sw_size_t size, bool preserveData=true, bool clearNewData=true, bool freeExtraMemory=false);

		/**
		*** @brief	Return a pointer to the data - NOTE this can return NULL
		***
		*** This method returns a pointer to the internal buffer. The pointer is returned
		*** as a <b>const void *</b> to force the caller to cast this object. Care should
		*** be taken when casting this to a non-const pointer when the buffer is marked as
		*** read-only. It is impossible to prevent this in the compiler so this is the
		*** responsibilty of the caller.
		**/
		void *				data() const		{ return m_pData; }

		/// Ensure the buffer capacity is sufficient and return a pointer to the buffer
		void *				data(sw_size_t nbytes, bool freeExtra=false);

		/**
		*** @brief	Return the byte at the specified position.
		**/
		sw_byte_t			getByteAt(sw_size_t idx) const;

		/**
		*** @brief	Set the byte at the specified position.
		**/
		void				setByteAt(sw_size_t idx, sw_byte_t value);

		/**
		*** @brief	Index operator (references the nth byte of the buffer).
		***
		*** This operator return a reference to the specified byte in the buffer.
		**/
		sw_byte_t & operator[](int i) const;

		/**
		*** @brief	Compare to memory buffer (using memcmp)
		***
		*** This methods compares the contents of the buffer with that of
		*** the specified buffer using the memcmp function. The comparision is done
		*** first comparing data and then (if neccessary) buffer length
		*** using the the following algorithm:
		***
		*** - The buffers are compared using memcmp for the minimum length
		***   of both buffers.
		*** - If the buffers are the same (memcmp returned zero) then the
		***   relative length is used to determine the result.
		***		- this->size() < size return -1
		***		- this->size() == size return 0
		***		- this->size() > size return 1
		***
		*** @param[in]	ptr		The memory buffer to compare with
		*** @param[in]	size	The size of the the memory buffer pointer to by ptr
		***
		*** @return		An integer determining if this buffer is less than (-ve result), equal to
		***				(zero result) or greater than (+ve result) than the buffer specified
		***				by the ptr and size parameters.
		**/
		int					compareTo(const void *ptr, size_t size) const;

		/**
		*** @brief	Compare to another Buffer (using memcmp)
		***
		*** This method uses the compareTo(const void *ptr, size_t size) method to compare
		*** the data.
		***
		*** @see compareTo(const void *ptr, size_t size)
		***
		*** @param[in] other	The buffer to compare with.
		**/
		int					compareTo(const SWAbstractBuffer &other) const;

		/// Sets the memory allocation stratgey flags
		void				setAllocationStrategy(sw_uint32_t flags);

public: // Cast operators

		/// Cast to a void *
		operator void *() const					{ return data(); }

		/// Cast to a const void *
		operator const void *() const			{ return data(); }

		/// Cast to a char *
		operator char *() const					{ return reinterpret_cast<char *>(data()); }

		/// Cast to a const char *
		operator const char *() const			{ return reinterpret_cast<const char *>(data()); }

		/// Cast to a wchar_t *
		operator wchar_t *() const				{ return reinterpret_cast<wchar_t *>(data()); }

		/// Cast to a const wchar_t *
		operator const wchar_t *() const		{ return reinterpret_cast<const wchar_t *>(data()); }

		/// Cast to a char *
		operator unsigned char *() const		{ return reinterpret_cast<unsigned char *>(data()); }

		/// Cast to a const char *
		operator const unsigned char *() const	{ return reinterpret_cast<const unsigned char *>(data()); }

public: // Comparison operators

		/**
		*** @brief	Test for equality
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is equal to the other object, false otherwise.
		**/
		bool	operator==(const SWAbstractBuffer &other) const	{ return (compareTo(other) == 0); }

		/**
		*** @brief	Test for inequality
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is not equal to the other object, false otherwise.
		**/
		bool	operator!=(const SWAbstractBuffer &other) const	{ return (compareTo(other) != 0); }

		/**
		*** @brief	Test for less than.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is less than the other object, false otherwise.
		**/
		bool	operator< (const SWAbstractBuffer &other) const	{ return (compareTo(other) <  0); }

		/**
		*** @brief	Test for greater than.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is greater than the other object, false otherwise.
		**/
		bool	operator> (const SWAbstractBuffer &other) const	{ return (compareTo(other) >  0); }

		/**
		*** @brief	Test for less than or equal to.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is less than or equal to the other object, false otherwise.
		**/
		bool	operator<=(const SWAbstractBuffer &other) const	{ return (compareTo(other) <= 0); }

		/**
		*** @brief	Test for greater than or equal to.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is greater than or equal to the other object, false otherwise.
		**/
		bool	operator>=(const SWAbstractBuffer &other) const	{ return (compareTo(other) >= 0); }



public: // Virtual overrides

		/// Override for SWObject writeToStream
		virtual sw_status_t		writeToStream(SWOutputStream &stream) const;

		/// Override for SWObject readFromStream
		virtual sw_status_t		readFromStream(SWInputStream &stream);


protected:
		SWMemoryManager	*m_pMemoryManager;	///< The memory manager to use for this buffer
		sw_byte_t		*m_pData;			///< The actual memory buffer
		sw_size_t		m_capacity;			///< The capacity of the memory buffer
		sw_uint32_t		m_flags;			///< Various internal flags
		};


/// General string exception
SW_DEFINE_EXCEPTION(SWBufferException, SWException);

/// Thrown when an attempt is made to modify a read-only buffer.
SW_DEFINE_EXCEPTION(SWBufferReadOnlyException, SWBufferException);

/// Thrown when a buffer modification would cause a buffer to overflow.
SW_DEFINE_EXCEPTION(SWBufferOverflowException, SWBufferException);




#endif
