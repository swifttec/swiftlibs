/**
*** @file		SWClientSocket.h
*** @brief		A generic socket implementation
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWClientSocket class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWClientSocket_h__
#define __SWClientSocket_h__

#include <swift/SWSocketAddress.h>
#include <swift/SWSocket.h>




/**
*** @brief	A non-specific client socket
***
*** This object provides access to the client parts of a socket implementation.
*** It allows the caller to set up the socket to connect to a listening
*** SWServerSocket.
***
*** The developer has the option of supplying the underlying socket or allowing
*** the class to determine the required socket type and provide it.
***
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWClientSocket : public SWObject
		{
public:
		/// Default constructor
		SWClientSocket();

		/// Constructor with specific socket implementation
		SWClientSocket(SWSocket *pSocket, bool delflag);

		/// Virtual destructor
		~SWClientSocket();

		/// Create a socket connection to a listening socket
		sw_status_t			connect(const SWSocketAddress &addr);

		/// Close the socket
		sw_status_t			close();

		/// Read data from the socket
		sw_status_t			read(void *pBuffer, sw_size_t nbytes, sw_size_t *pBytesRead=NULL);

		/// Write data to the socket
		sw_status_t			write(const void *pBuffer, sw_size_t nbytes, sw_size_t *pBytesWritten=NULL);

		/// Check if this socket is connected or not
		bool				isConnected() const;

		/// Returns a pointer to the socket implementation
		SWSocket *			getSocket()	{ return _pSocket; }

		/// Set the socket implementation
		void				setSocket(SWSocket *pSocket, bool delflag);

		/**
		*** @brief	Wait for input on the socket
		***
		*** This method waits for input activity on the socket.
		***
		*** @param[out]	waitms	The time in milliseconds to wait for input before
		***						timeout occurs.
		***
		*** @return				SW_STATUS_SUCCESSS if succesful, or an error
		***						code if the option fetch fails.
		**/
		sw_status_t			waitForInput(sw_int64_t waitms=SW_TIMEOUT_INFINITE);

protected:
		SWSocket	*_pSocket;	///< A pointer to the actual socket implementation
		bool		_delFlag;	///< A flag to indicate if the socket implementator should be delete on destruction
		};




#endif
