/**
*** @file		SWCollection.h
*** @brief		An abstract class for a collection of objects.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWCollection class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWCollection_h__
#define __SWCollection_h__

#include <swift/swift.h>
#include <swift/SWLockableObject.h>
#include <swift/SWIterator.h>



/**
*** @brief		Defines a data comparison function
***
*** This typedef defines a callback function called by the sort methods
*** of certain unordered collections to compare data. The function is
*** passed 2 void pointers which are pointers to the data to be compared.
*** The function should compare the data and return a value as follows:
***
*** - If pData1 < pData2 return a -ve value
*** - If pData1 == pData2 return zero
*** - If pData1 > pData2 return a +ve value
***
*** @param	pData1	A pointer to the first data block/object.
*** @param	pData2	A pointer to the second data block/object.
***
*** @return	An integer indicating if pData1 is less than, equal to, or greater than pData2.
**/
typedef int (sw_collection_compareDataFunction)(const void *pData1, const void *pData2);


/**
*** @brief An abstract collection class from which all SWIFT collections are derived.
***
*** SWCollection is an abstract class which forms the basis of all collection classes
*** int the SWIFT.  SWIFT Collections are basically a set of data pointers organised in
*** a particular fashion determined by the collection implementation.
*** The pointers can be pointers to data blocks or pointers to objects.
***
*** If the latter is used then special templated versions of the collections
*** are provided that will delete objects when removed from the collection.
***
*** All collections and maps in the SWIFT support the concept of modification checking when using iterators.
*** See SWIterator for more details.
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Safe when synchronized</td>
*** </tr>
*** </table>
***
***
*** @see		SWIterator, SWIterator_T
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWCollection : public SWLockableObject
		{
		//friend class SWIterator;

public:
		/// Constructor
		SWCollection() : m_modCount(0), m_nelems(0)		{ }

		virtual ~SWCollection()							{ }

		/// Returns true if this collection contains no objects
		virtual bool		isEmpty() const				{ return (size() == 0); }

		/// Returns the number of elements in this collection
		virtual sw_size_t	size() const				{ return m_nelems; }

		/// Returns the current modification count value
		virtual sw_size_t	getModCount() const			{ return m_modCount; }

protected:
		/**
		*** <i>incSize</i> increments the element count and automatically updates the modcount.
		*** <i>incSize</i> acts like the post-increment operator on the element count and returns
		*** the value of the element count <b>before</b> increment takes place
		***
		*** @brief	Increment the element count and automatically update the modcount
		*** @return	The previous element count
		**/
		sw_size_t		incSize()						{ m_modCount++; return m_nelems++; }

		/**
		*** <i>decSize</i> decrements the element count and automatically updates the modcount.
		*** <i>decSize</i> acts like the pre-decrement operator on the element count and returns
		*** the value of the element count <b>after</b> decrement takes place
		***
		*** @brief	Decrement the element count and automatically update the modcount
		*** @return	The new element count
		**/
		sw_size_t		decSize()						{ m_modCount++; return --m_nelems; }

		/**
		*** <i>setSize</i> sets the element count and automatically updates the modcount.
		***
		*** @brief	Set the element count and automatically update the modcount
		*** @param n	The new element count
		*** @return	The new element count
		**/
		sw_size_t		setSize(sw_size_t n)			{ if (m_nelems != n) { m_nelems = n; m_modCount++; } return m_nelems; }


		/**
		*** @brief		Returns an iterator for this collection
		*** @param[in]	iterateFromEnd	A flag indicating if the iterator starts at the beginning or the end of the collection.
		**/
		virtual SWCollectionIterator *	getIterator(bool iterateFromEnd=false) const=0;

		/**
		*** @brief	Compares the specified data for equality
		***
		*** This method is used by derived classes to compare data for ordering.
		*** The value returned indicates if the data pointed too is less than, equal
		*** to, or greater than.
		***
		*** If this method is not overridden this method compares the pointers and
		*** returns the appropriate result.
		***
		*** @param	pData1	A pointer to the first data element
		*** @param	pData2	A pointer to the second data element
		***
		*** @return		One of the following are returned
		***				- -ve	if pData1 < pData2
		*** 			- 0		if pData1 == pData2
		*** 			- +ve	if pData1 > pData2
		**/
		virtual int			compareData(const void *pData1, const void *pData2) const;

		/**
		*** @brief	Compares the specified data using either the specified comparision function (if not NULL) or the default one.
		***
		*** This method is normally used by derived classes to compare data for ordering
		*** when a sort is requested on an unordered collection. The pCompareFunc parameter
		*** can optionally specify a custom comparision function to use. If the pCompareFunc
		*** parameter is NULL the normal compareData method is called.
		***
		*** The value returned indicates if the data pointed too is less than, equal
		*** to, or greater than.
		***
		*** If this method is not overridden this method compares the pointers and
		*** returns the appropriate result.
		***
		*** @param	pCompareFunc	A pointer to the custom comparision function.
		*** @param	pData1			A pointer to the first data element
		*** @param	pData2			A pointer to the second data element
		***
		*** @return		One of the following are returned
		***				- -ve	if pData1 < pData2
		*** 			- 0		if pData1 == pData2
		*** 			- +ve	if pData1 > pData2
		***
		*** @see virtual int compareData(const void *pData1, const void *pData2) const, sw_collection_compareDataFunction
		**/
		virtual int
		doCompareData(sw_collection_compareDataFunction *pCompareFunc, const void *pData1, const void *pData2) const
				{
				return (pCompareFunc != NULL?pCompareFunc(pData1, pData2):compareData(pData1, pData2));
				}


public:
		static void	initIterator(SWAbstractIterator &it, SWCollectionIterator *pCollectionIterator);

private:
		sw_size_t		m_modCount;	///< A count of the number of modifications to this collection
		sw_size_t		m_nelems;	///< The number of element in this collection
		};



#endif
