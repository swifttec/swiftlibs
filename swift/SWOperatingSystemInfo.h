/**
*** @file		SWOperatingSystemInfo.h
*** @brief		Operating System information object.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWOperatingSystemInfo class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWOperatingSystemInfo_h__
#define __SWOperatingSystemInfo_h__


#include <swift/SWInfoObject.h>



// Forward declarations

/**
*** @brief	A class to represent the information about a system/machine/host.
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWOperatingSystemInfo : public SWInfoObject
		{
public:
		/// An enumeration of O/S family values.
		enum OSFamily
			{
			OS_FAMILY_UNKNOWN=0,	///< An unknown O/S family
			OS_FAMILY_UNIX,			///< A unix-based O/S (e.g. Solaris, Linux)
			OS_FAMILY_WINDOWS,		///< A windows-based O/S

			OS_FAMILY_MAX			///< The count of O/S families (always the last member)
			};

		/// An enumeration of O/S types
		enum OSType
			{
			OS_TYPE_UNKNOWN=0,		// an unknown O/S

			OS_TYPE_WIN31,			// Windows 3.1 running Win32s
			OS_TYPE_WIN95,			// Windows 95
			OS_TYPE_WIN98,			// Windows 98
			OS_TYPE_WINME,			// Windows Me
			OS_TYPE_WINNT351,		// Windows NT 3.51
			OS_TYPE_WINNT4,			// Windows NT 4
			OS_TYPE_WIN2K,			// Windows 2000
			OS_TYPE_WINXP,			// Windows XP
			OS_TYPE_WINDOWS,		// Windows (generic)

			OS_TYPE_SUNOS,			// Sun pre solaris 2
			OS_TYPE_SOLARIS,		// Sun solaris 2 onwards (SunOS 5.0 onwards)

			OS_TYPE_LINUX,			// Linux (generic)
			OS_TYPE_HPUX,			// HP-UX
			OS_TYPE_AIX,			// AIX
			OS_TYPE_TRU64,			// TRU64

			OS_TYPE_NETWARE,		// TRU64
			
			OS_TYPE_MAX				// The count of O/S types
			};

		/// An enumeration used to index values
		enum OSValueIndex
			{
			Family=0,			///< The O/S family (numeric) - see OSFamily enumeration.
			FamilyString,		///< The O/S family (string)
			Type,				///< The O/S specific type (e.g. WindowsXP) - see OSType enumeration.

			Name,				///< The O/S name (as reported by the O/S)
			CommonName,			///< The O/S name (as it is commonly known by)
			Description,		///< The O/S description
			Version,			///< The O/S version string (normalised for general use)
			Release,			///< The O/S release string
			VersionMajor,		///< The major component of the O/S version
			VersionMinor,		///< The minor component of the O/S version
			VersionRevision,	///< The revision component of the O/S version
			BuildNumber,		///< The build number component of the O/S version
			PatchLevel,			///< The patch level component of the O/S version
			SysVersion,			///< The O/S version as reported by the system (not the normalised version number)

			OSValueIndexMax		///< The count of O/S values (always the last member)
			};


public:
		/// Default constructor - constructs an empty value of type VtNone
		SWOperatingSystemInfo();

		/// Virtual destrcutor
		virtual ~SWOperatingSystemInfo();

		/// Copy constructor
		SWOperatingSystemInfo(const SWOperatingSystemInfo &other);

		/// Assignment operator
		SWOperatingSystemInfo &	operator=(const SWOperatingSystemInfo &other);


		/// Return the O/S family (e.g. Unix, Windows, etc)
		OSFamily	family() const		{ return (OSFamily)getUInt32(Family); }
		OSType		type() const		{ return (OSType)getUInt32(Type); }


public: // STATIC methods

		/**
		*** @brief	Gets the O/S information for the local system.
		**/
		static sw_status_t	getMachineInfo(SWOperatingSystemInfo &sysinfo);
		};



#endif
