/**
*** @file		SWHardwareInfo.h
*** @brief		Hardware information object.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWHardwareInfo class.
**/

/*********************************************************************************
Copyright (c) 2006-2016 SwiftTec Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWHardwareInfo_h__
#define __SWHardwareInfo_h__

#include <swift/SWInfoObject.h>

SWIFT_NAMESPACE_BEGIN

// Forward declarations

/**
*** @brief	A class to represent the information about a system/machine/host.
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWHardwareInfo : public SWInfoObject
		{
public:
		/// An enumeration of information fields
		enum HWProcessorFamily
			{
			HW_PROCESSOR_FAMILY_UNKNOWN=0,

			HW_PROCESSOR_FAMILY_ALPHA,
			HW_PROCESSOR_FAMILY_AMD64,
			HW_PROCESSOR_FAMILY_IA64,
			HW_PROCESSOR_FAMILY_MIPS,
			HW_PROCESSOR_FAMILY_POWERMAC,
			HW_PROCESSOR_FAMILY_POWERPC,
			HW_PROCESSOR_FAMILY_SPARC,
			HW_PROCESSOR_FAMILY_X86,

			HW_PROCESSOR_FAMILY_MAX
			};


		/// An enumeration used to index values
		enum HWValueIndex
			{
			ProcessorFamily=0,
			ProcessorArchitecture,
			ProcessorName,
			ProcessorInstructionSetList,
			Platform,
			Provider,
			SerialNo,

			IsWorkstation,
			IsServer,
			IsDomainController,

			HWValueIndexMax
			};


public:
		/// Default constructor
		SWHardwareInfo();

		/// Virtual destrcutor
		virtual ~SWHardwareInfo();

		/// Copy constructor
		SWHardwareInfo(const SWHardwareInfo &other);

		/// Assignment operator
		SWHardwareInfo &	operator=(const SWHardwareInfo &other);


public: // STATIC methods

		/**
		*** @brief	Gets the H/W information for the local system.
		**/
		static sw_status_t	getMachineInfo(SWHardwareInfo &sysinfo);
		};

SWIFT_NAMESPACE_END

#endif
