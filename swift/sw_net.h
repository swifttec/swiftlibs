/**
*** @file		sw_net.h
*** @brief		A collection of useful network utility functions
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains a collection of useful network utility functions.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#ifndef __sw_net_h__
#define __sw_net_h__

#include <swift/swift.h>

#include <swift/SWString.h>
#include <swift/SWException.h>


#if defined(SW_PLATFORM_WINDOWS)
	#include <winsock2.h>
#else
	#include <netinet/in.h>
#endif




/**
*** @brief Initialise the network layer
***
*** Initialises the network layer (as required by some platforms - notably winsock on windows)
*** to prepare for network operations. This is called internally by the network classes so it
*** is not necessary for this to be called by the developer, but it can be called at any point
*** if desired.
**/
SWIFT_DLL_EXPORT sw_status_t			sw_net_init(bool stopOnFail=true);

/// @brief Returns the status of the network layer initialisation.
SWIFT_DLL_EXPORT bool					sw_net_initialised();

/**
*** @brief Returns the last error returned by the network layer.
***
*** This returns the last status code returned by the operating system
*** network layer. It is required for operating systems such as windows
*** which have an independent error status for the network layer.
*** Under other operating systems this simply returns the value of errno.
**/
SWIFT_DLL_EXPORT sw_status_t			sw_net_getLastError();




/// @brief Convert from a host (platform-specific) to a network (platform-independent) long (32-bit integer)
SWIFT_DLL_EXPORT sw_uint32_t			sw_net_htonl(sw_uint32_t v);

/// @brief Convert from a network (platform-independent) to a host (platform-specific) long (32-bit integer)
SWIFT_DLL_EXPORT sw_uint32_t			sw_net_ntohl(sw_uint32_t v);

/// @brief Convert from a host (platform-specific) to a network (platform-independent) short (16-bit integer)
SWIFT_DLL_EXPORT sw_uint16_t			sw_net_htons(sw_uint16_t v);

/// @brief Convert from a network (platform-independent) to a host (platform-specific) short (16-bit integer)
SWIFT_DLL_EXPORT sw_uint16_t			sw_net_ntohs(sw_uint16_t v);


/// @brief Convert a string to an IPv4
SWIFT_DLL_EXPORT unsigned long			sw_net_inet_addr(const char* cp);

/// @brief Return the local hostname
SWIFT_DLL_EXPORT SWString				sw_net_localhostname();


/// @brief Get network host entry by name
SWIFT_DLL_EXPORT struct hostent *		sw_net_gethostbyname(const char *name);

/// @brief Get network host entry by address
SWIFT_DLL_EXPORT struct hostent *		sw_net_gethostbyaddr(const void *addr, socklen_t len, int type);

/*
// Perform a "ping" to the given host
SWIFT_DLL_EXPORT int					sw_net_ping(const SWString &host, int retries=5, int timeout=5000, int nbytes=32);
SWIFT_DLL_EXPORT int					sw_net_ping(SWHostname &host, int retries=5, int timeout=5000, int nbytes=32);
SWIFT_DLL_EXPORT int					sw_net_ping(SWIPAddress &host, int retries=5, int timeout=5000, int nbytes=32);

// Address related functions
SWIFT_DLL_EXPORT bool				sw_net_isIPAddress(const SWString &host);
SWIFT_DLL_EXPORT SWString			sw_net_hostname(const SWString &host);

// Returns local server information
SWIFT_DLL_EXPORT bool				sw_net_isInWindowsDomain(SWString *pDomainName=0);
SWIFT_DLL_EXPORT bool				sw_net_isInWindowsWorkgroup(SWString *pWorkgroupName=0);
*/

SW_DEFINE_EXCEPTION(SWNetException, SWException);




#endif
