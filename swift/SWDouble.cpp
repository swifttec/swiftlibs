/**
*** @file		SWDouble.cpp
*** @brief		A representation of date based on Julian day numbers
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWDouble class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <swift/SWDouble.h>
#include <swift/sw_printf.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





// Construct with today's date
SWDouble::SWDouble(double amount) :
	m_amount(amount)
		{
		}


SWDouble::SWDouble(int amount) :
	m_amount((double)amount)
		{
		}


SWDouble::SWDouble(const char *s)
		{
		*this = s;
		}


SWDouble &
SWDouble::operator=(const char *s)
		{
		m_amount = atof(s);

		return *this;
		}


SWDouble::SWDouble(const wchar_t *s)
		{
		*this = s;
		}


SWDouble &
SWDouble::operator=(const wchar_t *s)
		{
		m_amount = wtof(s);

		return *this;
		}


SWDouble::SWDouble(const SWString &s)
		{
		*this = s;
		}


SWDouble &
SWDouble::operator=(const SWString &s)
		{
		if (s.charSize() == sizeof(char)) *this = s.c_str();
		else if (s.charSize() == sizeof(wchar_t)) *this = s.w_str();
		else m_amount = 0;

		return *this;
		}


SWString
SWDouble::toString(const SWString &fmt) const
		{
		SWString	s;
		char		nstr[256];

		sw_snprintf(nstr, sizeof(nstr), fmt, m_amount);
		s = nstr;

		return s;
		}




