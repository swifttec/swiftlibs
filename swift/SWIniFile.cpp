/****************************************************************************
**	SWIniFile.C	A class for reading and writing .ini files
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <swift/SWIniFile.h>
#include <swift/SWGuard.h>
#include <swift/SWTokenizer.h>
#include <swift/SWConfigValue.h>
#include <swift/sw_printf.h>
#include <swift/SWCString.h>
#include <swift/SWWString.h>



// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif


SWIniFile::SWIniFile(const SWString &filename, bool readonly, bool woc) :
    SWConfigFile(filename, readonly, woc, true)
		{
		// Load this structure from the file
		if (!filename.isEmpty())
			{
			readFromFile(filename);
			}
		}



// Destructor

SWIniFile::~SWIniFile()
		{
		flush();
		}







/**
*** Loads the config structures from the ini file
**/
bool
SWIniFile::readFromFile(const SWString &filename)
		{
		SWTextFile	f;
		SWString	line;
		SWConfigKey	*key=NULL;
		int		p;

		//TCHAR		*s, tmpstr[1024];
		//SWString	comment;
		//ConfigSection	*section;
		//ConfigEntry	*entry;
		//int		r=0;

		clearKeysAndValues();
		_name = filename;
		if (_name == _T("")) return false;

		m_loading = true;
		m_loaded = false;
		if (f.open(filename, TF_MODE_READ) == SW_STATUS_SUCCESS)
			{
			while (line.readLine(f))
				{
				// Strip off trailing spaces
				// Ignore blank lines and comments
				line.strip(" \t");
				if (line.isEmpty() || line[0] == _T(';') || line[0] == _T('#')) continue;

				if (line[0] == _T('[')) // Start of section - [section]
					{
					if ((p = line.first(_T(']'))) > 0)
						{
						SWTokenizer	tok(line.substring(1, p-1), _T("/\\"));

						key = this;
						while (tok.hasMoreTokens()) key = key->findOrCreateKey(tok.nextToken());
						}
					}
				else	// A value
					{
					// Make sure we have a valid section

					if ((p = line.first(_T('='))) > 0)
						{
						SWString	name, value;
						SWConfigValue	*cv;

						name = line.substring(0, p-1);
						value = decode(line.substring(p+1, (sw_index_t)line.length()-1));

						if (key) cv = key->findOrCreateValue(name);
						else cv = findOrCreateValue(name);

						cv->setValue(value);
						}
					}
				}

			f.close();

			//clearChangedFlag();
			m_loaded = true;
			}

		m_loading = false;

		return true;
		}


/**
*** Loads the config structures from the ini file
**/
bool
SWIniFile::loadFromString(const SWString &text)
		{
		SWTextFile	f;
		SWString	line;
		SWConfigKey	*key=NULL;
		int		p;

		clearKeysAndValues();
		_name.clear();

		m_loading = true;
		m_loaded = false;

		SWTokenizer	tok(text, "\r\n");

		while (tok.hasMoreTokens())
			{
			line = tok.nextToken();

			// Strip off trailing spaces
			// Ignore blank lines and comments
			line.strip(" \t");
			if (line.isEmpty() || line[0] == _T(';')) continue;

			if (line[0] == _T('[')) // Start of section - [section]
				{
				if ((p = line.first(_T(']'))) > 0)
					{
					SWTokenizer	tok(line.substring(1, p-1), _T("/\\"));

					key = this;
					while (tok.hasMoreTokens()) key = key->findOrCreateKey(tok.nextToken());
					}
				}
			else	// A value
				{
				// Make sure we have a valid section
				if (!key) continue;

				if ((p = line.first(_T('='))) > 0)
					{
					SWString	name, value;
					SWConfigValue	*cv;

					name = line.substring(0, p-1);
					value = decode(line.substring(p+1, (sw_index_t)line.length()-1));

					cv = key->findOrCreateValue(name);
					cv->setValue(value);
					}
				}

			//clearChangedFlag();
			m_loaded = true;
			}

		m_loading = false;

		return true;
		}


void
SWIniFile::writeKeyToFile(const SWString &root, SWConfigKey *key, SWTextFile &f)
		{
		SWConfigValue	*cv;
		SWConfigKey	*ck;
		SWString	keypath;
		int		i;

		keypath = root;
		if (!keypath.isEmpty()) keypath += _T("/");
		keypath += key->name();

		// Write out the section
		sw_fprintf(f, "[%s]\n", keypath.c_str());

		// Write out the values
		for (i=0;i<key->valueCount();i++)
			{
			cv = key->getValue(i);
			if (f.encoding() == TF_ENCODING_UTF16_LE || f.encoding() == TF_ENCODING_UTF16_BE)
				{
				SWWString	line;

				line = cv->name();
				line += L"=";
				line += cv->toString();
				f.writeLine(line);
				}
			else
				{
				sw_fprintf(f, "%s=%s\n", cv->name().c_str(), encode(cv->toString()).c_str());
				}
			}
		sw_fprintf(f, "\n");

		// Write out the sub-keys
		for (i=0;i<key->keyCount();i++)
			{
			ck = key->getKey(i);
			writeKeyToFile(keypath, ck, f);
			}
		}


SWString
SWIniFile::writeKeyToString(const SWString &root, SWConfigKey *key)
		{
		SWConfigValue	*cv;
		SWConfigKey	*ck;
		SWString	keypath;
		int		i;

		keypath = root;
		if (!keypath.isEmpty()) keypath += _T("/");
		keypath += key->name();

		// Write out the section
		SWString	line;

		line = "[";
		line += keypath;
		line += "]\n";

		// Write out the values
		for (i=0;i<key->valueCount();i++)
			{
			cv = key->getValue(i);
			line += cv->name();
			line += "=";
			line += encode(cv->toString());
			line += "\n";
			}

		line += "\n";

		// Write out the sub-keys
		for (i=0;i<key->keyCount();i++)
			{
			ck = key->getKey(i);
			line += writeKeyToString(keypath, ck);
			}

		return line;
		}


bool
SWIniFile::writeToFile(const SWString &filename)
		{
		return writeToFile(filename, TF_ENCODING_UTF8);
		}

/**
*** Writes the data structure out to a file
**/
bool
SWIniFile::writeToFile(const SWString &filename, int encoding)
		{
		SWString	bakfile;
		SWTextFile	f;
		int			i;

		if (filename.isEmpty()) return false;

		/*
		// Backup support
		if (_backup)
			{
			bakfile = filename;
			bakfile += _T(".bak");
			unlink(bakfile);
			rename(filename, bakfile);
			}
		*/

		if (f.open(filename, TF_MODE_WRITE|encoding) != SW_STATUS_SUCCESS) return false;

		// First write out my 
		for (i=0;i<keyCount();i++)
			{
			writeKeyToFile(_T(""), getKey(i), f);
			}

		// Write out the values
		SWConfigValue	*cv;

		for (i=0;i<valueCount();i++)
			{
			cv = getValue(i);
			if (f.encoding() == TF_ENCODING_UTF16_LE || f.encoding() == TF_ENCODING_UTF16_BE)
				{
				SWWString	line;

				line = cv->name();
				line += L"=";
				line += cv->toString();
				f.writeLine(line);
				}
			else
				{
				sw_fprintf(f, "%s=%s\n", cv->name().c_str(), encode(cv->toString()).c_str());
				}
			}
		sw_fprintf(f, "\n");

		f.close();
		
		return true;
		}


SWString
SWIniFile::saveToString()
		{
		SWString		res;

		// First write out my 
		for (int i=0;i<keyCount();i++)
			{
			res += writeKeyToString(_T(""), getKey(i));
			}
		
		return res;
		}


SWString
SWIniFile::encode(const SWString &s)
		{
		SWString	res;

		// We have wide chars, we must encode them
		for (int i=0;i<(int)s.length(); i++)
			{
			int	c=s[i];

			if (c == '\n') res += "\\n";
			else if (c == '\t') res += "\\t";
			else if (c == '\r') res += "\\r";
			else if (c >= ' ' && c <= 255)
				{
				res += c;
				if (c == '\\') res += c;
				}
			else
				{
				res += "\\x";

				SWString	ns;

				ns.format("%04x", c&0xffff);
				res += ns;
				}
			}

		return res;
		}


SWString
SWIniFile::decode(const SWString &str)
		{
		SWString	s(str);
		int			cs=sizeof(char);

		if (s.first("\\x") >= 0)
			{
			// Store results in a wchar string
			cs = sizeof(wchar_t);
			}
		else
			{
			cs = (int)str.charSize();
			}

		SWString	res(0, 0, cs, true);

		int		p;

		// We have wide chars, we must encode them
		while ((p = s.first('\\')) >= 0)
			{
			res += s.left(p);
			s.remove(0, p+1);

			if (s.length())
				{
				switch (s[0])
					{
					case 'n':
						res += '\n';
						s.remove(0, 1);
						break;

					case 't':
						res += '\t';
						s.remove(0, 1);
						break;

					case 'r':
						res += '\r';
						s.remove(0, 1);
						break;

					case '\\':
						res += '\\';
						s.remove(0, 1);
						break;

					case 'x':
						res += s.mid(1, 4).toInt32(16);
						s.remove(0, 5);
						break;
					}
				}
			}

		res += s;

		return res;
		}

