/**
*** @file		SWSocketAddress.cpp
*** @brief		A object to hold the address for a socket
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWSocketAddress class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




#include "stdafx.h"

#include <swift/SWSocketAddress.h>
#include <swift/sw_net.h>

#if !defined(SW_PLATFORM_WINDOWS)
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>
	#include <netdb.h>
#else
	#if defined(SW_BUILD_MFC)
		#include <winsock2.h>
	#endif
#endif


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




// This function is imported from SWHostname.cpp, but we don't put it in a header file so we
// don't end up with it being made public.
struct hostent *	internal_gethostbyname_r(const char *name, struct hostent *result, char *buffer, int buflen, int *h_errnop);



SWSocketAddress::SWSocketAddress()
		{
		clear();
		}

SWSocketAddress::~SWSocketAddress()
		{
		clear();
		}


SWSocketAddress::SWSocketAddress(const SWString &hostname, int port)
		{
		clear();

		_len = sizeof(struct sockaddr_in);
		_type = IP4;
		
		struct sockaddr_in	*paddr=(struct sockaddr_in *)&_addr;

		paddr->sin_port = htons((sw_ushort_t)port);
		paddr->sin_family = AF_INET;

		if (hostname.length())
			{
			struct hostent	*hp=0, he;
			char			hbuf[1024];
			int				herrno;

			hp = internal_gethostbyname_r(hostname, &he, hbuf, sizeof(hbuf), &herrno);

			if (hp != NULL)
				{
				memcpy(&paddr->sin_addr, hp->h_addr_list[0], hp->h_length);
				}
			else
				{
				clear();
				}
			}
		else
			{
#if defined(SW_PLATFORM_UNIX)
			paddr->sin_addr.s_addr = INADDR_ANY;
#else
			paddr->sin_addr.S_un.S_addr = INADDR_ANY;
#endif
			}
		}


SWSocketAddress::SWSocketAddress(const SWSocketAddress &other)
		{
		*this = other;
		}


SWSocketAddress &
SWSocketAddress::operator=(const SWSocketAddress &other)
		{
		memcpy(&_addr, &other._addr, sizeof(_addr));
		_len = other._len;
		_type = other._type;

		return *this;
		}


void
SWSocketAddress::clear()
		{
		memset(&_addr, 0, sizeof(_addr));
		_len = 0;
		_type = Unknown;
		}



/*
SWSocketAddress &
SWSocketAddress::operator=(const struct sockaddr_in &addr)
		{
		set(&addr, sizeof(addr));

		return *this;
		}
*/



void
SWSocketAddress::set(const void *pVAddr, size_t len)
		{
		const struct sockaddr *pAddr=(const struct sockaddr *)pVAddr;
		
		clear();

		AddressType	type=Unknown;

		if (pAddr->sa_family == AF_INET) type = IP4;

		if (type == Unknown) throw SW_EXCEPTION_TEXT(SWException, "Unknown address family");

		_type = type;
		memcpy(&_addr, pAddr, len);
		_len = len;
		}



SWIPAddress
SWSocketAddress::toIPAddress() const
		{
		if (_type != IP4 /* && _type != IP6 */)
			{
			throw SW_EXCEPTION(SWException);
			}

		struct sockaddr_in	*paddr=(struct sockaddr_in *)&_addr;

#if defined(SW_PLATFORM_WINDOWS)
		SWIPAddress	ipaddr(paddr->sin_addr.S_un.S_addr);
#elif defined(SW_PLATFORM_UNIX)
		SWIPAddress	ipaddr(paddr->sin_addr.s_addr);
#else
		SWIPAddress	ipaddr(paddr->sin_addr._S_un._S_addr);
#endif

		return ipaddr;
		}



int
SWSocketAddress::toIPPort() const
		{
		if (_type != IP4 /* && _type != IP6 */)
			{
			throw SW_EXCEPTION(SWException);
			}

		struct sockaddr_in	*paddr=(struct sockaddr_in *)&_addr;

		return ntohs(paddr->sin_port);
		}


