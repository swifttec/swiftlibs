/**
*** @file		SWSystemInfo.h
*** @brief		System information object.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWSystemInfo class.
**/

/*********************************************************************************
Copyright (c) 2006-2016 SwiftTec Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWSystemInfo_h__
#define __SWSystemInfo_h__

#include <swift/SWInfoObject.h>
#include <swift/SWOperatingSystemInfo.h>
#include <swift/SWHardwareInfo.h>

SWIFT_NAMESPACE_BEGIN

// Forward declarations

/**
*** @brief	A class to represent the information about a system/hardware/host.
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWSystemInfo : public SWInfoObject
		{
public:
		/// An enumeration of information fields
		enum SIValueIndex
			{
			HostName=0,
			HostID,

			SIValueIndexMax
			};


public:
		/// Default constructor - constructs an empty value of type VtNone
		SWSystemInfo();

		/// Virtual destrcutor
		virtual ~SWSystemInfo();

		/// Copy constructor
		SWSystemInfo(const SWSystemInfo &other);

		/// Assignment operator
		SWSystemInfo &	operator=(const SWSystemInfo &other);


		/// Return the OS information.
		const SWOperatingSystemInfo &	OS() const		{ return m_os; }

		/// Return the local hardware information.
		const SWHardwareInfo &			hardware() const	{ return m_hardware; }

		/// Return the network information.
		//const SWNetworkInfo &			network() const	{ return m_network; }

public: // STATIC methods

		/**
		*** @brief	Gets the system information for the local system.
		**/
		static sw_status_t	getLocalInfo(SWSystemInfo &sysinfo);

protected:
		SWOperatingSystemInfo	m_os;		///< Information about the operating system (type, version, etc)
		SWHardwareInfo			m_hardware;	///< Information about the hardware (processor, memory, etc)
		//SWNetworkInfo			m_network;	///< Information about the network configuration.
		};

SWIFT_NAMESPACE_END

#endif
