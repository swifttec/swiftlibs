/**
*** @file		SWCondVar.cpp
*** @brief		A class to deal with conditional variables for thread synchronisation.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWCondVar class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <swift/SWCondVar.h>




// Constructor
SWCondVar::SWCondVar(int type, sw_mutex_t hMutex)
		{
		m_hcv = sw_condvar_create_ex(type, hMutex);
		}


/// Convienence macro to convert the SWCondVar handle to a pointer to the CV class
#define THIS_CV	((CV *)m_hcv)

// Destructor
SWCondVar::~SWCondVar()
		{
		if (m_hcv != NULL)
			{
			close();
			sw_condvar_destroy(m_hcv);
			m_hcv = NULL;
			}
		}


// Close this var
void
SWCondVar::close()
		{
		if (m_hcv != NULL)
			{
			sw_condvar_close(m_hcv);
			}
		}


// Signal NEXT thread waiting on this condition
void
SWCondVar::signal()
		{
		sw_status_t	res=SW_STATUS_INVALID_HANDLE;

		if (m_hcv != NULL)
			{
			res = sw_condvar_signal(m_hcv);
			}

		if (res == SW_STATUS_CONDVAR_CLOSED)
			{
			throw SW_EXCEPTION(SWCondVarInactiveException);
			}
		}


// Signal ALL threads waiting on this condition
void 
SWCondVar::broadcast()
		{
		sw_status_t	res=SW_STATUS_INVALID_HANDLE;

		if (m_hcv != NULL)
			{
			res = sw_condvar_broadcast(m_hcv);
			}

		if (res == SW_STATUS_CONDVAR_CLOSED)
			{
			throw SW_EXCEPTION(SWCondVarInactiveException);
			}
		}


/*
** Wait for a condition to occur on our variable
**
** @param	waitms	The time in millisecons to wait before timing out.
**
** @return	0=timeout, <0=error, >0 success
*/
sw_status_t
SWCondVar::wait(sw_uint64_t waitms)
		{
		sw_status_t	res=SW_STATUS_INVALID_HANDLE;

		if (m_hcv != NULL)
			{
			res = sw_condvar_timedWait(m_hcv, waitms);
			}

		if (res == SW_STATUS_CONDVAR_CLOSED)
			{
			throw SW_EXCEPTION(SWCondVarInactiveException);
			}

		return res;
		}


void
SWCondVar::acquire()
		{
		sw_status_t	res=SW_STATUS_INVALID_HANDLE;

		if (m_hcv != NULL)
			{
			res = sw_condvar_acquireMutex(m_hcv);
			}

		if (res == SW_STATUS_CONDVAR_CLOSED)
			{
			throw SW_EXCEPTION(SWCondVarInactiveException);
			}
		}

void
SWCondVar::release()
		{
		sw_status_t	res=SW_STATUS_INVALID_HANDLE;

		if (m_hcv != NULL)
			{
			res = sw_condvar_releaseMutex(m_hcv);
			}

		if (res == SW_STATUS_CONDVAR_CLOSED)
			{
			throw SW_EXCEPTION(SWCondVarInactiveException);
			}
		}


bool
SWCondVar::hasLock()
		{
		bool	res=false;

		if (m_hcv != NULL)
			{
			res = sw_condvar_mutexHasLock(m_hcv);
			}

		return res;
		}

/// Return a pointer to the mutex used for thread locking
// SWMutex *			getMutex() const	{ return m_pMutex; }



