/**
*** @file		SWAppConfig.h
*** @brief		Generic application config
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWAppConfig class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#ifndef __swift_SWAppConfig_h__
#define __swift_SWAppConfig_h__

#include <swift/SWStringArray.h>
#include <swift/SWDataObject.h>
#include <swift/SWRegistryKey.h>

class SWIFT_DLL_EXPORT SWAppConfig : public SWDataObject
		{
public:
		/// The folder IDs
		enum FolderID
			{
			InstallDir=0,
			SystemGlobalDataDir,
			SystemUserDataDir,
			GlobalDataDir,
			LocalDataDir,
			LogDir,
			UserDir,
			UserDataDir,
			UserDocsDir,
			UserResourcesDir,
			ProgramResourcesDir,
			ProgramDir,
			HelpDir,
			TempDir,

			// Last one
			MaxFolderID
			};

		enum FormatType
			{
			FMT_SWDate,
			FMT_SWTime
			};

		enum DateHint
			{
			HINT_NORMAL,
			HINT_PAST,
			HINT_FUTURE
			};

public:
		/// Constructor
		SWAppConfig();

		/// Constructor
		SWAppConfig(const SWString &company, const SWString &product, bool useCompanyForDataDir=true);
		
		///< Destructor
		~SWAppConfig();

#if defined(SW_PLATFORM_WINDOWS)
		/// initialise the config - windows specific version
		void					init(const SWString &cmdline);
#endif

		/// initialise the config - all programs must call one form of init
		void					init(int argc, char **argv);

		/// initialise the config - all programs must call one form of init
		void					init(const SWString &prog, const SWStringArray &args);

		/// Test to see if the config has been loaded
		bool					isInitialised() const			{ return (m_initstatus == 1); }

		/// Returns the product name
		SWString				getProductName()				{ return getString("PRODUCT_NAME"); }

		/// Returns the company name from the product info
		SWString				getCompanyName()				{ return getString("PRODUCT_COMPANY"); }

		/// Return the company URL from the product info
		SWString				getCompanyURL() const			{return getString("PRODUCT_COMPANY_URL"); }

		/// Return the company website from the product info
		SWString				getCompanyWebsite() const;

		/// Set the general data dir
		void					setDataDir(const SWString &filename, bool create=true);

		/// Gets the version string for the given prefix (PRODUCT or PROGRAM)
		SWString 				getVersionString(const SWString &prefix, const SWString &fmt="v%m.%n") const;

		/// Sets the give folder path
		void					setFolder(int idx, const SWString &path, bool create=true, bool grantEverybodyAccessOnCreate=false, bool onlyIfNotAlreadySet=false);

		/// Get the folder path
		SWString				getFolder(int idx, const SWString &subfolder="") const;

		/// Get the folder name
		SWString				getFolderName(int idx) const;

		const SWString &		getRegistryKey() const		{ return m_regKey; }

		SWString				getRegistryEncryptedString(const SWString &section, const SWString &param, const SWString &defval="", SWRegistryHive pdk=SW_REGISTRY_CURRENT_USER) const;
		void					setRegistryEncryptedString(const SWString &section, const SWString &param, const SWString &value, SWRegistryHive pdk=SW_REGISTRY_CURRENT_USER);

		SWString				getRegistryString(const SWString &section, const SWString &param, const SWString &defval="", SWRegistryHive pdk=SW_REGISTRY_CURRENT_USER) const;
		void					setRegistryString(const SWString &section, const SWString &param, const SWString &value, SWRegistryHive pdk=SW_REGISTRY_CURRENT_USER);

		int						getRegistryInteger(const SWString &section, const SWString &param, int defval=0, SWRegistryHive pdk=SW_REGISTRY_CURRENT_USER) const;
		void					setRegistryInteger(const SWString &section, const SWString &param, int value, SWRegistryHive pdk=SW_REGISTRY_CURRENT_USER);

		bool					getRegistryBoolean(const SWString &section, const SWString &param, bool defval=false, SWRegistryHive pdk=SW_REGISTRY_CURRENT_USER) const;
		void					setRegistryBoolean(const SWString &section, const SWString &param, bool value, SWRegistryHive pdk=SW_REGISTRY_CURRENT_USER);

		double					getRegistryDouble(const SWString &section, const SWString &param, double defval=0.0, SWRegistryHive pdk=SW_REGISTRY_CURRENT_USER) const;
		void					setRegistryDouble(const SWString &section, const SWString &param, double value, SWRegistryHive pdk=SW_REGISTRY_CURRENT_USER);

		void					removeRegistryValue(const SWString &section, const SWString &param, SWRegistryHive pdk=SW_REGISTRY_CURRENT_USER);
		void					removeRegistryKey(const SWString &section, SWRegistryHive pdk=SW_REGISTRY_CURRENT_USER);

		/// Get the date format string (overrides locale)
		SWString				getDateFormat(FormatType style=FMT_SWDate) const;

		/// Get the time format string (overrides locale)
		SWString				getTimeFormat(FormatType style=FMT_SWDate) const;

		/// Get the timestamp format string (overrides locale)
		SWString				getTimestampFormat(FormatType style=FMT_SWDate) const;

		/// Get the current symbol string
		SWString				getCurrencyString() const;

		/// Add the given path to the list of resource folders
		void					addResourceFolder(const SWString &path)				{ m_resourceFolderList.add(path); }

		/// Add the given path to the list of resource folders
		void					getResourceFolderList(SWStringArray &list);

		/// Get the resource file from the program or user resources folder
		virtual SWString		getResourceFile(const SWString &filename) const;

		/// Get the resource file from the specified resources folder
		virtual SWString		getResourceFile(const SWString &filename, const SWString &resourcedir) const;

		/// Get a copy of the program arguments
		void					getProgramArgs(SWStringArray &args);

		/// Get a copy of the program arguments
		const SWString &		getProgramPath() const								{ return m_programPath; }

public:	// Useful short hand methods

		/// Returns the users home or profile folder
		SWString				getUserDir(const SWString &subfolder="") const		{ return getFolder(UserDir, subfolder); }

		/// Returns the general (global) data dir
		SWString				getDataDir(const SWString &subfolder="") const		{ return getFolder(GlobalDataDir, subfolder); }

		/// Returns the general (global) data dir
		SWString				getLocalDataDir(const SWString &subfolder="") const	{ return getFolder(LocalDataDir, subfolder); }

		/// Returns the users data folder (app specific)
		SWString				getUserDataDir(const SWString &subfolder="") const	{ return getFolder(UserDataDir, subfolder); }

		/// Returns the users documents folder
		SWString				getUserDocsDir(const SWString &subfolder="") const	{ return getFolder(UserDocsDir, subfolder); }

		/// Return the install dir
		SWString				getInstallDir(const SWString &subfolder="") const	{ return getFolder(InstallDir, subfolder); }

		/// Return the logs dir
		SWString				getLogDir(const SWString &subfolder="") const		{ return getFolder(LogDir, subfolder); }

		/// Return the help dir
		SWString				getHelpPath(const SWString &subfolder="") const		{ return getFolder(HelpDir, subfolder); }

		/// Return the resources dir
		SWString				getProgramResourcesDir(const SWString &subfolder="") const	{ return getFolder(ProgramResourcesDir, subfolder); }

		/// Return the temp dir
		SWString				getTempDir(const SWString &subfolder="") const		{ return getFolder(TempDir, subfolder); }

		/// Return the program dir
		SWString				getProgDir(const SWString &subfolder="") const		{ return getFolder(ProgramDir, subfolder); }


protected: // Methods

		/// Set the command line info
		virtual void			setCommandLine(const SWString &prog, const SWStringArray &args);

		/// Called after the config has finished loaded for any app-specfic pre-init processing
		virtual void			onBeforeInit();

		/// Called after the config has finished loaded for any app-specfic post-init processing
		virtual void			onAfterInit();

protected:
		/// Check to see if the config has been initialised and throw an exception if not
		void					checkConfigInitialised() const;

		/// Called in constructor to do basic initialisation
		void					initDefaults(const SWString &company, const SWString &product, bool useCompanyForDataDir);

protected:
		int						m_initstatus;			///< Indicate the config initialisation status (0=not initialised, 1=initialised, 2=initialising)
		SWString				m_programPath;			///< The program path (executeable)
		SWStringArray			m_programArgs;			///< The command line arguments
		SWString				m_regKey;				///< The registry key
		SWStringArray			m_folderList;			///< An array of folder paths
		bool					m_useCompanyForDataDir;	///< If true use the company name as part of the data folder paths
		SWStringArray			m_resourceFolderList;	///< A list of folders to search for resource files
		};

#endif // __swift_SWAppConfig_h__
