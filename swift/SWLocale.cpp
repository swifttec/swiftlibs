/**
*** @file		SWLocale.cpp
*** @brief		A class representing locale information
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWLocale class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



// Needed for precompiled header support
#include "stdafx.h"

#include <swift/swift.h>
#include <swift/SWLocale.h>
#include <swift/SWException.h>

#include <time.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





// Special table for loading locales
typedef struct
	{
	const char		*langname;
	const char		*sublangname;
	const char		*langcode;
	const char		*sublangcode;
	sw_uint32_t		lcid;
	sw_uint32_t		codepage;
	} LocaleEntry;

static LocaleEntry	localeList[] =
	{
	// Language,					Sub-language			LCID	Codepage
	{ "Afrikaans",						"",						"af",	"",		0x0436,	1252	},
	{ "Albanian",						"",						"sq",	"",		0x041C,	1250	},
	{ "Amharic",						"",						"am",	"",		0x045E,	0		},
	{ "Arabic",							"Algeria",				"ar",	"dz",	0x1401,	1256	},
	{ "Arabic",							"Bahrain",				"ar",	"bh",	0x3C01,	1256	},
	{ "Arabic",							"Egypt",				"ar",	"eg",	0x0C01,	1256	},
	{ "Arabic",							"Iraq",					"ar",	"iq",	0x0801,	1256	},
	{ "Arabic",							"Jordan",				"ar",	"jo",	0x2C01,	1256	},
	{ "Arabic",							"Kuwait",				"ar",	"kw",	0x3401,	1256	},
	{ "Arabic",							"Lebanon",				"ar",	"lb",	0x3001,	1256	},
	{ "Arabic",							"Libya",				"ar",	"ly",	0x1001,	1256	},
	{ "Arabic",							"Morocco",				"ar",	"ma",	0x1801,	1256	},
	{ "Arabic",							"Oman",					"ar",	"om",	0x2001,	1256	},
	{ "Arabic",							"Qatar",				"ar",	"qa",	0x4001,	1256	},
	{ "Arabic",							"Saudi Arabia",			"ar",	"sa",	0x0401,	1256	},
	{ "Arabic",							"Syria",				"ar",	"sy",	0x2801,	1256	},
	{ "Arabic",							"Tunisia",				"ar",	"tn",	0x1C01,	1256	},
	{ "Arabic",							"United Arab Emirates",	"ar",	"ae",	0x3801,	1256	},
	{ "Arabic",							"Yemen",				"ar",	"ye",	0x2401,	1256	},
	{ "Armenian",						"",						"hy",	"",		0x042B,	0		},
	{ "Assamese",						"",						"as",	"",		0x044D,	0		},
	{ "Azeri",							"Cyrillic",				"az",	"az",	0x082C,	1251	},
	{ "Azeri",							"Latin",				"az",	"az",	0x042C,	1254	},
	{ "Basque",							"",						"eu",	"",		0x042D,	1252	},
	{ "Belarusian",						"",						"be",	"",		0x0423,	1251	},
	{ "Bengali",						"Bangladesh",			"bn",	"",		0x0845,	0		},
	{ "Bengali",						"India",				"bn",	"",		0x0445,	0		},
	{ "Bosnian",						"",						"bs",	"",		0x141A,	0		},
	{ "Bulgarian",						"",						"bg",	"",		0x0402,	1251	},
	{ "Burmese",						"",						"my",	"",		0x0455,	0		},
	{ "Catalan",						"",						"ca",	"",		0x0403,	1252	},
	{ "Chinese",						"China",				"zh",	"cn",	0x0804,	0		},
	{ "Chinese",						"Hong Kong SAR",		"zh",	"hk",	0x0C04,	0		},
	{ "Chinese",						"Macau SAR",			"zh",	"mo",	0x1404,	0		},
	{ "Chinese",						"Singapore",			"zh",	"sg",	0x1004,	0		},
	{ "Chinese",						"Taiwan",				"zh",	"tw",	0x0404,	0		},
	{ "Croatian",						"",						"hr",	"",		0x041A,	1250	},
	{ "Czech",							"",						"cs",	"",		0x0405,	1250	},
	{ "Danish",							"",						"da",	"",		0x0406,	1252	},
	{ "Divehi; Dhivehi; Maldivian",		"",						"dv",	"",		0x0465,	0		},
	{ "Dutch",							"Belgium",				"nl",	"be",	0x0813,	1252	},
	{ "Dutch",							"Netherlands",			"nl",	"nl",	0x0413,	1252	},
	{ "Edo",							"",						"",		"",		0x0466,	0		},
	{ "English",						"Australia",			"en",	"au",	0x0C09,	1252	},
	{ "English",						"Belize",				"en",	"bz",	0x2809,	1252	},
	{ "English",						"Canada",				"en",	"ca",	0x1009,	1252	},
	{ "English",						"Caribbean",			"en",	"cb",	0x2409,	1252	},
	{ "English",						"Great Britain",		"en",	"gb",	0x0809,	1252	},
	{ "English",						"India",				"en",	"in",	0x4009,	0		},
	{ "English",						"Ireland",				"en",	"ie",	0x1809,	1252	},
	{ "English",						"Jamaica",				"en",	"jm",	0x2009,	1252	},
	{ "English",						"New Zealand",			"en",	"nz",	0x1409,	1252	},
	{ "English",						"Phillippines",			"en",	"ph",	0x3409,	1252	},
	{ "English",						"Southern Africa",		"en",	"za",	0x1C09,	1252	},
	{ "English",						"Trinidad",				"en",	"tt",	0x2C09,	1252	},
	{ "English",						"United States",		"en",	"us",	0x0409,	1252	},
	{ "English",						"Zimbabwe",				"en",	"",		0x3009,	1252	},
	{ "Estonian",						"",						"et",	"",		0x0425,	1257	},
	{ "Faroese",						"",						"fo",	"",		0x0438,	1252	},
	{ "Farsi",							"Persian",				"fa",	"",		0x0429,	1256	},
	{ "Filipino",						"",						"",		"",		0x0464,	0		},
	{ "Finnish",						"",						"fi",	"",		0x040B,	1252	},
	{ "French",							"Belgium",				"fr",	"be",	0x080C,	1252	},
	{ "French",							"Cameroon",				"fr",	"",		0x2C0C,	0		},
	{ "French",							"Canada",				"fr",	"ca",	0x0C0C,	1252	},
	{ "French",							"Congo",				"fr",	"",		0x240C,	0		},
	{ "French",							"Cote d'Ivoire",		"fr",	"",		0x300C,	0		},
	{ "French",							"France",				"fr",	"fr",	0x040C,	1252	},
	{ "French",							"Luxembourg",			"fr",	"lu",	0x140C,	1252	},
	{ "French",							"Mali",					"fr",	"",		0x340C,	0		},
	{ "French",							"Monaco",				"fr",	"",		0x180C,	1252	},
	{ "French",							"Morocco",				"fr",	"",		0x380C,	0		},
	{ "French",							"Senegal",				"fr",	"",		0x280C,	0		},
	{ "French",							"Switzerland",			"fr",	"ch",	0x100C,	1252	},
	{ "French",							"West Indies",			"fr",	"",		0x1C0C,	0		},
	{ "Frisian",						"Netherlands",			"",		"",		0x0462,	0		},
	{ "FYRO Macedonia",					"",						"mk",	"",		0x042F,	1251	},
	{ "Gaelic",							"Ireland",				"gd",	"ie",	0x083C,	0		},
	{ "Gaelic",							"Scotland",				"gd",	"",		0x043C,	0		},
	{ "Galician",						"",						"gl",	"",		0x0456,	1252	},
	{ "Georgian",						"",						"ka",	"",		0x0437,	0		},
	{ "German",							"Austria",				"de",	"at",	0x0C07,	1252	},
	{ "German",							"Germany",				"de",	"de",	0x0407,	1252	},
	{ "German",							"Liechtenstein",		"de",	"li",	0x1407,	1252	},
	{ "German",							"Luxembourg",			"de",	"lu",	0x1007,	1252	},
	{ "German",							"Switzerland",			"de",	"ch",	0x0807,	1252	},
	{ "Greek",							"",						"el",	"",		0x0408,	1253	},
	{ "Guarani",						"Paraguay",				"gn",	"",		0x0474,	0		},
	{ "Gujarati",						"",						"gu",	"",		0x0447,	0		},
	{ "Hebrew",							"",						"he",	"",		0x040D,	1255	},
	{ "Hindi",							"",						"hi",	"",		0x0439,	0		},
	{ "Hungarian",						"",						"hu",	"",		0x040E,	1250	},
	{ "Icelandic",						"",						"is",	"",		0x040F,	1252	},
	{ "Igbo",							"Nigeria",				"",		"",		0x0470,	0		},
	{ "Indonesian",						"",						"id",	"",		0x0421,	1252	},
	{ "Italian",						"Italy",				"it",	"it",	0x0410,	1252	},
	{ "Italian",						"Switzerland",			"it",	"ch",	0x0810,	1252	},
	{ "Japanese",						"",						"ja",	"",		0x0411,	0		},
	{ "Kannada",						"",						"kn",	"",		0x044B,	0		},
	{ "Kashmiri",						"",						"ks",	"",		0x0460,	0		},
	{ "Kazakh",							"",						"kk",	"",		0x043F,	1251	},
	{ "Khmer",							"",						"km",	"",		0x0453,	0		},
	{ "Konkani",						"",						"",		"",		0x0457,	0		},
	{ "Korean",							"",						"ko",	"",		0x0412,	0		},
	{ "Kyrgyz",							"Cyrillic",				"",		"",		0x0440,	1251	},
	{ "Lao",							"",						"lo",	"",		0x0454,	0		},
	{ "Latin",							"",						"la",	"",		0x0476,	0		},
	{ "Latvian",						"",						"lv",	"",		0x0426,	1257	},
	{ "Lithuanian",						"",						"lt",	"",		0x0427,	1257	},
	{ "Malay",							"Brunei",				"ms",	"bn",	0x083E,	1252	},
	{ "Malay",							"Malaysia",				"ms",	"my",	0x043E,	1252	},
	{ "Malayalam",						"",						"ml",	"",		0x044C,	0		},
	{ "Maltese",						"",						"mt",	"",		0x043A,	0		},
	{ "Manipuri",						"",						"",		"",		0x0458,	0		},
	{ "Maori",							"",						"mi",	"",		0x0481,	0		},
	{ "Marathi",						"",						"mr",	"",		0x044E,	0		},
	{ "Mongolian",						"",						"mn",	"",		0x0850,	0		},
	{ "Mongolian",						"",						"mn",	"",		0x0450,	1251	},
	{ "Nepali",							"",						"ne",	"",		0x0461,	0		},
	{ "Norwegian",						"Bokml",				"nb",	"no",	0x0414,	1252	},
	{ "Norwegian",						"Nynorsk",				"nn",	"no",	0x0814,	1252	},
	{ "Oriya",							"",						"or",	"",		0x0448,	0		},
	{ "Polish",							"",						"pl",	"",		0x0415,	1250	},
	{ "Portuguese",						"Brazil",				"pt",	"br",	0x0416,	1252	},
	{ "Portuguese",						"Portugal",				"pt",	"pt",	0x0816,	1252	},
	{ "Punjabi",						"",						"pa",	"",		0x0446,	0		},
	{ "Raeto-Romance",					"",						"rm",	"",		0x0417,	0		},
	{ "Romanian",						"Moldova",				"ro",	"mo",	0x0818,	0		},
	{ "Romanian",						"Romania",				"ro",	"",		0x0418,	1250	},
	{ "Russian",						"",						"ru",	"",		0x0419,	1251	},
	{ "Russian",						"Moldova",				"ru",	"mo",	0x0819,	0		},
	{ "Sami Lappish",					"",						"",		"",		0x043B,	0		},
	{ "Sanskrit",						"",						"sa",	"",		0x044F,	0		},
	{ "Serbian",						"Cyrillic",				"sr",	"sp",	0x0C1A,	1251	},
	{ "Serbian",						"Latin",				"sr",	"sp",	0x081A,	1250	},
	{ "Sesotho (Sutu)",					"",						"",		"",		0x0430,	0		},
	{ "Setsuana",						"",						"tn",	"",		0x0432,	0		},
	{ "Sindhi",							"",						"sd",	"",		0x0459,	0		},
	{ "Sinhala/Sinhalese",				"",						"si",	"",		0x045B,	0		},
	{ "Slovak",							"",						"sk",	"",		0x041B,	1250	},
	{ "Slovenian",						"",						"sl",	"",		0x0424,	1250	},
	{ "Somali",							"",						"so",	"",		0x0477,	0		},
	{ "Sorbian",						"",						"sb",	"",		0x042E,	0		},
	{ "Spanish",						"Argentina",			"es",	"ar",	0x2C0A,	1252	},
	{ "Spanish",						"Bolivia",				"es",	"bo",	0x400A,	1252	},
	{ "Spanish",						"Chile",				"es",	"cl",	0x340A,	1252	},
	{ "Spanish",						"Colombia",				"es",	"co",	0x240A,	1252	},
	{ "Spanish",						"Costa Rica",			"es",	"cr",	0x140A,	1252	},
	{ "Spanish",						"Dominican Republic",	"es",	"do",	0x1C0A,	1252	},
	{ "Spanish",						"Ecuador",				"es",	"ec",	0x300A,	1252	},
	{ "Spanish",						"El Salvador",			"es",	"sv",	0x440A,	1252	},
	{ "Spanish",						"Guatemala",			"es",	"gt",	0x100A,	1252	},
	{ "Spanish",						"Honduras",				"es",	"hn",	0x480A,	1252	},
	{ "Spanish",						"Mexico",				"es",	"mx",	0x080A,	1252	},
	{ "Spanish",						"Nicaragua",			"es",	"ni",	0x4C0A,	1252	},
	{ "Spanish",						"Panama",				"es",	"pa",	0x180A,	1252	},
	{ "Spanish",						"Paraguay",				"es",	"py",	0x3C0A,	1252	},
	{ "Spanish",						"Peru",					"es",	"pe",	0x280A,	1252	},
	{ "Spanish",						"Puerto Rico",			"es",	"pr",	0x500A,	1252	},
	{ "Spanish",						"Spain (Traditional)",	"es",	"es",	0x040A,	1252	},
	{ "Spanish",						"Uruguay",				"es",	"uy",	0x380A,	1252	},
	{ "Spanish",						"Venezuela",			"es",	"ve",	0x200A,	1252	},
	{ "Swahili",						"",						"sw",	"",		0x0441,	1252	},
	{ "Swedish",						"Finland",				"sv",	"fi",	0x081D,	1252	},
	{ "Swedish",						"Sweden",				"sv",	"se",	0x041D,	1252	},
	{ "Syriac",							"",						"",		"",		0x045A,	0		},
	{ "Tajik",							"",						"tg",	"",		0x0428,	0		},
	{ "Tamil",							"",						"ta",	"",		0x0449,	0		},
	{ "Tatar",							"",						"tt",	"",		0x0444,	1251	},
	{ "Telugu",							"",						"te",	"",		0x044A,	0		},
	{ "Thai",							"",						"th",	"",		0x041E,	0		},
	{ "Tibetan",						"",						"bo",	"",		0x0451,	0		},
	{ "Tsonga",							"",						"ts",	"",		0x0431,	0		},
	{ "Turkish",						"",						"tr",	"",		0x041F,	1254	},
	{ "Turkmen",						"",						"tk",	"",		0x0442,	0		},
	{ "Ukrainian",						"",						"uk",	"",		0x0422,	1251	},
	{ "Urdu",							"",						"ur",	"",		0x0420,	1256	},
	{ "Uzbek",							"Cyrillic",				"uz",	"uz",	0x0843,	1251	},
	{ "Uzbek",							"Latin",				"uz",	"uz",	0x0443,	1254	},
	{ "Venda",							"",						"",		"",		0x0433,	0		},
	{ "Vietnamese",						"",						"vi",	"",		0x042A,	1258	},
	{ "Welsh",							"",						"cy",	"",		0x0452,	0		},
	{ "Xhosa",							"",						"xh",	"",		0x0434,	0		},
	{ "Yiddish",						"",						"yi",	"",		0x043D,	0		},
	{ "Zulu",							"",						"zu",	"",		0x0435,	0		},

	// Last One
	{ NULL, NULL, NULL, NULL, 0, 0 }
	};

// Static Members
SWLocale		SWLocale::sm_currlocale;
SWLocale		SWLocale::sm_userlocale;
SWLocale		SWLocale::sm_systemlocale;


// Get a reference to the current locale
SWLocale &
SWLocale::getUserLocale()
		{
		if (!sm_userlocale.isValid())
			{
#if defined(SW_PLATFORM_WINDOWS)
			sm_userlocale.load(GetUserDefaultLangID());
#else // defined(SW_PLATFORM_WINDOWS)
#endif // defined(SW_PLATFORM_WINDOWS)
			}

		return sm_userlocale;
		}


SWLocale &
SWLocale::getSystemLocale()
		{
		if (!sm_systemlocale.isValid())
			{
#if defined(SW_PLATFORM_WINDOWS)
			sm_systemlocale.load(GetSystemDefaultLangID());
#else // defined(SW_PLATFORM_WINDOWS)
#endif // defined(SW_PLATFORM_WINDOWS)
			}

		return sm_systemlocale;
		}


SWLocale &
SWLocale::getCurrentLocale()
		{
		if (!sm_currlocale.isValid())
			{
#if defined(SW_PLATFORM_WINDOWS)
			sm_currlocale.load(GetUserDefaultLangID());
#else // defined(SW_PLATFORM_WINDOWS)
			// Short day names
			sm_currlocale.m_shortDayName[0] = _T("Sun");
			sm_currlocale.m_shortDayName[1] = _T("Mon");
			sm_currlocale.m_shortDayName[2] = _T("Tue");
			sm_currlocale.m_shortDayName[3] = _T("Wed");
			sm_currlocale.m_shortDayName[4] = _T("Thu");
			sm_currlocale.m_shortDayName[5] = _T("Fri");
			sm_currlocale.m_shortDayName[6] = _T("Sat");

			// Full day names
			sm_currlocale.m_fullDayName[0] = _T("Sunday");
			sm_currlocale.m_fullDayName[1] = _T("Monday");
			sm_currlocale.m_fullDayName[2] = _T("Tuesday");
			sm_currlocale.m_fullDayName[3] = _T("Wednesday");
			sm_currlocale.m_fullDayName[4] = _T("Thursday");
			sm_currlocale.m_fullDayName[5] = _T("Friday");
			sm_currlocale.m_fullDayName[6] = _T("Saturday");


			// short month names
			sm_currlocale.m_shortMonthName[0] = _T("Jan");
			sm_currlocale.m_shortMonthName[1] = _T("Feb");
			sm_currlocale.m_shortMonthName[2] = _T("Mar");
			sm_currlocale.m_shortMonthName[3] = _T("Apr");
			sm_currlocale.m_shortMonthName[4] = _T("May");
			sm_currlocale.m_shortMonthName[5] = _T("Jun");
			sm_currlocale.m_shortMonthName[6] = _T("Jul");
			sm_currlocale.m_shortMonthName[7] = _T("Aug");
			sm_currlocale.m_shortMonthName[8] = _T("Sep");
			sm_currlocale.m_shortMonthName[9] = _T("Oct");
			sm_currlocale.m_shortMonthName[10] = _T("Nov");
			sm_currlocale.m_shortMonthName[11] = _T("Dec");

			// Full month names
			sm_currlocale.m_fullMonthName[0] = _T("January");
			sm_currlocale.m_fullMonthName[1] = _T("February");
			sm_currlocale.m_fullMonthName[2] = _T("March");
			sm_currlocale.m_fullMonthName[3] = _T("April");
			sm_currlocale.m_fullMonthName[4] = _T("May");
			sm_currlocale.m_fullMonthName[5] = _T("June");
			sm_currlocale.m_fullMonthName[6] = _T("July");
			sm_currlocale.m_fullMonthName[7] = _T("August");
			sm_currlocale.m_fullMonthName[8] = _T("September");
			sm_currlocale.m_fullMonthName[9] = _T("October");
			sm_currlocale.m_fullMonthName[10] = _T("November");
			sm_currlocale.m_fullMonthName[11] = _T("December");

			sm_currlocale.m_dateSeparator = _T("/");
			sm_currlocale.m_timeSeparator = _T(":");
			
			sm_currlocale.m_amStringUpper = _T("AM");
			sm_currlocale.m_amStringLower = _T("am");
			sm_currlocale.m_pmStringUpper = _T("PM");
			sm_currlocale.m_pmStringLower = _T("pm");

			sm_currlocale.m_trueChars = _T("yYtT");
			sm_currlocale.m_trueString = _T("true");
			sm_currlocale.m_falseChars = _T("nNfF");
			sm_currlocale.m_falseString = _T("false");

			// Finally set the validity flag
			sm_currlocale.m_valid = true;
#endif // defined(SW_PLATFORM_WINDOWS)
			}

		return sm_currlocale;
		}



/**
*** Default constructor
**/
SWLocale::SWLocale() :
	m_valid(false),
	m_lcid(0)
		{
		}


/**
*** Copy constructor
**/
SWLocale::SWLocale(const SWLocale &other) :
	SWObject()
		{
		*this = other;
		}


/**
*** Destructor
**/
SWLocale::~SWLocale()
		{
		}


/**
*** Assignment operator
**/
SWLocale &
SWLocale::operator=(const SWLocale &other)
		{
#define COPY(x) x = other.x
		COPY(m_valid);
		COPY(m_lcid);
		COPY(m_langName);
		COPY(m_langCode);
		COPY(m_sublangName);
		COPY(m_sublangCode);
		COPY(m_shortDayName);
		COPY(m_fullDayName);
		COPY(m_shortMonthName);
		COPY(m_fullMonthName);
		COPY(m_dateSeparator);
		COPY(m_dateFormat);
		COPY(m_timeSeparator);
		COPY(m_timeFormat);
		COPY(m_amStringUpper);
		COPY(m_amStringLower);
		COPY(m_pmStringUpper);
		COPY(m_pmStringLower);
		COPY(m_trueChars);
		COPY(m_falseChars);
		COPY(m_trueString);
		COPY(m_falseString);
		COPY(m_currencySymbol);
#undef COPY

		return *this;
		}


sw_status_t
SWLocale::load(sw_uint32_t lcid)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		sw_tchar_t	tmp[1024];

		m_lcid = lcid;

#if defined(LOCALE_SNAME)	&& defined(FINISH_LATER)
	#error Incomplete code
		if (GetLocaleInfo(lcid, LOCALE_SNAME, tmp, sizeof(tmp)))
			{
			// Split and assign tmp
			}
#else
		// Search our internal table
		LocaleEntry	*pEntry=localeList;
		bool		found=false;

		while (pEntry->lcid)
			{
			if (pEntry->lcid == lcid)
				{
				m_langName = pEntry->langname;
				m_langCode = pEntry->langcode;
				m_sublangName = pEntry->sublangname;
				m_sublangCode = pEntry->sublangcode;
				found = true;
				break;
				}

			pEntry++;
			}

#endif

#if defined(SW_PLATFORM_WINDOWS)
/*
1	CAL_GREGORIAN	Gregorian (localized)
2	CAL_GREGORIAN_US	Gregorian (English strings always)
3	CAL_JAPAN	Japanese Emperor Era
4	CAL_TAIWAN	Taiwan calendar
5	CAL_KOREA	Korean Tangun Era
6	CAL_HIJRI	Hijri (Arabic Lunar)
7	CAL_THAI	Thai
8	CAL_HEBREW	Hebrew (Lunar)
9	CAL_GREGORIAN_ME_FRENCH	Gregorian Middle East French
10	CAL_GREGORIAN_ARABIC	Gregorian Arabic
11	CAL_GREGORIAN_XLIT_ENGLISH	Gregorian transliterated English
12	CAL_GREGORIAN_XLIT_FRENCH	Gregorian transliterated French
23	CAL_UMALQURA	Windows Vista and later: Um Al Qura (Arabic lunar) calendar 
*/

		int			calendar=CAL_GREGORIAN;


		{ // BEGIN short day names
		UINT	idlist[] =  { CAL_SABBREVDAYNAME7, CAL_SABBREVDAYNAME1, CAL_SABBREVDAYNAME2, CAL_SABBREVDAYNAME3, CAL_SABBREVDAYNAME4, CAL_SABBREVDAYNAME5, CAL_SABBREVDAYNAME6, 0 };

		for (int i=0;idlist[i];i++)
			{
			GetCalendarInfo(lcid, calendar, idlist[i], tmp, sizeof(tmp), NULL);
			m_shortDayName[i] = tmp;
			}
		} // END short day names

		{ // BEGIN full day names
		UINT	idlist[] =  { CAL_SDAYNAME7, CAL_SDAYNAME1, CAL_SDAYNAME2, CAL_SDAYNAME3, CAL_SDAYNAME4, CAL_SDAYNAME5, CAL_SDAYNAME6, 0 };

		for (int i=0;idlist[i];i++)
			{
			GetCalendarInfo(lcid, calendar, idlist[i], tmp, sizeof(tmp), NULL);
			m_fullDayName[i] = tmp;
			}
		} // END full day names

		{ // BEGIN short day names
		UINT	idlist[] =  { CAL_SABBREVMONTHNAME1, CAL_SABBREVMONTHNAME2, CAL_SABBREVMONTHNAME3, CAL_SABBREVMONTHNAME4, CAL_SABBREVMONTHNAME5, CAL_SABBREVMONTHNAME6,  CAL_SABBREVMONTHNAME7, CAL_SABBREVMONTHNAME8, CAL_SABBREVMONTHNAME9, CAL_SABBREVMONTHNAME10, CAL_SABBREVMONTHNAME11, CAL_SABBREVMONTHNAME12, 0 };

		for (int i=0;idlist[i];i++)
			{
			GetCalendarInfo(lcid, calendar, idlist[i], tmp, sizeof(tmp), NULL);
			m_shortMonthName[i] = tmp;
			}
		} // END short day names

		{ // BEGIN full day names
		UINT	idlist[] =  { CAL_SMONTHNAME1, CAL_SMONTHNAME2, CAL_SMONTHNAME3, CAL_SMONTHNAME4, CAL_SMONTHNAME5, CAL_SMONTHNAME6,  CAL_SMONTHNAME7, CAL_SMONTHNAME8, CAL_SMONTHNAME9, CAL_SMONTHNAME10, CAL_SMONTHNAME11, CAL_SMONTHNAME12, 0 };

		for (int i=0;idlist[i];i++)
			{
			GetCalendarInfo(lcid, calendar, idlist[i], tmp, sizeof(tmp), NULL);
			m_fullMonthName[i] = tmp;
			}
		} // END full day names


		// Date Format String
		m_dateSeparator = _T("/");
		if (GetLocaleInfo(lcid, LOCALE_SDATE, tmp, sizeof(tmp)))
			{
			m_dateSeparator = tmp;
			}

		m_dateFormat.empty();
		if (GetLocaleInfo(lcid, LOCALE_IDATE, tmp, 2))
			{
			if (*tmp == '0')
				{
				// m_dateFormat = "MM/DD/YYYY";
				m_dateFormat += "MM";
				m_dateFormat += m_dateSeparator;
				m_dateFormat += "DD";
				m_dateFormat += m_dateSeparator;
				m_dateFormat += "YYYY";
				}
			else if (*tmp == '1')
				{
				// m_dateFormat = "DD/MM/YYYY";
				m_dateFormat += "DD";
				m_dateFormat += m_dateSeparator;
				m_dateFormat += "MM";
				m_dateFormat += m_dateSeparator;
				m_dateFormat += "YYYY";
				}
			else if (*tmp == '2')
				{
				// m_dateFormat = "YYYY/MM/DD";
				m_dateFormat += "YYYY";
				m_dateFormat += m_dateSeparator;
				m_dateFormat += "MM";
				m_dateFormat += m_dateSeparator;
				m_dateFormat += "DD";
				}
			}
		else
			{
			// m_dateFormat = "DD/MM/YYYY";
			m_dateFormat += "DD";
			m_dateFormat += m_dateSeparator;
			m_dateFormat += "MM";
			m_dateFormat += m_dateSeparator;
			m_dateFormat += "YYYY";
			}


		// Time Format String
		m_timeSeparator = _T(":");
		if (GetLocaleInfo(lcid, LOCALE_STIME, tmp, sizeof(tmp)))
			{
			m_timeSeparator = tmp;
			}

		m_timeFormat.empty();
		m_timeFormat += "HH";
		m_timeFormat += m_timeSeparator;
		m_timeFormat += "mm";
		m_timeFormat += m_timeSeparator;
		m_timeFormat += "ss";
		if (GetLocaleInfo(lcid, LOCALE_STIMEFORMAT, tmp, sizeof(tmp)))
			{
			m_timeFormat = tmp;
			}

		// Currency
		m_currencySymbol = "£";
		if (GetLocaleInfo(lcid, LOCALE_SCURRENCY, tmp, sizeof(tmp)))
			{
			m_currencySymbol = tmp;
			}

#else // defined(SW_PLATFORM_WINDOWS)


#endif // defined(SW_PLATFORM_WINDOWS)

		
		m_amStringUpper = _T("AM");
		m_amStringLower = _T("am");
		m_pmStringUpper = _T("PM");
		m_pmStringLower = _T("pm");

		m_trueChars = _T("yYtT");
		m_trueString = _T("true");
		m_falseChars = _T("nNfF");
		m_falseString = _T("false");

		// Finally set the validity flag
		m_valid = true;

/*
		CAL_ICALINTVALUE	An integer value indicating the calendar type of the alternate calendar.
		CAL_ITWODIGITYEARMAX	Windows Me/98, Windows 2000: An integer value indicating the upper boundary of the two-digit year range.
		CAL_IYEAROFFSETRANGE	One or more null-terminated strings that specify the year offsets for each of the era ranges. The last string has an extra terminating null character. This value varies in format depending on the type of optional calendar.
		CAL_SABBREVDAYNAME1	Abbreviated native name of the first day of the week.
		CAL_SABBREVDAYNAME2	Abbreviated native name of the second day of the week.
		CAL_SABBREVDAYNAME3	Abbreviated native name of the third day of the week.
		CAL_SABBREVDAYNAME4	Abbreviated native name of the fourth day of the week.
		CAL_SABBREVDAYNAME5	Abbreviated native name of the fifth day of the week.
		CAL_SABBREVDAYNAME6	Abbreviated native name of the sixth day of the week.
		CAL_SABBREVDAYNAME7	Abbreviated native name of the seventh day of the week.
		CAL_SABBREVERASTRING	Windows 7 and later: Abbreviated native name of an era. The full era is represented by the CAL_SERASTRING constant.
		CAL_SABBREVMONTHNAME1	Abbreviated native name of the first month of the year.
		CAL_SABBREVMONTHNAME2	Abbreviated native name of the second month of the year.
		CAL_SABBREVMONTHNAME3	Abbreviated native name of the third month of the year.
		CAL_SABBREVMONTHNAME4	Abbreviated native name of the fourth month of the year.
		CAL_SABBREVMONTHNAME5	Abbreviated native name of the fifth month of the year.
		CAL_SABBREVMONTHNAME6	Abbreviated native name of the sixth month of the year.
		CAL_SABBREVMONTHNAME7	Abbreviated native name of the seventh month of the year.
		CAL_SABBREVMONTHNAME8	Abbreviated native name of the eighth month of the year.
		CAL_SABBREVMONTHNAME9	Abbreviated native name of the ninth month of the year.
		CAL_SABBREVMONTHNAME10	Abbreviated native name of the tenth month of the year.
		CAL_SABBREVMONTHNAME11	Abbreviated native name of the eleventh month of the year.
		CAL_SABBREVMONTHNAME12	Abbreviated native name of the twelfth month of the year.
		CAL_SABBREVMONTHNAME13	Abbreviated native name of the thirteenth month of the year, if it exists.
		CAL_SCALNAME	Native name of the alternate calendar.
		CAL_SDAYNAME1	Native name of the first day of the week.
		CAL_SDAYNAME2	Native name of the second day of the week.
		CAL_SDAYNAME3	Native name of the third day of the week.
		CAL_SDAYNAME4	Native name of the fourth day of the week.
		CAL_SDAYNAME5	Native name of the fifth day of the week.
		CAL_SDAYNAME6	Native name of the sixth day of the week.
		CAL_SDAYNAME7	Native name of the seventh day of the week.
		CAL_SERASTRING	One or more null-terminated strings that specify each of the Unicode code points specifying the era associated with CAL_IYEAROFFSETRANGE. The last string has an extra terminating null character. This value varies in format depending on the type of optional calendar.
		CAL_SLONGDATE	Long date formats for the calendar type.
		CAL_SMONTHDAY	Windows 7 and later: Format of the month and day for the calendar type. The formatting is similar to that for CAL_SLONGDATE. For example, if the Month/Day pattern is the full month name followed by the day number with leading zeros, for example, "September 03", the format is "MMMM dd". Single quotation marks can be used to insert non-format characters, for example, 'de' in Spanish.

		Note  This calendar type supports only one format.
		CAL_SMONTHNAME1	Native name of the first month of the year.
		CAL_SMONTHNAME2	Native name of the second month of the year.
		CAL_SMONTHNAME3	Native name of the third month of the year.
		CAL_SMONTHNAME4	Native name of the fourth month of the year.
		CAL_SMONTHNAME5	Native name of the fifth month of the year.
		CAL_SMONTHNAME6	Native name of the sixth month of the year.
		CAL_SMONTHNAME7	Native name of the seventh month of the year.
		CAL_SMONTHNAME8	Native name of the eighth month of the year.
		CAL_SMONTHNAME9	Native name of the ninth month of the year.
		CAL_SMONTHNAME10	Native name of the tenth month of the year.
		CAL_SMONTHNAME11	Native name of the eleventh month of the year.
		CAL_SMONTHNAME12	Native name of the twelfth month of the year.
		CAL_SMONTHNAME13	Native name of the thirteenth month of the year, if it exists.
		CAL_SSHORTDATE	Short date formats for the calendar type.
		CAL_SSHORTESTDAYNAME1	Windows Vista and later: Short native name of the first day of the week.
		CAL_SSHORTESTDAYNAME2	Windows Vista and later: Short native name of the second day of the week.
		CAL_SSHORTESTDAYNAME3	Windows Vista and later: Short native name of the third day of the week.
		CAL_SSHORTESTDAYNAME4	Windows Vista and later: Short native name of the fourth day of the week.
		CAL_SSHORTESTDAYNAME5	Windows Vista and later: Short native name of the fifth day of the week.
		CAL_SSHORTESTDAYNAME6	Windows Vista and later: Short native name of the sixth day of the week.
		CAL_SSHORTESTDAYNAME7	Windows Vista and later: Short native name of the seventh day of the week.
		CAL_SYEARMONTH	Windows Me/98, Windows 2000: The year/month formats for the specified calendars.
*/

		return res;
		}


sw_status_t
SWLocale::load(const SWString &lcname)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		SWString	lc, sc;
		int			p;

		p = lcname.first("-");
		if (p > 0)
			{
			lc = lcname.left(p);
			sc = lcname.mid(p+1);
			}
		else
			{
			lc = lcname;
			}

		// Search our internal table
		LocaleEntry	*pEntry=localeList;
		bool		found=false;

		while (pEntry->lcid)
			{
			if (((lc.compareNoCase(pEntry->langcode) == 0 && sc.compareNoCase(pEntry->sublangcode) == 0)) || (lc.compareNoCase(pEntry->langname) == 0 && sc.compareNoCase(pEntry->sublangname) == 0))
				{
				res = load(pEntry->lcid);
				found = true;
				break;
				}

			pEntry++;
			}

		return res;
		}


/*
** Return the name for the given day in abbreviated or full format
*/
SWString
SWLocale::dayName(int day, bool abbreviated) const
		{
		SWString	s;

		if (day >= 0 && day <= 6)
			{
			if (abbreviated) s = m_shortDayName.get(day);
			else  s = m_fullDayName.get(day);
			}
		else throw SW_EXCEPTION(SWIndexOutOfBoundsException);

		return s;
		}


/*
** Return the name for the given month in abbreviated or full format
*/
SWString
SWLocale::monthName(int month, bool abbreviated) const
		{
		SWString	s;

		if (month >= 0 && month <= 11)
			{
			if (abbreviated) s = m_shortMonthName.get(month);
			else  s = m_fullMonthName.get(month);
			}
		else throw SW_EXCEPTION(SWIndexOutOfBoundsException);

		return s;
		}


SWString
SWLocale::code() const
		{
		SWString	s;

		s = m_langCode;
		if (!m_sublangCode.isEmpty())
			{
			s += _T("-");
			s += m_sublangCode;
			}

		return s;
		}


SWString
SWLocale::name() const
		{
		SWString	s;

		s = m_langName;
		if (!m_sublangName.isEmpty())
			{
			s += _T(" - ");
			s += m_sublangName;
			}

		return s;
		}






SWString
SWLocale::dateFormat(FormatType style) const
		{
		SWString	s;

		s = m_dateFormat;

		switch (style)
			{
			case FMT_SWDate:
				// Do nothing
				break;

			case FMT_SWTime:
				s.replaceAll("DD", "%d");
				s.replaceAll("MM", "%m");
				s.replaceAll("YYYY", "%Y");
				s.replaceAll("YY", "%y");
				break;
			}

		return s;
		}

/*
h	Hours without leading zeros for single-digit hours (12-hour clock).
hh	Hours with leading zeros for single-digit hours (12-hour clock).
H	Hours without leading zeros for single-digit hours (24-hour clock).
HH	Hours with leading zeros for single-digit hours (24-hour clock).

m	Minutes without leading zeros for single-digit minutes.
mm	Minutes with leading zeros for single-digit minutes.

s	Seconds without leading zeros for single-digit seconds.
ss	Seconds with leading zeros for single-digit seconds.

t	One-character time marker string.
tt	Multi-character time marker string.
*/
SWString
SWLocale::timeFormat(FormatType style) const
		{
		SWString	s;

		s = m_timeFormat;

		switch (style)
			{
			case FMT_SWDate:
				// Do nothing
				break;

			case FMT_SWTime:
				// Replace HH with %! before replace H then change %! to %H to avoiddouble replace
				s.replaceAll("HH", "%!");
				s.replaceAll("H", "%k");
				s.replaceAll("%!", "%H");

				s.replaceAll("hh", "%I");
				s.replaceAll("h", "%l");
				s.replaceAll("mm", "%M");
				s.replaceAll("m", "%M");
				s.replaceAll("ss", "%S");
				s.replaceAll("s", "%S");
				s.replaceAll("tt", "%P");
				s.replaceAll("t", "%p");
				break;
			}

		return s;
		}


/*
h	Hours without leading zeros for single-digit hours (12-hour clock).
hh	Hours with leading zeros for single-digit hours (12-hour clock).
H	Hours without leading zeros for single-digit hours (24-hour clock).
HH	Hours with leading zeros for single-digit hours (24-hour clock).

m	Minutes without leading zeros for single-digit minutes.
mm	Minutes with leading zeros for single-digit minutes.

s	Seconds without leading zeros for single-digit seconds.
ss	Seconds with leading zeros for single-digit seconds.

t	One-character time marker string.
tt	Multi-character time marker string.
*/
SWString
SWLocale::timestampFormat(FormatType style) const
		{
		SWString	s;

		s = m_dateFormat;
		s += " ";
		s += m_timeFormat;

		switch (style)
			{
			case FMT_SWDate:
				// Do nothing
				break;

			case FMT_SWTime:
				s.replaceAll("DD", "%d");
				s.replaceAll("YYYY", "%Y");
				s.replaceAll("YY", "%y");

				// Replace MM with %! before replace M then change %! to %H to avoiddouble replace
				// s.replaceAll("MM", "%m");
				s.replaceAll("MM", "%!");
				s.replaceAll("mm", "%M");
				s.replaceAll("m", "%M");
				s.replaceAll("%!", "%m");

				// Replace HH with %! before replace H then change %! to %H to avoiddouble replace
				s.replaceAll("HH", "%!");
				s.replaceAll("H", "%k");
				s.replaceAll("%!", "%H");

				s.replaceAll("hh", "%I");
				s.replaceAll("h", "%l");
				s.replaceAll("ss", "%S");
				s.replaceAll("s", "%S");
				s.replaceAll("tt", "%P");
				s.replaceAll("t", "%p");
				break;
			}

		return s;
		}
