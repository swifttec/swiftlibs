/**
*** @file		SWThreadPool.h
*** @brief		A thread pool object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWThreadPool class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWThreadPool_h__
#define __SWThreadPool_h__


#include <swift/SWJob.h>
#include <swift/SWOrderedList.h>
#include <swift/SWThread.h>
#include <swift/SWThreadEvent.h>
#include <swift/SWLockableObject.h>




#if defined(_MSC_VER)
	 // class xxxx<>' needs to have dll-interface to be used by clients
#endif


// Forward declarations
class SWThreadPool;
class SWThreadPoolThread;


/**
*** @brief	A class to represent a thread in the thread pool
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Safe</td>
*** </tr>
*** </table>
***
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWThreadPoolThread : public SWThread
		{
public:
		SWThreadPoolThread(SWThreadPool *pOwner);
		virtual ~SWThreadPoolThread();

		virtual int		run();

		bool			isActive() const		{ return m_active; }

private:
		SWThreadPool	*m_pOwner;	///< The owning pool
		bool			m_active;	///< A boolean indicating if this thread is currently active.
		};



/**
*** @brief	A class to represent a job in the thread pool
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Safe</td>
*** </tr>
*** </table>
***
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWThreadPoolJob : public SWObject
		{
friend class SWThreadPoolThread;

public:
		/// Default constructor
		SWThreadPoolJob(SWJob *pJob=0, sw_uint32_t jobpriority=0, int threadpriority=0);

		/// Virtual destructor
		virtual ~SWThreadPoolJob();

		/// Copy constructor
		SWThreadPoolJob(const SWThreadPoolJob &other);

		/// Assignment operator
		SWThreadPoolJob &	operator=(const SWThreadPoolJob &other);

		/// Comparision operator for list searching
		bool	operator==(const SWThreadPoolJob &other) const;

		/// Comparision operator for list ordering
		bool	operator<(const SWThreadPoolJob &other) const;

protected:
		SWJob					*m_pJob;		///< The actual job
		sw_uint32_t				m_jobPriority;	///< The job priority (lower value = higher priority)
		int						m_thrPriority;	///< The thread priority
		};


/**
*** @brief	A class to handle a pool of SWThread objects.
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Safe</td>
*** </tr>
*** </table>
***
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWThreadPool : public SWLockableObject
		{
friend class SWThreadPoolThread;
friend class SWThreadPoolJob;

public:
		/// A enumeration of thread pool operations
		enum Operation
			{
			AddJob,		///< Jobs can be added to the pool
			RemoveJob,	///< Jobs can be removed from the pool
			RunJob	///< Jobs can be executed by the pool
			};

		/// An enumeration of notification codes
		enum NotificationCode
			{
			JobAdded,		///< A job has been added
			JobStarted,		///< A job has started
			JobStopped,		///< A job has stopped
			AllJobsComplete	///< There are no jobs running
			};

		/// The job notification function specification used to provide job status feedback
		typedef void (*NotificationProc) (SWJob *, NotificationCode);

public:
		/**
		*** @brief	Construct a thread pool object with an optional thread count
		***
		*** This constructor allows the caller to create a thread pool object and
		*** optionally specify the number of threads. By default the thread pool
		*** uses a pool size of 1 (one) which means that only one thread will be
		*** used to execute all of the jobs.
		**/
		SWThreadPool(sw_uint32_t maxthreads=1);

		/// Virtual destrcutor
		virtual ~SWThreadPool();

		/**
		*** @brief	Add a job object to the job queue.
		***
		*** This method adds a job to the queue of jobs waiting to be run.
		*** If there are no active threads or the maximum number of threads
		*** has not been reached a new thread is started to handle the job.
		*** The caller can specify whether who will delete the job object by
		*** specifying the delflag parameter. If true the thread pool will be
		*** responsible for deleting the job object. If delflag is false the caller
		*** will be responsible for deleting the object when it is completed. In
		*** the latter case the caller must ensure that the object remains valid until
		*** it is notified that the job is complete.
		***
		*** The caller has the option of specifying various parameters (job priority,
		*** thread execution priority, notfication) when adding the job. See the
		*** parameter descriptions for more information.
		***
		*** @param[in]	pJob			A pointer to the job object to be added to the job queue.
		*** @param[in]	delflag			A boolean value indicating if the thread pool (delflag=true)
		***								of the caller (deflag=false) is responsible for deleting the
		***								job object.
		*** @param[in]	jobpriority		This optional parameter specifies the priority of the job. This
		***								is used to order the jobs according to priority. The lower the
		***								number, the higher the priority of the job, zero being the
		***								highest priority. The default priority value is 100.
		*** @param[in]	threadpriority	This optional parameter specifies the priority of the to be used
		***								by the thread during execution of the job. Callers should use one
		***								the priority from the SWThread class. The default priority is PriorityNormal.
		*** @param[in]	notify			This optional parameter can be used to override the notification value
		***								for the job. If this is NULL the job notification function is not
		***								changed, but if this parameter is not NULL the job notification function
		***								is replaced by the one specified.
		***
		*** @return		SW_STATUS_SUCCESS if successful, SW_STATUS_ILLEGAL_OPERATION if the queue is shutting
		***				down or AddJob operations are disabled.
		**/
		sw_status_t		add(SWJob *pJob, bool delflag, sw_uint32_t jobpriority=100, int threadpriority=0, SWJob::NotificationProc notify=NULL);

		/**
		*** @brief	Remove a job from the queue.
		***
		*** This method removes a job from the queue if the job is not currently
		*** running. If the job is already active this method will not remove the
		*** job but will return an error code.
		***
		*** @note
		*** If the thread pool was responsible for deleting the job it will be 
		*** deleted when it is removed and cease to be a valid object.
		***
		*** @param[in]	pJob			A pointer to the job object to be removed to the job queue.
		***
		*** @return		SW_STATUS_SUCCESS if successful, SW_STATUS_ILLEGAL_OPERATION if the queue is shutting
		***				down or AddJob operations are disabled.
		**/
		sw_status_t		remove(SWJob *pJob);


		/**
		*** @brief	Get the current maximum thread count.
		***
		*** Returns the maximum number of threads that can be active in the pool.
		**/
		sw_uint32_t		getMaximumThreadCount() const;


		/**
		*** @brief	Set the maximum thread count.
		***
		*** This method sets the maximum thread count, which is used internally
		*** to limit the number of threads used for execution. This method will 
		*** adjust the number of active threads according to the following criteria
		***
		*** If the new value is greater than the current value then the a new thread
		*** will be created for each job waiting to be run until the either there
		*** are no more waiting jobs or the new maximum thread count is reached.
		***
		*** If the new value is less than the current value this method first stops
		*** any inactive threads until no more inactive threads are available or
		*** the new maximum thread value is reached. If there are still too many threads
		*** running then the thread pool waits until a job is complete. On finishing a job
		*** a thread checks to see if there are too many threads and will automatically
		*** exit in this case. Under no circumstances are jobs prematurely exited.
		***
		*** @param[in]	maxthreads	The maximum number of threads this pool can use.
		***							This has a minimum value of 1. Values of zero will
		***							be ignored.
		**/
		void			setMaximumThreadCount(sw_uint32_t maxthreads);


		/**
		*** @brief	Enable/disable thread pool operations
		***
		*** This method enables or disables various internal operations according to the
		*** parameters specified. The op parameter specifies the parameter to be acted upon
		*** and the opval specifies if the operation is to be enabled (opval=true) or disabled
		*** (opval=false).
		***
		*** The values for op are taken from the SWThreadPool::Operation enumeration and they
		*** have the following meanings:
		*** - AddJob -		If enabled jobs can be added to the thread pool queue. This can be set to
		***					false when shutting down the pool to make sure that no more jobs are added.
		*** - RemoveJob -	If enabled inactive jobs can be removed from the internal job queue. Setting
		***					this operation to false (i.e. disabled) only prevents jobs from being removed
		***					by the external interface and does not prevent completed jobs from being
		***					removed by the internal threads.
		*** - RunJob -		If enabled jobs can be executed by the internal threads. If disabled the internal
		***					threads will not execute threads until the RunJob operation is re-enabled.
		***
		*** @param[in]	op		Specifies the operation to enable or disable.
		*** @param[in]	opval	A boolean indicating if the operation is to be enabled(true) or disabled(false).
		***
		*** @return		SW_STATUS_SUCCESS if successful or an error code otherwise.
		**/
		sw_status_t		enable(Operation op, bool opval);


		/**
		*** @brief	Shutdown the job pool
		***
		*** This method shuts down the job pool by disabling the addjob operation
		*** and waiting until all jobs have completed. When all the jobs are completed
		*** all the active threads are stopped.
		***
		*** This method will block until shutdown is complete and all threads have
		*** exited.
		**/
		void			shutdown();

		/**
		*** @brief	Blocks until all jobs are complete.
		***
		*** This method blocks until all the queued jobs are complete and then
		*** returns to the caller.
		**/
		void			waitUntilAllJobsComplete();


		/**
		*** @brief	Get the number of idle threads
		**/
		sw_uint32_t		getIdleThreadCount() const		{ return m_idleThreads; }

		/**
		*** @brief	Get the number of active threads
		**/
		sw_uint32_t		getActiveThreadCount() const	{ return m_activeThreads; }


		/**
		*** @brief	Get the total number of jobs in the pool (active and pending/queued).
		**/
		sw_uint32_t		getTotalJobCount() const		{ return getActiveJobCount() + getWaitingJobCount(); }

		/**
		*** @brief	Get the number of active jobs in the pool.
		**/
		sw_uint32_t		getActiveJobCount() const		{ return getActiveThreadCount(); }

		/**
		*** @brief	Get the number of jobs in the pool waiting to be executed.
		**/
		sw_uint32_t		getWaitingJobCount() const		{ return (sw_uint32_t)m_joblist.size(); }

		/// Set the notification function
		void			setNotificationFunction(NotificationProc notify);

private:
		/// Internal method to handle notification
		void			notify(NotificationCode code, SWJob *pJob);

protected:
		SWThreadEvent					m_event;			///< The event object used to wake up threads
		SWList<SWThread *>				m_threadlist;		///< The list of active threads
		SWOrderedList<SWThreadPoolJob>	m_joblist;			///< The list of waiting jobs
		sw_uint32_t						m_maxThreads;		///< The maximum number of threads
		sw_uint32_t						m_activeThreads;	///< The number of active threads
		sw_uint32_t						m_closeThreads;		///< The number of threads to be closed
		sw_uint32_t						m_idleThreads;		///< The number of idle threads
		sw_uint32_t						m_flags;			///< Internal flags
		NotificationProc				m_notify;		///< The notification callback function
		};

/**
*** @example	threadpool_simple.cpp
***
*** An example of how to use the SWThreadPool class in a really simple way based
*** on calling a function to execute a job.
**/

/**
*** @example	threadpool_callback.cpp
***
*** An example program that shows how to use the SWThreadPool class in conjunction with
*** callback functions to indicate job status.
**/

/**
*** @example	threadpool_derived.cpp
***
*** An example of how to use the SWThreadPool class using jobs derived
*** from the SWJob class. This example also demonstrates how to use job 
*** priorities to influence the execution order of jobs.
**/

/**
*** @example	threadpool_subjob.cpp
***
*** A program to show how to use the SWThreadPool class using jobs derived
*** from the SWJob class which are grouped into subjobs.
***
*** This example demonstrates that jobs can have their own execution function
*** as well as subjobs. Also shown is the ability to suspend and resume job execution.
**/






#endif
