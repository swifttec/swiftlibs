/**
*** @file		SWTokenizer.cpp
*** @brief		A String tokenizer class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWTokenizer class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <swift/SWTokenizer.h>
#include <swift/SWException.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif


SWTokenizer::SWTokenizer(const SWString &s, const SWString &sepchars, bool skipdups, bool stdquotes, int meta) :
	_string(s),
	_chars(sepchars)
		{
		_skip = skipdups;
		_doit = true;
		_meta = meta;
		reset();

		if (stdquotes)
			{
			addQuotes(_T('"'), _T('"'));
			addQuotes(_T('\''), _T('\''));
			}
		}

// Destructor
SWTokenizer::~SWTokenizer()
		{
		clear();	// Cleans up allocated strings
		clearQuotes();	// Cleans up allocated strings
		}


void
SWTokenizer::clear()
		{
		// Remove and delete all strings in the vector
		_tokenVec.clear();

		reset();
		_doit = true;
		}


void
SWTokenizer::clearQuotes()
		{
		_quoteVec.clear();
		_doit = true;
		}


void
SWTokenizer::reset()
		{
		_curr = 0;
		}


void
SWTokenizer::addQuotes(int qs, int qe)
		{
		_quoteVec.add(QuotePair(qs, qe));
		_doit = true;
		}


void
SWTokenizer::setDelimiters(const SWString &str)
		{
		_chars = str;
		_doit = true;
		}



void			
SWTokenizer::tokenize()
		{
		int			quoteEnum;
		int			c, n, pos, len=(int)_string.length();
		SWString	tmpstr;

		clear();
		for (pos=0;pos<len;)
			{
			if (_skip)
				{
				// Skip initial separators
				pos += (int)_string.count(_chars, SWString::including, pos);
				if (pos >= len) break;
				}

			// Check to a quoted token
			SWString	searchFor = _chars;

			c = _string[pos];

			for (quoteEnum=0;quoteEnum<(int)_quoteVec.size();quoteEnum++)
				{
				QuotePair	qp=_quoteVec[quoteEnum];

				if (qp.start == c)
					{
					searchFor = qp.end;
					pos++;
					break;
					}
				}

			tmpstr.clear();

			// Get the number of non-separator chars 
			n = (int)_string.count(searchFor, SWString::excluding, pos);

			// If we have a meta char specified the char we've just found
			// was prefixed by the meta char we must continue our search
			while (_meta && n > 0 && _string[pos+n-1] == _meta) // && s[n+1])
				{
				int	p, mcount=0;

				// Count the number of meta chars prefixing this
				// If it's even we're not really prefixed and we've found the real
				// quote!
				for (p=n-1;p>=0&&_string[pos+p]==_meta;--p) mcount++;
				if ((mcount % 2) == 0) break;

				n++;
				if (!_string[pos+n]) break;
				//tmpstr.insert(tmpstr.length(), s, n);
				tmpstr += _string.mid(pos, n);
				pos += n;
				n = (int)_string.count(searchFor, SWString::excluding, pos);
				}

			//tmpstr.insert(tmpstr.length(), s, n);
			tmpstr += _string.mid(pos, n);

			if (_meta)
				{
				int	p, tpos=0;

				while ((p=tmpstr.indexOf(_meta, tpos)) >= 0)
					{
					tmpstr.remove(p, 1);
					p++;
					tpos = p;
					}
				}

			// Allocate space for and copy the string and place into the vector
			_tokenVec.add(tmpstr);

			pos += n;
			if (pos >= len) break;

			// Skip over endQuote and delimiter
			bool	skippedDelimiter = false;

			pos++;
			if (searchFor != _chars) 
				{
				// We've skipped over the end quote and
				// should now be pointing at the delimiter
				// so we must move forward one if we can
				n = (int)_string.count(_chars, SWString::including, pos);
				if (n > 0) 
					{
					pos++;
					skippedDelimiter = true;
					}
				}
			else skippedDelimiter = true;

			// If we've now got to the end of the string AND we were in not
			// skip mode we must add an empty string to the token list
			if (pos >=len && !_skip && skippedDelimiter) _tokenVec.add(_T(""));
			}

		_doit = false;
		}




bool
SWTokenizer::hasMoreTokens()
		{
		if (_doit) tokenize();
		return (_curr < (int)_tokenVec.size());
		}


bool
SWTokenizer::nextToken(SWString &str)
		{
		if (_doit) tokenize();
		if (_curr < (int)_tokenVec.size())
			{
			str = _tokenVec[_curr++];
			return true;
			}

		return false;
		}


SWString
SWTokenizer::nextToken()
		{
		SWString	tmp;

		if (_doit) tokenize();
		if (_curr < (int)_tokenVec.size()) tmp = _tokenVec[_curr++];
		else throw SW_EXCEPTION(SWTokenizerException);

		return tmp;
		}


int
SWTokenizer::getTokenCount()
		{
		if (_doit) tokenize();
		return (int)_tokenVec.size();
		}


void
SWTokenizer::setString(const SWString &str)
		{
		_string = str;
		_doit = true;
		}


SWString
SWTokenizer::getToken(int n)
		{
		if (_doit) tokenize();
		if (n < 0 || n >= (int)_tokenVec.size()) throw SW_EXCEPTION(SWIndexOutOfBoundsException);
		return _tokenVec[n];
		}



