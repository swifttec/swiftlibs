/**
*** @file		SWProcessRWMutex.h
*** @brief		A process reader-writer mutex
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWProcessRWMutex class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWProcessRWMutex_h__
#define __SWProcessRWMutex_h__

#include <swift/SWRWMutex.h>
#include <swift/SWString.h>
#include <swift/SWThread.h>
#include <swift/SWThreadMutex.h>




/**
*** @brief	Inter-process version of a Reader-Writer Mutex.
***
*** Process-specific version of an RWMutex. This is a non-recursive mutex implementation
*** which means that any attempt for a mutex to be re-acquired by the same thread would
*** block indefinitely. To prevent this situation the Process class will throw a 
*** MutexAlreadyHeldException if this is detected.
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Safe</td>
*** </tr>
*** </table>
***
***
*** @see		RWMutex
*** @author		Simon Sparkes
*/
class SWIFT_DLL_EXPORT SWProcessRWMutex : public SWRWMutex
		{
public:
		/// Constructor - name version
		SWProcessRWMutex(const SWString &name);

		/// Virtual destructor
		virtual ~SWProcessRWMutex();

		/// Lock/Acquire this mutex for reading.
		virtual sw_status_t	acquireReadLock(sw_uint64_t waitms=SW_TIMEOUT_INFINITE);

		/// Lock/Acquire this mutex for writing
		virtual sw_status_t	acquireWriteLock(sw_uint64_t waitms=SW_TIMEOUT_INFINITE);

		/// Unlock/Release this mutex. If the mutex has been recursively acquired the mutex
		/// will only be unlocked when the recursive lock count reaches zero.
		virtual sw_status_t	release();

		/// Returns true if this thread already has this mutex locked for reading.
		virtual bool		hasReadLock();

		/// Returns true if this thread already has this mutex locked for writing.
		virtual bool		hasWriteLock();

		/// Check to see if this mutex is locked by someone for read access.
		virtual bool		isLockedForRead();

		/// Check to see if this mutex is locked by someone for write access.
		virtual bool		isLockedForWrite();

private:
		sw_handle_t		m_hMutex;			///< The handle to the actual mutex
		SWThreadMutex	m_tguard;			///< A thread guard
		bool			m_lockedForRead;	///< Used for recursive acquire safety checks
		bool			m_lockedForWrite;	///< Used for recursive acquire safety checks
		};




#endif
