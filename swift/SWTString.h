/**
*** @file		SWTString.h
*** @brief		A string classs that uses the TCHAR character size.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWTString class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWTString_h__
#define __SWTString_h__

#include <swift/SWCString.h>
#include <swift/SWWString.h>

#if !defined(SW_STRING_STANDALONE)


#endif

#if defined(UNICODE) || defined(_UNICODE)

	/// Define the runtime-specific TCHAR character type string
	typedef SWWString	SWTString;
#else
	/// Define the runtime-specific TCHAR character type string
	typedef SWCString	SWTString;
#endif

#if !defined(SW_STRING_STANDALONE)


#endif

#endif
