/****************************************************************************
**	SWBinaryConfigFile.C	A class for reading and writing .ini files
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2016 SwiftTec Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




#include "stdafx.h"

#include <xplatform/sw_math.h>

#include <swift/SWGuard.h>
#include <swift/SWTokenizer.h>
#include <swift/SWPagedFile.h>
#include <swift/SWException.h>
#include <swift/SWMemoryInputStream.h>
#include <swift/SWMemoryOutputStream.h>
#include <swift/SWBinaryConfigFile.h>
#include <swift/SWConfigValue.h>
#include <swift/SWConfigFile.h>

// Define this to debug this class
#undef DEBUG_SWBinaryConfigFile

#ifdef DEBUG_SWBinaryConfigFile
#include <swift/SWLog.h>
#endif


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif

// Begin the namespace
SWIFT_BEGIN_NAMESPACE

#define MIN_RECORD_LENGTH	12

#define BCF_MAGIC_NUMBER	0x0bcff11e

#define BCF_MAGIC_RECORD_POS	0
#define BCF_ROOT_RECORD_POS	4
#define BCF_HEAD_RECORD_POS	8
#define BCF_TAIL_RECORD_POS	12
#define BCF_DATA_RECORD_POS	16


#ifdef DEBUG_SWBinaryConfigFile
static SWLog	log;
#endif

class BCFOutputStream : public SWMemoryOutputStream
		{
public:
		sw_status_t		writePaddedData(const void *pData, sw_uint16_t len, int pad);
		sw_status_t		writePaddedString(const SWString &str, int pad);
		};


sw_status_t
BCFOutputStream::writePaddedData(const void *pData, sw_uint16_t len, int pad)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		res = writeData(pData, len);
		while (res == SW_STATUS_SUCCESS && (len % pad) != 0) 
			{
			write((sw_uint8_t)0);
			len++;
			}

		return res;
		}


sw_status_t
BCFOutputStream::writePaddedString(const SWString &str, int pad)
		{
		const char  *np=str.c_str();
		sw_uint16_t	len;

		len = (sw_uint16_t)strlen(np);
		write(len);			// Name length		(2 bytes)

		return writePaddedData(np, len, pad);
		}


class BCFInputStream : public SWMemoryInputStream
		{
public:
		SWString	readPaddedString(int pad);
		};


/**
*** Writes a string to a stream with embedded length
*** Optionally pads to n bytes
**/
SWString
BCFInputStream::readPaddedString(int pad)
		{
		SWString	str;
		sw_int16_t	namelen;
		char		*tmp;

		read(namelen);

		tmp = new char[namelen+1];
		for (int i=0;i<namelen;i++)
			{
			read(tmp[i]);
			}
		tmp[namelen] = 0;
		str = tmp;
		delete [] tmp;

		// Add name padding if required
		while ((namelen % pad) != 0) 
			{
			sw_int8_t	t;

			read(t);
			namelen++;
			}

		return str;
		}




/**********************************************************************************/
SWBinaryConfigValue::SWBinaryConfigValue(SWConfigKey *owner, const SWString &name) :
    SWConfigValue(owner, name),
    _fileRecordPosition(-1),
    _fileRecordLength(0)
		{
		}


void	
SWBinaryConfigValue::setFileRecordInfo(int position, int length)	
		{
		_fileRecordPosition = position;
		_fileRecordLength = length;
		}


/**********************************************************************************/
SWBinaryConfigKey::SWBinaryConfigKey(SWConfigKey *owner, const SWString &name, bool ignoreCase) :
    SWConfigKey(owner, name, ignoreCase),
    _fileRecordPosition(-1),
    _fileRecordLength(0)
		{
		}


void	
SWBinaryConfigKey::setFileRecordInfo(int position, int length)	
		{
		_fileRecordPosition = position;
		_fileRecordLength = length;
		}


/**
*** This method checks to see if the given sub-key already exists.
*** If it does it returns a pointer to it, if not the key is created
*** and returned.
***
*** @brief	Find or create a sub-key of the given name
**/
SWConfigKey *
SWBinaryConfigKey::findOrCreateKey(const SWString &name)
		{
		SWGuard		guard(this);
		SWConfigKey	*key = findKey(name);

		if (key == NULL)
			{
			key = new SWBinaryConfigKey(this, name, getIgnoreCase());
			_keyVec.add(key);

			setChangedFlag(KeyCreated, key);
			}

		return key;
		}



/**
*** This method checks to see if the given value already exists.
*** If it does it returns a pointer to it, if not the value is created
*** and returned.
***
*** @brief	Find or create a value of the given name
**/
SWConfigValue *
SWBinaryConfigKey::findOrCreateValue(const SWString &name)
		{
		SWGuard		guard(this);
		SWConfigValue	*value = findValue(name);

		if (value == NULL)
			{
			value = new SWBinaryConfigValue(this, name);
			_valueVec.add(value);

			// Notify parent of change and set changed flag
			setChangedFlag(ValueCreated, value);
			}

		return value;
		}


/**********************************************************************************/
SWBinaryConfigFreeRecord::SWBinaryConfigFreeRecord(SWPagedFile &pf, int position) :
    _position(position),
    _length(0),
    _prevpos(0),
    _nextpos(0)
		{
		pf.read(_position, _length);
		pf.read(_position+4, _prevpos);
		pf.read(_position+8, _nextpos);

#ifdef DEBUG_SWBinaryConfigFile
		log.debug(_T("FREESPACE: pos=%d len=%d prev=%d next=%d"), _position, _length, _prevpos, _nextpos);
#endif
		}



SWBinaryConfigFreeRecord::SWBinaryConfigFreeRecord(int position, int length, int prev, int next) :
    _position(position),
    _length(length),
    _prevpos(prev),
    _nextpos(next)
		{
		}


/**
*** Update the disk record with the current info
**/
void
SWBinaryConfigFreeRecord::updateDiskRecord(SWPagedFile &pf)
		{
		pf.write(_position, _length);
		pf.write(_position+4, _prevpos);
		pf.write(_position+8, _nextpos);
		}


/**********************************************************************************/
SWBinaryConfigFile::SWBinaryConfigFile(const TCHAR *filename, bool readonly, int pagesize, bool ignoreCase) :
    SWConfigFile(filename, readonly, true, ignoreCase),
    _pagesize(pagesize),
    _freeChainHead(0),
    _freeChainTail(0),
    _pagedFile(filename, readonly, pagesize),
    _newWritePos(BCF_DATA_RECORD_POS),
    _rootKeyPos(0),
    _rootKeyLen(0)
		{
#ifdef DEBUG_SWBinaryConfigFile
		log.setHandler("stdout:");
		//log.setHandler("file:C:/Users/Simon/Development/projects/LoyaltyTracker/bcf.log");
		log.autoflush(true);
		log.application("SWBinaryConfigFile");
		log.level(SW_LOG_LEVEL_MAXIMUM);
#endif

		// Load this structure from the file
		readFromFile(_pagedFile, true);
		}


// Destructor
SWBinaryConfigFile::~SWBinaryConfigFile()
		{
		flush();
		
		// Free up any remaining internal records
		while (_freeChainList.size() > 0)
			{
			SWBinaryConfigFreeRecord	*pRecord;

			_freeChainList.removeHead(&pRecord);
			delete pRecord;
			}
		}




// Calculate the record length of a SWConfigValue
int
SWBinaryConfigFile::calculateRecordLength(SWConfigValue *pValue)
		{
		int	len, namelen, datalen;

		namelen = (int)pValue->name().length();
		if ((namelen % 2) != 0) namelen++;
		
		datalen = (int)pValue->length();
		if ((datalen % 2) != 0) datalen++;

		// Calculate the record length
		len = sizeof(sw_int32_t)	// Record Length	4 bytes
			+ sizeof(sw_int16_t)	// Name Length		2 bytes
			+ namelen				// Name Data		(rounded to 2 byte boundary)
			+ sizeof(sw_int16_t)	// Type				2 bytes (0xffff = free space record)
			+ sizeof(sw_int32_t)	// Data Length		2 bytes
			+ datalen;				// Data				(rounded to 2 byte boundary)

		// We have a minimum record length of MIN_RECORD_LENGTH bytes to support the free chain
		if (len < MIN_RECORD_LENGTH) len = MIN_RECORD_LENGTH;

		return len;
		}



// Calculate the record length of a SWConfigKey
int
SWBinaryConfigFile::calculateRecordLength(SWConfigKey *key)
		{
		int	len, namelen;

		namelen = (int)key->name().length();
		if ((namelen % 2) != 0) namelen++;
		
		// Calculate the record length
		len = sizeof(sw_int32_t)				// Record Length	4 bytes
			+ (sizeof(sw_int16_t))	 			// Name Length		2 bytes
			+ namelen					// Name Data		(rounded to 2 byte boundary)
			+ (sizeof(sw_int16_t))				// Value Count		2 bytes
			+ (key->valueCount() * sizeof(sw_int32_t))	// Value Pointers	4 bytes each
			+ (sizeof(sw_int16_t))				// SubKey Count		2 bytes
			+ (key->keyCount() * sizeof(sw_int32_t)); 	// SubKey Pointers	4 bytes each

		// We have a minimum record length of MIN_RECORD_LENGTH bytes to support the free chain
		if (len < MIN_RECORD_LENGTH) len = MIN_RECORD_LENGTH;

		return len;
		}





/**
*** Loads the config structures from the given file
**/
bool
SWBinaryConfigFile::readFromFile(const SWString &filename)
		{
		_name = filename;
		if (_name == _T("")) return false;

		SWPagedFile	pf(filename, false, _pagesize);

		return readFromFile(pf, false);
		}



/**
*** Loads the config structures from the given SWPagedFile object
**/
bool
SWBinaryConfigFile::readFromFile(SWPagedFile &pf, bool createFlag)
		{
		clearKeysAndValues();
		m_loaded = false;
		_newWritePos = BCF_DATA_RECORD_POS;

		// Read in magic number
		sw_int32_t	magic;
		
		pf.read(0, magic);

		if (magic != BCF_MAGIC_NUMBER)
			{
			if (magic == 0 && createFlag)
				{
				// Write the initial parts of the file out
#ifdef DEBUG_SWBinaryConfigFile
				log.debug(_T("SWBinaryConfigFile::readFromFile - new file"));
#endif

				// Write magic number to file (0 = end of list)
				pf.write(BCF_MAGIC_RECORD_POS, BCF_MAGIC_NUMBER);

				// Write free space pointer to file (0 = end of list)
				// Since this is a new file there will be no free space
				// so the values are always zero.
				pf.write(BCF_HEAD_RECORD_POS, 0);
				pf.write(BCF_TAIL_RECORD_POS, 0);

				// Write the initial header
				pf.write(BCF_ROOT_RECORD_POS, BCF_DATA_RECORD_POS);
				_rootKeyPos = BCF_DATA_RECORD_POS;
				_rootKeyLen = calculateRecordLength(this);
				_newWritePos = writeKeyToFile(pf, _rootKeyPos, _rootKeyLen, this);
				}
#ifdef DEBUG_SWBinaryConfigFile
			else log.debug(_T("SWBinaryConfigFile::readFromFile got wrong magic number (%x)"), magic);
#endif

			return false;
			}

		m_loading = true;

		// Read in free chain pointer
		pf.read(BCF_HEAD_RECORD_POS, _freeChainHead);
		pf.read(BCF_TAIL_RECORD_POS, _freeChainTail);
#ifdef DEBUG_SWBinaryConfigFile
		log.debug(_T("SWBinaryConfigFile::readFromFile - free chain head = %d"), _freeChainHead);
#endif

		// Read in root key
		pf.read(BCF_ROOT_RECORD_POS, _rootKeyPos);
		_newWritePos = readKeyFromFile(pf, _rootKeyPos, NULL);
#ifdef DEBUG_SWBinaryConfigFile
		log.debug(_T("SWBinaryConfigFile::readFromFile - new write position (before free space)= %d"), _newWritePos);
#endif

		// Read in the free chain (NOT YET)
		if (_freeChainHead != 0)
			{
			SWBinaryConfigFreeRecord	*frp;
			int				pos;

			pos = _freeChainHead;
			while (pos != 0)
				{
				frp = new SWBinaryConfigFreeRecord(pf, pos);
				_freeChainList.add(frp);
				pos = frp->nextPosition();
				_newWritePos = sw_math_max(_newWritePos, frp->position() + frp->length());
				}
			}

#ifdef DEBUG_SWBinaryConfigFile
		dumpFreeSpaceList();
#endif

#ifdef DEBUG_SWBinaryConfigFile
		log.debug(_T("SWBinaryConfigFile::readFromFile - new write position (after free space)= %d"), _newWritePos);
#endif

		m_loading = false;
		m_loaded = true;

		return true;
		}


int
SWBinaryConfigFile::readKeyFromFile(SWPagedFile &pf, int pos, SWConfigKey *parent)
		{
		BCFInputStream	is;
		SWConfigKey		*key;
		char			buf[4];
		int			datalen, maxpos=0, p;

		// Read the record size from the file
		pf.read(pos, buf, 4);
		is.setBuffer(buf, 4);
		is.read(datalen);
		datalen -= 4;
		maxpos = pos + datalen + 4;

#ifdef DEBUG_SWBinaryConfigFile
		log.debug(_T("SWBinaryConfigFile::readKeyFromFile - pos=%d  reclen=%d"), pos, datalen+4);
#endif

		if (datalen > 0)
			{
			char		*data = new char[datalen];
			SWString	name;
			sw_int16_t	count;
			sw_int32_t	recpos;

			pf.read(pos+4, data, datalen);

			// Set the input stream to point to the data just read
			is.setBuffer(data, datalen);

			// Set the key name
			name = is.readPaddedString(2);
			if (parent == NULL) 
				{
				this->setName(name);
				key = this;
				_rootKeyPos = pos;
				_rootKeyLen = datalen+4;
				}
			else 
				{
				key = parent->findOrCreateKey(name);

				// Record the file info as we know that this will be
				// a SWBinaryConfigKey record
				dynamic_cast<SWBinaryConfigKey *>(key)->setFileRecordInfo(pos, datalen+4);
				}

			// Read in the value count and the values
			is.read(count);
			while (count-- > 0)
				{
				is.read(recpos);
				p = readValueFromFile(pf, recpos, key);
				maxpos = sw_math_max(maxpos, p);
				}

			// Read in the key count and the values
			is.read(count);
			while (count-- > 0)
				{
				is.read(recpos);
				p = readKeyFromFile(pf, recpos, key);
				maxpos = sw_math_max(maxpos, p);
				}

			delete [] data;
			}
		else if (parent != NULL)
			{
			throw SW_EXCEPTION_TEXT(SWConfigFileException, "BinaryConfigFile::readKeyFromFile - Corrupted file.");
			}

		return maxpos;
		}


/**
*** Writes the data structure out to a file
**/
bool
SWBinaryConfigFile::writeToFile(const SWString &filename)
		{
		SWString	bakfile;

		if (filename.isEmpty()) return false;

		SWPagedFile	pf(filename, false, _pagesize);

		// Write magic number to file (0 = end of list)
		pf.write(BCF_MAGIC_RECORD_POS, BCF_MAGIC_NUMBER);

		// Write free space pointer to file (0 = end of list)
		// Since this is a new file there will be no free space
		// so the values are always zero.
		pf.write(BCF_HEAD_RECORD_POS, 0);
		pf.write(BCF_TAIL_RECORD_POS, 0);

		pf.write(BCF_ROOT_RECORD_POS, BCF_DATA_RECORD_POS);
		writeKeyToFile(pf, BCF_DATA_RECORD_POS, calculateRecordLength(this), this);

		return true;
		}


/**
*** Writes a key, and in non-update mode all it's subkeys and values to the output stream
***
*** @param pf		The paged file to update
*** @param pos		The record position
*** @param len		The record length
*** @param key		The actual key
*** @param updateFlag	If true we are updating a key on file so we don't create
***			all the subkeys and values on use their existing position
***			data.
***
*** @return		The adjusted position
**/
int
SWBinaryConfigFile::writeKeyToFile(SWPagedFile &pf, int pos, int len, SWConfigKey *key, bool updateFlag)
		{
		BCFOutputStream	os;
		SWConfigValue		*cv;
		SWConfigKey		*ck;
		SWString		keypath;
		int			i, rlen, writepos=pos;

		// Calculate the record length
		rlen = calculateRecordLength(key);
		
		if (rlen > len)
			{
			SWString	msg;

			msg.format("SWBinaryConfigFile::writeKeyToFile - Calculated record length (%d) is greater than actual record length (%d)", rlen, len);
			throw SW_EXCEPTION_TEXT(SWConfigFileException, msg);
			}

#ifdef DEBUG_SWBinaryConfigFile
		log.debug("SWBinaryConfigFile::writeKeyToFile(%s) - pos=%d  len=%d  reclen=%d", key->name().c_str(), pos, len, rlen);
#endif

		// Calculate the record length
		pos += len;

		// Now write the data
		os.write(len);			// Record length	(4 bytes)

		// Write the name padded to 2 byte boundary
		os.writePaddedString(key->name(), 2);

		// Write out the values
		os.write((sw_int16_t)key->valueCount());	// Value count		(2 bytes)
		for (i=0;i<key->valueCount();i++)
			{
			cv = key->getValue(i);
			if (updateFlag) pos = ((SWBinaryConfigValue *)cv)->getFileRecordPosition();
			os.write(pos);
			if (!updateFlag) pos = writeValueToFile(pf, pos, calculateRecordLength(cv), cv);
			}

		// Write out the sub-keys
		os.write((sw_int16_t)key->keyCount());		// Key count		(2 bytes)
		for (i=0;i<key->keyCount();i++)
			{
			ck = key->getKey(i);
			if (updateFlag) pos = ((SWBinaryConfigKey *)ck)->getFileRecordPosition();
			os.write(pos);
			if (!updateFlag) pos = writeKeyToFile(pf, pos, calculateRecordLength(ck), ck);
			}

		// Finally write the memory to paged file
		pf.write(writepos, os.data(), rlen);
		if (rlen < len)
			{
			// We must pad out the rest of the buffer
			char	*tmp = new char[len-rlen];

			memset(tmp, 0, len-rlen);
			pf.write(writepos+rlen, tmp, len-rlen);
			delete [] tmp;
			}

		return pos;
		}




/**
*** Writes a single value to the output stream
***
*** @param pf		The paged file to update
*** @param pos		The record position
*** @param len		The record length
*** @param value	The actual value
***
*** @return		The adjusted position
**/
int
SWBinaryConfigFile::writeValueToFile(SWPagedFile &pf, int pos, int len, SWConfigValue *value)
		{
		BCFOutputStream	os;
		int			writepos=pos, rlen;

		// Calculate the record length
		rlen = calculateRecordLength(value);
		
		if (rlen > len)
			{
			SWString	msg;

			msg.format("SWBinaryConfigFile::writeValueToFile - Calculated record length (%d) is greater than actual record length (%d)", rlen, len);
			throw SW_EXCEPTION_TEXT(SWConfigFileException, msg);
			}

#ifdef DEBUG_SWBinaryConfigFile
		log.debug("SWBinaryConfigFile::writeValueToFile - pos=%d  len=%d  reclen=%d", pos, len, rlen);
#endif

		pos += len;

		// Now write the data
		os.write(len);			// Record length	(4 bytes)

		// Write the name padded to 2 byte boundary
		os.writePaddedString(value->name(), 2);

		// Write the data
		os.write(*value);

		pf.write(writepos, os.data(), rlen);
		if (rlen < len)
			{
			// We must pad out the rest of the buffer
			char	*tmp = new char[len-rlen];

			memset(tmp, 0, len-rlen);
			pf.write(writepos+rlen, tmp, len-rlen);
			delete [] tmp;
			}


		return pos;
		}

/**
*** Initialise this field from the given memory input stream
***
*** @return	The length of the data.
**/
int
SWBinaryConfigFile::readValueFromFile(SWPagedFile &pf, int pos, SWConfigKey *key)
		{
		BCFInputStream	is;
		char			buf[4];
		int			datalen;

		// Read the record size from the file
		pf.read(pos, buf, 4);
		is.setBuffer(buf, 4);
		is.read(datalen);
		datalen -= 4;

#ifdef DEBUG_SWBinaryConfigFile
		log.debug("SWBinaryConfigFile::readValueFromFile - pos=%d  reclen=%d", pos, datalen+4);
#endif

		if (datalen > 0)
			{
			char		*data = new char[datalen];
			SWString		name;
			SWConfigValue	*value;

			pf.read(pos+4, data, datalen);

			// Set the input stream to point to the data just read
			is.setBuffer(data, datalen);

			// Get the value name
			name = is.readPaddedString(2);
			value = key->findOrCreateValue(name);

			// Record the file info
			dynamic_cast<SWBinaryConfigValue *>(value)->setFileRecordInfo(pos, datalen+4);

			value->readFromStream(is);
			/*
			SWValue::ValueType	ft;
			sw_int16_t				sv;

			is.read(sv);
			ft = (SWValue::ValueType)sv;

			is.read(sv);
			datalen = sv;

			// Clear the field
			value->clear();

			switch (ft)
				{
				// Fixed length fields
				case SWConfigValue::VtNone:		
					break;

				case SWConfigValue::VtCChar:
				case SWConfigValue::VtInt8:
					{
					sw_int8_t	v;

					is.read(v);
					value->setValue(v, ft);
					}
					break;

				case SWConfigValue::VtWChar:		
				case SWConfigValue::VtInt16:		
					{
					sw_int16_t	v;

					is.read(v);
					value->setValue(v, ft);
					}
					break;

				case SWConfigValue::VtInt32:
					{
					sw_int32_t	v;

					is.read(v);
					value->setValue(v, ft);
					}
					break;

				case SWConfigValue::VtInt64:
					{
					sw_int64_t	v;

					is.read(v);
					value->setValue(v);
					}
					break;

				case SWConfigValue::VtFloat:
					{
					float	v;

					is.read(v);
					value->setValue(v);
					}
					break;
					
				case SWConfigValue::VtDouble:
					{
					double	v;

					is.read(v);
					value->setValue(v);
					}
					break;

				// Variable length fields
				case SWConfigValue::VtWString:
				case SWConfigValue::VtCString:
				case SWConfigValue::VtData:
					{
					SWBuffer	dbuf(datalen);
					sw_uint32_t	len;

					is.read(dbuf, len, datalen);
					datalen = len;
					switch (ft)
						{
						case SWConfigValue::VtWString:	value->setValue((const wchar_t *)(char *)dbuf);	break;
						case SWConfigValue::VtCString:	value->setValue((const char *)dbuf);	break;
						case SWConfigValue::VtData:		value->setValue(dbuf, datalen);		break;
						}
					}
					break;
				}
			*/

			delete [] data;
			}

		return pos+datalen+4;
		}


/**
*** This method checks to see if the given sub-key already exists.
*** If it does it returns a pointer to it, if not the key is created
*** and returned.
***
*** @brief	Find or create a sub-key of the given name
**/
SWConfigKey *
SWBinaryConfigFile::findOrCreateKey(const SWString &name)
		{
		SWGuard		guard(this);
		SWConfigKey	*key = findKey(name);

		if (key == NULL)
			{
			key = new SWBinaryConfigKey(this, name, getIgnoreCase());
			_keyVec.add(key);

			setChangedFlag(KeyCreated, key);
			}

		return key;
		}



/**
*** This method checks to see if the given value already exists.
*** If it does it returns a pointer to it, if not the value is created
*** and returned.
***
*** @brief	Find or create a value of the given name
**/
SWConfigValue *
SWBinaryConfigFile::findOrCreateValue(const SWString &name)
		{
		SWGuard		guard(this);
		SWConfigValue	*value = findValue(name);

		if (value == NULL)
			{
			value = new SWBinaryConfigValue(this, name);
			_valueVec.add(value);

			// Notify parent of change and set changed flag
			setChangedFlag(ValueCreated, value);
			}

		return value;
		}


// Set the changed flag.
// We must also set the changed flag of our owner
void
SWBinaryConfigFile::setChangedFlag(ChangeEventCode code, SWObject *obj)
        {
        if (!m_loading)
            {
            _changed = true;
            if (!m_readOnlyFlag) 
				{
				switch (code)
					{
					case ValueCreated:
#ifdef DEBUG_SWBinaryConfigFile
					log.debug("ValueCreated (%s)", ((SWConfigValue *)obj)->name().c_str());
#endif
						{
						SWBinaryConfigValue	*vp = dynamic_cast<SWBinaryConfigValue *>(obj);
						int			newlen=calculateRecordLength(vp), recpos, reclen;

#ifdef DEBUG_SWBinaryConfigFile
						log.debug("Disk record at %d length %d (new length=%d)", vp->getFileRecordPosition(), vp->getFileRecordLength(), newlen);
#endif

						findFreeSpace(_pagedFile, newlen, recpos, reclen);
						vp->setFileRecordInfo(recpos, reclen);

						writeValueToFile(_pagedFile, vp->getFileRecordPosition(), vp->getFileRecordLength(), vp);

						// Inform the owning record to update itself on disk
						SWBinaryConfigKey	*owner;
						
						owner = dynamic_cast<SWBinaryConfigKey *>(vp->m_pOwner);
						if (owner != NULL) owner->setChangedFlag(KeyUpdated, owner);
						}
					break;

					case ValueDeleted:
#ifdef DEBUG_SWBinaryConfigFile
						log.debug("ValueDeleted (%s)", ((SWConfigValue *)obj)->name().c_str());
#endif
						{
						SWBinaryConfigValue	*vp = dynamic_cast<SWBinaryConfigValue *>(obj);

#ifdef DEBUG_SWBinaryConfigFile
						log.debug("Disk record at position %d length %d (new length=%d)", vp->getFileRecordPosition(), vp->getFileRecordLength(), calculateRecordLength(vp));
#endif

						// Add the space freed by the record to the free space chain and
						// clear the changed flag so we don't do a flush.
						addFreeSpace(_pagedFile, vp->getFileRecordPosition(), vp->getFileRecordLength());

						// Inform the owning record to update itself on disk
						SWBinaryConfigKey	*owner = (SWBinaryConfigKey *)(vp->m_pOwner);
						if (owner != NULL) owner->setChangedFlag(KeyUpdated, owner);
						}
					break;

					case ValueUpdated:
#ifdef DEBUG_SWBinaryConfigFile
					log.debug("ValueUpdated (%s)", ((SWConfigValue *)obj)->name().c_str());
#endif
						{
						SWBinaryConfigValue	*vp = dynamic_cast<SWBinaryConfigValue *>(obj);
						int			newlen = calculateRecordLength(vp);
						bool		changedPos = false;

#ifdef DEBUG_SWBinaryConfigFile
						log.debug("Disk record at position %d length %d (new length=%d)", vp->getFileRecordPosition(), vp->getFileRecordLength(), newlen);
#endif

						if (newlen > vp->getFileRecordLength())
						{
						// Find space to write the data
						int	recpos, reclen;

						findFreeSpace(_pagedFile, newlen, recpos, reclen);
						if (recpos != vp->getFileRecordPosition()) addFreeSpace(_pagedFile, vp->getFileRecordPosition(), vp->getFileRecordLength());
						vp->setFileRecordInfo(recpos, reclen);
						changedPos = true;
						}

						writeValueToFile(_pagedFile, vp->getFileRecordPosition(), vp->getFileRecordLength(), vp);

						if (changedPos)
							{
							// Inform the owning record to update itself on disk
							SWBinaryConfigKey	*owner = (SWBinaryConfigKey *)(vp->m_pOwner);
							if (owner != NULL) owner->setChangedFlag(KeyUpdated, owner);
							}
						}
					break;

					case KeyCreated:
#ifdef DEBUG_SWBinaryConfigFile
					log.debug("KeyCreated (%s)", ((SWConfigKey *)obj)->name().c_str());
#endif
						{
						SWBinaryConfigKey	*kp = dynamic_cast<SWBinaryConfigKey *>(obj);
						int			newlen=calculateRecordLength(kp), recpos, reclen;

#ifdef DEBUG_SWBinaryConfigFile
						log.debug("Disk record at position %d length %d (new length=%d)", kp->getFileRecordPosition(), kp->getFileRecordLength(), calculateRecordLength(kp));
#endif


						findFreeSpace(_pagedFile, newlen, recpos, reclen);
						kp->setFileRecordInfo(recpos, reclen);

						writeKeyToFile(_pagedFile, kp->getFileRecordPosition(), kp->getFileRecordLength(), kp);

						// Inform the owning record to update itself on disk
						SWBinaryConfigKey	*owner = (SWBinaryConfigKey *)(kp->_owner);
						if (owner != NULL) owner->setChangedFlag(KeyUpdated, owner);
						}
					break;

					case KeyDeleted:
#ifdef DEBUG_SWBinaryConfigFile
					log.debug("KeyDeleted (%s)", ((SWConfigKey *)obj)->name().c_str());
#endif
						{
						SWBinaryConfigKey	*kp = dynamic_cast<SWBinaryConfigKey *>(obj);

#ifdef DEBUG_SWBinaryConfigFile
						log.debug("Disk record at position %d length %d (new length=%d)", kp->getFileRecordPosition(), kp->getFileRecordLength(), calculateRecordLength(kp));
#endif

						// Add the space freed by the record to the free space chain and
						// clear the changed flag so we don't do a flush.
						addFreeSpace(_pagedFile, kp->getFileRecordPosition(), kp->getFileRecordLength());

						// Inform the owning record to update itself on disk
						SWBinaryConfigKey	*owner = (SWBinaryConfigKey *)(kp->_owner);
						if (owner != NULL) owner->setChangedFlag(KeyUpdated, owner);
						}
					break;

					case KeyUpdated:
#ifdef DEBUG_SWBinaryConfigFile
					log.debug("KeyUpdated (%s)", ((SWConfigKey *)obj)->name().c_str());
#endif
					if (obj == this)
						{
						int		newlen = calculateRecordLength(this);

#ifdef DEBUG_SWBinaryConfigFile
						log.debug("ROOT KEY: Disk record at position %d length %d (new length=%d)", _rootKeyPos, _rootKeyLen, newlen);
#endif


						if (newlen > _rootKeyLen)
							{
							// Find space to write the data
							int	recpos, reclen;

							findFreeSpace(_pagedFile, newlen, recpos, reclen);
							if (recpos != _rootKeyPos) addFreeSpace(_pagedFile, _rootKeyPos, _rootKeyLen);
							_rootKeyPos = recpos;
							_rootKeyLen = reclen;
							_pagedFile.write(BCF_ROOT_RECORD_POS, _rootKeyPos);
							}

						writeKeyToFile(_pagedFile, _rootKeyPos, _rootKeyLen, this, true);
						}
					else
						{
						SWBinaryConfigKey	*kp = dynamic_cast<SWBinaryConfigKey *>(obj);
						int			newlen = calculateRecordLength(kp);
						bool			changedPos = false;

#ifdef DEBUG_SWBinaryConfigFile
						log.debug("Disk record at position %d length %d (new length=%d)", kp->getFileRecordPosition(), kp->getFileRecordLength(), newlen);
#endif


						if (newlen > kp->getFileRecordLength())
							{
							// Find space to write the data
							int	recpos, reclen;

							findFreeSpace(_pagedFile, newlen, recpos, reclen);
							if (recpos != kp->getFileRecordPosition()) addFreeSpace(_pagedFile, kp->getFileRecordPosition(), kp->getFileRecordLength());
							kp->setFileRecordInfo(recpos, reclen);
							changedPos = true;
							}

						writeKeyToFile(_pagedFile, kp->getFileRecordPosition(), kp->getFileRecordLength(), kp, true);

						if (changedPos)
							{
							// Inform the owning record to update itself on disk
							SWBinaryConfigKey	*owner = (SWBinaryConfigKey *)(kp->_owner);
							if (owner != NULL) owner->setChangedFlag(KeyUpdated, owner);
							}
						}
					break;
					}
				}
            }
        }


/**
*** Remove the given free space record from the list
*** and the disk
**/
void
SWBinaryConfigFile::removeFreeSpace(SWPagedFile &pf, SWBinaryConfigFreeRecord *frp)
		{
		SWBinaryConfigFreeRecord	*p;

		// Remove from the internal list
		_freeChainList.remove(frp);

		// Remove from the disk chain
		if (frp->prevPosition() == 0)
			{
			// This was the head record
			setFreeChainHead(pf, frp->nextPosition());
			}
		else
			{
			p = findFreeChainPosition(frp->prevPosition(), 0);
			p->setNextPosition(frp->nextPosition());
			p->updateDiskRecord(pf);
			}

		if (frp->nextPosition() == 0)
			{
			// This was the tail record
			setFreeChainTail(pf, frp->prevPosition());
			}
		else
			{
			p = findFreeChainPosition(frp->nextPosition(), 0);
			p->setPrevPosition(frp->prevPosition());
			p->updateDiskRecord(pf);
			}

		// Finally delete the object
		delete frp;
		}


/**
*** Find free space to write in either from the free space chain
*** or from the next write position
**/
void
SWBinaryConfigFile::findFreeSpace(SWPagedFile &pf, int wanted, int &position, int &size)
		{
		SWBinaryConfigFreeRecord	*frp;
		SWIterator<SWBinaryConfigFreeRecord *>			it=_freeChainList.iterator();

#ifdef DEBUG_SWBinaryConfigFile
		log.debug("SWBinaryConfigFile::findFreeSpace - looking for %d bytes of space", wanted);
#endif

		while (it.hasNext())
			{
			frp = *it.next();

			if (wanted <= frp->length())
				{
#ifdef DEBUG_SWBinaryConfigFile
				log.debug("- found %d bytes of space at position (%d, len=%d)", wanted, frp->position(), frp->length());
#endif

				// Save space info
				position = frp->position();
				size = frp->length();

				removeFreeSpace(pf, frp);

#ifdef DEBUG_SWBinaryConfigFile
		dumpFreeSpaceList();
#endif
				return;
				}
			}

		// If we get here then no suitable space has been found so we allocated from the end
		// of the file.
#ifdef DEBUG_SWBinaryConfigFile
		log.debug("- allocating %d bytes of space from end of file (%d)", wanted, _newWritePos);
#endif
		position = _newWritePos;
		size = wanted;
		_newWritePos += size;
		}



/**
*** Add the free space to the free chain
**/
void
SWBinaryConfigFile::addFreeSpace(SWPagedFile &pf, int pos, int len)
		{
		if (len < MIN_RECORD_LENGTH) throw SW_EXCEPTION_TEXT(SWConfigFileException, "SWBinaryConfigFile::addFreeSpace - Can't add < MIN_RECORD_LENGTH bytes free space.");

		SWIterator<SWBinaryConfigFreeRecord *>			it;
		SWBinaryConfigFreeRecord	*frp;
		bool				added=false;

#ifdef DEBUG_SWBinaryConfigFile
		log.debug("SWBinaryConfigFile::addFreeSpace(%d, %d)", pos, len);
#endif

		// First of all check if we can merge the space into an exist record
		it = _freeChainList.iterator();
		while (it.hasNext())
			{
			frp = *it.next();

			if ((frp->position() + frp->length()) == pos)
				{
#ifdef DEBUG_SWBinaryConfigFile
				log.debug("SWBinaryConfigFile::addFreeSpace - Added %d bytes to existing free space record at %d", len, frp->position());
#endif
				frp->setLength(frp->length() + len);

				// Update the free space record on disk
				frp->updateDiskRecord(pf);

				added = true;
				break;
				}
			else if ((pos + len) == frp->position())
				{
				//int	oldpos = frp->position();

#ifdef DEBUG_SWBinaryConfigFile
				log.debug("SWBinaryConfigFile::addFreeSpace - Adjusted position of existing free space record at %d by %d bytes", frp->position(), len);
#endif
				frp->setPosition(pos);
				frp->setLength(frp->length() + len);
				frp->updateDiskRecord(pf);

				if (frp->prevPosition() == 0)
					{
					// This is the head record - update the head pointer
					setFreeChainHead(pf, frp->position());
					}
				else
					{
					// Find the previous record and update it
					SWBinaryConfigFreeRecord	*p;

					p = findFreeChainPosition(frp->prevPosition());
					if (p == NULL)
						{
						throw SW_EXCEPTION_TEXT(SWConfigFileException, "SWBinaryConfigFile::addFreeSpace - Can't find previous free space record.");
						}
					else
						{
						p->setNextPosition(frp->position());
						p->updateDiskRecord(pf);
						}
					}

				if (frp->nextPosition() == 0)
					{
					// This is the tail record - update the tail pointer
					setFreeChainTail(pf, frp->position());
					}
				else
					{
					// Find the next record and update it
					SWBinaryConfigFreeRecord	*p;

					p = findFreeChainPosition(frp->nextPosition());
					if (p == NULL)
						{
						throw SW_EXCEPTION_TEXT(SWConfigFileException, "SWBinaryConfigFile::addFreeSpace - Can't find next free space record.");
						}
					else
						{
						p->setPrevPosition(frp->position());
						p->updateDiskRecord(pf);
						}
					}

				added = true;
				break;
				}
			}

		if (!added)
			{
			// We can't merge into an existing record so we
			// must create a new one. We insert in size order
			SWBinaryConfigFreeRecord	*prevfrp=NULL;

			it = _freeChainList.iterator();
			while (it.hasNext())
				{
				frp = *it.next();

				if (len < frp->length())
					{
					// Insert new record at this point
					SWBinaryConfigFreeRecord	*p;

					p = new SWBinaryConfigFreeRecord(pos, len, frp->prevPosition(), frp->position());
					p->updateDiskRecord(pf);

					// Update head record if required
					if (frp->prevPosition() == 0) setFreeChainHead(pf, p->position());

					// Update prev pointer to point to new record
					frp->setPrevPosition(p->position());
					frp->updateDiskRecord(pf);

					if (prevfrp != NULL)
						{
						prevfrp->setNextPosition(p->position());
						prevfrp->updateDiskRecord(pf);
						}

					it.add(p, it.AddBefore);
					added = true;
					break;
					}

				prevfrp = frp;
				}
			}

		if (!added) 
			{
			SWBinaryConfigFreeRecord	*pTail=NULL;

			_freeChainList.getTail(pTail);

			// If not added by now add it to the end of the list
			frp = new SWBinaryConfigFreeRecord(pos, len, _freeChainTail, 0);
			frp->updateDiskRecord(pf);
			_freeChainList.addTail(frp);

			if (_freeChainTail != 0)
				{
				pTail->_nextpos = pos;
				pTail->updateDiskRecord(pf);
				}

			setFreeChainTail(pf, pos);

			if (_freeChainHead == 0)
				{
				setFreeChainHead(pf, pos);
				}

#ifdef DEBUG_SWBinaryConfigFile
			log.debug("SWBinaryConfigFile::addFreeSpace - Added new free space record to end of list (position=%d, length=%d)", frp->position(), frp->length());
#endif
			}

#ifdef DEBUG_SWBinaryConfigFile
		dumpFreeSpaceList();
#endif
		}


void
SWBinaryConfigFile::dumpFreeSpaceList()
		{
#ifdef DEBUG_SWBinaryConfigFile
		SWIterator<SWBinaryConfigFreeRecord *>					it = _freeChainList.iterator();
		SWBinaryConfigFreeRecord	*frp;

		log.debug("-------------- Free Space list --------------");
		log.debug("head = %d", _freeChainHead);
		while (it.hasNext())
			{
			frp = *it.next();

			log.debug("%05d bytes at %d (next=%d prev=%d)", frp->length(), frp->position(), frp->nextPosition(), frp->prevPosition());
			}
		log.debug("tail = %d", _freeChainTail);
		log.debug("---------------------------------------------");
#endif
		}


/**
*** Find record in the free chain list with the given position
***
*** @param	v	The position value to search for
*** @param	t	The position type to search for. (t==0 => position, t < 0 => prevPosition, t > 0 => nextPosition)
**/
SWBinaryConfigFreeRecord *
SWBinaryConfigFile::findFreeChainPosition(int v, int t)
		{
		SWIterator<SWBinaryConfigFreeRecord *>	it = _freeChainList.iterator();
		SWBinaryConfigFreeRecord	*p;

		while (it.hasNext())
			{
			p = *it.next();
			if (t == 0 && p->position() == v) return p;
			else if (t < 0 && p->prevPosition() == v) return p;
			else if (t > 0 && p->nextPosition() == v) return p;
			}

		return NULL;
		}


// Updates the _freeChainHead member and the disk record too
void
SWBinaryConfigFile::setFreeChainHead(SWPagedFile &pf, int v)
		{
		_freeChainHead = v;
		pf.write(BCF_HEAD_RECORD_POS, v);
		}


// Updates the _freeChainTail member and the disk record too
void
SWBinaryConfigFile::setFreeChainTail(SWPagedFile &pf, int v)
		{
		_freeChainTail = v;
		pf.write(BCF_TAIL_RECORD_POS, v);
		}

void
SWBinaryConfigFile::flush()
		{
		if (isReadOnly() || !changed()) return;

		_pagedFile.flush();
		clearChangedFlag();
		}

// End the namespace
SWIFT_END_NAMESPACE
