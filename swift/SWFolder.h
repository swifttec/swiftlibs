/**
*** @file		SWFolder.h
*** @brief		A class to represent a handle to a directory.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWFolder class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




#ifndef __SWFolder_h__
#define __SWFolder_h__

#include <swift/swift.h>
#include <swift/SWFileInfo.h>
#include <swift/SWArray.h>


/**
*** @brief	A class to represent a handle to a folder/directory.
***
*** This class represents a handle to a folder/directory which can
*** operate on a single folder/directory entry at a time. This makes it
*** efficient on folder/directories with very large numbers of files.
***
*** No filtering is done on folder entries so all entries in the folder
*** are returned including hidden/system entries. To pre-filter the 
*** entries returned use a SWFilteredFolder.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWFolder : public SWObject
		{
public:
		/**
		*** @brief	Default constructor.
		***
		*** If the path is specified the directory will be opened by
		*** calling the open method.
		***
		*** @param	path		The directory to be opened
		*** @see	open
		**/
		SWFolder(const SWString &path=_T(""), const SWString &filter=_T(""));

		/**
		*** @brief	Copy constructor
		***
		*** The copy constructor opens a new handle to the directory
		*** represented by the specified folder so that the two SWFolder
		*** objects can operate independently.
		***
		*** @param[in]	other	The folder object to copy.
		**/
		SWFolder(const SWFolder &other);

		/**
		*** @brief	Assignment operator.
		***
		*** Opens a new handle to the directory represented by the specified SWFolder object.
		*** If the SWFolder object is currently open it will be closed.
		***
		*** @param[in]	other	The folder object to copy.
		**/
		SWFolder &	operator=(const SWFolder &other);

		/**
		*** @brief	Destructor
		***
		*** The destructor will close the directory handle if open.
		**/
		virtual ~SWFolder();

		/**
		*** @brief	Opens the specified directory.
		***
		*** If the folder is currently opened it will be automatically closed.
		***
		*** @param	path	The directory to be opened
		*** @return			If successful SW_STATUS_SUCCESS is returned, otherwise
		***					an error code is returned.
		**/
		virtual sw_status_t		open(const SWString &path);

		/**
		*** @brief	Read the next matching folder entry.
		***
		*** @param	entry	The SWFileInfo object to be filled in if an entry
		***					is found. The entry is not changed if a matching
		***					entry is not found.
		***
		*** @return			If successful SW_STATUS_SUCCESS is returned, otherwise
		***					an error code is returned.
		**/
		virtual sw_status_t		read(SWFileInfo &entry);

		/**
		*** @brief	Rewind the handle to the directory.
		***
		*** Causes the directory to start reading entries from the start.
		***
		*** @return			If successful SW_STATUS_SUCCESS is returned, otherwise
		***					an error code is returned.
		**/
		virtual sw_status_t		rewind();

		/**
		*** @brief	Closes the handle to the directory
		***
		*** @return			If successful SW_STATUS_SUCCESS is returned, otherwise
		***					an error code is returned.
		**/
		virtual sw_status_t		close();

		/// Determine if this directory is currently open
		bool				isOpen() const			{ return m_hDir != NULL; }

		/// Return the path for this directory handle
		const SWString &	path() const			{ return m_path; }


public: // STATIC methods
		/**
		*** @brief	Creates the specified folder.
		***
		*** @param path			The pathname of the directory to be created.
		*** @param createParent	This flag indicates if parent directories should be created to
		***						accomodate the folder creation.
		*** @param	pfa			A pointer to a file attribute object which is used when a folder
		***						is created. If not supplied default file attributes are used.
		***						The file attributes define how a folder is created and includes
		***						security information.
		***
		*** @return			If successful SW_STATUS_SUCCESS is returned, otherwise
		***					an error code is returned.
		**/
		static sw_status_t	create(const SWString &path, bool createParent=false, SWFileAttributes *pfa=0);

		/**
		*** @brief	Removes the specified folder.
		***
		*** @param path				The pathname of the folder to be removed.
		*** @param removeContents	If true the contents of the directory are removed
		***							to enable the directory to be deleted. If false
		***							this method will fail if folder is not empty.
		***
		*** @return			If successful SW_STATUS_SUCCESS is returned, otherwise
		***					an error code is returned.
		**/
		static sw_status_t	remove(const SWString &path, bool removeContents=false);

		/**
		*** @brief	Push the current directory.
		***
		*** Pushes the current directory onto an internal stack
		*** and attempts to change to the specified directory.
		***
		*** If the setCurrentFolder fails the current directory
		*** is NOT pushed onto the stack.
		***
		*** @return	The result of calling getCurrentFolder
		**/
		static sw_status_t	pushCurrentDirectory(const SWString &dir);


		/**
		*** @brief	Pop the current directory.
		***
		*** Pops the a previously remembered directory name from the internal
		*** stack and changes to that directory.
		***
		*** If the setCurrentFolder fails the current directory
		*** is NOT pushed onto the stack.
		***
		*** @return	The result of calling getCurrentFolder
		**/
		static sw_status_t	popCurrentDirectory();

		/**
		*** @brief	Set the current working directory
		***
		*** @return	true is successful, false otherwise.
		**/
		static sw_status_t	setCurrentDirectory(const SWString &dir);

		/**
		*** @brief	Gets the current working directory
		**/
		static SWString		getCurrentDirectory();

protected: // Methods

		/// Internal method for filename filtering.
		virtual bool		includeFile(const SWString &file);

		/// Internal method for filename filtering.
		virtual bool		excludeFile(const SWString &file);


protected: // Static Members
		static SWArray<SWString>	sm_dirstack;	///< The directory stack for push and pop operations

protected: // Members
		SWString			m_path;				///< The pathname of the directory
#if defined(SW_PLATFORM_WINDOWS)
		HANDLE				m_hDir;				///< The directory handle
		WIN32_FIND_DATA		m_findFileData;		///< Receives the data from the FindFirst/FindNext (Windows only).
		bool				m_haveData;			///< A flag to indicate that the m_findFileData is valid (Windows only).
#else
		sw_handle_t			m_hDir;				///< The directory handle/pointer
		char				*m_buf;				///< The buffer to store results (Unix only)
#endif
		};






#endif
