/**
*** @file		SWHashTable.h
*** @brief		A simple hashtable template class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWHashTable class.
**/

/*********************************************************************************
Copyright (c) 2006-2016 SwiftTec Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWHashTable_h__
#define __SWHashTable_h__

#include <swift/SWGuard.h>
#include <swift/SWHashCode.h>
#include <swift/SWList.h>
#include <swift/SWIterator.h>

SWIFT_NAMESPACE_BEGIN

/**
*** @brief	Templated form of the SWHashTable class.
***
*** SWHashTable is a templated class that implements a simple hash table.
*** Unlike several other collections only the templated form of hash table
*** is available due to the need to be able to calculate hash values on the 
*** elements inserted.
***
*** The hash table allows duplicate objects to be inserted.
***
*** Developers need to provide a method of calculating the hash value of the
*** inserted data. This can be done in one of several ways:
*** - Numeric cast			- If a numeric cast is already available then it will be used.
*** - Derive from SWObject	- The data type is an object that is dervied from SWObject
***							  then the hashCode method can be overridden to determine the hashcode.
*** - SWHashCode cast		- Providing a SWHashCode cast will allow this template to compile.
***
*** The data type used must also have a == and assignment operator.
***
*** @par General Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Safe when synchronized</td>
*** </tr>
*** </table>
***
*** @par Collection Attributes
*** <table class="collection">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>Internal Structure</td>
***		<td>Hash table</td>
*** </tr>
*** <tr>
***		<td>Stored Data Type</td>
***		<td>Templated Data</td>
*** </tr>
*** <tr>
***		<td>Allows duplicate values?</td>
***		<td>No</td>
*** </tr>
*** <tr>
***		<td>Ordered?</td>
***		<td>No</td>
*** </tr>
*** <tr>
***		<td>Sortable?</td>
***		<td>No</td>
*** </tr>
*** <tr>
***		<td>Requirements for stored type</td>
***		<td><ul>
***			<li>Copy constructor
***			<li>operator=
***			<li>operator==
***		</ul></td>
*** </tr>
*** </table>
***
***
*** @see		SWHashCode, SWCollection
*** @author		Simon Sparkes
**/
template<class T>
class SWHashTable : public SWCollection
		{
friend class SWHashTableIterator<T>;

public:
		/// Default constructor - empty list
		SWHashTable(int initialCapacity=23, float loadFactor=0.75, bool autoRehash=false);


		/// Virtual destructor
		virtual ~SWHashTable();


		/**
		*** @brief Removes all the elements from the hash table
		**/
		virtual void		clear();


		/**
		*** @brief	Adds the specified data to the hash table.
		***
		*** The specified data is added to the hash table. The exact position within
		*** the hash table is dependent on the hash value of the data and the capacity
		*** (number of buckets) of the hash table.
		***
		*** @param[in]	data	The data to add to the hash table.
		***
		*** @return				true if the collection is changed because of the addition.
		**/
		bool				add(const T &data);



		/**
		*** @brief	Get the specified data from the hash table
		***
		*** The hash table is searched for data that matches that passed into
		*** the get method (tested using the == operator). If found the data
		*** is copied to the data parameter.
		***
		*** @param[in]	search	Specifies the data to search for.
		*** @param[out]	data	If the data is found this parameter receives a copy
		***						of the data found.
		***
		*** @return			true if the data is found, false otherwise.
		**/
		bool				get(const T &search, T &data) const;



		/**
		*** @brief	Checks to see if the specified data is contained in the hash table.
		***
		*** Checking is for equality is done using the equals method
		*** the hash table is dependent on the hashcode returned by the objects
		*** hashcode method.
		***
		*** @param data[in]			The data to check for.
		*** @param pFoundData[in]	If not NULL and the data was found this points to
		***							an element to receive a copy of the data found.
		***
		*** @return			true if the collection contains the given data, false otherwise.
		**/
		bool				contains(const T &data, T *pFoundData=NULL) const;


		/**
		*** @brief	Remove the specified data from the hash table.
		***
		*** This method removes the first occurrence of the specified data from
		*** the hash table if it is found.
		***
		*** @param[in]	data		The data to search for
		*** @param[in]	pFoundData	If not NULL recevies a copy of the data removed
		***
		*** @return	true if the collection is changed because of the operation
		**/
		bool				remove(const T &data, T *pFoundData=NULL);

		/**
		*** @brief	Rehashes the hash table by doubling the number of buckets.
		**/
		virtual void	rehash();

		/**
		*** Returns an iterator over the elements of this collection.
		***
		*** @param	iterateFromEnd	If false (default) the iterator starts
		***							at the beginning of the vector. If true
		***							the iterator starts at the end of the
		***							vector.
		**/
		SWIterator<T>		iterator(bool iterateFromEnd=false) const;

protected:
		typedef struct htbucket
			{
			public:
				SWListElement<T>	*head;
				SWListElement<T>	*tail;
			} Bucket;

protected:
		int								bucket(const T &data) const;
		void							addElement(SWListElement<T> *pElement);
		void							removeElement(Bucket *pBucket, SWListElement<T> *pElement);

		virtual SWCollectionIterator *	getIterator(bool iterateFromEnd=false) const;


protected:
		SWMemoryManager		*m_pBucketMemoryManager;	///< The memory manager for the buckets
		SWMemoryManager		*m_pElementMemoryManager;	///< The memory manager for the elements
		sw_size_t			m_elemsize;					///< The size of each element to be stored
		float				m_loadFactor;				///< The load factor
		int					m_threshold;				///< The number of elements threshold which when exceeded causes a rehash takes place.
		int					m_bucketCount;				///< The capacity (number of buckets)
		Bucket				*m_bucketArray;				///< An array of buckets
		bool				m_autoRehash;				///< A flag to indicate if rehashing is automatic
		};

SWIFT_NAMESPACE_END

// Include the implementation
#include <swift/SWHashTable.cpp>

#endif
