/**
*** @file		SWFixedSizeMemoryManager.cpp
*** @brief		An memory allocator that sub-allocates from local memory.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWFixedSizeMemoryManager class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWFixedSizeMemoryManager.h>
#include <swift/SWLocalMemoryManager.h>
#include <swift/SWGuard.h>


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




SWFixedSizeMemoryManager::SWFixedSizeMemoryManager(sw_size_t blocksize, sw_size_t minblocks, sw_size_t maxblocks, SWMemoryManager *pMemoryManager) :
	m_blocksize(0),
	m_blocksPerSuper(minblocks),
	m_maxBlocksPerSuper(maxblocks),
	m_superlist(NULL),
	m_freelist(NULL),
	m_pMemoryManager(pMemoryManager)
		{
		if (blocksize != 0)
			{
			// Fix the block size now
			m_blocksize = (((blocksize + sizeof(void *) - 1) / sizeof(void *)) * sizeof(void *));
			}

		if (m_pMemoryManager == NULL) m_pMemoryManager = SWMemoryManager::getDefaultMemoryManager();
		}


SWFixedSizeMemoryManager::~SWFixedSizeMemoryManager()
		{
		MemorySuperBlock	*pNextSuper;

		while (m_superlist != NULL)
			{
			pNextSuper = m_superlist->next;
			m_pMemoryManager->free(m_superlist);
			m_superlist = pNextSuper;
			}
		}


void *
SWFixedSizeMemoryManager::malloc(sw_size_t nbytes)
		{
		SWGuard	guard(this);
		void	*p=NULL;

		if (nbytes > 0 && m_blocksize == 0)
			{
			// This is the first non-zero malloc and we are using JIT sizing.
			// Use the nbytes parameter to set the m_blocksize parameter
			m_blocksize = (((nbytes + sizeof(void *) - 1) / sizeof(void *)) * sizeof(void *));
			}

		if (nbytes > 0 && nbytes <= m_blocksize)
			{
			MemoryBlock	*pBlock=NULL;

			if (m_freelist != NULL)
				{
				// Take the block from the free list
				pBlock = m_freelist;
				m_freelist = pBlock->next;
				}
			else
				{
				MemorySuperBlock	*pSuper=m_superlist;

				if (pSuper == NULL || pSuper->nextfree == pSuper->nelements)
					{
					// We need a new superblock
					pSuper = reinterpret_cast<MemorySuperBlock *>(m_pMemoryManager->malloc(sizeof(MemorySuperBlock) + ((m_blocksize + sizeof(MemoryBlock)) * m_blocksPerSuper)));
					if (pSuper == NULL) throw SW_EXCEPTION(SWMemoryException);

					pSuper->nelements = m_blocksPerSuper;
					pSuper->nextfree = 0;
					pSuper->next = m_superlist;
					m_superlist = pSuper;
					if (m_blocksPerSuper < m_maxBlocksPerSuper)
						{
						m_blocksPerSuper <<= 1;
						if (m_blocksPerSuper > m_maxBlocksPerSuper) m_blocksPerSuper = m_maxBlocksPerSuper;
						}
					}

				pBlock = reinterpret_cast<MemoryBlock *>(reinterpret_cast<char *>(pSuper) + sizeof(MemorySuperBlock) + ((pSuper->nextfree++) * (m_blocksize + sizeof(MemoryBlock))));
				}

			if (pBlock == NULL) throw SW_EXCEPTION(SWMemoryException);

			p = (reinterpret_cast<char *>(pBlock) + sizeof(MemoryBlock));
			pBlock->next = reinterpret_cast<MemoryBlock *>(this);
			}

		return p;
		}


void *
SWFixedSizeMemoryManager::realloc(void *pOldData, sw_size_t nbytes)
		{
		void	*p=NULL;

		if (nbytes == 0 && pOldData != NULL)
			{
			this->free(pOldData);
			}
		else if (nbytes > 0 && nbytes <= m_blocksize)
			{
			p = pOldData;
			}

		return p;
		}



void
SWFixedSizeMemoryManager::free(void *pData)
		{
		if (pData != NULL)
			{
			SWGuard	guard(this);

			MemoryBlock	*pBlock=reinterpret_cast<MemoryBlock *>(reinterpret_cast<char *>(pData) - sizeof(MemoryBlock));

			if (pBlock->next != reinterpret_cast<MemoryBlock *>(this))
				{
				// This block does not belong to us.
				throw SW_EXCEPTION(SWMemoryException);
				}

			pBlock->next = m_freelist;
			m_freelist = pBlock;
			}
		}






