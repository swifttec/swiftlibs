/**
*** @file		SWProcess.cpp
*** @brief		A process controlling class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWProcess class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWProcess.h>
#include <swift/SWTokenizer.h>
#include <swift/SWThread.h>
#include <swift/SWTime.h>
#include <swift/SWFilename.h>
#include <swift/SWTString.h>
#include <swift/SWFileInfo.h>
#include <swift/SWFolder.h>
#include <swift/sw_misc.h>

#include <xplatform/sw_process.h>


#if defined(SW_BASE_USES_ACE)
	#if defined(_MSC_VER)
		#pragma warning(disable: 4312 4244 4311)
	#endif

	#include <ace/OS.h>

	#if defined(_MSC_VER)
		#pragma warning(default: 4312 4244 4311)
	#endif
#else
	#if defined(SW_PLATFORM_WINDOWS)
	#else
		#include <signal.h>
		#include <sys/wait.h>
	#endif
#endif


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




SWProcess::SWProcess() :
	m_hProcess(0),
	m_pid(0),
	m_exitcode(0)
		{
		}


// Destructor
SWProcess::~SWProcess()
		{
		waitUntilStopped();
		}


sw_status_t
SWProcess::create(const SWString &cmdline)
		{
		return create(cmdline, m_pid);
		}



sw_status_t
SWProcess::create(const SWString &program, const SWStringArray &args)
		{
		return create(program, args, m_pid);
		}


// Kill the process by sending it the specified signal
sw_status_t
SWProcess::kill(int sig)
		{
		return kill(m_pid, sig);
		}



// Forcefully terminate the process (may bypass cleanup and therefore leave resources open)
sw_status_t
SWProcess::terminate()
		{
		return terminate(m_pid);
		}


// Test to see if the process is running.
bool
SWProcess::isRunning() const
		{
		return isRunning(m_pid);
		}



// Wait for this process to stop executing
sw_status_t
SWProcess::waitUntilStopped(sw_uint64_t waitms)
		{
		sw_status_t	res;

		if (m_pid != 0)
			{
			res = waitForProcess(m_pid, waitms, m_exitcode);
			}
		else
			{
			res = SW_STATUS_NOT_FOUND;
			}

		return res;
		}


sw_status_t
SWProcess::attach(sw_pid_t pid)
		{
		m_pid = pid;

		return SW_STATUS_SUCCESS;
		}


sw_status_t
SWProcess::detach()
		{
		m_pid = 0;
		return SW_STATUS_SUCCESS;
		}






////////////////////////////////////////////////////////////////////////////////
// Static methods
////////////////////////////////////////////////////////////////////////////////

sw_pid_t
SWProcess::self()
		{
		sw_pid_t	res=0;

#if defined(SW_BASE_USES_ACE)
		res = (sw_pid_t)ACE_OS::getpid();
#else
	#if defined(SW_PLATFORM_WINDOWS)
		res = (sw_pid_t)::GetCurrentProcessId();
	#else
		res = (sw_pid_t)::getpid();
	#endif
#endif

		return res;
		}


sw_status_t
SWProcess::create(const SWString &cmdline, sw_pid_t &pid)
		{
		SWStringArray	args;
		SWString		program;
		SWTokenizer		tok(cmdline, " \t\n", true, true);

		if (tok.hasMoreTokens()) program = tok.nextToken();
		while (tok.hasMoreTokens())
			{
			args.add(tok.nextToken());
			}

		return create(program, args, pid);
		}

SWString
SWProcess::getProgramPath(const SWString &program)
		{
		SWStringArray	pathlist;

		return getProgramPath(program, pathlist, true);
		}


SWString
SWProcess::getProgramPath(const SWString &program, const SWAppConfig &appconfig)
		{
		SWStringArray	pathlist;

		pathlist.add(SWFilename::fix(appconfig.getFolder(SWAppConfig::ProgramDir), SWFilename::PathTypePlatform));

		return getProgramPath(program, pathlist, true);
		}


SWString
SWProcess::getProgramPath(const SWString &program, const SWStringArray &pathlist, bool searchPathListFirst)
		{
		SWString	res, progname=program, progpath;

#if defined(SW_PLATFORM_WINDOWS)
		if (!program.endsWith(".exe", SWString::ignoreCase)) progname += ".exe";
#endif
		
		progpath = progname;
		if (!SWFileInfo::isExecutable(progpath) && searchPathListFirst)
			{
			for (int i=0;!SWFileInfo::isExecutable(progpath) && i<(int)pathlist.size();i++)
				{
				progpath = SWFilename::fix(SWFilename::concatPaths(pathlist.get(i), progname), SWFilename::PathTypePlatform);
				}
			}

		if (!SWFileInfo::isExecutable(progpath))
			{
			// Search the path
			SWString	path;

			sw_misc_getEnvironmentVariable("PATH", path);

			SWTokenizer	tok(path, ";:");

			while (!SWFileInfo::isExecutable(progpath) && tok.hasMoreTokens())
				{
				progpath = SWFilename::fix(SWFilename::concatPaths(tok.nextToken(), progname), SWFilename::PathTypePlatform);
				}
			}

		if (!SWFileInfo::isExecutable(progpath))
			{
			progpath = SWFilename::fix(SWFilename::concatPaths(SWFolder::getCurrentDirectory(), progname), SWFilename::PathTypePlatform);
			}

		if (!SWFileInfo::isExecutable(progpath) && !searchPathListFirst)
			{
			for (int i=0;!SWFileInfo::isExecutable(progpath) && i<(int)pathlist.size();i++)
				{
				progpath = SWFilename::fix(SWFilename::concatPaths(pathlist.get(i), progname), SWFilename::PathTypePlatform);
				}
			}

		if (SWFileInfo::isExecutable(progpath))
			{
			res = progpath;
			}

		return res;
		}


sw_status_t
SWProcess::create(const SWString &program, const SWStringArray &args, sw_pid_t &pid)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		SWString	progpath=getProgramPath(program);

		if (!SWFileInfo::isExecutable(progpath))
			{
			res = SW_STATUS_NOT_EXECUTABLE;
			}
		else
			{

/*
			const char	**feargs;
			int			i, r;
			SWString	tmp(progpath);


			feargs = new const char *[args.size()+2];
			feargs[0] = (const char *)tmp;
			for (i=0;i<args.size();i++) feargs[1+i] = (const char *)args.get(i);
			feargs[i+1] = 0;

			r = ACE_OS::fork();

			if (r == 0)
				{
				// Child process
				r = ACE_OS::execvp((const char *)tmp, (char * const*)feargs);
				if (r < 0) exit(1);
				}
			else if (r > 0)
				{
				pid = r;
				}
			else
				{
				res = r;
				}
*/


#if defined(SW_BASE_USES_ACE)
			const ACE_TCHAR	**feargs;
			int			i, r;
			SWString	tmp=SWFilename::fix(progpath, SWFilename::PathTypePlatform);


			feargs = new const ACE_TCHAR *[args.size()+2];
			feargs[0] = (const ACE_TCHAR *)tmp;
			for (i=0;i<args.size();i++) feargs[1+i] = (const ACE_TCHAR *)args.get(i);
			feargs[i+1] = 0;

			r = ACE_OS::fork_exec((ACE_TCHAR **)feargs);
			if (r > 0) pid = r;

			delete [] feargs;

			if (r <= 0) res = ACE_OS::last_error();
#else // defined(SW_BASE_USES_ACE)

	#if defined(SW_PLATFORM_WINDOWS)
			PROCESS_INFORMATION	pinfo;
			STARTUPINFO			sinfo;
			SWTString			tmp=SWFilename::fix(progpath, SWFilename::PathTypePlatform);
			SWTString			cmdline;

			// if (tmp.first(' ') >= 0) cmdline += '"';
			cmdline += tmp.toQuotedString();
			// if (tmp.first(' ') >= 0) cmdline += '"';

			for (int i=0;i<(int)args.size();i++)
				{
				tmp = args.get(i);

				cmdline += ' ';
				if (tmp.length() == 0)
					{
					cmdline += '"';
					cmdline += '"';
					}
				else
					{
					cmdline += tmp.toQuotedString();
					}
				/*
				if (tmp.length() == 0 || tmp.first(' ') >= 0) cmdline += '"';
				cmdline += tmp;
				if (tmp.length() == 0 || tmp.first(' ') >= 0) cmdline += '"';
				*/
				}

			// According to the documentation the UNICODE version will fail if the command line
			// is a const string, so we make a copy!
			TCHAR		*cp;
			sw_size_t	cmdlen=cmdline.length() * sizeof(TCHAR);

			cp = new TCHAR[cmdlen+1];
			memcpy(cp, cmdline.t_str(), cmdlen+sizeof(TCHAR));

			memset(&sinfo, 0, sizeof(sinfo));
			sinfo.cb = sizeof(sinfo); 
			sinfo.lpTitle = cp;
			sinfo.dwFlags = 0;

			if (::CreateProcess(NULL, cp, NULL, NULL, FALSE, 0, NULL, NULL, &sinfo, &pinfo))
				{
				// CreateProcess succeeded
				pid = (sw_pid_t)pinfo.dwProcessId;
				}
			else
				{
				// CreateProcess failed
				res = ::GetLastError();
				}

			// Free up the buffer copy we had to make.
			delete [] cp;

	#else // defined(SW_PLATFORM_WINDOWS)
			const char	**feargs;
			int			i, r;
			SWString	tmp=SWFilename::fix(progpath, SWFilename::PathTypePlatform);


			feargs = new const char *[args.size()+2];
			feargs[0] = (const char *)tmp;
			for (i=0;i<(int)args.size();i++) feargs[1+i] = (const char *)args.get(i);
			feargs[i+1] = 0;

			// Should we use fork1????
			r = fork();
			if (r < 0)
				{
				// The fork failed.
				res = errno;
				}
			else if (r == 0)
				{
				// We are the child process
				// we need to do an exec to get the new image
				r = execvp((const char *)tmp, (char * const*)feargs);
				if (r < 0)
					{
					// The exec failed - exit immediately
					exit(r);
					}
				}
			else
				{
				// We are the parent
				pid = r;
				}

			delete [] feargs;
	#endif // defined(SW_PLATFORM_WINDOWS)
#endif // defined(SW_BASE_USES_ACE)
			}

		return sw_status_setLastError(res);
		}



/// Check to see if the specified process is running.
bool
SWProcess::isRunning(sw_pid_t pid)
		{
		return (SWProcess::kill(pid, 0) == 0);
		}


SWString
SWProcess::getName(sw_pid_t pid)
		{
		char		buf[1024];
		SWString	res;

		if (sw_process_getname(pid, buf, sizeof(buf)))
			{
			res = buf;
			}

		return res;
		}


/// Terminate the process with the specified process ID.
sw_status_t
SWProcess::terminate(sw_pid_t pid)
		{
#ifndef SIGKILL
	#define SIGKILL 9
#endif
		return (SWProcess::kill(pid, SIGKILL) == 0);
		}



/// Kill the process with the specified process ID.
sw_status_t
SWProcess::kill(sw_pid_t pid, int sig)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

#if defined(SW_PLATFORM_WINDOWS)
		// Under ACE 5.4 this is a null operation, so report it as such
		// but we should be able to do a little better

		if (pid != 0)
			{
			HANDLE	hProc;
			DWORD	access=0;
			
			switch (sig)
				{
				case 0: // test for existence
					access = PROCESS_QUERY_INFORMATION;
					break;

				case SIGKILL:
					access = PROCESS_TERMINATE;
					break;
				}

			hProc = ::OpenProcess(access, FALSE, (DWORD)pid);
			if (hProc == NULL)
				{
				// The process doen't exist or we have no access
				res = GetLastError();
				}
			else
				{
				if (sig == SIGKILL)
					{
					// Simulate SIGKILL
					if (::TerminateProcess(hProc, SIGKILL) == 0) res = GetLastError();
					}

				::CloseHandle(hProc);
				}
			}
		else
			{
			res = SW_STATUS_INVALID_PARAMETER;
			}
#else
		if (::kill(pid, sig) != 0) res = errno;
#endif

		return res;
		}



/**
*** Wait for the process with the specified process ID to exit.
***
*** @param	pid			The ID of the process to wait for. If this has a value of
***						zero then the first process to exit is captured and it's
***						exitcode is returned.
*** @param	waitms		Specifies the time period (in milliseconds) to wait for a
***						a process to die. Can be specified as SW_TIMEOUT_INFINITE.
*** @param	exitcode	Received the exit code of the exiting process.
**/
sw_status_t
SWProcess::waitForProcess(sw_pid_t pid, sw_uint64_t waitms, int &exitcode)
		{
		sw_status_t		res=SW_STATUS_SUCCESS;

#if defined(SW_BASE_USES_ACE)
		ACE_exitcode	aceExitCode=0;
		int				options=0;
		sw_pid_t		rpid=0;
		SWTime			now, when;
		double			timeout=0.0;

#if _MSC_VER < 0x700
		// The older MS compilers (VS6) must convert to signed int before conversion to double
		timeout = (double)(sw_int64_t)waitms / 1000.0;
#else
		timeout = (double)waitms / 1000.0;
#endif

		if (waitms != SW_TIMEOUT_INFINITE) options = WNOHANG;

		now.update();
		while (res == SW_STATUS_SUCCESS)
			{
			rpid = ACE_OS::waitpid(pid, &aceExitCode, options);

			if (rpid < 0)
				{
				res = ACE_OS::last_error();
				break;
				}

			if (rpid == 0)
				{
				if (waitms == SW_TIMEOUT_INFINITE)
					{
					res = ACE_OS::last_error();
					break;
					}

				// Get the current time to check for a timeout
				double	remaining;

				when.update();
				remaining = timeout - ((double)when - (double)now - timeout);
				if (remaining <= 0)
					{
					res = SW_STATUS_TIMEOUT;
					}
				else
					{
					int	ms=(int)(remaining * 1000.0);

					// Sleep for a maximum of 50 ms before re-checking
					if (ms > 50) ms = 50;
					SWThread::sleep(ms);
					}
				}
			else
				{
#if defined(WIN32) || defined(_WIN32)
				exitcode = aceExitCode;
#else
				if (WIFEXITED(aceExitCode)) exitcode = WEXITSTATUS(aceExitCode);
#endif
				//pid = rpid;
				break;
				}
			}

#else // defined(SW_BASE_USES_ACE)
	#if defined(SW_PLATFORM_WINDOWS)
		if (pid != 0)
			{
			HANDLE	hProc;

			hProc = ::OpenProcess(SYNCHRONIZE | PROCESS_QUERY_INFORMATION, FALSE, (DWORD)pid);
			if (hProc == NULL)
				{
				// The process doen't exist or we have no access
				res = ::GetLastError();
				}
			else
				{
				DWORD r, ms;
				
				if (waitms == SW_TIMEOUT_INFINITE) ms = INFINITE;
				else ms = (DWORD)waitms;
				
				r = ::WaitForSingleObject(hProc, ms);
				if (r == WAIT_TIMEOUT) res = SW_STATUS_TIMEOUT;
				else if (r == WAIT_OBJECT_0)
					{
					// Got it!
					if (::GetExitCodeProcess(hProc, &r))
						{
						exitcode = r;
						}
					else
						{
						// Should we return an error here?
						// The wait was successful, BUT we failed to get the error code.
						// so for now we will simply set the exitcode to 0.
						exitcode = 0;
						}
					}
				else res = ::GetLastError();

				::CloseHandle(hProc);
				}
			}
		else
			{
			res = SW_STATUS_INVALID_PARAMETER;
			}

	#else // defined(SW_PLATFORM_WINDOWS)
		int			sl=0;
		int			options=0;
		pid_t		rpid=0;
		SWTime		now, when;
		double		timeout=0.0;

#if _MSC_VER < 0x700
		// The older MS compilers (VS6) must convert to signed int before conversion to double
		timeout = (double)(sw_int64_t)waitms / 1000.0;
#else
		timeout = (double)waitms / 1000.0;
#endif

		if (waitms != SW_TIMEOUT_INFINITE) options = WNOHANG;

		now.update();
		while (res == SW_STATUS_SUCCESS)
			{
			rpid = ::waitpid(pid, &sl, options);

			if (rpid < 0)
				{
				res = errno;
				break;
				}

			if (rpid == 0)
				{
				if (waitms == SW_TIMEOUT_INFINITE)
					{
					res = errno;
					break;
					}

				// Get the current time to check for a timeout
				double	remaining;

				when.update();
				remaining = timeout - ((double)when - (double)now - timeout);
				if (remaining <= 0)
					{
					res = SW_STATUS_TIMEOUT;
					}
				else
					{
					int	ms=(int)(remaining * 1000.0);

					// Sleep for a maximum of 50 ms before re-checking
					if (ms > 50) ms = 50;
					SWThread::sleep(ms);
					}
				}
			else
				{
				if (WIFEXITED(sl)) exitcode = WEXITSTATUS(sl);
				break;
				}
			}
	#endif // defined(SW_PLATFORM_WINDOWS)
#endif // defined(SW_BASE_USES_ACE)

		return res;
		}





