/**
*** @file		SWRegistryKey.h
*** @brief		An object representing a single value contained in a registry key.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWRegistryKey class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWRegistryKey_h__
#define __SWRegistryKey_h__

#undef SW_REGISTRY_SUPPORT_NETWORK_ACCESS

#include <swift/swift.h>
#include <swift/SWObject.h>
#include <swift/SWArray.h>
#include <swift/SWStringArray.h>
#include <swift/SWFile.h>

#undef SW_REGISTRY_SUPPORT_NETWORK_ACCESS
#if defined(SW_PLATFORM_WINDOWS)
	#define SW_REGISTRY_SUPPORT_NATIVE
	#undef SW_REGISTRY_SUPPORT_EMULATED
#else
	#undef SW_REGISTRY_SUPPORT_NATIVE
	#define SW_REGISTRY_SUPPORT_EMULATED
#endif

#include <swift/SWRegistryValue.h>

/// An enumeration of supported registry pre-defined keys
enum SWRegistryHive
		{
		SW_REGISTRY_NONE=-1,
		SW_REGISTRY_CLASSES_ROOT=0,
		SW_REGISTRY_CURRENT_USER,
		SW_REGISTRY_LOCAL_MACHINE
		};

/**
*** @brief	An object representing a single value contained in a registry key.
***
*** This object is used to represent a value that has been extracted from the registry
*** or a value to be written to the registry. As all registry values have names this
*** class is dervied from the SWNamedKey class. It extends the SWNamedKey class by
*** adding to it a registry value type (enum RegKeyType) which defines the type as
*** extracted from the registry, or that should be written to it.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWRegistryKey : public SWObject
		{
public:
		/**
		*** @brief	Default constructor
		***
		*** The default constructor constructs a registry key object but does
		*** not attempt to open any key. Most operations (except open) will 
		*** return an error indicating the object is not in a valid state
		*** until the caller uses the open method to open a key.
		**/
		SWRegistryKey();

		/// Copy constructor
		SWRegistryKey(const SWRegistryKey &other);

		/// Assignment operator
		SWRegistryKey &		operator=(const SWRegistryKey &other);

		/**
		*** @brief	Construct and open a key
		***
		*** This constructor constructs a registry key object and then calls the
		*** open method to perform the open.
		***
		*** @param[in]	pdk			The pre-defined key to open. See open for details.
		*** @param[in]	key			The key to open. See open for details.
		*** @param[in]	createflag	A flag indicating if the key is to be created. See open for details.
		**/
		SWRegistryKey(SWRegistryHive pdk, const SWString &key, bool createflag=false);

#ifdef SW_REGISTRY_SUPPORT_NETWORK_ACCESS
		/**
		*** @brief	Construct and open a key on the specified server.
		***
		*** This constructor constructs a registry key object and then calls the
		*** open method to perform the open.
		***
		*** @param[in]	hostname	The name of the server which registry should be opened. See open for details.
		*** @param[in]	pdk			The pre-defined key to open. See open for details.
		*** @param[in]	key			The key to open. See open for details.
		*** @param[in]	createflag	A flag indicating if the key is to be created. See open for details.
		**/
		SWRegistryKey(const SWString &hostname, SWRegistryHive pdk, const SWString &key, bool createflag=false);
#endif // SW_REGISTRY_SUPPORT_NETWORK_ACCESS

		/// Virtual destrcutor
		virtual ~SWRegistryKey();


#ifdef SW_REGISTRY_SUPPORT_NETWORK_ACCESS
		/// Return the server name (may be blank)
		const SWString &	host() const						{ return m_hostname; }
#endif // SW_REGISTRY_SUPPORT_NETWORK_ACCESS

		/// Return the key name (may be blank)
		const SWString &	name() const						{ return m_name; }
		
		/// Check to see if this is a remote registry key
		bool				isRemote() const;

		/// Check to see if this is a local registry key
		bool				isLocal() const;


		/**
		*** @brief	Connect to the registry and open the specified key
		***
		*** This method is the same as calling the method
		*** open(const SWString &hostname, SWRegistryHive pdk, const SWString &key, bool createflag)
		*** with a blank hostname to connect to the local host.
		***
		*** @param[in]	pdk			The pre-defined key to open. See open for details.
		*** @param[in]	key			The key to open. See open for details.
		*** @param[in]	createflag	A flag indicating if the key is to be created. See open for details.
		***
		*** @return		SW_STATUS_SUCCESS if successful, an error code otherwise.
		**/
		sw_status_t			open(SWRegistryHive pdk, const SWString &key, bool createflag=false);

		
#ifdef SW_REGISTRY_SUPPORT_NETWORK_ACCESS
		/**
		*** @brief	Connect to the registry on the specified host and open the specified key
		***
		*** This method connects to the registry service on the specified host and attempts to
		*** open the key defined by the pdk (pre-defined key) and key parameters. If the createflag
		*** parameter is true and the key does not exist this method will attempt to create the
		*** specified key.
		***
		*** The hostname parameter is simply the name of the host to connect to. Unlike the underlying
		*** Microsoft function the hostname <b>does not</b> need to start with a double backslash. If the
		*** hostname parameter is blank then the local machine is used for the connection.
		***
		*** The pre-defined key defines which part of the registry to connect to. A key from the 
		*** SWRegistryHive enumeration should be used. The key parameter specifies the key to open
		*** (relative to the predefined key).
		***
		*** @note
		*** For portable code the hostname should not contain double backslash at the start of the string
		*** but this is supported for ease of porting existing software.
		***
		*** @param[in]	hostname	The name of the host to connect to. If blank then the local registry is used.
		*** @param[in]	pdk			Specifies which pre-defined key to open.
		*** @param[in]	key			The key (relative to the pre-defined key) to open.
		*** @param[in]	createflag	If true the key specified is created if it does not already exist.
		***
		*** @return		SW_STATUS_SUCCESS if successful, an error code otherwise.
		**/
		sw_status_t			open(const SWString &hostname, SWRegistryHive pdk, const SWString &key, bool createflag=false);
#endif // SW_REGISTRY_SUPPORT_NETWORK_ACCESS

		/**
		*** @brief	Close the registry key
		***
		*** Closes the handle to the current registry key so that this object
		*** can be re-used to access another key.
		***
		*** @return		SW_STATUS_SUCCESS if successful, an error code otherwise.
		**/
		sw_status_t			close();

		/**
		*** @brief	Flush any changes to the registry
		***
		*** This method causes any registry changes to be flushed from memory.
		***
		*** @return		SW_STATUS_SUCCESS if successful, an error code otherwise.
		**/
		sw_status_t			flush();

		/// Test to see if this key is open
		bool				isOpen() const;

/**
*** @name	Value operations
**/
//@{
		/**
		*** @brief Get an array of value names
		***
		*** This method enumerates all of the values on the current registry key
		*** and adds their names to the specified string array.
		***
		*** @param[out]	sa	The string array to receive the value names.
		***
		*** @return		SW_STATUS_SUCCESS if successful, an error code otherwise.
		**/
		sw_status_t			valueNames(SWArray<SWString> &sa);
		sw_status_t			valueNames(SWStringArray &sa);

		/**
		*** @brief	Test to see if the specified value name exists in the current key
		***
		*** This method attempts to access the specified value in the current key
		*** and returns a boolean indicating if the value exists.
		***
		*** @param[in]	name	The name of the value to check.
		***
		*** @return		true if the specified value exists, false otherwise.
		**/
		bool				valueExists(const SWString &name);

		/**
		*** @brief	Get the data of the specified value
		***
		*** This method attempts to retrieve the data for the specified value
		*** and if successful updates the given SWRegistryValue object.
		***
		*** @param[in]		name	The name of the value to fetch
		*** @param[out]		value	Receives the value data if found.
		***
		*** @return		SW_STATUS_SUCCESS if successful, an error code otherwise.
		**/
		sw_status_t			getValue(const SWString &name, SWRegistryValue &value);
		SWRegistryValue		getValue(const SWString &name);

		/**
		*** @brief	Set the data of the specified value
		***
		*** This method attempts to set the data for the specified value.
		*** If the value does not exist it will be created. The name of the
		*** value is specified in the SWRegistryValue object.
		***
		*** @param[in]		value	Receives the value data if found.
		***
		*** @return		SW_STATUS_SUCCESS if successful, an error code otherwise.
		**/
		sw_status_t			setValue(const SWRegistryValue &value);

		/**
		*** @brief	Delete the specified value.
		***
		*** This method attempts to delete the specified value.
		***
		*** @param[in]		name	The name of the value to delete.
		***
		*** @return		SW_STATUS_SUCCESS if successful, an error code otherwise.
		**/
		sw_status_t			deleteValue(const SWString &name);

		/**
		*** @brief	Rename the specified value.
		***
		*** This method attempts to rename the specified value using the new name
		*** given. If the new value name already exists this method will return an
		*** error.
		***
		*** @param[in]		oldname		The existing name of the value to be renamed.
		*** @param[in]		newname		The desired name of the value.
		***
		*** @return		SW_STATUS_SUCCESS if successful, an error code otherwise.
		**/
		sw_status_t			renameValue(const SWString &oldname, const SWString &newname);
//@}



/**
*** @name	Sub-key operations
***
*** All sub-key operations act relative to the current registry key
*** represented by this object.
**/
//@{
		/**
		*** @brief Get an array of sub-key names
		***
		*** This method enumerates all of the sub-keys on the current registry key
		*** and adds their names to the specified string array.
		***
		*** @param[out]	sa	The string array to receive the keys names.
		***
		*** @return		SW_STATUS_SUCCESS if successful, an error code otherwise.
		**/
		sw_status_t			keyNames(SWArray<SWString> &sa);
		sw_status_t			keyNames(SWStringArray &sa);

		/**
		*** @brief	Test to see if the specified sub-key name exists in the current key
		***
		*** This method attempts to access the specified sub-key in the current key
		*** and returns a boolean indicating if the sub-key exists.
		***
		*** @param[in]	name	The name of the sub-key to check.
		***
		*** @return		true if the specified sub-key exists, false otherwise.
		**/
		bool				keyExists(const SWString &name);

		/**
		*** @brief	Opens an existing sub-key
		***
		*** This method attempts to open the specified sub-key. If it exists and
		*** the open is successful the key parameter receives the sub-key data
		*** and can be used for further operations on the key.
		***
		*** @param[in]	name	The name of the sub-key to open.
		*** @param[out]	key		Receives the sub-key data if the open is successful.
		***
		*** @return		SW_STATUS_SUCCESS if successful, an error code otherwise.
		**/
		sw_status_t			openKey(const SWString &name, SWRegistryKey &key);

		/**
		*** @brief	Open or create a sub-key
		***
		*** This method attempts to open the specified sub-key. If it exists and
		*** the open is successful the key parameter receives the sub-key data
		*** and can be used for further operations on the key. If the sub-key does
		*** not exists this method will attempt to create it,
		***
		*** @param[in]	name	The name of the sub-key to open.
		*** @param[out]	key		Receives the sub-key data if the open is successful.
		***
		*** @return		SW_STATUS_SUCCESS if successful, an error code otherwise.
		**/
		sw_status_t			createKey(const SWString &name, SWRegistryKey &key);

		/**
		*** @brief	Delete the specified key.
		***
		*** This method attempts to delete the specified key. If the key contains
		*** values or sub-keys these will be deleted.
		***
		*** @param[in]		name	The name of the key to delete.
		***
		*** @return		SW_STATUS_SUCCESS if successful, an error code otherwise.
		**/
		sw_status_t			deleteKey(const SWString &name);

		/**
		*** @brief	Rename the specified key.
		***
		*** This method attempts to rename the specified key using the new name
		*** given. If the new key name already exists this method will return an
		*** error.
		***
		*** @param[in]		oldname		The existing name of the key to be renamed.
		*** @param[in]		newname		The desired name of the key.
		***
		*** @return		SW_STATUS_SUCCESS if successful, an error code otherwise.
		**/
		sw_status_t			renameKey(const SWString &oldname, const SWString &newname);

//@}
		
		sw_status_t			copyKeysAndValues(SWRegistryKey &rk);

/**
*** @name	Static Methods
**/
//@{
public: // Static methods
		
		/// Get the name for the given hive
		static SWString			hiveAsString(SWRegistryHive hive);

		/// Get a hive value to match the given string
		static SWRegistryHive	hiveFromString(const SWString &name);
//@}

protected:
		SWRegistryHive	m_pdk;			///< The containing pre-defined key
		SWString		m_name;			///< The name of the open key
#ifdef SW_REGISTRY_SUPPORT_NATIVE
		sw_hkey_t		m_hKey;			///< The handle to the registry key
#endif
#ifdef SW_REGISTRY_SUPPORT_NETWORK_ACCESS
		SWString		m_hostname;		///< The name of the host
#endif // SW_REGISTRY_SUPPORT_NETWORK_ACCESS
		};

#endif
