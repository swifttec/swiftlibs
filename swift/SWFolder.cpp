/**
*** @file		SWFolder.cpp
*** @brief		A class to represent a handle to a directory.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWFolder class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




// Needed for precompiled header support
#include "stdafx.h"

#if defined(SW_PLATFORM_UNIX)
	#include <dirent.h>
	#include <sys/stat.h>
	#include <sys/types.h>
#endif

#include <swift/swift.h>
#include <swift/SWFolder.h>
#include <swift/SWFile.h>
#include <swift/SWFileInfo.h>
#include <swift/SWFilename.h>


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





#undef USES_REGEXP

/*
** Default constructor.
*/
SWFolder::SWFolder(const SWString &path, const SWString & /* filter */) :
#if defined(SW_PLATFORM_UNIX)
	m_buf(0),
#endif
    m_hDir(0)
		{
		open(path);
		}




/*
** Copy constructor
*/
SWFolder::SWFolder(const SWFolder &other) :
	SWObject(),
#if defined(SW_PLATFORM_UNIX)
	m_buf(0),
#endif
    m_hDir(0)
		{
		*this = other;
		}


/*
** Destructor
*/
SWFolder::~SWFolder()
		{
		close();
		}


/*
** Assignment operator
*/
SWFolder &
SWFolder::operator=(const SWFolder &other)
		{
		close();
		open(other.m_path);

		return *this;
		}


/*
** Opens the directory.
*/
sw_status_t
SWFolder::open(const SWString &path)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (isOpen())
			{
			res = SW_STATUS_ALREADY_OPEN;
			}
		else if (path.isEmpty())
			{
			res = SW_STATUS_INVALID_PARAMETER;
			}
		else
			{
#if defined(SW_PLATFORM_WINDOWS)
			SWString search;

			if (path == "/")
				{
				// Windows doesn't like a single / for a path spec
				// so quietly change it to a backslash
				search = SWFilename::concatPaths("\\", "*");
				}
			else 
				{
				search = SWFilename::concatPaths(path, "*");
				}

			m_hDir = FindFirstFile(search, &m_findFileData);

			if (m_hDir != INVALID_HANDLE_VALUE)
				{
				m_haveData = true;
				}
			else
				{
				m_hDir = NULL;
				res = SW_STATUS_INVALID_PARAMETER;
				}
	
#elif defined(SW_PLATFORM_UNIX)
			m_buf = new char[sizeof(struct dirent) + pathconf(path, _PC_NAME_MAX)];
			m_hDir = opendir(path);
			if (m_hDir == NULL) res = errno;
#else
			res = SW_STATUS_UNSUPPORTED_OPERATION;
#endif
			}

		if (res == SW_STATUS_SUCCESS) m_path = path;

		return res;
		}


/*
** Read the next matching directory entry.
*/
sw_status_t
SWFolder::read(SWFileInfo &entry)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (m_hDir != NULL)
			{
			bool	gotit=false;

			// We use a loop here to support filtered folders
			while (!gotit)
				{
#if defined(SW_PLATFORM_WINDOWS)
				// Windows specific code
				if (!m_haveData)
					{
					// We have no previously cached data so read the next entry
					m_haveData = (FindNextFile(m_hDir, &m_findFileData) != 0);
					}

				if (m_haveData)
					{
					m_haveData = false;

					// If we have a filter, apply it
					if (excludeFile(m_findFileData.cFileName)) continue;

					entry.reset();

					entry._directory = m_path;
					entry._name = m_findFileData.cFileName;
					entry._dosname = m_findFileData.cAlternateFileName;

					entry._dosdir = entry._directory;

					entry._createTime = m_findFileData.ftCreationTime;
					entry._lastAccessTime = m_findFileData.ftLastAccessTime;
					entry._lastStatusChangeTime = entry._lastModificationTime = m_findFileData.ftLastWriteTime;

					entry._size = ((sw_uint64_t)m_findFileData.nFileSizeHigh << 32) | (sw_uint64_t)m_findFileData.nFileSizeLow;

					entry._attributes.set(entry._name, 0, m_findFileData.dwFileAttributes, false);
					entry._validData = SWFileInfo::FileName | SWFileInfo::FileTime | SWFileInfo::FileSize | SWFileInfo::FileAttributes;
					gotit = true;
					}
				else
					{
					res = SW_STATUS_NO_MORE_DATA;
					break;
					}
#else
				// Unix specific code
				struct dirent	*dp=NULL;

#if defined(SW_PLATFORM_SOLARIS)
				dp = readdir_r((DIR *)m_hDir, (struct dirent *)m_buf);
#else
				readdir_r((DIR *)m_hDir, (struct dirent *)m_buf, &dp);
#endif
				if (dp != NULL)
					{
					// If we have a filter, apply it
					if (excludeFile(dp->d_name)) continue;

					// Otherwise store it.
					//entry.set(_path, de);
					entry.reset();
					
					entry._directory = m_path;
					entry._name = dp->d_name;

					// it makes no sense to this in Unix (for now) so just use the normal name
					entry._dosname = entry._name;
					entry._dosdir = entry._directory;

					entry._validData = SWFileInfo::FileName;

					gotit = true;
					}
				else
					{
					res = SW_STATUS_NO_MORE_DATA;
					break;
					}
#endif
				}
			}
		else res = SW_STATUS_INVALID_HANDLE;

		return res;
		}


/*
** Rewind the handle to the directory.
**
** Causes the directory to start reading entries from the start.
*/
sw_status_t
SWFolder::rewind()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (m_hDir != NULL)
			{
#if defined(SW_PLATFORM_UNIX)
			rewinddir((DIR *)m_hDir);
#else
			SWString	tmp=path();

			close();
			res = open(tmp);
#endif
			}
		else res = SW_STATUS_INVALID_HANDLE;

		return res;
		}


/*
** Closes the handle to the directory
*/
sw_status_t
SWFolder::close()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (m_hDir != NULL)
			{
#if defined(SW_PLATFORM_WINDOWS)
			::FindClose(m_hDir);
#elif defined(SW_PLATFORM_UNIX)
			if (closedir((DIR *)m_hDir) != 0) res = errno;
#else
			res = SW_STATUS_UNSUPPORTED_OPERATION;
#endif

			m_hDir = NULL;
			}
		else res = SW_STATUS_INVALID_HANDLE;

#if defined(SW_PLATFORM_UNIX)
		delete [] (char *)m_buf;
		m_buf = 0;
#endif

		return res;
		}


/*
** Check to see if the given file name would be included or excluded
** by the current file filter.
*/
bool
SWFolder::includeFile(const SWString & /*file*/)
		{
		return true;
		}


/*
** Check to see if the given file name would be included or excluded
** by the current file filter.
*/
bool
SWFolder::excludeFile(const SWString & /*file*/)
		{
		return false;
		}



sw_status_t
SWFolder::create(const SWString &path, bool createParent, SWFileAttributes *pfa)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		int			mode=0777;

		if (pfa != NULL)
			{
			// Since we know the bottom 9 bits match standard unix we can use a straight mask
			mode = pfa->attributes() & 0777;
			}

#if defined(SW_PLATFORM_WINDOWS)
		if (!::CreateDirectory(path, NULL)) res = ::GetLastError();
#elif defined(SW_PLATFORM_UNIX)
		if (mkdir(path, (mode_t)mode) != 0) res = errno;
#else
		res = SW_STATUS_UNSUPPORTED_OPERATION;
#endif

		if (res != SW_STATUS_SUCCESS)
			{
			if (createParent)
				{
				SWString	parentDir = SWFilename::dirname(path);

				if (!SWFileInfo::isDirectory(parentDir) 
					&& parentDir != _T(".") 
					&& parentDir != _T("/")
					&& parentDir != _T("\\")
					)
					{
					res = create(parentDir, true, pfa);
					if (res != SW_STATUS_SUCCESS) return res;
					}
				}

#if defined(SW_PLATFORM_WINDOWS)
			if (!::CreateDirectory(path, NULL)) res = ::GetLastError();
#elif defined(SW_PLATFORM_UNIX)
			if (mkdir(path, (mode_t)mode) != 0) res = errno;
#else
			res = SW_STATUS_UNSUPPORTED_OPERATION;
#endif
			}

		return res;
		}


sw_status_t
SWFolder::remove(const SWString &path, bool removeContents)
		{
		sw_status_t	r, res=SW_STATUS_SUCCESS;

		if (removeContents)
			{
			SWFolder	folder(path);
			SWFileInfo	info;
			SWString	filename;
			
			while (folder.read(info) == SW_STATUS_SUCCESS)
				{
				if (info == "." || info == "..") continue;

				filename = SWFilename::concatPaths(path, info.getName());

				if (info.isDirectory())	r = remove(filename, true);
				else r = SWFile::remove(filename);

				// Return the FIRST error, not the last
				if (r != SW_STATUS_SUCCESS && res == SW_STATUS_SUCCESS) res = r;
				}
			}

#if defined(SW_PLATFORM_WINDOWS)
		if (!::RemoveDirectory(path)) res = GetLastError();
#elif defined(SW_PLATFORM_UNIX)
		if (::rmdir(SWString(path)) != 0) res = errno;
#else
		res = SW_STATUS_UNSUPPORTED_OPERATION;
#endif

		return res;
		}


SWArray<SWString>	SWFolder::sm_dirstack;


/**
*** Pushes the current directory onto an internal stack
*** and attempts to change to the specified directory.
***
*** If the setCurrentDirectory fails the current directory
*** is NOT pushed onto the stack.
***
*** @return	The result of calling setCurrentDirectory on the specified directory
**/
sw_status_t
SWFolder::pushCurrentDirectory(const SWString &dir)
		{
		sw_status_t	res;
		SWString	currdir;

		currdir = getCurrentDirectory();
		res = setCurrentDirectory(dir);
		if (res == SW_STATUS_SUCCESS) sm_dirstack.push(currdir);

		return res;
		}


/**
*** Pops the a previously remembered directory name from the internal
*** stack and changes to that directory.
***
*** @return	The result of calling setCurrentDirectory on the popped directory.
**/
sw_status_t
SWFolder::popCurrentDirectory()
		{
		return setCurrentDirectory(sm_dirstack.pop());
		}


/**
*** Set the current working directory
***
*** @return	true is successful, false otherwise.
**/
sw_status_t
SWFolder::setCurrentDirectory(const SWString &dir)
		{
		sw_status_t	result=SW_STATUS_SUCCESS;

#if defined(WIN32) || defined(_WIN32)
		if (!SetCurrentDirectory(SWString(dir))) result = GetLastError();
#else
		if (chdir(SWString(dir)) != 0) result = errno;
#endif

		return result;
		}



/**
*** Gets the current working directory
**/
SWString
SWFolder::getCurrentDirectory()
		{
		SWString	dir;

#if defined(WIN32) || defined(_WIN32)
		TCHAR	tmp[1024];

		if (GetCurrentDirectory(1024, tmp) != 0) dir = tmp;
#else
		char	tmp[PATH_MAX+1];

		if (getcwd(tmp, sizeof(tmp)) != NULL) dir = tmp;
#endif

		return dir;
		}






