/**
*** @file		SWLockableObject.cpp
*** @brief		The generic base object from which all lockable objects are defined
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWLockableObject class.
***
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWLockableObject.h>
#include <swift/SWThreadMutex.h>
#include <swift/SWException.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




// Constructor
SWLockableObject::SWLockableObject() :
    m_pMutex(NULL)
		{
		}


// Destructor
SWLockableObject::~SWLockableObject()
		{
		// If this object has it's own mutex, delete it.
		if (m_pMutex != NULL && m_pMutex->deleteOnClose())
			{
			delete m_pMutex;
			}

		m_pMutex = NULL;
		} 



sw_status_t
SWLockableObject::synchronize()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (m_pMutex == NULL)
			{
			res = synchronize(new SWThreadMutex(true), true);
			}

		return res;
		}


sw_status_t
SWLockableObject::synchronize(SWMutex *pMutex, bool deleteOnClose)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (pMutex != m_pMutex)
			{
			if (m_pMutex != NULL)
				{
				if (m_pMutex->deleteOnClose())
					{
					delete m_pMutex;
					}

				m_pMutex = NULL;
				}

			m_pMutex = pMutex;

			if (m_pMutex != NULL)
				{
				if (deleteOnClose)
					{
					m_pMutex->m_flags |= SW_MUTEX_DELETE_ON_CLOSE;
					}
				else
					{
					m_pMutex->m_flags &= ~SW_MUTEX_DELETE_ON_CLOSE;
					}
				}
			} 

		return res;
		}


// Lock the object (if synchronised).
sw_status_t
SWLockableObject::lock(sw_uint64_t waitms) const
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (m_pMutex != NULL)
			{
			res = m_pMutex->acquire(waitms);
			}

		return res;
		}


// Lock the object for reading (if synchronised).
sw_status_t
SWLockableObject::lockForReading(sw_uint64_t waitms) const
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (m_pMutex != NULL)
			{
			res = m_pMutex->acquireReadLock(waitms);
			}

		return res;
		}


// Lock the object for writing (if synchronised).
sw_status_t
SWLockableObject::lockForWriting(sw_uint64_t waitms) const
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (m_pMutex != NULL)
			{
			res = m_pMutex->acquireWriteLock(waitms);
			}

		return res;
		}


// Unlock the object (if synchronised).
sw_status_t
SWLockableObject::unlock() const
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (m_pMutex != NULL)
			{
			res = m_pMutex->release();
			}

		return res;
		}




