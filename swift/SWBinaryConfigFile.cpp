/****************************************************************************
**	SWBinaryConfigFile.C	Class for handling a hierarchical binary config file
****************************************************************************/

#include "stdafx.h"

#include <xplatform/sw_math.h>
#include <swift/SWGuard.h>
#include <swift/SWTokenizer.h>
#include <swift/SWPagedFile.h>
#include <swift/SWException.h>
#include <swift/SWMemoryInputStream.h>
#include <swift/SWMemoryOutputStream.h>
#include <swift/SWBinaryConfigFile.h>
#include <swift/SWConfigValue.h>
#include <swift/SWConfigFile.h>



// Define this to debug this class
#undef DEBUG_SWBinaryConfigFile

#ifdef DEBUG_SWBinaryConfigFile
#include <swift/SWLog.h>
#endif


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif

#define MIN_RECORD_LENGTH	12

#define BCF_MAGIC_NUMBER	0x0bcff11e

#define BCF_MAGIC_RECORD_POS	0
#define BCF_ROOT_RECORD_POS	4
#define BCF_HEAD_RECORD_POS	8
#define BCF_TAIL_RECORD_POS	12
#define BCF_DATA_RECORD_POS	16


#ifdef DEBUG_SWBinaryConfigFile
static SWLog	log;
#endif

class BCFOutputStream : public SWMemoryOutputStream
		{
public:
		sw_status_t		writePaddedData(const void *pData, sw_uint16_t len, int pad);
		sw_status_t		writePaddedString(const SWString &str, int pad);
		};


sw_status_t
BCFOutputStream::writePaddedData(const void *pData, sw_uint16_t len, int pad)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		res = writeData(pData, len);
		while (res == SW_STATUS_SUCCESS && (len % pad) != 0) 
			{
			write((sw_uint8_t)0);
			len++;
			}

		return res;
		}


sw_status_t
BCFOutputStream::writePaddedString(const SWString &str, int pad)
		{
		const char  *np=str.c_str();
		sw_uint16_t	len;

		len = (sw_uint16_t)strlen(np);
		write(len);			// Name length		(2 bytes)

		return writePaddedData(np, len, pad);
		}


class BCFInputStream : public SWMemoryInputStream
		{
public:
		SWString	readPaddedString(int pad);
		};


/**
*** Writes a string to a stream with embedded length
*** Optionally pads to n bytes
**/
SWString
BCFInputStream::readPaddedString(int pad)
		{
		SWString	str;
		sw_int16_t	namelen;
		char		*tmp;

		read(namelen);

		tmp = new char[namelen+1];
		for (int i=0;i<namelen;i++)
			{
			read(tmp[i]);
			}
		tmp[namelen] = 0;
		str = tmp;
		delete [] tmp;

		// Add name padding if required
		while ((namelen % pad) != 0) 
			{
			sw_int8_t	t;

			read(t);
			namelen++;
			}

		return str;
		}




/**********************************************************************************/
SWBinaryConfigValue::SWBinaryConfigValue(SWConfigKey *owner, const SWString &name) :
    SWConfigValue(owner, name),
    _fileRecordPosition(-1),
    _fileRecordLength(0)
		{
		}


void	
SWBinaryConfigValue::setFileRecordInfo(int position, int length)	
		{
		_fileRecordPosition = position;
		_fileRecordLength = length;
		}


/**********************************************************************************/
SWBinaryConfigKey::SWBinaryConfigKey(SWConfigKey *owner, const SWString &name, bool ignoreCase) :
    SWConfigKey(owner, name, ignoreCase),
    _fileRecordPosition(-1),
    _fileRecordLength(0)
		{
		}


void	
SWBinaryConfigKey::setFileRecordInfo(int position, int length)	
		{
		_fileRecordPosition = position;
		_fileRecordLength = length;
		}


/**
*** This method checks to see if the given sub-key already exists.
*** If it does it returns a pointer to it, if not the key is created
*** and returned.
***
*** @brief	Find or create a sub-key of the given name
**/
SWConfigKey *
SWBinaryConfigKey::findOrCreateKey(const SWString &name)
		{
		SWGuard		guard(this);
		SWConfigKey	*key = findKey(name);

		if (key == NULL)
			{
			key = new SWBinaryConfigKey(this, name, getIgnoreCase());
			_keyVec.add(key);

			setChangedFlag(KeyCreated, key);
			}

		return key;
		}



/**
*** This method checks to see if the given value already exists.
*** If it does it returns a pointer to it, if not the value is created
*** and returned.
***
*** @brief	Find or create a value of the given name
**/
SWConfigValue *
SWBinaryConfigKey::findOrCreateValue(const SWString &name)
		{
		SWGuard		guard(this);
		SWConfigValue	*value = findValue(name);

		if (value == NULL)
			{
			value = new SWBinaryConfigValue(this, name);
			_valueVec.add(value);

			// Notify parent of change and set changed flag
			setChangedFlag(ValueCreated, value);
			}

		return value;
		}



/**********************************************************************************/
SWBinaryConfigFile::SWBinaryConfigFile() :
    SWConfigFile(_T(""), true, true, false),
    _pagesize(1024),
    _newWritePos(BCF_DATA_RECORD_POS),
    _rootKeyPos(0),
    _rootKeyLen(0)
		{
#ifdef DEBUG_XBinaryConfigFile
		log.setHandler("stdout:");
		//log.setHandler("file:C:/Users/Simon/Development/projects/LoyaltyTracker/bcf.log");
		log.autoflush(true);
		log.application("XBinaryConfigFile");
		log.level(X_LOG_LEVEL_MAXIMUM);
#endif
		}


SWBinaryConfigFile::SWBinaryConfigFile(const SWString &filename, bool readonly, int pagesize, bool ignoreCase) :
    SWConfigFile(filename, readonly, true, ignoreCase),
    _pagesize(pagesize),
    _newWritePos(BCF_DATA_RECORD_POS),
    _rootKeyPos(0),
    _rootKeyLen(0)
		{
#ifdef DEBUG_SWBinaryConfigFile
		log.setHandler("stdout:");
		//log.setHandler("file:C:/Users/Simon/Development/projects/LoyaltyTracker/bcf.log");
		log.autoflush(true);
		log.application("SWBinaryConfigFile");
		log.level(SW_LOG_LEVEL_MAXIMUM);
#endif

		// Load this structure from the file
		readFromFile(filename);
		}


// Destructor
SWBinaryConfigFile::~SWBinaryConfigFile()
		{
		flush();
		}




// Calculate the record length of a SWConfigValue
int
SWBinaryConfigFile::calculateRecordLength(SWConfigValue *pValue)
		{
		int	len, namelen, datalen;

		namelen = (int)pValue->name().length();
		if ((namelen % 2) != 0) namelen++;
		
		datalen = (int)pValue->length();
		if ((datalen % 2) != 0) datalen++;

		// Calculate the record length
		len = sizeof(sw_int32_t)	// Record Length	4 bytes
			+ sizeof(sw_int16_t)	// Name Length		2 bytes
			+ namelen				// Name Data		(rounded to 2 byte boundary)
			+ sizeof(sw_int16_t)	// Type				2 bytes (0xffff = free space record)
			+ sizeof(sw_int32_t)	// Data Length		2 bytes
			+ datalen;				// Data				(rounded to 2 byte boundary)

		// We have a minimum record length of MIN_RECORD_LENGTH bytes to support the free chain
		if (len < MIN_RECORD_LENGTH) len = MIN_RECORD_LENGTH;

		return len;
		}



// Calculate the record length of a SWConfigKey
int
SWBinaryConfigFile::calculateRecordLength(SWConfigKey *key)
		{
		int	len, namelen;

		namelen = (int)key->name().length();
		if ((namelen % 2) != 0) namelen++;
		
		// Calculate the record length
		len = sizeof(sw_int32_t)				// Record Length	4 bytes
			+ (sizeof(sw_int16_t))	 			// Name Length		2 bytes
			+ namelen					// Name Data		(rounded to 2 byte boundary)
			+ (sizeof(sw_int16_t))				// Value Count		2 bytes
			+ (key->valueCount() * sizeof(sw_int32_t))	// Value Pointers	4 bytes each
			+ (sizeof(sw_int16_t))				// SubKey Count		2 bytes
			+ (key->keyCount() * sizeof(sw_int32_t)); 	// SubKey Pointers	4 bytes each

		// We have a minimum record length of MIN_RECORD_LENGTH bytes to support the free chain
		if (len < MIN_RECORD_LENGTH) len = MIN_RECORD_LENGTH;

		return len;
		}





/**
*** Loads the config structures from the given file
**/
bool
SWBinaryConfigFile::readFromFile(const SWString &filename)
		{
		_name = filename;
		if (_name == _T("")) return false;

		SWPagedFile	pf(filename, true, _pagesize);

		return readFromFile(pf);
		}



/**
*** Loads the config structures from the given SWPagedFile object
**/
bool
SWBinaryConfigFile::readFromFile(SWPagedFile &pf)
		{
		clearKeysAndValues();
		m_loaded = false;
		_newWritePos = BCF_DATA_RECORD_POS;

		// Read in magic number
		sw_int32_t	magic;
		
		pf.read(0, magic);

		if (magic != BCF_MAGIC_NUMBER)
			{
			return false;
			}

		m_loading = true;

		// Read in root key
		pf.read(BCF_ROOT_RECORD_POS, _rootKeyPos);
		_newWritePos = readKeyFromFile(pf, _rootKeyPos, NULL);
#ifdef DEBUG_SWBinaryConfigFile
		log.debug(_T("SWBinaryConfigFile::readFromFile - new write position (before free space)= %d"), _newWritePos);
#endif

#ifdef DEBUG_SWBinaryConfigFile
		log.debug(_T("SWBinaryConfigFile::readFromFile - new write position (after free space)= %d"), _newWritePos);
#endif

		m_loading = false;
		m_loaded = true;

		return true;
		}


int
SWBinaryConfigFile::readKeyFromFile(SWPagedFile &pf, int pos, SWConfigKey *parent)
		{
		BCFInputStream	is;
		SWConfigKey		*key;
		char			buf[4];
		int			datalen, maxpos=0, p;

		// Read the record size from the file
		pf.read(pos, buf, 4);
		is.setBuffer(buf, 4);
		is.read(datalen);
		datalen -= 4;
		maxpos = pos + datalen + 4;

#ifdef DEBUG_SWBinaryConfigFile
		log.debug(_T("SWBinaryConfigFile::readKeyFromFile - pos=%d  reclen=%d"), pos, datalen+4);
#endif

		if (datalen > 0)
			{
			char		*data = new char[datalen];
			SWString	name;
			sw_int16_t	count;
			sw_int32_t	recpos;

			pf.read(pos+4, data, datalen);

			// Set the input stream to point to the data just read
			is.setBuffer(data, datalen);

			// Set the key name
			name = is.readPaddedString(2);
			if (parent == NULL) 
				{
				this->setName(name);
				key = this;
				_rootKeyPos = pos;
				_rootKeyLen = datalen+4;
				}
			else 
				{
				key = parent->findOrCreateKey(name);

				// Record the file info as we know that this will be
				// a SWBinaryConfigKey record
				dynamic_cast<SWBinaryConfigKey *>(key)->setFileRecordInfo(pos, datalen+4);
				}

			// Read in the value count and the values
			is.read(count);
			while (count-- > 0)
				{
				is.read(recpos);
				p = readValueFromFile(pf, recpos, key);
				maxpos = sw_math_max(maxpos, p);
				}

			// Read in the key count and the values
			is.read(count);
			while (count-- > 0)
				{
				is.read(recpos);
				p = readKeyFromFile(pf, recpos, key);
				maxpos = sw_math_max(maxpos, p);
				}

			delete [] data;
			}
		else if (parent != NULL)
			{
			throw SW_EXCEPTION_TEXT(SWConfigFileException, "BinaryConfigFile::readKeyFromFile - Corrupted file.");
			}

		return maxpos;
		}


/**
*** Writes the data structure out to a file
**/
bool
SWBinaryConfigFile::writeToFile(const SWString &filename)
		{
		SWString	bakfile;

		if (filename.isEmpty()) return false;

		SWPagedFile	pf(filename, false, _pagesize);

		// Write magic number to file (0 = end of list)
		pf.write(BCF_MAGIC_RECORD_POS, BCF_MAGIC_NUMBER);

		// Write free space pointer to file (0 = end of list)
		// Since this is a new file there will be no free space
		// so the values are always zero.
		pf.write(BCF_HEAD_RECORD_POS, 0);
		pf.write(BCF_TAIL_RECORD_POS, 0);

		// Set the new write pos before we start
		_newWritePos = BCF_DATA_RECORD_POS;
		pf.write(BCF_ROOT_RECORD_POS, writeKeyToFile(pf, this));

		return true;
		}


/**
*** Writes a key, and in non-update mode all it's subkeys and values to the output stream
***
*** @param pf		The paged file to update
*** @param key		The actual key
***
*** @return		The record position
**/
int
SWBinaryConfigFile::writeKeyToFile(SWPagedFile &pf, SWConfigKey *key)
		{
		SWConfigValue	*cv;
		SWConfigKey		*ck;

		// First write out all values (updates the file position info)
		for (int i=0;i<key->valueCount();i++)
			{
			cv = key->getValue(i);
			writeValueToFile(pf, cv);
			}

		// Second write out all subkeys (updates the file position info)
		for (int i=0;i<key->keyCount();i++)
			{
			ck = key->getKey(i);
			writeKeyToFile(pf, ck);
			}

		// Now write out the data for the key record itself
		BCFOutputStream	os;
		SWString		keypath;
		int				pos;

		// Calculate the record length
		int		rlen=calculateRecordLength(key);

		// Now write the data
		os.write(rlen);			// Record length	(4 bytes)

		// Write the name padded to 2 byte boundary
		os.writePaddedString(key->name(), 2);

		// Write out the values
		os.write((sw_int16_t)key->valueCount());	// Value count		(2 bytes)
		for (int i=0;i<key->valueCount();i++)
			{
			cv = key->getValue(i);
			pos = ((SWBinaryConfigValue *)cv)->getFileRecordPosition();
			os.write(pos);
			}

		// Write out the sub-keys
		os.write((sw_int16_t)key->keyCount());		// Key count		(2 bytes)
		for (int i=0;i<key->keyCount();i++)
			{
			ck = key->getKey(i);
			pos = ((SWBinaryConfigKey *)ck)->getFileRecordPosition();
			os.write(pos);
			}

		// Finally write the memory to paged file
		int		writepos=_newWritePos;
		
#ifdef DEBUG_SWBinaryConfigFile
		log.debug("SWBinaryConfigFile::writeKeyToFile(%s) - pos=%d  reclen=%d", key->name().c_str(), writepos, rlen);
#endif

		// Updated the record position if appropriate
		if (dynamic_cast<SWBinaryConfigKey *>(key) != NULL)
			{
			dynamic_cast<SWBinaryConfigKey *>(key)->setFileRecordInfo(writepos, rlen);
			}

		pf.write(writepos, os.data(), rlen);
		_newWritePos += rlen;

		return writepos;
		}




/**
*** Writes a single value to the output stream
***
*** @param pf		The paged file to update
*** @param value	The actual value
***
*** @return		The record position
**/
int
SWBinaryConfigFile::writeValueToFile(SWPagedFile &pf, SWConfigValue *value)
		{
		BCFOutputStream	os;

		// Calculate the record length
		int		rlen = calculateRecordLength(value);
		
		// Now write the data
		os.write(rlen);			// Record length	(4 bytes)

		// Write the name padded to 2 byte boundary
		os.writePaddedString(value->name(), 2);

		// Write the data
		SWValue	v=*value;

		os.write(v);

		// Finally write the memory to paged file
		int		writepos=_newWritePos;
		
#ifdef DEBUG_SWBinaryConfigFile
		log.debug("SWBinaryConfigFile::writeValueToFile - pos=%d  len=%d  reclen=%d", writepos, len, rlen);
#endif

		dynamic_cast<SWBinaryConfigValue *>(value)->setFileRecordInfo(writepos, rlen);
		pf.write(writepos, os.data(), rlen);
		_newWritePos += rlen;

		return writepos;
		}

/**
*** Initialise this field from the given memory input stream
***
*** @return	The length of the data.
**/
int
SWBinaryConfigFile::readValueFromFile(SWPagedFile &pf, int pos, SWConfigKey *key)
		{
		BCFInputStream	is;
		char			buf[4];
		int			datalen;

		// Read the record size from the file
		pf.read(pos, buf, 4);
		is.setBuffer(buf, 4);
		is.read(datalen);
		datalen -= 4;

#ifdef DEBUG_SWBinaryConfigFile
		log.debug("SWBinaryConfigFile::readValueFromFile - pos=%d  reclen=%d", pos, datalen+4);
#endif

		if (datalen > 0)
			{
			char		*data = new char[datalen];
			SWString		name;
			SWConfigValue	*value;

			pf.read(pos+4, data, datalen);

			// Set the input stream to point to the data just read
			is.setBuffer(data, datalen);

			// Get the value name
			name = is.readPaddedString(2);
			value = key->findOrCreateValue(name);

			// Record the file info
			dynamic_cast<SWBinaryConfigValue *>(value)->setFileRecordInfo(pos, datalen+4);

			SWValue	v;

			v.readFromStream(is);
			*(SWValue *)value = v;

			delete [] data;
			}

		return pos+datalen+4;
		}


/**
*** This method checks to see if the given sub-key already exists.
*** If it does it returns a pointer to it, if not the key is created
*** and returned.
***
*** @brief	Find or create a sub-key of the given name
**/
SWConfigKey *
SWBinaryConfigFile::findOrCreateKey(const SWString &name)
		{
		SWGuard		guard(this);
		SWConfigKey	*key = findKey(name);

		if (key == NULL)
			{
			key = new SWBinaryConfigKey(this, name, getIgnoreCase());
			_keyVec.add(key);
			}

		return key;
		}



/**
*** This method checks to see if the given value already exists.
*** If it does it returns a pointer to it, if not the value is created
*** and returned.
***
*** @brief	Find or create a value of the given name
**/
SWConfigValue *
SWBinaryConfigFile::findOrCreateValue(const SWString &name)
		{
		SWGuard		guard(this);
		SWConfigValue	*value = findValue(name);

		if (value == NULL)
			{
			value = new SWBinaryConfigValue(this, name);
			_valueVec.add(value);
			}

		return value;
		}


// Set the changed flag.
// We must also set the changed flag of our owner
void
SWBinaryConfigFile::setChangedFlag(ChangeEventCode code, SWObject *obj)
        {
        if (!m_loading)
            {
            _changed = true;
#ifdef NEVER
            if (!m_readOnlyFlag) 
				{
				switch (code)
					{
					case ValueCreated:
#ifdef DEBUG_SWBinaryConfigFile
					log.debug("ValueCreated (%s)", ((SWConfigValue *)obj)->name().c_str());
#endif
						{
						SWBinaryConfigValue	*vp = dynamic_cast<SWBinaryConfigValue *>(obj);
						int			newlen=calculateRecordLength(vp), recpos, reclen;

#ifdef DEBUG_SWBinaryConfigFile
						log.debug("Disk record at %d length %d (new length=%d)", vp->getFileRecordPosition(), vp->getFileRecordLength(), newlen);
#endif

						findFreeSpace(_pagedFile, newlen, recpos, reclen);
						vp->setFileRecordInfo(recpos, reclen);

						writeValueToFile(_pagedFile, vp->getFileRecordPosition(), vp->getFileRecordLength(), vp);

						// Inform the owning record to update itself on disk
						SWBinaryConfigKey	*owner;
						
						owner = dynamic_cast<SWBinaryConfigKey *>(vp->m_pOwner);
						if (owner != NULL) owner->setChangedFlag(KeyUpdated, owner);
						}
					break;

					case ValueDeleted:
#ifdef DEBUG_SWBinaryConfigFile
						log.debug("ValueDeleted (%s)", ((SWConfigValue *)obj)->name().c_str());
#endif
						{
						SWBinaryConfigValue	*vp = dynamic_cast<SWBinaryConfigValue *>(obj);

#ifdef DEBUG_SWBinaryConfigFile
						log.debug("Disk record at position %d length %d (new length=%d)", vp->getFileRecordPosition(), vp->getFileRecordLength(), calculateRecordLength(vp));
#endif

						// Add the space freed by the record to the free space chain and
						// clear the changed flag so we don't do a flush.
						addFreeSpace(_pagedFile, vp->getFileRecordPosition(), vp->getFileRecordLength());

						// Inform the owning record to update itself on disk
						SWBinaryConfigKey	*owner = (SWBinaryConfigKey *)(vp->m_pOwner);
						if (owner != NULL) owner->setChangedFlag(KeyUpdated, owner);
						}
					break;

					case ValueUpdated:
#ifdef DEBUG_SWBinaryConfigFile
					log.debug("ValueUpdated (%s)", ((SWConfigValue *)obj)->name().c_str());
#endif
						{
						SWBinaryConfigValue	*vp = dynamic_cast<SWBinaryConfigValue *>(obj);
						int			newlen = calculateRecordLength(vp);
						bool		changedPos = false;

#ifdef DEBUG_SWBinaryConfigFile
						log.debug("Disk record at position %d length %d (new length=%d)", vp->getFileRecordPosition(), vp->getFileRecordLength(), newlen);
#endif

						if (newlen > vp->getFileRecordLength())
						{
						// Find space to write the data
						int	recpos, reclen;

						findFreeSpace(_pagedFile, newlen, recpos, reclen);
						if (recpos != vp->getFileRecordPosition()) addFreeSpace(_pagedFile, vp->getFileRecordPosition(), vp->getFileRecordLength());
						vp->setFileRecordInfo(recpos, reclen);
						changedPos = true;
						}

						writeValueToFile(_pagedFile, vp->getFileRecordPosition(), vp->getFileRecordLength(), vp);

						if (changedPos)
							{
							// Inform the owning record to update itself on disk
							SWBinaryConfigKey	*owner = (SWBinaryConfigKey *)(vp->m_pOwner);
							if (owner != NULL) owner->setChangedFlag(KeyUpdated, owner);
							}
						}
					break;

					case KeyCreated:
#ifdef DEBUG_SWBinaryConfigFile
					log.debug("KeyCreated (%s)", ((SWConfigKey *)obj)->name().c_str());
#endif
						{
						SWBinaryConfigKey	*kp = dynamic_cast<SWBinaryConfigKey *>(obj);
						int			newlen=calculateRecordLength(kp), recpos, reclen;

#ifdef DEBUG_SWBinaryConfigFile
						log.debug("Disk record at position %d length %d (new length=%d)", kp->getFileRecordPosition(), kp->getFileRecordLength(), calculateRecordLength(kp));
#endif


						findFreeSpace(_pagedFile, newlen, recpos, reclen);
						kp->setFileRecordInfo(recpos, reclen);

						writeKeyToFile(_pagedFile, kp->getFileRecordPosition(), kp->getFileRecordLength(), kp);

						// Inform the owning record to update itself on disk
						SWBinaryConfigKey	*owner = (SWBinaryConfigKey *)(kp->_owner);
						if (owner != NULL) owner->setChangedFlag(KeyUpdated, owner);
						}
					break;

					case KeyDeleted:
#ifdef DEBUG_SWBinaryConfigFile
					log.debug("KeyDeleted (%s)", ((SWConfigKey *)obj)->name().c_str());
#endif
						{
						SWBinaryConfigKey	*kp = dynamic_cast<SWBinaryConfigKey *>(obj);

#ifdef DEBUG_SWBinaryConfigFile
						log.debug("Disk record at position %d length %d (new length=%d)", kp->getFileRecordPosition(), kp->getFileRecordLength(), calculateRecordLength(kp));
#endif

						// Add the space freed by the record to the free space chain and
						// clear the changed flag so we don't do a flush.
						addFreeSpace(_pagedFile, kp->getFileRecordPosition(), kp->getFileRecordLength());

						// Inform the owning record to update itself on disk
						SWBinaryConfigKey	*owner = (SWBinaryConfigKey *)(kp->_owner);
						if (owner != NULL) owner->setChangedFlag(KeyUpdated, owner);
						}
					break;

					case KeyUpdated:
#ifdef DEBUG_SWBinaryConfigFile
					log.debug("KeyUpdated (%s)", ((SWConfigKey *)obj)->name().c_str());
#endif
					if (obj == this)
						{
						int		newlen = calculateRecordLength(this);

#ifdef DEBUG_SWBinaryConfigFile
						log.debug("ROOT KEY: Disk record at position %d length %d (new length=%d)", _rootKeyPos, _rootKeyLen, newlen);
#endif


						if (newlen > _rootKeyLen)
							{
							// Find space to write the data
							int	recpos, reclen;

							findFreeSpace(_pagedFile, newlen, recpos, reclen);
							if (recpos != _rootKeyPos) addFreeSpace(_pagedFile, _rootKeyPos, _rootKeyLen);
							_rootKeyPos = recpos;
							_rootKeyLen = reclen;
							_pagedFile.write(BCF_ROOT_RECORD_POS, _rootKeyPos);
							}

						writeKeyToFile(_pagedFile, _rootKeyPos, _rootKeyLen, this, true);
						}
					else
						{
						SWBinaryConfigKey	*kp = dynamic_cast<SWBinaryConfigKey *>(obj);
						int			newlen = calculateRecordLength(kp);
						bool			changedPos = false;

#ifdef DEBUG_SWBinaryConfigFile
						log.debug("Disk record at position %d length %d (new length=%d)", kp->getFileRecordPosition(), kp->getFileRecordLength(), newlen);
#endif


						if (newlen > kp->getFileRecordLength())
							{
							// Find space to write the data
							int	recpos, reclen;

							findFreeSpace(_pagedFile, newlen, recpos, reclen);
							if (recpos != kp->getFileRecordPosition()) addFreeSpace(_pagedFile, kp->getFileRecordPosition(), kp->getFileRecordLength());
							kp->setFileRecordInfo(recpos, reclen);
							changedPos = true;
							}

						writeKeyToFile(_pagedFile, kp->getFileRecordPosition(), kp->getFileRecordLength(), kp, true);

						if (changedPos)
							{
							// Inform the owning record to update itself on disk
							SWBinaryConfigKey	*owner = (SWBinaryConfigKey *)(kp->_owner);
							if (owner != NULL) owner->setChangedFlag(KeyUpdated, owner);
							}
						}
					break;
					}
				}
#endif
            }
        }


/**
*** Find free space to write in either from the free space chain
*** or from the next write position
**/
void
SWBinaryConfigFile::findFreeSpace(SWPagedFile &pf, int wanted, int &position, int &size)
		{
#ifdef DEBUG_SWBinaryConfigFile
		log.debug("SWBinaryConfigFile::findFreeSpace - looking for %d bytes of space", wanted);
		log.debug("- allocating %d bytes of space from end of file (%d)", wanted, _newWritePos);
#endif
		position = _newWritePos;
		size = wanted;
		_newWritePos += size;
		}



void
SWBinaryConfigFile::flush()
		{
		if (name().isEmpty() || isReadOnly() || !changed()) return;

		writeToFile(name());
		clearChangedFlag();
		}
