/**
*** @file		SWResourceModule.h
*** @brief		An object to represent a single resource module (e.g. a DLL)
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWResourceModule class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWResourceModule_h__
#define __SWResourceModule_h__

#include <swift/SWObjectArray.h>
#include <swift/SWString.h>
#include <swift/SWResource.h>
#include <swift/SWLoadableModule.h>




/**
*** @brief	An object to represent a single resource module (e.g. a DLL)
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWResourceModule : public SWObject
		{
public:
		/// An enumeration of the possible resource module types
		enum ResourceModuleType
			{
			None,			///< No resource type - used when no resource module is loaded.
			LoadableModule,	///< A loadable module loaded via the SWLoadableModule class
			Windows,		///< A windows resource module (typically a DLL or EXE)
			WindowsHandle	///< A special resource module type constructed directly from a windows handle
			};

public:
		/**
		*** @brief	Default constructor with optional auto-load of resource file.
		***
		*** This method constructs the object and if a filename is specified calls
		*** the load method with the specified filename.
		***
		*** @param[in]	filename	The filename to pass to the load method
		*** @param[in]	type		The type of file being loaded.
		***
		*** @see load
		**/
		SWResourceModule(const SWString &filename="", SWLoadableModule::LibType type=SWLoadableModule::SharedLibrary);

		/// Virtual destructor.
		virtual ~SWResourceModule();

#if defined(SW_PLATFORM_WINDOWS)
		/**
		*** @brief Special constructor for windows only to load from an already loaded module handle
		***
		*** This special constructor is intended to support windows resource modules and is
		*** used when the resource module has already been loaded into the operating environment.
		***
		*** @param[in]	hInst	The instance handle of the resource module.
		**/
		SWResourceModule(HINSTANCE hInst);
#endif

		/**
		*** @brief	Load the specified resource module
		***
		*** This method attempts to load the specified filename as a resource module.
		*** The type parameter specifies the type of resource module being loaded
		*** and is used to control how resources are accessed from the loaded resource
		*** (if successfully loaded).
		***
		*** @param[in]	filename	The filename to pass to the load method
		*** @param[in]	type		The type of file being loaded.
		***
		*** @return		The status code SW_STATUS_SUCCESS if successful, or an error code otherwise.
		**/
		sw_status_t			load(const SWString &filename, SWLoadableModule::LibType type=SWLoadableModule::SharedLibrary);

		/**
		*** @brief	Unload any previously loaded resources.
		***
		*** This method unload the resource module, freeing any loaded resources from memory
		*** and any handles used to load the resource module.
		***
		*** @return		The status code SW_STATUS_SUCCESS if successful, or an error code otherwise.
		**/
		sw_status_t			unload();

		/// Returns the load status of this resource module.
		bool				isLoaded() const			{ return (m_hModule != 0); }

		/// Returns the filename used to load the module if loaded or blank otherwise.
		const SWString &	filename() const			{ return m_filename; }

		/// Load a string matching the specified id and type from this module.
		bool				loadString(sw_resourceid_t id, SWString &str, SWResource::ResourceType rtype=SWResource::String);

		/// Load a string matching the specified id and type from this module.
		bool				loadString(sw_resourceid_t id, SWString &str, SWResource::ResourceType rtype, sw_uint32_t languageid);

private: // members
		SWString				m_filename;		///< The filename of the loaded resource
		sw_handle_t				m_hModule;		///< A handle to the resource module
		ResourceModuleType		m_type;			///< The type of the loaded resource
		};




#endif
