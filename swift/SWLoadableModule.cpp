/**
*** @file		SWLoadableModule.cpp
*** @brief		A class for handling shared libraries
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWLoadableModule class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




// Needed for precompiled header support
#include "stdafx.h"

#include <swift/SWLoadableModule.h>
#include <swift/SWFilename.h>

#if defined(SW_USE_UNIX_DLLS)
	#include <dlfcn.h>
	#if !defined(SW_PLATFORM_AIX) && !defined(SW_PLATFORM_MACH)
		#include <link.h>
	#endif
#endif


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif



/*
** Default constructor
*/
SWLoadableModule::SWLoadableModule(const SWString &name) :
	m_hModule(0),
	m_mode(Normal)
		{
		if (!name.isEmpty())
			{
			load(name, Normal);
			}
		}


/*
** Default constructor
*/
SWLoadableModule::SWLoadableModule(const SWString &name, LibType libtype, OpenMode mode) :
	m_hModule(0),
	m_mode(Normal)
		{
		if (!name.isEmpty())
			{
			open(name, libtype, mode);
			}
		}


/*
** Copy constructor
*/
SWLoadableModule::SWLoadableModule(const SWLoadableModule &other) :
	SWObject()
		{
		*this = other;
		}


/*
** Destructor
*/
SWLoadableModule::~SWLoadableModule()
		{
		close();
		}


/*
** Assignment operator
*/
SWLoadableModule &
SWLoadableModule::operator=(const SWLoadableModule &other)
		{
		close();
		load(other.m_name, other.m_mode);

		return *this;
		}


// Open the specified module
sw_status_t
SWLoadableModule::load(const SWString &name, OpenMode mode)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		// Call close just in case
		close();

		if (!name.isEmpty())
			{
#if defined(SW_USE_WINDOWS_DLLS)
			m_hModule = LoadLibraryEx(name, NULL, 0);
			if (m_hModule == NULL)
				{
				res = GetLastError();
				}
#elif defined(SW_USE_UNIX_DLLS)
			int		openmode=RTLD_LAZY;

			if (mode == ResolveAll)
				{
				openmode = RTLD_NOW;
				}

			m_hModule = dlopen(name, openmode);
			if (m_hModule == NULL)
				{
				res = errno;
				if (res == 0) res = SW_STATUS_NOT_FOUND;
				}
#else
			res = SW_STATUS_UNSUPPORTED_OPERATION;
#endif

			if (res == SW_STATUS_SUCCESS)
				{
				m_name = name;
				m_mode = mode;
				}
			}
		else res = SW_STATUS_INVALID_PARAMETER;

		return res;
		}


// Open the specified module
sw_status_t
SWLoadableModule::open(const SWString &name, LibType libtype, OpenMode mode)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		// Call close just in case
		close();

		// First of all try the plain name
		res = load(name, mode);
		if (res != SW_STATUS_SUCCESS)
			{
			SWString	tmpname, dirname, basename;
			
			dirname = SWFilename::dirname(name);
			basename = SWFilename::basename(name);

			switch (libtype)
				{
				case DLL:
#if defined(SW_PLATFORM_WINDOWS)
					tmpname = basename;
					tmpname += ".dll";
#else
					tmpname = "lib";
					tmpname += basename;
					tmpname += ".so";
#endif
					res = load(tmpname, mode);
					if (res != SW_STATUS_SUCCESS) res = load(SWFilename::concatPaths(dirname, tmpname), mode);
					break;

				case Program:
#if defined(SW_PLATFORM_WINDOWS)
					tmpname = basename;
					tmpname += ".exe";
					res = load(SWFilename::concatPaths(dirname, tmpname), mode);
#endif
					break;

				default:
					res = SW_STATUS_INVALID_PARAMETER;
					break;
				}
			}

		return res;
		}


// Return a pointer to the specified symbol in the library.
/// Returns NULL if the symbol cannot be found.
void *	
SWLoadableModule::symbol(const SWString &name, bool allowNameMods)
		{
		void	*p=NULL;

		if (m_hModule != NULL)
			{
#if defined(SW_USE_WINDOWS_DLLS)
			p = GetProcAddress((HMODULE)m_hModule, name);
#elif defined(SW_USE_UNIX_DLLS)
			p = dlsym(m_hModule, name);
#endif
			if (p == NULL && allowNameMods)
				{
				SWString	tmp("_");

				tmp += name;

#if defined(SW_USE_WINDOWS_DLLS)
				p = GetProcAddress((HMODULE)m_hModule, tmp);
#elif defined(SW_USE_UNIX_DLLS)
				p = dlsym(m_hModule, tmp);
#endif
				}
			}

		return p;
		}


// Close the module
sw_status_t
SWLoadableModule::close()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (isOpen())
			{
#if defined(SW_USE_WINDOWS_DLLS)
			if (FreeLibrary((HMODULE)m_hModule))
#elif defined(SW_USE_UNIX_DLLS)
			if (dlclose(m_hModule) == 0)
#endif
				{
				m_name.empty();
				m_mode = Normal;
				}
			else
				{
#if defined(SW_PLATFORM_WINDOWS)
				res = GetLastError();
#else
				res = errno;
#endif
				}
			}

		return res;
		}


bool
SWLoadableModule::isOpen() const
		{
		return (!m_name.isEmpty());
		}


bool
SWLoadableModule::find(const SWString &name, SWString &path)
		{
		SW_UNREFERENCED_PARAMETER(name);
		SW_UNREFERENCED_PARAMETER(path);

		/*
		SWStringArray	search, prefix, suffix;
		SWString		dirname, base, ext, tmp;
		bool			found=false, search=false;

		SWFilename::split(name, dir, base, ext);
		search.add(dir);
		if (dir == ".")
			{
			if (name.first('/') < 0 || name.first('\\') < 0)
				{
				// name is of the form x (i.e. no path component)
				char	*ep;

				ep = getenv("LD_LIBRARY_PATH");
				if (ep != NULL)
					{
					SWTokenizer	tok(ep, ":;");

					while (tok.hasMoreTokens()) search.add(tok.nextToken());
					}

				ep = getenv("PATH");
				if (ep != NULL)
					{
					SWTokenizer	tok(ep, ":;");

					while (tok.hasMoreTokens()) search.add(tok.nextToken());
					}
				}
			}

		// Set up standard prefixes
		prefix.add("");
#if !defined(SW_PLATFORM_WINDOWS)
		prefix.add("lib");
#endif

		// Set up standard suffixes
		prefix.add("");
#if defined(SW_PLATFORM_WINDOWS)
		prefix.add(".dll");
#else
		prefix.add(".so");
#endif

		// Now try each directory to look for the file.
		for (i=0;i<search.size();i++)
			{
			tmp = SWFilename::concatPaths(search[i], base);
			if (access(tmp, 

			if (ext.isEmpty())
				{
				}
			}
		*/

		return false;
		}



