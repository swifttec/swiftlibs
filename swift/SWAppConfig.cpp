/**
*** @file		SWAppConfig.cpp
*** @brief		Generic config file implementation
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWAppConfig class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"

#include <swift/SWAppConfig.h>
#include <swift/SWGuard.h>
#include <swift/SWTokenizer.h>
#include <swift/SWLocale.h>
#include <swift/SWFilename.h>
#include <swift/SWFileInfo.h>
#include <swift/SWFolder.h>
#include <swift/sw_misc.h>
#include <swift/SWConfigValue.h>
#include <swift/sw_file.h>
#include <swift/sw_registry.h>
#include <swift/sw_crypt.h>
#include <swift/sw_main.h>
#include <swift/SWTempFile.h>
#include <swift/SWTextFile.h>
#include <xplatform/sw_process.h>
#include <config/config.h>

#if defined(SW_PLATFORM_WINDOWS)
	#include <ShlObj.h>
#endif

SWAppConfig::SWAppConfig()
		{
		initDefaults(SW_CONFIG_DEFAULT_COMPANY, "", true);
		}


SWAppConfig::SWAppConfig(const SWString &company, const SWString &product, bool useCompanyForDataDir)
		{
		initDefaults(company, product, useCompanyForDataDir);
		}



#if defined(SW_PLATFORM_WINDOWS)
static SWString
getWindowsFolderPath(int folderid)
		{
		TCHAR	path[MAX_PATH];
			
		SHGetFolderPath(NULL, folderid, NULL, SHGFP_TYPE_CURRENT, path);

		return SWString(path);
		}
#endif



void
SWAppConfig::initDefaults(const SWString &company, const SWString &product, bool useCompanyForDataDir)
		{
		set("PRODUCT_COMPANY", company);
		set("PRODUCT_NAME", product);
		m_initstatus = 0;
		m_useCompanyForDataDir = useCompanyForDataDir;

		SWTextFile	tf;
		SWString	sysConfFilename;

#if defined(SW_PLATFORM_WINDOWS)
		sysConfFilename = SWFilename::concatPaths(getWindowsFolderPath(CSIDL_COMMON_APPDATA), company + "/" + company + ".conf");
#else
		sysConfFilename = "/etc/"+company+".conf";
#endif

		if (tf.open(sysConfFilename, TF_MODE_READ) == SW_STATUS_SUCCESS)
			{
			SWString	line;

			while (tf.readLine(line) == SW_STATUS_SUCCESS)
				{
				line.strip(" \t", SWString::both);
				if (line.startsWith("#")) continue;

				int		p=line.first('=');

				if (p > 0)
					{
					set(line.left(p), line.mid(p+1));
					}
				}

			tf.close();
			}

		SWString	defaultGlobalDataDir=getString("GlobalDataDir");

		if (defaultGlobalDataDir.isEmpty())
			{
#if defined(SW_PLATFORM_WINDOWS)
			defaultGlobalDataDir = getWindowsFolderPath(CSIDL_COMMON_APPDATA);
#else
			defaultGlobalDataDir = "/var";
#endif
			}

		SWString		path=defaultGlobalDataDir;

		if (m_useCompanyForDataDir)
			{
			setDataDir(SWFilename::concatPaths(path, SWFilename::concatPaths(company, product)), false);
			}
		else
			{
			setDataDir(SWFilename::concatPaths(path, product), false);
			}

#if defined(SW_PLATFORM_WINDOWS)
		setFolder(UserDir, getWindowsFolderPath(CSIDL_PROFILE));

		path = getWindowsFolderPath(CSIDL_LOCAL_APPDATA);

		if (useCompanyForDataDir)
			{
			setFolder(UserDataDir, SWFilename::concatPaths(path, SWFilename::concatPaths(company, product)), false);
			}
		else
			{
			setFolder(UserDataDir, SWFilename::concatPaths(path, product), false);
			}
#else
		SWString	s;
		
		sw_misc_getEnvironmentVariable("HOME", s);
		setFolder(UserDir, s, false);

		if (useCompanyForDataDir)
			{
			setFolder(UserDataDir, SWFilename::concatPaths(getUserDir(".appdata"), SWFilename::concatPaths(company, product)), false);
			}
		else
			{
			setFolder(UserDataDir, SWFilename::concatPaths(getUserDir(".appdata"), product), false);
			}
#endif

		addResourceFolder("resources");
		addResourceFolder("../resources");

#if !defined(SW_PLATFORM_WINDOWS)
		addResourceFolder("Resources");
		addResourceFolder("../Resources");
#endif
		}



SWAppConfig::~SWAppConfig()
		{
		}


void
SWAppConfig::checkConfigInitialised() const
		{
		if (m_initstatus == 0)
			{
#if defined(SW_PLATFORM_WINDOWS)
			TRACE("SWAppConfig NOT initialised - throwing exception");
			if (!sw_process_isConsoleApp())
				{
				MessageBox(NULL, _T("SWAppConfig NOT initialised - throwing exception"), _T("Software Exception!!!"), MB_OK);
				}
			else
#endif
				{
				fprintf(stderr, "SWAppConfig NOT initialised - throwing exception\n");
				fflush(stdout);
				}

			throw SW_EXCEPTION_TEXT(SWException, "SWAppConfig NOT initialised");
			}
		}


#if defined(SW_PLATFORM_WINDOWS)
void
SWAppConfig::init(const SWString &cmdline)
		{
		SWString		prog;
		SWStringArray	args;
		TCHAR			path[1024];

		GetModuleFileName(NULL, path, sizeof(path));
		prog = path;

		sw_main_parseCommandLine(cmdline, prog, args, SW_MAIN_EXPAND_WILDCARDS|SW_MAIN_WINDOWS_CMDLINE);
		
		return init(prog, args);
		}
#endif


void
SWAppConfig::init(int argc, char **argv)
		{
		SWString		prog;
		SWStringArray	args;


		if (argc > 0) prog = argv[0];

		for (int i=1;i<argc;i++)
			{
			args.add(argv[i]);
			}

		init(prog, args);
		}


void
SWAppConfig::init(const SWString &prog, const SWStringArray &args)
		{
		m_initstatus = 2;

		// First of save the command line
		setCommandLine(prog, args);

		// Call onLoadConfig to allow dervied classes to load config info before continuing processing
		onBeforeInit();

		// Set the registry key
		m_regKey = "Software/";
		m_regKey += getString("PRODUCT_COMPANY");
		m_regKey += "/";

		if (!getString("PRODUCT_LPR").isEmpty()) m_regKey += getString("PRODUCT_LPR");
		else m_regKey += getString("PRODUCT_NAME");

		
		// Set up the various useful values
		SWString	s, t, sep, defaultValue, dateFormat, timeFormat;

		// Date Format String
		SWLocale	locale=SWLocale::getUserLocale();

		defaultValue = locale.dateFormat();
		set("FORMAT_DATE", dateFormat = getRegistryString("", "dateformat", defaultValue));


		// Time Format String
		defaultValue = locale.timeFormat();
		set("FORMAT_TIME", timeFormat = getRegistryString("", "timeformat", defaultValue));

		defaultValue = dateFormat;
		defaultValue += " ";
		defaultValue += timeFormat;
		set("FORMAT_TIMESTAMP", getRegistryString("", "timestampformat", defaultValue));


		// Override the install dir (if stored in a registry value)
		s = getRegistryString("", "installdir", "", SW_REGISTRY_LOCAL_MACHINE);
		if (!s.isEmpty())
			{
			setFolder(InstallDir, s, false, false);
			}
		
		// Just in case 
		if (getInstallDir().isEmpty())
			{
			setFolder(InstallDir, SWFolder::getCurrentDirectory(), false, false);
			}

		// The user's home dir
		sw_misc_getEnvironmentVariable("HOME", s);
		if (s.isEmpty()) sw_misc_getEnvironmentVariable("USERPROFILE", s);
		if (s.isEmpty())
			{
			sw_misc_getEnvironmentVariable("HOMEDRIVE", s);
			sw_misc_getEnvironmentVariable("HOMEPATH", t);
			s += t;
			}

		setFolder(UserDir, s);

		// Global Data Root
		SWString	path;

#if defined(SW_PLATFORM_WINDOWS)
		path = getWindowsFolderPath(CSIDL_COMMON_APPDATA);
#else
		path = "/var";
#endif

		setFolder(SystemGlobalDataDir, path, true, true);

#if defined(SW_PLATFORM_WINDOWS)
		path = getWindowsFolderPath(CSIDL_LOCAL_APPDATA);
#else
		path = getFolder(UserDir, ".appdata");
#endif

		setFolder(SystemUserDataDir, path, true, true);

		// Global Data Dir
		SWString	defaultGlobalDataDir=getString("GlobalDataDir");

		if (defaultGlobalDataDir.isEmpty())
			{
#if defined(SW_PLATFORM_WINDOWS)
			defaultGlobalDataDir = getWindowsFolderPath(CSIDL_COMMON_APPDATA);
#else
			defaultGlobalDataDir = "/var";
#endif
			}


		s = getRegistryString("", "globaldatadir", "", SW_REGISTRY_LOCAL_MACHINE);
		if (s.isEmpty()) s = getRegistryString("", "datadir", "", SW_REGISTRY_LOCAL_MACHINE);
		if (s.isEmpty())
			{
			path = defaultGlobalDataDir;

			if (m_useCompanyForDataDir)
				{
				s = SWFilename::concatPaths(path, SWFilename::concatPaths(getCompanyName(), getProductName()));
				}
			else
				{
				s = SWFilename::concatPaths(path, getProductName());
				}
			}
		
		setFolder(GlobalDataDir, s, true, true, true);

		// Local Data Dir
		s = getRegistryString("", "localdatadir", "", SW_REGISTRY_CURRENT_USER);
		if (s.isEmpty())
			{
#if defined(SW_PLATFORM_WINDOWS)
			path = getWindowsFolderPath(CSIDL_LOCAL_APPDATA);
#else
			SWString	path=getFolder(UserDir, ".appdata");
#endif

			if (m_useCompanyForDataDir)
				{
				s = SWFilename::concatPaths(path, SWFilename::concatPaths(getCompanyName(), getProductName()));
				}
			else
				{
				s = SWFilename::concatPaths(path, getProductName());
				}
			}

		setFolder(LocalDataDir, s, true, true, true);

		// User docs dir (My Documents)
			{
#if defined(SW_PLATFORM_WINDOWS)
			path = getWindowsFolderPath(CSIDL_MYDOCUMENTS);
			s = path;
#else
			s = getFolder(UserDir, "Documents");
#endif
			}

		setFolder(UserDocsDir, s, true, true, true);

		// User Resource Dir
		s = getRegistryString("", "userresourcedir", "");
		if (!SWFileInfo::isFolder(s))
			{
			s = SWFilename::concatPaths(getFolder(UserDir, "Resources"), getProductName());
			}

		setFolder(UserResourcesDir, s, true, true, true);
		
		// Temp Dir
		s = getRegistryString("", "tempdir", SWTempFile::getTempDirectory(), SW_REGISTRY_LOCAL_MACHINE);
		if (s.isEmpty())
			{
			s = SWFilename::concatPaths(getDataDir(), "temp");
			}

		setFolder(TempDir, s, true, true, true);


		// Log Dir
		setFolder(LogDir, SWFilename::concatPaths(getFolder(GlobalDataDir), "logs"), true, true, true);
		setFolder(ProgramResourcesDir, SWFilename::concatPaths(getFolder(InstallDir), "resources"), true, true, true);

		// Currency
		defaultValue = locale.currencySymbol();
		set("SYMBOL_CURRENCY", getRegistryString("", "currency", defaultValue));
		
		// Mark the config as loaded
		m_initstatus = 1;

		// Finally mark the config as loaded
		onAfterInit();
		}


void
SWAppConfig::getProgramArgs(SWStringArray &args)
		{
		args = m_programArgs;
		}


void
SWAppConfig::setCommandLine(const SWString &prog, const SWStringArray &args)
		{
		m_programPath = prog;
		m_programArgs = args;
		
		SWString	progdir=SWFilename::dirname(prog);

		setFolder(ProgramDir, progdir);

		if (SWFilename::basename(progdir) == "bin")
			{
			setFolder(InstallDir, SWFilename::dirname(progdir));
			}
		else
			{
			setFolder(InstallDir, progdir);
			}
		}


void
SWAppConfig::setFolder(int idx, const SWString &path, bool create, bool grantEverybodyAccessOnCreate, bool onlyIfNotAlreadySet)
		{
		// printf("(%p) %s = %s\n", this, getFolderName(idx).c_str(), path.c_str());

		SWString	res;
		
		if (idx >= 0)
			{
			if (!onlyIfNotAlreadySet || m_folderList[idx].isEmpty())
				{
				m_folderList[idx] = path;
				}

			if (create)
				{
				SWFolder::create(path, create);
				if (grantEverybodyAccessOnCreate)
					{
					sw_file_grantEverybodyAccessToFile(path);
					}
				}
			}

		if (idx == InstallDir)
			{
			// Prog Resource Dir
			SWString	progResourceDir = getRegistryString("", "resourcedir", "", SW_REGISTRY_LOCAL_MACHINE);
			SWString	progdir=getFolder(ProgramDir);

			if (!SWFileInfo::isFolder(progResourceDir) && !progdir.isEmpty())
				{
				progResourceDir = SWFilename::concatPaths(progdir, "resources");
				}

			if (!SWFileInfo::isFolder(progResourceDir))
				{
				progResourceDir = SWFilename::concatPaths(getInstallDir(), "resources");
				}

			if (!SWFileInfo::isFolder(progResourceDir))
				{
				progResourceDir = SWFilename::concatPaths(getInstallDir(), "../resources");
				}

			if (!SWFileInfo::isFolder(progResourceDir))
				{
				progResourceDir = SWFilename::concatPaths(SWFilename::dirname(progdir), "resources");
				}

			if (!SWFileInfo::isFolder(progResourceDir))
				{
				progResourceDir = SWFilename::concatPaths(SWFilename::dirname(progdir), "../resources");
				}

			if (!SWFileInfo::isFolder(progResourceDir))
				{
				progResourceDir = SWFilename::concatPaths(SWFolder::getCurrentDirectory(), "resources");
				}

			if (SWFileInfo::isFolder(progResourceDir))
				{
				progResourceDir = SWFilename::absolutePath(progResourceDir, SWFolder::getCurrentDirectory(), SWFilename::PathTypePlatform);
				setFolder(ProgramResourcesDir, progResourceDir);
				}
			}
		}


SWString
SWAppConfig::getFolderName(int idx) const
		{
		SWString	res;

		switch (idx)
			{
			case InstallDir:			res = " InstallDir";			break;
			case SystemGlobalDataDir:	res = " SystemGlobalDataDir";	break;
			case SystemUserDataDir:		res = " SystemUserDataDir";		break;
			case GlobalDataDir:			res = " GlobalDataDir";			break;
			case LocalDataDir:			res = " LocalDataDir";			break;
			case LogDir:				res = " LogDir";				break;
			case UserDir:				res = " UserDir";				break;
			case UserDataDir:			res = " UserDataDir";			break;
			case UserDocsDir:			res = " UserDocsDir";			break;
			case UserResourcesDir:		res = " UserResourcesDir";		break;
			case ProgramResourcesDir:	res = " ProgramResourcesDir";	break;
			case ProgramDir:			res = " ProgramDir";			break;
			case HelpDir:				res = " HelpDir";				break;
			case TempDir:				res = " TempDir";				break;

			default:
				res.format("Dir#%d", idx);
				break;
			}

		return res;
		}


SWString
SWAppConfig::getFolder(int idx, const SWString &subfolder) const
		{
		SWString	res;

#ifdef SW_BUILD_DEBUG
		if (idx == ProgramResourcesDir)
			{
			// When in development our resources will be local - so override in this special case
			res = "resources";
			if (!SWFileInfo::isFolder(res)) res = "../resources";
			if (SWFileInfo::isFolder(res)) return res;
			}	
#endif
		
		if (idx >= 0 && idx < (int)m_folderList.size())
			{
			res = m_folderList.get(idx);
			}
			
		if (!subfolder.isEmpty()) res = SWFilename::concatPaths(res, subfolder);

		return res;
		}


void
SWAppConfig::setDataDir(const SWString &path, bool create)
		{
		setFolder(GlobalDataDir, path, create, true);
		}


SWString
SWAppConfig::getVersionString(const SWString &prefix, const SWString &fmt) const
		{
		SWString	res, key, name;
		int			majorVersion, minorVersion, revision, build;

		key = prefix;
		key += "_MAJOR_VERSION";
		majorVersion = getInt32(key);

		key = prefix;
		key += "_MINOR_VERSION";
		minorVersion = getInt32(key);

		key = prefix;
		key += "_REVISION";
		revision = getInt32(key);

		key = prefix;
		key += "_BUILD";
		build = getInt32(key);

		key = prefix;
		key += "_NAME";
		name = getString(key);

		SWString	mstr, nstr, rstr, bstr;

		mstr.format("%u", majorVersion);
		nstr.format("%u", minorVersion);
		rstr.format("%u", revision);
		bstr.format("%u", build);

		res = fmt;
		res.replaceAll("%m", mstr.c_str());
		res.replaceAll("%n", nstr.c_str());
		res.replaceAll("%r", rstr.c_str());
		res.replaceAll("%b", bstr.c_str());
		res.replaceAll("%t", name.c_str());

		return res;
		}




void
SWAppConfig::onBeforeInit()
		{
		// By default do nothing - apps can override this
		}


void
SWAppConfig::onAfterInit()
		{
		// By default do nothing - apps can override this
		}




SWString
SWAppConfig::getRegistryEncryptedString(const SWString &section, const SWString &param, const SWString &defval, SWRegistryHive pdk) const
		{
		checkConfigInitialised();

		SWString	s;
		SWString	key(m_regKey);
		
		if (!section.isEmpty())
			{
			key += "/";
			key += section;
			}
			
		s = (const sw_tchar_t *)sw_registry_getString(pdk, key, param, defval);
		if (!s.isEmpty()) s = sw_crypt_decryptString(s);

		return s;
		}
		
		
void
SWAppConfig::setRegistryEncryptedString(const SWString &section, const SWString &param, const SWString &value, SWRegistryHive pdk)
		{
		checkConfigInitialised();

		SWString	key(m_regKey);
		
		if (!section.isEmpty())
			{
			key += "/";
			key += section;
			}
			
		sw_registry_setString(pdk, key, param, sw_crypt_encryptString(value));
		}


SWString
SWAppConfig::getRegistryString(const SWString &section, const SWString &param, const SWString &defval, SWRegistryHive pdk) const
		{
		checkConfigInitialised();

		SWString	s;
		
		SWString	key(m_regKey);
		
		if (!section.isEmpty())
			{
			key += "/";
			key += section;
			}
			
		s = (const sw_tchar_t *)sw_registry_getString(pdk, key, param, defval);

		return s;
		}
		
		
void
SWAppConfig::setRegistryString(const SWString &section, const SWString &param, const SWString &value, SWRegistryHive pdk)
		{
		checkConfigInitialised();

		SWString	key(m_regKey);
		
		if (!section.isEmpty())
			{
			key += "/";
			key += section;
			}
			
		sw_registry_setString(pdk, key, param, value);
		}


int
SWAppConfig::getRegistryInteger(const SWString &section, const SWString &param, int defval, SWRegistryHive pdk) const
		{
		checkConfigInitialised();

		int			s;
		SWString	key(m_regKey);
		
		if (!section.isEmpty())
			{
			key += "/";
			key += section;
			}
			
		s = sw_registry_getInteger(pdk, key, param, defval);

		return s;
		}
		
		
void
SWAppConfig::setRegistryInteger(const SWString &section, const SWString &param, int value, SWRegistryHive pdk)
		{
		checkConfigInitialised();

		SWString	key(m_regKey);
		
		if (!section.isEmpty())
			{
			key += "/";
			key += section;
			}
			
		sw_registry_setInteger(pdk, key, param, value);
		}


bool
SWAppConfig::getRegistryBoolean(const SWString &section, const SWString &param, bool defval, SWRegistryHive pdk) const
		{
		checkConfigInitialised();

		bool		s;
		SWString	key(m_regKey);
		
		if (!section.isEmpty())
			{
			key += "/";
			key += section;
			}
			
		s = sw_registry_getBoolean(pdk, key, param, defval);

		return s;
		}
		
		
void
SWAppConfig::setRegistryBoolean(const SWString &section, const SWString &param, bool value, SWRegistryHive pdk)
		{
		checkConfigInitialised();

		SWString	key(m_regKey);
		
		if (!section.isEmpty())
			{
			key += "/";
			key += section;
			}
			
		sw_registry_setBoolean(pdk, key, param, value);
		}

double
SWAppConfig::getRegistryDouble(const SWString &section, const SWString &param, double defval, SWRegistryHive pdk) const
		{
		checkConfigInitialised();

		double		s;
		SWString	key(m_regKey);
		
		if (!section.isEmpty())
			{
			key += "/";
			key += section;
			}
			
		s = sw_registry_getDouble(pdk, key, param, defval);

		return s;
		}
		
		
void
SWAppConfig::setRegistryDouble(const SWString &section, const SWString &param, double value, SWRegistryHive pdk)
		{
		checkConfigInitialised();

		SWString	key(m_regKey);
		
		if (!section.isEmpty())
			{
			key += "/";
			key += section;
			}
			
		sw_registry_setDouble(pdk, key, param, value);
		}


void
SWAppConfig::removeRegistryValue(const SWString &section, const SWString &param, SWRegistryHive pdk)
		{
		checkConfigInitialised();

		SWString	key(m_regKey);
		
		if (!section.isEmpty())
			{
			key += "/";
			key += section;
			}
			
		sw_registry_removeValue(pdk, key, param);
		}


void
SWAppConfig::removeRegistryKey(const SWString &section, SWRegistryHive pdk)
		{
		checkConfigInitialised();

		SWString	key(m_regKey);
		
		if (!section.isEmpty())
			{
			key += "/";
			key += section;
			}
			
		sw_registry_removeKey(pdk, key);
		}


SWString
SWAppConfig::getCurrencyString() const
		{
		return getString("SYMBOL_CURRENCY");
		}


SWString
SWAppConfig::getDateFormat(FormatType style) const
		{
		SWString	s;

		s = getString("FORMAT_DATE");

		switch (style)
			{
			case FMT_SWDate:
				// Do nothing
				break;

			case FMT_SWTime:
				s.replaceAll("DD", "%d");
				s.replaceAll("MM", "%m");
				s.replaceAll("YYYY", "%Y");
				s.replaceAll("YY", "%y");
				break;
			}

		return s;
		}


/*
h	Hours without leading zeros for single-digit hours (12-hour clock).
hh	Hours with leading zeros for single-digit hours (12-hour clock).
H	Hours without leading zeros for single-digit hours (24-hour clock).
HH	Hours with leading zeros for single-digit hours (24-hour clock).

m	Minutes without leading zeros for single-digit minutes.
mm	Minutes with leading zeros for single-digit minutes.

s	Seconds without leading zeros for single-digit seconds.
ss	Seconds with leading zeros for single-digit seconds.

t	One-character time marker string.
tt	Multi-character time marker string.
*/
SWString
SWAppConfig::getTimeFormat(FormatType style) const
		{
		SWString	s;

		s = getString("FORMAT_TIME");

		switch (style)
			{
			case FMT_SWDate:
				// Do nothing
				break;

			case FMT_SWTime:
				// Replace HH with %! before replace H then change %! to %H to avoiddouble replace
				s.replaceAll("HH", "%!");
				s.replaceAll("H", "%k");
				s.replaceAll("%!", "%H");

				s.replaceAll("hh", "%I");
				s.replaceAll("h", "%l");
				s.replaceAll("mm", "%M");
				s.replaceAll("m", "%M");
				s.replaceAll("ss", "%S");
				s.replaceAll("s", "%S");
				s.replaceAll("tt", "%P");
				s.replaceAll("t", "%p");
				break;
			}

		return s;
		}


/*
h	Hours without leading zeros for single-digit hours (12-hour clock).
hh	Hours with leading zeros for single-digit hours (12-hour clock).
H	Hours without leading zeros for single-digit hours (24-hour clock).
HH	Hours with leading zeros for single-digit hours (24-hour clock).

m	Minutes without leading zeros for single-digit minutes.
mm	Minutes with leading zeros for single-digit minutes.

s	Seconds without leading zeros for single-digit seconds.
ss	Seconds with leading zeros for single-digit seconds.

t	One-character time marker string.
tt	Multi-character time marker string.
*/
SWString
SWAppConfig::getTimestampFormat(FormatType style) const
		{
		SWString	s;

		s = getString("FORMAT_TIMESTAMP");

		switch (style)
			{
			case FMT_SWDate:
				// Do nothing
				break;

			case FMT_SWTime:
				s.replaceAll("DD", "%d");
				s.replaceAll("YYYY", "%Y");
				s.replaceAll("YY", "%y");

				// Replace MM with %! before replace M then change %! to %H to avoiddouble replace
				// s.replaceAll("MM", "%m");
				s.replaceAll("MM", "%!");
				s.replaceAll("mm", "%M");
				s.replaceAll("m", "%M");
				s.replaceAll("%!", "%m");

				// Replace HH with %! before replace H then change %! to %H to avoiddouble replace
				s.replaceAll("HH", "%!");
				s.replaceAll("H", "%k");
				s.replaceAll("%!", "%H");

				s.replaceAll("hh", "%I");
				s.replaceAll("h", "%l");
				s.replaceAll("ss", "%S");
				s.replaceAll("s", "%S");
				s.replaceAll("tt", "%P");
				s.replaceAll("t", "%p");
				break;
			}

		return s;
		}


void
SWAppConfig::getResourceFolderList(SWStringArray &list)
		{
		list.clear();

		list.add(getFolder(UserResourcesDir));
		list.add(getFolder(ProgramResourcesDir));
		list.add(getFolder(ProgramDir, "resources"));
		list.add(getFolder(ProgramDir, "../resources"));

		for (int i=0;i<(int)m_resourceFolderList.size();i++)
			{
			list.add(m_resourceFolderList.get(i));
			}
		}


SWString
SWAppConfig::getResourceFile(const SWString &filename) const
		{
		checkConfigInitialised();

		SWString	res;

		res = getResourceFile(filename, getFolder(UserResourcesDir));
		
		if (res.isEmpty())
			{
			res = getResourceFile(filename, getFolder(ProgramResourcesDir));
			}
		
		if (res.isEmpty())
			{
			res = getResourceFile(filename, getFolder(ProgramDir, "resources"));
			}
		
		if (res.isEmpty())
			{
			res = getResourceFile(filename, getFolder(ProgramDir, "../resources"));
			}

		for (int i=0;res.isEmpty()&&i<(int)m_resourceFolderList.size();i++)
			{
			res = getResourceFile(filename, m_resourceFolderList.get(i));
			}

		return res;
		}



SWString
SWAppConfig::getResourceFile(const SWString &filename, const SWString &resourcedir) const
		{
		checkConfigInitialised();

		bool		found=false;
		SWLocale	&clc=SWLocale::getCurrentLocale();
		SWLocale	&ulc=SWLocale::getUserLocale();
		SWLocale	&slc=SWLocale::getSystemLocale();
		SWString	folder, path;

		// Check the current locale
		if (!found)
			{
			path = SWFilename::concatPaths(SWFilename::concatPaths(resourcedir, clc.code()), filename);
			found = SWFileInfo::isFile(path);
			}

		if (!found)
			{
			path = SWFilename::concatPaths(SWFilename::concatPaths(resourcedir, clc.languageCode()), filename);
			found = SWFileInfo::isFile(path);
			}

		// Check the user locale if different
		if (ulc.id() != clc.id())
			{
			if (!found)
				{
				path = SWFilename::concatPaths(SWFilename::concatPaths(resourcedir, ulc.code()), filename);
				found = SWFileInfo::isFile(path);
				}

			if (!found)
				{
				path = SWFilename::concatPaths(SWFilename::concatPaths(resourcedir, ulc.languageCode()), filename);
				found = SWFileInfo::isFile(path);
				}
			}

		// Check the system locale if different from the previous 2
		if (slc.id() != clc.id() && slc.id() != ulc.id())
			{
			if (!found)
				{
				path = SWFilename::concatPaths(SWFilename::concatPaths(resourcedir, slc.code()), filename);
				found = SWFileInfo::isFile(path);
				}

			if (!found)
				{
				path = SWFilename::concatPaths(SWFilename::concatPaths(resourcedir, slc.languageCode()), filename);
				found = SWFileInfo::isFile(path);
				}
			}

		// Finally check all neutral locale (i.e. just the resource dir)
		if (!found)
			{
			path = SWFilename::concatPaths(resourcedir, filename);
			found = SWFileInfo::isFile(path);
			}

		if (!found)
			{
			path.empty();
			}
		else
			{
			path = SWFilename::absolutePath(path, SWFolder::getCurrentDirectory(), SWFilename::PathTypePlatform);
			path = SWFilename::fix(path);
			}

		return path;
		}


SWString
SWAppConfig::getCompanyWebsite() const
		{
		SWString	website=getCompanyURL();

		if (website.startsWith("http://")) website.remove(0, 7);
		else if (website.startsWith("https://")) website.remove(0, 8);

		if (website.endsWith("/")) website.remove((int)website.length()-1, 1);

		return website;
		}


