/**
*** @file		SWFlags.cpp
*** @brief		A class to represent a set of boolean flags.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWFlags class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <swift/SWFlags.h>
#include <swift/SWException.h>
//#include <swift/SWInputStream.h>
//#include <swift/SWOutputStream.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif



// We currently use an array of ints to hold the bits
// but we can easily change this and redefine the following 
// typedef
typedef int	BitContainer;

#define CONTAINER_BITSIZE	(int)(sizeof(BitContainer) * 8)

// Given the number of bits (n) this macro returns a mask
// for that number of bits. E.g. if n == 4 the mask is 0x000000f
static inline int	MASK(int n)						{ return (n>=CONTAINER_BITSIZE?~0:(1<<(n))-1); }
static inline int	sw_math_max(int a, int b)       { return a>b?a:b; }
static inline int	sw_math_min(int a, int b)       { return a<b?a:b; }



// Default constructor
SWFlags::SWFlags(int nbits, bool fixedSize) :
    _nbits(nbits),
    _fixedSize(fixedSize)
		{
		// We must make sure the array is pre-allocated
		int	i, n;

		n = (nbits / CONTAINER_BITSIZE) + 1;
		for (i=0;i<n;i++) _array[i] = 0;
		}


// String constructor
SWFlags::SWFlags(const SWString &str, bool fixedSize) :
    _nbits(0),
    _fixedSize(false)
		{
		*this = str;
		_fixedSize = fixedSize;
		}


// String assignment operator
const SWFlags &
SWFlags::operator=(const SWString &str)
		{
		int		i, l, slen;

		clear();
		l = slen = (int)str.length();

		for (i=0;i<slen;i++)
			{
			--l;
			if (_fixedSize && l >= _nbits) continue;
			setBit(l, str[i]=='1');
			}

		return *this;
		}


// Assign bits from the given integer
const SWFlags &
SWFlags::operator=(int i)
		{
		clear();
		if (!_fixedSize) _nbits = sizeof(int) * 8;
		_array[0] = i & MASK(sw_math_min(sizeof(int) * 8, _nbits));
		return *this;
		}


// Clear the bit set.
void
SWFlags::clear()
		{
		_array.clear();
		if (!_fixedSize) _nbits = 0;
		}


// Sets all the bits in the set to 0
void
SWFlags::clearAllBits()
		{
		int	i, nb=_nbits;

		for (i=0;nb>0;i++,nb-=CONTAINER_BITSIZE) _array[i] = 0;
		}


// Sets all the bits in the set to 1
void
SWFlags::setAllBits()
		{
		int	i, nb=_nbits;

		for (i=0;nb>CONTAINER_BITSIZE;i++,nb-=CONTAINER_BITSIZE) _array[i] = ~0;
		_array[i] = MASK(nb);
		}


// Toggle all the bits in the set to 1
void
SWFlags::toggleAllBits()
		{
		int	i, nb=_nbits;

		for (i=0;nb>CONTAINER_BITSIZE;i++,nb-=CONTAINER_BITSIZE) _array[i] ^= ~0;
		_array[i] ^= MASK(nb);
		}


// Copy constructor
SWFlags::SWFlags(const SWFlags &v) :
	SWBitSet(),
    _nbits(0),
    _fixedSize(false)
		{
		*this = v;
		}


// Assignment operator
const SWFlags &
SWFlags::operator=(const SWFlags &v)
		{
		_nbits = v._nbits;
		_fixedSize = v._fixedSize;
		_array = v._array;

		return *this;
		}


// Set the index and offset for the given block
// returns -1 (out of range) or 0 (OK)
void
SWFlags::calcIndexAndOffset(int n, int &index, int &offset) const
		{
		index = n / CONTAINER_BITSIZE;
		offset = n % CONTAINER_BITSIZE;
		}



// Set the index and offset for the given block
// returns -1 (out of range) or 0 (OK)
void
SWFlags::getIndexAndOffset(int n, int &index, int &offset)
		{
		if (n < 0 || (n >= _nbits && _fixedSize)) throw SW_EXCEPTION(SWIndexOutOfBoundsException);

		// Update bit count
		if (n >= _nbits) _nbits = n + 1;

		calcIndexAndOffset(n, index, offset);
		}




// Set the nth bit to the specified value
void
SWFlags::setBit(int n, bool v)
		{
		int	index, offset;

		getIndexAndOffset(n, index, offset);

		int	vbits=0;
		
		if (index < (int)_array.size()) vbits =_array[index];

		if (v) vbits |= (1<<offset);
		else vbits &= ~(1<<offset);
		_array[index] = vbits;
		}


// Set the nth bit to the specified value
bool
SWFlags::getBit(int n) const
		{
		if (n < 0 || (n >= _nbits && _fixedSize)) throw SW_EXCEPTION(SWIndexOutOfBoundsException);

		bool	res=false;

		if (n < _nbits)
			{
			int	index, offset;

			index = n / CONTAINER_BITSIZE;
			offset = n % CONTAINER_BITSIZE;

			int	vbits=_array.get(index);
			res = ((vbits&(1<<offset)) != 0);
			}

		return res;
		}


SWString
SWFlags::toString()
		{
		SWString	str;

		for (int i=0;i<size();i++)
			{
			str += getBit(size()-i-1)?_T('1'):_T('0');
			}

		return str;
		}


/// AND operator
SWFlags &	
SWFlags::operator&=(const SWFlags &v)
		{
		int	tidx, toff;	// this offset and index
		int	vidx, voff;	// v offset and index
		int	vsize, tsize;
		int	i=0;

		// Adjust the size as required if it's not fixed
		if (!_fixedSize) _nbits = sw_math_max(v._nbits, _nbits);

		getIndexAndOffset(_nbits-1, tidx, toff);
		v.calcIndexAndOffset(v._nbits-1, vidx, voff);

		vsize = voff;
		if (vidx-tidx > 0) vsize += (vidx-tidx) * CONTAINER_BITSIZE;

		tsize = toff;
		if (tidx-vidx > 0) tsize += (tidx-vidx) * CONTAINER_BITSIZE;

		// first AND all whole BitContainers
		for (i=0;i<tidx&&i<vidx;i++) _array[i] &= v._array.get(i);

		if (vidx > tidx && !_fixedSize)
			{
			// We have more whole BitContainers available in v
			// We must grow to accomodate v's extra bits
			// We assume here that all the previous unused bits
			// in our array are initialised to zero.
			while (i <= vidx) 
				{
				_array[i] &= v._array.get(i);
				i++;
				}
			}
		else
			{
			// We have run out of whole BitContainers either from v
			// or for ourselves. Therefore we AND in the
			// the last portion masked by the minimum of
			// our size and v's size
			_array[i] &= v._array.get(i) & MASK(sw_math_min(vsize, tsize)+1);

			// Finally zero out any of our bits that weren't touched
			// which by definition should go to zero
			while (++i <= tidx) _array[i] = 0;
			}

		return *this;
		}


/// OR operator
SWFlags &	
SWFlags::operator|=(const SWFlags &v)
		{
		int	tidx, toff;	// this offset and index
		int	vidx, voff;	// v offset and index
		int	vsize, tsize;
		int	i=0;

		// Adjust the size as required if it's not fixed
		if (!_fixedSize) _nbits = sw_math_max(v._nbits, _nbits);

		getIndexAndOffset(_nbits-1, tidx, toff);
		v.calcIndexAndOffset(v._nbits-1, vidx, voff);

		vsize = voff;
		if (vidx-tidx > 0) vsize += (vidx-tidx) * CONTAINER_BITSIZE;

		tsize = toff;
		if (tidx-vidx > 0) tsize += (tidx-vidx) * CONTAINER_BITSIZE;

		// first OR all whole BitContainers
		for (i=0;i<tidx&&i<vidx;i++) _array[i] |= v._array.get(i);

		if (vidx > tidx && !_fixedSize)
			{
			// We have more whole BitContainers available in v
			// We must grow to accomodate v's extra bits
			// We assume here that all the previous unused bits
			// in our array are initialised to zero.
			while (i <= vidx) 
				{
				_array[i] |= v._array.get(i);
				i++;
				}
			}
		else
			{
			// We have run out of whole BitContainers either from v
			// or for ourselves. Therefore we OR in the
			// the last portion masked by the minimum of
			// our size and v's size
			_array[i] |= v._array.get(i) & MASK(sw_math_min(vsize, tsize)+1);
			}

		return *this;
		}


/// XOR operator
SWFlags &	
SWFlags::operator^=(const SWFlags &v)
		{
		int	tidx, toff;	// this offset and index
		int	vidx, voff;	// v offset and index
		int	vsize, tsize;
		int	i=0;

		// Adjust the size as required if it's not fixed
		if (!_fixedSize) _nbits = sw_math_max(v._nbits, _nbits);

		getIndexAndOffset(_nbits-1, tidx, toff);
		v.calcIndexAndOffset(v._nbits-1, vidx, voff);

		vsize = voff;
		if (vidx-tidx > 0) vsize += (vidx-tidx) * CONTAINER_BITSIZE;

		tsize = toff;
		if (tidx-vidx > 0) tsize += (tidx-vidx) * CONTAINER_BITSIZE;

		// first OR all whole BitContainers
		for (i=0;i<tidx&&i<vidx;i++) _array[i] ^= v._array.get(i);

		if (vidx > tidx && !_fixedSize)
			{
			// We have more whole BitContainers available in v
			// We must grow to accomodate v's extra bits
			// We assume here that all the previous unused bits
			// in our array are initialised to zero.
			while (i <= vidx) 
				{
				_array[i] ^= v._array.get(i);
				i++;
				}
			}
		else
			{
			// We have run out of whole BitContainers either from v
			// or for ourselves. Therefore we OR in the
			// the last portion masked by the minimum of
			// our size and v's size
			_array[i] ^= v._array.get(i) & MASK(sw_math_min(vsize, tsize)+1);
			}

		return *this;
		}


/// Dump this object
void
SWFlags::dump()
		{
		int	n = (int)_array.size();

		printf("_nbits=%d  _fixedSize=%d  _arraySize=%zd\n", _nbits, _fixedSize, _array.size());
		while (--n >= 0)
			{
			printf("%08x ", _array[n]);
			}
		putchar('\n');
		}


/*
sw_status_t
SWFlags::writeToStream(SWOutputStream &stream) const
		{
		sw_status_t	res;

		res = stream.write((sw_int32_t)_nbits);
		if (res == SW_STATUS_SUCCESS) res = stream.write(_array);

		return res;
		}


sw_status_t
SWFlags::readFromStream(SWInputStream &stream)
		{
		sw_status_t		res;
		sw_int32_t		n;
		SWIntegerArray	ia;

		res = stream.read(n);
		if (res == SW_STATUS_SUCCESS) res = stream.read(ia);
		if (res == SW_STATUS_SUCCESS)
			{
			if (_fixedSize && n > _nbits)
				{
				// This is a fixed size flags set and there is too much data
				// to fit within the number of flags
				res = SW_STATUS_TOO_MUCH_DATA;
				}
			else
				{
				clear();
				_nbits = n;
				_array = ia;
				}
			}

		return res;
		}
*/	



