/**
*** @file		SWStdioFile.cpp
*** @brief		Basic file handling class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWStdioFile class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



// Needed for precompiled header support
#include "stdafx.h"

#include <swift/swift.h>
#include <swift/SWStdioFile.h>
#include <swift/SWFolder.h>
#include <swift/SWGuard.h>

#include <stdio.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





/*
** Default constructor
*/
SWStdioFile::SWStdioFile() :
	m_hFile(0)
		{
		}


/*
** Destructor
*/
SWStdioFile::~SWStdioFile()
		{
		close();
		}


sw_status_t
SWStdioFile::open(const SWString &filename, const SWString &mode)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (isOpen())
			{
			res = SW_STATUS_ALREADY_OPEN;
			}
		else
			{
			SWGuard	guard(this);

#if defined(SW_PLATFORM_WINDOWS)
	#pragma warning(disable: 4996)
#endif
			m_hFile = fopen(filename, mode);

#if defined(SW_PLATFORM_WINDOWS)
	#pragma warning(default: 4996)
#endif
			if (m_hFile == NULL)
				{
				res = errno;
				}
			}

		return res;
		}


/*
** Close the file
**
** @return true if successful, false otherwise
*/
sw_status_t
SWStdioFile::close()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		SWGuard		guard(this);

		if (isOpen())
			{
			fclose(*this);
			m_hFile = NULL;
			}

		return res;
		}



bool
SWStdioFile::isOpen() const
		{
		return (m_hFile != NULL);
		}


void
SWStdioFile::flush()
		{
		if (isOpen())
			{
			fflush(*this);
			}
		}




