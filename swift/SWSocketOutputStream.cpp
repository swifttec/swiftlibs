/**
*** @file		SWSocketOutputStream.cpp
*** @brief		A generic socket implementation
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWSocketOutputStream class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <swift/SWSocketOutputStream.h>
#include <swift/SWSocket.h>
#include <swift/SWException.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




SWSocketOutputStream::SWSocketOutputStream() :
	_pSocket(NULL),
	_delFlag(false)
		{
		}


SWSocketOutputStream::SWSocketOutputStream(SWSocket *pSocket, bool delflag) :
	_pSocket(pSocket),
	_delFlag(delflag)
		{
		}


SWSocketOutputStream::~SWSocketOutputStream()
		{
		if (_delFlag)
			{
			delete _pSocket;
			_pSocket = 0;
			}
		}


sw_status_t
SWSocketOutputStream::writeData(const void *pData, sw_size_t len)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		sw_size_t	count;

		do
			{
			res = _pSocket->write(pData, len, &count);
			if (res == SW_STATUS_SUCCESS)
				{
				len -= (sw_uint32_t)count;
				pData = (char *)pData + count;
				}
			}
		while (res == SW_STATUS_SUCCESS && len > 0);

		return res;
		}



void
SWSocketOutputStream::setSocket(SWSocket *pSocket, bool delflag)
		{
		if (_pSocket != NULL && pSocket != _pSocket && _delFlag)
			{
			// Delete the previously held socket
			delete _pSocket;
			}

		_pSocket = pSocket;
		_delFlag = delflag;
		}




