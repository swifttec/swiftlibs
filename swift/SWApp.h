/****************************************************************************
**	SWApp.h
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __swift_SWApp_h__
#define __swift_SWApp_h__

#include <swift/swift.h>

#include <swift/SWDate.h>
#include <swift/SWTime.h>
#include <swift/sw_registry.h>

#include <swift/SWAppConfig.h>
#include <swift/SWLockableObject.h>

class SWIFT_DLL_EXPORT SWApp : public SWLockableObject
		{
public:
		enum ExclusionType
			{
			UNIQUE_TO_SYSTEM=0,
			UNIQUE_TO_DESKTOP,
			UNIQUE_TO_SESSION,
			UNIQUE_TO_TRUSTEE,
			};

		enum HelpFileType
			{
			TextFile=0,
			HtmlFile
			};

		enum AppTypeID
			{
			Console	= 1,
			GUI		= 2,
			Service	= 4
			};

public:
		SWApp(SWAppConfig *pAppConfig=NULL, int appflags=Console, bool mainapp=true);
		virtual ~SWApp();


		/**
		*** Initialise the application instance
		***
		*** This method is called by the execute code and should be overwritten by the developer
		*** in order to create the primary window. 
		***
		*** @param	prog	The path of the program being executed
		*** @param	args	The command line args
		**/
		virtual bool	initInstance(const SWString &prog, const SWStringArray &args);

		/**
		*** Exit the application instance
		***
		*** This method is called when the runInstance method is exited to allow the program
		*** to cleanup any resources.
		**/
		virtual int		exitInstance();

		/**
		*** Initialise the application instance
		***
		*** This should be overwritten by the developer as the main execute thread code.
		**/
		virtual void	runInstance();

		void			setHelpFile(const SWString &filename, int filetype=HtmlFile);
		
		SWString		createExclusionName(const SWString &guid, ExclusionType kind=UNIQUE_TO_SYSTEM);

		void			setAppID(const SWString &v);
		void			setAppTitle(const SWString &v);
		void			setRegistryKey(const SWString &v);

		// Call to launch the application help
		virtual void	onAppHelp();

		/// Show the help file
		virtual bool	showHelp(const SWString &filename);
 
		/// Get the exit code set when runInstance returned.
		int				exitcode() const			{ return m_exitcode; }

		/// Test to see if this is a console app
		bool			isConsoleApp() const		{ return ((m_appFlags & Console) != 0); }

		/// Test to see if this is a GUI (windowed) app
		bool			isGuiApp() const			{ return ((m_appFlags & GUI) != 0); }

public: // Overrides
		SWAppConfig		*m_pAppConfig;
		bool			m_mainApp;

public:
		static SWApp	*m_pMainApp;


public: // Static methods

		/// Internal method used by SW_APP_EXECUTE - Do not call directly
		static int		execute(SWApp &app, const SWString &prog, const SWStringArray &args);

protected:
		SWString		m_appExecutable;
		SWStringArray	m_appArgs;
		SWString		m_appID;
		SWString		m_appTitle;
		SWString		m_appRegistryKey;
		SWString		m_appHelpFile;
		int				m_appHelpFileType;
		int				m_exitcode;
		int				m_appFlags;
		bool			m_pAppConfigDelFlag;
		};


#if defined(SW_PLATFORM_WINDOWS)

	#define SW_APP_EXECUTE(app)	\
		int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) \
				{ \
				SWString			prog; \
				SWStringArray	args; \
				TCHAR			path[1024]; \
				GetModuleFileName(NULL, path, sizeof(path)); \
				prog = path; \
				sw_main_parseCommandLine(GetCommandLine(), prog, args, SW_MAIN_CMDLINE_FLAGS); \
				return SWApp::execute(app, prog, args); \
				}

#else

	#define SW_APP_EXECUTE(app)	\
		int main(int argc, char **argv) \
				{ \
				SWString			prog = argv[0]; \
				SWStringArray	args; \
				for (int i=1;i<argc;i++) args.add(argv[i]); \
				return SWApp::execute(app, prog, args); \
				}

#endif

#endif // __swift_SWApp_h__
