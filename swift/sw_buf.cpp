// PalBase.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"

#include <swift/sw_buf.h>

// Read a signed integer from a memory buffer (max 8 bytes, 64 bits)
SWIFT_DLL_EXPORT sw_int64_t
sw_buf_getSignedInteger(void *buf, int nbytes, int order)
		{
		sw_int64_t	v=0;
		bool	first=true;
		bool	negative=false;

		if (order == SW_BYTEORDER_BIG_ENDIAN)
			{
			sw_byte_t	*bp=((sw_byte_t *)buf);

			v = 0;
			while (nbytes-- > 0)
				{
				sw_byte_t	bv=*bp++;

				if (first && (bv & 0x80) != 0) negative = true;
				first = false;
				if (negative) bv ^= 0xff;
				v = ((v<<8)|((bv)&0xff));
				}
			}
		else
			{
			sw_byte_t	*bp=((sw_byte_t *)buf)+nbytes-1;

			v = 0;
			while (nbytes-- > 0)
				{
				sw_byte_t	bv=*bp--;

				if (first && (bv & 0x80) != 0) negative = true;
				first = false;
				if (negative) bv ^= 0xff;
				v = ((v<<8)|((bv)&0xff));
				}
			}

		if (negative)
			{
			v = -v - 1;
			}

		return v;
		}


// Write a signed integer into a memory buffer (max 8 bytes, 64 bits)
SWIFT_DLL_EXPORT void
sw_buf_putSignedInteger(void *buf, sw_int64_t v, int nbytes, int order)
		{
		int			count=nbytes;

		if (order == SW_BYTEORDER_BIG_ENDIAN)
			{
			sw_byte_t	*bp=((sw_byte_t *)buf)+nbytes-1;

			while (count-- > 0)
				{
				*bp-- = (sw_byte_t)(v & 0xff);
				v >>= 8;
				}
			}
		else
			{
			sw_byte_t	*bp=((sw_byte_t *)buf);

			while (count-- > 0)
				{
				*bp++ = (sw_byte_t)(v & 0xff);
				v >>= 8;
				}
			}
		}


// Read an unsigned integer from a memory buffer (max 8 bytes, 64 bits)
SWIFT_DLL_EXPORT sw_uint64_t
sw_buf_getUnsignedInteger(void *buf, int nbytes, int order)
		{
		sw_uint64_t	v=0;
	
		if (order == SW_BYTEORDER_BIG_ENDIAN)
			{
			sw_byte_t	*bp=((sw_byte_t *)buf);

			v = 0;
			while (nbytes-- > 0)
				{
				v = ((v<<8)|((*bp++)&0xff));
				}
			}
		else
			{
			sw_byte_t	*bp=((sw_byte_t *)buf)+nbytes-1;

			v = 0;
			while (nbytes-- > 0)
				{
				v = ((v<<8)|((*bp--)&0xff));
				}
			}

		return v;
		}


// Write an unsigned integer into a memory buffer (max 8 bytes, 64 bits)
SWIFT_DLL_EXPORT void
sw_buf_putUnsignedInteger(void *buf, sw_uint64_t v, int nbytes, int order)
		{
		int			count=nbytes;

		if (order == SW_BYTEORDER_BIG_ENDIAN)
			{
			sw_byte_t	*bp=((sw_byte_t *)buf)+nbytes-1;

			while (count-- > 0)
				{
				*bp-- = (sw_byte_t)(v & 0xff);
				v >>= 8;
				}
			}
		else
			{
			sw_byte_t	*bp=((sw_byte_t *)buf);

			while (count-- > 0)
				{
				*bp++ = (sw_byte_t)(v & 0xff);
				v >>= 8;
				}
			}
		}

