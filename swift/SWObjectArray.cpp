/**
*** @file		SWObjectArray.cpp
*** @brief		An array of Objects
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWObjectArray class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <swift/SWObjectArray.h>
#include <swift/SWException.h>
#include <swift/SWInputStream.h>
#include <swift/SWOutputStream.h>
#include <swift/SWGuard.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




SWObjectArray::SWObjectArray(sw_size_t initialSize, bool delFlag) :
    SWAbstractArray(NULL, sizeof(SWObject *)),
    m_delFlag(delFlag)
		{
		ensureCapacity(initialSize);
		}


SWObjectArray::~SWObjectArray()
		{
		clear();
		}


SWObjectArray::SWObjectArray(const SWObjectArray &ua, bool delFlag) :
    SWAbstractArray(NULL, sizeof(SWObject *)),
    m_delFlag(delFlag)
		{
		*this = ua;
		}


const SWObjectArray &
SWObjectArray::operator=(const SWObjectArray &ua)
		{
		SWGuard		guard(this);
		sw_size_t	n = ua.size();

		clear();

		// Can we copy the flag?
		// m_delFlag = ua.m_delFlag;

		ensureCapacity(n);
		memcpy(m_pData, ua.m_pData, n * sizeof(SWObject *));
		m_nelems = ua.m_nelems;

		return *this;
		}


void
SWObjectArray::clear()
		{
		remove(0, m_nelems);
		}


void
SWObjectArray::remove(sw_index_t pos, sw_size_t count)
		{
		SWGuard	guard(this);

		if (m_delFlag)
			{
			for (sw_index_t i=pos;i<(sw_index_t)m_nelems&&i<(pos+(sw_index_t)count);i++)
				{
				SWObject    *p = (*this)[i];
				delete p;
				(*this)[i] = 0;
				}
			}

		// Now call the clear
		SWAbstractArray::remove(pos, count);
		}


SWObject * &	
SWObjectArray::operator[](sw_index_t i)
		{
		if (i < 0) throw SW_EXCEPTION(SWArrayIndexOutOfBoundsException);

		SWGuard	guard(this);

		ensureCapacity(i+1);
		if (i >= (sw_index_t)m_nelems) m_nelems = i+1;

		SWObject **ip = (SWObject **)(m_pData + (i * sizeof(SWObject *)));

		return *ip;
		}



SWObject * &	
SWObjectArray::get(sw_index_t i) const
		{
		if (i < 0 || i >= (sw_index_t)m_nelems) throw SW_EXCEPTION(SWArrayIndexOutOfBoundsException);

		SWObject **ip = (SWObject **)(m_pData + (i * sizeof(SWObject *)));

		return *ip;
		}


SWObject *
SWObjectArray::pop()
		{
		if (m_nelems < 1) throw SW_EXCEPTION(SWArrayIndexOutOfBoundsException);

		SWObject **ip = (SWObject **)(m_pData + ((--m_nelems) * sizeof(SWObject *)));

		return *ip;
		}


/**
*** Add all the elements to this array
**/
void
SWObjectArray::add(const SWObjectArray &oa)
		{
		SWGuard		guard(this);
		sw_size_t	n = oa.size();
		sw_size_t	s = size();

		ensureCapacity(n + s);
		memcpy(m_pData + (s * sizeof(SWObject *)), oa.m_pData, n * sizeof(SWObject *));
		m_nelems += n;
		}


void
SWObjectArray::add(SWObject *pObject)
		{
		(*this)[(sw_index_t)m_nelems] = pObject;
		}


void
SWObjectArray::insert(SWObject *pObject, sw_index_t pos)
		{
		insertElements(pos, 1);
		(*this)[pos] = pObject;
		}


void
SWObjectArray::push(SWObject *pObject)
		{
		(*this)[(sw_index_t)m_nelems] = pObject;
		}




