/**
*** @file		SWInterlockedInt64.h
*** @brief		An interlocked 64-bit signed integer.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWInterlockedInt64 class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWInterlockedInt64_h__
#define __SWInterlockedInt64_h__

#include <swift/SWInterlocked.h>
#include <swift/SWGuard.h>



// Forward declarations


/**
*** @brief	An interlocked 64-bit signed integer.
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***             <td><b>Attribute</b></td>
***             <td><b>Value</b></td>
*** </tr>
*** <tr>
***             <td>MT-Level</td>
***             <td>Safe</td>
*** </tr>
*** </table>
***
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWInterlockedInt64 : public SWInterlocked
		{
public:
		/// Constructor
		SWInterlockedInt64(sw_int64_t v);

		/// Virtual destructor
		virtual ~SWInterlockedInt64();

		/// Assignment operator
		const sw_int64_t &	operator=(sw_int64_t v);

		/// "Cast" operator
		operator sw_int64_t() const;

		/// Prefix increment operator.
		const sw_int64_t &	operator++();

		/// Postfix increment operator.
		sw_int64_t			operator++(int);

		/// Prefix decrement operator.
		const sw_int64_t &	operator--();

		/// Postfix decrement operator.
		sw_int64_t			operator--(int);

private:
		sw_int64_t	m_value;	///< the value
		};




#endif
