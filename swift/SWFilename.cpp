/**
*** @file		SWFilename.cpp
*** @brief		A class for manipulating file names.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWFilename class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




// Needed for precompiled header support
#include "stdafx.h"

#include <swift/SWFilename.h>
#include <swift/SWArray.h>
#include <swift/SWInputStream.h>
#include <swift/SWOutputStream.h>


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif






// Default constructor
SWFilename::SWFilename(const SWString &path, PathType type)
		{
		if (type != PathTypeUnix && type != PathTypeWindows)
			{
#if defined(WIN32) || defined(_WIN32)
			m_type = PathTypeWindows;
#else
			m_type = PathTypeUnix;
#endif
			}

		m_path = fix(path, m_type);
		}


// Copy constructor
SWFilename::SWFilename(const SWFilename &other) :
	SWObject()
		{
		*this = other;
		}


/*
** Destructor
*/
SWFilename::~SWFilename()
		{
		}


// Assignment operator
SWFilename &
SWFilename::operator=(const SWFilename &other)
		{
		m_path = other.m_path;
		m_type = other.m_type;

		return *this;
		}


// Assignment operator
SWFilename &
SWFilename::operator=(const SWString &path)
		{
		m_path = fix(path, PathTypeNoChange);
		m_type = PathTypeNoChange;

		return *this;
		}


// Test to see if the given path is relative
bool
SWFilename::isRelativePath() const
		{
		return isRelativePath(m_path);
		}


// Test to see if the given path is relative (STATIC VERSION)
bool
SWFilename::isRelativePath(const SWString &path)
		{
		if (path.length() > 0) return !isAbsolutePath(path);

		return false;
		}


// Test to see if the given path is absolute
bool
SWFilename::isAbsolutePath() const
		{
		return isAbsolutePath(m_path);
		}


// Test to see if the given path is absolute (STATIC VERSION)
bool
SWFilename::isAbsolutePath(const SWString &path)
		{
		if (path.length() > 0)
			{
			if (path[0] == '/' || path[0] == '\\') return true;

			if (path.length() > 2 && path[1] == ':' && (path[2] == '\\' || path[2] == '/')) return true;
			}

		return false;
		}



// Fix the given path (STATIC)
SWString
SWFilename::fix(const SWString &filename, PathType type)
		{
		SWString	f=filename, sep;
		int			l=(int)f.length(), p;

		switch (type)
			{
			case PathTypeNoChange:
			case PathTypePlatform:
#if defined(WIN32) || defined(_WIN32)
				sep = "\\";
				if (type != PathTypeNoChange) f.replaceAll('/', '\\');
#else
				sep = "/";
				if (type != PathTypeNoChange) f.replaceAll('\\', '/');
#endif
				break;

			case PathTypeWindows:
				sep = "\\";
				f.replaceAll('/', '\\');
				break;

			case PathTypeUnix:
				sep = "/";
				f.replaceAll('\\', '/');
				break;
			}

		// Only work on non null strings
		if (l > 0)
			{
			// Check for C: case (append \)
			if (l == 2 && f[1] == _T(':'))
				{
				f += sep;
				l++;
				}
			else
				{
				// Check for trailing path separator
				while (l > 1 && (f[l-1] == _T('/') || f[l-1] == _T('\\')))
					{
					if (l == 3 && f[1] == _T(':')) break;

					if (l > 3 || (l > 2 && f[1] != _T(':'))) f.remove(--l, 1);
					}
				//if (l > 3 && f[l-1] == _T('\\')) f.remove(--l, 1);

				// Check for double separators
				while ((p = f.indexOf(_T("//"))) >= 0)
					{
					f.remove(p, 1);
					}

				// Check for double separators but not in 1st position
				// which is allowed for Dos paths (e.g. \\server\...)
				while ((p = f.indexOf(_T("\\\\"), 1)) >= 0)
					{
					f.remove(p, 1);
					}
				}
			}

		return f;
		}



/**
*** Split a path into it's component parts. (STATIC)
**/
sw_size_t
SWFilename::splitPath(const SWString &path, SWStringArray &sa)
		{
		const sw_tchar_t	*sp, *tp;
		sw_index_t			idx=0;
		bool				root=false;

		sa.clear();

		tp = path;

		if (path.startsWith(_T("/")))
			{
			// Absolute Unix path
			sa[idx++] = _T("/");
			tp++;
			root = true;
			}
		else if (path.startsWith(_T("\\\\")))
			{
			// UNC Path \\server\xxx.....
			sa[idx++] = _T("\\\\");
			tp += 2;
			root = true;
			}
		else if (path.length() > 2 && path[1] == ':' && (path[2] == '\\' || path[2] == '/'))
			{
			// Dos path x:\....
			sa[idx++] = path.substring(0, 2);
			tp += 3;
			root = true;
			}

		while (*tp)
			{
			// Ignore duplicate separators
			while (*tp == '/' || *tp == '\\') tp++;

			sp = tp;
			while (*tp && *tp != '/' && *tp != '\\') tp++;

			SWString	tmp(sp, (int)(tp-sp));

			if (*tp) tp++;

			if (tmp == _T(".")) continue;
			if (tmp == _T(".."))
				{
				if (idx > 0)
					{
					// Make sure we don't go above root!
					if ((root && idx == 1) || sa[idx-1] == _T("..")) continue;
					idx--;
					continue;
					}
				}

			sa[idx++] = tmp;
			}

		// truncate the array to the correct size
		if ((sw_size_t)idx < sa.size())
			{
			sa.remove(idx, sa.size()-idx);
			}

		return (sw_size_t)idx;
		}


/**
*** Optimize a path by reducing duplicate path separators
*** . and .. where possible
**/
SWString	
SWFilename::optimizePath(const SWString &path, PathType type)
		{
		SWStringArray	sa;
		SWString		tfname=path;


		switch (type)
			{
			case PathTypePlatform:
#if defined(WIN32) || defined(_WIN32)
				tfname.replaceAll('/', '\\');
#else
				tfname.replaceAll('\\', '/');
#endif
				break;

			case PathTypeUnix:
				tfname.replaceAll('\\', '/');
				break;

			case PathTypeWindows:	
				tfname.replaceAll('/', '\\');
				break;

			default:
				// Do nothing
				break;
			}

		splitPath(tfname, sa);
		return joinPath(sa, type);
		}


/**
*** Joins path components to make a path
**/
SWString	
SWFilename::joinPath(SWStringArray &sa, PathType type)
		{
		int				c;
		int				i, n=0;
		SWString		path;
		bool			root=false;
		SWArray<int>	ia;

		switch (type)
			{
			case PathTypeUnix:	c = _T('/');		break;
			case PathTypeWindows:	c = _T('\\');		break;
			default:	c = SW_PATH_SEPARATOR;	break;
			}
		
		// First analyse the path to remove .. and .
		// and build up an index list in ia
		for (i=0;i<(int)sa.size();i++)
			{
			if (i == 0 && isAbsolutePath(sa[i])) root = true;

			// Ignore references to .
			if (sa[i] == _T(".")) continue;

			if (sa[i] == _T(".."))
				{
				if (n > 0 && sa[ia[n-1]] != _T(".."))
					{
					--n;
					continue;
					}
				}

			// Add this position to the array
			ia[n++] = i;
			}

		// Now simply join the parts together
		for (i=0;i<n;i++)
			{
			if (i > (root?1:0)) path += c;
			path += sa[ia[i]];
			}

		// Make sure we have a valid path
		if (path.isEmpty()) path = _T(".");

		return path;
		}


/**
*** Returns the absolute path name for the given path
*** in relation to the specified directory (default current)
***
*** If the neither the directory nor the path passed to this function are absolute
*** it is impossible to generate an absolute path and the result will be the same
*** as calling concatPaths.
***
*** @param path		The path name
*** @param dir		The directory name used to generate a relative path
*** @param type		The path type (Default: 0 - platform dependent, 1 - Unix, 2 - Dos/Windows)
**/
SWString
SWFilename::absolutePath(const SWString &path, const SWString &dir, PathType type)
		{
		SWString	result;

		if (isAbsolutePath(path)) result = path;
		else result = concatPaths(dir, path, type);

		return optimizePath(result, type);
		}


/**
*** Returns the relative path name for the given path
*** in relation to the specified directory (default .)
***
*** There are certain conditions when it is impossible to calculate a relative
*** path. These are described below:
***
*** When the path is absolute and the directory is relative it is impossible
*** to determine the relative path between them. Under these circumstances the
*** orginal path is returned.
***
*** When the path and the directory are on different physical drives (e.g. c: and d:)
*** there is no relative path between them. Under these circumstances the
*** orginal path is returned.
***
*** @param p		The path name (can be absolute)
*** @param d		The directory name used to generate an absolute path
*** @param type		The path type (Default: 0 - platform dependent, 1 - Unix, 2 - Dos/Windows)
**/
SWString
SWFilename::relativePath(const SWString &p, const SWString &d, PathType type)
		{
		// Check for impossible situations which require special handling
		if (isAbsolutePath(p) && isRelativePath(d)) return p;
		if (p.length() > 1 && d.length() > 1 && p[1] == ':' && d[1] == ':' && p[0] != d[0]) return p;

		SWString	dir=d;
		SWString	path=p;

		// Now path AND dir are both absolute
		SWStringArray	pa, da, ra;
		int					i, j, npa, nda;

		npa = (int)splitPath(path, pa);
		nda = (int)splitPath(dir, da);

		i = 0;
		while (i < npa && i < nda)
			{
			// Ignore case on dos paths
			if (pa[i].compareTo(da[i], type==PathTypeWindows?SWString::ignoreCase:SWString::exact) != 0) break;
			i++;
			}

		for (j=i;j<nda;j++)
			{
			if (!isAbsolutePath(da[j])) ra.add(_T(".."));
			}

		for (j=i;j<npa;j++) 
			{
			if (!isAbsolutePath(pa[j])) ra.add(pa[j]);
			}

		if (ra.size() == 0) return _T(".");

		return joinPath(ra, type);
		}



SWString
SWFilename::extension() const
		{
		return extension(m_path);
		}


// This function returns the extension of the given file name.
SWString
SWFilename::extension(const SWString &file)
		{
		SWString	extension;
		int		p;

		extension = basename(file);

		if ((p = extension.last(_T('.'))) >= 0) extension.remove(0, p+1);
		else extension.clear();

		return extension;
		}


void
SWFilename::split(SWString &base, SWString &extension) const
		{
		split(m_path, base, extension);
		}


void
SWFilename::split(const SWString &file, SWString &base, SWString &extension)
		{
		int	p;

		base = basename(file);
		extension.clear();

		if ((p = base.last(_T('.'))) >= 0)
			{
			extension = base;
			extension.remove(0, p+1);
			base.remove(p, (int)base.length()-p);
			}
		}


void
SWFilename::split(const SWString &file, SWString &directory, SWString &base, SWString &extension)
		{
		int	p;

		directory = dirname(file);
		base = basename(file);
		extension.clear();

		if ((p = base.last(_T('.'))) >= 0)
			{
			extension = base;
			extension.remove(0, p+1);
			base.remove(p, (int)base.length()-p);
			}
		}


SWString
SWFilename::concatPaths(const SWString &path, const SWString &path2, PathType type)
		{
		SWString	result=path;
		SWString	p2=path2;
		int			c;

		switch (type)
			{
			case PathTypeUnix:		c = _T('/');		break;
			case PathTypeWindows:	c = _T('\\');		break;
			default:				c = SW_PATH_SEPARATOR;	break;
			}
		
		// First strip off any trailing slashes
		result.strip('/', SWString::trailing);
		result.strip('\\', SWString::trailing);

		// Strip off slashes from start of second path
		p2.strip('/', SWString::leading);
		p2.strip('\\', SWString::leading);

		if (!p2.isEmpty())
			{
			// Next add the separator and the second path

			// can't use result to test here since a root path spec '/' will become empty!
			// if (!result.isEmpty()) result += c;
			if (path.length()) result += c;

			result += p2;
			}
		else
			{
			// Check we didn't zap a root spec (i.e. /)
			if (path.length() && result.length() == 0) result = c;
			}

		return result;
		}


SWString
SWFilename::dirname() const
		{
		return dirname(m_path);
		}


SWString
SWFilename::dirname(const SWString &path)
		{
		SWString	v(path);
		int			p, l=(int)v.length();

		// Remove all the trailing slashes (but not if it it the one and only char)
		while (l > 1 && (v[l-1] == _T('/') || v[l-1] == _T('\\')))
			{
			if (l == 3 && v[1] == _T(':'))
				{
				// This is a DOS path with a drive
				// i.e. X:\ so just return it
				return v;
				}

			v.remove(l--, 1);
			}

		if (v != _T("/") && v != _T("\\") && !(v.length() == 2 && v[1] == _T(':')))
			{
			if (v.isEmpty() || (p = v.lastOf(_T("/\\"))) < 0) v = _T(".");
			else
				{
				v.remove(p+1, (int)v.length()-p-1);
				l = (int)v.length();

				// Remove all the trailing slashes (but not if it it the one and only char)
				while (l > 1 && (v[l-1] == _T('/') || v[l-1] == _T('\\')))
					{
					if (l == 3 && v[1] == _T(':'))
						{
						// This is a DOS path with a drive
						// i.e. X:\ so just return it
						return v;
						}

					v.remove(--l, 1);
					}

				}
			}

		return v;
		}


SWString
SWFilename::basename() const
		{
		return basename(m_path);
		}


SWString
SWFilename::basename(const SWString &path, bool stripextension)
		{
		SWString	v(path);
		int			p;

		if (!v.isEmpty() 
			&& v != _T("/") 
			&&  v != _T("\\")
			&& !(v.length() == 3 && v[1] == _T(':') && (v[2] == _T('/') || v[2] == _T('\\')))
			)
			{
			if ((p = v.lastOf(_T("/\\"))) >= 0) v.remove(0, p+1);
			}

		if (stripextension)
			{
			int		p;

			if ((p = v.last(_T('.'))) >= 0)
				{
				v.remove(p, (int)v.length()-p);
				}
			}

		return v;
		}


SWString
SWFilename::drivename() const
		{
		return drivename(m_path);
		}


// This function returns the drivename of the given file name.
SWString
SWFilename::drivename(const SWString &filename)
		{
		SWString	v;
		SWString	path=filename;

		// Case 1: \\server\share\...
		if (path.length() > 2 && path[0] == _T('\\') && path[1] == _T('\\'))
			{
			SWString	tmp;

			tmp = sharename(path);
			if (tmp.length() == 2 && tmp[1] == _T('$')) v = tmp[0];
			}
		// Case 2: X:...
		else if (path.length() > 1 && path[1] == _T(':'))
			{
			v = path[0];
			}

		return v;
		}


SWString
SWFilename::sharename() const
		{
		return sharename(m_path);
		}


// This function returns the sharename of the given file name.
SWString
SWFilename::sharename(const SWString &filename)
		{
		SWString	v;
		SWString	path=filename;

		// Case 1: \\server\share\...
		if (path.length() > 2 && path[0] == _T('\\') && path[1] == _T('\\'))
			{
			SWString	tmp;
			int			p;

			tmp = path;
			tmp.remove(0, 2);

			// NOW server\share\....
			if ((p = tmp.first(_T('\\'))) >= 0)
				{
				// NOW share\....
				tmp.remove(0, p+1);

				if ((p = tmp.first(_T('\\'))) >= 0) tmp.remove(p, (int)tmp.length()-p);

				// NOW share
				v = tmp;
				}
			}

		return v;
		}



int
SWFilename::compareTo(const SWFilename &other) const
		{
		return comparePaths(this->path(), other.path(), (this->type() == PathTypeWindows || other.type() == PathTypeWindows));
		}


int
SWFilename::comparePaths(const SWString &path1, const SWString &path2, bool ignoreCase)
		{
		SWStringArray	parts1, parts2;
		int				r=0, idx=0;

		splitPath(path1, parts1);
		splitPath(path2, parts2);

		idx = 0;
		while (r == 0 && idx < (int)parts1.size() && idx < (int)parts2.size())
			{
			r = parts1[idx].compareTo(parts2[idx], ignoreCase?SWString::ignoreCase:SWString::exact);
			idx++;
			}

		if (r == 0)
			{
			if (parts1.size() > parts2.size()) r = 1;
			else if (parts1.size() < parts2.size()) r = -1;
			}

		return r;
		}

sw_status_t
SWFilename::writeToStream(SWOutputStream &stream) const
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		res = stream.write((sw_uint8_t)m_type);
		if (res == SW_STATUS_SUCCESS) res = stream.write(m_path);

		return res;
		}


sw_status_t
SWFilename::readFromStream(SWInputStream &stream)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		sw_uint8_t	type;
		SWString	path;

		res = stream.read(type);
		if (res == SW_STATUS_SUCCESS) res = stream.read(path);
		if (res == SW_STATUS_SUCCESS)
			{
			m_type = (PathType)type;
			m_path = path;
			}

		return res;
		}








