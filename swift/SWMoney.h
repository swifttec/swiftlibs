/**
*** @file		SWMoney.h
*** @brief		A class to represent a date
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWMoney class.
***
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWMoney_h__
#define __SWMoney_h__

#include <swift/swift.h>
#include <swift/SWObject.h>
#include <swift/SWException.h>
#include <swift/SWString.h>




// Forward declarations
class SWTime;

#define MF_NOCOMMA		0x01
#define MF_NOSIGNSPACE	0x02

/**
*** @brief	A generic date class based on Julian Day Numbers.
***
***	This is a generic date class that represents dates as Julian Day Numbers.
*** This gives a enormous range of dates that are stored in a 32-bit integer value.
***
*** The class stores date information only and has no time component. However it
*** can easily be converted both to and from a SWTime value. Where both date and
*** time information are required developers should use the SWTime class.
***
*** @author		Simon Sparkes
*/
class SWIFT_DLL_EXPORT SWMoney
		{
public:
		SWMoney(sw_money_t amount=0);
		SWMoney(int amount);
		SWMoney(const char *s);
		SWMoney(const wchar_t *s);
		SWMoney(const SWString &s);
		SWMoney(sw_money_t w, sw_money_t f);

		SWMoney &	operator=(const char *s);
		SWMoney &	operator=(const wchar_t *s);
		SWMoney &	operator=(const SWString &s);

		SWMoney &	operator=(sw_money_t amount)	{ m_amount = amount; return *this; }
		SWMoney &	operator=(int amount)			{ m_amount = (sw_money_t)amount; return *this; }

		/// Return the whole part of the money, e.g. pounds
		sw_money_t		whole() const;

		/// Return the fractional part of the money, e.g. pence
		sw_money_t		fraction() const;

		SWString		toString(int flags=0) const;

		operator sw_money_t() const			{ return m_amount; }
		operator sw_money_t &()				{ return m_amount; }

private:
		sw_money_t	m_amount;
		};





#endif
