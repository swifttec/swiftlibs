/**
*** @file		SWInfoObject.h
*** @brief		Operating System information object.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWInfoObject class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWInfoObject_h__
#define __SWInfoObject_h__


#include <swift/SWValueArray.h>



// Forward declarations

/**
*** @brief	A class to represent the information about a system/machine/host.
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWInfoObject : public SWObject
		{
public:
		/// Default constructor - constructs an empty value of type VtNone
		SWInfoObject();

		/// Virtual destrcutor
		virtual ~SWInfoObject();

		/// Copy constructor
		SWInfoObject(const SWInfoObject &other);

		/// Assignment operator
		SWInfoObject &	operator=(const SWInfoObject &other);

		/**
		*** @brief	Get the value specified by the index.
		***
		*** This method gets the value specified by the parameter idx
		*** and returns it to the caller. If the value at the specified
		*** index does not exist a value is created with the type SWValue::VtNone.
		***
		*** @param[in]	idx		The index of the value to fetch form the array.
		***
		*** @return				A reference to the specified value.
		***
		*** @throws				SWIndexOutOfBoundsException if the idx parameter is out of range.
		**/
		SWValue				getValue(int idx) const;

		/// Return the parameter specified by the index idx as a string.
		SWString			getString(int idx) const				{ return getValue(idx).toString(); }

		/// Return the parameter specified by the index idx as a bool.
		bool				getBoolean(int idx) const				{ return getValue(idx).toBoolean(); }

		/// Return the parameter specified by the index idx as an signed 32-bit integer.
		sw_int32_t			getInt32(int idx) const					{ return getValue(idx).toInt32(); }

		/// Return the parameter specified by the index idx as an unsigned 32-bit integer.
		sw_uint32_t			getUInt32(int idx) const				{ return getValue(idx).toUInt32(); }

		/**
		*** @brief	Clears all values from this info object.
		**/
		void				clear()									{ m_values.clear(); }

		/**
		*** @brief	Set the value at the specified index to that specified.
		**/
		void				setValue(int idx, const SWValue &v)		{ m_values[idx] = v; }

		/**
		*** @brief	Check to see if the value at the specified index has been defined
		**/
		//bool				defined(int idx) const					{ return m_values.defined(idx); }

protected:
		SWValueArray	m_values;	///< Various values
		};



#endif
