/**
*** @file		sw_assert.h
*** @brief		A collection of functions to support assertion handling
*** @version	1.0
*** @author		Simon Sparkes
***
*** A collection of functions to support assertion handling
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __sw_assert_h__
#define __sw_assert_h__

#include <swift/SWException.h>




#define SW_ASSERT_FLAG_THROW_EXCEPTION	0x80000000	///< Causes the sw_assert_report function to throw an exception
#define SW_ASSERT_FLAG_ABORT			0x40000000	///< Causes the sw_assert_report function to abort the program

/**
*** @internal
*** @brief	An internal function to report an assertion
***
*** This function is intended primarily for internal use only, but can be used
*** by developers for custom assertion reporting. This function, unlike the SW_ASSERT_XXXX
*** macros is available in both RELEASE and DEBUG modes. This means that assertions can be
*** thrown from RELEASE mode should there be a requirement to do so. It is recommended that
*** developers use the macros provided whenever possible.
***
*** @param[in]	expression	A text representation of the expression that failed
*** @param[in]	file		The filename where the assertion occurred (normally taken from __FILE__)
*** @param[in]	line		The line number in the file where the assertion occurred (normally taken from __LINE__)
*** @param[in]	flags		The flags that specify how the assertion is to be handled. If the flag
***							SW_ASSERT_FLAG_THROW_EXCEPTION is set then a SWAssertException is thrown.
***							If SW_ASSERT_FLAG_ABORT is set then the C function abort is called.
***							If no supported flags are set then assert will report the assertion and continue.
**/
SWIFT_DLL_EXPORT void		sw_assert_report(const char *expression, const char *file, int line, sw_uint32_t flags);


/// Generic assertion exception
SW_DEFINE_EXCEPTION(SWAssertException, SWException);



/**
*** @def	SW_ASSERT(expr)
***
*** @brief	Report an assertion and abort the program (Debug Only).
***
*** This macro only functions in DEBUG mode (SW_BUILD_DEBUG defined).
*** It will test the expression expr and if it evaluates to false it will
*** then call the sw_assert_report function with the SW_ASSERT_FLAG_ABORT flag set
*** which will report the problem and then abort the program.
***
*** In RELEASE mode (SW_BUILD_RELEASE defined) this macro does nothing.
***
*** @param[in]	expr	The condition to test which if evaluates to true will
***						cause the assertion.
**/

/**
*** @def	SW_ASSERT_EXCEPTION(expr)
***
*** @brief	Report an assertion and throw an exception (Debug Only).
***
*** This macro only functions in DEBUG mode (SW_BUILD_DEBUG defined).
*** It will test the expression expr and if it evaluates to false it will
*** then call the sw_assert_report function with the SW_ASSERT_FLAG_THROW_EXCEPTION flag set
*** which will report the problem and then throw a SWAssertException.
***
*** In RELEASE mode (SW_BUILD_RELEASE defined) this macro does nothing.
***
*** @param[in]	expr	The condition to test which if evaluates to true will
***						cause the assertion.
**/


#if defined(SW_BUILD_DEBUG)
	#define SW_ASSERT(expr)				if (!(expr)) sw_assert_report(#expr, __FILE__, __LINE__, SW_ASSERT_FLAG_ABORT)
	#define SW_ASSERT_EXCEPTION(expr)	if (!(expr)) sw_assert_report(#expr, __FILE__, __LINE__, SW_ASSERT_FLAG_THROW_EXCEPTION)
#else
	#define SW_ASSERT(expr)
	#define SW_ASSERT_EXCEPTION(expr)
#endif





#endif
