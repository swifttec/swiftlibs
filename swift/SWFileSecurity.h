/**
*** @file		SWFileSecurity.h
*** @brief		A class to represent the security descriptor of a file.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWFileSecurity class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




#ifndef __SWFileSecurity_h__
#define __SWFileSecurity_h__

#include <swift/swift.h>

#include <swift/SWObject.h>
#include <swift/SWString.h>





/**
*** @brief	Represent the security information for a file (owner, group, ACL)
***
*** This object provides portable security information about a file which
*** is used when creating a file via SWFile or when retrieving file information
*** via SWFileInfo.
***
*** @see		SWFileAttributes, SWFileInfo, SWFile
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWFileSecurity : public SWObject
		{
friend class SWFile;

public:
		/// Construct a file object ready to be opened.
		SWFileSecurity();

		/// Virtual destructor
		virtual ~SWFileSecurity();

protected: // Members
		/* Excected members include:
		SWUser		m_user;		///< The primary owner's user ID
		SWGroup		m_group;	///< The primary owner's group ID
		SWFileACL	m_acl;		///< An access control list
		*/
		};





#endif
