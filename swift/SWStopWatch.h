/**
*** @file		SWStopWatch.h
*** @brief		A thread pool object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWStopWatch class.
**/

/*********************************************************************************
Copyright (c) 2006-2016 SwiftTec Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWStopWatch_h__
#define __SWStopWatch_h__

#include <swift/SWVector.h>
#include <swift/SWTime.h>

SWIFT_NAMESPACE_BEGIN


#if defined(_MSC_VER)
	#pragma warning(disable: 4251) // class xxxx<>' needs to have dll-interface to be used by clients
#endif


/**
*** @brief	A class to handle the timing of events
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Safe when synchronised</td>
*** </tr>
*** </table>
***
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWStopWatch : public SWLockableObject
		{
public:
		/// Construct a stopwatch object.
		SWStopWatch();

		/// Copy constructor
		SWStopWatch(const SWStopWatch &other);

		/// Virtual destrcutor
		virtual ~SWStopWatch();

		/// Assignment operator
		SWStopWatch &	operator=(const SWStopWatch &other);

		/**
		*** @brief	Start the stopwatch.
		***
		*** This method starts the stopwatch if it is not running or
		*** it has been reset. It records the initial time and sets the
		*** internal runflag.
		***
		*** @return	true if the stopwatch could be started, false otherwise.
		**/
		bool	start();

		/**
		*** @brief	Stop the stopwatch.
		***
		*** This method stops a running stopwatch by clearing the internal
		*** runflag and recording the final time.
		***
		*** @return	true if the stopwatch could be stopped, false otherwise.
		**/
		bool	stop();

		/**
		*** @brief	Reset the stopwatch.
		***
		*** This method will reset the stopwatch so that it can be re-run.
		*** It will stop the stopwatch if it is currently running and remove
		*** all recorded times.
		***
		*** @return	true if the stopwatch could be reset, false otherwise.
		**/
		bool	reset();

		/**
		*** @brief	Add a checkpoint time.
		***
		*** This method only functions if the stopwatch is currently running.
		*** It adds a time record to the stopwatch.
		***
		*** @return	true if a checkpoint time could be added to the stopwatch,
		***			false otherwise.
		**/
		bool	checkpoint();

		/**
		*** @brief	Get the elasped time in seconds as a double
		***
		*** This method returns the elasped time of the stopwatch.
		*** The value returned depends on whether or not the stopwatch
		*** is currently running.
		***
		*** If the stopwatch is currently running this method returns the
		*** time elasped since the stopwatch was started. If the stopwatch
		*** has been stopped it represents the time between the start and 
		*** stop time.
		***
		*** If the stopwatch has not run this method will return a value of 0.0
		**/
		double	elapsed() const;

		/**
		*** @brief	Get the elasped time in seconds as a sw_timespec_t.
		***
		*** This method is the same as the elapsed() method except that
		*** it takes a sw_timespec_t reference to receive the time difference.
		***
		*** @param[out]	tv		A sw_timespec_t reference that receives the time
		***						difference.
		*** 
		*** @return		a boolean indicating if a valid time difference was returned.
		**/
		void	elapsed(sw_timespec_t &tv) const;

		/**
		*** @brief	Return the elasped time in seconds from the start time to the given checkpoint.
		***
		*** This method returns a double representing the time in seconds from
		*** the start time and the time at the specified checkpoint index.
		*** The start time is always at index zero. Checkpoint times start at index
		*** value one and the stop time is at the last position in the list.
		***
		*** If the index is out of range an SWIndexOutOfBoundsException will be thrown.
		***
		*** @param[in]	idx		The index of the checkpoint to measure the time from.
		*** @return				The elasped time in seconds.
		***
		*** @throws		SWIndexOutOfBoundsException
		**/
		double	elapsed(int idx) const;
		void	elapsed(int idx, sw_timespec_t &tv) const;

		/**
		*** @brief	Return the elasped time from the start time to the given checkpoint.
		***
		*** This method returns a double representing the time in seconds from
		*** the time at the specified start index and the time at the specified finish index.
		*** The start time is always at index zero. Checkpoint times start at index
		*** value one and the stop time is at the last position in the list.
		***
		*** If either index is out of range or the finish index is less than the start index
		*** an SWIndexOutOfBoundsException will be thrown.
		***
		*** @param[in]	start	The index of the checkpoint to measure the time from.
		*** @param[in]	finish	The index of the checkpoint to measure the time to.
		*** @return				The elasped time in seconds.
		***
		*** @throws		SWIndexOutOfBoundsException
		**/
		double	elapsed(int start, int finish) const;
		void	elapsed(int start, int finish, sw_timespec_t &tv) const;

		/**
		*** @brief	Return the number of time records currently stored in the stopwatch.
		**/
		int		count() const		{ return m_timelist.size(); }

		/**
		*** @brief	Return a boolean indicating if the stopwatch is currently running.
		**/
		bool	isRunning() const	{ return m_runflag; }

		/**
		*** @brief	Return the time recorded at a specific index.
		***
		*** This method returns the time recorded at the specified index.
		*** The start time is always at index zero. Checkpoint times start at index
		*** value one and the stop time is at the last position in the list.
		***
		*** If the index is out of range an SWIndexOutOfBoundsException will be thrown.
		***
		*** @param[in]	idx		The index of the time record to fetch.
		***
		*** @throws		SWIndexOutOfBoundsException
		**/
		SWTime	timeAt(int idx) const;

public: // Comparison operators

		/**
		*** @brief	Compare this object with another.
		***
		*** This method compares this object with the one specified and returns a
		*** result indicating if the other object is less than, equal to, or greater
		*** than this object.
		***
		*** - If this object is less than the specified object then a negative result is returned.
		*** - If this object is greater than the specified object then a positive result is returned.
		*** - If this object is equal to the specified object then a zero result is returned.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				The result of the comparision:
		***						-  0 - This object is equal to the other object.
		***						- <0 - This object is less than the other object.
		***						- >0 - This object is greater than the other object.
		***
		*** @note
		*** The compareTo method is not required to return -1 and +1 for the negative and positive
		*** values although this is typically the value returned. Callers should test for \< 0 and \> 0
		*** rather than for specific values of -1 and +1.
		**/
		int			compareTo(const SWStopWatch &other) const;

		/**
		*** @brief	Test for equality
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is equal to the other object, false otherwise.
		**/
		bool	operator==(const SWStopWatch &other) const	{ return (compareTo(other) == 0); }

		/**
		*** @brief	Test for inequality
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is not equal to the other object, false otherwise.
		**/
		bool	operator!=(const SWStopWatch &other) const	{ return (compareTo(other) != 0); }

		/**
		*** @brief	Test for less than.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is less than the other object, false otherwise.
		**/
		bool	operator< (const SWStopWatch &other) const	{ return (compareTo(other) <  0); }

		/**
		*** @brief	Test for greater than.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is greater than the other object, false otherwise.
		**/
		bool	operator> (const SWStopWatch &other) const	{ return (compareTo(other) >  0); }

		/**
		*** @brief	Test for less than or equal to.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is less than or equal to the other object, false otherwise.
		**/
		bool	operator<=(const SWStopWatch &other) const	{ return (compareTo(other) <= 0); }

		/**
		*** @brief	Test for greater than or equal to.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is greater than or equal to the other object, false otherwise.
		**/
		bool	operator>=(const SWStopWatch &other) const	{ return (compareTo(other) >= 0); }



public: // virtual overrides

		/// Override for SWObject writeToStream
		virtual sw_status_t		writeToStream(SWOutputStream &stream) const;

		/// Override for SWObject readFromStream
		virtual sw_status_t		readFromStream(SWInputStream &stream);


protected:

		/// Calculate the difference between the 2 times
		void	diff(const SWTime &a, const SWTime &b, sw_timespec_t &tv) const;

protected:
		SWVector<SWTime>	m_timelist;		///< A vector of time objects.
		bool				m_runflag;		///< A flag indicatiing if the stopwatch is currently running.
		};


/**
*** @example stopwatch.cpp
***
*** An example of using the SWStopWatch class
**/


#if defined(_MSC_VER)
	#pragma warning(default: 4251) // class xxxx<>' needs to have dll-interface to be used by clients
#endif


SWIFT_NAMESPACE_END

#endif
