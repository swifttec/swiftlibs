/**
*** @file		SWThreadPool.cpp
*** @brief		A generic value object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWThreadPool class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWThreadPool.h>
#include <swift/SWString.h>
#include <swift/SWGuard.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif


#define SW_TPFLAG_ADDJOB		0x00000001		///< Jobs can be added
#define SW_TPFLAG_REMOVEJOB		0x00000002		///< Jobs can be removed
#define SW_TPFLAG_RUNJOB		0x00000004		///< Jobs can be run
#define SW_TPFLAG_ADJTHRCOUNT	0x40000000		///< Adjusting thread count
#define SW_TPFLAG_SHUTDOWN		0x80000000		///< Shutting down.


SWThreadPoolJob::SWThreadPoolJob(SWJob *pJob, sw_uint32_t jobpriority, int threadpriority) :
	m_pJob(pJob),
	m_jobPriority(jobpriority),
	m_thrPriority(threadpriority)
		{
		}



SWThreadPoolJob::SWThreadPoolJob(const SWThreadPoolJob &other) :
	SWObject()
		{
		*this = other;
		}


SWThreadPoolJob::~SWThreadPoolJob()
		{
		}


SWThreadPoolJob &
SWThreadPoolJob::operator=(const SWThreadPoolJob &other)
		{
		m_jobPriority = other.m_jobPriority;
		m_thrPriority = other.m_thrPriority;
		m_pJob = other.m_pJob;

		return *this;
		}


bool
SWThreadPoolJob::operator==(const SWThreadPoolJob &other) const
		{
		return (m_pJob == other.m_pJob);
		}


bool
SWThreadPoolJob::operator<(const SWThreadPoolJob &other) const
		{
		return (m_jobPriority < other.m_jobPriority);
		}









SWThreadPoolThread::SWThreadPoolThread(SWThreadPool *pOwner) :
	SWThread(0),
	m_pOwner(pOwner),
	m_active(false)
		{
		}


SWThreadPoolThread::~SWThreadPoolThread()
		{
		}


int
SWThreadPoolThread::run()
		{
		while (runnable())
			{
			// Wait on the event (will block until we are released)
			m_pOwner->m_event.wait();

			// Now grab the owner's lock so we can access it's internal structures
			SWGuard	guard(m_pOwner);

			if (m_pOwner->m_joblist.size() != 0)
				{
				// Mark this thread as active
				m_active = true;
				m_pOwner->m_idleThreads--;
				m_pOwner->m_activeThreads++;

				// Grab the first job from the queue
				SWThreadPoolJob	job;

				m_pOwner->m_joblist.removeHead(&job);

				// Save thread characteristics in case the job changes them
				int		   savedpriority;

				savedpriority = getPriority();

				// Restore the thread characteristics
				setPriority(job.m_thrPriority);

				// Run the job - releasing the owner mutex for the duration.
				m_pOwner->unlock();
				if (job.m_pJob != NULL)
					{
					m_pOwner->notify(SWThreadPool::JobStarted, job.m_pJob);
					job.m_pJob->execute();
					m_pOwner->notify(SWThreadPool::JobStopped, job.m_pJob);
					}
				m_pOwner->lock();

				// Restore the thread characteristics
				setPriority(savedpriority);

				m_active = false;
				m_pOwner->m_idleThreads++;
				m_pOwner->m_activeThreads--;

				if (job.m_pJob != NULL && job.m_pJob->getDeleteFlag())
					{
					// We should delete this job
					delete job.m_pJob;
					job.m_pJob = NULL;
					}

				// Check to see if all jobs are complete
				if (m_pOwner->m_joblist.size() == 0 && m_pOwner->m_activeThreads == 0)
					{
					m_pOwner->unlock();
					m_pOwner->notify(SWThreadPool::AllJobsComplete, NULL);
					m_pOwner->lock();
					}
				}
			else if ((m_pOwner->m_flags & SW_TPFLAG_SHUTDOWN) != 0)
				{
				// The pool is shutting down and there is nothing to do, so exit.
				break;
				}

			if (m_pOwner->m_closeThreads != 0)
				{
				// The pool needs to close some threads but is not shutting down
				m_pOwner->m_closeThreads--;
				m_pOwner->m_idleThreads--;
				break;
				}

			if (m_pOwner->m_joblist.size() == 0 && (m_pOwner->m_flags & SW_TPFLAG_SHUTDOWN) == 0)
				{
				// Reset the event so that we will block
				m_pOwner->m_event.reset();
				}
			}

		return 0;
		}









SWThreadPool::SWThreadPool(sw_uint32_t maxthreads) :
	m_event(SWEvent::ManualReset),
	m_maxThreads(maxthreads),
	m_flags(0),
	m_activeThreads(0),
	m_idleThreads(0),
	m_closeThreads(0),
	m_notify(NULL)
		{
		// Make this a synchronised object
		synchronize();

		m_flags = SW_TPFLAG_ADDJOB | SW_TPFLAG_REMOVEJOB | SW_TPFLAG_RUNJOB;
		}


SWThreadPool::~SWThreadPool()
		{
		shutdown();
		}


sw_status_t
SWThreadPool::add(SWJob *pJob, bool delflag, sw_uint32_t jobpriority, int threadpriority, SWJob::NotificationProc notifyfunc)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if ((m_flags & SW_TPFLAG_ADDJOB) != 0)
			{
			SWGuard	guard(this);

			// Set the job parameters
			pJob->setDeleteFlag(delflag);
			if (notifyfunc != NULL) pJob->setNotificationFunction(notifyfunc);

			// add the job to the internal queue.
			m_joblist.add(SWThreadPoolJob(pJob, jobpriority, threadpriority));
			notify(JobAdded, pJob);


			// set the event to indicate that there are waiting jobs
			if ((m_flags & SW_TPFLAG_RUNJOB) != 0)
				{
				m_event.set();
				}

			// If we have not reached the thread limit AND there are no idle threads
			// then we start another thread 
			if ((sw_uint32_t)m_threadlist.size() < getMaximumThreadCount() && getIdleThreadCount() < (sw_uint32_t)m_joblist.size())
				{
				// Create a thread and add it to the list
				SWThreadPoolThread	*pThread;

				pThread = new SWThreadPoolThread(this);
				pThread->start();
				m_threadlist.addTail(pThread);
				m_idleThreads++;
				}
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}

		return res;
		}


sw_status_t
SWThreadPool::remove(SWJob *pJob)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if ((m_flags & SW_TPFLAG_REMOVEJOB) != 0)
			{
			SWGuard			guard(this);
			SWThreadPoolJob	search(SWThreadPoolJob(pJob, 0, 0));

			if (m_joblist.contains(search))
				{
				if (!pJob->isRunning())
					{
					m_joblist.remove(search);
					if (m_joblist.size() == 0)
						{
						// There are no jobs left, reset the event
						m_event.reset();
						}
					}
				else
					{
					res = SW_STATUS_ILLEGAL_OPERATION;
					}
				}
			else
				{
				res = SW_STATUS_NOT_FOUND;
				}
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}


		return res;
		}


sw_uint32_t
SWThreadPool::getMaximumThreadCount() const
		{
		SWGuard	guard(this);

		return m_maxThreads;
		}



void
SWThreadPool::setMaximumThreadCount(sw_uint32_t maxthreads)
		{
		if (maxthreads != 0 && (m_flags & SW_TPFLAG_ADJTHRCOUNT) == 0)
			{
			SWGuard	guard(this);

			m_flags |= SW_TPFLAG_ADJTHRCOUNT;

			if (maxthreads > m_maxThreads)
				{
				m_maxThreads = maxthreads;

				// We need to add one thread for each pending job in the queue
				// until there are no more jobs OR we reach the new thread limit.

				while ((sw_uint32_t)m_threadlist.size() < getMaximumThreadCount() && getIdleThreadCount() < (sw_uint32_t)m_joblist.size())
					{
					// Create a thread and add it to the list
					SWThreadPoolThread	*pThread;

					pThread = new SWThreadPoolThread(this);
					pThread->start();
					m_threadlist.addTail(pThread);
					m_idleThreads++;
					}
				}
			else
				{
				// We need to close the specified number of threads
				sw_uint32_t	count;

				// we set m_closeThreads to the number of threads that need to 
				// be closed. Each thread will check the m_closeThreads value
				// and exit if m_closeThreads > 0.
				count = m_closeThreads = m_maxThreads - maxthreads;
				m_maxThreads = maxthreads;
				m_event.set();

				while ((sw_uint32_t)m_threadlist.size() > getMaximumThreadCount())
					{
					unlock();
					SWThread::sleep(100);
					lock();

					if (m_closeThreads != count)
						{
						// Scan the thread list and look for stopped threads.
						SWIterator<SWThread *>	it=m_threadlist.iterator();
						SWThread				**ppThread;

						while (count && it.hasNext())
							{
							ppThread = it.next();
							if ((*ppThread)->stopped())
								{
								it.remove();
								delete *ppThread;
								count--;
								}
							}
						}
					}
				}

			m_flags &= ~SW_TPFLAG_ADJTHRCOUNT;
			}
		}


sw_status_t
SWThreadPool::enable(Operation op, bool opval)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if ((m_flags & SW_TPFLAG_SHUTDOWN) == 0)
			{
			sw_uint32_t	flags=0;

			switch (op)
				{
				case AddJob:	flags = SW_TPFLAG_ADDJOB;		break;
				case RemoveJob:	flags = SW_TPFLAG_REMOVEJOB;	break;
				case RunJob:	flags = SW_TPFLAG_RUNJOB;		break;
				}

			SWGuard	guard(this);

			if (opval)
				{
				m_flags |= flags;

				if (op == RunJob && m_joblist.size() > 0)
					{
					// set the event to ensure that threads wake and execute jobs
					m_event.set();
					}
				}
			else
				{
				m_flags &= ~flags;

				if (op == RunJob)
					{
					// reset the event to ensure that threads do not wake
					// and execute jobs
					m_event.reset();
					}
				}

			res = (flags?SW_STATUS_SUCCESS:SW_STATUS_INVALID_PARAMETER);
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}

		return res;
		}


void
SWThreadPool::waitUntilAllJobsComplete()
		{
		while (m_joblist.size() > 0 || m_activeThreads > 0)
			{
			SWThread::sleep(100);
			}
		}


void
SWThreadPool::shutdown()
		{
		// Set the shutdown flag and prevent further jobs from being added.
		m_flags |= SW_TPFLAG_SHUTDOWN;
		m_flags &= ~SW_TPFLAG_ADDJOB;

		waitUntilAllJobsComplete();

		while (m_threadlist.size() > 0)
			{
			SWThread	*pThread=NULL;

			// Set the event to wake up the threads
			m_event.set();
			m_threadlist.removeHead(&pThread);
			delete pThread;
			}
		}


void
SWThreadPool::notify(NotificationCode code, SWJob *pJob)
		{
		if (m_notify != NULL)
			{
			m_notify(pJob, code);
			}
		}


void
SWThreadPool::setNotificationFunction(NotificationProc notify)
		{
		m_notify = notify;
		}

