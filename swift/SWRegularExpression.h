/****************************************************************************
**	SWRegularExpression.h	A class to handle regular expressions
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWRegularExpression_h__
#define __SWRegularExpression_h__



#include <swift/SWObject.h>
#include <swift/SWString.h>
#include <swift/SWVector.h>



#if defined(SW_PLATFORM_WINDOWS)
	 // class xxxx<>' needs to have dll-interface to be used by clients
#endif

// Forward declarations
class SWRegExNode;

/**
*** A class to handle regular expressions
***
*** The regular expression are like perl regular expressions.
***	
***         \   Quote the next metacharacter
***         ^   Match the beginning of the line
***         .   Match any character (except newline)
***         $   Match the end of the line (or before newline at the end)
***         |   Alternation
***         ()  Grouping
***         []  Character class
**/
class SWIFT_DLL_EXPORT SWRegularExpression : public SWObject
		{
public:
		/// Default constructor
		SWRegularExpression(const SWString &re=_T(""), bool greedy=true, bool caseSensitive=true);

		/// Destructor
		~SWRegularExpression();

		// Change the RE string
		const SWRegularExpression &	operator=(const SWString &re);

		// Test to see if this RE matches the given string
		bool			matches(const SWString &str);

		/// Return the number of matched bracketed strings from the last call to matches
		int				matchCount() const		{ return (int)_matchVec.size(); }
		
		/// Returns the nth string in the match array
		SWString		lastMatchString(int n);

		/// Return the greedy status
		bool			isGreedy() const		{ return _greedy; }

		/// Set the greedy flag
		void			setGreedyMatching(bool v)	{ _greedy = v; }

		/// Return the case sensitivity flag
		bool			isCaseSensitive() const		{ return _caseSensitive; }

		/// Set the case sensitive flag
		void			setCaseSensitive(bool v)	{ _caseSensitive = v; }

		// Debugging aid
		virtual void		dump();

private:
		// Internal function to compile the regular expression
		void			compile(const sw_tchar_t *re);

		// Clears the internal structure
		void			clear();

		// Adds a node to the compiled tree
		void			addNode(SWRegExNode *);

		void			getQuantifier(const sw_tchar_t *pp, int &min, int &max, int &skip);
		sw_tchar_t		metachar(const sw_tchar_t *pp, int &skip);

private:
		SWRegExNode				*_rootNode;	// The root node
		SWRegExNode				*_currNode;	// The current node
		SWVector<SWRegExNode *>	_matchVec;	// A vector of capture nodes
		bool					_greedy;	// Greedy matching flag (if true each expressions matches as many as possible)
		bool					_caseSensitive;	// Case sensitive flag
		};




#endif
