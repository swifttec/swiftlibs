/****************************************************************************
**	SWIniFile.h	Class for handling an ini file (like the windows .ini files)
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWIniFile_h__
#define __SWIniFile_h__


#include <swift/SWConfigFile.h>

#include <swift/SWTextFile.h>

/**
*** @brief	Class for handling an ini file (like the windows .ini files)
*** @author	Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWIniFile : public SWConfigFile
		{
public:
		/// Constructor
		SWIniFile(const SWString &filename="", bool readonly=false, bool writeOnChange=false);

		/// Destructor
		virtual ~SWIniFile();

		/**
		*** @param	filename	The name of the file to load
		***
		*** This method loads the specified text file (normally a .ini file) creates
		*** config structures from the ini file.
		**/
		virtual bool		readFromFile(const SWString &filename);

		/// Saves the internal structures to the file
		virtual bool		writeToFile(const SWString &filename);

		/// Saves the internal structures to the file
		virtual bool		writeToFile(const SWString &filename, int textFileEncoding);

		/// Loads the internal structures from the file
		virtual bool		loadFromString(const SWString &text);

		/// Loads the internal structures from the file
		virtual SWString	saveToString();

private: // Methods

		// Writes a key section to the open file
		void			writeKeyToFile(const SWString &root, SWConfigKey *key, SWTextFile &f);

		// Writes a key section to the open file
		SWString		writeKeyToString(const SWString &root, SWConfigKey *key);

		// Encode a string for writing to a file
		SWString		encode(const SWString &value);

		// Decode a string read from a file
		SWString		decode(const SWString &value);
		};

#endif
