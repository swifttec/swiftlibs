/**
*** @file		SWVector.h
*** @brief		Templated vector class.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWVector class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWVector_h__
#define __SWVector_h__


#include <swift/SWGuard.h>
#include <swift/SWIterator.h>
#include <swift/SWLocalMemoryManager.h>




/**
*** @internal
*** @brief	An internal object used to encapsulate a vector element and the templated data type.
**/
template<class T>
class SWVectorElement
		{
public:
		/// Default Constructor
		SWVectorElement()											{ }

		/// Data copy constructor
		SWVectorElement(const T &data) : m_data(data)				{ }

		/// Custom new operator
		void *			operator new(size_t nbytes, sw_size_t &res)	{ res = nbytes; return malloc(nbytes); }

		/// Custom new operator
		void *			operator new(size_t /*nbytes*/, void *p)	{ return p; }

		/// Custom delete operator
		void			operator delete(void *, sw_size_t &)		{ }

		/// Custom delete operator
		void			operator delete(void *, void *)				{ }

		/// Custom delete operator
		void			operator delete(void *)						{ }

		/// Get data reference
		T &				data()										{ return m_data; }

private:
		T	m_data;		///< The actual data
		};


/**
*** @brief	SWVector is a templated version of the SWVector class.
***
*** @see		SWVector, SWCollection
*** @author		Simon Sparkes
**/
template<class T>
class SWVector : public SWCollection
		{
public:
		/**
		*** @brief	Construct a SWVector object with the specified number of elements.
		**/
		SWVector();


		/**
		*** @brief	Virtual destructor
		**/
		virtual ~SWVector();


		/**
		*** @brief Copy constructor
		***
		*** The copy constructor creates an empty array and the calls the
		*** assignment operator to copy all of the data.
		***
		*** @param[in]	other	Th array to copy.
		**/
		SWVector(const SWVector<T> &other);


		/**
		*** @brief Assignment operator
		***
		*** The assignment operator will clear the existing data from
		*** the array using the clear method and copy all of the values
		*** from the array specified.
		***
		*** @param[in]	other	Th array to copy.
		**/
		SWVector<T> &		operator=(const SWVector<T> &other);

		/**
		*** @brief	Clear the vector of all elements.
		***
		*** This method clears the vector of all elements. The capacity of the vector remains
		*** unchanged.
		**/
		virtual void		clear();


		/**
		*** @brief	Returns the element at the specified position in this list.
		***
		*** This method eturns the element at the specified position in this list.
		*** If the index is not within the bounds of the vector an exception
		*** is thrown.
		***
		*** @param[in]	pos		The position/index of the element to fetched.
		***
		*** @return				A reference to the element at the specified position.
		***
		*** @throws	SWIndexOutOfBoundsException
		**/
		T &					get(int pos) const;


		/**
		*** @brief	Appends a copy of the specified element to the end of this list.
		***
		*** This method appends a copy of the specified element to the end of this list.
		*** The copy of the specified element is made using the copy constructor
		*** of the element.
		***
		*** @param[in]	data	The data element to copy and add.
		**/
		void				add(const T &data);


		/**
		*** @brief	Inserts a copy of the specified element at the specified position in this vector.
		***
		*** This method inserts a copy of the specified element at the specified position in this vector.
		*** The copy of the specified element is made using the copy constructor
		*** of the element.
		***
		*** @param[in]	data	The data element to copy and add.
		*** @param[in]	pos		The position at which to insert the data element.
		**/
		void				insert(const T &data, int pos);


		/**
		*** @brief	Replaces the contents of the element at the specified position
		***
		*** This method replaces the contents of the element at the specified position
		*** in this vector with the specified element. The caller can optionally receive
		*** a copy of the data replaced by supplying the pOldData parameter.
		***
		*** The position specified by the pos parameter must be within the bounds of
		*** the vector otherwise an exception will be thrown.
		***
		*** @param[in]	data		The data to set at the specified position.
		*** @param[in]	pos			The position at which to set the data.
		*** @param[out]	pOldData	A pointer which if not NULL receives a copy of the data
		***							being replaced.
		***
		*** @throws		SWIndexOutOfBoundsException
		**/
		void				set(const T &data, int pos, T *pOldData=NULL);


		/**
		*** @brief	Removes the element at the specified position.
		***
		*** This method removes the element at the specified position.
		*** The position specified by the pos parameter must be within the bounds of
		*** the vector otherwise an exception will be thrown.  The caller can optionally receive
		*** a copy of the data replaced by supplying the pOldData parameter.
		***
		*** @param[in]	pos			The position at which to remove the element.
		*** @param[out]	pOldData	A pointer which if not NULL receives a copy of the data
		***							being replaced.
		***
		*** @throws		SWIndexOutOfBoundsException
		**/
		void				removeAt(int pos, T *pOldData=NULL);


		/**
		*** @brief	Removes the number of specified elements at the specified position.
		***
		*** This method removes the number of specified elements beginning at the
		*** specified position.
		*** The position specified by the pos parameter must be within the bounds of
		*** the vector otherwise an exception will be thrown. However if the value of
		*** pos + count is greater than returned by size only size()-pos elements
		*** will be removed and no execption will be thrown.
		***
		*** @param[in]	pos		The position at which to remove elements.
		*** @param[in]	count	The number of elements to remove (default=1).
		***
		*** @throws				SWIndexOutOfBoundsException
		**/
		void				removeAt(int pos, int count);


		/**
		*** @brief	Returns the index of the first occurence of the specified data.
		***
		*** This method returns the index of the first occurence of the specified data
		*** or -1 if not found. The search begins at the start of the vector unless the pos
		*** parameter specifies otherwise.
		***
		*** <i>Equality of the data is established by using the == operator on the data</i>.
		***
		*** @param[in]	data	The data value to search for using the == operator.
		*** @param[in]	pos		The position in the vector at which to start searching.
		***
		*** @return				The index of the data pointer if found or -1 if not found.
		**/
		int					indexOf(const T &data, int pos=0) const;


		/**
		*** @brief	Returns the index of the last occurence of the specified data.
		***
		*** This method returns the index of the last occurence of the specified data or -1 if not found.
		***
		*** <i>Equality of the data is established by using the == operator on the data</i>.
		***
		*** @param[in]	data	The data pointer to search for using the == operator.
		***
		*** @return				The index of the data pointer if found or -1 if not found.
		**/
		int					lastIndexOf(const T &data) const;


		/**
		*** @brief	Returns the index of the last occurence of the specified data starting from the specified position.
		***
		*** This method searches for the last occurence of the specified data starting at
		*** the specified postiion. The method returns the index of the element found 
		*** or -1 if not found.
		***
		*** <i>Equality of the data is established by using the == operator on the data</i>.
		***
		*** @param[in]	data	The data pointer to search for using the == operator.
		*** @param[in]	pos		The position in the vector at which to start searching.
		***
		*** @return				The index of the data pointer if found or -1 if not found.
		**/
		int					lastIndexOf(const T &data, int pos) const;


		/**
		*** @brief	Removes the first occurrence of the specified data from the collection.
		***
		*** This method searches the collection for the first occurence of specified data
		*** and if found the element is removed from the collection. If found and removed
		*** this method returns true, otherwise the collection remains unchanged and this method
		*** returns false.
		***
		*** <i>Equality of the data is established by using the == operator on the data</i>.
		***
		*** @param[in]	data	The data pointer to search for using the == operator.
		***
		*** @return			true if a matching element is found and removed,
		***					false otherwise.
		**/
		bool				remove(const T &data);


		/// Return true if the vector contains the given data value.
		bool				contains(const T &data) const;


		/**
		*** @brief	A useful cast for a quick get!
		***
		*** @param[in]	idx	The index of the element to get.
		***
		*** @see	get()
		**/
		T &					operator[](int idx) const;


		/**
		*** @brief	Set the size of the vector.
		***
		*** If the specified size is greater than the current size of the vector
		*** the vector is expanded adding NULL pointers as required.
		*** If the specified size is less than the current size of the vector
		*** the extra elements are discarded.
		***
		*** @param[in]	newsize		Specifies the the number of elements the vector is to contain.
		**/
		void				setSize(int newsize);


		/**
		*** @brief	Returns an iterator over the elements of this collection.
		***
		*** @param[in]	iterateFromEnd	If false (default) the iterator starts
		***							at the beginning of the vector. If true
		***							the iterator starts at the end of the
		***							vector.
		**/
		SWIterator<T>		iterator(bool iterateFromEnd=false) const;


		/**
		*** @brief	Returns an iterator over the elements of this collection.
		***
		*** @param[in]	startpos	Specifies the index at which to start
		***							the iteration.
		**/
		SWIterator<T>		iterator(int startpos) const;


		/**
		*** @brief	Sort the data in the vector.
		***
		*** This method uses a sort algorithm to sort the elements in the vector.
		*** The caller is required supply a comparision function to specify a
		*** sort order.
		***
		*** @param[in]	pCompareFunc	A custom compare function.
		**/
		void				sort(sw_collection_compareDataFunction *pCompareFunc);


		/**
		*** @brief Ensure the vector can contain at least the specified number of elements.
		***
		*** This method ensures the vector can contain at least the specified number of elements.
		*** without changing the size of the vector (number of stored elements). The method
		*** acquires the mutex (if synchronised) and then calls the ensureCapacityNoLock method.
		***
		*** @param[in]	n	The desired capacity counted in elements, not bytes.
		**/
		void				ensureCapacity(int n);

protected:
		/**
		*** @brief		Internal function for comparing two elements
		*** @param[in]	pData1	A pointer to the first data element to compare
		*** @param[in]	pData2	A pointer to the second data element to compare
		*** @return		-ve  if pData1 < pData2
		*** 			+ve  if pData1 > pData2
		*** 			zero if pData1 == pData2
		**/
		virtual int			compareData(const void *pData1, const void *pData2) const;

		/**
		*** @brief		Returns an iterator for this collection starting from the specified position.
		*** @param[in]	iterateFromEnd	If false the iterator is initially placed at the beginning of
		***								vector. If false 
		**/
		virtual SWCollectionIterator *		getIterator(bool iterateFromEnd=false) const;

		/**
		*** @brief		Returns an iterator for this collection starting from the specified position.
		*** @param[in]	startpos	The index at which to initially position the iterator.
		**/
		virtual SWCollectionIterator *		getIterator(int startpos) const;

		/**
		*** @brief				Set the internal element count to that specified.
		*** @param[in]	size	The number of elements.
		**/
		void								setElementCount(int size);

		/**
		*** @brief Ensure the vector can hold enough elements without taking a lock.
		***
		*** This method ensures the vector can hold the specified number of elements
		*** in it's internal storage. This method assumes that it has exclusive access
		*** to the internal members and does not re-acquire any object mutex to aid
		*** performance.
		***
		*** If necessary the internal storage is resized and may be be relocated.
		***
		*** @param[in]	n	The desired capacity counted in elements, not bytes.
		**/
		void								ensureCapacityNoLock(int n);

private:
		/**
		*** @internal
		*** @brief	Internal method for sorting the vector using the quick-sort algorithm.
		***
		*** @param[in]	pCompareFunc	The data compare function
		*** @param[in]	left			The left pivot point
		*** @param[in]	right			The right pivot point
		***/
		void			quickSort(sw_collection_compareDataFunction *pCompareFunc, int left, int right);

private:
		SWMemoryManager	*m_pMemoryManager;		///< The memory manager
		char			*m_data;				///< The data buffer
		sw_size_t		m_capacity;				///< The capacity of the buffer (bytes)
		sw_size_t		m_elemsize;				///< The size of each element
		};




// Include the implementation
#include <swift/SWVector.cpp>

#endif
