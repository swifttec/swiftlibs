/**
*** @file		SWString.h
*** @brief		A comprehensive string class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWString class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWString_h__
#define __SWString_h__

#include <swift/swift.h>
#include <swift/SWBuffer.h>
#include <swift/SWObject.h>





#define SW_STRING_CHARSIZE_MASK		0x0007 ///< the buffer holds a char string
#define SW_STRING_CHARSIZE_FIXED	0x0080 ///< the charsize is fixed and any incoming data must be converted.
#define SW_STRING_CONVERSION_VALID	0x0100 ///< the conversion buffer is valid
#define SW_STRING_LOCKED			0x8000 ///< the buffer is locked for external modification.


// Forward declarations
class SWValue;
class SWTextFile;


/**
*** @brief	Comprehensive string class
***
*** @author		Simon Sparkes
***/
class SWIFT_DLL_EXPORT SWString : public SWObject
		{
public:
		/// Used for specifying case comparision operations
		enum caseCompare
			{
			exact,		///< String comparision is exact (i.e. no allowance for case)
			ignoreCase	///< String comparison ignores the case of the characters
			};

		/// Used for string stripping operations
		enum stripType
			{
			leading,	///< Characters are stripped from the beginning of the string
			trailing,	///< Characters are stripped from the end of the string
			both		///< Characters are stripped from both the beginning and the end of the string
			};

		/// Used for include/exclude for span, count, etc
		enum incType
			{
			including,
			excluding
			};

		/// Used for env var expansion
		enum expandType
			{
			unixVarTypes=0x0001,
			windowsVarTypes=0x0002,
			allVarTypes=0xffff
			};


public: // Constructors and destructors

		/// Default constructor - a blank string
		SWString();

		/// Constructor - copy of the given char string
		SWString(const char *s);

		/// Constructor - copy of the given wide-character string
		SWString(const wchar_t *s);

		/// Constructor - copy of the given char string
		SWString(const char *s, sw_size_t n);

		/// Constructor - copy of the given wide-character string
		SWString(const wchar_t *s, sw_size_t n);

		/// Copy constructor
		SWString(const SWString &s);

		/**
		*** Constructs a string containing the specified character c repeated the specfied number of times (default=1).
		***
		*** This constructor uses an int for the character for simplicity. By default the character size for the string
		*** is calculated from the value of the character using the smallest size that the character will fit into.
		*** This can be overridden by specifying a charsize of 1, 2 or 4.
		***
		*** @param	c				The character to assign to the string.
		*** @param	n				The number of copies of the character to assign (default=1).
		*** @param	charsize		The size of the characters (in bytes) to use for the string.
		***							Acceptable values are:
		***							- 0 - Size will be calculated by constructor based on c.
		***							- 1 - Single-byte character string
		***							- 2 - Double-byte character string
		***							- 4 - Quad-byte character string
		***							any other values will be ignore and will result in the default behaviour.
		*** @param fixedCharSize	If true the charsize calculated or specifies if fixed and will not change
		***							if a string of another width is assigned.
		**/
		SWString(int c, sw_size_t n=1, sw_size_t charsize=0, bool fixedCharSize=false);

		/// Virtual destructor
		virtual ~SWString();


protected:
		// Special internal constructor
		SWString(void *pData, sw_size_t slen, sw_size_t cs);

public: // Miscellaneous

		/// Clear (empty) the string and return a reference to self.
		virtual void		clear();

		/// Clear (empty) the string and return a reference to self.
		SWString &			empty()					{ clear(); return *this; }

		/// Return the string length
		sw_size_t			length() const			{ return m_length; }

		/// Return the current char size (can be zero if unassigned).
		sw_size_t			charSize() const		{ return (m_flags & SW_STRING_CHARSIZE_MASK); }

		/// Returns true if this is a zero lengthed string (i.e., the null string).
		bool				isLocked() const		{ return ((m_flags & SW_STRING_LOCKED) != 0); }	

		/// Returns true if this is a zero lengthed string (i.e., the null string).
		bool				isEmpty() const			{ return (m_length == 0); }	

		/// Returns true if self is a fixed char type
		bool				isFixedCharSize() const	{ return ((m_flags & SW_STRING_CHARSIZE_FIXED) != 0); };

		/// Ensure the string can hold at least nchars of the current char size
		void				capacity(sw_size_t nchars, bool freeExtraMem=false);

		/// Return the current capacity in characters
		sw_size_t			capacity() const;

		/// Cast to a char *
		const char *		c_str() const;

		/// Cast to a wchar_t *
		const wchar_t *		w_str() const;

		const sw_tchar_t *	t_str() const;

		/// Get a hashcode for this string
		sw_uint32_t			hash() const;

		/// Get a hashcode for this string
		sw_uint32_t			hash(caseCompare cmp) const;

		// Extra (non-standard but useful) functions

		/**
		*** Expands environment variables in the string.
		***
		*** Environment variables of the form $VAR or ${VAR} are replaced with the
		*** value found in the runtime environment (using getenv).
		***
		*** The environment variable $$ is expanded to the value of the current process id.
		*** as is common to several unix shells.
		**/
		void			expandEnvVars(int flags=allVarTypes);

		/**
		*** Assign to the string by reading a single line from the given file.
		***
		*** Reads a line from the file f and assigns it to the string and optionally
		*** strips newline and carriage return characters from the end of the string.
		***
		*** @param	f		The file stream to read from.
		*** @param	stripNL	If true then any newline and carriage-return characters are stripped
		***					from the end of the line.
		***
		*** @return			Returns if readLine fails to read input from the string or true otherwise.
		**/
		bool			readLine(FILE *f, bool stripNL=true);	
		bool			readLine(SWTextFile &f, bool stripNL=true);	

		SWString	left(sw_size_t n) const;
		SWString	right(sw_size_t n) const;
		SWString	mid(sw_index_t pos) const;
		SWString	mid(sw_index_t pos, sw_size_t n) const;

		/// Returns a new string that is a substring of this string.
		SWString	substring(sw_index_t beginIndex) const;

		/// Returns a new string that is a substring of this string.
		SWString	substring(sw_index_t beginIndex, sw_index_t endIndex) const;



		/**
		*** @brief	Convert this string to a signed 32-bit integer.
		***
		*** This method will allow the string to be convert into an integer value using
		*** the specified base value.
		***
		*** @param[in]	base	Specify the base to use when interpreting the character in the string (2-16).
		**/
		sw_int32_t	toInt32(int base=0) const;

		/**
		*** @brief	Convert this string to a unsigned 32-bit integer.
		***
		*** This method will allow the string to be convert into an integer value using
		*** the specified base value.
		***
		*** @param[in]	base	Specify the base to use when interpreting the character in the string (2-16).
		**/
		sw_uint32_t	toUInt32(int base=0) const;

		/**
		*** @brief	Convert this string to a signed 64-bit integer.
		***
		*** This method will allow the string to be convert into an integer value using
		*** the specified base value.
		***
		*** @param[in]	base	Specify the base to use when interpreting the character in the string (2-16).
		**/
		sw_int64_t	toInt64(int base=0) const;

		/**
		*** @brief	Convert this string to a unsigned 64-bit integer.
		***
		*** This method will allow the string to be convert into an integer value using
		*** the specified base value.
		***
		*** @param[in]	base	Specify the base to use when interpreting the character in the string (2-16).
		**/
		sw_uint64_t	toUInt64(int base=0) const;

		/**
		*** @brief	Convert this string to a double.
		***
		*** This method will allow the string to be convert into an floating point value.
		**/
		double		toDouble() const;

public: // String modification

		void		setCharAt(sw_index_t pos, int c);
		int			getCharAt(sw_index_t pos) const;

		/**
		*** @brief	Resize the string to the specified length.
		***
		*** This method resizes the string to contain the specified number of characters.
		*** This will either shrink or grow the string as required. The capacity of the
		*** is increased if required to store the specified number of characters but
		*** is the number of characters is reduced this method does not release memory
		*** back to the program.
		***
		*** If the length specified is greater than the current string length and
		*** the specified pad character is non-zero the string is padded
		*** with the specified pad character. If pad character is zero (the nul character)
		*** the string length remains the same and but the capacity of the string is increased
		*** as if the capacity(nchars) had been called.
		***
		*** @param[in]	nchars	The desired string length.
		*** @param[in]	pad		The padding character
		***
		*** @return		\n		A reference to the current string.
		**/
		SWString &	resize(sw_size_t nchars, int pad);

		/**
		*** @brief	Truncate the string to the specified length.
		***
		*** This method truncates the string to contain the specified number of characters.
		*** if the number of characters specified is greater than the current length.
		***
		*** @param[in]	nchars	The desired string length.
		***
		*** @return				A reference to the current string.
		**/
		SWString &	truncate(size_t nchars);

		SWString &	prepend(int c);								///< Prepend a char to the end of the string
		SWString &	prepend(const char *s);						///< Prepend a string to the front of the string
		SWString &	prepend(const wchar_t *s);					///< Prepend a string to the front of the string
		SWString &	prepend(const SWString &s);					///< Prepend a string to the front of the string
		SWString &	append(int c, sw_size_t count=1);					///< Append a char to the end of the string
		SWString &	append(const char *s);						///< Append a string to the end of the string
		SWString &	append(const wchar_t *s);					///< Append a string to the end of the string
		SWString &	append(const SWString &s);					///< Append a string to the end of the string
		SWString &	insert(sw_index_t pos, const char *s);		///< Insert a string at the specified position
		SWString &	insert(sw_index_t pos, const wchar_t *s);	///< Insert a string at the specified position
		SWString &	insert(sw_index_t pos, const SWString &s);	///< Insert a string at the specified position
		SWString &	remove(sw_index_t pos);						///< Remove all the characters from the position specified
		SWString &	remove(sw_index_t pos, sw_size_t n);			///< Remove n characters from the position specified.
		SWString &	replace(sw_index_t pos, sw_size_t n, const char *s);
		SWString &	replace(sw_index_t pos, sw_size_t n, const wchar_t *s);
		SWString &	replace(sw_index_t pos, sw_size_t n, const SWString &s);
		SWString &	replaceAll(int oldchar, int newchar, int count=0);
		SWString &	replaceAll(const char *oldstr, const char *newstr, caseCompare cmp=exact, int count=0);
		SWString &	replaceAll(const wchar_t *oldstr, const wchar_t *newstr, caseCompare cmp=exact, int count=0);
		SWString &	replaceAll(const SWString &oldstr, const SWString &newstr, caseCompare cmp=exact, int count=0);
		SWString &	strip(int c, stripType type=trailing);
		SWString &	strip(const SWString &s, stripType type=trailing);
		SWString &	toLower();
		SWString &	toUpper();
		SWString &	removeAll(int c);							///< Remove all instances of character c

		/**
		*** @brief	Adds quotes to the string according to parameters.
		***
		*** The quote method will modify the string by adding quotes to the beginning
		*** and end of the string.
		***
		*** If the string does not already start and end with the quote
		*** char (qchar) and it contains white space or onlyIfWhiteSpace is false
		*** then the string is quoted with qchar.
		***
		*** @param qchar			The quote char to add to each end of the string.
		*** @param onlyIfWhiteSpace	If true the string is only quoted if it contains spaces
		*** @param backslash		The char to prefix any embedded quote char with
		***
		**/
		void		quote(int qchar='"', bool onlyIfWhiteSpace=true, int backslash=0);

		/**
		*** @brief	Remove start and end quotes from the string.
		***
		*** If the string starts and ends with the given quote char (qchar)
		*** the quotes are removed. If the backslash parameter is non-null all
		*** instances of the backslash char followed by the quote char are replaced
		*** with the quote char. E.g. if qchar is '"' and backslash is \ and the string 
		*** is <b>"abc \"def\" ghi"</b> the result will be <b>abc "def" ghi</b>
		***
		*** @param qchar		The quote char
		*** @param backslash		The char to prefix any embedded quote char with
		***
		**/
		void		unquote(int qchar='"', int backslash=0);

		/**
		*** @brief	Returns a quoted version of the string object without changing the current string.
		***
		*** @see SWString::quote
		***
		*** @param qchar		The quote char
		*** @param onlyIfWhiteSpace	If true the string is only quoted if it contains spaces
		*** @param backslash		The char to prefix any embedded quote char with
		***
		**/
		SWString	toQuotedString(int qchar='"', bool onlyIfWhiteSpace=true, int backslash='\\') const;

		/**
		*** @brief	Returns a unquoted version of the string object without changing the current string.
		***
		*** @see SWString::unquote
		***
		*** @param qchar		The quote char
		*** @param backslash		The char to prefix any embedded quote char with
		***
		**/
		SWString	toUnquotedString(int qchar='"', int backslash='\\') const;

		/// Reverse all the characters in the string
		SWString &	reverse();

		/// Count the number of characters starting at position pos that are contained with the set of chars specified.
		sw_size_t	count(const SWString &charSet, incType itype=including, sw_index_t pos=0, bool reverse=false) const;

		/**
		*** @brief	Extracts characters from the string that are in the set of characters specified.
		***
		*** This method starts at the position specified (default 0) extracting characters which are contained
		*** within the character set specified by the charSet parameter. If the first character of the string
		*** is not in the character set, then SpanIncluding returns an empty string. Otherwise, it returns a
		*** sequence of consecutive characters that are in the set.
		***
		*** @param	pos			The position at which to start extracting characters.
		*** @param	charSet		A string interpreted as a set of characters. 
		***
		*** @return				A string consisting only of characters specified by the charSet parameter.
		***						The returned string will be blank if there are no characters from the charSet
		***						found at the first searched character position.
		**/
		SWString	span(const SWString &charSet, incType itype=including, sw_index_t pos=0, bool reverse=false) const;


		/**
		*** @brief	Get direct access to string buffer
		***
		*** This method provides direct access to the SWString buffer. The caller should
		*** determine what size characters are in the buffer by calling the getCharSize method.
		***
		*** The caller must call ReleaseBuffer when buffer manipulation is complete.
		***
		*** @param minlen	Increases the capacity of the string to contain at least
		***					minlen characters (not bytes).
		**/
		void *		lockBuffer(sw_size_t minlen, sw_size_t charsize);

		/**
		*** @brief	Release direct access to string buffer
		***
		*** This method releases direct access to the SWString buffer previously
		*** set up by a call to GetBuffer or GetBufferSetLength. 
		***
		*** @param len		Specifies the new length of the string in characters.
		***					If len is negative then the length of the string is
		***					determined using strlen, dclken or qcslen.  Otherwise
		***					the length the string is set to be that specified by len.
		**/
		void		unlockBuffer();

		/// Returns TRUE if self contains no bytes with the high bit set.
		bool		isAscii() const;					


public: // Searching

		int		first(int c) const						{ return indexOf(c); }
		int		first(const char *str) const			{ return indexOf(str); }
		int		first(const wchar_t *str) const			{ return indexOf(str); }
		int		first(const SWString &str) const		{ return indexOf(str); }

		int		last(int c) const						{ return lastIndexOf(c); }
		int		last(const char *str) const				{ return lastIndexOf(str); }
		int		last(const wchar_t *str) const			{ return lastIndexOf(str); }
		int		last(const SWString &str) const			{ return lastIndexOf(str); }

		int		firstOf(const char *str) const;
		int		firstOf(const wchar_t *str) const;
		int		firstOf(const SWString &str) const;
		int		lastOf(const char *str) const;
		int		lastOf(const wchar_t *str) const;
		int		lastOf(const SWString &str) const;

		bool	contains(const char *s, caseCompare cmp=SWString::exact) const		{ return (indexOf(s, cmp) >= 0); }
		bool	contains(const wchar_t *s, caseCompare cmp=SWString::exact) const	{ return (indexOf(s, cmp) >= 0); }
		bool	contains(const SWString &s, caseCompare cmp=SWString::exact) const	{ return (indexOf(s, cmp) >= 0); }

		bool	startsWith(const SWString &s, caseCompare cmp=SWString::exact) const;

		bool	endsWith(const SWString &s, caseCompare cmp=SWString::exact) const;

		int		indexOf(int ch) const;
		int		indexOf(int ch, sw_index_t pos) const;
		int		indexOf(const char *str, caseCompare cmp=SWString::exact) const;
		int		indexOf(const char *str, sw_index_t pos, caseCompare cmp=SWString::exact) const;
		int		indexOf(const wchar_t *str, caseCompare cmp=SWString::exact) const;
		int		indexOf(const wchar_t *str, sw_index_t pos, caseCompare cmp=SWString::exact) const;
		int		indexOf(const SWString &str, caseCompare cmp=SWString::exact) const;
		int		indexOf(const SWString &str, sw_index_t pos, caseCompare cmp=SWString::exact) const;

		int		lastIndexOf(int ch) const;
		int		lastIndexOf(int ch, sw_index_t pos) const;
		int		lastIndexOf(const char *str, caseCompare cmp=SWString::exact) const;
		int		lastIndexOf(const char *str, sw_index_t pos, caseCompare cmp=SWString::exact) const;
		int		lastIndexOf(const wchar_t *str, caseCompare cmp=SWString::exact) const;
		int		lastIndexOf(const wchar_t *str, sw_index_t pos, caseCompare cmp=SWString::exact) const;
		int		lastIndexOf(const SWString &str, caseCompare cmp=SWString::exact) const;
		int		lastIndexOf(const SWString &str, sw_index_t pos, caseCompare cmp=SWString::exact) const;


public: // Formatting

		// Printf like formatting format string
		SWString &		load(sw_uint32_t id);

		// Printf like formatting format string
		SWString &		format(sw_uint32_t id, ...);

		// Printf like formatting using varargs format string
		SWString &		format(sw_uint32_t id, va_list argList);

		/// Printf like formatting (const char *)format string
		SWString &		format(const char *fmt, ...);

		/// Printf like formatting (const wchar_t *)format string
		SWString &		format(const wchar_t *fmt, ...);

		/// Printf like formatting using varargs (const char *)format string
		SWString &		formatv(const char *fmt, va_list argList);

		/// Printf like formatting using varargs (const wchar_t *)format string
		SWString &		formatv(const wchar_t *fmt, va_list argList);


public: // General Operators
		SWString &			operator=(int);					///< Assignment from single-byte char
		SWString &			operator=(const SWString &);	///< Assignment from a SWString object
		SWString &			operator=(const char *);		///< Assignment from a SWString object
		SWString &			operator=(const wchar_t *);		///< Assignment from a SWString object
		SWString &			operator+=(int c);				///< Append a single character
		SWString &			operator+=(const SWString &s);	///< Append a string
		SWString &			operator+=(const char *);		///< Append a string
		SWString &			operator+=(const wchar_t *);	///< Append a string

		SWString			operator+(int c) const;				///< Append a single character
		SWString			operator+(const SWString &s) const;	///< Append a string
		SWString			operator+(const char *) const;		///< Append a string
		SWString			operator+(const wchar_t *) const;		///< Append a string

		operator const char *() const;
		operator const wchar_t *() const;

		/// Returns the nth character of the string
		int				operator[](int n) const;				

		/// Returns the nth character of the string
		int				operator()(int n) const;				

		/**
		*** Substring operator - Returns a string which is part of the current string
		*** as specified by the parameters
		***
		*** @param start Zero based start index
		*** @param len	Maximum number of characters to take
		***
		*** @brief Substring operator
		**/
		SWString		operator()(int start, int len) const;

public: // Comparison operators
		/**
		*** @brief	Compare this object with another.
		***
		*** This method compares this object with the one specified and returns a
		*** result indicating if the other object is less than, equal to, or greater
		*** than this object.
		***
		*** - If this object is less than the specified object then a negative result is returned.
		*** - If this object is greater than the specified object then a positive result is returned.
		*** - If this object is equal to the specified object then a zero result is returned.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				The result of the comparision:
		***						-  0 - This object is equal to the other object.
		***						- <0 - This object is less than the other object.
		***						- >0 - This object is greater than the other object.
		***
		*** @note
		*** The compareTo method is not required to return -1 and +1 for the negative and positive
		*** values although this is typically the value returned. Callers should test for \< 0 and \> 0
		*** rather than for specific values of -1 and +1.
		**/
		int			compareTo(const SWString &other, caseCompare cc=SWString::exact) const;
		int			compareTo(const char *other, caseCompare cc=SWString::exact) const;
		int			compareTo(const wchar_t *other, caseCompare cc=SWString::exact) const;

		int			compareNoCase(const SWString &other) const	{ return compareTo(other, ignoreCase); }
		int			compareNoCase(const char *other) const	{ return compareTo(other, ignoreCase); }
		int			compareNoCase(const wchar_t *other) const	{ return compareTo(other, ignoreCase); }

		/**
		*** @brief	Test for equality
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is equal to the other object, false otherwise.
		**/
		bool	operator==(const SWString &other) const	{ return (compareTo(other) == 0); }
		bool	operator==(const char *other) const		{ return (compareTo(other) == 0); }
		bool	operator==(const wchar_t *other) const	{ return (compareTo(other) == 0); }

		/**
		*** @brief	Test for inequality
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is not equal to the other object, false otherwise.
		**/
		bool	operator!=(const SWString &other) const	{ return (compareTo(other) != 0); }
		bool	operator!=(const char *other) const		{ return (compareTo(other) != 0); }
		bool	operator!=(const wchar_t *other) const	{ return (compareTo(other) != 0); }

		/**
		*** @brief	Test for less than.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is less than the other object, false otherwise.
		**/
		bool	operator< (const SWString &other) const	{ return (compareTo(other) <  0); }
		bool	operator< (const char *other) const		{ return (compareTo(other) <  0); }
		bool	operator< (const wchar_t *other) const	{ return (compareTo(other) <  0); }

		/**
		*** @brief	Test for greater than.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is greater than the other object, false otherwise.
		**/
		bool	operator> (const SWString &other) const	{ return (compareTo(other) >  0); }
		bool	operator> (const char *other) const		{ return (compareTo(other) >  0); }
		bool	operator> (const wchar_t *other) const	{ return (compareTo(other) >  0); }

		/**
		*** @brief	Test for less than or equal to.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is less than or equal to the other object, false otherwise.
		**/
		bool	operator<=(const SWString &other) const	{ return (compareTo(other) <= 0); }
		bool	operator<=(const char *other) const		{ return (compareTo(other) <= 0); }
		bool	operator<=(const wchar_t *other) const	{ return (compareTo(other) <= 0); }

		/**
		*** @brief	Test for greater than or equal to.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is greater than or equal to the other object, false otherwise.
		**/
		bool	operator>=(const SWString &other) const	{ return (compareTo(other) >= 0); }
		bool	operator>=(const char *other) const		{ return (compareTo(other) >= 0); }
		bool	operator>=(const wchar_t *other) const	{ return (compareTo(other) >= 0); }


		friend bool	operator==(const char *s1,     const SWString &s2)		{ return (s2.compareTo(s1) == 0); }
		friend bool	operator==(const wchar_t *s1,  const SWString &s2)		{ return (s2.compareTo(s1) == 0); }

		friend bool	operator!=(const char *s1,     const SWString &s2)		{ return (s2.compareTo(s1) != 0); }
		friend bool	operator!=(const wchar_t *s1,  const SWString &s2)		{ return (s2.compareTo(s1) != 0); }

		friend bool	operator< (const char *s1,     const SWString &s2)		{ return (s2.compareTo(s1) >  0); }
		friend bool	operator< (const wchar_t *s1,  const SWString &s2)		{ return (s2.compareTo(s1) >  0); }

		friend bool	operator> (const char *s1,     const SWString &s2)		{ return (s2.compareTo(s1) <  0); }
		friend bool	operator> (const wchar_t *s1,  const SWString &s2)		{ return (s2.compareTo(s1) <  0); }

		friend bool	operator<=(const char *s1,     const SWString &s2)		{ return (s2.compareTo(s1) >= 0); }
		friend bool	operator<=(const wchar_t *s1,  const SWString &s2)		{ return (s2.compareTo(s1) >= 0); }

		friend bool	operator>=(const char *s1,     const SWString &s2)		{ return (s2.compareTo(s1) <= 0); }
		friend bool	operator>=(const wchar_t *s1,  const SWString &s2)		{ return (s2.compareTo(s1) <= 0); }

public: // MFC compatibillty
#if defined(SW_BUILD_MFC)
		SWString(const CString &s);
		operator CString() const;
		SWString &			operator=(const CString &);	///< Assignment from a CString object
		SWString &			operator+=(const CString &);	///< Append a CString object
		int					compareTo(const CString &other, caseCompare cc=SWString::exact) const;

		int					compareNoCase(const CString &other) const		{ return compareTo(other, ignoreCase); }

		bool	operator==(const CString &other) const						{ return (compareTo(other) == 0); }
		bool	operator!=(const CString &other) const						{ return (compareTo(other) != 0); }
		bool	operator< (const CString &other) const						{ return (compareTo(other) <  0); }
		bool	operator> (const CString &other) const						{ return (compareTo(other) >  0); }
		bool	operator<=(const CString &other) const						{ return (compareTo(other) <= 0); }
		bool	operator>=(const CString &other) const						{ return (compareTo(other) >= 0); }

		friend bool	operator==(const CString &s1, const SWString &s2)		{ return (s2.compareTo(s1) == 0); }
		friend bool	operator!=(const CString &s1, const SWString &s2)		{ return (s2.compareTo(s1) != 0); }
		friend bool	operator< (const CString &s1, const SWString &s2)		{ return (s2.compareTo(s1) >  0); }
		friend bool	operator> (const CString &s1, const SWString &s2)		{ return (s2.compareTo(s1) <  0); }
		friend bool	operator<=(const CString &s1, const SWString &s2)		{ return (s2.compareTo(s1) >= 0); }
		friend bool	operator>=(const CString &s1, const SWString &s2)		{ return (s2.compareTo(s1) <= 0); }
#endif


public: // Microsoft CString methods

		/// Return the length of the string
		int			GetLength() const												{ return (int)length(); }
		bool		IsEmpty() const													{ return isEmpty(); }
		void		Empty()															{ empty(); }
		sw_char_t	GetAt(int n) const												{ return getCharAt(n); }
		void		SetAt(int n, sw_char_t c)										{ setCharAt(n, c); }
		int			Compare(const char *s) const									{ return compareTo(s, exact); }
		int			Compare(const wchar_t *s) const									{ return compareTo(s, exact); }
		int			Compare(const SWString &s) const								{ return compareTo(s, exact); }
		int			CompareNoCase(const char *s) const								{ return compareTo(s, ignoreCase); }
		int			CompareNoCase(const wchar_t *s) const							{ return compareTo(s, ignoreCase); }
		int			CompareNoCase(const SWString &s) const							{ return compareTo(s, ignoreCase); }
		//int		Collate(sw_tchar_t *s) const									{ return collate(s, exact); }
		//int		CollateNoCase(sw_tchar_t *s) const								{ return collate(s, ignoreCase); }
		SWString	Mid(int pos) const												{ return mid(pos); }
		SWString	Mid(int pos, int n) const										{ return mid(pos, n); }
		SWString	Left(int n) const												{ return left(n); }
		SWString	Right(int n) const												{ return right(n); }
		void		MakeLower()														{ toLower(); }
		void		MakeUpper()														{ toUpper(); }
		void		MakeReverse()													{ reverse(); }
		SWString	SpanIncluding(const sw_tchar_t *charSet) const					{ return span(charSet, including); }
		SWString	SpanExcluding(const sw_tchar_t *charSet) const					{ return span(charSet, excluding); }

		// Replace all occurences of oldchar with newchar
		int			Replace(int oldchar, int newchar)								{ int count=0; replaceAll(oldchar, newchar, count); return count; }
		int			ReplaceAll(int oldchar, int newchar)							{ int count=0; replaceAll(oldchar, newchar, count); return count; }
		
		// Replace all occurences of oldstr with newstr
		int			Replace(const char *oldstr, const char *newstr)					{ int count=0; replaceAll(oldstr, newstr, exact, count); return count; }
		int			Replace(const wchar_t *oldstr, const wchar_t *newstr)			{ int count=0; replaceAll(oldstr, newstr, exact, count); return count; }
		int			ReplaceAll(const char *oldstr, const char *newstr)				{ int count=0; replaceAll(oldstr, newstr, exact, count); return count; }
		int			ReplaceAll(const wchar_t *oldstr, const wchar_t *newstr)		{ int count=0; replaceAll(oldstr, newstr, exact, count); return count; }

		// Remove all occurences of char in string
		int			Remove(sw_tchar_t c);
		int			Append(const char *s)											{ append(s); return (int)length(); }
		int			Append(const wchar_t *s)										{ append(s); return (int)length(); }
		int			Append(sw_tchar_t c)											{ sw_tchar_t s[2]; s[0] = c; s[1] = 0; append(s); return (int)length(); }
		int			Insert(int pos, sw_tchar_t c)									{ sw_tchar_t s[2]; s[0] = c; s[1] = 0; insert(pos, s); return (int)length(); }
		int			Insert(int pos, const sw_tchar_t *s)							{ insert(pos, s); return (int)length(); }
		int			Delete(int pos, int n=1)										{ remove(pos, n); return (int)length(); }

		// Strip the specified characters from the string
		void		TrimLeft(sw_tchar_t c)													{ sw_tchar_t	s[2]; s[0] = c; s[1] = 0; strip(s, leading); }
		void		TrimLeft(const sw_tchar_t *s=_T(" \t\n"))						{ strip(s, leading); }
		void		TrimRight(sw_tchar_t c)											{ sw_tchar_t	s[2]; s[0] = c; s[1] = 0; strip(s, trailing); }
		void		TrimRight(const sw_tchar_t *s=_T(" \t\n"))						{ strip(s, trailing); }


		// Printf like formatting
		//void		Format(const sw_tchar_t *fmt, ...);
		//void		Format(UINT id, ...);
		//void		FormatV(const sw_tchar_t *fmt, va_list argList);

		// FormatMessage (not yet supported)
		// void		FormatMessage(sw_tchar_t *fmt, ...);
		// void		FormatMessage(UINT id, ...);

		// Load the string specified by the id
		bool			LoadString(sw_uint32_t id)											{ load(id); return true; }
/*
bool			LoadString(UINT id, CA_RESOURCE_MODULE hModule);
#if defined(WIN32) || defined(_WIN32)
		bool			LoadString(UINT id, HINSTANCE hInst);
#endif
*/

		// Find characters
		int			Find(sw_tchar_t ch) const										{ return first(ch); }
		int			Find(sw_tchar_t *str) const										{ return indexOf(str, 0, exact); }
		int			Find(sw_tchar_t ch, int pos) const								{ return indexOf(ch, pos); }
		int			Find(sw_tchar_t *str, int pos) const							{ return indexOf(str, pos, exact); }
		int			ReverseFind(sw_tchar_t ch) const								{ return last(ch); }
		int			FindOneOf(sw_tchar_t *charSet) const							{ return firstOf(charSet); };

		// Direct access to buffer
		sw_tchar_t *		GetBuffer(sw_size_t size)								{ return (sw_tchar_t *)lockBuffer(size, sizeof(sw_tchar_t)); }
		void		ReleaseBuffer()											{ unlockBuffer(); }

		//sw_tchar_t *		GetBuffer(int minlen);
		//sw_tchar_t *		GetBufferSetLength(int len);
		//void		ReleaseBuffer(int len=-1);
		//void		FreeExtra();
		//void		LockBuffer();
		//void		UnlockBuffer();
		//bool		BufferLocked() const									{ return (_strobj != NULL && _strobj->_refcount < 0); }


public: // Virtual overrides

		/// Override for SWObject writeToStream
		virtual sw_status_t		writeToStream(SWOutputStream &stream) const;

		/// Override for SWObject readFromStream
		virtual sw_status_t		readFromStream(SWInputStream &stream);


public: // STATIC methods
		static sw_int32_t	toInt32(const char *str, int base=0);			///< Convert the given string to a signed integer
		static sw_int32_t	toInt32(const wchar_t *str, int base=0);		///< Convert the given string to a signed integer
		static sw_uint32_t	toUInt32(const char *str, int base=0);			///< Convert the given string to an unsigned integer
		static sw_uint32_t	toUInt32(const wchar_t *str, int base=0);		///< Convert the given string to an unsigned integer
		static sw_int64_t	toInt64(const char *str, int base=0);			///< Convert the given string to a 64-bit integer
		static sw_int64_t	toInt64(const wchar_t *str, int base=0);		///< Convert the given string to a 64-bit integer
		static sw_uint64_t	toUInt64(const char *str, int base=0);			///< Convert the given string to a 64-bit integer
		static sw_uint64_t	toUInt64(const wchar_t *str, int base=0);		///< Convert the given string to a 64-bit integer
		static double		toDouble(const char *str);						///< Convert the given string to a double
		static double		toDouble(const wchar_t *str);					///< Convert the given string to a double

		/**
		*** @brief	Static method to return a quoted version of the string parameter.
		***
		*** @see SWString::quote
		***
		*** @param[in] str				The string to process
		*** @param[in] qchar			The quote char
		*** @param[in] onlyIfWhiteSpace	If true the string is only quoted if it contains spaces
		*** @param[in] backslash		The char to prefix any embedded quote char with
		***
		**/
		static SWString		quote(const SWString &str, int qchar='"', bool onlyIfWhiteSpace=true, int backslash=0);


		/**
		*** @brief	Static method to return an unquoted version of the string parameter.
		***
		*** @see SWString::unquote
		***
		*** @param[in] str				The string to process
		*** @param[in] qchar			The quote char
		*** @param[in] backslash		The char to prefix any embedded quote char with
		***
		**/
		static SWString		unquote(const SWString &str, int qchar='"', int backslash=0);

		/// Static method to convert the specifed string to lower case and return it in a new string.
		static SWString		toLower(const SWString &s);

		/// Static method to convert the specifed string to upper case and return it in a new string.
		static SWString		toUpper(const SWString &s);

		static SWString valueOf(const SWValue &v);
		static SWString	valueOf(bool v);
		static SWString	valueOf(sw_int8_t v);
		static SWString	valueOf(sw_int16_t v);
		static SWString	valueOf(sw_int32_t v);
		static SWString	valueOf(sw_int64_t v);
		static SWString	valueOf(sw_uint8_t v);
		static SWString	valueOf(sw_uint16_t v);
		static SWString	valueOf(sw_uint32_t v);
		static SWString	valueOf(sw_uint64_t v);
		static SWString	valueOf(float v);
		static SWString	valueOf(double v);

		static bool		isSpaceChar(int c);
		static bool		isAscii(const char *s);

protected: // methods
		void				init();
		void				bufCopy(const void *pData, sw_size_t nchars, sw_size_t charsize);
		void				bufAppend(const void *pData, sw_size_t nchars, sw_size_t charsize);
		void				bufInsert(sw_size_t pos, const void *pData, sw_size_t nchars, sw_size_t charsize);
		void				bufRemove(sw_size_t pos, sw_size_t nchars, sw_size_t charsize);
		void				bufSetChars(int c, sw_size_t pos, sw_size_t nchars, bool checkCapacity, bool addNullChar);
		int					bufCharAt(sw_size_t pos) const;
		void				setCharSize(sw_size_t cs);
		void *				ensureCapacity(sw_size_t nchars, bool freeExtraMem);
		void				clearConversion();
		bool				regionMatches(sw_index_t pos, sw_size_t len, const SWString &other, sw_index_t otherpos, caseCompare cmp) const;
		void				readyForUpdate();

protected: // static methods
		static bool				isWideChar(int c)		{ return (c > 255 || c < -128); }
		static int				getCharFromBuffer(const void *bp, sw_index_t offset, sw_size_t charsize);
		static int				indexOfString(const void *pat, sw_size_t patlen, sw_size_t patcsize, const void *str, size_t slen, sw_size_t strcsize, sw_index_t pos, caseCompare cmp, bool reverse);
		static int				findOneOf(const void *pat, sw_size_t patlen, sw_size_t patcsize, const void *str, size_t slen, sw_size_t strcsize, sw_index_t pos, caseCompare cmp, bool reverse);
		static unsigned long	convertStringToULong(const void *sp, int cs, int base, bool uflag);
		static sw_uint64_t		convertStringToUInt64(const void *sp, int cs, int base, bool uflag);


protected: // static members
		static SWMemoryManager	*sm_pMemoryManager;
		static sw_size_t		sm_zero;

protected: // members
		SWBuffer			m_data;			///< The buffer containing the string data
		sw_size_t			m_length;		///< The string length in characters (not multi-byte)
		sw_uint16_t			m_flags;		///< The various string flags
		mutable SWBuffer	m_altdata;		///< The buffer containing the string conversion data
		};

SW_DEFINE_EXCEPTION(SWStringException, SWException);

// Global operators
//SWString	operator+(const SWString &lhs, int rhs);
//SWString	operator+(const SWString &lhs, const char *rhs);
//SWString	operator+(const SWString &lhs, const wchar_t *rhs);
//SWString	operator+(const SWString &lhs, const SWString &rhs);

SWIFT_DLL_EXPORT SWString	operator+(int lhs, const SWString &rhs);
SWIFT_DLL_EXPORT SWString	operator+(const char *lhs, const SWString &rhs);
SWIFT_DLL_EXPORT SWString	operator+(const wchar_t *lhs, const SWString &rhs);




#endif
