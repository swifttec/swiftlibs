/**
*** @file		SWDataQueue.h
*** @brief		A class to queue data in arbitary chunks
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWDataQueue class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




#ifndef __SWDataQueue_h__
#define __SWDataQueue_h__

#include <swift/SWAbstractQueue.h>
#include <swift/SWException.h>
#include <swift/SWBuffer.h>

// Forward declarations
SW_DEFINE_EXCEPTION(SWDataQueueException, SWQueueException);


/**
*** @brief A abstract class for queuing data in arbitary chunks.
***
*** This is an abstract class for queuing data in arbitary chunks.
*** Data queues copy data added to them rather than storing
*** a pointer to the data. This allows the data to be stored in
*** an arbitary location, e.g. disk or memory.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWDataQueue : public SWAbstractQueue
		{
public:
		// Constructors, destructors and operators
		SWDataQueue();
		virtual ~SWDataQueue();

		/**
		*** Add the specified data to the end of the queue.
		***
		*** @param	data		A pointer the data to be added.
		*** @param	datalen		The amount of the data (in bytes) to be copied.
		*** @return				SW_STATUS_SUCCESS if successful, or an error code.
		***
		*** @throws	SWQueueInactiveException if the queue is disabled.
		**/
		bool		add(const void *data, int datalen)						{ return putData(data, datalen); }

		/**
		*** Add the contents of the specified buffer to the end of the queue.
		***
		*** @param	data		A buffer containing the data to be added.
		*** @return				SW_STATUS_SUCCESS if successful, or an error code.
		***
		*** @throws	SWQueueInactiveException if the queue is disabled.
		**/
		bool		add(SWBuffer &data)										{ return putData(data.data(), data.size()); }

		/**
		*** Gets and removes the next data block from the queue.
		***
		*** If a data block is available the the buffer specified is filled in
		*** with the data and the data block is removed from the queue.
		***
		*** @return 			SW_STATUS_SUCCESS if data was available and returned.
		***						SW_STATUS_TIMEOUT if no data was available within the
		***						specified timeout period.
		***						The error code if another error occurred.
		***
		*** @throws	SWQueueInactiveException if the queue is disabled.
		**/
		bool		get(SWBuffer &data, sw_uint64_t waitms=SW_TIMEOUT_INFINITE)	{ return getData(data, false, waitms); }


		/**
		*** Gets the next data block from the queue without removing it.
		***
		*** If a data block is available the the buffer specified is filled in
		*** with the data. The data block remains in the queue.
		***
		*** @return 			SW_STATUS_SUCCESS if data was available and returned.
		***						SW_STATUS_TIMEOUT if no data was available within the
		***						specified timeout period.
		***						The error code if another error occurred.
		***
		*** @throws	SWQueueInactiveException if the queue is disabled.
		**/
		bool		peek(SWBuffer &data, sw_uint64_t waitms=SW_TIMEOUT_INFINITE)	{ return getData(data, true, waitms); }

		/**
		*** Gets the size of the next data block that will be returned by the next
		*** call to get or peek.
		***
		*** @return 			SW_STATUS_SUCCESS if data was available and returned.
		***						SW_STATUS_TIMEOUT if no data was available within the
		***						specified timeout period.
		***						The error code if another error occurred.
		***
		*** @throws	SWQueueInactiveException if the queue is disabled.
		**/
		bool		peekSize(size_t &size, sw_uint64_t waitms=SW_TIMEOUT_INFINITE)	{ return getDataSize(size, waitms); }

public: // Overrides for SWAbstractQueue methods

		// Empty the queue of all messages
		virtual void		clear(bool emptyInactiveQueue);

public: // Overrides for SWCollection methods
		virtual void		clear()		{ clear(false); }


public: // Pure virtual methods which must be supplied by SWDataQueue implementors
		virtual sw_int64_t		datasize()=0;	// The number of bytes of data in the queue

protected: // Pure virtual methods which must be supplied by SWDataQueue implementors
		virtual bool		getDataSize(size_t &size, sw_uint64_t waitms)=0;
		virtual bool		getData(SWBuffer &data, bool peekflag, sw_uint64_t waitms)=0;
		virtual bool		putData(const void *data, size_t datalen)=0;
		virtual bool		clearData(bool emptyInactiveQueue)=0;
		};

#endif
