/**
*** @file
*** @brief	A collection of utility functions
**/

#ifndef __swift_sw_buf_h__
#define __swift_sw_buf_h__

#include <swift/swift.h>

/// Little endian byte order (LSB..MSB)
#define SW_BYTEORDER_LITTLE_ENDIAN	0

/// Big endian byte order (MSB..LSB)
#define SW_BYTEORDER_BIG_ENDIAN	1

/// Network byte order (same as big endian)
#define SW_BYTEORDER_NETWORK		SW_BYTEORDER_BIG_ENDIAN

/**
*** Read a signed integer from a memory buffer (max 8 bytes, 64 bits).
***
*** @param	buf		The memory buffer to read from
*** @param	nbytes	The number of bytes to read (1-8)
*** @param	order	The expected byte order (0=LSB..MSB), (1=MSB..LSB)
*** @return			The integer value read from the buffer
***
*** This function reads a signed integer from the given memory buffer using the specified
*** number of bytes (max 8 bytes, 64 bits). The integer is expected to be in the form
*** LSB ... MSB, i.e. the least significant byte will be the first byte read.
***
*** The maximum size of integer that can be read is 8 bytes. Smaller values can be read by 
*** casting the result into the required integer type, for example:
***
*** short v = (short)sw_buf_getSignedInteger(pBuf, sizeof(v));
**/
SWIFT_DLL_EXPORT sw_int64_t		sw_buf_getSignedInteger(void *buf, int nbytes, int order=SW_BYTEORDER_LITTLE_ENDIAN);
		
/**
*** Write a signed integer into a memory buffer (max 8 bytes, 64 bits).
***
*** @param	buf		The memory buffer to write to
*** @param	v		The integer value to write into memory
*** @param	order	The expected byte order (0=LSB..MSB), (1=MSB..LSB)
*** @param	nbytes	The number of bytes to write (1-8)
***
*** This function writes a signed integer into the given memory buffer using the specified
*** number of bytes (max 8 bytes, 64 bits). The integer will be written in the form
*** LSB ... MSB, i.e. the least significant byte will be the first byte written.
***
*** The maximum size of integer that can be written is 8 bytes.
**/
SWIFT_DLL_EXPORT void			sw_buf_putSignedInteger(void *buf, sw_int64_t v, int nbytes, int order=SW_BYTEORDER_LITTLE_ENDIAN);

/**
*** @brief			Read an unsigned integer from a memory buffer (max 8 bytes, 64 bits)
***
*** @param	buf		The memory buffer to read from
*** @param	nbytes	The number of bytes to read (1-8)
*** @param	order	The expected byte order (0=LSB..MSB), (1=MSB..LSB)
*** @return			The integer value read from the buffer
***
*** This function reads an unsigned integer from the given memory buffer using the specified
*** number of bytes (max 8 bytes, 64 bits). The integer is expected to be in the form
*** LSB ... MSB, i.e. the least significant byte will be the first byte read.
***
*** The maximum size of integer that can be read is 8 bytes. Smaller values can be read by 
*** casting the result into the required integer type, for example:
***
*** unsigned short v = (unsigned short)sw_buf_getUnsignedInteger(pBuf, sizeof(v));
**/
SWIFT_DLL_EXPORT sw_uint64_t		sw_buf_getUnsignedInteger(void *buf, int nbytes, int order=SW_BYTEORDER_LITTLE_ENDIAN);
		

/**
*** @brief			Write an unsigned integer into a memory buffer (max 8 bytes, 64 bits)
***
*** @param	buf		The memory buffer to write to
*** @param	v		The integer value to write into memory
*** @param	nbytes	The number of bytes to write (1-8)
*** @param	order	The expected byte order (0=LSB..MSB), (1=MSB..LSB)
***
*** This function writes an unsigned integer into the given memory buffer using the specified
*** number of bytes (max 8 bytes, 64 bits). The integer will be written according to the order
*** parameter where:
***
*** SW_BYTEORDER_LITTLE_ENDIAN	= LSB..MSB , i.e. the least significant byte will be the first byte written.
*** SW_BYTEORDER_BIG_ENDIAN	= MSB..LSB , i.e. the most significant byte will be the first byte written.
***
*** The maximum size of integer that can be written is 8 bytes.
**/
SWIFT_DLL_EXPORT void			sw_buf_putUnsignedInteger(void *buf, sw_uint64_t v, int nbytes, int order=SW_BYTEORDER_LITTLE_ENDIAN);

#endif // __swift_sw_buf_h__
