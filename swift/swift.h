/**
*** @file		swift.h
*** @brief		General header for the swift library.
*** @version	1.0
*** @author		Simon Sparkes
***
*** This file is the general header used by all of the components of the
*** swift library.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __swift_h__
#define __swift_h__

#include <xplatform/xplatform.h>
// #include <swift/sw_product.h>

#if defined(SW_PLATFORM_WINDOWS)
	// Disable common warnings
	#pragma warning(disable: 4251)
	#pragma warning(disable: 4996)

	// Determine the import/export type for a DLL build
	#if defined(SWIFT_BUILD_DLL)
		#define SWIFT_DLL_EXPORT __declspec(dllexport)
	#else
		#define SWIFT_DLL_EXPORT __declspec(dllimport)
	#endif // defined(SW_BUILD_DLL)

#else // defined(SW_PLATFORM_WINDOWS)

	// On non-windows platforms or non-DLL builds we just define this to be nothing
	#define SWIFT_DLL_EXPORT

#endif // defined(SW_PLATFORM_WINDOWS)


#if !defined(SW_BUILD_RTTI)
	#error RTTI not enabled
#endif

#if !defined(SW_BUILD_NATIVE_WCHAR)
	#error NATIVE_WCHAR not enabled
#endif

#include <xplatform/sw_types.h>
//#include <swift/sw_memory.h>

// Include the generated status codes - must be after include sw_types.h
#include <xplatform/sw_status_codes.h>

// Always include miscellaeous and O/S compatibility functions
#include <swift/sw_functions.h>

#define safe_delete(x)			if (x != NULL) { delete x; x = NULL; }
#define safe_delete_array(x)	if (x != NULL) { delete [] x; x = NULL; }

#ifndef __sw_trace_h__
	#include <swift/sw_trace.h>
#endif

#endif // __swift_h__
