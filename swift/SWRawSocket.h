/**
*** @file		SWRawSocket.h
*** @brief		A raw socket
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWRawSocket class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWRawSocket_h__
#define __SWRawSocket_h__

#include <swift/SWSocket.h>



// Forward declarations

/**
*** @brief	A raw socket class
***
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWRawSocket : public SWSocket
		{

public:
		/// Default constructor
		SWRawSocket();

		/// constructor
		SWRawSocket(int family, int protocol);

		/// Virtual destructor
		virtual ~SWRawSocket();

		/// Opens/creates the socket without making a connection.
		virtual sw_status_t			open();

		/// Closes the socket.
		virtual sw_status_t			close();

		/**
		*** @brief	Receive data fron an unconnected receiver.
		***
		*** This method reads data from the socket address. The specified
		*** addr parameter receives the address that the data was received from.
		***
		*** @param[out]	addr		Receives the socket address the data was sent from.
		*** @param[in]	pBuffer		A pointer to a buffer that will receive the data.
		*** @param[in]	bytesToRead	The number of bytes to attempt to read from the socket.
		*** @param[out]	pBytesRead	A pointer which if non-NULL points to a
		***							sw_size_t variable which receives a count
		***							of the bytes actually read from the socket.
		***
		*** @return		SW_STATUS_SUCCESS if the operation was successful, or a
		***				status code otherwise.
		***
		*** @note
		*** Due to the nature of UDP sockets (being packet oriented in nature) the amount of data
		*** actually read from the socket is normally less than that of the bytesToRead parameter.
		*** If there is more data in the packet than could be read into the buffer, the additional
		*** data would be lost.
		***
		**/
		virtual sw_status_t			receiveFrom(SWSocketAddress &addr, void *pBuffer, sw_size_t bytesToRead, sw_size_t *pBytesRead=NULL);

		/**
		*** Read data from a connected socket into the specified buffer.
		***
		*** This method reads data from a connected socket.
		***
		*** @param[in]	pBuffer		A pointer to a buffer that will receive the data.
		*** @param[in]	bytesToRead	The number of bytes to attempt to read from the socket.
		*** @param[out]	pBytesRead	A pointer which if non-NULL points to a
		***							sw_size_t variable which receives a count
		***							of the bytes actually read from the socket.
		***
		*** @return					A status code indicating the success or failure of the operation:
		***							- SW_STATUS_UNSUPPORTED_OPERATION if this socket does not support this operation.
		***							- SW_STATUS_SUCCESSFUL if the read was successful.
		***							- An error code if some other failure occurred.
		**/
		virtual sw_status_t			read(void *pBuffer, sw_size_t bytesToRead, sw_size_t *pBytesRead=NULL);

		/**
		*** Read available data from a connected socket into the specified buffer.
		***
		*** This method reads available data from a connected socket. If no data is available then
		*** none is returned.
		***
		*** @param[in]	pBuffer		A pointer to a buffer that will receive the data.
		*** @param[in]	bytesToRead	The number of bytes to attempt to read from the socket.
		*** @param[out]	pBytesRead	A pointer which if non-NULL points to a
		***							sw_size_t variable which receives a count
		***							of the bytes actually read from the socket.
		***
		*** @return					A status code indicating the success or failure of the operation:
		***							- SW_STATUS_UNSUPPORTED_OPERATION if this socket does not support this operation.
		***							- SW_STATUS_SUCCESSFUL if the read was successful.
		***							- An error code if some other failure occurred.
		**/
		virtual sw_status_t			receive(void *pBuffer, sw_size_t bytesToRead, sw_size_t *pBytesRead=NULL);


		virtual sw_status_t			addMembership(int index);
		virtual sw_status_t			dropMembership(int index);

private:
		int		m_family;		///< The socket address family - normally AF_PACKET
		int		m_protocol;		///< The socket protocol to use
		};




#endif
