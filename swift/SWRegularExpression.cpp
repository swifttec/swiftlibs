/****************************************************************************
**	SWRegularExpression.cpp	A class to handle regular expressions
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



// Needed for precompiled header support
#include "stdafx.h"

#include <swift/SWRegularExpression.h>
#include <swift/SWStringArray.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





/**
*** Internal node used in a compiled regular expression
**/
class SWRegExNode : public SWObject
		{
	friend class SWRegularExpression;

	public:
		enum SWRegExNodetype
			{
			String,	// A fixed string
			Range,	// A character range
			Anchor,	// An anchor to the beginning or the end of the string
			Capture,	// Used to tag the start and end of a string capture
			Unknown
			} ;

	protected:
		SWRegExNode(SWRegularExpression *owner, SWRegExNodetype t);

	public:
		virtual int		matches(const sw_tchar_t *s, bool matchStart)		{ return -1; }
		int				nextNodeMatches(const sw_tchar_t *s, bool sflag)		{ return (_next != NULL)?_next->matches(s, sflag):0; }

		void			setMatchFlag(bool v)		{ _matchFlag = v; }

		virtual void	dump();

		SWRegExNodetype	type() const			{ return _type; }

	protected:
		SWRegExNodetype		_type;		// The node type
		bool			_matchFlag;	// Specifies what we want to match
		SWRegExNode		*_next;		// The next node to be evalutated
		SWRegularExpression	*_owner;
		};


SWRegExNode::SWRegExNode(SWRegularExpression *owner, SWRegExNodetype t) :
    _owner(owner),
    _type(t),
    _matchFlag(true),
    _next(0)
    	{
		}


void
SWRegExNode::dump()
		{
		printf("Unknown - type=%d  match=%d\n", _type, _matchFlag);
		}


/**
*** Anchor - matches the string only
**/
class SWRegExAnchor : public SWRegExNode
		{
	public:
		SWRegExAnchor(SWRegularExpression *owner, bool start) : SWRegExNode(owner, Anchor), _startFlag(start) { }
		
		virtual int	matches(const sw_tchar_t *s, bool matchStart);
		virtual void	dump();

		bool		isStartAnchor() const	{ return _startFlag; }

	private:
		bool	_startFlag;
		};


int	
SWRegExAnchor::matches(const sw_tchar_t *str, bool matchStart)
		{
		int	p = nextNodeMatches(str, _startFlag);

		if (p >= 0 && !_startFlag)
			{
			if (str[p]) return -1;
			}

		return p;
		}


void
SWRegExAnchor::dump()
		{
		printf("Anchor (%s)\n", _startFlag?"start":"end");
		}


/**
*** String - matches the string only
**/
class SWRegExString : public SWRegExNode
		{
	public:
		SWRegExString(SWRegularExpression *owner, const SWString &str) : SWRegExNode(owner, String), _string(str) { }
		
		virtual int	matches(const sw_tchar_t *s, bool matchStart);

		virtual void	dump();

	private:
		SWString	_string;
		};


int
SWRegExString::matches(const sw_tchar_t *str, bool matchStart)
		{
		const sw_tchar_t	*sp;
		bool				gotMatch=false;
		sw_tchar_t			fc = _string[0];
		int					r, count=0, len=(int)_string.length();

		for (sp=str;*sp;count++,sp++)
			{
			if (_owner->isCaseSensitive()) r = t_strncmp(_string, sp, 1);
			else r = t_strnicmp(_string, sp, 1);

			if (r == 0)
				{
				if (_owner->isCaseSensitive()) r = t_strncmp(_string, sp, len);
				else r = t_strnicmp(_string, sp, len);

				if (r == 0)
					{
					// We've got a match - now see if the next node matches
					// from here
					r = nextNodeMatches(sp+len, false);
					if (r >= 0) return len+count+r;
					}
				}

			if (matchStart) break;
			}

		return -1;
		}


void
SWRegExString::dump()
		{
		printf("String (%s)\n", _string.c_str());
		}

/**
*** Range Node - matches any chars in the given range
**/
class SWRegExRange : public SWRegExNode
		{
	public:
		SWRegExRange(SWRegularExpression *owner) : SWRegExNode(owner, Range), _minChars(0), _maxChars(0) {}

		void		addMatchPair(sw_tchar_t f, sw_tchar_t l);

		void		setMinMax(int min, int max)	{ _minChars = min; _maxChars = max; }

		virtual void	dump();

		virtual int	matches(const sw_tchar_t *s, bool matchStart);

	public:
		SWStringArray	_matchPairArray;
		int		_minChars;	// The minimum # of chars to match
		int		_maxChars;	// The minimum # of chars to match
		};


int
SWRegExRange::matches(const sw_tchar_t *str, bool matchStart)
		{
		const sw_tchar_t	*sp;
		bool		gotMatch=false;
		int		i, r, count=0;

		if (_owner->isGreedy())
			{
			// Like perl match as many chars as possible (default)
			for (sp=str;*sp;sp++)
				{
				if ((_maxChars != 0 && count >= _maxChars)) break;

				// Check to see if this char is in one of the specified ranges
				gotMatch = false;

				sw_tchar_t	c, f, l;

				c = *sp;
				if (!_owner->isCaseSensitive() && c >= 'A' && c <= 'Z') c = c - 'A' + 'a';

				for (i=0;i<(int)_matchPairArray.size();i++)
					{
					SWString	&sr = _matchPairArray[i];

					f = sr[0];
					l = sr[1];
					if (!_owner->isCaseSensitive() && f >= 'A' && f <= 'Z') f = f - 'A' + 'a';
					if (!_owner->isCaseSensitive() && l >= 'A' && l <= 'Z') l = l - 'A' + 'a';

					if (c >= f && c <= l)
						{
						gotMatch = true;
						break;
						}
					}

				if (gotMatch != _matchFlag) break;
				count++;
				}

			// If we haven't got enough chars - FAIL
			if (count < _minChars) return -1;

			// Now starting
			while (count >= _minChars)
				{
				// now see if the next node matches
				r = nextNodeMatches(sp, true);
				if (r >= 0) return count+r;

				// Try moving back one char
				sp--;
				count--;
				}
			}
		else
			{
			// Unlike perl match as few chars as possible (original code)
			for (sp=str;;count++,sp++)
				{
				if (count >= _minChars && (_maxChars == 0 || count <= _maxChars))
					{
					// We've got a match - now see if the next node matches
					// from here
					r = nextNodeMatches(sp, true);
					if (r >= 0) return count+r;
					}

				if (!*sp || (_maxChars != 0 && count > _maxChars)) break;

				// Check to see if this char is in one of the specified ranges
				gotMatch = false;

				for (i=0;i<(int)_matchPairArray.size();i++)
					{
					SWString	&sr = _matchPairArray[i];

					if (*sp >= sr[0] && *sp <= sr[1])
						{
						gotMatch = true;
						break;
						}
					}

				if (gotMatch != _matchFlag) break;
				}
			}

		return -1;
		}


void
SWRegExRange::dump()
		{
		int	i;

		printf("Range (min=%d  max=%d)\n", _minChars, _maxChars);
		for (i=0;i<(int)_matchPairArray.size();i++)
			{
			SWString	s = _matchPairArray[i];

			printf("-> %c-%c\n", s[0], s[1]);
			}
		}


void		
SWRegExRange::addMatchPair(sw_tchar_t f, sw_tchar_t l)	
		{
		sw_tchar_t	s[3];

		s[0] = f;
		s[1] = l;
		s[2] = 0;
		_matchPairArray.add(s);
		}


/**
*** Capture - matches the string only
**/
class SWRegExCapture : public SWRegExNode
		{
	public:
		SWRegExCapture(SWRegularExpression *owner, SWRegExCapture *startnode);
		
		virtual int		matches(const sw_tchar_t *s, bool matchStart);

		virtual void		dump();

		const SWString &	string() const	{ return _string; }

	private:
		SWRegExCapture	*_startNode;	// A link to the startnode
		SWRegExCapture	*_endNode;	// A link to the endnode
		const sw_tchar_t	*_endPoint;	// A pointer to the end point of the captured string
		SWString	_string;	// The captured string
		};


SWRegExCapture::SWRegExCapture(SWRegularExpression *owner, SWRegExCapture *startnode) :
    SWRegExNode(owner, Capture),
    _startNode(startnode),
    _endNode(NULL),
    _endPoint(NULL)
    	{
	if (_startNode != NULL)
			{
			// We are an end-node so update our start node to point to us
			// so that it knows we are valid
			_startNode->_endNode = this;
			}
	}


/**
*** A capture node always matches but records the endpoint
*** or start point of a string
**/
int
SWRegExCapture::matches(const sw_tchar_t *str, bool matchStart)
		{
		int	r;

		_string.empty();
		_endPoint = NULL;
		r = nextNodeMatches(str, false);
		if (r >= 0)
			{
			if (_startNode != NULL)
				{
				// We are an end node - record the endpoint before returning
				_endPoint = str;
				}
			else if (_endNode != NULL && _endNode->_endPoint != NULL)
				{
				// We are a correctly matched startpoint and our end point has recorded
				// a string pointer for us.
				_string = SWString(str, (int)(_endNode->_endPoint-str));
				}
			}

		return r;
		}


void
SWRegExCapture::dump()
		{
		printf("Capture (%s)\n", _startNode==NULL?"Start":"End");
		}




SWRegularExpression::SWRegularExpression(const SWString &re, bool greedy, bool caseSensitive) :
    _rootNode(0),
    _currNode(0),
    _greedy(greedy),
    _caseSensitive(caseSensitive)
		{
		compile(re);
		}


SWRegularExpression::~SWRegularExpression()
		{
		clear();
		}


const SWRegularExpression &	
SWRegularExpression::operator=(const SWString &re)
		{
		clear();
		compile(re);

		return *this;
		}

void
SWRegularExpression::clear()
		{
		SWRegExNode	*p = _rootNode, *np;

		_matchVec.clear();
		for (p=_rootNode;p!=NULL;p=np)
			{
			np = p->_next;
			delete p;
			}

		_rootNode = _currNode = NULL;
		}


void		
SWRegularExpression::addNode(SWRegExNode *np)
		{
		if (_rootNode == NULL) _rootNode = np;
		if (_currNode != NULL) _currNode->_next = np;
		_currNode = np;
		}


/**
*** PRIVATE function to compiler the regular expression string into
*** a tree of node which can be used to evaluate any strings for a
*** match
**/
void
SWRegularExpression::compile(const sw_tchar_t *pattern)
		{
		const sw_tchar_t	*pp=pattern;
		bool		gotMatch=false;
		int		minChars, maxChars, skip;
		SWString	str;
		SWRegExCapture	*startCaptureNode=NULL;

		for (pp=pattern;*pp;pp++)
			{
			if (pp == pattern && *pp == '^')
				{
				// Match start of string.
				addNode(new SWRegExAnchor(this, true));
				continue;
				}

			if (*pp == '$')
				{
				if (!str.isEmpty()) 
					{
					addNode(new SWRegExString(this, str));
					str.empty();
					}

				// Match end of string.
				addNode(new SWRegExAnchor(this, false));
				continue;
				}

			if (*pp == '(' && startCaptureNode == NULL)
				{
				if (!str.isEmpty()) 
					{
					addNode(new SWRegExString(this, str));
					str.empty();
					}

				addNode(startCaptureNode = new SWRegExCapture(this, NULL));
				continue;
				}

			if (*pp == ')' && startCaptureNode != NULL)
				{
				if (!str.isEmpty()) 
					{
					addNode(new SWRegExString(this, str));
					str.empty();
					}

				addNode(new SWRegExCapture(this, startCaptureNode));
				_matchVec.add(startCaptureNode);
				startCaptureNode = NULL;
				continue;
				}

			if (*pp == '.' || *pp == '\\' || *pp == '[')
				{
				// Match ANY character(s) in given range
				// or the range specified by the meta-char
				bool			wantMatch=true;		// True if we want to matching things in ranges
				sw_tchar_t			cmd=*pp;		// The command char
				bool			do_break=false;
				bool			do_return=false;
				bool			return_value=false;

				SWRegExRange		*rp;

				rp = new SWRegExRange(this);
				pp++;

				if (cmd == '.')
					{
					// Match ANY char - just set wantMatch to false
					// but don't add any character pairs
					rp->setMatchFlag(false);
					}
				else if (cmd == '\\')
					{
					switch (*pp)
						{
						case 'S':	// Non-White space
							rp->setMatchFlag(false);

						case 's':	// White space
							rp->addMatchPair(' ', ' ');
							rp->addMatchPair('\t', '\t');
							rp->addMatchPair('\n', '\n');
							rp->addMatchPair('\r', '\r');
							pp++;
							break;

						case 'D':	// Non-digit
							rp->setMatchFlag(false);

						case 'd':	// Digit
							rp->addMatchPair('0', '9');
							pp++;
							break;

						case 'W':	// Non-word
							rp->setMatchFlag(false);

						case 'w':	// Word
							rp->addMatchPair('A', 'Z');
							rp->addMatchPair('a', 'z');
							pp++;
							break;

						default:
							str += *pp;
							delete rp;
							continue;
						}
					}
				else if (cmd == '[')
					{
					sw_tchar_t	f, l;

					if (*pp == '^')
						{
						rp->setMatchFlag(false);
						pp++;
						}

					while (*pp && *pp != ']')
						{
						f = l = *pp++;

						if (f == '\\')
							{
							f = l = metachar(pp, skip);
							pp += skip;
							}

						if (*pp == '-')
							{
							// This is a range (e.g. [a-z])
							pp++;
							if (*pp && *pp != ']')
								{
								l = *pp;
								if (l == '\\')
									{
									l = metachar(pp, skip);
									pp += skip;
									}
								}
							}

						rp->addMatchPair(f, l);
						}

					if (*pp) pp++;
					}

				getQuantifier(pp, minChars, maxChars, skip);
				if (skip > 0)
					{
					int	count=0;
					
					// Move the pattern pointer forwards
					pp += skip;

					rp->setMinMax(minChars, maxChars);
					}

				if (!str.isEmpty())
					{
					addNode(new SWRegExString(this, str));
					str.empty();
					}

				addNode(rp);
				--pp;
				continue;
				}

			// Just check plain characters
			str += *pp;
			}

		if (!str.isEmpty())
			{
			addNode(new SWRegExString(this, str));
			str.empty();
			}
		}


void
SWRegularExpression::dump()
		{
		// For debugging purposes dump the match tree
		printf("--------------------Start of node dump--------------------\n");
		SWRegExNode	*np;

		for (np=_rootNode;np!=NULL;np=np->_next)
			{
			np->dump();
			}
		printf("---------------------End of node dump---------------------\n");
		}


/**
*** PRIVATE function to get the quantifier from the given string
***         *      Match 0 or more times
***         +      Match 1 or more times
***         ?      Match 1 or 0 times
***         {n}    Match exactly n times
***         {n,}   Match at least n times
***         {n,m}  Match at least n but not more than m times
*/
void
SWRegularExpression::getQuantifier(const sw_tchar_t *pp, int &min, int &max, int &skip)
		{
		min = 0;	// The minimum # of chars to match
		max = 0;	// The maximum # of chars to match (0=no limit)
		skip = 0;	// The # of pattern chars to skip
		switch (*pp)
			{
			case '+':
				min = 1;
			max = 0;
			skip = 1;
			break;

			case '*':
				min = 0;
			max = 0;
			skip = 1;
			break;

			case '?':
				min = 0;
			max = 1;
			skip = 1;
			break;

			case '{':
				{
				sw_tchar_t	tmp[32];
				int	i;

				pp++;
				skip++;

				// Read minimum
				for (i=0;i<31;i++,pp++)
					{
					if (*pp >= '0' && *pp <= '9') tmp[i] = *pp;
					else break;
					}

				if (i >= 31 || (*pp != ',' && *pp != '}'))
					{
					// Looks like a naff pattern - drop out
					max = min = skip = 0;
					break;
					}

				tmp[i] = 0;

				min = t_atoi(tmp);

				if (*pp == '}')
					{
					// End of quantifier - exact match
					max = min;
					skip += i+1;
					break;
					}

				// Got a comma - now try for max
				pp++;
				skip += i+1;

				for (i=0;i<31;i++,pp++)
					{
					if (*pp >= '0' && *pp <= '9') tmp[i] = *pp;
					else break;
					}

				if (i >= 31 || *pp != '}')
					{
					// Looks like a naff pattern - drop out
					max = min = skip = 0;
					break;
					}

				tmp[i] = 0;
				max = t_atoi(tmp);
				skip += i+1;
				}
			break;
			}
		}


/**
*** PRIVATE function for mrtachar handling
***
*** When a char is prefixed by a backslash this function
*** will return the real char!
**/
sw_tchar_t
SWRegularExpression::metachar(const sw_tchar_t *pp, int &skip)
		{
		sw_tchar_t	c, cmd=*pp;

		skip = 1;
		switch (cmd)
			{
			case 'a':	c='\a';	break;	// bell
			case 'n':	c='\n';	break;	// NL
			case 'r':	c='\r';	break;	// CR
			case 't':	c='\t';	break;	// Tab
			case 'f':	c='\f';	break;	// FormFeed

			case '0':	// Octal number representing a char
			case 'x':	// Hex number representing a char
				{
				int	i, m;

				m = cmd=='0'?8:16;
				c = 0;
				pp++;
				for (i=0;i<2;i++)
					{
					if (!*pp) break;

					if (*pp >= '0' && *pp <= (cmd=='x'?'9':'7'))
						{
						c = (c * m) + (*pp - '0');
						}
					else if (cmd == 'x' && *pp >= 'a' && *pp <= 'f') c = (c * m) + (*pp - 'a');
					else if (cmd == 'x' && *pp >= 'A' && *pp <= 'F') c = (c * m) + (*pp - 'A');
					else break;

					pp++;
					skip++;
					}

				if (i == 0) c = cmd=='0'?0:'x';
				}

			default: 	
			c = cmd;
			break;
			}

		return c;
		}


/**
*** Test to see if this RE matches the string given
**/
bool
SWRegularExpression::matches(const SWString &str)
		{
		// A null RE always matches
		if (_rootNode == NULL) return true;

		const sw_tchar_t	*s=str;

		while (*s)
			{
			if (_rootNode->matches(s, false) >= 0) return true;
			if (_rootNode->type() == SWRegExNode::Anchor)
				{
				if (((SWRegExAnchor *)_rootNode)->isStartAnchor()) break;
				}
			s++;
			}

		return false;
		}

SWString
SWRegularExpression::lastMatchString(int n)	
		{
		SWRegExCapture	*cp;

		// Should throw an exception if out of range
		cp = (SWRegExCapture *)_matchVec[n];

		return cp->string();
		}



