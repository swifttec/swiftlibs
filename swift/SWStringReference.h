/**
*** @file		SWStringReference.h
*** @brief		A string reference class.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWStringReference class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWStringReference_h__
#define __SWStringReference_h__

#include <swift/SWString.h>





/**
*** @brief	A string reference class.
***
*** SWStringReference is a SWString object that references an external
*** buffer, i.e. one that is not under the control of SWString. A SWStringReference
*** can act on an external buffer if multiple ways.
***
*** A buffer can be read-only via the const construction methods.
*** Any attempt to modify a read-only SWStringReference will result in an exception.
***
*** @author		Simon Sparkes
*** @see		SWString
**/
class SWIFT_DLL_EXPORT SWStringReference : public SWString
		{
public:
		/// Constructor - const reference of the given char buffer
		SWStringReference(const char *s);

		/// Constructor - const reference of the given wide-character buffer
		SWStringReference(const wchar_t *s);
		};




#endif
