/**
*** @file		SWFlags64.h
*** @brief		A class to represent a set of boolean flags.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWFlags64 class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWFlags64_h__
#define __SWFlags64_h__

#include <swift/swift.h>
#include <swift/SWBitSet.h>



// Forward declarations

/**
*** @brief A class to represent a set of booleans with a fixed length of 64-bits.
***
*** This class represents a set of 64 boolean flags in a single 64-bit integer.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWFlags64 : public SWBitSet
		{
public:
		/// Default constructor
		SWFlags64(sw_int64_t v=0) : m_flags(v)				{ }

		/// Copy constructor
		SWFlags64(const SWFlags64 &v) : SWBitSet(), m_flags(v.m_flags)	{ }

public: // Overrides for the SWBitSet class

		/**
		*** @brief	Set the specified bit to the specified value (default true)
		**/
		virtual void				setBit(int n, bool v=true);

		/**
		*** @brief	Get the specified bit
		**/
		virtual bool				getBit(int n) const;

		/**
		*** @brief	Get the number of bits in the bit field.
		**/
		virtual int					size() const					{ return 8 * sizeof(m_flags); }

		/**
		*** @brief	Return the fixed size flag (compatibilty with SWBitSet).
		**/
		virtual bool				isFixedSize() const				{ return true; }

		/**
		*** @brief	Clear the bit set.
		**/
		virtual void				clear()							{ m_flags = 0; }

		/**
		*** @brief	Set all the bits in this set to 0.
		**/
		virtual void				clearAllBits()					{ m_flags = 0; }

		/**
		*** @brief	Set all the bits in this set to 1.
		**/
		virtual void				setAllBits()					{ m_flags = (sw_int64_t)~0; }

		/**
		*** @brief	Toggle all the bits in this set to (1->0 or 0->1).
		**/
		virtual void				toggleAllBits()					{ m_flags ^= (sw_int64_t)~0; }

		/**
		*** @brief	Integer cast
		**/
		operator sw_int64_t() const							{ return m_flags; }

		/**
		*** @brief	Assignment operator
		**/
		SWFlags64 &	operator=(SWFlags64 &v)					{ m_flags = v.m_flags; return *this; }

		/**
		*** @brief	Assigns the bit set directly from the integer passed.
		***
		*** If the bit set is not of a fixed size it assumes the number of bits
		*** held by a native integer (which is the underlying storage unit).
		***
		*** If the bit set has a fixed size then only the relavent bits are set.
		***
		*** @param[in]	v	The integer value to assign to the underlying integer.
		**/
		SWFlags64 &	operator=(sw_int64_t v)					{ m_flags = v; return *this; }


		/// AND operator
		SWFlags64 &	operator&=(SWFlags64 &v)				{ m_flags &= v.m_flags; return *this; }

		/// OR operator
		SWFlags64 &	operator|=(SWFlags64 &v)				{ m_flags |= v.m_flags; return *this; }

		/// XOR operator
		SWFlags64 &	operator^=(SWFlags64 &v)				{ m_flags ^= v.m_flags; return *this; }


		/// AND operator (sw_int64_t)
		SWFlags64 &	operator&=(sw_int64_t v)				{ m_flags &= v; return *this; }

		/// OR operator (sw_int64_t)
		SWFlags64 &	operator|=(sw_int64_t v)				{ m_flags |= v; return *this; }

		/// XOR operator (sw_int64_t)
		SWFlags64 &	operator^=(sw_int64_t v)				{ m_flags ^= v; return *this; }


		/**
		*** @brief	Test to see if the specified bits are set.
		***
		*** @param	v		The value to test.
		*** @param	anyBits	If true then isSet will succeed if any of the supplied bits
		***					are set. If false (default) then isSet will succeed only if
		***					all the specified bits are set.
		**/
		bool		isSet(sw_int64_t v, bool anyBits=false) const;

		/**
		*** @brief	Set the specified bits
		***
		*** @param[in]	v	The flags to clear
		**/
		void		setBits(sw_int64_t v)						{ m_flags |= v; }


		/**
		*** @brief	Clear the specified bits
		***
		*** @param[in]	v	The flags to clear
		**/
		void		clearBits(sw_int64_t v)						{ m_flags &= ~v; }


public: // virtual overrides

		/// Override for SWObject writeToStream
		//virtual sw_status_t		writeToStream(SWOutputStream &stream) const;

		/// Override for SWObject readFromStream
		//virtual sw_status_t		readFromStream(SWInputStream &stream);


private:
		sw_int64_t	m_flags;	///< The flags
		};



#endif
