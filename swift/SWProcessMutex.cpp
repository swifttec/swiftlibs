/**
*** @file		SWProcessMutex.cpp
*** @brief		A process mutex implementation
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWProcessMutex class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <swift/SWProcessMutex.h>
#include <swift/SWGuard.h>
#include <swift/SWCString.h>
#include <swift/sw_printf.h>

	#if !defined(SW_USE_WINDOWS_MUTEXES)
		#include <xplatform/sw_mutex.h>
	#endif


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





// Constructor
SWProcessMutex::SWProcessMutex(const SWString &name, bool recursive) :
	SWMutex(recursive?SW_MUTEX_RECURSIVE:0),
	m_tguard(true),
	m_hMutex(0)
		{
		m_hMutex = sw_mutex_create(SW_MUTEX_TYPE_PROCESS|(recursive?SW_MUTEX_TYPE_RECURSIVE:0), name);
		if (m_hMutex == NULL) throw SW_EXCEPTION(SWMutexCreateException);
		}


// Destructor
SWProcessMutex::~SWProcessMutex()
		{
		sw_mutex_destroy(m_hMutex);
		m_hMutex = NULL;
		}


// Acquire the mutex
sw_status_t
SWProcessMutex::acquire(sw_uint64_t waitms)
		{
		sw_status_t		res=SW_STATUS_SUCCESS;

		res = m_tguard.acquire(waitms);
		if (res == SW_STATUS_SUCCESS)
			{
			res = sw_mutex_timedLock(m_hMutex, waitms);
			if (res == SW_STATUS_ALREADY_LOCKED)
				{
				throw SW_EXCEPTION(SWMutexAlreadyHeldException);
				}
			}

		return res;
		}


// Release the mutex
sw_status_t
SWProcessMutex::release()
		{
		sw_status_t		res=SW_STATUS_SUCCESS;

		res = sw_mutex_unlock(m_hMutex);
		if (res == SW_STATUS_NOT_OWNER)
			{
			throw SW_EXCEPTION(SWMutexNotHeldException);
			}
		else
			{
			// Now release the thread mutex
			res = m_tguard.release();
			}

		return res;
		}

// Release the mutex
bool
SWProcessMutex::hasLock()
		{
		SWMutexGuard	guard(m_tguard);

		return (sw_mutex_hasLock(m_hMutex) != 0);
		}


bool
SWProcessMutex::isLocked()
		{
		SWMutexGuard	guard(m_tguard);

		return (sw_mutex_isLocked(m_hMutex) != 0);
		}




