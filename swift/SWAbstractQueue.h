/**
*** @file		SWAbstractQueue.h
*** @brief		Base class for all queue classes.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWAbstractQueue class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWAbstractQueue_h__
#define __SWAbstractQueue_h__

#include <swift/swift.h>
#include <swift/SWThreadCondVar.h>
#include <swift/SWThreadMutex.h>
#include <swift/SWList.h>
#include <swift/SWFlags32.h>



SW_DEFINE_EXCEPTION(SWQueueException, SWException);						///< Generic queue exception
SW_DEFINE_EXCEPTION(SWQueueInactiveException, SWQueueException);		///< Operation attempted on inactive/disabled queue
SW_DEFINE_EXCEPTION(SWQueueInterruptedException, SWQueueException);		///< SWQueue operation interrupted
SW_DEFINE_EXCEPTION(SWQueueTimeoutException, SWQueueException);			///< SWQueue operation timed-out


/*
** Queue flags
*/
//#define SW_QUEUE_INTERRUPT				0x00000001		///< Flag to indicate interrupt condition
#define SW_QUEUE_WAITMODE				0x00000002		///< Flag to indicate get should suspend the thread on an empty queue
#define SW_QUEUE_DISABLE_WHEN_EMPTY		0x00000004		///< Flag to indicate the queue must be disabled when empty
#define SW_QUEUE_INTERRUPT_WHEN_EMPTY	0x00000008		///< Flag to indicate the queue must interrupted when empty
#define SW_QUEUE_OPERATION_ADD			0x10000000		///< Flag to indicate the ADD operation is permitted
#define SW_QUEUE_OPERATION_GET			0x20000000		///< Flag to indicate the GET operation is permitted
#define SW_QUEUE_OPERATION_PEEK			0x40000000		///< Flag to indicate the PEEK operation is permitted
#define SW_QUEUE_OPERATION_MASK			0xf0000000		///< Mask for queue operation flags

/// Full queue operation flags
#define SW_QUEUE_OPERATION_ALL			(SW_QUEUE_OPERATION_ADD|SW_QUEUE_OPERATION_GET|SW_QUEUE_OPERATION_PEEK)

/**
*** @brief An abstract queue class.
***
*** SWAbstractQueue forms the base of all queue classes within the SWIFT.
*** All queues should provide these basic queue functions even if
*** they are a null action.
***
*** A queue is basically an enhanced list and works like a FIFO.
*** A queue can be enabled and disabled. When disabled operations
*** on the queue will cause an exception.
***
*** A SWIFT queue also comes in two forms - a pointer-based queue and a
*** data queue. A pointer-based queue takes pointers to data blocks or objects
*** and stores them in an internal list. They are read off by a reader who is
*** responsible for deleting any memory associated with them.
***
*** A data queue takes <b>copies</b> of the the data added and stores the data
*** copy in an internal list. When read the data is copied back. This means that
*** a data queue has a copying overhead associated with it but has the advantage
*** that the data does not have to be held in memory and can be written to disk
*** instead. This can be advantageous when large amounts of data are to be queued
*** or if a queue is be cluster-aware (persistent if one node of a cluster fails).
***
*** Queue can also operate in two modes:
***
*** In Wait mode a get on an empty queue will suspend the calling thread until
*** data is available. In NoWait mode the queue will simply return without data
*** if no data is available.
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Safe when synchronized</td>
*** </tr>
*** </table>
***
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWAbstractQueue : public SWCollection
		{
protected:
		/// Protected constructor
		SWAbstractQueue();

public:
		/// Cirtual destructor
		virtual ~SWAbstractQueue();

		/**
		*** @brief Enable operations on the queue.
		***
		*** This method enables some or all operations on the queue according
		*** to the flags specified. Any attempt to perform a disabled operation
		*** on a queue will cause a SWQueueInactiveException.
		***
		*** Queue operations can be disabled using the disable method.
		***
		*** @param flags	Flags specifying which operations on the queue
		***					should be enabled. By default all operations on a queue are enabled.
		***					The caller can specify a combination of the following flags:
		***					- SW_QUEUE_OPERATION_ADD (elements can be added to the queue)
		***					- SW_QUEUE_OPERATION_GET (elements can be removed from the queue)
		***					- SW_QUEUE_OPERATION_PEEK (elements can be peeked from the queue)
		**/
		virtual void		enable(int flags=SW_QUEUE_OPERATION_ALL)=0;

		/**
		*** @brief Disable operations on the queue.
		***
		*** This method disables some or all operations on the queue according
		*** to the flags specified. Any attempt to perform a disabled operation
		*** on a queue will cause a SWQueueInactiveException.
		***
		*** Queue operations can be enabled using the enable method.
		***
		*** @param flags	Flags specifying which operations on the queue
		***					should be disabled. By default all operations on a queue are disabled.
		***					The caller can specify a combination of the following flags:
		***					- SW_QUEUE_OPERATION_ADD (elements can be added to the queue)
		***					- SW_QUEUE_OPERATION_GET (elements can be removed from the queue)
		***					- SW_QUEUE_OPERATION_PEEK (elements can be peeked from the queue)
		**/
		virtual void		disable(int flags=SW_QUEUE_OPERATION_ALL)=0;

		/**
		*** @brief Check to see if the queue operations are enabled?
		***
		*** The caller can check specific operations on a queue by overriding the
		*** flags.
		***
		*** @param flags	Flags specifying which parts of the queue
		***					should be disabled. By default all operations on a queue are disabled.
		***					The caller can specify a combination of the following flags:
		***					- SW_QUEUE_OPERATION_ADD (elements can be added to the queue)
		***					- SW_QUEUE_OPERATION_GET (elements can be removed from the queue)
		***					- SW_QUEUE_OPERATION_PEEK (elements can be peeked from the queue)
		**/
		virtual bool		isEnabled(int flags=SW_QUEUE_OPERATION_ALL) const;

		/**
		*** @brief	Shutdown/Disable operations on the queue.
		***
		*** Any attempt to perform an operation on a disabled queue
		*** will cause a SWQueueInactiveException.
		***
		*** @param[in]	disableReaderWhenEmpty	When true the queue is only disabled
		***										when the queue becomes empty. If false
		***										the queue is disabled immediately.
		**/
		virtual void		shutdown(bool disableReaderWhenEmpty=false)=0;
		
		/**
		*** @brief	Check to see if the queue is fully active/enabled (same as isEnabled).
		**/
		virtual bool		isActive() const	{ return isEnabled(); }

		/**
		*** Interupt any thread waiting on the queue.
		***
		*** By default interrupt any readers immediately (flag interruptReaderWhenEmpty=false).
		***
		*** If the interruptReaderWhenEmpty flag is true then the readers are interrupted
		*** only when the queue empties.
		***
		*** @param[in]	interruptReaderWhenEmpty	Indicates if the readers are interrupted immediately
		***											or only when the queue is empty.
		**/
		virtual void		interrupt(bool interruptReaderWhenEmpty=false)=0;

		/**
		*** @brief	Empty the queue of all messages.
		***
		*** @param emptyInactiveQueue	If false (default) then any attempt to empty the
		***								queue when it is inactive will result in an exception.
		***								If true 
		**/
		virtual void		clear(bool emptyInactiveQueue)=0;


		/**
		*** @brief	Return the number of threads currently waiting for data.
		**/
		int					waitingThreadCount() const	{ return m_waitingCount; }

public: // Overrides for SWCollection methods

		/// Clears the queue of all messages
		virtual void		clear()		{ clear(false); }

protected: // Overrides for SWCollection methods

		/// Gets an iterator for the queue.
		virtual	SWCollectionIterator *	getIterator(bool iterateFromEnd) const;

protected:
		SWFlags32		m_flags;			///< Various flags to indicate state.
		int				m_interruptCount;	///< The number of readers still to be interrupted
		int				m_waitingCount;		///< The number of readers waiting for data
		};



#endif
