/**
*** @file		SWLogMessageHandler.h
*** @brief		A class to define an abstract class for handling log messages.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWLogMessageHandler class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWLogMessageHandler_h__
#define __SWLogMessageHandler_h__

#include <swift/swift.h>
#include <swift/SWLogMessage.h>

#include <swift/SWLockableObject.h>




// Forward declarations
class SWLog;

/**
*** @brief		An abstract class defining a SWLogMessage handler
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWLogMessageHandler : public SWLockableObject
		{
friend class SWLog;

protected:
		/// Protected constructor
		SWLogMessageHandler();

public:
		/// Virtual destructor
		virtual ~SWLogMessageHandler();

		/**
		*** @brief Set the log format for this handler.
		***
		*** Thie method sets format string used to format log messages. If the format string
		*** is set to an empty string, the format from the SWLog object that calls this
		*** handler is used to format the message.
		***
		*** @param[in]	v	The format string.
		***
		*** @see SWLogMessage::format for details of the format string
		**/
		void				format(const SWString &v)		{ flush(); m_format = v; }

		/**
		*** @brief Get the default log format for all handlers associated with this log object.
		*** @see SWLogMessage::format for details of the format string
		**/
		const SWString &	format() const					{ return m_format; }

		/**
		*** @brief	Set the log level.
		***
		*** This method sets the maximum level (or type) of log messages that are sent to the 
		*** registered message handlers. Valid values for the parameter are specified in the 
		*** sw_log_defines.h file as SW_LOG_LEVEL_XXXX, where XXXX is one of FATAL, ERROR, INFO, etc
		***
		*** @param	v	The log level to set.
		***
		*** @return		The previous log level.
		**/
		sw_uint32_t			level(sw_uint32_t v)			{ flush(); sw_int32_t tmp=m_logLevel; m_logLevel = v & SW_LOG_LEVEL_MASK; return tmp; }

		/**
		*** @brief	Returns the current log level.
		*** @return	The current log level.
		**/
		sw_uint32_t			level() const					{ return m_logLevel; }

		/**
		*** @brief	Set the log type mask.
		***
		*** This method sets the type mask for this SWLog object. Any messages that do not
		*** have type codes that correspond to this bit field are not sent to the message
		*** handlers. This can be used to selectively mask out groups of messages, e.g. all
		*** messages of a particular type. The log message types are defined in sw_log_defines.h
		*** as SW_LOG_TYPE_XXXX.
		***
		*** @param	v	The log type mask to set.
		***
		*** @return		The previous log type mask.
		**/
		sw_uint32_t			typeMask(sw_uint32_t v)			{ flush(); sw_uint32_t tmp=m_typeMask; m_typeMask = v & SW_LOG_TYPE_MASK; return tmp; }

		/**
		*** @brief	Return the current log type mask.
		*** @return	The current type mask.
		**/
		sw_uint32_t			typeMask() const				{ return m_typeMask; }


		/**
		*** @brief	Set the log feature mask.
		***
		*** This method sets the feature mask for this SWLog object. Any messages that do not
		*** have feature codes that correspond to this bit field are not sent to the message
		*** handlers. This can be used to selectively mask out groups of messages, e.g. all
		*** messages from a single module.
		***
		*** @param	v	The feature mask to set.
		***
		*** @return		The previous feature mask.
		**/
		sw_uint64_t			featureMask(sw_uint64_t v)		{ flush(); sw_uint64_t tmp=m_featureMask; m_featureMask = v; return tmp; }

		/**
		*** @brief	Return the current log feature mask.
		*** @return	The current feature mask.
		**/
		sw_uint64_t			featureMask() const				{ return m_featureMask; }


		/**
		*** @brief	Filter log messages for handling.
		***
		*** This method compares the log message attributes (log level, message type
		*** and features) with the levels set for this handler and returns a boolean value
		*** indicating whether or not the message is to be included
		***
		*** The message is to be included under the following conditions:
		*** - The message log level is less than or equal to the handler log level.
		*** - The message type is zero or the message type matches at least one bit in the handler type mask
		*** - The message feature code is zero or the message feature code matches at least one bit in the handler feature code mask
		***
		*** If the message is to be included this method will return a value or true.
		***
		*** @param[in]	msg		The log message to check against this handler's parameters.
		***
		*** @return				true if this log message is to be handled, false if it is
		***						to be filtered out.
		**/
		bool				filter(const SWLogMessage &msg) const;
		
		/**
		*** @brief	Format a message into the string using the current message format.
		***
		*** This method formats the log message into a string by first of all finding the format
		*** string using the following algorithm:
		***
		*** - If specified use the local format for this handler.
		*** - If not use the format from the owning SWLog
		*** - Check each higher SWLog until a format is found, or no higher SWLog object available
		*** - Use a default value if no other format can be found.
		***
		*** Having got a format string this method then calls the SWLogMessage::format method to
		*** create the string.
		**/
		void				formatMsg(SWString &str, const SWLogMessage &msg) const;

protected: // Pure virtual methods that need to be overridden

		/**
		*** @brief	Called by a SWLog object when the handler is added to the SWLog object.
		***
		*** The handler should use this method to perform any initialisation required,
		*** opening log file, etc.
		***
		*** The handler should return a status code of SW_STATUS_SUCCESS if the operation
		*** is succesful, or an error code otherwise. If an error code is returned the SWLog
		*** object may chose not to add this handler.
		**/
		virtual sw_status_t	open()=0;

		/**
		*** @brief	Called by a SWLog object when a message needs to be processed.
		*** @param	lm		The log message object to be processed/reported.
		***
		*** The handler should return a status code of SW_STATUS_SUCCESS if the operation
		*** is succesful, or an error code otherwise. If an error code is returned the SWLog
		*** object may chose to close and remove this handler.
		**/
		virtual sw_status_t	process(const SWLogMessage &lm)=0;

		/**
		*** @brief	Called by a SWLog object when any messages need to be flushed to disk, etc
		***
		*** The handler should return a status code of SW_STATUS_SUCCESS if the operation
		*** is succesful, or an error code otherwise. If an error code is returned the SWLog
		*** object may chose to close and remove this handler.
		**/
		virtual sw_status_t	flush()=0;

		/**
		*** @brief	Called by a SWLog object when the handler is about to be removed from the SWLog object
		***
		*** The handler should use this call to close any open files, shutdown any connections
		*** and peform any cleanup required.
		***
		*** The handler should return a status code of SW_STATUS_SUCCESS if the operation
		*** is succesful, or an error code otherwise.
		**/
		virtual sw_status_t	close()=0;

protected: // Methods
		/// Set the owning SWLog
		void		owner(SWLog *pOwner)	{ m_pOwner = pOwner; }

		/// Get the owning SWLog (may return NULL)
		SWLog *		owner() const			{ return m_pOwner; }

protected: // Members
		sw_uint32_t	m_logLevel;		///< The current log level
		sw_uint32_t	m_typeMask;		///< The current log type mask
		sw_uint64_t	m_featureMask;	///< The current log feature mask
		SWString	m_format;		///< The default format for log output
		SWLog		*m_pOwner;		///< The SWLog object that owns this handler.
		};


inline bool
SWLogMessageHandler::filter(const SWLogMessage &msg) const
		{
		return (msg.level() <= m_logLevel && (msg.type() == 0 || (msg.type() & m_typeMask) != 0)  && (msg.feature() == 0 || (msg.feature() & m_featureMask) != 0));
		}





#endif
