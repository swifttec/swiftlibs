/**
*** @file		SWAbstractBuffer.cpp
*** @brief		General-purpose buffer
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWAbstractBuffer class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWAbstractBuffer.h>
#include <swift/SWException.h>
#include <swift/SWLocalMemoryManager.h>
#include <swift/SWInputStream.h>
#include <swift/SWOutputStream.h>

#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif






SWAbstractBuffer::SWAbstractBuffer(SWMemoryManager *pManager, sw_byte_t *pData, sw_size_t nbytes, sw_uint32_t flags)
		{
		m_pMemoryManager = pManager;
		m_pData = pData;
		m_capacity = 0;
		m_flags = flags;

		if (m_pMemoryManager == NULL && !isReference())
			{
			m_pMemoryManager = SWMemoryManager::getDefaultMemoryManager();
			}
		
		if (nbytes != 0)
			{
			if (isReference()) m_capacity = nbytes;
			else capacity(nbytes);
			}
		}


SWAbstractBuffer::~SWAbstractBuffer()
		{
		if (m_pMemoryManager != NULL && !isReference())
			{
			m_pMemoryManager->free(m_pData);
			m_pData = NULL;
			}
		}


SWAbstractBuffer::SWAbstractBuffer(const SWAbstractBuffer &other) :
	SWObject()
		{
		m_pMemoryManager = SWMemoryManager::getDefaultMemoryManager();
		m_pData = NULL;
		m_capacity = 0;
		m_flags = 0;

		*this = other;
		}


void
SWAbstractBuffer::setAllocationStrategy(sw_uint32_t flags)
		{
		m_flags = (m_flags & ~(AllocateExactAmount|AllocateDoubleCapacity)) | (flags & ((AllocateExactAmount|AllocateDoubleCapacity)));
		}
			


SWAbstractBuffer &
SWAbstractBuffer::operator=(const SWAbstractBuffer &other)
		{
		if (isReadOnly())
			{
			// We can't do this so throw an exception
			throw SW_EXCEPTION(SWBufferReadOnlyException);
			}

		if (other.data() == NULL)
			{
			clear();
			}
		else
			{
			memcpy(data(other.size()), other.data(), other.size());
			}

		size(other.size());

		return *this;
		}


void *
SWAbstractBuffer::data(sw_size_t nbytes, bool freeExtra)
		{
		capacity(nbytes, true, true, freeExtra);

		return m_pData;
		}


sw_size_t
SWAbstractBuffer::capacity() const
		{
		return m_capacity;
		}


sw_size_t
SWAbstractBuffer::size() const
		{
		return capacity();
		}


void
SWAbstractBuffer::clear()
		{
		if (capacity() > 0) memset(m_pData, 0, capacity());
		size(0);
		}



void
SWAbstractBuffer::size(sw_size_t size, bool preserveOldData, bool clearNewData, bool freeExtraMemory)
		{
		// Default version is mapped to call the capacity method
		capacity(size, preserveOldData, clearNewData, freeExtraMemory);
		}


void
SWAbstractBuffer::capacity(sw_size_t nbytes, bool preserveOldData, bool clearNewData, bool freeExtraMemory)
		{
		if (isReadOnly())
			{
			// We can't do this so throw an exception
			throw SW_EXCEPTION(SWBufferReadOnlyException);
			}

		sw_byte_t		*pNewData = NULL;
		sw_size_t		oldsize = m_capacity;
		sw_size_t		wanted = nbytes;

		// Force a minimum size
		sw_size_t	sizemod=16;

		if (isReference()) sizemod = 1;
		
		if (wanted < sizemod) wanted = sizemod;

		if (wanted > m_capacity && m_pMemoryManager == NULL)
			{
			throw SW_EXCEPTION(SWBufferException);
			}

		// Don't need to do anything!
		if (wanted == oldsize)
			{
			if (!(freeExtraMemory && wanted != m_capacity)) return;
			}

		if (wanted > m_capacity)
			{
			if (isFixedSize() || isReference())
				{
				unlock();
				throw SW_EXCEPTION(SWBufferOverflowException);
				}

			sw_size_t		newCapacity = m_capacity;

			// Calculate the new capacity according to the _increment selected
			// 0  = set capacity to size requested
			// <0 = double capacity until enough space
			// >0 = increment capacity by _increment until enough space
			if ((m_flags & AllocateExactAmount) != 0 || freeExtraMemory) newCapacity = wanted;
			else if ((m_flags & AllocateDoubleCapacity) != 0)
				{
				// Double in size until capacity reached
				if (newCapacity == 0) newCapacity = 1;
				while (wanted > newCapacity) newCapacity *= 2;
				}
			else
				{
				newCapacity = ((wanted + sizemod - 1) / sizemod) * sizemod;
				}

			pNewData = reinterpret_cast<sw_byte_t *>(m_pMemoryManager->realloc(m_pData, newCapacity));
			if (pNewData == NULL)
				{
				throw SW_EXCEPTION(SWMemoryAllocationException);
				}

			// Save the new (possibly unchanged) buffer
			m_pData = pNewData;
			m_capacity = newCapacity;
			}
		else if (wanted < m_capacity && freeExtraMemory && !isReference())
			{
			pNewData = reinterpret_cast<sw_byte_t *>(m_pMemoryManager->realloc(m_pData, wanted));
			if (pNewData == NULL)
				{
				throw SW_EXCEPTION(SWMemoryAllocationException);
				}

			// Save the new (possibly unchanged) buffer
			m_pData = pNewData;
			m_capacity = wanted;
			}

		// Clear out any new memory
		if (clearNewData)
			{
			if (!preserveOldData)
				{
				// Data preservation is false, zero is true
				// Clear the buffer
				memset(m_pData, 0, m_capacity);
				}
			else if (wanted > oldsize)
				{
				// Only clear out the new parts of the buffer
				memset(m_pData+oldsize, 0, wanted-oldsize);
				}
			}
		}


// write to the buffer from another buffer adjusting size if required
// returns the number of bytes copied
sw_size_t
SWAbstractBuffer::write(const void *from, sw_size_t nbytes, sw_size_t offset)
		{
		if (isReadOnly())
			{
			// We can't do this so throw an exception
			throw SW_EXCEPTION(SWBufferReadOnlyException);
			}

		if (nbytes > 0)
			{
			size(nbytes + offset, true, false, false);
			memcpy(m_pData+offset, from, nbytes);
			}

		return nbytes;
		}


// read from the buffer to another buffer
// returns the number of bytes copied
sw_size_t
SWAbstractBuffer::read(void *to, sw_size_t nbytes, sw_size_t offset) const
		{
		if ((nbytes + offset) > m_capacity)	
			{
			if (offset > m_capacity) nbytes = 0;
			else nbytes = m_capacity - offset;
			}

		if (nbytes > 0)
			{
			memcpy(to, m_pData+offset, nbytes);
			}

		return nbytes;
		}


// Compare the buffer to the ptr/len provided
int
SWAbstractBuffer::compareTo(const void *ptr, size_t len) const
		{
		sw_size_t	l1, l2, minlen;
		int			r;

		l1 = size();
		l2 = len;
		minlen = (l1<l2)?l1:l2;

		r = memcmp(m_pData, ptr, minlen);

		if (r != 0) return r;
		if (l1 == l2) return 0;
		return (l2>l1?-1:1);
		}


int
SWAbstractBuffer::compareTo(const SWAbstractBuffer &other) const
		{
		return compareTo(other.m_pData, other.size());
		}


void
SWAbstractBuffer::setContents(sw_byte_t v)
		{
		if (isReadOnly())
			{
			// We can't do this so throw an exception
			throw SW_EXCEPTION(SWBufferReadOnlyException);
			}

		memset(m_pData, v, m_capacity);
		}


sw_byte_t
SWAbstractBuffer::getByteAt(sw_size_t idx) const
		{
		if (idx >= m_capacity) throw SW_EXCEPTION(SWIndexOutOfBoundsException);
		return m_pData[idx];
		}
		

void
SWAbstractBuffer::setByteAt(sw_size_t idx, sw_byte_t v)
		{
		if (isReadOnly())
			{
			// We can't do this so throw an exception
			throw SW_EXCEPTION(SWBufferReadOnlyException);
			}

		if (idx >= m_capacity) throw SW_EXCEPTION(SWIndexOutOfBoundsException);
		m_pData[idx] = v;
		}
		

sw_status_t
SWAbstractBuffer::writeToStream(SWOutputStream &stream) const
		{
		sw_status_t		res=SW_STATUS_SUCCESS;

		// We write a buffer as follows:
		// Buffer Length (4 bytes)
		// Data
		res = stream.write((sw_uint32_t)size());
		if (res == SW_STATUS_SUCCESS) res = stream.writeData(m_pData, (sw_uint32_t)size());

		return res;
		}


sw_status_t
SWAbstractBuffer::readFromStream(SWInputStream &stream)
		{
		sw_status_t		res=SW_STATUS_SUCCESS;
		sw_uint32_t		datalen;

		res = stream.read(datalen);
		if (res == SW_STATUS_SUCCESS)
			{
			capacity(datalen, true, true, false);
			res = stream.readData(m_pData, datalen);
			if (res == SW_STATUS_SUCCESS)
				{
				size(datalen);
				}
			}

		return res;
		}


sw_byte_t &
SWAbstractBuffer::operator[](int i) const
		{
		if (isReadOnly())
			{
			// We can't do this so throw an exception
			throw SW_EXCEPTION(SWBufferReadOnlyException);
			}

		if (i < 0 || (size_t)i > m_capacity) throw SW_EXCEPTION(SWIndexOutOfBoundsException);
		return m_pData[i];
		}
		


#ifdef BUFFER_LOAD_SAVE
		// load and save to/from files
		//int		load(int fd, int nbytes, int offset);
		//int		load(SWFile &file, int nbytes, int offset);
		//int		save(int fd, int nbytes, int offset);
		//int		save(SWFile &file, int nbytes, int offset);


/**
*** Load this buffer by reading from the given file descriptor
**/
int
SWBuffer::load(File &file, int nbytes, int offset)
		{
		int	r=nbytes;

		if (nbytes > 0)
			{
			// Make sure we have enough space
			if ((nbytes + offset) > _size) setSize(nbytes + offset, true, false, false);
			r = file.read(m_pData+offset, nbytes);
			if (r != nbytes && r >= 0) setSize(r + offset, true, false, false);
			}

		return r;
		}


/**
*** Save this buffer by writing to the given file descriptor
**/
int
SWBuffer::save(File &file, int nbytes, int offset)
		{
		if ((nbytes + offset) > _size) nbytes = _size - offset;

		if (nbytes > 0) nbytes = file.write(m_pData+offset, nbytes);

		return nbytes;
		}


/**
*** Load this buffer by reading from the given file descriptor
**/
int
SWBuffer::load(int fd, int nbytes, int offset)
		{
		int	r=nbytes;

		if (nbytes > 0)
			{
			// Make sure we have enough space
			if ((nbytes + offset) > _size) setSize(nbytes + offset, true, false, false);
			r = ::read(fd, m_pData+offset, nbytes);
			if (r != nbytes && r >= 0) setSize(r + offset, true, false, false);
			}

		return r;
		}


/**
*** Save this buffer by writing to the given file descriptor
**/
int
SWBuffer::save(int fd, int nbytes, int offset)
		{
		if ((nbytes + offset) > _size) nbytes = _size - offset;

		if (nbytes > 0) nbytes = ::write(fd, m_pData+offset, nbytes);

		return nbytes;
		}


#endif // BUFFER_LOAD_SAVE




