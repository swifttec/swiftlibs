/****************************************************************************
**	sw_trace.h
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#ifndef __sw_trace_h__
#define __sw_trace_h__

	SWIFT_DLL_EXPORT void		sw_trace(const char *fmt, ...);

	#if defined(SW_BUILD_DEBUG)

		#ifndef __swift_h__
			#include <swift/swift.h>
		#endif

		#if !defined(SW_BUILD_MFC)
			#ifndef TRACE
				#define TRACE		sw_trace
			#endif

			#define SW_TRACE	sw_trace
		#else
			#define SW_TRACE	TRACE
		#endif

	#else // defined(SW_BUILD_DEBUG)

		#ifndef TRACE
			#define TRACE		if (false) sw_trace
		#endif

		#define SW_TRACE	if (false) sw_trace

	#endif // defined(SW_BUILD_DEBUG)

	#define SW_TRACE_LOCATION()	sw_trace("At line %d of %s\n", __LINE__, __FILE__)

#endif
