/**
*** @file		SWHashMap.cpp
*** @brief		A template map class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWHashMap class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWHashMap_cpp__
#define __SWHashMap_cpp__

#include <swift/SWHashMap.h>
#include <swift/SWHashCode.h>
#include <swift/SWFixedSizeMemoryManager.h>
#include <swift/SWLocalMemoryManager.h>




template<class K, class V>
SWHashMap<K,V>::SWHashMap(int initialCapacity, float loadFactor, bool autoRehash) :
	m_pBucketMemoryManager(0),
	m_pElementMemoryManager(0),
	m_elemsize(0),
	m_autoRehash(autoRehash),
    m_loadFactor(loadFactor),
    m_bucketArray(0),
	m_bucketCount(initialCapacity)
		{
		// Use new to get the element size instead of using sizeof
		//delete new (m_elemsize) SWHashMapEntry<K,V>;
		m_elemsize = sizeof(SWHashMapEntry<K,V>);

		// Try to ensure good alignment internally
		m_elemsize = ((m_elemsize + sizeof(void *) - 1) / sizeof(void *)) * sizeof(void *);

		m_pElementMemoryManager = new SWFixedSizeMemoryManager(m_elemsize);	// Use JIT block sizing

		m_pBucketMemoryManager = SWLocalMemoryManager::getDefaultMemoryManager();

		m_bucketArray = (Bucket *)m_pBucketMemoryManager->malloc(m_bucketCount * sizeof(Bucket));

		int		i;
		Bucket	*pBucket;

		for (i=0,pBucket=m_bucketArray;i<m_bucketCount;i++,pBucket++)
			{
			pBucket->head = pBucket->tail = NULL;
			}
		}


template<class K, class V>
SWHashMap<K,V>::SWHashMap(const SWHashMap<K, V> &other) :
	m_pBucketMemoryManager(0),
	m_pElementMemoryManager(0),
	m_elemsize(0),
	m_autoRehash(other.m_autoRehash),
    m_loadFactor(other.m_loadFactor),
    m_bucketArray(0),
	m_bucketCount(other.m_bucketCount)
		{
		// Use new to get the element size instead of using sizeof
		//delete new (m_elemsize) SWHashMapEntry<K,V>;
		m_elemsize = sizeof(SWHashMapEntry<K,V>);

		// Try to ensure good alignment internally
		m_elemsize = ((m_elemsize + sizeof(void *) - 1) / sizeof(void *)) * sizeof(void *);

		m_pElementMemoryManager = new SWFixedSizeMemoryManager(m_elemsize);	// Use JIT block sizing

		m_pBucketMemoryManager = SWLocalMemoryManager::getDefaultMemoryManager();

		m_bucketArray = (Bucket *)m_pBucketMemoryManager->malloc(m_bucketCount * sizeof(Bucket));

		int		i;
		Bucket	*pBucket;

		for (i=0,pBucket=m_bucketArray;i<m_bucketCount;i++,pBucket++)
			{
			pBucket->head = pBucket->tail = NULL;
			}

		*this = other;
		}


template<class K, class V> SWHashMap<K, V> &
SWHashMap<K,V>::operator=(const SWHashMap<K, V> &other)
		{
		SWIterator< SWHashMapEntry<K,V> >	it=other.iterator();
		SWHashMapEntry<K,V>					*pEntry;

		clear();
		while (it.hasNext())
			{
			pEntry = it.next();
			put(pEntry->key(), pEntry->value());
			}

		return *this;
		}


template<class K, class V>
SWHashMap<K,V>::~SWHashMap()
		{
		clear();
	
		m_pBucketMemoryManager->free(m_bucketArray);

		delete m_pElementMemoryManager;
		}

template<class K, class V> void
SWHashMap<K,V>::clear()
		{
		SWWriteGuard	guard(this);	// Lock the object for the duration
		int				i;
		Bucket			*pBucket;

		// Clear out the objects from each list
		for (i=0,pBucket=m_bucketArray;i<m_bucketCount;i++,pBucket++)
			{
			if (pBucket->head != NULL)
				{
				SWHashMapEntry<K,V>	*pElement=pBucket->head, *pNextElement;

				while (pElement != NULL)
					{
					pNextElement = pElement->next;

					delete pElement;
					m_pElementMemoryManager->free(pElement);

					pElement = pNextElement;
					}
				}

			pBucket->head = pBucket->tail = NULL;
			}

		// Inform the collection that the collection has changed
		SWCollection::setSize(0);
		}


template<class K, class V> void
SWHashMap<K,V>::rehash()
		{
		SWWriteGuard	guard(this);
		Bucket			*pOldBucketArray=m_bucketArray;
		int				oldBucketCount=m_bucketCount;
		int				i;
		Bucket			*pBucket;

		// Update the size and calculate the new threshold
		m_bucketCount = m_bucketCount * 2 + 1;
		m_threshold = (int)(m_loadFactor * (float)m_bucketCount);

		// Create and initialise a new bucket array
		m_bucketArray = (Bucket *)m_pBucketMemoryManager->malloc(m_bucketCount * sizeof(Bucket));

		for (i=0,pBucket=m_bucketArray;i<m_bucketCount;i++,pBucket++)
			{
			pBucket->head = pBucket->tail = NULL;
			}

		// Run through the old buckets and re-insert all the elements
		for (i=0,pBucket=pOldBucketArray;i<oldBucketCount;i++,pBucket++)
			{
			if (pBucket->head != NULL)
				{
				SWHashMapEntry<K,V>	*pElement=pBucket->head, *pNextElement;

				while (pElement != NULL)
					{
					pNextElement = pElement->next;
					addEntry(pElement);
					pElement = pNextElement;
					}
				}
			}
		
		m_pBucketMemoryManager->free(pOldBucketArray);
		}


template<class K, class V> SWHashMapEntry<K,V> *
SWHashMap<K,V>::getMapEntry(const K &key) const
		{
		Bucket				*pBucket=&m_bucketArray[bucket(key)];
		SWHashMapEntry<K,V>	*pFoundEntry=NULL;

		if (pBucket != NULL)
			{
			SWHashMapEntry<K,V>	*pEntry=pBucket->head;

			while (pEntry != NULL)
				{
				if (pEntry->key() == key)
					{
					pFoundEntry = pEntry;
					break;
					}

				// Since we keep the list ordered we can break out early
				if (key < pEntry->key()) break;

				pEntry = pEntry->next;
				}
			}

		return pFoundEntry;
		}


template<class K, class V> void
SWHashMap<K,V>::addEntry(SWHashMapEntry<K,V> *pNewEntry)
		{
		Bucket	*pBucket=&m_bucketArray[bucket(pNewEntry->key())];

		if (pBucket->tail == NULL)
			{
			pBucket->head = pBucket->tail = pNewEntry;
			pNewEntry->next = pNewEntry->prev = NULL;
			}
		else
			{
			// For search efficiency we keep an ordered list, so
			// we must insert the element in the appropriate place in the list
			SWHashMapEntry<K,V>	*pEntry=pBucket->head;

			while (pEntry != NULL)
				{
				if (pNewEntry->key() < pEntry->key())
					{
					pNewEntry->prev = pEntry->prev;
					pNewEntry->next = pEntry;
					if (pEntry->prev != NULL) pEntry->prev->next = pNewEntry;
					pEntry->prev = pNewEntry;

					if (pBucket->head == pEntry) pBucket->head = pNewEntry;
					break;
					}

				pEntry = pEntry->next;
				}

			if (pEntry == NULL)
				{
				// No entry was found to insert in front of so we add the entry
				// to the end of the list.
				pBucket->tail->next = pNewEntry;
				pNewEntry->prev = pBucket->tail;
				pNewEntry->next = NULL;
				pBucket->tail = pNewEntry;
				}
			}
		}


template<class K, class V> bool
SWHashMap<K,V>::put(const K &key, const V &value, V *pOldValue)
		{
		SWWriteGuard		guard(this);
		SWHashMapEntry<K,V>	*pEntry=getMapEntry(key);
		bool				res=false;

		if (pEntry == NULL)
			{
			// There is no map entry yet, so we must create a new one
			void				*p;

			p = m_pElementMemoryManager->malloc(m_elemsize);
			if (p == NULL) throw SW_EXCEPTION(SWMemoryException);
			pEntry = new (p) SWHashMapEntry<K,V>(key, value);

			addEntry(pEntry);

			// Inform the collection that the collection has changed
			incSize();

			if (m_autoRehash && (int)size() > m_threshold)
				{
				rehash();
				}

			res = false;
			}
		else
			{
			// We must overwrite the existing map entry
			if (pOldValue != NULL) *pOldValue = pEntry->value();
			pEntry->value() = value;

			res = true;
			}

		return res;
		}



template<class K, class V> bool
SWHashMap<K,V>::get(const K &key, V &value) const
		{
		SWReadGuard			guard(this);
		SWHashMapEntry<K,V>	*pEntry=getMapEntry(key);

		if (pEntry != NULL ) value = pEntry->value();
		
		return (pEntry != NULL);
		}


template<class K, class V> void
SWHashMap<K,V>::removeEntry(Bucket *pBucket, SWHashMapEntry<K,V> *pEntry)
		{
		SWHashMapEntry<K,V>	*n, *p;

		n = pEntry->next;
		p = pEntry->prev;
		if (pBucket->head == pEntry) pBucket->head = pEntry->next;
		if (pBucket->tail == pEntry) pBucket->tail = pEntry->prev;
		if (n) n->prev = pEntry->prev;
		if (p) p->next = pEntry->next;

		// Destruct and free up the memory used by the entry
		delete pEntry;
		m_pElementMemoryManager->free(pEntry);

		SWCollection::decSize();
		}


template<class K, class V> bool
SWHashMap<K,V>::remove(const K &key, V *pValue)
		{
		SWWriteGuard		guard(this);
		SWHashMapEntry<K,V>	*pEntry=getMapEntry(key);

		if (pEntry != NULL)
			{
			if (pValue != NULL) *pValue = pEntry->value();

			removeEntry(&m_bucketArray[bucket(key)], pEntry);
			}
		
		return (pEntry != NULL);
		}



template<class K, class V> bool
SWHashMap<K,V>::containsKey(const K &key) const
		{
		return (getMapEntry(key) != NULL);
		}



template<class K, class V> bool
SWHashMap<K,V>::containsValue(const V &value) const
		{
		SWWriteGuard	guard(this);
		SWIterator<V>	it=values();
		V				*pValue;
		bool			res=false;

		while (!res && it.hasNext())
			{
			pValue = it.next();
			res = (*pValue == value);
			}

		return res;
		}



template<class K, class V> SWIterator< SWHashMapEntry<K,V> >
SWHashMap<K,V>::iterator(bool iterateFromEnd) const
		{
		SWIterator< SWHashMapEntry<K,V> >	res;

		initIterator(res, getIterator(ITER_ENTRIES, iterateFromEnd));

		return res;
		}



template<class K, class V> SWIterator<K>
SWHashMap<K,V>::keys(bool iterateFromEnd) const
		{
		SWIterator<K>	res;

		initIterator(res, getIterator(ITER_KEYS, iterateFromEnd));

		return res;
		}



template<class K, class V> SWIterator<V>
SWHashMap<K,V>::values(bool iterateFromEnd) const
		{
		SWIterator<V>	res;

		initIterator(res, getIterator(ITER_VALUES, iterateFromEnd));

		return res;
		}


template<class K, class V> int
SWHashMap<K,V>::bucket(const K &key) const
		{
		SWHashCode	hk(key);

		return (hk.hashcode() % m_bucketCount);
		}



/*******************************************************************************
**	SWHashMapIterator Implementaion (internal class)
*******************************************************************************/


/**
*** Actual iterator for a hash table
**/
template<class K, class V>
class SWHashMapIterator : public SWCollectionIterator
		{
friend class SWHashMap<K,V>;

public:
		SWHashMapIterator(SWHashMap<K,V> *ht, int type, bool iterateFromEnd);

public:

		virtual bool		hasCurrent();
		virtual bool		hasNext();
		virtual bool		hasPrevious();

		virtual void *		current();
		virtual void *		next();
		virtual void *		previous();

		virtual bool		remove(void *pOldData);
		virtual bool		add(void *data, SWAbstractIterator::AddLocation where);
		virtual bool		set(void *data, void *pOldData);

		SWHashMap<K,V> *	collection()	{ return (SWHashMap<K,V> *)m_pCollection; }

private:
		void				findNextElement(SWHashMapEntry<K,V> *pElement, int bucket, SWHashMapEntry<K,V> *&pNextElement, int &nextbucket);
		void				findPrevElement(SWHashMapEntry<K,V> *pElement, int bucket, SWHashMapEntry<K,V> *&pPrevElement, int &prevbucket);

private:
		int						m_type;		// The iterator type (0=MapEntry, 1=Key, 2=Value)
		int						m_currBucketNo;	// The bucket number / chain number
		int						m_nextBucketNo;	// The bucket number / chain number
		int						m_prevBucketNo;	// The bucket number / chain number
		SWHashMapEntry<K,V>		*m_curr;	// The "current" element
		SWHashMapEntry<K,V>		*m_next;	// The next element
		SWHashMapEntry<K,V>		*m_prev;	// The previous element
		};



template<class K, class V> SWCollectionIterator *
SWHashMap<K,V>::getIterator(bool iterateFromEnd) const
		{
		return new SWHashMapIterator<K,V>((SWHashMap<K,V> *)this, ITER_ENTRIES, iterateFromEnd);
		}


template<class K, class V> SWCollectionIterator *
SWHashMap<K,V>::getIterator(SWHashMapIteratorType type, bool iterateFromEnd) const
		{
		return new SWHashMapIterator<K,V>((SWHashMap<K,V> *)this, type, iterateFromEnd);
		}





template<class K, class V>
SWHashMapIterator<K,V>::SWHashMapIterator(SWHashMap<K,V> *ht, int type, bool iterateFromEnd) :
    SWCollectionIterator(ht),
	m_type(type),
    m_curr(NULL),
    m_next(NULL),
    m_prev(NULL),
	m_currBucketNo(-1),
	m_nextBucketNo(-1),
	m_prevBucketNo(-1)
		{
		SWReadGuard				guard(m_pCollection);
		typename SWHashMap<K,V>::Bucket	*pBucket;
		int						i;


		m_modcount = collection()->getModCount();

		if (iterateFromEnd)
			{
			// Look for the last non-empty bucket
			i = ht->m_bucketCount - 1;
			pBucket = &ht->m_bucketArray[i];
			while (i >= 0)
				{
				if (pBucket->tail != NULL)
					{
					m_prev = pBucket->tail;
					m_prevBucketNo = i;
					break;
					}

				i--;
				pBucket--;
				}
			}
		else
			{
			// Look for the first non-empty bucket
			for (i=0,pBucket=ht->m_bucketArray;i<ht->m_bucketCount;i++,pBucket++)
				{
				if (pBucket->head != NULL)
					{
					m_next = pBucket->head;
					m_nextBucketNo = i;
					break;
					}
				}
			}
		}


template<class K, class V> bool
SWHashMapIterator<K,V>::hasCurrent()
		{
		SWReadGuard	guard(m_pCollection);

		checkForComodification();

		return (m_curr != NULL);
		}


template<class K, class V> bool		
SWHashMapIterator<K,V>::hasNext()
		{
		SWReadGuard	guard(m_pCollection);

		checkForComodification();

		return (m_next != NULL);
		}


template<class K, class V> bool		
SWHashMapIterator<K,V>::hasPrevious()
		{
		SWReadGuard	guard(m_pCollection);

		checkForComodification();

		return (m_prev != NULL);
		}


template<class K, class V> void *
SWHashMapIterator<K,V>::current()
		{
		SWReadGuard	guard(m_pCollection);

		checkForComodification();

		if (m_curr == NULL) throw SW_EXCEPTION(SWNoSuchElementException);

		void	*res;

		if (m_type == SWHashMap<K,V>::ITER_VALUES) res = (void *)&(m_curr->value());
		else if (m_type == SWHashMap<K,V>::ITER_KEYS) res = (void *)&(m_curr->key());
		else res = m_curr;
		
		return res;
		}


template<class K, class V> void
SWHashMapIterator<K,V>::findNextElement(SWHashMapEntry<K,V> *pElement, int bucket, SWHashMapEntry<K,V> *&pNextElement, int &nextbucket)
		{
		if (pElement->next != NULL)
			{
			pNextElement = pElement->next;
			nextbucket = bucket;
			}
		else
			{
			// Look for the next non-empty bucket
			typename SWHashMap<K,V>::Bucket	*pBucket;
			int							i;
			bool						found=false;

			i = bucket+1;
			pBucket = &collection()->m_bucketArray[i];
			while (i < collection()->m_bucketCount)
				{
				if (pBucket->head != NULL)
					{
					pNextElement = pBucket->head;
					nextbucket = i;
					found = true;
					break;
					}

				i++;
				pBucket++;
				}

			if (!found)
				{
				pNextElement = NULL;
				nextbucket = -1;
				}
			}
		}


template<class K, class V> void
SWHashMapIterator<K,V>::findPrevElement(SWHashMapEntry<K,V> *pElement, int bucket, SWHashMapEntry<K,V> *&pPrevElement, int &prevbucket)
		{
		if (pElement->prev != NULL)
			{
			pPrevElement = pElement->prev;
			prevbucket = bucket;
			}
		else
			{
			// Look for the next non-empty bucket
			typename SWHashMap<K,V>::Bucket	*pBucket;
			int							i;
			bool						found=false;

			i = bucket-1;
			pBucket = &collection()->m_bucketArray[i];
			while (i >= 0)
				{
				if (pBucket->tail != NULL)
					{
					pPrevElement = pBucket->tail;
					prevbucket = i;
					found = true;
					break;
					}

				i--;
				pBucket--;
				}

			if (!found)
				{
				pPrevElement = NULL;
				prevbucket = -1;
				}
			}
		}


template<class K, class V> void *	
SWHashMapIterator<K,V>::next()
		{
		SWReadGuard	guard(m_pCollection);

		checkForComodification();

		if (m_next == NULL) throw SW_EXCEPTION(SWNoSuchElementException);

		m_curr = m_next;
		m_currBucketNo = m_nextBucketNo;

		findNextElement(m_curr, m_currBucketNo, m_next, m_nextBucketNo);
		findPrevElement(m_curr, m_currBucketNo, m_prev, m_prevBucketNo);

		void	*res;

		if (m_type == SWHashMap<K,V>::ITER_VALUES) res = (void *)&(m_curr->value());
		else if (m_type == SWHashMap<K,V>::ITER_KEYS) res = (void *)&(m_curr->key());
		else res = m_curr;
		
		return res;
		}


template<class K, class V> void *	
SWHashMapIterator<K,V>::previous()
		{
		SWReadGuard	guard(m_pCollection);

		checkForComodification();

		if (m_prev == NULL) throw SW_EXCEPTION(SWNoSuchElementException);

		m_curr = m_prev;
		m_currBucketNo = m_prevBucketNo;

		findNextElement(m_curr, m_currBucketNo, m_next, m_nextBucketNo);
		findPrevElement(m_curr, m_currBucketNo, m_prev, m_prevBucketNo);

		void	*res;

		if (m_type == SWHashMap<K,V>::ITER_VALUES) res = (void *)&(m_curr->value());
		else if (m_type == SWHashMap<K,V>::ITER_KEYS) res = (void *)&(m_curr->key());
		else res = m_curr;
		
		return res;
		}

template<class K, class V> bool
SWHashMapIterator<K,V>::remove(void *pOldData)
		{
		SWWriteGuard	guard(m_pCollection);

		checkForComodification();

		if (m_curr == NULL) throw SW_EXCEPTION(SWNoSuchElementException);

		if (pOldData != NULL)
			{
			
			if (m_type == SWHashMap<K,V>::ITER_VALUES) *(V *)pOldData = m_curr->value();
			else if (m_type == SWHashMap<K,V>::ITER_KEYS) *(K *)pOldData = m_curr->key();
			else *(SWHashMapEntry<K,V> *)pOldData = *m_curr;
			}

		collection()->removeEntry(&(collection()->m_bucketArray[m_currBucketNo]), m_curr);
		m_curr = NULL;
		m_modcount = collection()->getModCount();

		return true;
		}


template<class K, class V> bool
SWHashMapIterator<K,V>::add(void * /*data*/, SWAbstractIterator::AddLocation /*where*/)
		{
		bool	res=false;

		if (!res) throw SW_EXCEPTION(SWUnsupportedOperationException);

		return res;
		}


template<class K, class V> bool
SWHashMapIterator<K,V>::set(void * /*data*/, void * /*pOldData*/)
		{
		bool	res=false;

		if (!res) throw SW_EXCEPTION(SWUnsupportedOperationException);

		return res;
		}




#endif
