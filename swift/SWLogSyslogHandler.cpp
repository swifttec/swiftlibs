/**
*** @file		SWLogSyslogHandler.cpp
*** @brief		Internal listener for the Log class which
***				writes to syslogd or it's equivalent.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWLogSyslogHandler class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




#include "stdafx.h"


#include <swift/SWLog.h>
#include <swift/SWLogSyslogHandler.h>

#if !defined(SW_PLATFORM_WINDOWS)
	#include <unistd.h>
	#include <syslog.h>
#endif


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





SWLogSyslogHandler::SWLogSyslogHandler()
		{
#if defined(SW_PLATFORM_WINDOWS)
		m_eventHandle = 0;
#endif

		// Override the default format
		format("${msg}");
		}

SWLogSyslogHandler::~SWLogSyslogHandler()
		{
		close();
		}


sw_status_t
SWLogSyslogHandler::open()
		{
		// Any opens are put off until the process method
		// so we can pick up the app name.

		return SW_STATUS_SUCCESS;
		}


sw_status_t
SWLogSyslogHandler::process(const SWLogMessage &lm)
		{
		if (m_appname != lm.application())
			{
			if (m_appname != _T("")) close();
			m_appname = lm.application();
#if defined(SW_PLATFORM_WINDOWS)
			m_eventHandle = RegisterEventSource(NULL, m_appname);
#else
			// Unix syslog
			openlog(m_appname, LOG_PID|LOG_CONS, LOG_LOCAL1);
#endif
			}

		SWString	str;

		formatMsg(str, lm);

#if defined(SW_PLATFORM_WINDOWS)
		if (m_eventHandle != 0)
			{
			const TCHAR *sa[1];
			
			sa[0] = str;

			int	et = EVENTLOG_ERROR_TYPE;

			switch (lm.type())
				{
				case SW_LOG_TYPE_STDERR:	et = EVENTLOG_INFORMATION_TYPE;		break;
				case SW_LOG_TYPE_STDOUT:	et = EVENTLOG_INFORMATION_TYPE;		break;
				case SW_LOG_TYPE_SUCCESS:	et = EVENTLOG_SUCCESS;				break;
				case SW_LOG_TYPE_FATAL:		et = EVENTLOG_ERROR_TYPE;			break;
				case SW_LOG_TYPE_ERROR:		et = EVENTLOG_ERROR_TYPE;			break;
				case SW_LOG_TYPE_WARNING:	et = EVENTLOG_WARNING_TYPE;			break;
				case SW_LOG_TYPE_INFO:		et = EVENTLOG_INFORMATION_TYPE;		break;
				case SW_LOG_TYPE_DEBUG:		et = EVENTLOG_INFORMATION_TYPE;		break;
				case SW_LOG_TYPE_ALERT:		et = EVENTLOG_ERROR_TYPE;			break;
				case SW_LOG_TYPE_NOTICE:	et = EVENTLOG_WARNING_TYPE;			break;
				case SW_LOG_TYPE_CRITICAL:	et = EVENTLOG_ERROR_TYPE;			break;
				case SW_LOG_TYPE_EMERGENCY:	et = EVENTLOG_ERROR_TYPE;			break;
				}

			ReportEvent(m_eventHandle,	// event log handle 
				(WORD)et,	// event type
				(WORD)lm.category(),	// category zero 
				lm.id(),		// event identifier 
				NULL,			// no user security identifier 
				1,			// one substitution string 
				0,			// no data 
				sa,			// pointer to string array 
				NULL);			// pointer to data 
			}

#else
		int	s = LOG_NOTICE;

		switch (lm.type())
			{
			case SW_LOG_TYPE_STDERR:	s = LOG_INFO;		break;
			case SW_LOG_TYPE_STDOUT:	s = LOG_INFO;		break;
			case SW_LOG_TYPE_SUCCESS:	s = LOG_INFO;		break;
			case SW_LOG_TYPE_FATAL:		s = LOG_ALERT;		break;
			case SW_LOG_TYPE_ERROR:		s = LOG_ERR;		break;
			case SW_LOG_TYPE_WARNING:	s = LOG_WARNING;	break;
			case SW_LOG_TYPE_INFO:		s = LOG_INFO;		break;
			case SW_LOG_TYPE_DEBUG:		s = LOG_DEBUG;		break;
			case SW_LOG_TYPE_ALERT:		s = LOG_ALERT;		break;
			case SW_LOG_TYPE_NOTICE:	s = LOG_NOTICE;		break;
			case SW_LOG_TYPE_CRITICAL:	s = LOG_CRIT;		break;
			case SW_LOG_TYPE_EMERGENCY:	s = LOG_EMERG;		break;
			}

		syslog(s|LOG_LOCAL1, str);
#endif

		return SW_STATUS_SUCCESS;
		}

sw_status_t
SWLogSyslogHandler::close()
		{
#if defined(SW_PLATFORM_WINDOWS)
		// For now do nothing, later call event log
		if (m_eventHandle != 0) DeregisterEventSource(m_eventHandle); 
		m_eventHandle = 0;
#else
		closelog();
#endif
		m_appname = "";

		return SW_STATUS_SUCCESS;
		}


sw_status_t
SWLogSyslogHandler::flush()
		{
		return SW_STATUS_SUCCESS;
		}




