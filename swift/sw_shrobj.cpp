/**
*** @file		sw_shrobj.cpp
*** @brief		Implement various functions which are missing from a given platform.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the various functions missing from certain platforms.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/sw_printf.h>
#include <swift/SWString.h>
#include <swift/SWSharedMemoryManager.h>
#include <swift/SWThreadMutex.h>
#include <swift/SWGuard.h>


#if defined(SW_USE_SOLARIS_MUTEXES)
	#include <thread.h>
	#include <synch.h>

	typedef mutex_t				sw_shrobj_mutex_t;
#elif defined(SW_USE_POSIX_MUTEXES)
	#include <pthread.h>

	typedef pthread_mutex_t		sw_shrobj_mutex_t;
#endif


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




/*
** Record is stored as follows:
**		Header	16 bytes
**		Name	namelen padded to sizeof(void *)
**		Data	datalen padded to sizeof(void *)
*/
typedef struct sw_shrobj_header
		{
		sw_uint32_t		magic;		// The record magic number
		sw_uint32_t		next;		// The offset of the next record
		sw_uint32_t		prev;		// The offset of the next record
		sw_uint16_t		namelen;	// The length of the name (bytes)
		sw_uint16_t		datalen;	// The length of the data (bytes)
		sw_uint16_t		type;		// The type of the data
		sw_uint16_t		refcount;	// The number of references to this data
		} SharedObjectHeader;


typedef struct sw_shrobj_list
		{
		sw_uint32_t		head;		// The offset of the first record
		sw_uint32_t		tail;		// The offset of the last record
		sw_uint16_t		size;		// The number of elements in the list
		} SharedObjectList;


// A special global mutex
class ShrObjMutex : public SWMutex
		{
public:
		ShrObjMutex();
		virtual ~ShrObjMutex();

		virtual bool		isLocked();
		virtual bool		hasLock();
		virtual sw_status_t	acquire(sw_uint64_t);
		virtual sw_status_t	release();

#if defined(SW_USE_SOLARIS_MUTEXES) || defined(SW_USE_POSIX_MUTEXES)
		void				init(void *pMutex, bool createflag);
#endif // defined(SW_USE_SOLARIS_MUTEXES) || defined(SW_USE_POSIX_MUTEXES)

private:
		// We cannot use the sw_mutex interface here since it calls us. Therefore we can only
		// use native interfaces.
#if defined(SW_USE_SOLARIS_MUTEXES) || defined(SW_USE_POSIX_MUTEXES)
		sw_shrobj_mutex_t	*m_pMutex;
#elif defined(SW_USE_WINDOWS_MUTEXES)
		HANDLE				m_pMutex;
#else
		#error Unsupported platform. Please update this file and/or sw_platform.h
#endif
		int					m_locked;
		SWThreadMutex		m_tmutex;
		};


ShrObjMutex::ShrObjMutex() :
	SWMutex(0),
	m_locked(0)
		{
#if defined(SW_USE_SOLARIS_MUTEXES) || defined(SW_USE_POSIX_MUTEXES)
		m_pMutex = NULL;
#elif defined(SW_USE_WINDOWS_MUTEXES)
		m_pMutex = CreateMutex(NULL, FALSE, _T("sw_shrobj_global_ShrObjMutex"));
		if (m_pMutex == NULL && GetLastError() == ERROR_ACCESS_DENIED)
			{
			// printf("Failed to create global mutex - attempting to open instead\n");
			m_pMutex = OpenMutex(SYNCHRONIZE, FALSE, _T("sw_shrobj_global_ShrObjMutex"));
			}

		if (m_pMutex == NULL)
			{
			// printf("Failed to open or create global mutex\n");
			throw SW_EXCEPTION(SWMutexCreateException);
			}
#else
		#error Unsupported platform. Please update this file and/or sw_platform.h
#endif
		}


ShrObjMutex::~ShrObjMutex()
		{
#if defined(SW_USE_WINDOWS_MUTEXES)
		CloseHandle(m_pMutex);
#endif
		}


bool
ShrObjMutex::isLocked()
		{
		return (m_locked != 0);
		}


bool
ShrObjMutex::hasLock()
		{
		return (m_locked != 0);
		}


sw_status_t
ShrObjMutex::acquire(sw_uint64_t)
		{
		SWMutexGuard	guard(m_tmutex);

		if (m_pMutex != NULL)
			{
			if (m_locked++ == 0)
				{
#if defined(SW_USE_SOLARIS_MUTEXES)
				mutex_lock(m_pMutex);
#elif defined(SW_USE_POSIX_MUTEXES)
				pthread_mutex_lock(m_pMutex);
#elif defined(SW_USE_WINDOWS_MUTEXES)
				WaitForSingleObject(m_pMutex, INFINITE);
#else
	#error Unsupported platform. Please update this file and/or sw_platform.h
#endif

				}
			}

		return SW_STATUS_SUCCESS;
		}


sw_status_t
ShrObjMutex::release()
		{
		SWMutexGuard	guard(m_tmutex);

		if (m_pMutex != NULL && m_locked > 0)
			{
			if (--m_locked == 0)
				{
#if defined(SW_USE_SOLARIS_MUTEXES)
				mutex_unlock(m_pMutex);
#elif defined(SW_USE_POSIX_MUTEXES)
				pthread_mutex_unlock(m_pMutex);
#elif defined(SW_USE_WINDOWS_MUTEXES)
				ReleaseMutex(m_pMutex);
#else
	#error Unsupported platform. Please update this file and/or sw_platform.h
#endif
				}
			}

		return SW_STATUS_SUCCESS;
		}


#if defined(SW_USE_SOLARIS_MUTEXES) || defined(SW_USE_POSIX_MUTEXES)
void
ShrObjMutex::init(void *pMutex, bool createflag)
		{
		SWMutexGuard	guard(m_tmutex);

		m_pMutex = reinterpret_cast<sw_shrobj_mutex_t *>(pMutex);
		if (m_pMutex != NULL && createflag)
			{
#if defined(SW_USE_SOLARIS_MUTEXES)
			mutex_init(m_pMutex, USYNC_PROCESS, 0); 
#elif defined(SW_USE_POSIX_MUTEXES)
			pthread_mutexattr_t	attr;

			pthread_mutexattr_init(&attr);
			pthread_mutexattr_setpshared(&attr, PTHREAD_PROCESS_SHARED);
			pthread_mutex_init(m_pMutex, &attr);
			pthread_mutexattr_destroy(&attr);
#endif
			}
		}
#endif // defined(SW_USE_SOLARIS_MUTEXES) || defined(SW_USE_POSIX_MUTEXES)



// The "global" variables
static bool						initdone=false;
static ShrObjMutex				mutex;
static SWSharedMemoryManager	shm;
static SharedObjectList			*pList=NULL;


#define SW_SHROBJ_SHMID		0x8f0a5132
#define SW_SHROBJ_SHMSIZE	0x10000
#define SW_SHROBJ_RESERVE	256
#define SW_SHROBJ_MAGIC		0x8a6c10fe



static sw_status_t
sw_shrobj_init()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (!initdone)
			{
			res = shm.attach(SW_SHROBJ_SHMID, &mutex, SW_SHROBJ_RESERVE);
			if (res == SW_STATUS_SUCCESS)
				{
				// Attach to the mutex
#if defined(SW_USE_SOLARIS_MUTEXES) || defined(SW_USE_POSIX_MUTEXES)
				mutex.init(shm.address(0), false);
				pList = reinterpret_cast<SharedObjectList *>(shm.address(sizeof(sw_shrobj_mutex_t)));
#elif defined(SW_USE_WINDOWS_MUTEXES)
				pList = reinterpret_cast<SharedObjectList *>(shm.address(0));
#else
		#error Unsupported platform. Please update this file and/or sw_platform.h
#endif
				}
			else
				{
				// Try creating the segment
				res = shm.create(SW_SHROBJ_SHMID, SW_SHROBJ_SHMSIZE, &mutex, SW_SHROBJ_RESERVE);

				if (res == SW_STATUS_SUCCESS)
					{
#if defined(SW_USE_SOLARIS_MUTEXES) || defined(SW_USE_POSIX_MUTEXES)
					mutex.init(shm.address(0), true);
					pList = reinterpret_cast<SharedObjectList *>(shm.address(sizeof(sw_shrobj_mutex_t)));
#elif defined(SW_USE_WINDOWS_MUTEXES)
					pList = reinterpret_cast<SharedObjectList *>(shm.address(0));
#else
		#error Unsupported platform. Please update this file and/or sw_platform.h
#endif

					// Initialise the list
					pList->head = 0;
					pList->tail = 0;
					pList->size = 0;
					}
				}

			initdone = true;
			}

		return res;
		}


static void
sw_shrobj_list_add(SharedObjectHeader *pElement)
		{
		sw_uint32_t	offset = shm.offset(pElement);

		pElement->next = 0;
		pElement->prev = pList->tail;

		if (pList->tail != 0)
			{
			SharedObjectHeader	*pTailElement=reinterpret_cast<SharedObjectHeader *>(shm.address(pList->tail));

			pTailElement->next = offset;
			}

		pList->tail = offset;
		if (pList->head == 0) pList->head = offset;
		pList->size++;
		}



static void
sw_shrobj_list_remove(SharedObjectHeader *pElement)
		{
		sw_uint32_t	offset = shm.offset(pElement);
		sw_uint32_t	n, p;

		n = pElement->next;
		p = pElement->prev;
		if (pList->head == offset) pList->head = pElement->next;
		if (pList->tail == offset) pList->tail = pElement->prev;
		if (n)
			{
			pElement=reinterpret_cast<SharedObjectHeader *>(shm.address(n));
			pElement->prev = p;
			}
		if (p)
			{
			pElement=reinterpret_cast<SharedObjectHeader *>(shm.address(p));
			pElement->next = n;
			}

		pList->size--;
		}


static SharedObjectHeader *
sw_shrobj_list_find(const sw_tchar_t *name)
		{
		SharedObjectHeader	*pElement=NULL;
		sw_uint32_t			offset;
		
		offset = pList->head;
		while (offset != 0)
			{
			pElement=reinterpret_cast<SharedObjectHeader *>(shm.address(offset));
			if (t_strcmp(reinterpret_cast<sw_tchar_t *>(pElement+1), name) == 0) break;
			offset = pElement->next;
			}

		if (offset == 0) pElement = NULL;

		return pElement;
		}



static void
sw_shrobj_list_dump()
		{
		SharedObjectHeader	*pElement=NULL;
		sw_uint32_t			offset;
		
		offset = pList->head;
		while (offset != 0)
			{
			pElement=reinterpret_cast<SharedObjectHeader *>(shm.address(offset));

			sw_tchar_t	*pName=reinterpret_cast<sw_tchar_t *>(pElement+1);
			sw_printf("handle %p, offset %5d, next %5d, prev %5d, namelen %hd, datalen %hd, type %hd, refcount %hd, name %s\n",
				pElement,
				offset,
				pElement->next,
				pElement->prev,
				pElement->namelen,
				pElement->datalen,
				pElement->type,
				pElement->refcount,
				pName);

			offset = pElement->next;
			}
		}



SWIFT_DLL_EXPORT sw_status_t
sw_shrobj_create(const sw_tchar_t *name, sw_uint16_t datatype, sw_uint16_t datalen, sw_handle_t &hobj)
		{
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (!initdone) res = sw_shrobj_init();

		if (datatype == 0) res = SW_STATUS_INVALID_PARAMETER;

		hobj = 0;
		if (res == SW_STATUS_SUCCESS)
			{
			SWMutexGuard		guard(mutex);
			SharedObjectHeader	*pHeader;

			pHeader = sw_shrobj_list_find(name);
			if (pHeader != NULL)
				{
				// The object already exists, so report an error
				res = SW_STATUS_ALREADY_EXISTS;
				hobj = pHeader;
				}
			else
				{
				size_t		namelen = (t_strlen(name)+1) * sizeof(sw_tchar_t);

				// Adjust name and data lengths to be multiples of (void *).
				namelen = ((namelen  + sizeof(void *) - 1) / sizeof(void *)) * sizeof(void *);
				datalen = (sw_uint16_t)(((datalen  + sizeof(void *) - 1) / sizeof(void *)) * sizeof(void *));

				pHeader = reinterpret_cast<SharedObjectHeader *>(shm.malloc(sizeof(SharedObjectHeader) + namelen + datalen));
				if (pHeader == NULL)
					{
					res = SW_STATUS_OUT_OF_MEMORY;
					}
				else
					{
					pHeader->magic = SW_SHROBJ_MAGIC;
					pHeader->next = 0;
					pHeader->prev = 0;
					pHeader->type = datatype;
					pHeader->refcount = 1;
					pHeader->namelen = (sw_uint16_t)namelen;
					pHeader->datalen = (sw_uint16_t)datalen;

					sw_tchar_t	*pName = reinterpret_cast<sw_tchar_t *>(pHeader+1);
					t_strcpy(pName, name);

					char		*pData = reinterpret_cast<char *>(pHeader+1) + namelen;
					memset(pData, 0, datalen);

					sw_shrobj_list_add(pHeader);

					hobj = pHeader;
					}
				}
			}

		return res;
		}


SWIFT_DLL_EXPORT sw_status_t
sw_shrobj_attach(const sw_tchar_t *name, sw_uint16_t type, sw_handle_t &hobj)
		{
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (!initdone) res = sw_shrobj_init();

		hobj = 0;
		if (res == SW_STATUS_SUCCESS)
			{
			SWMutexGuard		guard(mutex);
			SharedObjectHeader	*pHeader;

			pHeader = sw_shrobj_list_find(name);
			if (pHeader == NULL)
				{
				// The object does not exist, so report an error
				res = SW_STATUS_NOT_FOUND;
				}
			else if (pHeader->type != type && type != 0)
				{
				// The object does exists, but is the wrong type, so report an error
				res = SW_STATUS_ILLEGAL_OPERATION;
				hobj = pHeader;
				}
			else
				{
				pHeader->refcount++;
				hobj = pHeader;
				}
			}

		return res;
		}


SWIFT_DLL_EXPORT sw_status_t
sw_shrobj_detach(sw_handle_t hobj, bool autodelete)
		{
		SharedObjectHeader	*pHeader=reinterpret_cast<SharedObjectHeader *>(hobj);
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (!initdone) res = sw_shrobj_init();

		if (pHeader == NULL || pHeader->magic != SW_SHROBJ_MAGIC)
			{
			res = SW_STATUS_INVALID_PARAMETER;
			}

		if (res == SW_STATUS_SUCCESS)
			{
			SWMutexGuard	guard(mutex);

			if (pHeader->refcount == 0)
				{
				// The object does exists, but is the wrong type, so report an error
				res = SW_STATUS_ILLEGAL_OPERATION;
				}
			else
				{
				pHeader->refcount--;
				if (pHeader->refcount == 0 && autodelete)
					{
					sw_shrobj_list_remove(pHeader);
					shm.free(pHeader);
					}
				}
			}

		return res;
		}


SWIFT_DLL_EXPORT sw_status_t
sw_shrobj_delete(const sw_tchar_t *name)
		{
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (!initdone) res = sw_shrobj_init();

		if (res == SW_STATUS_SUCCESS)
			{
			SWMutexGuard	guard(mutex);

			SharedObjectHeader	*pHeader=sw_shrobj_list_find(name);

			if (pHeader != NULL)
				{
				if (pHeader->refcount == 0)
					{
					sw_shrobj_list_remove(pHeader);
					shm.free(pHeader);
					}
				else
					{
					res = SW_STATUS_ILLEGAL_OPERATION;
					}
				}
			else
				{
				res = SW_STATUS_NOT_FOUND;
				}
			}

		return res;
		}


SWIFT_DLL_EXPORT void *
sw_shrobj_data(sw_handle_t hobj)
		{
		SharedObjectHeader	*pHeader=reinterpret_cast<SharedObjectHeader *>(hobj);
		char				*pData=NULL;

		if (pHeader != NULL && pHeader->magic == SW_SHROBJ_MAGIC)
			{
			pData = reinterpret_cast<char *>(pHeader+1) + pHeader->namelen;
			}

		return pData;
		}


SWIFT_DLL_EXPORT sw_tchar_t *
sw_shrobj_name(sw_handle_t hobj)
		{
		SharedObjectHeader	*pHeader=reinterpret_cast<SharedObjectHeader *>(hobj);
		sw_tchar_t			*pData=NULL;

		if (pHeader != NULL && pHeader->magic == SW_SHROBJ_MAGIC)
			{
			pData = reinterpret_cast<sw_tchar_t *>(pHeader+1);
			}

		return pData;
		}


SWIFT_DLL_EXPORT sw_uint16_t
sw_shrobj_datatype(sw_handle_t hobj)
		{
		SharedObjectHeader	*pHeader=reinterpret_cast<SharedObjectHeader *>(hobj);
		sw_uint16_t			res=0;

		if (pHeader != NULL && pHeader->magic == SW_SHROBJ_MAGIC)
			{
			res = pHeader->type;
			}

		return res;
		}


SWIFT_DLL_EXPORT sw_uint16_t
sw_shrobj_datalen(sw_handle_t hobj)
		{
		SharedObjectHeader	*pHeader=reinterpret_cast<SharedObjectHeader *>(hobj);
		sw_uint16_t			res=0;

		if (pHeader != NULL && pHeader->magic == SW_SHROBJ_MAGIC)
			{
			res = pHeader->datalen;
			}

		return res;
		}


SWIFT_DLL_EXPORT void
sw_shrobj_dump()
		{
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (!initdone) res = sw_shrobj_init();

		if (res == SW_STATUS_SUCCESS)
			{
			SWMutexGuard	guard(mutex);

			printf("Shared object list: head=%d, tail=%d, size=%d\n",
				pList->head,
				pList->tail,
				pList->size);

			sw_shrobj_list_dump();
			}
		}


SWIFT_DLL_EXPORT void
sw_shrobj_lock()
		{
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (!initdone) res = sw_shrobj_init();

		mutex.acquire(SW_TIMEOUT_INFINITE);
		}




SWIFT_DLL_EXPORT void
sw_shrobj_unlock()
		{
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (!initdone) res = sw_shrobj_init();

		mutex.release();
		}




