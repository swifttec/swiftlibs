/**
*** @file		SWUInt32.h
*** @brief		A class to represent a date
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWUInt32 class.
***
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWUInt32_h__
#define __SWUInt32_h__

#include <swift/swift.h>
#include <swift/SWObject.h>
#include <swift/SWException.h>
#include <swift/SWString.h>

// Forward declarations
class SWTime;

/**
*** @brief	A generic date class based on Julian Day Numbers.
***
***	This is a generic date class that represents dates as Julian Day Numbers.
*** This gives a enormous range of dates that are stored in a 32-bit integer value.
***
*** The class stores date information only and has no time component. However it
*** can easily be converted both to and from a SWTime value. Where both date and
*** time information are required developers should use the SWTime class.
***
*** @author		Simon Sparkes
*/
class SWIFT_DLL_EXPORT SWUInt32
		{
public:
		SWUInt32(sw_uint32_t amount=0) : m_amount(amount)		{ }
		SWUInt32(int amount) : m_amount((sw_uint32_t)amount)	{ }
		SWUInt32(const char *s);
		SWUInt32(const wchar_t *s);
		SWUInt32(const SWString &s);

		SWUInt32 &	operator=(const char *s);
		SWUInt32 &	operator=(const wchar_t *s);
		SWUInt32 &	operator=(const SWString &s);
		SWUInt32 &	operator=(int amount)			{ m_amount = (sw_uint32_t)amount; return *this; }
		SWUInt32 &	operator=(sw_uint32_t amount)	{ m_amount = amount; return *this; }

		SWString		toString(const SWString &fmt="%u") const;

		operator sw_uint32_t() const			{ return m_amount; }
		operator sw_uint32_t &()				{ return m_amount; }

private:
		sw_uint32_t	m_amount;
		};


#endif
