/**
*** @file		SWThreadRWMutex.h
*** @brief		A thread reader-writer mutex
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWThreadRWMutex class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWThreadRWMutex_h__
#define __SWThreadRWMutex_h__

#include <swift/SWRWMutex.h>
#include <swift/SWThread.h>
#include <swift/SWThreadIdCount.h>
#include <swift/SWOrderedList.h>
#include <swift/SWThreadMutex.h>




#if defined(_MSC_VER)
	 // class xxxx<>' needs to have dll-interface to be used by clients
#endif

/**
*** @brief	Inter-thread (within a single process) non-recusrsive reader-writer mutex.
***
*** Thread-specific version of an RWMutex. This is a non-recursive mutex implementation
*** which means that any attempt for a mutex to be re-acquired by the same thread would
*** block indefinitely. To prevent this situation the Thread class will throw a 
*** MutexAlreadyHeldException if this is detected.
***
*** This class does not support upgrading from a read to a write lock
***
*** @see		RWMutex
*** @author		Simon Sparkes
*/
class SWIFT_DLL_EXPORT SWThreadRWMutex : public SWRWMutex
		{
public:
		/// Constructor
		SWThreadRWMutex();

		/// Virtual destructor
		virtual ~SWThreadRWMutex();

		/// Lock/Acquire this mutex for reading.
		virtual sw_status_t	acquireReadLock(sw_uint64_t waitms=SW_TIMEOUT_INFINITE);

		/// Lock/Acquire this mutex for writing
		virtual sw_status_t	acquireWriteLock(sw_uint64_t waitms=SW_TIMEOUT_INFINITE);

		/// Unlock/Release this mutex. If the mutex has been recursively acquired the mutex
		/// will only be unlocked when the recursive lock count reaches zero.
		virtual sw_status_t	release();

		/**
		*** @brief	Test to see if the current thread has a read lock.
		***
		*** Returns true if this thread already has this mutex locked for reading
		*** or for writing. We can do this because the write lock is an exclusive
		*** lock and by definition allows read access.
		**/
		virtual bool		hasReadLock();

		/// Returns true if this thread already has this mutex locked for writing.
		virtual bool		hasWriteLock();

		/// Check to see if this mutex is locked by someone for read access.
		virtual bool		isLockedForRead();

		/// Check to see if this mutex is locked by someone for write access.
		virtual bool		isLockedForWrite();

private:
		sw_handle_t					m_hMutex;	///< The handle to the actual mutex
		SWOrderedList<sw_thread_id_t>	m_rtidlist;	///< Used for recursive acquire safety checks 
		sw_thread_id_t					m_wtid;		///< Used for recursive acquire safety checks
		SWThreadMutex				m_tguard;	///< Used for recursive acquire safety checks
		};






#endif
