/**
*** @file		SWMultiString.h
*** @brief		A microsoft like multi-string
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWMultiString class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWMultiString_h__
#define __SWMultiString_h__

#include <swift/SWString.h>
#include <swift/SWObject.h>
#include <swift/SWDataBuffer.h>
#include <swift/SWStringArray.h>
#include <swift/SWRegistryValue.h>


/**
*** @brief	An array template based on the Array class.
***
*** An array of SWString objects based on the Array class.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWMultiString : public SWObject
		{
public:
		// Construct an empty multi-string
		SWMultiString();

		/// Construct from a char pointer
		SWMultiString(const char *sp);

		/// Construct from a wchar_t pointer
		SWMultiString(const wchar_t *sp);

		/// Construct from a registry value
		SWMultiString(const SWRegistryValue &rv);

		/// Copy constructor
		SWMultiString(const SWStringArray &sa);

		/// Copy constructor
		SWMultiString(const SWMultiString &other);

		/// Virtual destructor
		virtual ~SWMultiString();

		/// Assignment operator
		SWMultiString &		operator=(const SWMultiString &other);

		/// Assignment operator
		SWMultiString &		operator=(const char *sp);

		/// Assignment operator
		SWMultiString &		operator=(const wchar_t *sp);

		/// Assignment operator
		SWMultiString &		operator=(const SWStringArray &);

		/// Assignment operator
		SWMultiString &		operator=(const SWRegistryValue &rv);

		/// Assignment operator
		void				load(const SWStringArray &sa, int cs=0);

		/**
		*** @brief Returns the nth element without growing the array
		***
		*** This method returns a reference to the nth element in the array
		*** which can be modified as required.
		*** If the index specified is greater than the number of elements in the
		*** array, the array is automatically extended to encompass the referenced
		*** element.
		***
		*** If the index specified is negative an exception is thrown.
		***
		*** @param[in]	i	The index of the element to access.
		**/
		SWString			operator[](sw_index_t i) const;


		/**
		*** @brief Returns the nth element without growing the array
		***
		*** Gets the nth element without growing the array. If the index is out of bounds
		*** an exception is thrown rather than growing the array automatically. Provided
		*** for use when a const operation is required.
		***
		*** @param[in]	i	The index of the element to access.
		**/
		SWString			get(sw_index_t i) const;


		/// Returns the number of elements
		sw_size_t			size() const		{ return m_nelems; }


		/**
		*** @brief	Clear the array of all data and reset the size to zero.
		**/
		virtual void		clear();


		/**
		*** @brief	Clear the array of all data and reset the size to zero.
		**/
		virtual void		clear(int cs);


		/**
		*** @brief Remove one or more elements from the array.
		***
		*** @param[in]	pos		The position to remove elements from.
		*** @param[in]	count	The number of elements to remove.
		void				remove(sw_index_t pos, sw_size_t count=1);
		**/



		/**
		*** @brief	Add the specified value to the end of the array.
		***
		*** @param[in]	v		The value to add.
		**/
		void				add(const SWString &v);


		/**
		*** Add (insert) at the specified position in the array
		***
		*** @param[in]	v		The value to add.
		*** @param[in]	pos		The position at which to insert the value.
		void				insert(const SWString &v, sw_index_t pos);
		**/

		/// Convert to a string array
		void				toStringArray(SWStringArray &sa);

		/// Convert to a delimited string
		SWString			convertToString(int sep, int squote, int equote, int meta);

		/// Convert from a delimited string
		void				convertFromString(const SWString &s, int sep, int squote, int equote, int meta, bool append);
		
		const void *		data() const		{ return m_data; }
		sw_size_t			datasize() const	{ return m_data.size(); }
public: // Comparison operators

		/**
		*** @brief	Compare this object with another.
		***
		*** This method compares this object with the one specified and returns a
		*** result indicating if the other object is less than, equal to, or greater
		*** than this object.
		***
		*** - If this object is less than the specified object then a negative result is returned.
		*** - If this object is greater than the specified object then a positive result is returned.
		*** - If this object is equal to the specified object then a zero result is returned.
		***
		*** @note
		*** The compareTo method is not required to return -1 and +1 for the negative and positive
		*** values although this is typically the value returned. Callers should test for \< 0 and \> 0
		*** rather than for specific values of -1 and +1.
		***
		*** @param[in]	other	The data to compare this object to.
		**/
		int			compareTo(const SWMultiString &other) const;

		/**
		*** @brief	Test for equality
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is equal to the other object, false otherwise.
		**/
		bool	operator==(const SWMultiString &other) const	{ return (compareTo(other) == 0); }

		/**
		*** @brief	Test for inequality
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is not equal to the other object, false otherwise.
		**/
		bool	operator!=(const SWMultiString &other) const	{ return (compareTo(other) != 0); }

		/**
		*** @brief	Test for less than.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is less than the other object, false otherwise.
		**/
		bool	operator< (const SWMultiString &other) const	{ return (compareTo(other) <  0); }

		/**
		*** @brief	Test for greater than.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is greater than the other object, false otherwise.
		**/
		bool	operator> (const SWMultiString &other) const	{ return (compareTo(other) >  0); }

		/**
		*** @brief	Test for less than or equal to.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is less than or equal to the other object, false otherwise.
		**/
		bool	operator<=(const SWMultiString &other) const	{ return (compareTo(other) <= 0); }

		/**
		*** @brief	Test for greater than or equal to.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is greater than or equal to the other object, false otherwise.
		**/
		bool	operator>=(const SWMultiString &other) const	{ return (compareTo(other) >= 0); }



public: // virtual overrides

		/// Override for SWObject writeToStream
		virtual sw_status_t		writeToStream(SWOutputStream &stream) const;

		/// Override for SWObject readFromStream
		virtual sw_status_t		readFromStream(SWInputStream &stream);

protected:
		void *			getElement(sw_index_t pos) const;

protected:
		SWDataBuffer	m_data;
		sw_size_t		m_charsize;
		sw_size_t		m_nelems;
		};




#endif
