/**
*** @file		SWFile.h
*** @brief		Basic file handling class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWFile class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




#ifndef __SWFile_h__
#define __SWFile_h__

#include <swift/swift.h>

#include <swift/SWObject.h>
#include <swift/SWString.h>
#include <swift/SWFilename.h>
#include <swift/SWFileRange.h>






// Forward declarations
class SWFileSecurity;
class SWFileAttributes;


/**
*** @brief	Basic file handling class.
***
*** SWFile is a basic file handling class which hides the 32/64 bit implementation
*** of the underlying operating system by providing a 64-bit interface.
*** SWFile does not do any data caching or buffering an provides an interface
*** that is as close to the underlying O/S as possible.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWFile : public SWObject
		{
public:
		/**
		*** @brief	An enumeration to specify the seek method
		**/
		enum SeekMethod
			{
			SeekSet=0,		///< Seek to specified position.
			SeekRelative=1,	///< Seek relative to current position.
			SeekEnd=2		///< Seek relative to the end of the file.
			};

		/**
		*** @brief	Flags to specify the mode in which a file is opened.
		**/
		enum OpenMode
			{
			ModeReadOnly		= 0x00000000,	///< Open for read-only access.
			ModeWriteOnly		= 0x00000001,	///< Open for write-only access.
			ModeReadWrite		= 0x00000002,	///< Open for read-write access.
			ModeCreate			= 0x00000004,	///< Create file if it does not exist.
			ModeTruncate		= 0x00000008,	///< Truncate the file to zero length if it exists.
			ModeAppend			= 0x00000010,	///< File pointer is set to the end of the file before every write.
			ModeExclusive		= 0x00000020,	///< When combined with ModeCreate the open will fail if the file exists.
			ModeTemporary		= 0x00000040,	///< The file is being used for temporary storage and should not be flushed to disk if possible.
			ModeShortLived		= 0x00000080,	///< The file is a short-term file and should not be flushed to disk if possible.
			ModeNonBlocking		= 0x00000100,	///< Use non-blocking I/O.
			ModeSynchronised	= 0x00000200,	///< Writes to the file do not complete until flushed to disk.

			ModeRandom			= 0x00001000,	///< File system hint: Primarily random access on this file.
			ModeSequential		= 0x00002000,	///< File system hint: Primarily sequential access on this file.

			ModeBinary			= 0x00000000,	///< A binary file where data is untranslated (default).
			ModeText			= 0x00004000,	///< A text file where data may be translated (platform dependent).

			ModeNoInherit		= 0x00008000,	///< A text file where data may be translated (platform dependent).
			ModeNoCtrlTTY		= 0x00010000,	///< Don't allocate controlling tty.
			ModeDeleteOnClose	= 0x00020000,	///< Delete the file when closed.
			ModeNoFixPath		= 0x00040000	///< Don't try and fix the path
			};

		/**
		*** @brief	Flags to specify how a file is shared when opened.
		**/
		enum ShareMode
			{
			ShareDenyNone		= 0x00000000,	///< Do not deny access to this file.
			ShareDenyRead		= 0x10000000,	///< Deny access to this file for reading.
			ShareDenyWrite		= 0x20000000,	///< Deny access to this file for writing.
			ShareDenyAll		= 0x30000000	///< Deny all access to this file.
			};

public:
		/// Construct a file object ready to be opened.
		SWFile();

		/// Virtual destructor
		virtual ~SWFile();

		/**
		*** @brief	Opens the specified file.
		***
		*** The open method opens the specified file in the manner described by the arguments.
		*** The open mode is specified using a combination of the OpenMode and ShareMode flags,
		*** however only one of ModeReadOnly, ModeWriteOnly and ModeReadWrite can be specified.
		***
		*** <b>Note:</b>
		*** Although many modes are provided, not all operating systems will provide all features
		*** and even on a single operating system that supports multiple file system types, not all
		*** the modes will be supported on all the file system types.
		***
		*** @param	filename	The name of the file to be opened.
		*** @param	mode		The mode in which the file is opened. This is a combination
		***						of the OpenMode and ShareMode flags specified above.
		*** @param	pfa			A pointer to a file attribute object which is used when a file
		***						is created. If not supplied default file attributes are used.
		***						Only used if the file mode ModeCreate is set and a file is created.
		***						The file attributes define how a file is created and includes
		***						security information.
		***
		*** @return SW_STATUS_SUCCESS if successful or the error code.
		**/
		sw_status_t			open(const SWString &filename, int mode, SWFileAttributes *pfa=0);

		/**
		*** @brief	Closes the file
		***
		*** @return SW_STATUS_SUCCESS if successful or the error code.
		**/
		sw_status_t			close();

		/// Return the open status of the file.
		bool				isOpen() const;

		/// Returns the current size of the file.
		sw_filesize_t		size();

//@{
/// @name I/O Methods
/// Various I/O methods

		/**
		*** @brief	Read nbytes of data from the file into the specified memory location.
		***
		*** @return the number of bytes actually read or -1 on failure.
		**/
		sw_status_t			read(void *data, sw_size_t nbytes, sw_size_t *pBytesRead=NULL);

		/**
		*** @brief	Write nbytes of data to the file from the specified memory location.
		***
		*** @return the number of bytes actually written or -1 on failure.
		**/
		sw_status_t			write(const void *data, sw_size_t nbytes, sw_size_t *pBytesWritten=NULL);

		/**
		*** @brief	Seek to the specified position
		***
		*** Seek to the specified position in the file for subsequent I/O
		*** operations.
		***
		*** @param	offset	The position to seek to within the file.
		*** @param	method	Specfies how to apply the offset. The method
		***					can have one of the following values:
		***					- SeekSet - Seek to the location specified by the offset.
		***					- SeekRelative - Seek to the offset relative to the current location in the file.
		***					- SeekEnd - Seek to the end of the file plus the specified offset.
		**/
		sw_status_t			lseek(sw_filepos_t offset, SeekMethod method, sw_filepos_t *pCurrentOffset=NULL);

		/// Returns the current file position, where data will next be read from or written to.
		sw_filepos_t		tell();

		/// Returns the end-of-file status.
		bool				eof();

		/// Truncate the file to the specified size.
		sw_status_t			truncate(sw_filepos_t size);

		/// Flushes outstanding data from the buffers to the disks.
		sw_status_t			flush();

		/// Free a section of the file (i.e make it sparse). Not all file systems
		/// and/or operating systems support this.
		sw_status_t			freeSection(const SWFileRange &range);

		/// Lock a section of the file.
		sw_status_t			lockSection(const SWFileRange &range);

		/// Unlock a previously locked section of the file.
		sw_status_t			unlockSection(const SWFileRange &range);

		/// Free a section of the file (i.e make it sparse). Not all file systems
		/// and/or operating systems support this.
		sw_status_t			freeSection(sw_filepos_t offset, sw_size_t length)		{ return freeSection(SWFileRange(offset, length)); }

		/// Lock a section of the file.
		sw_status_t			lockSection(sw_filepos_t offset, sw_size_t length)		{ return lockSection(SWFileRange(offset, length)); }

		/// Unlock a previously locked section of the file.
		sw_status_t			unlockSection(sw_filepos_t offset, sw_size_t length)	{ return unlockSection(SWFileRange(offset, length)); }
//@}

		/// Return the last error code returned on this file.
		sw_status_t			getLastError() const	{ return _lastErrno; }

		// Return the name of the file
		const SWString &	name()	{ return _name; }

		SWString			getFullPath() const;


		bool				isReadOnly() const		{ return ((_mode & 3) == ModeReadOnly); }

		/**
		*** @brief	Returns the file handle for this file for use with special operations.
		***
		*** This method is provided to give access to the file handle used internally
		*** so that non-generic operations can be carried out on a file. The sw_filehandle_t
		*** type will depend on the platform.  The file handle will be invalid if the file is not open.
		***
		*** <b>Care should be taken not to close the file using the file handle returned.</b>
		**/
		sw_filehandle_t		handle() const				{ return _hFile; }


		/**
		*** @brief	Detach and return the file handle for this file.
		***
		*** This is similar to the getFileHandle method except that the file
		*** handle information is removed from the object and subsequent file operations
		*** can only be done directly on the file handle via the O/S interface.
		**/
		sw_status_t			detach(sw_filehandle_t &fh);


		/**
		*** @brief	Attaches a native O/S file handle to this object.
		***
		*** This method attempts to attach the specified file handle to this object.
		***
		*** <b>When using this method it is important to note that filename and directory
		*** name information may not be available as availability is platform dependent.</b>
		***
		*** <b>SPECIAL NOTE FOR WINDOWS</b>Currently this will only work with native windows
		*** file handles (CreateFile, et al), it will not work with windows files opened using the unix layer for
		*** open/close/read/write. An attempt to attach a unix-style file descriptor will cause an exception.
		***
		*** @return	SW_STATUS_SUCCESS if successful, or an error code otherwise.
		**/
		sw_status_t			attach(sw_filehandle_t fh);


		/**
		*** @brief	Duplicates the current file handle.
		***
		*** This method attempts to duplicate the file handle returning a new file
		*** handle to the same file.
		***
		*** @param	fh		The SWFile object that is to be assigned the new file handle.
		***
		*** @return	SW_STATUS_SUCCESS if successful, or an error code otherwise.
		***							is already open.
		**/
		sw_status_t			duplicate(SWFile &fh);
		
public:
		/// Remove (unlink) the specified file.
		static sw_status_t	remove(const SWString &filename);

		/// Move the specified file
		static sw_status_t	move(const SWString &from, const SWString &to);

		/// Copy the specified file.
		static sw_status_t	copy(const SWString &from, const SWString &to);

private: // Members
		SWString		_name;			///< The name of the file when opened/created
		SWString		_dir;			///< The name of the directory when opened/created
		int				_mode;			///< The mode in which the file was opened
		sw_filehandle_t	_hFile;			///< A handle to the file
		sw_status_t		_lastErrno;		///< The last error number
		};




#endif
