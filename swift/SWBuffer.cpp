/**
*** @file		SWBuffer.cpp
*** @brief		A general purpose self-contained memory buffer
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWBuffer class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWBuffer.h>
#include <swift/SWGuard.h>
#include <swift/SWInputStream.h>
#include <swift/SWOutputStream.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





SWBuffer::SWBuffer(SWMemoryManager *pMemoryManager) :
	SWAbstractBuffer(pMemoryManager, NULL, 0, 0)
		{
		}


SWBuffer::SWBuffer(sw_size_t bufsize, SWMemoryManager *pMemoryManager) :
	SWAbstractBuffer(pMemoryManager, NULL, bufsize, 0)
		{
		}


SWBuffer::SWBuffer(void *pBuf, sw_size_t bufsize) :
	SWAbstractBuffer(NULL, (sw_byte_t *)pBuf, bufsize, Reference|FixedSize)
		{
		}


SWBuffer::SWBuffer(const void *pBuf, sw_size_t bufsize) :
	SWAbstractBuffer(NULL, (sw_byte_t *)pBuf, bufsize, ReadOnly|Reference|FixedSize)
		{
		}


SWBuffer::~SWBuffer()
		{
		}


SWBuffer::SWBuffer(const SWBuffer &other, SWMemoryManager *pMemoryManager) :
	SWAbstractBuffer(pMemoryManager, NULL, 0, 0)
		{
		*this = other;
		}


SWBuffer &
SWBuffer::operator=(const SWBuffer &other)
		{
		SWAbstractBuffer::operator=(other);

		return *this;
		}


/*
void *
SWBuffer::data(sw_size_t nbytes, bool freeExtra)
		{
		// Force a minimum size
		sw_size_t	sizemod=16;

		if (nbytes < sizemod) nbytes = sizemod;
		else nbytes = ((nbytes + sizemod - 1) / sizemod) * sizemod;

		if (nbytes > m_capacity && m_pMemoryManager == NULL)
			{
			throw SW_EXCEPTION(SWMemoryAllocationException);
			}

		if ((nbytes > m_capacity) || (freeExtra && nbytes < m_capacity))
			{
			sw_byte_t	*pNewBuf;

			pNewBuf = reinterpret_cast<sw_byte_t *>(m_pMemoryManager->realloc(m_pData, nbytes));
			if (pNewBuf == NULL) throw SW_EXCEPTION(SWMemoryAllocationException);
			m_pData = pNewBuf;
			m_capacity = nbytes;
			}
		
		return m_pData;
		}
*/



