/**
*** @file		SWInputStream.cpp
*** @brief		A generic input stream.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWInputStream class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




#include "stdafx.h"

#include <swift/SWInputStream.h>
#include <swift/SWException.h>
#include <swift/SWDataBuffer.h>
#include <swift/SWString.h>


#if defined(SW_PLATFORM_WINDOWS)
	#include <float.h>
	#include <math.h>
	#define isnand(x)	_isnan(x)
	#define finite(x)	_finite(x)
	#define	IsNegNAN(d)	(d < 0.0)
#else
	#include <unistd.h>

	#if defined(SW_PLATFORM_SOLARIS)
		#include <ieeefp.h>
		#include <nan.h>
	#endif // linux
#endif // WIN32

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




double
SWInputStream::longBitsToDouble(sw_int64_t bits)
		{
		double	d;

#ifdef SW_PLATFORM_NATIVE_DOUBLE_IN_IEEE_FORMAT
		d = *((double *)&bits);
#else

		// Using atof here since I can't find (yet) the defs for infinity, etc
		if (bits == 0x7ff8000000000000L) d = atof("NAN");
		else if (bits == 0xfff8000000000000L) d = atof("-NAN");
		else if (bits == 0x7ff0000000000000L) d = atof("Inf");
		else if (bits == 0xfff0000000000000L) d = atof("-Inf");
		else
			{
			int	s = ((bits >> 63) == 0) ? 1 : -1;
			int	e = (int)((bits >> 52) & 0x7ffL);
			sw_int64_t	m = (e == 0) ?  (bits & 0xfffffffffffffL) << 1 : (bits & 0xfffffffffffffL) | 0x10000000000000L;

			// printf("%016llx -> sign=%d  e=%d  exp=%d  mantissa=%016llx\n", bits, s, e, e-1075, m);


			d = ldexp((double)m, e-1075) * (double)s;
			}

		// printf("%016llx -> %.16f\n", bits, d);
#endif

		return d;
		}
 

float
SWInputStream::intBitsToFloat(sw_int32_t bits)
		{
		float	f;

	#ifdef SW_PLATFORM_NATIVE_FLOAT_IN_IEEE_FORMAT
		f = *((float *)&bits);
	#else
		// Using atof here since I can't find (yet) the defs for infinity, etc
		if (bits == 0x7fc00000) f = (float)atof("NAN");
		else if (bits == 0xffc00000) f = (float)atof("-NAN");
		else if (bits == 0x7f800000) f = (float)atof("Inf");
		else if (bits == 0xff800000) f = (float)atof("-Inf");
		else
			{
			int	s = ((bits >> 31) == 0) ? 1 : -1;
			int	e = ((bits >> 23) & 0xff);
			int	m = (e == 0) ?  (bits & 0x7fffff) << 1 : ((bits & 0x7fffff) | 0x800000);

			// printf("%08x -> sign=%d  e=%d  exp=%d  mantissa=%08x\n", bits, s, e, e-150, m);

			f = (float)ldexp((double)m, e-150) * (float)s;
			}

		// printf("%08x -> %f\n", bits, f);
	#endif

		return f;
		}
 

sw_status_t
SWInputStream::readSignedInteger(sw_int64_t &v, int nbytes)
		{
		sw_status_t	res;
		sw_byte_t	buf[32], *bp=buf+nbytes-1;

		res = readData(buf, nbytes);
		if (res == SW_STATUS_SUCCESS)
			{
			v = 0;
			while (nbytes-- > 0)
				{
				v = ((v<<8)|((*bp--)&0xff));
				}
			}

		return res;
		}


sw_status_t	
SWInputStream::readUnsignedInteger(sw_uint64_t &v, int nbytes)
		{
		sw_status_t	res;
		sw_byte_t	buf[32], *bp=buf+nbytes-1;

		res = readData(buf, nbytes);
		if (res == SW_STATUS_SUCCESS)
			{
			v = 0;
			while (nbytes-- > 0)
				{
				v = ((v<<8)|((*bp--)&0xff));
				}
			}

		return res;
		}


sw_status_t	
SWInputStream::readDouble(double &v)
		{
		sw_status_t	res;
		sw_int64_t	iv;

		res = readSignedInteger(iv, sizeof(sw_int64_t));
		if (res == SW_STATUS_SUCCESS) v = longBitsToDouble(iv);

		return res;
		}


sw_status_t	
SWInputStream::readFloat(float &v)
		{
		sw_status_t	res;
		sw_int64_t	tmp;

		res = readSignedInteger(tmp, sizeof(sw_int32_t));
		if (res == SW_STATUS_SUCCESS) v = intBitsToFloat((sw_int32_t)tmp);

		return res;
		}


sw_status_t	
SWInputStream::readBoolean(bool &v)
		{
		sw_status_t	res;
		sw_uint64_t	tmp;

		res = readUnsignedInteger(tmp, 1);
		if (res == SW_STATUS_SUCCESS) v = (tmp != 0);

		return res;
		}


sw_status_t
SWInputStream::readBinaryData(void *pData, sw_size_t &len, sw_uint32_t buflen)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		sw_uint64_t	tmp;

		res = readUnsignedInteger(tmp, sizeof(len));
		if (res == SW_STATUS_SUCCESS)
			{
			sw_uint32_t	datalen=(sw_uint32_t)tmp;

			if (datalen > buflen)
				{
				// We can only read a portion of the data and we must discard the rest.
				res = readData(pData, buflen);
				if (res == SW_STATUS_SUCCESS)
					{
					char	*tmpbuf;

					tmpbuf = new char[datalen-buflen];
					res = readData(tmpbuf, datalen-buflen);
					delete tmpbuf;
					if (res == SW_STATUS_SUCCESS)
						{
						// Mark the result as having lost data.
						res = SW_STATUS_BUFFER_TOO_SMALL;
						}
					}
				else
					{
					len = 0;
					}
				}
			else
				{
				res = readData(pData, datalen);
				if (res == SW_STATUS_SUCCESS) len = datalen;
				else len = 0;
				}
			}

		return res;
		}


sw_status_t
SWInputStream::readRawData(void *pData, sw_size_t nbytes)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		res = readData(pData, nbytes);

		return res;
		}

sw_status_t
SWInputStream::readData(void * /*pData*/, sw_size_t /*len*/)
		{
		// The default behaviour is to return an error as we cannot perform this operation.
		return SW_STATUS_UNSUPPORTED_OPERATION;
		}



sw_status_t
SWInputStream::rewindStream()
		{
		// The default behaviour is to return an error as we cannot perform this operation.
		return SW_STATUS_UNSUPPORTED_OPERATION;
		}


sw_status_t
SWInputStream::read(bool &v)
		{
		return readBoolean(v);
		}


sw_status_t
SWInputStream::read(sw_int8_t &v)
		{
		sw_status_t	res;
		sw_int64_t	tmp;

		res = readSignedInteger(tmp, sizeof(v));
		if (res == SW_STATUS_SUCCESS) v = (sw_int8_t)tmp;

		return res;
		}


sw_status_t
SWInputStream::read(sw_int16_t &v)
		{
		sw_status_t	res;
		sw_int64_t	tmp;

		res = readSignedInteger(tmp, sizeof(v));
		if (res == SW_STATUS_SUCCESS) v = (sw_int16_t)tmp;

		return res;
		}


sw_status_t
SWInputStream::read(sw_int32_t &v)
		{
		sw_status_t	res;
		sw_int64_t	tmp;

		res = readSignedInteger(tmp, sizeof(v));
		if (res == SW_STATUS_SUCCESS) v = (sw_int32_t)tmp;

		return res;
		}


sw_status_t
SWInputStream::read(sw_int64_t &v)
		{
		return readSignedInteger(v, sizeof(v));
		}


sw_status_t
SWInputStream::read(sw_uint8_t &v)
		{
		sw_status_t	res;
		sw_uint64_t	tmp;

		res = readUnsignedInteger(tmp, sizeof(v));
		if (res == SW_STATUS_SUCCESS) v = (sw_uint8_t)tmp;

		return res;
		}


sw_status_t
SWInputStream::read(sw_uint16_t &v)
		{
		sw_status_t	res;
		sw_uint64_t	tmp;

		res = readUnsignedInteger(tmp, sizeof(v));
		if (res == SW_STATUS_SUCCESS) v = (sw_uint16_t)tmp;

		return res;
		}


sw_status_t
SWInputStream::read(sw_uint32_t &v)
		{
		sw_status_t	res;
		sw_uint64_t	tmp;

		res = readUnsignedInteger(tmp, sizeof(v));
		if (res == SW_STATUS_SUCCESS) v = (sw_uint32_t)tmp;

		return res;
		}


sw_status_t
SWInputStream::read(sw_uint64_t &v)
		{
		return readUnsignedInteger(v, sizeof(v));
		}


sw_status_t
SWInputStream::read(float &v)
		{
		return readFloat(v);
		}


sw_status_t
SWInputStream::read(double &v)
		{
		return readDouble(v);
		}


sw_status_t
SWInputStream::read(char *v, sw_uint32_t buflen)
		{
		sw_status_t	res;
		SWString	s;

		res = read(s);
		if (res == SW_STATUS_SUCCESS)
			{
			const char	*sp=s;
			sw_size_t	datalen=strlen(sp)+1;

			if (datalen > buflen)
				{
				datalen = buflen;
				res = SW_STATUS_BUFFER_TOO_SMALL;
				}

			memcpy(v, sp, datalen * sizeof(*v));
			}

		return res;
		}


sw_status_t
SWInputStream::read(wchar_t *v, sw_uint32_t buflen)
		{
		sw_status_t	res;
		SWString	s;

		res = read(s);
		if (res == SW_STATUS_SUCCESS)
			{
			const wchar_t	*sp=s;
			sw_size_t		datalen=wcslen(sp)+1;

			if (datalen > buflen)
				{
				datalen = buflen;
				res = SW_STATUS_BUFFER_TOO_SMALL;
				}

			memcpy(v, sp, datalen * sizeof(*v));
			}

		return res;
		}


sw_status_t
SWInputStream::read(void *pData, sw_size_t &datalen, sw_uint32_t buflen)
		{
		sw_status_t		res;
		SWDataBuffer	buf;

		res = read(buf);
		if (res == SW_STATUS_SUCCESS)
			{
			sw_size_t	nbytes=buf.size();

			datalen = nbytes;
			if (nbytes > buflen)
				{
				nbytes = buflen;
				res = SW_STATUS_BUFFER_TOO_SMALL;
				}
			else if (nbytes < buflen)
				{
				res = SW_STATUS_INSUFFICIENT_DATA;
				}

			memcpy(pData, buf.data(), nbytes);
			}

		return res;
		}



