/**
*** @file		SWResource.h
*** @brief		An object to represent a single resource entity
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWResource class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWResource_h__
#define __SWResource_h__

#include <swift/SWObjectArray.h>
#include <swift/SWString.h>



// Forward declarations
class SWResourceModule;


/**
*** @brief	An object to represent a single resource entity
*** @author	Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWResource : public SWObject
		{
public:
		/// An enumeration of resource types
		enum ResourceType
			{
			None=0,				///< A null resource
			String=6,			///< A string resource
			MessageTable=11		///< A string resource from a messagetable.
			};
		
public:
		/**
		*** @brief	Default constructor
		***
		*** Constructs a single resource accoring to the given parameters
		***
		*** @param[in]	id		The ID of the resource
		*** @param[in]	type	The type of the resource
		*** @param[in]	data	The resource data
		*** @param[in]	size	The size of the resource data
		**/
		SWResource(sw_resourceid_t id=0, ResourceType type=None, void *data=0, int size=0);

		/// Virtual destructor
		virtual ~SWResource();

		/// Copy constructor
		SWResource(const SWResource &other);

		/// Assignment operator
		SWResource &	operator=(const SWResource &other);

		/// Return the ID of this resource
		sw_resourceid_t		id() const			{ return m_id; }

		/// return the size of the resource data in bytes
		int					size() const		{ return m_size; }

		/// return the type of the resource data in bytes
		ResourceType		type() const		{ return m_type; }

		/// return a pointer to the resource data
		const void *		data() const		{ return m_pData; }

		/// HashCode info (for collections, etc)
		virtual sw_uint32_t		hashCode() const	{ return m_id; }


public: // static methods

		/// Internal method to initialise the resource module list.
		static void					initModuleList();

		/// Add a module to the end of the global module list.
		static void					addModule(SWResourceModule *pModule);

		/// Insert a module into the global module list at the specified position.
		static void					insertModule(SWResourceModule *pModule, int position);

		/// Remove all instances of the specified module from the global module list.
		static void					removeModule(SWResourceModule *pModule);

		/// Remove the module from the specified position of the global module list
		static void					removeModule(int pos);

		/// Return a count of the modules in the global list
		static sw_size_t			getModuleCount();

		/// Return a pointer to the specified module in the global list.
		static SWResourceModule *	getModule(int pos);

		/// Load a string matching the specified ID and type.
		static bool					loadString(sw_resourceid_t id, SWString &str, ResourceType rtype=SWResource::String);

private: // static members
		static SWObjectArray	sm_moduleArray;	///< An array of resource modules to be searched for resources
		static bool				sm_initdone;	///< A boolean to indicate the init status of the module list

private:
		sw_resourceid_t		m_id;		///< The resource ID
		ResourceType		m_type;		///< The type of the resource
		int					m_size;		///< The size in bytes of the resource
		const void			*m_pData;	///< The pointer to the data of the resource
		};




#endif
