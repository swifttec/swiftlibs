/**
*** @file		SWReferencedObject.h
*** @brief		A basic object which allows reference counting.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWReferencedObject class.
**/

/*********************************************************************************
Copyright (c) 2006-2017 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#ifndef __swift_SWReferencedObject_h__
#define __swift_SWReferencedObject_h__

#include <swift/SWLockableObject.h>

/**
*** @brief		A basic object which allows reference counting.
***
***	A simple object which allows reference counting. It is expected that other objects
*** will derive themselves from this class. It is derived from the SWLockableObject
*** class to allow individual objects to be locked
**/
class SWIFT_DLL_EXPORT SWReferencedObject : public SWLockableObject
		{
public:
		/// Constructor
		SWReferencedObject(bool syncflag);

		/// Destructor
		~SWReferencedObject();

		/// Increase and return the ref count
		int			incRefCount();
		
		/// Decrease and return the ref count
		int			decRefCount();
		
		/// Get the ref count
		int			getRefCount();

private:
		int			m_refcount;		///< The number of references to this object
		};

#endif // __swift_ReferencedObject_h__
