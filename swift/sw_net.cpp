/**
*** @file		sw_net.cpp
*** @brief		A collection of useful network functions
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains a collection of useful network functions
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"
#include <swift/sw_net.h>

#include <swift/SWGuard.h>
#include <swift/SWTime.h>
#include <swift/sw_misc.h>
//#include <swift/sw_string.h>
//#include <swift/SWOSVersionInfo.h>
//#include <swift/SWRegistryKey.h>
//#include <swift/SWRegistryValue.h>
//#include <swift/SWTokenizer.h>
#include <swift/SWThreadMutex.h>

//#include <swift/SWRawSocket.h>
//#include <swift/SWHostname.h>
//#include <swift/SWNetworkAdapter.h>

#if defined(SW_PLATFORM_WINDOWS)
	#include <Iphlpapi.h>
	#include <Iptypes.h>
	#include <IPIfCons.h>
	#include <lm.h>
	#include <NtSecApi.h>

	// #pragma comment(lib, "netapi32.lib")
	#pragma comment(lib, "Ws2_32.lib")

	#pragma warning(disable: 4996)

#elif defined(linux)
	#include <netinet/ip_icmp.h>
	#include <netdb.h>
#elif defined(SW_PLATFORM_MACH)
	#include <netdb.h>
#endif

#include <stdio.h>


#if !defined(SW_PLATFORM_WINDOWS)
	#include <netinet/in.h>
	#include <inttypes.h>
	#include <arpa/inet.h>
	#include <sys/utsname.h>
#else
	#include <errno.h>
	#if defined(SW_BUILD_MFC)
		#include <winsock2.h>
	#endif
#endif

/*
** This is required under windows in order to initialise the sockets
*/
static bool				sw_net_init_done = false;
static SWThreadMutex	sw_net_mutex;


/**
*** sw_net_init initialises the network layer for the given platform.
***
*** This call is not required (even though it is supported) by unix and
*** is primarily there to allow winsock (win32) to be initialised.
***
*** sw_net_init is called automatically by the SW network classes and is not
*** required to be done explicitly. However it is provided to allow the caller
*** to control the action on failure.
***
*** @param stopOnFail	If this flag is true and the net layer initialisation
***			fails, then an fatal error message is produced followed
***			by a call to exit(1). If the flag is false and the net
***			layer initialisation fails the function returns -1.
***
*** @return 0=ok, -ve otherwise.
**/
SWIFT_DLL_EXPORT sw_status_t
sw_net_init(bool stopOnFail)
		{
		SWMutexGuard	guard(sw_net_mutex);

		if (sw_net_init_done) return SW_STATUS_SUCCESS;

#if defined(SW_PLATFORM_WINDOWS)
		WORD	wVersionRequested;
		WSADATA	wsaData;
		int	err;
	 
		wVersionRequested = MAKEWORD(2, 0);
	 
		err = WSAStartup(wVersionRequested, &wsaData);
		if (err != 0)
			{
			if (stopOnFail)
				{
				char	tmp[128];

				sprintf(tmp, "No WinSock DLL found (err=%d)", err);

				throw SW_EXCEPTION_TEXT(SWNetException, tmp);
				}

			return WSAGetLastError();
			}
	 
		// Confirm that the WinSock DLL supports 2.
		// Note that if the DLL supports versions greater
		// than 2.2 in addition to 2.2, it will still return
		// 2.2 in wVersion since that is the version we
		// requested.
		/*
		if (LOBYTE(wsaData.wVersion) != 2 || HIBYTE(wsaData.wVersion) != 2 )
			{
			// Tell the user that we could not find a usable
			// WinSock DLL.
			WSACleanup( );
			return; 
			}
		*/

		/* The WinSock DLL is acceptable. Proceed. */
#else
		/// Other platforms do not require any initialisation
		SW_UNREFERENCED_PARAMETER(stopOnFail);
#endif
		sw_net_init_done = true;

		return SW_STATUS_SUCCESS;
		}


/**
*** Test to see of the network has been initialised.
***
*** @return	true if the network is initialsed, false otherwise
**/
SWIFT_DLL_EXPORT bool
sw_net_initialised()
		{
		return sw_net_init_done;
		}


SWIFT_DLL_EXPORT sw_status_t
sw_net_getLastError()
		{
		sw_status_t	res;

#if defined(WIN32) || defined(_WIN32)
		res = ::WSAGetLastError();
#else
		res = errno;
#endif

		return res;
		}



// Convert from a host (platform-specific) to a network (platform-independent) long (32-bit integer)
SWIFT_DLL_EXPORT sw_uint32_t
sw_net_htonl(sw_uint32_t v)
		{
		return htonl(v);
		}

// Convert from a network (platform-independent) to a host (platform-specific) long (32-bit integer)
SWIFT_DLL_EXPORT sw_uint32_t
sw_net_ntohl(sw_uint32_t v)
		{
		return ntohl(v);
		}

// Convert from a host (platform-specific) to a network (platform-independent) short (16-bit integer)
SWIFT_DLL_EXPORT sw_uint16_t
sw_net_htons(sw_uint16_t v)
		{
		return htons(v);
		}

// Convert from a network (platform-independent) to a host (platform-specific) short (16-bit integer)
SWIFT_DLL_EXPORT sw_uint16_t
sw_net_ntohs(sw_uint16_t v)
		{
		return ntohs(v);
		}





#ifdef NOT_YET







#if defined(_WIN32) || defined(WIN32)
/*
** The following code section is a win32-special version of ping
** that does not require admin rights on the local machine.
*/
typedef HANDLE (WINAPI* pfnHV)(VOID);
typedef BOOL (WINAPI* pfnBH)(HANDLE);
typedef DWORD (WINAPI* pfnDHDPWPipPDD)(HANDLE, DWORD, LPVOID, WORD, PIP_OPTION_INFORMATION, LPVOID, DWORD, DWORD); // evil, no?

typedef struct 
	{
	DWORD Address;                             // Replying address
	unsigned long  Status;                     // Reply status
	unsigned long  RoundTripTime;              // RTT in milliseconds
	unsigned short DataSize;                   // Echo data size
	unsigned short Reserved;                   // Reserved for system use
	void *Data;                                // Pointer to the echo data
	IP_OPTION_INFORMATION Options;             // Reply options
	} IP_ECHO_REPLY, *PIP_ECHO_REPLY;


static HINSTANCE	hIcmp=0;
static pfnHV		pIcmpCreateFile=0;
static pfnBH		pIcmpCloseHandle=0;
static pfnDHDPWPipPDD	pIcmpSendEcho=0;
static bool		useIcmpDllFlag=false;

/**
*** Function to cleanup from the use of ICMP.DLL
***
*** This function is not yet called, but included for completeness
*** and intended to be linked via onexit.
**/
extern "C" static int
sw_net_internal_dll_ping_cleanup()
	{
	/*
	//SWMutexGuard	guard(sw_net_mutex);
	sw_log_trace0(_T("sw_net_internal_dll_ping_cleanup"));

	pIcmpCreateFile = 0;
	pIcmpCloseHandle = 0;
	pIcmpSendEcho = 0;

	if (hIcmp != 0)
	    {
	    FreeLibrary(hIcmp);
	    hIcmp = 0;
	    }
	*/

	return 0;
	}


/**
*** Internal version of ping via ICMP.DLL for win32 only
***
*** This code was adapted from public domain code found at the
*** following URL:
***
*** http://tangentsoft.net/wskfaq/examples/dllping.html
***
*** @param host		The host to ping
*** @param retries	The number of times to attempt a ping before giving up.
*** @param timeout	The timeout time in milliseconds before retrying
*** @param nbytes	The number of bytes to send in the ping packet
***
*** @return		Returns 0 for success or <0 for failure.
**/
static int
sw_net_internal_dll_ping(SWSocketAddress &dest, int retries, int timeout, int nbytes)
	{
	HANDLE		hIP=0;
	char	acPingBuffer[64];

	//sw_log_trace3(_T("sw_net_internal_dll_ping - pinging %s (retries=%d,  timeout=%d ms)"), dest.ipaddr().toString().data(), retries, timeout);

	{ // BEGIN guarded section
	// Load the ICMP.DLL and initialise the function pointers.

	SWMutexGuard	guard(sw_net_mutex);
	if (hIcmp == 0)
	    {
	    hIcmp = LoadLibrary(_T("ICMP.DLL"));
	    if (hIcmp == 0)
		{
		//sw_log_trace0(_T("sw_net_internal_dll_ping - Failed to load ICMP.DLL"));
		return -100;
		}

	    //sw_log_trace0(_T("sw_net_internal_dll_ping - Loaded ICMP.DLL"));

	    if (pIcmpCreateFile == 0) pIcmpCreateFile = (pfnHV)GetProcAddress(hIcmp, "IcmpCreateFile");
	    if (pIcmpCloseHandle == 0) pIcmpCloseHandle = (pfnBH)GetProcAddress(hIcmp, "IcmpCloseHandle");
	    if (pIcmpSendEcho == 0) pIcmpSendEcho = (pfnDHDPWPipPDD)GetProcAddress(hIcmp, "IcmpSendEcho");

	    if (pIcmpCreateFile == 0 || pIcmpCloseHandle == 0 || pIcmpSendEcho == 0)
		{
		//sw_log_trace0(_T("sw_net_internal_dll_ping - Failed to get proc addrs in ICMP.DLL"));
		return -101;
		}
	    }
	} // END guarded section

	if (pIcmpCreateFile == 0 || pIcmpCloseHandle == 0 || pIcmpSendEcho == 0) return -101;

	// Open the ping service
	hIP = pIcmpCreateFile();
	if (hIP == INVALID_HANDLE_VALUE)
	    {
	    //sw_log_trace0(_T("sw_net_internal_dll_ping - Unable to open ping service."));
	    return -102;
	    }

	// Build ping packet
	memset(acPingBuffer, '\xAA', sizeof(acPingBuffer));

	PIP_ECHO_REPLY	pIpe=0;
	pIpe = (PIP_ECHO_REPLY)GlobalAlloc(GMEM_FIXED|GMEM_ZEROINIT, sizeof(IP_ECHO_REPLY) + sizeof(acPingBuffer));
	if (pIpe == 0) 
	    {
	    //sw_log_trace0(_T("sw_net_internal_dll_ping - Failed to allocate global ping packet buffer."));
	    return -103;
	    }

	pIpe->Data = acPingBuffer;
	pIpe->DataSize = sizeof(acPingBuffer);      

	// Send the ping packet
	DWORD	dwStatus;
	int	retcode=-1;
	int	i;
	
	for (i=0;i<retries;i++)
	    {
	    //sw_log_trace4(_T("sw_net_internal_dll_ping - Pinging %s with %d bytes of data (try=%d, limit=%d)"), dest.ipaddr().toString().data(), nbytes, i+1, retries);
	    dwStatus = pIcmpSendEcho(hIP, dest.ipaddr(), acPingBuffer, sizeof(acPingBuffer), NULL, pIpe, sizeof(IP_ECHO_REPLY) + sizeof(acPingBuffer), timeout);
	    if (dwStatus != 0)
	    	{
		retcode = 0;
		break;
		}
	    }

	/*
	if (dwStatus != 0) 
	    {
	    cout << "Addr: " <<
	    int(LOBYTE(LOWORD(pIpe->Address))) << "." <<
	    int(HIBYTE(LOWORD(pIpe->Address))) << "." <<
	    int(LOBYTE(HIWORD(pIpe->Address))) << "." <<
	    int(HIBYTE(HIWORD(pIpe->Address))) << ", " <<
	    "RTT: " << int(pIpe->RoundTripTime) << "ms, " <<
	    "TTL: " << int(pIpe->Options.Ttl) << endl;
	    }
	else {
	cerr << "Error obtaining info from ping packet." << endl;
	}
	*/

	// Shut down...
	if (pIpe != 0)
	    {
	    GlobalFree(pIpe);
	    pIpe = 0;
	    }

	if (hIP != 0)
	    {
	    pIcmpCloseHandle(hIP);
	    hIP = 0;
	    }

	return retcode;
	}
#endif




/*
 *			I N _ C K S U M
 *
 * Checksum routine for Internet Protocol family headers (C Version)
 *
 */
static int
in_cksum(u_short *addr, int len)
	{
	register int		nleft=len;
	register u_short	*w=addr;
	register u_short	answer;
	register int		sum=0;

	/*
	 *  Our algorithm is simple, using a 32 bit accumulator (sum),
	 *  we add sequential 16 bit words to it, and at the end, fold
	 *  back all the carry bits from the top 16 bits into the lower
	 *  16 bits.
	 */
	while (nleft > 1)
	    {
	    sum += *w++;
	    nleft -= 2;
	    }

	/* mop up an odd byte, if necessary */
	if (nleft == 1)
	    {
	    u_short	u = 0;

	    *(u_char *)(&u) = *(u_char *)w ;
	    sum += u;
	    }

	/*
	 * add back carry outs from top 16 bits to low 16 bits
	 */
	sum = (sum >> 16) + (sum & 0xffff);	/* add hi 16 to low 16 */
	sum += (sum >> 16);			/* add carry */
	answer = ~sum;				/* truncate to 16 bits */

	return answer;
	}

#if defined(linux)
	typedef struct icmphdr	sw_net_icmph_t;
#else
	typedef icmph_t			sw_net_icmph_t;
#endif

static int
sendEchoRequest(SWRawSocket &sock, SWSocketAddress &addr, int nbytes)
	{
	static int	echoId=1;
	static int	echoSeq=1;

	sw_net_icmph_t	*ep;
	int		i, r, bufsize;
	SWBuffer	buf;
	char		*bp, *dp;

	if (nbytes < 0) nbytes = 0;
	buf.setSize((bufsize = sizeof(sw_net_icmph_t) + nbytes));
	buf.clearContents();
	bp = buf;

	ep = (sw_net_icmph_t *)bp;
	dp = bp + sizeof(sw_net_icmph_t);

#if defined(linux)
	ep->type = ICMP_ECHO;
	ep->code = 0;
	ep->checksum = 0;
	ep->un.echo.id = echoId++;
	ep->un.echo.sequence = echoSeq++;
#else
	ep->icmph_type = ICMP_ECHO_REQUEST;
	ep->icmph_code = 0;
	ep->icmph_checksum = 0;
	ep->icmph_u.u_echo.u_echo_ident = echoId++;
	ep->icmph_u.u_echo.u_echo_seqnum = echoSeq++;
#endif

	// Fill in some data to send
	for (i=0;i<nbytes;i++) *dp++ = i & 0xff;

	// Put data in packet and compute checksum
#if defined(linux)
	ep->checksum = in_cksum((u_short *)bp, bufsize);
#else
	ep->icmph_checksum = in_cksum((u_short *)bp, bufsize);
#endif

	// Send the echo request  								  
	if (sock.sendTo(buf, addr)) r = bufsize;
	else r = -1;

	return r;
	}


static int
receiveEchoReply(SWRawSocket &sock, SWSocketAddress &addr, int nbytes, int ms)
	{
	int	r;

	if ((r = sock.waitForPacket(ms)) <= 0) return r;

	SWBuffer	buf;

	if (!sock.receiveFrom(buf, addr)) return -1;

	return buf.size();
	}


/**
*** Internal version of ping
***
*** @param host		The host to ping
*** @param retries	The number of times to attempt a ping before giving up.
*** @param timeout	The timeout time in milliseconds before retrying
*** @param nbytes	The number of bytes to send in the ping packet
***
*** @return		Returns 0 for success or <0 for failure.
**/
static int
sw_net_internal_ping(SWSocketAddress &dest, int retries, int timeout, int nbytes)
	{
	SWSocketAddress	src;
	int		i, r;
	SWRawSocket	sock(IPPROTO_ICMP);

	//sw_log_trace3(_T("sw_net_ping - pinging %s (retries=%d,  timeout=%d ms)"), dest.ipaddr().toString().data(), retries, timeout);

#if defined(WIN32) || defined(_WIN32)
	if (!ca_net_initialised()) sw_net_init();

	if (useIcmpDllFlag)
	    {
	    return sw_net_internal_dll_ping(dest, retries, timeout, nbytes);
	    }
#endif

	//sw_log_trace3(_T("sw_net_ping - pinging %s (retries=%d,  timeout=%d ms)"), dest.ipaddr().toString().data(), retries, timeout);

	if (sock.open() < 0)
	    {
#if !defined(WIN32) && !defined(_WIN32)
	    // We are probably on a unix machine where raw sockets
	    // require supervisor access. Try running the ping program quietly
	    SWString	cmd;

	    cmd.format(_T("ping %s > /dev/null"), dest.ipaddr().toString().data());
	    r = t_system(cmd);
	    if (r == 0) return 0;
#endif

	    //sw_log_trace1(_T("sw_net_ping - Failed to open socket (errno=%d)"), errno);
	    return -1;
	    }


	for (i=0;i<retries;i++)
	    {
	    // Send ICMP echo request
	    //sw_log_trace4(_T("sw_net_ping - Pinging %s with %d bytes of data (try=%d, limit=%d)"), dest.ipaddr().toString().data(), nbytes, i+1, retries);
	    r = sendEchoRequest(sock, dest, nbytes);
	    if (r < 0)
		{
		//sw_log_trace1(_T("sw_net_ping - Failed to send echo request - errno=%d"), errno);

#if defined(WIN32) || defined(_WIN32)
		if (WSAGetLastError() == WSAEACCES)
		    {
		    // We are probably running as a non-admin user so we switch
		    // to the ICMP.DLL methods which ping.exe and friends use.
		    //sw_log_trace0(_T("sw_net_ping - switching to ICMP.DLL mode"));

		    useIcmpDllFlag = true;
		    return sw_net_internal_dll_ping(dest, retries, timeout, nbytes);
		    }
#endif
		return -6;
		}

	    SWTime	then;

	    for (;;)
		{
		SWTime	now;
		int	waitms;

		waitms = timeout - (int)(((double)now - (double)then) * 1000.0);
		if (waitms <= 0)
		    {
		    r = 0;
		    break;
		    }

		r = receiveEchoReply(sock, src, nbytes, waitms);
		if (r < 0)
		    {
		    //sw_log_trace1(_T("sw_net_ping - socket error - errno %d"), errno);
		    return -3;
		    }

// Need to verify this code further. It may be possible that if we have a machine
// with 2 or more addresses on a subnet that if we ping 1 the answer may come back
// another.
		
		// If we have a timeout OR the src and destination match break out the loop
		if (r == 0 || src.ipaddr() == dest.ipaddr()) break;

		//sw_log_trace2(_T("sw_net_ping - got unexpected reply for %s from %s"), dest.ipaddr().toString().data(), src.ipaddr().toString().data());
		}

	    if (r == 0)
		{
		//sw_log_trace0(_T("sw_net_ping - timeout"));
		}
	    else
		{
		// Receive reply
		SWTime	now;
		int	elapsed = (int)(((double)now-(double)then)*1000.0);

		//sw_log_trace3(_T("sw_net_ping - got reply from %s time=%dms TTL=%d"), src.ipaddr().toString().data(), elapsed, 0);
		return 0;
		}
	    }

	return -1;
	}


/**
*** Attempt to ping the given host.
***
*** @param host		The host to ping
*** @param retries	The number of times to attempt a ping before giving up.
*** @param timeout	The timeout time in milliseconds before retrying
*** @param nbytes	The number of bytes to send in the ping packet
***
*** @return		Returns 0 for success or <0 for failure.
**/
SWIFT_DLL_EXPORT int			
sw_net_ping(SWHostname &host, int retries, int timeout, int nbytes)
	{
	SWSocketAddress	dest;

	// Setup destination socket address
	dest.setPort(0);

	if (host.addressCount() == 0)
	    {
	    //sw_log_trace1(_T("sw_net_ping - Failed resolve host %s"), host.toString().data());
	    return -2;
	    }

	// We disable reverse lookup here to improve performance
	dest.setAddress(host.address(0), false);

	// Call internal ping to do the real work
	return sw_net_internal_ping(dest, retries, timeout, nbytes);
	}


/**
*** Attempt to ping the given host.
***
*** @param ipaddr	The ip address to ping
*** @param retries	The number of times to attempt a ping before giving up.
*** @param timeout	The timeout time in milliseconds before retrying
*** @param nbytes	The number of bytes to send in the ping packet
***
*** @return		Returns 0 for success or <0 for failure.
**/
SWIFT_DLL_EXPORT int			
sw_net_ping(SWIPAddress &ipaddr, int retries, int timeout, int nbytes)
	{
	SWSocketAddress	dest;

	// Setup destination socket address
	dest.setPort(0);

	// We disable reverse lookup here to improve performance
	dest.setAddress(ipaddr, false);

	// Call internal ping to do the real work
	return sw_net_internal_ping(dest, retries, timeout, nbytes);
	}


/**
*** Attempt to ping the given host.
***
*** @param host		The host name or ip address to ping
*** @param retries	The number of times to attempt a ping before giving up.
*** @param timeout	The timeout time in milliseconds before retrying
*** @param nbytes	The number of bytes to send in the ping packet
***
*** @return		Returns 0 for success or <0 for failure.
**/
SWIFT_DLL_EXPORT int
sw_net_ping(const SWString &host, int retries, int timeout, int nbytes)
	{
	SWHostname	hostname(host);

	// Call internal ping to do the real work
	return sw_net_ping(hostname, retries, timeout, nbytes);
	}


/**
*** Tests to see if this is a valid IP address
**/
SWIFT_DLL_EXPORT bool
sw_net_isIPAddress(const SWString &host)
	{
	int	n, a, b, c, d;
	bool	ok=false;

#if defined(WIN32) || defined(_WIN32)
	if (!ca_net_initialised()) sw_net_init();
#endif

	n = t_sscanf(host.data(), _T("%d.%d.%d.%d"), &a, &b, &c, &d);
	if (n == 4)
	    {
	    if (a >=0 && a <= 255 && b >=0 && b <= 255 && c >=0 && c <= 255 && d >=0 && d <= 255) ok = true;
	    }

	return ok;
	}


/**
*** Returns the hostname for the given host name or IP address string
**/
SWIFT_DLL_EXPORT SWString
sw_net_hostname(const SWString &name)
	{
#if defined(WIN32) || defined(_WIN32)
	if (!ca_net_initialised()) sw_net_init();
#endif

	SWHostname	hn(name);
	SWString	res;

	if (hn.isValid()) res = (const TCHAR *)hn;

	return res;
	}


#if defined(WIN32) || defined(_WIN32)
/**
***	Lookup primary domain info in the LSA
***	Name returned will be name of workgroup or domain (no distinction made)
***	but also get back SID for domain (none for workgroup)
***	Hence test for if server is in a domain is to test for existance of SID
***  returns TRUE if local server is in a domain
**/
static int
sw_net_internal_isServerInDomain(SWString&	_sDomain)
		{
		BOOL							bInDomain = FALSE;
		NTSTATUS						nRet;
		LSA_HANDLE						hPolicyHandle;
		LSA_OBJECT_ATTRIBUTES			ObjectAttributes;
		PPOLICY_PRIMARY_DOMAIN_INFO		PrimaryDomainInfo;

		//DB((DB_PR"IsServerInDomain: entry"));

		// open the LSA policy
		memset( &ObjectAttributes, 0, sizeof(LSA_OBJECT_ATTRIBUTES) );
		//InitializeObjectAttributes( &ObjectAttributes,
		//							NULL,					// name
		//							OBJ_SWSE_INSENSITIVE,
		//							NULL,					// root
		//							NULL );					// SD
		nRet = LsaOpenPolicy(NULL,
							 &ObjectAttributes,
							 POLICY_ALL_ACCESS,
							 &hPolicyHandle );
		if (ERROR_SUCCESS == nRet )
		{
			// get the server role
			nRet = LsaQueryInformationPolicy(hPolicyHandle, PolicyPrimaryDomainInformation, (PVOID*)(&PrimaryDomainInfo) );
			if (ERROR_SUCCESS == nRet )
			{
				bInDomain = (PrimaryDomainInfo->Sid) ? TRUE : FALSE;
				if (bInDomain)
				{
					//DB(( DB_PR"RntIsServerInDomain: Server is in a DOMAIN\n" ));
					WCHAR	*wtmp = new WCHAR[PrimaryDomainInfo->Name.Length+1];

					wcsncpy(wtmp, PrimaryDomainInfo->Name.Buffer, PrimaryDomainInfo->Name.Length / sizeof(WCHAR));
					wtmp[(PrimaryDomainInfo->Name.Length  / sizeof(WCHAR))] = 0;

					_sDomain = wtmp;//SWString(PrimaryDomainInfo->Name.Buffer,  PrimaryDomainInfo->Name.Length/sizeof(WCHAR));
					delete wtmp;
				}
				else
					{
					//DB(( DB_PR"RntIsServerInDomain: Server is NOT in a domain\n" ));
					WKSTA_INFO_100* pwsa = NULL;
					NetWkstaGetInfo(NULL, 100, (LPBYTE*)&pwsa);
					if (pwsa)
						{
						_sDomain = pwsa->wki100_langroup;
						NetApiBufferFree(pwsa);
						}

					}

				//DB(( DB_PR"Domain/Workgroup Name is %.*ls\n", PrimaryDomainInfo->Name.Length, PrimaryDomainInfo->Name.Buffer ) );
				}
			else
				{
				//DBERR(( DB_PR"LsaQueryInformationPolicy: %d\n", LsaNtStatusToWinError(ret) ));
				}

			nRet = LsaClose(hPolicyHandle );
			if (ERROR_SUCCESS != nRet )
				{
				//DBERR(( DB_PR"LsaClose: %d\n", LsaNtStatusToWinError(ret) ));
				}
			}
		else
			{
			//DBERR(( DB_PR"LsaOpenPolicy: %d\n", LsaNtStatusToWinError(ret) ));
			}

		//DB((DB_PR"IsServerInDomain: exit (%S)", inDomain?L"TRUE":L"FALSE"));
		return (int)bInDomain;
		}
#endif // defined(WIN32) || defined(_WIN32)


static SWString	__domainOrWorkgroupName;
static bool	__inDomainFlag=false;
static bool	__inWorkgroupFlag=false;
static bool	__haveDomainOrWorkgroupInfo=false;

/**
*** Tests to see if the machine is in a windows domain.
***
*** If the machine is in a domain the function returns true and
*** optionally returns the domain name if a SWString pointer is
*** provided.
***
*** If the machine is NOT in a domain the function returns false
*** and the optional domain name will be set to blank if a pointer
*** is proivded.
***
*** Currently non-windows machines will always return false
**/
SWIFT_DLL_EXPORT bool
sw_net_isInWindowsDomain(SWString *pDomainName)
		{
		if (!__haveDomainOrWorkgroupInfo)
			{
#if defined(WIN32) || defined(_WIN32)
			// Get the windows info
			__inDomainFlag = sw_net_internal_isServerInDomain(__domainOrWorkgroupName);
			__inWorkgroupFlag = !__inDomainFlag;
#else
			// For unix systems do nothing
#endif
			__haveDomainOrWorkgroupInfo = true;
			}

		if (pDomainName != NULL)
			{
			if (__inDomainFlag) *pDomainName = __domainOrWorkgroupName;
			else pDomainName->empty();
			}

		return __inDomainFlag;
		}


/**
*** Tests to see if the machine is in a windows workgroup.
***
*** If the machine is in a workgroup the function returns true and
*** optionally returns the workgroup name if a SWString pointer is
*** provided.
***
*** If the machine is NOT in a workgroup the function returns false
*** and the optional workgroup name will be set to blank if a pointer
*** is proivded.
***
*** Currently non-windows machines will always return false
**/
SWIFT_DLL_EXPORT bool
sw_net_isInWindowsWorkgroup(SWString *pWorkgroupName)
		{
		if (!__haveDomainOrWorkgroupInfo)
			{
#if defined(WIN32) || defined(_WIN32)
			// Get the windows info
			__inDomainFlag = sw_net_internal_isServerInDomain(__domainOrWorkgroupName);
			__inWorkgroupFlag = !__inDomainFlag;
#else
			// For unix systems do nothing
#endif
			__haveDomainOrWorkgroupInfo = true;
			}

		if (pWorkgroupName != NULL)
			{
			if (__inWorkgroupFlag) *pWorkgroupName = __domainOrWorkgroupName;
			else pWorkgroupName->empty();
			}

		return __inWorkgroupFlag;
		}
#endif // NOT_YET

SWIFT_DLL_EXPORT unsigned long
sw_net_inet_addr(const char* cp)
		{
		return inet_addr(cp);
		}


SWIFT_DLL_EXPORT SWString
sw_net_localhostname()
		{
		static SWString	myname;

		if (!myname.isEmpty()) return myname;

		if (!sw_net_initialised()) sw_net_init();

#ifndef MAXHOSTNAMELEN
#define MAXHOSTNAMELEN	256
#endif

		char	name[MAXHOSTNAMELEN];
		if (gethostname(name, sizeof(name)) == 0) myname = name;

#if !defined(SW_PLATFORM_WINDOWS)
		if (myname.isEmpty())
			{
			struct utsname	ut;

			uname(&ut);
			myname = ut.nodename;
			}
#endif

		return myname;
		}



SWIFT_DLL_EXPORT struct hostent *
sw_net_gethostbyname(const char *name)
		{
		return gethostbyname(name);
		}


SWIFT_DLL_EXPORT struct hostent *
sw_net_gethostbyaddr(const void *addr, socklen_t len, int type)
		{
		return gethostbyaddr((const char *)addr, len, type);
		}
