/**
*** @file		SWUUID.cpp
*** @brief		A class to represent a set of boolean flags.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWUUID class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <swift/SWUUID.h>
#include <swift/SWTime.h>
#include <swift/SWException.h>
#include <swift/SWMemoryInputStream.h>
#include <swift/SWMemoryOutputStream.h>
#include <swift/SWThread.h>
#include <swift/SWProcess.h>
#include <swift/sw_printf.h>
#include <xplatform/sw_math.h>

#if defined(SW_PLATFORM_WINDOWS)
	#include <Nb30.h>

	#pragma comment(lib, "Netapi32.lib")
#else
	#include <unistd.h>
	#include <netdb.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>
	#include <netinet/tcp.h>
	#if defined(SW_PLATFORM_AIX) || defined(SW_PLATFORM_MACH)
		#include <net/if.h>
		#include <sys/param.h>
		#include <sys/ioctl.h>
		#include <net/if_dl.h>
		#include <ifaddrs.h>
	#endif
	#include <net/if_arp.h>
	#if defined(SW_PLATFORM_SOLARIS)
		#include <sys/sockio.h>
	#elif defined(SW_PLATFORM_LINUX)
		#include <sys/ioctl.h>
		#include <linux/sockios.h>
		#include <linux/if.h>
		#include <asm/param.h>
	#elif defined(SW_PLATFORM_OSF1)
		#include <sys/param.h>
		#include <sys/ioctl.h>
		
		int gethostid ( void );
	#endif
#endif

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif



#if defined(SW_PLATFORM_MACH)
/*
** Find the first mac address for this computer.
** returns: 0 Success, -1 error, 1 not found
*/
static int
getFirstMacAddress(sw_byte_t *addr)
		{
		struct ifaddrs	*if_addrs=NULL;
		struct ifaddrs	*if_addr=NULL;
		int				res=1;

		if (getifaddrs(&if_addrs) == 0)
			{    
			for (if_addr = if_addrs; if_addr != NULL; if_addr = if_addr->ifa_next) 
				{
				// MAC address
				if (if_addr->ifa_addr != NULL && if_addr->ifa_addr->sa_family == AF_LINK)
					{
					struct sockaddr_dl	*sdl = (struct sockaddr_dl *)if_addr->ifa_addr;
					unsigned char mac[6];

					if (sdl->sdl_alen == 6)
						{
						memcpy(mac, LLADDR(sdl), sdl->sdl_alen);
						printf("mac  : %02x:%02x:%02x:%02x:%02x:%02x\n", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

						if (mac[0] != 0 ||mac[1] != 0 ||mac[2] != 0 ||mac[3] != 0 ||mac[4] != 0 ||mac[5] != 0)
							{
							// We have found a valid Mac address
							memcpy(addr, mac, 6);
							res = 0;
							}
						}
					}

				printf("\n");
				}

			freeifaddrs(if_addrs);
			if_addrs = NULL;
			}
		else
			{
			// printf("getifaddrs() failed with errno =  %i %s\n", errno, strerror(errno));
			res = -1;
			}

		return res;
		}
#endif


static void
getNodeInfo(sw_byte_t *pNodeInfo)
		{
		static sw_byte_t	nodeinfo[6];
		static bool			nodeinfovalid=false;

		if (!nodeinfovalid)
			{
			memset(nodeinfo, 0, 6);

#if defined(SW_PLATFORM_WINDOWS)
			/** Define a structure for use with the netbios routine */
			struct ADAPTERSTAT
				{
				ADAPTER_STATUS adapt;
				NAME_BUFFER    NameBuff [30];
				};

			NCB				ncb;
			LANA_ENUM		lenum;
			unsigned char	result;

			memset (&ncb, 0, sizeof(ncb));
			ncb.ncb_command = NCBENUM;
			ncb.ncb_buffer  = (unsigned char *)&lenum;
			ncb.ncb_length  = sizeof(lenum);

			result = Netbios (&ncb);

			for (int i=0;i<lenum.length;i++)
				{
				memset(&ncb, 0, sizeof(ncb));
				ncb.ncb_command  = NCBRESET;
				ncb.ncb_lana_num = lenum.lana [i];

				/** Reset the netbios */
				result = Netbios (&ncb);

				if (ncb.ncb_retcode == NRC_GOODRET)
					{
					ADAPTERSTAT	adapter;

					memset (&ncb, 0, sizeof (ncb));
					strcpy((char *)ncb.ncb_callname, "*");
					ncb.ncb_command     = NCBASTAT;
					ncb.ncb_lana_num    = lenum.lana[i];
					ncb.ncb_buffer      = (unsigned char *)&adapter;
					ncb.ncb_length      = sizeof (adapter);

					result = Netbios (&ncb);

					if (result == 0)
						{
						memcpy(nodeinfo, adapter.adapt.adapter_address, 6);
						break;
						}
					}
				}

#else
	#if defined(SW_PLATFORM_MACH)
			if (getFirstMacAddress(nodeinfo) == 0)
				{
				nodeinfovalid = true;
				}
	#elif defined(SW_PLATFORM_LINUX)
			struct ifreq ifr;

			int	 handle = socket(PF_INET, SOCK_DGRAM, 0);

			if (handle >= 0)
				{
				strcpy (ifr.ifr_name, "eth0");

				if (ioctl(handle, SIOCGIFHWADDR, &ifr) >= 0)
					{
					struct sockaddr* sa = (struct sockaddr *) &ifr.ifr_addr;

					memcpy(nodeinfo, sa->sa_data, 6);
					nodeinfovalid = true;
					}

				close(handle);
				}
	#endif

#if !defined(SW_PLATFORM_MACH)
			if (!nodeinfovalid)
				{
#ifndef MAXHOSTNAMELEN
	#define MAXHOSTNAMELEN	512
#endif
				/* obtain the local host name */
				char		hostname [MAXHOSTNAMELEN];

				gethostname(hostname, sizeof(hostname));

				/** Get the hostent to use with ioctl */
				struct hostent *phost = gethostbyname (hostname);

				if (phost != NULL)
					{
					int	 handle = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);

					if (handle >= 0)
						{
						char **paddrs = phost->h_addr_list;

						struct arpreq ar;

						struct sockaddr_in *psa = (struct sockaddr_in *)&(ar.arp_pa);

						memset (&ar, 0, sizeof (struct arpreq));

						psa->sin_family = AF_INET;

						memcpy(&(psa->sin_addr), *paddrs, sizeof (struct in_addr));

						if (ioctl(handle, SIOCGARP, &ar) >= 0)
							{
							memcpy(nodeinfo, ar.arp_ha.sa_data, 6);
							nodeinfovalid = true;
							}

						close(handle);
						}
					}
				}
#endif // !defined(SW_PLATFORM_MACH)

			if (!nodeinfovalid)
				{
				long	hostid = gethostid();

				nodeinfo[2] = (hostid >> 24) & 0xff;
				nodeinfo[3] = (hostid >> 16) & 0xff;
				nodeinfo[4] = (hostid >> 8) & 0xff;
				nodeinfo[5] = hostid & 0xff;
				}

#endif
			
			nodeinfovalid = true;
			}

		// Just copy the stored info
		memcpy(pNodeInfo, nodeinfo, 6);
		}




SWInterlockedInt32	SWUUID::sm_seq(0);


SWUUID::SWUUID()
		{
		memset(&m_uuid, 0, sizeof(m_uuid));
		}


SWUUID::SWUUID(const sw_uuid_t &other) :
	SWObject(),
	m_uuid(other)
		{
		}


SWUUID::SWUUID(const SWUUID &other) :
	SWObject(),
	m_uuid(other.m_uuid)
		{
		}


SWUUID::SWUUID(const SWString &s)
		{
		memset(&m_uuid, 0, sizeof(m_uuid));

		int	fields[11], r;

		r = sscanf(s.c_str(), "%8x-%4x-%4x-%2x%2x-%2x%2x%2x%2x%2x%2x",
			&fields[0],
			&fields[1],
			&fields[2],
			&fields[3],
			&fields[4],
			&fields[5],
			&fields[6],
			&fields[7],
			&fields[8],
			&fields[9],
			&fields[10]);

		if (r == 11)
			{
			m_uuid.timeLow = fields[0];
			m_uuid.timeMid = (sw_uint16_t)fields[1];
			m_uuid.timeHighAndVersion = (sw_uint16_t)fields[2];
			m_uuid.clockSeqHighAndVariant = (sw_uint8_t)fields[3];
			m_uuid.clockSeqLow = (sw_uint8_t)fields[4];
			m_uuid.node[0] = (sw_byte_t)fields[5];
			m_uuid.node[1] = (sw_byte_t)fields[6];
			m_uuid.node[2] = (sw_byte_t)fields[7];
			m_uuid.node[3] = (sw_byte_t)fields[8];
			m_uuid.node[4] = (sw_byte_t)fields[9];
			m_uuid.node[5] = (sw_byte_t)fields[10];
			}
		}


SWUUID::SWUUID(UUIDType ut)
		{
		if (generate(*this, ut) != SW_STATUS_SUCCESS)
			{
			throw SW_EXCEPTION(SWUUIDException);
			}
		}


SWUUID::SWUUID(const SWBuffer &buf)
		{
		bool	ok=false;

		if (buf.size() >= 16)
			{
			SWMemoryInputStream	is(buf, buf.size());

			ok = (readFromStream(is) == SW_STATUS_SUCCESS);
			}

		if (!ok) throw SW_EXCEPTION(SWUUIDException);
		}


SWUUID &
SWUUID::operator=(const SWUUID &other)
		{
		m_uuid = other.m_uuid;

		return *this;
		}



void
SWUUID::toBinary(SWBuffer &buf) const
		{
		SWMemoryOutputStream	os(buf);

		writeToStream(os);
		}


SWString
SWUUID::toString() const
		{
		SWString	s;

		s.format("%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x",
			m_uuid.timeLow,
			m_uuid.timeMid,
			m_uuid.timeHighAndVersion,
			m_uuid.clockSeqHighAndVariant,
			m_uuid.clockSeqLow,
			m_uuid.node[0],
			m_uuid.node[1],
			m_uuid.node[2],
			m_uuid.node[3],
			m_uuid.node[4],
			m_uuid.node[5]
			);

		return s;
		}


sw_status_t
SWUUID::generate(SWUUID &uuid, UUIDType ut)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		switch (ut)
			{
			case UtNil:
				memset(&uuid.m_uuid, 0, sizeof(uuid.m_uuid));
				break;

			case UtRandom:
				uuid.m_uuid.timeLow = sw_math_randomUInt32();
				uuid.m_uuid.timeMid = sw_math_randomUInt16();
				uuid.m_uuid.timeHighAndVersion = (sw_uint16_t)((sw_math_randomUInt16() & 0x0fff) | 0x4000);
				uuid.m_uuid.clockSeqHighAndVariant = (sw_uint8_t)((sw_math_randomUInt8() & 0x3f) | 0x80);
				uuid.m_uuid.clockSeqLow = sw_math_randomUInt8();
				uuid.m_uuid.node[0] = sw_math_randomUInt8();
				uuid.m_uuid.node[1] = sw_math_randomUInt8();
				uuid.m_uuid.node[2] = sw_math_randomUInt8();
				uuid.m_uuid.node[3] = sw_math_randomUInt8();
				uuid.m_uuid.node[4] = sw_math_randomUInt8();
				uuid.m_uuid.node[5] = sw_math_randomUInt8();
				break;

			case UtTime:
				{
				SWTime		now(true);
				sw_uint64_t	tv;
				sw_uint64_t	tid=(sw_uint64_t)SWThread::self();

				tv = ((sw_uint64_t)now.toInt64() * 1000000000) + now.nanosecond() + (++sm_seq);

				uuid.m_uuid.timeLow = (sw_uint32_t)(tv & 0xffffffff);
				uuid.m_uuid.timeMid = (sw_uint16_t)((tv >> 32) & 0xffff);
				uuid.m_uuid.timeHighAndVersion = (sw_uint16_t)(((tv >> 48) & 0x0fff) | 0x1000);
				uuid.m_uuid.clockSeqHighAndVariant = (sw_uint8_t)(0x80 | (sw_uint8_t)(SWProcess::self() & 0x3f));
				uuid.m_uuid.clockSeqLow = (sw_uint8_t)(tid & 0xff);
				getNodeInfo(uuid.m_uuid.node);
				}
				break;

			default:
				res = SW_STATUS_INVALID_PARAMETER;
				break;
			}

		return res;
		}


int
SWUUID::compareTo(const SWUUID &other) const
		{
		char	tmp1[64], tmp2[64];
		int		r=0;

		sw_snprintf(tmp1, sizeof(tmp1), "%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x",
			m_uuid.timeLow,
			m_uuid.timeMid,
			m_uuid.timeHighAndVersion,
			m_uuid.clockSeqHighAndVariant,
			m_uuid.clockSeqLow,
			m_uuid.node[0],
			m_uuid.node[1],
			m_uuid.node[2],
			m_uuid.node[3],
			m_uuid.node[4],
			m_uuid.node[5]
			);

		sw_snprintf(tmp2, sizeof(tmp2), "%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x",
			other.m_uuid.timeLow,
			other.m_uuid.timeMid,
			other.m_uuid.timeHighAndVersion,
			other.m_uuid.clockSeqHighAndVariant,
			other.m_uuid.clockSeqLow,
			other.m_uuid.node[0],
			other.m_uuid.node[1],
			other.m_uuid.node[2],
			other.m_uuid.node[3],
			other.m_uuid.node[4],
			other.m_uuid.node[5]
			);

		r = strcmp(tmp1, tmp2);

		return r;
		}



sw_status_t
SWUUID::writeToStream(SWOutputStream &stream) const
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (res == SW_STATUS_SUCCESS) res = stream.write(m_uuid.timeLow);
		if (res == SW_STATUS_SUCCESS) res = stream.write(m_uuid.timeMid);
		if (res == SW_STATUS_SUCCESS) res = stream.write(m_uuid.timeHighAndVersion);
		if (res == SW_STATUS_SUCCESS) res = stream.write(m_uuid.clockSeqHighAndVariant);
		if (res == SW_STATUS_SUCCESS) res = stream.write(m_uuid.clockSeqLow);
		for (unsigned int i=0;res == SW_STATUS_SUCCESS && i<sizeof(m_uuid.node);i++)
			{
			res = stream.write(m_uuid.node[i]);
			}

		return res;
		}


sw_status_t
SWUUID::readFromStream(SWInputStream &stream)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		sw_uuid_t	tmp;

		memset(&tmp, 0, sizeof(tmp));
		if (res == SW_STATUS_SUCCESS) res = stream.read(tmp.timeLow);
		if (res == SW_STATUS_SUCCESS) res = stream.read(tmp.timeMid);
		if (res == SW_STATUS_SUCCESS) res = stream.read(tmp.timeHighAndVersion);
		if (res == SW_STATUS_SUCCESS) res = stream.read(tmp.clockSeqHighAndVariant);
		if (res == SW_STATUS_SUCCESS) res = stream.read(tmp.clockSeqLow);
		for (unsigned int i=0;res == SW_STATUS_SUCCESS && i<sizeof(tmp.node);i++)
			{
			res = stream.read(tmp.node[i]);
			}

		if (res == SW_STATUS_SUCCESS) m_uuid = tmp;

		return res;
		}
	



