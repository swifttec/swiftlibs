/**
*** @file		SWConsole.cpp
*** @brief		A class for controlling console (terminal) I/O.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWConsole class.
**/

/*********************************************************************************
Copyright (c) 2006-2016 SwiftTec Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



// Needed for precompiled header support
#include "stdafx.h"

#include <swift/SWConsole.h>

#if defined(SW_PLATFORM_WINDOWS)
	#include <windows.h>
	#include <stdio.h>
	#include <io.h>
	#include <conio.h>
	#include <fcntl.h>
	#include <string.h>
	#include <Wincon.h>
	#include <conio.h>
#else
	#include <termios.h>
#endif



// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif

#if !defined(SW_PLATFORM_WINDOWS)
static void
echo(bool v)
		{
		struct termios settings;
		tcgetattr( STDIN_FILENO, &settings );
		settings.c_lflag = v
					   ? (settings.c_lflag |   ECHO )
					   : (settings.c_lflag & ~(ECHO));
		tcsetattr(STDIN_FILENO, TCSANOW, &settings);
		}
#endif


SWString
SWConsole::getStringNoEcho()
		{
		SWString		res;
		sw_tchar_t	c, s[80];
		int			pos=0;

		s[pos] = 0;

#if !defined(SW_PLATFORM_WINDOWS)
		echo(false);
#endif

		for (;;)
			{
#if defined(SW_PLATFORM_WINDOWS)
			c = _getch();
#else
			c = getchar();
#endif

			if (c == '\n' || c == '\r')
				{
				break;
				}
			else if (c == 8 && pos > 0)
				{
				pos--;
				}
			else if (c >= 32 && pos < 79)
				{
				s[pos++] = c;
				}

			s[pos] = 0;
			}

#if !defined(SW_PLATFORM_WINDOWS)
		echo(true);
#endif // defined(SW_PLATFORM_WINDOWS)

		res = s;

		return res;
		}


SWString
SWConsole::getString()
		{
		SWString		res;

		res.readLine(stdin);

		return res;
		}

#ifdef NOT_YET_IMPLEMENTED
/**
*** Default constructor
**/
SWConsole::SWConsole()
		{
#if defined(SW_PLATFORM_WINDOWS)
		CONSOLE_SCREEN_BUFFER_INFO  consoleInfo;

		m_handle = GetStdHandle(STD_OUTPUT_HANDLE);
		GetConsoleScreenBufferInfo(m_handle, &consoleInfo);
		m_currentAttr = m_savedAttr = consoleInfo.wAttributes;
#endif
		}


/**
*** Destructor
**/
SWConsole::~SWConsole()
		{
#if defined(SW_PLATFORM_WINDOWS)
		// Restore console attribs
		SetConsoleTextAttribute(m_handle, m_savedAttr);
#endif
		}



/**
*** Return the size of the console in char dimensions
**/
SWSize
SWConsole::size()
		{
		SWSize	size;

#if defined(SW_PLATFORM_WINDOWS)
		CONSOLE_SCREEN_BUFFER_INFO  consoleInfo;

		GetConsoleScreenBufferInfo(m_handle, &consoleInfo);
		size = SWSize(consoleInfo.srWindow.Right - consoleInfo.srWindow.Left + 1, consoleInfo.srWindow.Bottom - consoleInfo.srWindow.Top + 1);
#endif

		return size;
		}


/**
*** Return the current cursor position
**/
SWPoint
SWConsole::getCursorPos()
		{
		SWPoint	pt;

#if defined(SW_PLATFORM_WINDOWS)
		CONSOLE_SCREEN_BUFFER_INFO  consoleInfo;

		GetConsoleScreenBufferInfo(m_handle, &consoleInfo);

		pt.set(consoleInfo.dwCursorPosition.X, consoleInfo.dwCursorPosition.Y);
#endif

		return pt;
		}


/**
*** Set the cursor position using a SWPoint object
**/
void
SWConsole::setCursorPos(SWPoint p)
		{
#if defined(SW_PLATFORM_WINDOWS)
		SetConsoleCursorPosition(m_handle, p);
#endif
		}


/**
*** Set the cursor position using x and y
**/
void
SWConsole::setCursorPos(int x, int y)
		{
		setCursorPos(SWPoint(x, y));
		}


/**
*** Set/clear the bold attribute
**/
void
SWConsole::bold(bool v)
		{
#if defined(SW_PLATFORM_WINDOWS)
		if (v) m_currentAttr |= FOREGROUND_INTENSITY;
		else m_currentAttr &= ~FOREGROUND_INTENSITY;

		SetConsoleTextAttribute(m_handle, m_currentAttr);
#endif
		}


/**
*** Set/clear the concealed attribute
**/
void
SWConsole::concealed(bool v)
		{
#if defined(SW_PLATFORM_WINDOWS)
		if (v)
			{
			saveState();

			m_currentAttr &= ~(FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_BLUE|FOREGROUND_INTENSITY);
			if ((m_currentAttr & BACKGROUND_RED)) m_currentAttr |= FOREGROUND_RED;
			if ((m_currentAttr & BACKGROUND_GREEN)) m_currentAttr |= FOREGROUND_GREEN;
			if ((m_currentAttr & BACKGROUND_BLUE)) m_currentAttr |= FOREGROUND_BLUE;
			if ((m_currentAttr & BACKGROUND_INTENSITY)) m_currentAttr |= FOREGROUND_INTENSITY;

			SetConsoleTextAttribute(m_handle, m_currentAttr);
			}
		else
			{
			restoreState();
			}
#endif
		}


/**
*** Save the current text attribute state (push)
**/
void
SWConsole::saveState()
		{
#if defined(SW_PLATFORM_WINDOWS)
		m_stateArray.add(m_currentAttr);
#endif // defined(SW_PLATFORM_WINDOWS)
		}


/**
*** Restore the current text attribute state from a previously saved state (pop)
**/
void
SWConsole::restoreState()
		{
#if defined(SW_PLATFORM_WINDOWS)
		if (m_stateArray.size() > 0)
			{
			int		pos=(int)m_stateArray.size()-1;

			m_currentAttr = (WORD)m_stateArray[pos];
			m_stateArray.remove(pos);

			SetConsoleTextAttribute(m_handle, m_currentAttr);
			}
#endif
		}


/**
*** Set the underline attribute
**/
void
SWConsole::underline(bool v)
		{
#if defined(SW_PLATFORM_WINDOWS)
		if (v) m_currentAttr |= COMMON_LVB_UNDERSCORE;
		else m_currentAttr &= ~COMMON_LVB_UNDERSCORE;

		SetConsoleTextAttribute(m_handle, m_currentAttr);
#endif
		}


/**
*** Set the foreground color
**/
void
SWConsole::setForegroundColor(int color)
		{
#if defined(SW_PLATFORM_WINDOWS)
		// Set foreground color
		m_currentAttr &= ~(FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_BLUE|FOREGROUND_INTENSITY);
		if (color & 1) m_currentAttr |= FOREGROUND_RED;
		if (color & 2) m_currentAttr |= FOREGROUND_GREEN;
		if (color & 4) m_currentAttr |= FOREGROUND_BLUE;
		if (color & 8) m_currentAttr |= FOREGROUND_INTENSITY;

		SetConsoleTextAttribute(m_handle, m_currentAttr);
#endif
		}


/**
*** Set the background color
**/
void
SWConsole::setBackgroundColor(int color)
		{
#if defined(SW_PLATFORM_WINDOWS)
		// Set foreground color
		m_currentAttr &= ~(BACKGROUND_RED|BACKGROUND_GREEN|BACKGROUND_BLUE|BACKGROUND_INTENSITY);
		if (color & 1) m_currentAttr |= BACKGROUND_RED;
		if (color & 2) m_currentAttr |= BACKGROUND_GREEN;
		if (color & 4) m_currentAttr |= BACKGROUND_BLUE;
		if (color & 8) m_currentAttr |= BACKGROUND_INTENSITY;

		SetConsoleTextAttribute(m_handle, m_currentAttr);
#endif
		}


/**
*** Reverse the colors - i.e. swap foreground and background
**/
void
SWConsole::reverseColors()
		{
#if defined(SW_PLATFORM_WINDOWS)
		WORD	attr = m_currentAttr;

		// Reversed - set fg to the same as bg
		m_currentAttr &= ~(FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_BLUE|FOREGROUND_INTENSITY|BACKGROUND_RED|BACKGROUND_GREEN|BACKGROUND_BLUE|BACKGROUND_INTENSITY);
		if ((attr & BACKGROUND_RED)) m_currentAttr |= FOREGROUND_RED;
		if ((attr & BACKGROUND_GREEN)) m_currentAttr |= FOREGROUND_GREEN;
		if ((attr & BACKGROUND_BLUE)) m_currentAttr |= FOREGROUND_BLUE;
		if ((attr & BACKGROUND_INTENSITY)) m_currentAttr |= FOREGROUND_INTENSITY;
		if ((attr & FOREGROUND_RED)) m_currentAttr |= BACKGROUND_RED;
		if ((attr & FOREGROUND_GREEN)) m_currentAttr |= BACKGROUND_GREEN;
		if ((attr & FOREGROUND_BLUE)) m_currentAttr |= BACKGROUND_BLUE;
		if ((attr & FOREGROUND_INTENSITY)) m_currentAttr |= BACKGROUND_INTENSITY;

		SetConsoleTextAttribute(m_handle, m_currentAttr);
#endif
		}



/**
*** Clear the screen
**/
void
SWConsole::clear(bool /*home*/)
		{
#if defined(SW_PLATFORM_WINDOWS)
#endif
		}


void	
SWConsole::getPassword(SWString &pass)
		{
#if defined(SW_PLATFORM_WINDOWS)
		TCHAR	buf[256];
		TCHAR	c, *p;
		int		buflen=sizeof(buf) / sizeof(TCHAR);

		p = buf;
		for (;;)
			{
			c = (TCHAR)getch();
			if ((c == 127 || c == 8) && p > buf)
				{
				putchar(8);
				putchar(' ');
				putchar(8);
				--p;
				}
			else if (c == '\n' || c == '\r')
				{
				putchar('\n');
				*p = 0;
				break;
				}
			else if ((p-buf) < buflen -1 && ((c > ' ' && c < 127) || (c > 128+' ' && c < 256)))
				{
				*p++ = c;
				putchar('*');
				}
			}
#else
		// Use the built-in function for now
		char	buf[256];

	#if defined(SW_PLATFORM_SOLARIS)
		getpassphrase(buf);
	#else
		getpass(buf);
	#endif
#endif

		pass = buf;
		}

#endif // NOT_YET_IMPLEMENTED