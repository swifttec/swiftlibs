/**
*** @file		SWOrderedList.cpp
*** @brief		Ordered templated list class.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWOrderedList class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __base_SWOrderedList_cpp__
#define __base_SWOrderedList_cpp__

#include <swift/SWOrderedList.h>
#include <swift/SWFixedSizeMemoryManager.h>



/// Default constructor - empty list
template<class T>
SWOrderedList<T>::SWOrderedList() :
	m_pMemoryManager(0),
	m_elemsize(0),
	m_head(0),
	m_tail(0)
		{
		// Try to ensure good alignment internally
		m_elemsize = sizeof(SWListElement<T>);
		m_elemsize = ((m_elemsize + sizeof(void *) - 1) / sizeof(void *)) * sizeof(void *);

		m_pMemoryManager = new SWFixedSizeMemoryManager(m_elemsize);
		}


/// Virtual destructor
template<class T>
SWOrderedList<T>::~SWOrderedList()
		{
		clear();

		delete m_pMemoryManager;
		}


template<class T> void
SWOrderedList<T>::clear()
		{
		SWWriteGuard		guard(this);
		SWListElement<T> 	*p, *np;

		for (p=m_head;p;p=np)
			{
			np = p->next;
			delete p;
			m_pMemoryManager->free(p);
			}

		m_head = m_tail = NULL;
		setSize(0);
		}


#define AddPosNone	0
#define AddPosHead	1
#define AddPosTail	2

template<class T> void
SWOrderedList<T>::addElement(SWListElement<T> *pElement, bool addToHead, sw_collection_compareDataFunction *pCompareFunc)
		{
		int		addpos=AddPosNone;

		if (m_head != NULL)
			{
			void			*pData=&(pElement->data());

			if (doCompareData(pCompareFunc, pData, &(m_head->data())) <= 0) addpos = AddPosHead;
			else if (doCompareData(pCompareFunc, pData, &(m_tail->data())) > 0) addpos = AddPosTail;
			else
				{
				// Add it into the middle
				SWListElement<T>	*lle=0;
				int					r;

				for (lle=m_head;lle!=NULL;lle=lle->next)
					{
					r = doCompareData(pCompareFunc, pData, &(lle->data()));
					if (r <= 0)
						{
						// Add before the current element
						pElement->prev = lle->prev;
						pElement->next = lle;
						if (lle->prev != NULL) lle->prev->next = pElement;
						lle->prev = pElement;

						// Adjust the head pointer
						if (m_head == lle) m_head = pElement;
						break;
						}
					}

				if (lle == NULL) addpos = AddPosTail;
				}
			}
		else addpos = (addToHead?AddPosHead:AddPosTail);

		if (addpos == AddPosHead)
			{
			// Add the list element to the head of the list
			pElement->prev = NULL;
			pElement->next = m_head;
			if (m_head) m_head->prev = pElement;
			if (m_tail == NULL) m_tail = pElement;
			m_head = pElement;
			}
		else if (addpos == AddPosTail)
			{
			// Add the list element to the tail of the list
			pElement->next = NULL;
			pElement->prev = m_tail;
			if (m_tail) m_tail->next = pElement;
			if (m_head == NULL) m_head = pElement;
			m_tail = pElement;
			}
		}



template<class T> void
SWOrderedList<T>::add(const T &data)
		{
		void	*p;

		p = m_pMemoryManager->malloc(m_elemsize);
		if (p == NULL) throw SW_EXCEPTION(SWMemoryException);

		SWWriteGuard	guard(this);

		addElement(new (p) SWListElement<T>(data), true, NULL);
		incSize();
		}


template<class T> bool
SWOrderedList<T>::get(ListPosition pos, T &data) const
		{
		SWReadGuard	guard(this);
		bool		res=false;

		if (pos == ListHead)
			{
			if (m_head != NULL)
				{
				data = m_head->data();
				res = true;
				}
			}
		else
			{
			if (m_tail != NULL)
				{
				data = m_tail->data();
				res = true;
				}
			}

		return res;
		}


template<class T> bool
SWOrderedList<T>::getHead(T &data) const
		{
		return get(ListHead, data);
		}


template<class T> bool
SWOrderedList<T>::getTail(T &data) const
		{
		return get(ListTail, data);
		}



template<class T> bool
SWOrderedList<T>::remove(ListPosition pos, T *pData)
		{
		SWWriteGuard	guard(this);
		bool			res=false;

		if (pos == ListHead)
			{
			if (m_head != NULL)
				{
				if (pData != NULL) *pData = m_head->data();
				removeElement(m_head);
				res = true;
				}
			}
		else
			{
			if (m_tail != NULL)
				{
				if (pData != NULL) *pData = m_tail->data();
				removeElement(m_tail);
				res = true;
				}
			}

		return res;
		}


template<class T> bool
SWOrderedList<T>::removeHead(T *pData)
		{
		return remove(ListHead, pData);
		}


template<class T> bool
SWOrderedList<T>::removeTail(T *pData)
		{
		return remove(ListTail, pData);
		}



template<class T> bool
SWOrderedList<T>::contains(const T &data) const
		{
		SWReadGuard			guard(this);
		SWListElement<T>	*pElement=m_tail;
		bool				res=false;

		for (pElement=m_head;pElement!=NULL;pElement=pElement->next)
			{
			if (data == pElement->data())
				{
				res = true;
				break;
				}
			}

		return res;
		}


template<class T> bool
SWOrderedList<T>::find(const T &search, T &data) const
		{
		SWReadGuard			guard(this);
		SWListElement<T>	*pElement=m_tail;
		bool				res=false;

		for (pElement=m_head;pElement!=NULL;pElement=pElement->next)
			{
			if (search == pElement->data())
				{
				data = pElement->data();
				res = true;
				break;
				}
			}

		return res;
		}


template<class T> void
SWOrderedList<T>::removeElement(SWListElement<T> *pElement)
		{
		if (pElement != NULL)
			{
			SWListElement<T>	*n, *p;

			n = pElement->next;
			p = pElement->prev;
			if (m_head == pElement) m_head = pElement->next;
			if (m_tail == pElement) m_tail = pElement->prev;
			if (n) n->prev = pElement->prev;
			if (p) p->next = pElement->next;

			delete pElement;
			m_pMemoryManager->free(pElement);

			decSize();
			}
		}


template<class T> bool
SWOrderedList<T>::remove(const T &search, T *pData)
		{
		SWWriteGuard		guard(this);
		SWListElement<T>	*pElement=m_tail;
		bool				res=false;

		for (pElement=m_head;pElement!=NULL;pElement=pElement->next)
			{
			if (search == pElement->data())
				{
				if (pData != NULL) *pData = pElement->data();
				removeElement(pElement);
				res = true;
				break;
				}
			}

		return res;
		}


template<class T> SWIterator<T>
SWOrderedList<T>::iterator(bool iterateFromEnd) const
		{
		SWIterator<T>		res;

		initIterator(res, getIterator(iterateFromEnd));

		return res;
		}



template<class T> int
SWOrderedList<T>::compareData(const void *pData1, const void *pData2) const
		{
		int		res=0;

		if (*(T *)pData1 < *(T *)pData2) res = -1;
		else if (*(T *)pData1 == *(T *)pData2) res = 0;
		else res = 1;

		return res;
		}


//--------------------------------------------------------------------------------
// SWOrderedList Iterator
//--------------------------------------------------------------------------------


template<class T>
class SWOrderedListIterator : public SWCollectionIterator
		{
		friend class SWOrderedList<T>;

public:
		SWOrderedListIterator(SWOrderedList<T> *list, bool iterateFromEnd);

		virtual bool		hasCurrent();
		virtual bool		hasNext();
		virtual bool		hasPrevious();

		virtual void *		current();
		virtual void *		next();
		virtual void *		previous();

		virtual bool		remove(void *pOldData);
		virtual bool		add(void *data, SWAbstractIterator::AddLocation where);
		virtual bool		set(void *data, void *pOldData);

		SWOrderedList<T> *	collection()	{ return (SWOrderedList<T> *)m_pCollection; }

private:
		SWListElement<T>	*m_curr;	// The "current" element
		SWListElement<T>	*m_next;	// The next element
		SWListElement<T>	*m_prev;	// The previous element
		};


template<class T> SWCollectionIterator *
SWOrderedList<T>::getIterator(bool iterateFromEnd) const
		{
		return new SWOrderedListIterator<T>((SWOrderedList<T> *)this, iterateFromEnd);
		}


template<class T> 
SWOrderedListIterator<T>::SWOrderedListIterator(SWOrderedList<T> *list, bool iterateFromEnd) :
    SWCollectionIterator(list),
    m_curr(NULL),
    m_next(NULL),
    m_prev(NULL)
		{
		if (iterateFromEnd)
			{
			// Start at the tail of the list
			m_prev = collection()->m_tail;
			}
		else
			{
			// Start at the head of the list
			m_next = collection()->m_head;
			}
		}


template<class T> bool
SWOrderedListIterator<T>::hasCurrent()
		{
		SWReadGuard	guard(m_pCollection);

		checkForComodification();
		return (m_curr != NULL);
		}


template<class T> bool
SWOrderedListIterator<T>::hasNext()
		{
		SWReadGuard	guard(m_pCollection);

		checkForComodification();
		return (m_next != NULL);
		}


template<class T> bool
SWOrderedListIterator<T>::hasPrevious() 
		{
		SWReadGuard	guard(m_pCollection);

		checkForComodification();
		return (m_prev != NULL);
		}


template<class T> void *
SWOrderedListIterator<T>::current() 
		{
		SWReadGuard	guard(m_pCollection);

		checkForComodification();

		if (m_curr == NULL) throw SW_EXCEPTION(SWNoSuchElementException);

		return &(m_curr->data());
		}

template<class T> void *
SWOrderedListIterator<T>::next() 
		{
		SWReadGuard	guard(m_pCollection);

		checkForComodification();

		if (m_next == NULL) throw SW_EXCEPTION(SWNoSuchElementException);

		m_curr = m_next;
		m_next = m_curr->next;
		m_prev = m_curr->prev;

		return &(m_curr->data());
		}

template<class T> void *
SWOrderedListIterator<T>::previous() 
		{
		SWReadGuard	guard(m_pCollection);

		checkForComodification();

		if (m_prev == NULL) throw SW_EXCEPTION(SWNoSuchElementException);

		m_curr = m_prev;
		m_next = m_curr->next;
		m_prev = m_curr->prev;

		return &(m_curr->data());
		}


template<class T> bool
SWOrderedListIterator<T>::remove(void *pOldData)
		{
		SWWriteGuard	guard(m_pCollection);

		checkForComodification();

		if (m_curr == NULL) throw SW_EXCEPTION(SWNoSuchElementException);

		if (pOldData != NULL) *(T *)pOldData = m_curr->data();

		collection()->removeElement(m_curr);
		m_curr = NULL;
		m_modcount = collection()->getModCount();

		return true;
		}


template<class T> bool
SWOrderedListIterator<T>::set(void *data, void *pOldData)
		{
		SWWriteGuard	guard(m_pCollection);

		checkForComodification();
		if (m_curr == NULL) throw SW_EXCEPTION(SWIllegalStateException);

		if (pOldData != NULL) *(T *)pOldData = m_curr->data();
		m_curr->data() = *(T *)data;

		return true;
		}


template<class T> bool
SWOrderedListIterator<T>::add(void *data, SWAbstractIterator::AddLocation where) 
		{
		SWWriteGuard		guard(m_pCollection);
		SWListElement<T>	*e=NULL;

		checkForComodification();
		if (m_pCollection == NULL) throw SW_EXCEPTION(SWIllegalStateException);

		// Create a new list element to hold the data pointer
		void	*p;

		p = collection()->m_pMemoryManager->malloc(collection()->m_elemsize);
		if (p == NULL) throw SW_EXCEPTION(SWMemoryException);
		e = new (p) SWListElement<T>(*(T *)data);

		if (where == SWAbstractIterator::AddBefore)
			{
			if (collection()->m_head == NULL)
				{
				// Simply add the element
				collection()->m_head = collection()->m_tail = e;
				}
			else
				{
				if (m_curr == NULL)
					{
					// No current element - try adding after the previous element
					if (m_prev == NULL)
						{
						// Must be at the front of the list as we have no previous element
						e->next = collection()->m_head;
						collection()->m_head->prev = e;
						collection()->m_head = e;
						}
					else
						{
						e->next = m_prev->next;
						e->prev = m_prev;
						if (m_prev->next != NULL) m_prev->next->prev = e;
						m_prev->next = e;

						// Adjust the tail pointer
						if (collection()->m_tail == m_prev) collection()->m_tail = e;
						}
					}
				else
					{
					// Add before the current element
					e->prev = m_curr->prev;
					e->next = m_curr;
					if (m_curr->prev != NULL) m_curr->prev->next = e;
					m_curr->prev = e;

					// Adjust the head pointer
					if (collection()->m_head == m_curr) collection()->m_head = e;
					}
				}

			// Make sure our previous pointer refers to the new element
			m_prev = e;
			}
		else
			{
			// Add after the current element
			if (collection()->m_tail == NULL)
				{
				// Simply add the element
				collection()->m_head = collection()->m_tail = e;
				}
			else
				{
				if (m_curr == NULL)
					{
					// No current element - try adding before the next element
					if (m_next == NULL)
						{
						// Must be at the front of the list as we have no previous element
						e->prev = collection()->m_tail;
						collection()->m_tail->next = e;
						collection()->m_tail = e;
						}
					else
						{
						e->prev = m_next->prev;
						e->next = m_next;
						if (m_next->prev != NULL) m_next->prev->next = e;
						m_next->prev = e;

						// Adjust the head pointer
						if (collection()->m_head == m_next) collection()->m_head = e;
						}
					}
				else
					{
					// Add after the current element
					e->next = m_curr->next;
					e->prev = m_curr;
					if (m_curr->next != NULL) m_curr->next->prev = e;
					m_curr->next = e;

					// Adjust the tail pointer
					if (collection()->m_tail == m_curr) collection()->m_tail = e;
					}
				}

			// Make sure our next pointer refers to the new element
			m_next = e;
			}

		// Finally increment the size and
		// make sure we have an up-to-date mod count
		collection()->incSize();
		m_modcount = collection()->getModCount();

		return true;
		}






#endif
