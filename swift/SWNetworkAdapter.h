/****************************************************************************
**	SWNetworkAdapter.h	A class to represent a network adapter
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __swift_SWNetworkAdapter_h__
#define __swift_SWNetworkAdapter_h__

#include <swift/SWIPAddress.h>
#include <swift/SWString.h>
#include <swift/SWTime.h>
#include <swift/SWArray.h>
#include <swift/SWObjectArray.h>

//#include <swift/SWBuffer.h>

#if defined(SW_PLATFORM_WINDOWS)
	#include <Iptypes.h>
	#include <IPIfCons.h>

	#pragma comment(lib, "Iphlpapi.lib")
#endif

#include <swift/SWRegistryKey.h>


/**
*** A class to represent a network adapter
**/
class SWIFT_DLL_EXPORT SWNetworkAdapter
		{
public :
		enum AdapterType
			{
			Ethernet,
			TokenRing,
			FDDI,
			PPP,
			Loopback,
			Slip,
			Other
			};
		
public:
		/// Default constructor
		SWNetworkAdapter();
		SWNetworkAdapter(const SWNetworkAdapter &v);
		SWNetworkAdapter & operator=(const SWNetworkAdapter &v);

		void				clear();

#if defined(SW_PLATFORM_WINDOWS)
		// IP_ADAPTER_INFO constructor
		SWNetworkAdapter(const IP_ADAPTER_INFO &adapter);

		// Assignment operator
		SWNetworkAdapter &	operator=(const IP_ADAPTER_INFO &adapter);

		// Registry constructor
		SWNetworkAdapter(SWRegistryKey &nk);
#endif

		/// Returns the network adapter name
		const SWString &	name() const						{ return m_name; }

		/// Returns the network adapter display name
		const SWString &	displayName() const					{ return m_displayName; }

		/// Returns the network adapter description
		const SWString &	desc() const						{ return m_desc; }

		/// Returns the type of this adapter
		int					type() const						{ return m_type; }

		// returns the type of this adapter as a string
		SWString			typeAsString() const				{ return typeAsString(m_type); }

		/// Returns the hardware address
		SWBuffer			address()							{ return m_address; }

		// Returns the hardware address as a string
		SWString			addressAsString();

		/// returns the adapter index
		int					index() const						{ return m_index; }

		/// returns the number of IP addresses/masks
		int					ipAddressCount() const				{ return (int)m_ipaddrs.size(); }

		// returns the nth IP address/mask
		SWIPAddress			ipAddress(int n);
		SWIPAddress			ipAddressMask(int n);

		// returns the current IP address/mask
		int					currentIPAddressIndex()				{ return m_currAddr; }
		SWIPAddress			currentIPAddress();
		SWIPAddress			currentIPAddressMask();

		/// Returns the default gateway
		SWIPAddress			gateway()							{ return m_gateway; }

		/// Returns the number of dns servers
		int					dnsServerCount()					{ return (int)m_dnsServers.size(); }

		// Returns the nth dns server
		SWIPAddress			dnsServer(int n);

		/// If true this adapter registers itself with DNS
		bool				dnsRegisterHostName() const			{ return m_dnsRegisterHostName; }

		/// If true this adapter uses the AdapterDomainName suffix in DNS registration.
		bool				dnsRegisterAdapterHostName() const	{ return m_dnsRegisterAdapterHostName; }

		/// Returns the primary domain name for this adapter
		SWString			dnsPrimaryDomainName() const		{ return m_dnsPrimaryDomainName; }

		/// Returns the DNS name that is specific to this adapter (may be blank)
		SWString			dnsAdapterDomainName() const		{ return m_dnsAdapterDomainName; }

		/// Returns the dhcp status
		bool				dhcpEnabled() const					{ return m_dhcp; }

		/// Returns the dhcp server
		SWIPAddress			dhcpServer()						{ return m_dhcpServer; }

		/// Returne the dhcp lease obtained time
		SWTime				dhcpLeaseObtained()					{ return m_dhcpLeaseObtained; }

		/// Returne the dhcp lease expiry time
		SWTime				dhcpLeaseExpires()					{ return m_dhcpLeaseExpires; }

		/// returns the wins status
		bool				winsEnabled() const					{ return m_wins; }

		/// Returns the primary wins server ip address
		SWIPAddress			winsPrimaryServer()					{ return m_winsPrimaryServer; }

		/// Returns the secondary wins server ip address
		SWIPAddress			winsSecondaryServer()				{ return m_winsSecondaryServer; }

public: // Static functions
		// returns the type of this adapter as a string
		static SWString		typeAsString(int t);

		// Fills an array of adapaters
		static bool			getLocalAdapters(SWArray<SWNetworkAdapter> &aa);

		/// Get the best network adapter to use to reach the given address
		static sw_status_t	getBestRouteForAddr(const SWIPAddress &addr, SWNetworkAdapter &adapter, int &ipaddrindex);

protected:
		void				getDnsInfo();

protected:
		SWString				m_name;							///< the adpater name
		SWString				m_desc;							///< the adapter description
		SWString				m_displayName;					///< the adapter display name
		SWBuffer				m_address;						///< the adapter hardware address
		int						m_index;						///< the adapter index
		int						m_type;							///< the adapter type
		int						m_currAddr;						///< The current ip addr (if any)
		SWArray<SWIPAddress>	m_ipaddrs;						///< A list of IP addresses for this adapter	
		SWArray<SWIPAddress>	m_ipmasks;						///< A list of IP address masks for this adapter	
		SWIPAddress				m_gateway;						///< The default gateway for this adapter

		// DNS
		SWArray<SWIPAddress>	m_dnsServers;					///< A list of DNS servers for this adapter
		bool					m_dnsRegisterAdapterHostName;	///< A flag indicating that the adapter host name will be registered with DNS
		bool					m_dnsRegisterHostName;			///< A flag indicating that the adpater will register with DNS
		SWString				m_dnsAdapterDomainName;			///< The DNS domain specifically for this adapter
		SWString				m_dnsPrimaryDomainName;			///< The primary DNS domain for this machine

		// DHCP
		bool					m_dhcp;							///< the dhcp flag (true if enabled)
		SWIPAddress				m_dhcpServer;					///< the dhcp server
		SWTime					m_dhcpLeaseObtained;			///< when the dhcp lease was obtained
		SWTime					m_dhcpLeaseExpires;				///< when the dhcp lease will expire

		// WINS
		bool					m_wins;							///< the wins flag (true if available)
		SWIPAddress				m_winsPrimaryServer;			///< the primary wins server
		SWIPAddress				m_winsSecondaryServer;			///< the secondary wins server
		};


#endif
