/**
*** @file		SWValue.h
*** @brief		A generic value or variant object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWValue class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWValue_h__
#define __SWValue_h__


#include <swift/swift.h>
#include <swift/SWBuffer.h>
#include <swift/SWString.h>
#include <swift/SWDate.h>
#include <swift/SWTime.h>
#include <swift/SWMoney.h>

// Forward declarations
#if defined(SW_BUILD_MFC)
class CDBVariant;
#endif



/**
*** @brief	A generic value (variant) object.
***
*** A generic value object that is used as a basis for
*** classes such as SWMsgField, SWRegistryValue and SWConfigValue.
***
*** Each SWValue object has a changed flag associated with it so that a use of a SWValue
*** object can track whether or not any changes have been made to the object since it was
*** loaded with a value. Any operation that modifies the value will set the changed flag.
*** This can be useful when the caller needs to know if a value has been modified and needs
*** to be written back to permenant storage.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWValue : public SWObject
		{
public:
		/// An enumeration of value types
		enum ValueType
			{
			VtNone		= 0x00,	///< No data
			VtBoolean	= 0x01,	///< Boolean data type

			VtInt8		= 0x11,	///< 8 bit signed integer
			VtInt16		= 0x12,	///< 16 bit signed integer
			VtInt32		= 0x14,	///< 32 bit signed integer
			VtInt64		= 0x18,	///< 64 bit signed integer

			VtUInt8		= 0x21,	///< 8 bit unsigned integer
			VtUInt16	= 0x22,	///< 16 bit unsigned integer
			VtUInt32	= 0x24,	///< 32 bit unsigned integer
			VtUInt64	= 0x28,	///< 64 bit unsigned integer

			VtFloat		= 0x30,	///< 32 bit floating point
			VtDouble	= 0x31,	///< 64 bit floating point

			VtCChar		= 0x41,	///< Single-byte char
			VtWChar		= 0x42,	///< Wide char

			VtCString	= 0x51,	///< Single-byte Char string (nul terminated)
			VtWString	= 0x52,	///< Wide char

			VtDate		= 0x80, ///< A date based on SWDate
			VtTime		= 0x81, ///< A date/time based on SWTime
			VtMoney		= 0x82,	///< A currency based on SWMoney
			VtCurrency	= 0x83,	///< A currency based on SWCurrency

			VtPointer	= 0xfe,	///< Data pointer
			VtData		= 0xff	///< Data block - binary (covers anything else!)
			};


public:
/**
*** @name Constructors and Destructors
**/
//@{

		/// Default constructor - constructs an empty value of type VtNone
		SWValue();

		/// Virtual destrcutor
		virtual ~SWValue();

		/// Copy constructor
		SWValue(const SWValue &v);

		/**
		*** Construct an integer or char value with the specified type.
		***
		*** This constructor is required because the compiler often cannot work out
		*** the difference between an integer and one of the char types.
		***
		*** @param[in]	v		The integer or character value to assign.
		*** @param[in]	type	The type of value which can be any one of the integer or
		***						character types.
		**/
		SWValue(int v, ValueType type);

		SWValue(bool v);				///< Constructs a value with the type VtNone
		SWValue(sw_int8_t v);			///< Constructs a value with the type VtInt8
		SWValue(sw_int16_t v);			///< Constructs a value with the type VtInt16
		SWValue(sw_int32_t v);			///< Constructs a value with the type VtInt16
		SWValue(sw_int64_t v);			///< Constructs a value with the type VtInt16
		SWValue(sw_uint8_t v);			///< Constructs a value with the type VtUInt8
		SWValue(sw_uint16_t v);			///< Constructs a value with the type VtUInt8
		SWValue(sw_uint32_t v);			///< Constructs a value with the type VtUInt8
		SWValue(sw_uint64_t v);			///< Constructs a value with the type VtUInt8
		SWValue(float v);				///< Constructs a value with the type VtFloat
		SWValue(double v);				///< Constructs a value with the type VtDouble
		SWValue(const char *v);			///< Constructs a value with the type VtString
		SWValue(const wchar_t *v);		///< Constructs a value with the type VtWString
		SWValue(const void *, size_t len, bool copyData=true);	///< Constructs a value with the type VtData from the data pointer and size
		SWValue(const SWBuffer &v);		///< Constructs a value with the type VtData from SWBuffer
		SWValue(const SWString &v);		///< Constructs a value with a string type of VtString or VtWString depending on the SWString
		SWValue(const SWDate &v);		///< Constructs a value with the type VtDate
		SWValue(const SWTime &v);		///< Constructs a value with the type VtTime
		SWValue(const SWMoney &v);		///< Constructs a value with the type VtMoney

#if defined(SW_BUILD_MFC)
		SWValue(const COleVariant &v);
		SWValue(const CDBVariant &v);
		SWValue(const CString &v);
#endif

		/**
		*** Special constructor to construct a value specified by the string v
		*** and the type specified via string t. See the setValue(const SWString &v, const SWString &t)
		*** for details of the parameters.
		*** 
		*** @param[in]	v	The value to be assigned in string form.
		*** @param[in]	t	The type of value to be assigned in string form.
		***
		*** @see setValue(const SWString &v, const SWString &t)
		**/
		SWValue(const SWString &v, const SWString &t);
//@}



/**
*** @name Set value methods
**/
//@{
		// Set methods
		virtual bool		setValue(const SWValue &v);								///< Set the value of this object from the value.
		virtual bool		setValue(bool v);										///< Set the value of this object from the specified bool value.
		virtual bool		setValue(sw_int8_t v);									///< Set the value of this object from the specified 8-bit signed integer value.
		virtual bool		setValue(sw_int16_t v);									///< Set the value of this object from the specified 16-bit signed integer value.
		virtual bool		setValue(sw_int32_t v);									///< Set the value of this object from the specified 32-bit signed integer value.
		virtual bool		setValue(sw_int64_t v);									///< Set the value of this object from the specified 64-bit signed integer value.
		virtual bool		setValue(sw_uint8_t v);									///< Set the value of this object from the specified 8-bit unsigned integer value.
		virtual bool		setValue(sw_uint16_t v);								///< Set the value of this object from the specified 16-bit unsigned integer value.
		virtual bool		setValue(sw_uint32_t v);								///< Set the value of this object from the specified 32-bit unsigned integer value.
		virtual bool		setValue(sw_uint64_t v);								///< Set the value of this object from the specified 64-bit unsigned integer value.
		virtual bool		setValue(float v);										///< Set the value of this object from the specified float value.
		virtual bool		setValue(double v);										///< Set the value of this object from the specified double value.
		virtual bool		setValue(const char *v);								///< Set the value of this object from the specified single-byte string value.
		virtual bool		setValue(const wchar_t *v);								///< Set the value of this object from the specified double-byte string value.
		virtual bool		setValue(const SWString &v);							///< Set the value of this object from the specified SWString object.
		virtual bool		setValue(const SWBuffer &v);							///< Set the value of this object from the specified SWBuffer object as binary data.
		virtual bool		setValue(const SWDate &v);								///< Set the value of this object from the specified SWDate object.
		virtual bool		setValue(const SWTime &v);								///< Set the value of this object from the specified SWTime object.
		virtual bool		setValue(const SWMoney &v);								///< Set the value of this object from the specified SWTime object.

		/**
		*** @brief	Set the value of this object from the given integer value.
		***
		*** When a const integer is passed to the SWValue object the compiler doesn't
		*** always know which type of integer to use. This method allows the caller
		*** to precisely determine which integer type to use.
		***
		*** @param[in]	v		The integer value to assign
		*** @param[in]	type	The integer type to use. Valid values are:
		***						VtInt8, VtInt16, VtInt32, VtInt64, 
		***						VtUInt8, VtUInt16, VtUInt32, VtUInt64,
		***						and VtBoolean.
		***
		**/
		virtual bool		setValue(int v, ValueType type);

		/**
		*** @brief	Set the value of this object to be data or pointer.
		***
		*** This method allows the value of this object to be set to be either
		*** binary data (type=VtData) or to be a pointer (type=VtPointer). The 
		*** type is determined by the copyData parameter. If copyData is true
		*** then the type of the object is set to be VtData, and len bytes of
		*** data are copied into this object. If copyData is false, then the 
		*** type of this object is set to be VtPointer and no data is copied.
		***
		*** @param[in]	v			A pointer to the data to be stored.
		*** @param[in]	len			The size of the data pointed to.
		*** @param[in]	copyData	A boolean indicating if the data type is a pointer
		***							(copyData=false) or the data type is binary data
		***							(copyData=true) and len bytes of data should be copied
		***							into this object.
		**/
		virtual bool		setValue(const void *v, size_t len, bool copyData=true);

		/**
		*** @brief	Set the value of this object by converting the supplied string.
		***
		*** This method is supplied to support the conversion of values from strings.
		*** The caller provides both the value and type information as strings and this
		*** method attempts to convert the string into the specified type. Supported 
		*** type names are:
		***
		*** - bool, boolean
		*** - char, character
		*** - int, integer, long, int8, int16, int32, int64
		*** - unsigned, uint, "unsigned integer", "unsigned long", uint8, uint16, uint32, uint64
		*** - float, double
		*** - date - expected in the form YYYYMMDD
		*** - time - expected in the form hh:mm:ss
		*** - datetime - expected in the form YYYYMMDD hh:mm:ss
		*** - datetimems - expected in the form YYYYMMDD hh:mm:ss.ms
		*** - date:format, time:format - where format specifies the values specified in the value string.
		***	  Supported format values are:
		***		- YYYY 4-digit year
		***		- YY 2-digit year (00-99)
		***		- MM 2-digit month (01-12)
		***		- DD 2-digit day (01-31)
		***		- hh 2-digit hour (00-23)
		***		- mm 2-digit minute (00-59)
		***		- ss 2-digit second (00-61)
		***		- ms milliseconds (000-999)
		***		- us milliseconds (000000-999999)
		***		- ns milliseconds (000000000-999999999)
		***
		*** @param[in]	v	The value to be assigned in string form.
		*** @param[in]	t	The type of value to be assigned in string form.
		***
		*** @return		true if conversion was successful, false otherwise.
		**/
		virtual bool		setValue(const SWString &v, const SWString &t);	

#if defined(SW_BUILD_MFC)
		virtual bool		setValue(const COleVariant &v);								///< Set the value of this object from the specified COleVariant object.
		virtual bool		setValue(const CDBVariant &v);								///< Set the value of this object from the specified COleVariant object.
		virtual bool		setValue(const CString &v);								///< Set the value of this object from the specified CString object.
#endif

//@}


/**
*** @name Assignment operators
**/
//@{
		SWValue &	operator=(const SWValue &v);		///< Assign this object from another SWValue object.
		SWValue &	operator=(bool v);					///< Assign this object from an bool value.
		SWValue &	operator=(sw_int8_t v);				///< Assign this object from an 8-bit signed integer value.
		SWValue &	operator=(sw_int16_t v);			///< Assign this object from an 16-bit signed integer value.
		SWValue &	operator=(sw_int32_t v);			///< Assign this object from an 32-bit signed integer value.
		SWValue &	operator=(sw_int64_t v);			///< Assign this object from an 64-bit signed integer value.
		SWValue &	operator=(sw_uint8_t v);			///< Assign this object from an 8-bit unsigned integer value.
		SWValue &	operator=(sw_uint16_t v);			///< Assign this object from an 16-bit unsigned integer value.
		SWValue &	operator=(sw_uint32_t v);			///< Assign this object from an 32-bit unsigned integer value.
		SWValue &	operator=(sw_uint64_t v);			///< Assign this object from an 64-bit unsigned integer value.
		SWValue &	operator=(float v);					///< Assign this object from an float value.
		SWValue &	operator=(double v);				///< Assign this object from an double value.
		SWValue &	operator=(const char *v);			///< Assign this object from an single-byte string value.
		SWValue &	operator=(const wchar_t *v);		///< Assign this object from an double-byte string value.
		SWValue &	operator=(const SWString &v);		///< Assign this object from a SWString object.
		SWValue &	operator=(const SWBuffer &v);		///< Assign this object from a SWBuffer object as binary data.
		SWValue &	operator=(const SWDate &v);			///< Assign this object from a SWDate object.
		SWValue &	operator=(const SWTime &v);			///< Assign this object from a SWTime object.
		SWValue &	operator=(const SWMoney &v);		///< Assign this object from a SWTime object.

#if defined(SW_BUILD_MFC)
		SWValue &	operator=(const COleVariant &v);	///< Set the value of this object from the specified COleVariant object.
		SWValue &	operator=(const CDBVariant &v);	///< Set the value of this object from the specified COleVariant object.
		SWValue &	operator=(const CString &v);	///< Set the value of this object from the specified CString object.
#endif
//@}


/**
*** @name Conversion methods
**/
//@{
		virtual bool			toBoolean() const;									///< Convert this object to a bool value.
		virtual float			toFloat() const;									///< Convert this object to a float value.
		virtual double			toDouble() const;									///< Convert this object to a double value.
		virtual sw_int8_t		toInt8() const;										///< Convert this object to a 8-bit signed integer value.
		virtual sw_int16_t		toInt16() const;									///< Convert this object to a 16-bit signed integer value.
		virtual sw_int32_t		toInt32() const;									///< Convert this object to a 32-bit signed integer value.
		virtual sw_int64_t		toInt64() const;									///< Convert this object to a 64-bit signed integer value.
		virtual sw_uint8_t		toUInt8() const;									///< Convert this object to a 8-bit unsigned integer value.
		virtual sw_uint16_t		toUInt16() const;									///< Convert this object to a 16-bit unsigned integer value.
		virtual sw_uint32_t		toUInt32() const;									///< Convert this object to a 32-bit unsigned integer value.
		virtual sw_uint64_t		toUInt64() const;									///< Convert this object to a 64-bit unsigned integer value.
		virtual short			toShort() const;									///< Convert this object to a signed short value.
		virtual long			toLong() const;										///< Convert this object to a signed long value.
		virtual int				toInteger() const;									///< Convert this object to a signed int value.
		virtual unsigned short	toUShort() const;									///< Convert this object to a unsigned short value.
		virtual unsigned long	toULong() const;									///< Convert this object to a unsigned long value.
		virtual unsigned int	toUInteger() const;									///< Convert this object to a unsigned int value.
		virtual int				toChar() const;										///< Convert this object to a char value.
		virtual SWString		toString() const;									///< Convert this object to a SWString value.
		virtual SWDate			toDate() const;										///< Convert this object to a SWDate value.
		virtual SWTime			toTime() const;										///< Convert this object to a SWTime value.
		virtual SWMoney			toMoney() const;									///< Convert this object to a SWTime value.
//@}

/**
*** @name Cast operators
**/
//@{
		operator bool()				{ return toBoolean(); }			///< Cast this object to a bool value.
		operator float()			{ return toFloat(); }			///< Cast this object to a float value.
		operator double()			{ return toDouble(); }			///< Cast this object to a double value.
		operator char()				{ return (char)toChar(); }		///< Cast this object to a char value.
		operator SWString()			{ return toString(); }			///< Cast this object to a SWString value.
		operator SWDate()			{ return toDate(); }			///< Cast this object to a SWDate value.
		operator SWTime()			{ return toTime(); }			///< Cast this object to a SWTime value.
		operator SWMoney()			{ return toMoney(); }			///< Cast this object to a SWTime value.

		operator short()			{ return toShort(); }			///< Cast this object to a signed short value.
		operator int()				{ return toInteger(); }			///< Cast this object to a signed int value.
		operator long()				{ return toLong(); }			///< Cast this object to a signed long value.
		operator sw_int64_t()		{ return toInt64(); }			///< Cast this object to a signed 64-bit integer value.

		operator unsigned short()	{ return toUShort(); }			///< Cast this object to a unsigned short value.
		operator unsigned int()		{ return toUInteger(); }		///< Cast this object to a unsigned integer value.
		operator unsigned long()	{ return toULong(); }			///< Cast this object to a unsigned long value.
		operator sw_uint64_t()		{ return toUInt64(); }			///< Cast this object to a unsigned 64-bit integer value.
//@}


/**
*** @name Miscellaneous
**/
//@{

		/// Returns the type of this value
		virtual ValueType		type() const			{ return _type; }

		/// Returns the type of this value as a string
		virtual SWString		typeAsString() const;

		/// Returns the length of the data
		virtual size_t			length() const;

		/// Return a pointer to the data
		virtual	const void *	data() const;

		/**
		*** @brief	Dumps the field to stdout for debug purposes
		***
		*** @param [in]	showbytes	If true then the data bytes are written as a hex dump
		***							along with the other object information.
		**/
		virtual void			dump(); //bool showbytes=false);

		/// Clear/Reset field to NONE
		virtual void			clear()		{ clearValue(true); }

		/// Resets the changed flag to false.
		void					clearChangedFlag()				{ setChangedFlag(false); }

		/// Sets the changed flag to the specified value.
		virtual void			setChangedFlag(bool v=true)		{ _changed = v; }

		/// Returns the value of the changed flag.
		bool					changed() const					{ return _changed; }

		/// Test to see if this is a numeric value (VtInt8, VtInt16, etc)
		bool					isNumeric() const;

		/// Test to see if this is a string value (VtCString, VtWString)
		bool					isString() const;

		/// Read the data from a file (Sets file type as VtData)
		sw_status_t				readDataFromFile(const SWString &filename);

		/// Read the data from a file (Sets file type as VtData)
		sw_status_t				writeDataToFile(const SWString &filename);

		/// Return the value as a string containing Hexadecimal byte values separated by spaces, e.g. 00 AF 15 ...
		SWString				toHexDataString() const;
//@}


/**
*** @name Cast operators
**/
//@{
		/**
		*** Compares this value with the value supplied for comparision.
		***
		*** This method compares the value with the one supplied. If checkTypes is
		*** set to true then the data types must match before any data value is performed.
		*** If the types do not match the result returned is based on the numeric comparison
		*** of the type value.  If checkTypes is false then a "smart" comparision is done.
		***
		*** Data checking is as follows:
		*** Integer values are compared using subtraction to return a zero, <0 or >0 result and
		*** the result returned is the equivalent of (this->toInteger() - other.toInteger()).
		*** String values are compared using the strcmp like logic.
		***
		*** If checkTypes is false then "smart" comparision is done by converting values to a similar
		*** base. For example strings can be compared directly via the SWString class, numbers can
		*** be compared normally without casting. If a sensible smart conversion cannot be determine
		*** the comparison is done by calling the toInteger method.
		***
		*** @param	other		The data to compare against.
		*** @param	checkTypes	If true check data types, if false ignore data type
		***						and do an appropriate conversion to compare unlike
		***						data types.
		*** @return				The result of the comparision:
		***						-  0 - This object is equal to the other object.
		***						- <0 - This object is less than the other object.
		***						- >0 - This object is greater than the other object.
		**/
		int				compareTo(const SWValue &other, bool checkTypes=true) const;

		/// Equals operator - returns true if the TYPE and DATA match
		bool	operator==(const SWValue &other) const		{ return (compareTo(other, true) == 0); }

		/// NOT Equals operator - returns true if either the TYPE or the DATA fail to match
		bool	operator!=(const SWValue &other) const		{ return (compareTo(other, true) != 0); }

		/// Less Than operator
		bool	operator<(const SWValue &other) const		{ return (compareTo(other, false) < 0); }

		/// Less Than or equals operator
		bool	operator<=(const SWValue &other) const		{ return (compareTo(other, false) <= 0); }

		/// Greater Than operator
		bool	operator>(const SWValue &other) const		{ return (compareTo(other, false) > 0); }

		/// Greater Than or equals operator
		bool	operator>=(const SWValue &other) const		{ return (compareTo(other, false) >= 0); }
//@}


public: // Virtual overrides

		/// Override for SWObject writeToStream
		virtual sw_status_t		writeToStream(SWOutputStream &stream) const;

		/// Override for SWObject readFromStream
		virtual sw_status_t		readFromStream(SWInputStream &stream);


public: // STATIC methods

		/// Returns the type of this value as a string
		static SWString		typeAsString(ValueType vt);

		/**
		*** @brief Returns a string representing a boolean value.
		***
		*** This method takes the boolean value specified and converts it to a string using
		*** the current locale. If no information is available in the locale the string
		*** "true" or "false" is returned, depending on the value of the boolean parameter.
		***
		*** @param[in]	v	The boolean value to convert to a string.
		*** @return			The string representation of the boolean value.
		***
		*** @see SWLocale
		**/
		static SWString		booleanToString(bool v);


		/**
		*** @brief Returns a single character representing a boolean value.
		***
		*** This method takes the boolean value specified and converts it to a character using
		*** the current locale. If no information is available in the locale the character
		*** 'y' or 'n' is returned, depending on the value of the boolean parameter.
		***
		*** @param[in]	v	The boolean value to convert to a string.
		*** @return			The character representation of the boolean value.
		***
		*** @see SWLocale
		**/
		static int			booleanToChar(bool v);

		/**
		*** @brief Convert the given char to a true or false value
		***
		*** Static method to convert the given char to a true or false value.
		*** If the character is numeric ('0'..'9') then the boolean value is determined
		*** by the character be not equal to zero. Otherwise the current locale is used
		*** to determine if the given character corresponds to a true or false value.
		*** All other chars are considered to be false.
		***
		*** @param[in]	v	The character value to convert to a boolean.
		*** @return			The boolean value.
		***
		*** @see SWLocale
		**/
		static bool			charToBoolean(int v);

protected:
		/**
		*** @brief	Initialise this field according to the data given
		***
		*** @param[in]	ft			The data(value) type to assign to the object
		*** @param[in]	datalen		The length of the data
		*** @param[in]	changed		The value to assign to the changed flag.
		**/
		void				init(ValueType ft, size_t datalen, bool changed);

		/**
		*** @brief	Clear the field to be empty (type=VtNone)
		***
		*** @param[in]	updateChanged	If true the changed flag should be set if clearing this
		***								object causes the value to be changed. If false the value
		***								of the changed flag is unaffected by calling this method.
		**/
		virtual bool		clearValue(bool updateChanged);

protected: // STATIC members
		static SWMemoryManager			*__pMemoryManager;	///< The memory manager instance

protected:
		ValueType	_type;		///< The type of this value
		size_t		_datalen;	///< The size of the data
		bool		_changed;	///< A flag to indicate if the field has been changed
		union
			{
			bool				b;
			sw_int8_t			i8;
			sw_int16_t			i16;
			sw_int32_t			i32;
			sw_int64_t			i64;
			sw_uint8_t			ui8;
			sw_uint16_t			ui16;
			sw_uint32_t			ui32;
			sw_uint64_t			ui64;
			char				cc;
			wchar_t				wc;
			float				f;
			double				d;
			void				*pData;
			char				*cstr;
			wchar_t				*wstr;
			sw_int32_t			julian;
			sw_money_t			money;
			sw_internal_time_t	ts;
			} _u;				///< A union containing all possible data types
		};



#endif
