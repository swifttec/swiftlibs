/**
*** @file		SWPointerArray.cpp
*** @brief		An array of Pointers
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWPointerArray class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <swift/SWPointerArray.h>
#include <swift/SWPointer.h>
#include <swift/SWException.h>
#include <swift/SWInputStream.h>
#include <swift/SWOutputStream.h>
#include <swift/SWGuard.h>


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




SWPointerArray::SWPointerArray(sw_size_t initialSize) :
    SWAbstractArray(NULL, sizeof(void *))
		{
		ensureCapacity(initialSize);
		}


SWPointerArray::~SWPointerArray()
		{
		clear();
		}


void * &	
SWPointerArray::operator[](sw_index_t i)
		{
		if (i < 0) throw SW_EXCEPTION(SWArrayIndexOutOfBoundsException);
		ensureCapacity(i+1);
		if (i >= (int)m_nelems)
			{
			memset((void **)(m_pData) + m_nelems, 0, (i + 1 - m_nelems) * sizeof(int));
			m_nelems = i+1;
			}

		void **ip = (void **)(m_pData + (i * sizeof(void *)));

		return *ip;
		}

	
void * &	
SWPointerArray::get(sw_index_t i) const
		{
		if (i < 0 || i >= (sw_index_t)m_nelems) throw SW_EXCEPTION(SWArrayIndexOutOfBoundsException);

		void **ip = (void **)(m_pData + (i * sizeof(void *)));

		return *ip;
		}
	

void *
SWPointerArray::pop()
		{
		if (m_nelems < 1) throw SW_EXCEPTION(SWArrayIndexOutOfBoundsException);

		void **ip = (void **)(m_pData + ((--m_nelems) * sizeof(void *)));

		return *ip;
		}


SWPointerArray::SWPointerArray(const SWPointerArray &ua) :
    SWAbstractArray(NULL, sizeof(void *))
		{
		*this = ua;
		}


SWPointerArray &
SWPointerArray::operator=(const SWPointerArray &ua)
		{
		sw_size_t	n = ua.size();

		clear();

		// Do it backwards to prevent ensureCapacity from
		// doing multiple reallocs
		while (n > 0)
			{
			--n;
			(*this)[(sw_index_t)n] = ua.get((sw_index_t)n);
			}

		return *this;
		}


sw_status_t
SWPointerArray::writeToStream(SWOutputStream &stream) const
		{
		sw_status_t	res;
		SWPointer	p;
		int			i;

		res = stream.write((sw_int32_t)m_nelems);
		for (i=0;res==SW_STATUS_SUCCESS&&i<(int)m_nelems;i++)
			{
			p = get(i);
			res = stream.write(p);
			}

		return res;
		}


sw_status_t
SWPointerArray::readFromStream(SWInputStream &stream)
		{
		sw_status_t		res;
		sw_uint32_t		i, n;
		SWPointer		p;

		res = stream.read(n);
		if (res == SW_STATUS_SUCCESS)
			{
			clear();
			ensureCapacity(n);
			for (i=0;res==SW_STATUS_SUCCESS&&i<n;i++)
				{
				res = stream.read(p);
				if (res == SW_STATUS_SUCCESS) (*this)[i] = p;
				}
			}
		
		return res;
		}
	

int
SWPointerArray::compareTo(const SWPointerArray &other) const
		{
		SWReadGuard	guard(this);
		sw_size_t	nelems1=size();
		sw_size_t	nelems2=other.size();
		sw_index_t	pos=0;
		sw_index_t	res=0;
		void		*v1, *v2;

		while (res == 0 && (sw_size_t)pos < nelems1 && (sw_size_t)pos < nelems2)
			{
			v1 = get(pos);
			v2 = other.get(pos);

			if (v1 > v2) res = 1;
			else if (v1 < v2) res = -1;

			pos++;
			}

		if (res == 0)
			{
			if (nelems1 > nelems2) res = 1;
			else if (nelems1 < nelems2) res = -1;
			}

		return res;
		}


void
SWPointerArray::insert(void *v, sw_index_t pos)
		{
		insertElements(pos, 1);
		(*this)[pos] = v;
		}


void
SWPointerArray::add(void *v)
		{
		(*this)[(sw_index_t)m_nelems] = v;
		}


void
SWPointerArray::push(void *v)
		{
		(*this)[(sw_index_t)m_nelems] = v;
		}







