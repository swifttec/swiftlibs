/****************************************************************************
**	sw_crypt.cpp	Encryption and Decryption functions
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



// Needed for precompiled header support
#include "stdafx.h"

#include <swift/sw_trace.h>
#include <swift/sw_printf.h>


SWIFT_DLL_EXPORT void
sw_trace(const char *fmt, ...)
		{
		va_list		ap; 

		va_start(ap, fmt); 

#if defined(SW_PLATFORM_WINDOWS)
		char		str[1024];

		sw_vsnprintf(str, sizeof(str), fmt, ap);
		OutputDebugStringA(str);
#else
		sw_vprintf(fmt, ap);
#endif

		va_end(ap);
		}
