/**
*** @file		SWUUID.h
*** @brief		A class to represent a Universally Unique IDentifier (UUID).
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWUUID class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWUUID_h__
#define __SWUUID_h__

#include <swift/SWBitSet.h>
#include <swift/SWIntegerArray.h>
#include <swift/SWString.h>
#include <swift/SWInterlockedInt32.h>



// Forward declarations

/**
*** @brief	A class to represent a Universally Unique IDentifier (UUID).
***
*** A SWUUID object represents a Universally Unique IDentifier (UUID).
***	It can be constructed as a nil UUID (all zeros), from a string or from
*** binary representation of UUID. Methods are provided to return the UUID
*** in binary or string format.
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Unsafe</td>
*** </tr>
*** </table>
***
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWUUID: public SWObject
		{
public:
		/// An enumeration of UUID types
		enum UUIDType
			{
			UtNil=0,			///< A nil UUID (all zero)
			UtTime=1,			///< A time-based UUID.
			UtDCESecurity=2,	///< Reserved for DCE - Security version, with embedded POSIX UIDs.
			UtName=3,			///< A name-based UUID
			UtRandom=4,			///< A randomly or pseudo-randomly generated UUID.
			UtType5=5,			///< A type 5 UUID (reserved and currently invalid)
			UtType6=6,			///< A type 6 UUID (reserved and currently invalid)
			UtType7=7,			///< A type 7 UUID (reserved and currently invalid)
			UtType8=8,			///< A type 8 UUID (reserved and currently invalid)
			UtType9=9,			///< A type 9 UUID (reserved and currently invalid)
			UtType10=10,		///< A type 10 UUID (reserved and currently invalid)
			UtType11=11,		///< A type 11 UUID (reserved and currently invalid)
			UtType12=12,		///< A type 12 UUID (reserved and currently invalid)
			UtType13=13,		///< A type 13 UUID (reserved and currently invalid)
			UtType14=14,		///< A type 14 UUID (reserved and currently invalid)
			UtType15=15,		///< A type 15 UUID (reserved and currently invalid)
			UtInvalid=16		///< An invalid type.
			};

public:
		/**
		*** @brief	Default constructor - generates a nil UUID
		**/
		SWUUID();


		/**
		*** @brief	Default constructor - generates a UUID of the specified type
		**/
		SWUUID(UUIDType);


		/**
		*** @brief	Copy constructor
		**/
		SWUUID(const SWUUID &);


		/**
		*** @brief	Construct a UUID from a sw_uuid_t struct.
		**/
		SWUUID(const sw_uuid_t &);


		/**
		*** @brief	Construct a UUID from a string
		**/
		SWUUID(const SWString &);


		/**
		*** @brief	Construct a UUID from the specified buffer
		**/
		SWUUID(const SWBuffer &);


		/// Virtual Destructor
		virtual ~SWUUID()	{ }

		/**
		*** @brief	Assignment operator
		**/
		SWUUID &	operator=(const SWUUID &);


		/**
		*** @brief	Return a representation of this UUID set as a string
		**/
		virtual SWString	toString() const;


		/**
		*** @brief			Return a binary representation of this UUID in a buffer
		*** @param[out]	buf	Receives the binary data of this UUID object.
		**/
		void				toBinary(SWBuffer &buf) const;


		/**
		*** @brief	Returns the type of UUID
		**/
		UUIDType			type() const		{ int	version=(m_uuid.timeHighAndVersion >> 12) & 0xf; UUIDType t=((version > 0 && version < 5)?(UUIDType)version:(version==0&&(*this==SWUUID())?UtNil:UtInvalid)); return t; }


		/**
		*** @brief	Returns the timestamp portion of the UUID
		**/
		sw_uint64_t			timestamp() const	{  return ((sw_uint64_t)m_uuid.timeLow | ((sw_uint64_t)m_uuid.timeMid << 32) | ((sw_uint64_t)(m_uuid.timeHighAndVersion & 0x0fff) << 48)); }


		/**
		*** @brief	Returns the clockseq portion of the UUID
		**/
		sw_uint16_t			clockseq() const	{ return (sw_uint16_t)(((sw_uint16_t)(m_uuid.clockSeqHighAndVariant & 0x3f) << 8) | (sw_uint16_t)m_uuid.clockSeqLow); }


		/**
		*** @brief	Compare this object with another.
		***
		*** This method compares this object with the one specified and returns a
		*** result indicating if the other object is less than, equal to, or greater
		*** than this object.
		***
		*** - If this object is less than the specified object then a negative result is returned.
		*** - If this object is greater than the specified object then a positive result is returned.
		*** - If this object is equal to the specified object then a zero result is returned.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				The result of the comparision:
		***						-  0 - This object is equal to the other object.
		***						- <0 - This object is less than the other object.
		***						- >0 - This object is greater than the other object.
		***
		*** @note
		*** The compareTo method is not required to return -1 and +1 for the negative and positive
		*** values although this is typically the value returned. Callers should test for \< 0 and \> 0
		*** rather than for specific values of -1 and +1.
		**/
		int			compareTo(const SWUUID &other) const;

		/**
		*** @brief	Test for equality
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is equal to the other object, false otherwise.
		**/
		bool	operator==(const SWUUID &other) const	{ return (compareTo(other) == 0); }

		/**
		*** @brief	Test for inequality
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is not equal to the other object, false otherwise.
		**/
		bool	operator!=(const SWUUID &other) const	{ return (compareTo(other) != 0); }

		/**
		*** @brief	Test for less than.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is less than the other object, false otherwise.
		**/
		bool	operator< (const SWUUID &other) const	{ return (compareTo(other) <  0); }

		/**
		*** @brief	Test for greater than.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is greater than the other object, false otherwise.
		**/
		bool	operator> (const SWUUID &other) const	{ return (compareTo(other) >  0); }

		/**
		*** @brief	Test for less than or equal to.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is less than or equal to the other object, false otherwise.
		**/
		bool	operator<=(const SWUUID &other) const	{ return (compareTo(other) <= 0); }

		/**
		*** @brief	Test for greater than or equal to.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is greater than or equal to the other object, false otherwise.
		**/
		bool	operator>=(const SWUUID &other) const	{ return (compareTo(other) >= 0); }


		operator sw_uuid_t() const						{ return m_uuid; }

public: // Static methods

		/**
		*** @brief	Static method used to generate a UUID of the requested type
		***
		*** This method can be used to generate a UUID based on the specified
		*** type. The types of UUID that can be generated by this class are
		***
		*** - UtNil
		*** - UtTime
		*** - UtRandom
		***
		*** @param[out]	uuid	The uuid object to receive the generated value
		*** @param[in]	ut		The type of UUID to generated (nil, time-based or random)
		***
		*** @return			SW_STATUS_SUCCESSFUL if successful or an error code otherwise.
		**/
		static sw_status_t		generate(SWUUID &uuid, UUIDType ut);

public: // virtual overrides

		/// Override for SWObject writeToStream
		virtual sw_status_t		writeToStream(SWOutputStream &stream) const;

		/// Override for SWObject readFromStream
		virtual sw_status_t		readFromStream(SWInputStream &stream);


private: // static members
		static SWInterlockedInt32	sm_seq;		///< A static interlocked integer used for generating sequntial UUIDs

private: // members
		sw_uuid_t		m_uuid;	///< The UUID data
		};


/// A general UUID exception.
SW_DEFINE_EXCEPTION(SWUUIDException, SWException);




#endif
