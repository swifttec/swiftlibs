/**
*** @file		SWFlags32.cpp
*** @brief		A class to represent a set of boolean flags.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWFlags32 class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <swift/SWFlags32.h>
//#include <swift/SWInputStream.h>
//#include <swift/SWOutputStream.h>



// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




// Set the nth bit to the specified value
void
SWFlags32::setBit(int n, bool v)
		{
		if (n < 0 || n >= size()) throw SW_EXCEPTION(SWIndexOutOfBoundsException);

		if (v) m_flags |= ((sw_int32_t)1<<n);
		else m_flags &= ~((sw_int32_t)1<<n);
		}


// Set the nth bit to the specified value
bool
SWFlags32::getBit(int n) const
		{
		if (n < 0 || n >= size()) throw SW_EXCEPTION(SWIndexOutOfBoundsException);

		return (m_flags&((sw_int32_t)1<<n))?1:0;
		}


bool
SWFlags32::isSet(sw_int32_t v, bool anyBits) const
		{
		sw_int32_t	res;

		res = m_flags & v;

		if (anyBits) return (res != 0);
		
		return (res == v);
		}


/*
sw_status_t
SWFlags32::writeToStream(SWOutputStream &stream) const
		{
		return stream.write(m_flags);
		}


sw_status_t
SWFlags32::readFromStream(SWInputStream &stream)
		{
		return stream.read(m_flags);
		}
*/	


