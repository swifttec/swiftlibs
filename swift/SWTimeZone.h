/**
*** @file		SWTimeZone.h
*** @brief		A class representing timezone information
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWTimeZone class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




#ifndef __SWTimeZone_h__
#define __SWTimeZone_h__

#include <swift/swift.h>
#include <swift/SWObject.h>
#include <swift/SWString.h>




// Forward declarations

/**
*** @brief	A class representing timezone information
***
*** A class representing timezone information
**/
class SWIFT_DLL_EXPORT SWTimeZone : public SWObject
		{
public:
		SWTimeZone();

		/**
		*** @brief	Default constructor
		***
		*** @param[in]	name			The name of this timezone.
		*** @param[in]	tzAdjustment	The adjustment in seconds for the timezone (from GMT)
		*** @param[in]	dstAdjustment	The adjustment in seconds to be applied be DST is in effect
		***	@param[in]	dstAvailable	Indicates if DST is available or not (see isDST for more)
		*** @param[in]	stdText			The timezone name when DST is not in effect (e.g. GMT)
		*** @param[in]	dstText			The timezone name when DST is in effect (e.g. BST)
		**/
		SWTimeZone(const SWString &name, int tzAdjustment=0, int dstAdjustment=0, int dstAvailable=0, const SWString &stdText="", const SWString &dstText="");

		/// Virtual destructor
		virtual ~SWTimeZone();

		/// Copy constructor
		SWTimeZone(const SWTimeZone &other);

		/// Assignment operator
		SWTimeZone &	operator=(const SWTimeZone &other);

		// Check is TZ is valid
		bool				isValid() const			{ return m_valid; }

		/// Returns the time adjustment in seconds for the timezone
		int					tzAdjustment() const	{ return m_tzAdjustment; }

		/// Returns the time adjustment in seconds to be applied when Daylight Savings is in effect
		int					dstAdjustment() const	{ return m_dstAdjustment; }

		/// Returns the availability of DST for this timezone
		int					dstAvailable() const	{ return m_dstAvailable; }

		/// Return the name of this timezone
		const SWString &	name() const	{ return m_name; }

		/// Return the timezone mnemonic
		const SWString &	stdText() const	{ return m_stdText; }

		/// Return the timezone mnemonic
		const SWString &	dstText() const	{ return m_dstText; }

		/**
		*** Sets the timezone parameters.
		***
		*** @param	tzname			The name of the timezone.
		*** @param	tzAdjustment	The adjustment in seconds for the timezone (from GMT)
		*** @param	dstAdjustment	The adjustment in seconds to be applied be DST is in effect
		***	@param	dstAvailable	Indicates if DST is available or not (see isDST for more)
		*** @param	stdText			The timezone name when DST is not in effect (e.g. GMT)
		*** @param	dstText			The timezone name when DST is in effect (e.g. BST)
		**/
		void				set(const SWString &tzname, int tzAdjustment, int dstAdjustment, int dstAvailable, const SWString &stdText, const SWString &dstText);

public: // Static methods

		/// Get a reference to the current time zone
		static SWTimeZone &		getCurrentTimeZone();


private: // Static Members
		static SWTimeZone		sm_currtimezone;		///< The current timezone

private: // Members
		bool		m_valid;			///< A valididty flag
		SWString	m_name;				///< The name of this timezone
		int			m_tzAdjustment;		///< Time adjustment in seconds for the timezone
		int			m_dstAdjustment;	///< Time adjustment in seconds for Daylight Savings
		int			m_dstAvailable;		///< DST availability indicator (-1=Unavailable, 0=No, +1=Yes)
		SWString	m_stdText;			///< The text for the timezone when DST is not in effect
		SWString	m_dstText;			///< The text for the timezone when DST is in effect
		};




#endif
