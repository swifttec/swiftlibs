/**
*** @file		SWArrayS.h
*** @brief		An array template
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWArrayS class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWArrayS_h__
#define __SWArrayS_h__

#include <swift/SWObject.h>
#include <swift/SWMemoryManager.h>
#include <swift/SWInputStream.h>
#include <swift/SWOutputStream.h>

template<class T>
class SWArraySElement
		{
public:
		/// Default Constructor
		SWArraySElement()											{ }

		/// Data copy constructor
		SWArraySElement(const T &data) : m_data(data)				{ }

		/// Custom new operator
		void *			operator new(size_t nbytes, sw_size_t &res)	{ res = nbytes; return malloc(nbytes); }

		/// Custom new operator
		void *			operator new(size_t /*nbytes*/, void *p)	{ return p; }

		/// Custom delete operator
		void			operator delete(void *, sw_size_t &)		{ }

		/// Custom delete operator
		void			operator delete(void *, void *)				{ }

		/// Custom delete operator
		void			operator delete(void *)						{ }

		/// Get data reference
		T &				data()										{ return m_data; }

private:
		T	m_data;		///< The actual data
		};


/**
*** @brief	An array template based on the Array class.
***
*** An array of SWString objects based on the Array class.
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Safe when synchronized</td>
*** </tr>
*** </table>
***
*** @author		Simon Sparkes
**/
template<class T>
class SWArrayS : public SWObject
		{
public:
		/**
		*** @brief	Construct an array with an optional initial size and increment.
		***
		*** @param[in]	initialSize		The initial size of the array (default=0)
		*** @param[in]	incsize			The amount by which to grow the capacity of the array when a resize is required.
		**/
		SWArrayS();

		/// Destructor
		virtual ~SWArrayS();


		/**
		*** @brief Copy constructor
		***
		*** The copy constructor creates an empty array and the calls the
		*** assignment operator to copy all of the data.
		***
		*** @param[in]	other	Th array to copy.
		**/
		SWArrayS(const SWArrayS<T> &other);


		/**
		*** @brief Assignment operator
		***
		*** The assignment operator will clear the existing data from
		*** the array using the clear method and copy all of the values
		*** from the array specified.
		***
		*** @param[in]	other	Th array to copy.
		**/
		SWArrayS<T> &		operator=(const SWArrayS<T> &other);


		/**
		*** @brief Returns the nth element growing the array as required
		***
		*** This method returns a reference to the nth element in the array
		*** which can be modified as required.
		*** If the index specified is greater than the number of elements in the
		*** array, the array is automatically extended to encompass the referenced
		*** element.
		***
		*** If the index specified is negative an exception is thrown.
		***
		*** @param[in]	i	The index of the element to access.
		**/
		T &		operator[](sw_index_t i);


		/**
		*** @brief Returns the nth element without growing the array
		***
		*** Gets the nth element without growing the array. If the index is out of bounds
		*** an exception is thrown rather than growing the array automatically. Provided
		*** for use when a const operation is required.
		***
		*** @param[in]	i	The index of the element to access.
		**/
		T &				get(sw_index_t i) const;


		/**
		*** @brief	Clear the array of all data and reset the size to zero.
		**/
		virtual void	clear();


		/**
		*** @brief Remove one or more elements from the array.
		***
		*** @param[in]	pos		The position to remove elements from.
		*** @param[in]	count	The number of elements to remove.
		**/
		void			remove(sw_index_t pos, sw_size_t count=1);



		/**
		*** @brief	Add the specified value to the end of the array.
		***
		*** @param[in]	v		The value to add.
		**/
		void			add(const T &v);


		/**
		*** Add (insert) at the specified position in the array
		***
		*** @param[in]	v		The value to add.
		*** @param[in]	pos		The position at which to insert the value.
		**/
		void			insert(const T &v, sw_index_t pos);


		/**
		*** @brief	Push a value onto the end of the array
		***
		*** @param[in]	v	The value to add.
		**/
		void			push(const T &v);


		/**
		*** @brief	Pop and return the last element off of the array
		**/
		T				pop();

		/// Return the number of elements in the array
		sw_size_t		size() const;


		/**
		*** @brief	Write this object to the given output stream.
		***
		*** This method will write the object data to the output stream in a way
		*** that can be read by a corresponding input stream.
		***
		*** @note
		*** Object developers should override these methods as the default handling
		*** is to return an error status.
		***
		*** @param[in]	stream	The stream to write to.
		***
		*** @return				The write status.
		**/
		virtual sw_status_t		writeToStream(SWOutputStream &stream) const;


		/**
		*** @brief	Read this object from the given input stream.
		***
		*** This method will read the object data from the input stream. The input
		*** stream data is expected to have been written by a corresponding output
		*** stream.
		***
		*** @note
		*** Object developers should override these methods as the default handling
		*** is to return an error status.
		***
		*** @param[in]	stream	The stream to write to.
		***
		*** @return				The write status.
		**/
		virtual sw_status_t		readFromStream(SWInputStream &stream);

private:
		void					ensureCapacity(sw_size_t nelems);

private:
		SWMemoryManager	*m_pMemoryManager;	///< The memory manager
		char			*m_pData;			///< The data buffer
		sw_size_t		m_capacity;			///< The capacity of the data buffer
		sw_size_t		m_elemsize;			///< The data element size
		sw_size_t		m_nelems;			///< The number of data elements
		};





// Include the implementation
#include <swift/SWArrayS.cpp>

#endif
