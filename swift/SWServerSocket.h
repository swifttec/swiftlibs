/**
*** @file		SWServerSocket.h
*** @brief		A generic socket implementation
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWServerSocket class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWServerSocket_h__
#define __SWServerSocket_h__

#include <swift/SWClientSocket.h>



// Forward declarations
class SWSocket;

/**
*** @brief	A non-specific server socket
***
*** This object provides access to the server parts of a socket implementation.
*** It allows the caller to set up the socket to listen for and accept connections
*** into SWClientSocket object.
***
*** The developer has the option of supplying the underlying socket or allowing
*** the class to provide it.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWServerSocket : public SWObject
		{
public:
		/// Default constructor
		SWServerSocket();

		// Specific socket type constructor
		SWServerSocket(SWSocket *pSocket, bool delflag=false);

		/// Virtual destructor
		~SWServerSocket();

		/// Opens the socket and prepares it to accept connections
		sw_status_t			listen(const SWSocketAddress &sockaddr);

		/// Close the socket
		sw_status_t			close();

		/// Accept a connection on the socket
		sw_status_t			accept(SWClientSocket &sock, sw_int64_t timeout=SW_TIMEOUT_INFINITE);

		/// Check if this socket is listening or not.
		bool				isListening() const;

		/// Returns a pointer to the socket implementation
		SWSocket *			getSocket()	{ return _pSocket; }

protected:
		SWSocket	*_pSocket;	///< A pointer to the actual socket implementation
		bool		_delFlag;	///< A flag to indicate if the socket implementator should be delete on destruction
		};




#endif
