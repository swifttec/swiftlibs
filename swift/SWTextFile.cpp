/**
*** @file		SWTextFile.cpp
*** @brief		A class to handle a text file
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWTextFile class.
***
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <swift/SWTextFile.h>
#include <swift/SWException.h>



/******************** Generic Text File Handler ********************/
SWTextFileHandler::SWTextFileHandler(SWTextFile *pTextFile) :
	m_pTextFile(pTextFile)
		{
		}

SWTextFileHandler::~SWTextFileHandler()
		{
		}

/******************** ANSI Text Files ********************/
class ANSITextFileHandler : public SWTextFileHandler
		{
public:
		ANSITextFileHandler(SWTextFile *pTextFile) : SWTextFileHandler(pTextFile)	{ }

		virtual sw_status_t			readChar(int &c);
		virtual sw_status_t			writeBOM();
		virtual sw_status_t			writeChar(int c);
		};

sw_status_t
ANSITextFileHandler::readChar(int &c)
		{
		sw_status_t	res;
		sw_byte_t	b;

		res = m_pTextFile->readByte(b);
		if (res == SW_STATUS_SUCCESS)
			{
			c = b;
			}

		return res;
		}


sw_status_t
ANSITextFileHandler::writeChar(int c)
		{
		sw_byte_t	b[1];

		b[0] = c & 0xff;
		return m_pTextFile->writeBytes(b, 1);
		}


sw_status_t
ANSITextFileHandler::writeBOM()
		{
		// Do nothing!

		return SW_STATUS_SUCCESS;
		}


/******************** UTF8 Text Files ********************/
class UTF8TextFileHandler : public SWTextFileHandler
		{
public:
		UTF8TextFileHandler(SWTextFile *pTextFile) : SWTextFileHandler(pTextFile)	{ }

		virtual sw_status_t			readChar(int &c);
		virtual sw_status_t			writeBOM();
		virtual sw_status_t			writeChar(int c);
		};


sw_status_t
UTF8TextFileHandler::readChar(int &c)
		{
		sw_status_t	res;
		sw_byte_t	b1, b2, b3, b4;

		if ((res = m_pTextFile->readByte(b1)) == SW_STATUS_SUCCESS)
			{
			if (b1 < 0x80) // U-0000 - U-007F: 0bbbbbbb, plus EOF / ERROR
				{
				c = b1;
				}
			else
				{
				if ((res = m_pTextFile->readByte(b2)) == SW_STATUS_SUCCESS)
					{
					if (b1 < 0xe0) // U-0080 - U-07FF: 110bbbbb 10bbbbbb
						{
						c = ((b1 & 0x1F) << 6) | (b2 & 0x3F);
						}
					else if ((res = m_pTextFile->readByte(b3)) == SW_STATUS_SUCCESS)
						{
						if (b1 < 0xf0) // U-0800 - U-FFFF: 1110bbbb 10bbbbbb 10bbbbbb
							{
							c =  ((b1 & 0x0F) << 12) | ((b2 & 0x3F) << 6) | (b3 & 0x3F);
							}
						else if ((res = m_pTextFile->readByte(b4)) == SW_STATUS_SUCCESS)
							{
							if (b1 < 0xf8) // U-00010000 - U-001FFFFF:  11110bbb 10bbbbbb 10bbbbbb 10bbbbbb
								{
								c = ((b1 & 0x07) << 18) | ((b2 & 0x3F) << 12) | ((b3 & 0x3F) << 6) | (b4 & 0x3f);
								}
							else
								{
								// illegal lead-in byte
								c = 0xFFFD;
								}
							}
						}
					}
				}
			}

		return res;
		}


sw_status_t
UTF8TextFileHandler::writeChar(int ch)
		{
		sw_byte_t	b[4];
		int			len=0;

		if (ch < 0x80)              // U-0000 - U-007F: 0bbbbbbb
			{
			b[0] = (sw_byte_t)ch;
			len = 1;
			}
		else if (ch < 0x800)             // U-0080 - U-07FF: 110bbbbb 10bbbbbb
			{
			b[0] = (sw_byte_t)((ch >> 6) | 0xc0);
			b[1] = (sw_byte_t)((ch & 0x3F) | 0x80);
			len = 2;
			}
		else if (ch < 0xFFFF)            // U-0800 - U-FFFF: 1110bbbb 10bbbbbb 10bbbbbb
			{
			b[0] = (sw_byte_t)((ch >> 12) | 0xe0);
			b[1] = (sw_byte_t)(((ch >> 6) & 0x3F) | 0x80);
			b[2] = (sw_byte_t)((ch & 0x3F) | 0x80);
			len = 3;
			}
		else
			{
			// U-00010000 - U-001FFFFF: 11110bbb 10bbbbbb 10bbbbbb 10bbbbbb
			b[0] = (sw_byte_t)((ch >> 18) | 0xf0);
			b[1] = (sw_byte_t)(((ch >> 12) & 0x3F) | 0x80);
			b[2] = (sw_byte_t)(((ch >> 6) & 0x3F) | 0x80);
			b[3] = (sw_byte_t)((ch & 0x3F) | 0x80);
			len = 4;
			}

		return m_pTextFile->writeBytes(b, len);
		}


sw_status_t
UTF8TextFileHandler::writeBOM()
		{
		sw_byte_t bom[3] = { 0xEF, 0xBB, 0xBF };

		return m_pTextFile->writeBytes(bom, 3);
		}


/******************** UTF16 (Little Endian) Text Files ********************/
class UTF16LETextFileHandler : public SWTextFileHandler
		{
public:
		UTF16LETextFileHandler(SWTextFile *pTextFile) : SWTextFileHandler(pTextFile)	{ }

		virtual sw_status_t			readChar(int &c);
		virtual sw_status_t			writeBOM();
		virtual sw_status_t			writeChar(int c);
		};

sw_status_t
UTF16LETextFileHandler::readChar(int &c)
		{
		sw_status_t	res;
		sw_byte_t	b[2];

		res = m_pTextFile->readByte(b[0]);
		if (res == SW_STATUS_SUCCESS) res = m_pTextFile->readByte(b[1]);
		if (res == SW_STATUS_SUCCESS)
			{
			c = b[1] << 8 | b[0];
			}

		return res;
		}


sw_status_t
UTF16LETextFileHandler::writeChar(int c)
		{
		sw_byte_t	b[2];

		b[0] = c & 0xff;
		b[1] = (c >> 8) & 0xff;
		return m_pTextFile->writeBytes(b, 2);
		}


sw_status_t
UTF16LETextFileHandler::writeBOM()
		{
		sw_byte_t bom[] = { 0xFF, 0xFE };

		return m_pTextFile->writeBytes(bom, 2);
		}



/******************** UTF16 (Big Endian) Text Files ********************/
class UTF16BETextFileHandler : public SWTextFileHandler
		{
public:
		UTF16BETextFileHandler(SWTextFile *pTextFile) : SWTextFileHandler(pTextFile)	{ }

		virtual sw_status_t			readChar(int &c);
		virtual sw_status_t			writeBOM();
		virtual sw_status_t			writeChar(int c);
		};


sw_status_t
UTF16BETextFileHandler::readChar(int &c)
		{
		sw_status_t	res;
		sw_byte_t	b[2];

		res = m_pTextFile->readByte(b[0]);
		if (res == SW_STATUS_SUCCESS) res = m_pTextFile->readByte(b[1]);
		if (res == SW_STATUS_SUCCESS)
			{
			c = b[0] << 8 | b[1];
			}

		return res;
		}


sw_status_t
UTF16BETextFileHandler::writeChar(int c)
		{
		sw_byte_t	b[2];

		b[1] = c & 0xff;
		b[0] = (c >> 8) & 0xff;
		return m_pTextFile->writeBytes(b, 2);
		}


sw_status_t
UTF16BETextFileHandler::writeBOM()
		{
		sw_byte_t bom[] = { 0xFE, 0xFF };

		return m_pTextFile->writeBytes(bom, 2);
		}





/******************** Main Class ********************/

// Default constructor
SWTextFile::SWTextFile() :
	m_pFileHandler(NULL),
	m_datapos(0),
	m_flags(0),
	m_savedChar(0),
	m_savedCharFlag(false)
		{
		// Set initial buffer capacities
		m_data.capacity(16384);
		m_linedata.capacity(1024);
		}


SWTextFile::~SWTextFile()
		{
		close();
		}


sw_status_t
SWTextFile::open(const SWString &filename, int mode, SWFileAttributes *pfa)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		int			filemode=0;

		m_datapos = 0;
		m_data.size(0);
		switch (mode & TF_MODE_MASK)
			{
			case TF_MODE_READ:		
				filemode = SWFile::ModeBinary | SWFile::ModeReadOnly;	
				break;

			case TF_MODE_WRITE:		
				filemode = SWFile::ModeBinary | SWFile::ModeCreate | SWFile::ModeTruncate | SWFile::ModeWriteOnly;
				break;

			/*
			case TF_MODE_APPEND:	
				filemode = SWFile::ModeBinary | SWFile::ModeCreate | SWFile::ModeWriteOnly;
				break;
			*/

			default:
				res = SW_STATUS_INVALID_DATA;
				break;
			}

		if (res == SW_STATUS_SUCCESS)
			{
			res = m_file.open(filename, filemode, pfa);
			}

		if (res == SW_STATUS_SUCCESS)
			{
			// We have successfully opened the file - now do the initial setup
			m_flags = mode;
			m_savedChar = 0;
			m_savedCharFlag = false;

			if (isReadMode())
				{
				if (!(m_flags & TF_FLAGS_NO_READ_BOM))
					{
					int	b1, b2, b3;

					b1 = readByte();
					if (b1 == 0xff)
						{
						b2 = readByte();
						if (b2 == 0xfe)
							{
							// This is a UTF16LE file
							m_flags &= ~ TF_ENCODING_MASK;
							m_flags |= TF_ENCODING_UTF16_LE;
							}
						else
							{
							m_datapos -= 2;
							}
						}
					else if (b1 == 0xfe)
						{
						b2 = readByte();
						if (b2 == 0xff)
							{
							// This is a UTF16BE file
							m_flags &= ~ TF_ENCODING_MASK;
							m_flags |= TF_ENCODING_UTF16_BE;
							}
						else
							{
							m_datapos -= 2;
							}
						}
					else if (b1 == 0xef)
						{
						b2 = readByte();
						if (b2 == 0xbb)
							{
							b3 = readByte();
							if (b3 == 0xbf)
								{
								// This is a UTF18 file
								m_flags &= ~ TF_ENCODING_MASK;
								m_flags |= TF_ENCODING_UTF8;
								}
							else
								{
								m_datapos -= 3;
								}
							}
						else
							{
							m_datapos -= 2;
							}
						}
					else
						{
						m_datapos = 0;
						}
					}
				}

			switch (encoding())
				{
				case TF_ENCODING_ANSI:		m_pFileHandler = new ANSITextFileHandler(this);		break;
				case TF_ENCODING_UTF8:		m_pFileHandler = new UTF8TextFileHandler(this);		break;
				case TF_ENCODING_UTF16_LE:	m_pFileHandler = new UTF16LETextFileHandler(this);	break;
				case TF_ENCODING_UTF16_BE:	m_pFileHandler = new UTF16BETextFileHandler(this);	break;

				default:					m_pFileHandler = new ANSITextFileHandler(this);		break;
				}


			if (isWriteMode() && !(m_flags & TF_FLAGS_NO_WRITE_BOM))
				{
				// write the BOM unless flags prohibit
				m_pFileHandler->writeBOM();
				}
			}

		return res;
		}


sw_status_t
SWTextFile::close()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (isOpen())
			{
			flush();
			m_file.close();
			m_flags = 0;
			delete m_pFileHandler;
			m_pFileHandler = NULL;
			m_datapos = 0;
			m_savedChar = 0;
			m_savedCharFlag = false;
			}

		return res;
		}


sw_status_t
SWTextFile::flush()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (isWriteMode())
			{
			if (m_datapos > 0)
				{
				res = m_file.write(m_data, m_datapos);
				m_datapos = 0;
				}
			}

		return res;
		}


sw_status_t
SWTextFile::readLine(SWString &line, bool stripnl)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		int			c;
		int			e=encoding();
		int			cs=0;
		int			pos=0;

		if (e == TF_ENCODING_ANSI) cs = sizeof(char);
		else cs = sizeof(wchar_t);

		line.clear();
		if (isReadMode())
			{
			while ((res = readChar(c)) == SW_STATUS_SUCCESS)
				{
				if (c == '\r')
					{
					// Possible CR/LF
					int		c2;

					if (readChar(c2) == SW_STATUS_SUCCESS)
						{
						if (c2 != '\n')
							{
							m_savedChar = c2;
							m_savedCharFlag = true;
							}
						}

					c = '\n';
					}

				if (c != '\n' || !stripnl)
					{
					if (e == TF_ENCODING_ANSI)
						{
						char	*p=(char *)m_linedata.data((pos + 1) * cs);

						p[pos++] = c;
						}
					else
						{
						wchar_t	*p=(wchar_t *)m_linedata.data((pos + 1) * cs);

						p[pos++] = c;
						}
					}

				if (c == '\n')
					{
					break;
					}
				}

			if (res != SW_STATUS_SUCCESS && pos > 0)
				{
				// Postpone EOF error
				res = SW_STATUS_SUCCESS;
				}
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}

		if (res == SW_STATUS_SUCCESS)
			{
			if (e == TF_ENCODING_ANSI)
				{
				char	*p=(char *)m_linedata.data((pos + 1) * cs);

				p[pos++] = 0;
				line = p;
				}
			else
				{
				wchar_t	*p=(wchar_t *)m_linedata.data((pos + 1) * cs);

				p[pos++] = 0;
				line = p;
				}
			}

		return res;
		}


sw_status_t
SWTextFile::readChar(int &c)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (isReadMode())
			{
			if (m_savedCharFlag)
				{
				c = m_savedChar;
				m_savedCharFlag = false;
				}
			else
				{
				res = m_pFileHandler->readChar(c);
				}
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}

		return res;
		}


sw_status_t
SWTextFile::writeLine(const SWString &line)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (isWriteMode())
			{
			res = writeString(line);
			res = writeChar('\n');
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}

		return res;
		}


sw_status_t
SWTextFile::writeChar(int c)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (isWriteMode())
			{
			m_pFileHandler->writeChar(c);
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}

		return res;
		}


int
SWTextFile::readByte()
		{
		sw_byte_t	b;
		int			res;

		if (readByte(b) == SW_STATUS_SUCCESS)
			{
			res = b & 0xff;
			}
		else
			{
			res = -1;
			}

		return res;
		}


sw_status_t
SWTextFile::readByte(sw_byte_t &b)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (isReadMode())
			{
			if (m_datapos >= m_data.size())
				{
				sw_size_t	bytesread;

				m_datapos = 0;
				res = m_file.read(m_data, m_data.capacity(), &bytesread);
				if (res == SW_STATUS_SUCCESS)
					{
					m_data.size(bytesread);
					}
				}

			if (m_datapos < m_data.size())
				{
				b = m_data.getByteAt(m_datapos++);
				}
			else
				{
				res = SW_STATUS_NO_MORE_DATA;
				}
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}

		return res;
		}


sw_status_t
SWTextFile::writeBytes(sw_byte_t *pb, int nbytes)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (isWriteMode())
			{
			while (res == SW_STATUS_SUCCESS && nbytes > 0)
				{
				if (m_datapos == m_data.capacity())
					{
					flush();
					}

				m_data.setByteAt(m_datapos++, *pb++);
				nbytes--;
				}
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}

		return res;
		}


sw_status_t
SWTextFile::writeString(const SWString &v)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (isWriteMode())
			{
			if (encoding() == TF_ENCODING_ANSI)
				{
				const char	*s=v.c_str();

				while (res == SW_STATUS_SUCCESS && *s)
					{
					res = writeChar(*s++);
					}
				}
			else
				{
				const wchar_t	*s=v.w_str();

				while (res == SW_STATUS_SUCCESS && *s)
					{
					res = writeChar(*s++);
					}
				}
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}

		return res;
		}



