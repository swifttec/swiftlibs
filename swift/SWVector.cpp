/**
*** @file		SWVector.cpp
*** @brief		Templated vector implementation.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWVector class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWVector_cpp__
#define __SWVector_cpp__



template<class T> 
SWVector<T>::SWVector() :
	m_pMemoryManager(0),
	m_elemsize(0),
	m_capacity(0),
	m_data(0)
		{
		// Try to ensure good alignment internally
		m_elemsize = sizeof(SWVectorElement<T>);
		m_elemsize = ((m_elemsize + sizeof(void *) - 1) / sizeof(void *)) * sizeof(void *);

		m_pMemoryManager = SWMemoryManager::getDefaultMemoryManager();
		}


template<class T> 
SWVector<T>::SWVector(const SWVector<T> &other) :
	m_pMemoryManager(0),
	m_elemsize(0),
	m_capacity(0),
	m_data(0)
		{
		// Try to ensure good alignment internally
		m_elemsize = sizeof(SWVectorElement<T>);
		m_elemsize = ((m_elemsize + sizeof(void *) - 1) / sizeof(void *)) * sizeof(void *);

		m_pMemoryManager = SWMemoryManager::getDefaultMemoryManager();

		// Copy the data via the assigment operator
		*this = other;
		}



template<class T> SWVector<T> &
SWVector<T>::operator=(const SWVector<T> &other)
		{
		sw_index_t	n = (sw_index_t)other.size();

		clear();
		setSize(n);

		// Do it backwards to avoid doing multiple reallocs
		while (n > 0)
			{
			--n;
			(*this)[n] = other.get(n);
			}

		return *this;
		}


template<class T> 
SWVector<T>::~SWVector()
		{
		clear();

		if (m_data != NULL) m_pMemoryManager->free(m_data);
		}

template<class T> void
SWVector<T>::clear()
		{
		setElementCount(0);
		}


template<class T> T &
SWVector<T>::get(int pos) const
		{
		SWReadGuard				guard(this);
		SWVectorElement<T>	*pElement=NULL;

		if (pos >= 0 && pos < (int)size())
			{
			pElement = (SWVectorElement<T> *)((char *)m_data + pos * m_elemsize);
			}
		else throw SW_EXCEPTION(SWIndexOutOfBoundsException);

		return pElement->data();
		}


template<class T> void
SWVector<T>::add(const T &data)
		{
		SWWriteGuard	guard(this);
		int				nelems=(int)size();

		ensureCapacityNoLock(nelems+1);

		new ((char *)m_data + nelems * m_elemsize) SWVectorElement<T>(data);
		incSize();
		}


template<class T> void
SWVector<T>::insert(const T &data, int pos)
		{
		SWWriteGuard			guard(this);
		int						nelems=(int)size();

		if (pos >= 0 && pos <= nelems)
			{
			ensureCapacityNoLock(nelems+1);

			int		ipos = nelems;
			char	*p = m_data + (ipos * m_elemsize);
			char	*q = p - m_elemsize;
			bool	exists=false;

			// Increase the number of elems by one and shift all the elems up by 1
			// not forgetting to make sure we have enough space first!
			if (ipos > pos)
				{
				new (p) SWVectorElement<T>((((SWVectorElement<T> *)q)->data()));
				p -= m_elemsize;
				q -= m_elemsize;
				ipos--;
				exists = true;
				}
			
			while (ipos > pos)
				{
				(((SWVectorElement<T> *)p)->data()) = (((SWVectorElement<T> *)q)->data());
				p -= m_elemsize;
				q -= m_elemsize;
				ipos--;
				}
				
			if (exists) ((SWVectorElement<T> *)p)->data() = data;
			else new (p) SWVectorElement<T>(data);

			incSize();
			}
		else throw SW_EXCEPTION(SWIndexOutOfBoundsException);
		}


template<class T> void
SWVector<T>::set(const T &data, int pos, T *pOldData)
		{
		SWWriteGuard			guard(this);
		SWVectorElement<T>	*pElement=NULL;
		int						nelems=(int)size();

		if (pos >= 0 && pos < nelems)
			{
			pElement = (SWVectorElement<T> *)((char *)m_data + pos * m_elemsize);

			if (pOldData != NULL) *pOldData = pElement->data();
			pElement->data() = data;
			}
		else throw SW_EXCEPTION(SWIndexOutOfBoundsException);
		}


template<class T> void
SWVector<T>::removeAt(int pos, T *pOldData)
		{
		SWWriteGuard	guard(this);
		int				nelems=(int)size();

		if (pos >= 0 && pos < nelems)
			{
			if (pOldData != NULL) *pOldData = get(pos);
			removeAt(pos, 1);
			}
		else throw SW_EXCEPTION(SWIndexOutOfBoundsException);
		}


template<class T> void
SWVector<T>::removeAt(int pos, int count)
		{
		if (count > 0)
			{
			SWWriteGuard	guard(this);
			int				nelems=(int)size();

			if (pos >= 0 && pos < nelems)
				{
				if (pos + count > nelems) count = nelems - pos;

				if (count > 0)
					{
					int		startpos=pos, endpos=pos+count;
					char	*p = m_data + (startpos * m_elemsize);
					char	*q = m_data + (endpos * m_elemsize);
					int		oldn = (int)size();

					// Reduce the number of elems and shift all the elems down
					for (pos=endpos;pos<(int)size();pos++)
						{
						((SWVectorElement<T> *)p)->data() = ((SWVectorElement<T> *)q)->data();
						p += m_elemsize;
						q += m_elemsize;
						}

					 // Blank out the rest
					pos = nelems - count;
					p = m_data + (pos * m_elemsize);
					while (pos<oldn)
						{
						delete (SWVectorElement<T> *)p;
						p += m_elemsize;
						pos++;
						}

					SWCollection::setSize(nelems - count);
					}
				}
			else throw SW_EXCEPTION(SWIndexOutOfBoundsException);
			}
		}


template<class T> int
SWVector<T>::indexOf(const T &data, int pos) const
		{
		SWReadGuard			guard(this);
		int					nelems=(int)size();
		int					res=-1;

		// Use a linear search
		char	*p = m_data + (pos * m_elemsize);

		while (pos < nelems)
			{
			if (data == ((SWVectorElement<T> *)p)->data())
				{
				res = pos;
				break;
				}

			p += m_elemsize;
			pos++;
			}

		return res;
		}


template<class T> int
SWVector<T>::lastIndexOf(const T &data) const
		{
		return lastIndexOf(data, (int)size()-1);
		}


template<class T> int
SWVector<T>::lastIndexOf(const T &data, int pos) const
		{
		SWReadGuard			guard(this);
		int					nelems=(int)size();

		// Use a linear search
		if (pos > nelems-1) pos = nelems - 1;

		char	*p = m_data + (pos * m_elemsize);

		while (pos >= 0)
			{
			if (data == ((SWVectorElement<T> *)p)->data())
				{
				break;
				}

			pos--;
			p -= m_elemsize;
			}

		return pos;
		}


template<class T> bool
SWVector<T>::remove(const T &data)
		{
		SWWriteGuard	guard(this);
		int				pos=indexOf(data);

		if (pos >= 0) removeAt(pos);

		return (pos >= 0);
		}


template<class T> bool
SWVector<T>::contains(const T &data) const
		{
		return (indexOf(data) >= 0);
		}


template<class T> T &
SWVector<T>::operator[](int i) const				{ return get(i); }


template<class T> void
SWVector<T>::setSize(int newsize)
		{
		setElementCount(newsize);
		}


template<class T> void
SWVector<T>::ensureCapacity(int wanted)
		{
		SWWriteGuard	guard(this);	// Guard this object

		ensureCapacityNoLock(wanted);
		}


template<class T> void
SWVector<T>::ensureCapacityNoLock(int wanted)
		{
		SWWriteGuard	guard(this);	// Guard this object

		if ((sw_size_t)wanted > m_capacity)
			{
			if (m_capacity == 0) m_capacity = 128;

			while (m_capacity < (sw_size_t)wanted)
				{
				m_capacity <<= 1;
				}

			char	*pOldData = m_data;

			m_data = (char *)m_pMemoryManager->malloc(m_capacity * m_elemsize);
			if (m_data == NULL)
				{
				m_data = pOldData;
				throw SW_EXCEPTION(SWMemoryException);
				}

			if (pOldData != NULL)
				{
				// We must "new" all the existing data elements
				char	*pTo=(char *)m_data;
				char	*pFrom=(char *)pOldData;
				int		i, n=(int)size();

				for (i=0;i<n;i++)
					{
					new (pTo) SWVectorElement<T>(((SWVectorElement<T> *)pFrom)->data());
					delete (SWVectorElement<T> *)pFrom;
					pTo += m_elemsize;
					pFrom += m_elemsize;
					}

				m_pMemoryManager->free(pOldData);
				}
			}
		}


// Force the size of the _data array
template<class T> void
SWVector<T>::setElementCount(int n)
		{
		SWWriteGuard	guard(this);	// Guard this object
		int				nelems=(int)size();
		
		if (n > nelems)
			{
			// Grow the array, adding nulls
			ensureCapacityNoLock(n);

			char		*pTo=(char *)m_data + (nelems * m_elemsize);
			int			i;

			for (i=nelems;i<n;i++)
				{
				new (pTo) SWVectorElement<T>;
				pTo += m_elemsize;
				}

			SWCollection::setSize(n);
			}
		else if (n < nelems)
			{
			removeAt(n, nelems-n);
			}
		}


template<class T> SWIterator<T>
SWVector<T>::iterator(bool iterateFromEnd) const
		{
		SWIterator<T>		res;

		initIterator(res, getIterator(iterateFromEnd));

		return res;
		}


template<class T> SWIterator<T>
SWVector<T>::iterator(int startpos) const
		{
		SWIterator<T>		res;

		initIterator(res, getIterator(startpos));

		return res;
		}


template<class T> int
SWVector<T>::compareData(const void *pData1, const void *pData2) const
		{
		int		res=0;

		if (*(T *)pData1 == *(T *)pData2) res = 0;
		else res = 1;

		return res;
		}


template<class T> void
SWVector<T>::quickSort(sw_collection_compareDataFunction *pCompareFunc, int left, int right)
		{
		int		pivot, l_hold, r_hold;
		T		pivotdata;

		l_hold = left;
		r_hold = right;
		pivotdata = ((SWVectorElement<T> *)(m_data + (left * m_elemsize)))->data();
		while (left < right)
			{
			while ((doCompareData(pCompareFunc, &(((SWVectorElement<T> *)(m_data + (right * m_elemsize)))->data()), &pivotdata) >= 0) && (left < right))
				{
				right--;
				}
			if (left != right)
				{
				((SWVectorElement<T> *)(m_data + (left * m_elemsize)))->data() = ((SWVectorElement<T> *)(m_data + (right * m_elemsize)))->data();
				left++;
				}
			while ((doCompareData(pCompareFunc, &(((SWVectorElement<T> *)(m_data + (left * m_elemsize)))->data()), &pivotdata) <= 0) && (left < right))
				{
				left++;
				}
			if (left != right)
				{
				((SWVectorElement<T> *)(m_data + (right * m_elemsize)))->data() = ((SWVectorElement<T> *)(m_data + (left * m_elemsize)))->data();
				right--;
				}
			}
		((SWVectorElement<T> *)(m_data + (left * m_elemsize)))->data() = pivotdata;
		pivot = left;
		left = l_hold;
		right = r_hold;
		if (left < pivot) quickSort(pCompareFunc, left, pivot-1);
		if (right > pivot) quickSort(pCompareFunc, pivot+1, right);
		}

template<class T> void
SWVector<T>::sort(sw_collection_compareDataFunction *pCompareFunc)
		{
		if (size() > 1)
			{
			SWWriteGuard	guard(this);

			quickSort(pCompareFunc, 0, (int)size()-1);
			}
		}


/*************************************************************
** Iterator handling
*************************************************************/

// Define a collection iterator class
template<class T>
class SWVectorIterator : public SWCollectionIterator
		{
		friend class SWCollection;

public:
		SWVectorIterator(SWVector<T> *v, int start);	// Constructor

		virtual bool		hasCurrent();
		virtual bool		hasNext();
		virtual bool		hasPrevious();

		virtual void *		current();
		virtual void *		next();
		virtual void *		previous();

		virtual bool		remove(void *pOldData);
		virtual bool		add(void *data, SWAbstractIterator::AddLocation where);
		virtual bool		set(void *pNewData, void *pOldData);

		SWVector<T> *	collection()	{ return (SWVector<T> *)m_pCollection; }

private:
		int		m_curr;
		int		m_next;
		int		m_prev;
		};


// The main iterator function from the SWVector class
template<class T> SWCollectionIterator *
SWVector<T>::getIterator(bool iterateFromEnd) const
		{
		int	startpos=0;

		if (iterateFromEnd) startpos = (int)size()-1;
		if (startpos < 0) startpos = 0;

		return getIterator(startpos);
		}



// The main iterator function from the SWVector class
template<class T> SWCollectionIterator *
SWVector<T>::getIterator(int startpos) const
		{
		if (startpos < 0 || (startpos >= (int)size() && size() > 0)) throw SW_EXCEPTION(SWIndexOutOfBoundsException);

		return new SWVectorIterator<T>((SWVector<T> *)this, startpos);
		}



template<class T>
SWVectorIterator<T>::SWVectorIterator(SWVector<T> *v, int start) :
    SWCollectionIterator(v)
		{
		m_curr = -1;
		m_next = start;
		m_prev = start - 1;
		}


template<class T> bool
SWVectorIterator<T>::hasCurrent()
		{
		SWGuard	guard(m_pCollection);

		checkForComodification();

		return (m_curr >= 0);
		}


template<class T> bool
SWVectorIterator<T>::hasNext()
		{
		SWGuard	guard(m_pCollection);

		checkForComodification();

		return (m_next < (int)collection()->size());
		}


template<class T> bool
SWVectorIterator<T>::hasPrevious()
		{
		SWGuard	guard(m_pCollection);

		checkForComodification();

		return (m_prev >= 0);
		}


template<class T> void *
SWVectorIterator<T>::current()
		{
		SWGuard	guard(m_pCollection);

		checkForComodification();
		if (m_curr < 0) throw SW_EXCEPTION(SWNoSuchElementException);

		return &(collection()->get(m_curr));
		}


template<class T> void *
SWVectorIterator<T>::next()
		{
		SWGuard	guard(m_pCollection);

		checkForComodification();
		if (m_next >= (int)collection()->size()) throw SW_EXCEPTION(SWNoSuchElementException);

		m_curr = m_next++;
		m_prev = m_curr - 1;

		return &(collection()->get(m_curr));
		}


template<class T> void *
SWVectorIterator<T>::previous()
		{
		SWGuard	guard(m_pCollection);

		checkForComodification();
		if (m_prev < 0) throw SW_EXCEPTION(SWNoSuchElementException);

		m_curr = m_prev--;
		m_next = m_curr + 1;

		return &(collection()->get(m_curr));
		}



template<class T> bool
SWVectorIterator<T>::remove(void *pOldData)
		{
		SWGuard	guard(m_pCollection);

		checkForComodification();
		if (m_curr < 0)  throw SW_EXCEPTION(SWNoSuchElementException);

		if (pOldData != NULL) *(T *)pOldData = collection()->get(m_curr);
		collection()->removeAt(m_curr, 1);

		m_curr = -1;
		m_next--;
		m_modcount = collection()->getModCount();

		return true;
		}


template<class T> bool
SWVectorIterator<T>::set(void *pData, void *pOldData)
		{
		SWGuard	guard(m_pCollection);

		checkForComodification();
		if (m_curr < 0)  throw SW_EXCEPTION(SWNoSuchElementException);

		if (pOldData != NULL) *(T *)pOldData = collection()->get(m_curr);
		collection()->set(*(T *)pData, m_curr);
		m_modcount = collection()->getModCount();

		return true;
		}


template<class T> bool
SWVectorIterator<T>::add(void *data, SWAbstractIterator::AddLocation where)
		{
		SWGuard	guard(m_pCollection);

		checkForComodification();

		if (where == SWAbstractIterator::AddBefore)
			{
			if (m_curr >= 0)
				{
				// Simply add before our current position
				collection()->insert(*(T *)data, m_curr++);
				}
			else
				{
				// We have no current position
				collection()->insert(*(T *)data, m_prev+1);
				}
			m_prev++;
			m_next++;
			}
		else
			{
			if (m_curr >= 0)
				{
				// Simply add after our current position
				collection()->insert(*(T *)data, m_curr+1);
				}
			else
				{
				// We have no current position
				collection()->insert(*(T *)data, m_next);
				}
			}

		m_modcount = collection()->getModCount();

		return true;
		}




#endif
