/**
*** @file		SWObject.cpp
*** @brief		The generic base object from which all library objects are defined
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWObject class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <swift/SWObject.h>




SWObject::SWObject()
		{
		}


SWObject::~SWObject()
		{
		}


#if defined(WIN32) || defined(_WIN32)
	// The windows compiler normally (and rightly so) warns about the cast from
	// pointer to integer. However in this case we know better.
	#pragma warning(disable: 4311)
#endif

// Get the 32-bit hashcode for this object
sw_uint32_t	
SWObject::hash() const
		{
		sw_uint32_t	v=0;

#if SW_SIZEOF_POINTER == 8
		v = (((sw_uint64_t)this) & 0xffffffff);
#else
		v = (sw_uint32_t)this;
#endif

		// The bottom 2 bits are like to be insignificant as we are probabbly
		// aligned on a 4-byte boundary. Hence we do (v >> 2).

		return (sw_uint32_t)((v >> 2) & 0xffffffff);
		}

#if defined(WIN32) || defined(_WIN32)
	#pragma warning(default: 4311)
#endif


sw_status_t
SWObject::lock(sw_uint64_t waitms) const
		{
		return SW_STATUS_SUCCESS;
		}


sw_status_t
SWObject::lockForReading(sw_uint64_t waitms) const
		{
		return SW_STATUS_SUCCESS;
		}


sw_status_t
SWObject::lockForWriting(sw_uint64_t waitms) const
		{
		return SW_STATUS_SUCCESS;
		}


sw_status_t
SWObject::unlock() const
		{
		return SW_STATUS_SUCCESS;
		}


sw_status_t
SWObject::writeToStream(SWOutputStream & /*stream*/) const
		{
		// The default behaviour is to return an error as we cannot perform this operation.
		return SW_STATUS_UNSUPPORTED_OPERATION;
		}


sw_status_t
SWObject::readFromStream(SWInputStream & /*stream*/)
		{
		// The default behaviour is to return an error as we cannot perform this operation.
		return SW_STATUS_UNSUPPORTED_OPERATION;
		}


void
SWObject::clear()
		{
		// The default behaviour is to do nothing
		}



#if defined(SW_BUILD_DEBUG) && defined(SW_DEBUG_MEMORY)
void *
SWObject::operator new(size_t size)
		{
		return sw_memory_addBlock(size, 3, "", 0);
		}


void *
SWObject::operator new(size_t size, const char *file, int line)
		{
		return sw_memory_addBlock(size, 3, file, line);
		}


void *
SWObject::operator new[](size_t size)
		{
		return sw_memory_addBlock(size, 4, "", 0);
		}


void *
SWObject::operator new[](size_t size, const char *file, int line)
		{
		return sw_memory_addBlock(size, 4, file, line);
		}


void	
SWObject::operator delete(void *p)
		{
		sw_memory_removeBlock(p, 3, "", 0);
		}


void
SWObject::operator delete[](void *p)
		{
		sw_memory_removeBlock(p, 4, "", 0);
		}


void
SWObject::operator delete(void *p, const char *file, int line)
		{
		sw_memory_removeBlock(p, 4, file, line);
		}


void
SWObject::operator delete[](void *p, const char *file, int line)
		{
		sw_memory_removeBlock(p, 4, file, line);
		}


#endif // defined(SW_BUILD_DEBUG)



