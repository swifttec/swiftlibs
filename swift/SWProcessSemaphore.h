/**
*** @file		SWProcessSemaphore.h
*** @brief		A process semaphore
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWProcessSemaphore class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWProcessSemaphore_h__
#define __SWProcessSemaphore_h__

#include <swift/SWSemaphore.h>
#include <swift/SWString.h>
#include <swift/SWThreadMutex.h>




/**
*** @brief	Inter-process semaphore.
***
*** This is an inter-process version of a Dijkstra style general semaphore.
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Safe</td>
*** </tr>
*** </table>
***
***
*** @see		Semaphore
*** @author		Simon Sparkes
*/
class SWIFT_DLL_EXPORT SWProcessSemaphore : public SWSemaphore
		{
public:
		/// Constructor - using a name
		SWProcessSemaphore(const SWString &name, int count=1, int maxcount=0x7fffffff);

		/// Virtual destructor
		virtual ~SWProcessSemaphore();

		/// Lock/Acquire this mutex. The same thread can acquire the mutex more than once.
		virtual sw_status_t	acquire(sw_uint64_t waitms=SW_TIMEOUT_INFINITE);

		/// Unlock/Release this mutex. If the mutex has been recursively acquired the mutex
		/// will only be unlocked when the recursive lock count reaches zero.
		virtual sw_status_t	release();

private:
		SWThreadMutex	m_tguard;		///< A thread guard
		sw_handle_t		m_hSemaphore;	///< The actual process mutex
		};




#endif
