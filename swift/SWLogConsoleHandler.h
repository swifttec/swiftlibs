/**
*** @file		SWLogConsoleHandler.h
*** @brief		A class write messages to a FILE.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWLogConsoleHandler class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWLogConsoleHandler_h__
#define __SWLogConsoleHandler_h__

#include <SWLogMessage.h>
#include <SWLogMessageHandler.h>

#include <SWString.h>

#include <SWConsole.h>


// Begin the namespace
SWIFT_NAMESPACE_BEGIN


/**
*** @brief		A class write messages to a SWConsole object.
***
*** This class handles messages from a SWLog object and writes them to a SWConsole
*** object. This adds colour to log messages on platforms that support it.
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Unsafe</td>
*** </tr>
*** </table>
***
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWLogConsoleHandler : public SWLogMessageHandler
		{
public:
		/**
		*** @brief	Constuct a log message handler that writes messages to the console/tty.
		**/
		SWLogConsoleHandler();

		/// Virtual destructor
		virtual ~SWLogConsoleHandler();

public: // Virtual method overrides
		virtual sw_status_t	open();
		virtual sw_status_t	process(const SWLogMessage &lm);
		virtual sw_status_t	flush();
		virtual sw_status_t	close();

private:
		SWConsole	m_console;			///< The file name
		};

// End the namespace
SWIFT_NAMESPACE_END

#endif
