/**
*** @file		SWTokenizer.h
*** @brief		A string tokenizer class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWTokenizer class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWTokenizer_h__
#define __SWTokenizer_h__



#include <swift/SWObject.h>
#include <swift/SWString.h>
#include <swift/SWException.h>
#include <swift/SWArray.h>

#if defined(SW_PLATFORM_WINDOWS)
	#pragma warning(disable: 4251)
#endif


/**
*** @brief	A powerful string tokenizer class
***
*** The Tokenizer class is a powerful and flexible string tokenizer class. It will
*** tokenize strings using any delimiters specified and allows the use of symmetric and
*** asymmetric quote pairs. It also supports meta chars (e.g. \) which allows prefixed
*** quotes to be in strings.
*** 
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Unsafe</td>
*** </tr>
*** </table>
***
***
*** @author	Simon Sparkes
***/
class SWIFT_DLL_EXPORT SWTokenizer : public SWObject
		{
public:
		/**
		*** @brief		Construct a SWTokenizer object according to the given parameters.
		***
		*** The SWTokenizer can be used any set of characters as delimiter characters 
		*** by specifying the <i>sepchars</i> parameter.
		***
		*** The <i>skipdups</i> parameter tells the tokenizer whether or not to ignore 
		*** two or more delimiters or not. If true (default) then a string followed by any
		*** number of delimiters followed by another string will return only two tokens.
		*** If <i>skipdups</i> is false then one token is returned for each delimiter
		*** found, with empty strings being returned where duplicate delimiters were found.
		***
		*** Example:
		***
		*** <b>Delimiter:</b> ':'<br>
		*** <b>String:</b> A::B:C:<br>
		*** <b>skipdups=true:</b> [A] [B] [C]<br>
		*** <b>skipdups=false:</b> [A] [] [B] [C] []<br>
		*** 
		*** If stdquotes is true	the standard quote pairs "..." and '...' are added.
		***
		*** 
		*** @param[in] s			The initial string to tokenize. [default: empty string]
		*** @param[in] sepchars		A string containing the chars that are considered to be token delimiters. [default: " \\t\\n"]
		*** @param[in] skipdups		A bool defining the action to be taken when 2 delimiter characters are found. [default: true]
		*** @param[in] stdquotes	A bool defining if the standard quote pairs "..." and '...' are to be added. [default: false]
		*** @param[in] meta			The character which is used to prefix quotes and separators to allow them to be ignored.
		**/
		SWTokenizer(const SWString &s=_T(""), const SWString &sepchars=_T(" \t\n"), bool skipdups=true, bool stdquotes=false, int meta=0);

		/// Virtual destructor
		virtual ~SWTokenizer();

		/**
		*** @brief	Test to see if there are more tokens available
		***
		*** This method tests to see if there are more tokens available in the
		*** current set. If tokenization has not yet taken place it will occur
		*** when this method is called.
		***
		*** The actual token can be retrieved by calling the nextToken method.
		***
		*** @return true if there are more tokens available, false otherwise
		**/
		bool				hasMoreTokens();

		/**
		*** @brief Get the next token from the token set.
		***
		*** This variation on the nextToken method checks to see if another
		*** token is available in the token set. If it is the next token
		*** is placed into the str parameter and a true value is returned
		*** to the caller.
		***
		*** If there are no more tokens available the str parameter is unchanged
		*** and a false value is returned to the caller.
		***
		*** @param[out]	 str	If a token is available this receives the next token.
		***
		*** @return true if there was a token available, false otherwise (str is unchanged)
		**/
		bool				nextToken(SWString &str);

		/**
		*** @brief	Get the next token from the token set.
		***
		*** If there are more tokens available in the token set the next token
		*** is retrieved and returned to the caller.  If there are no more tokens
		*** available an exception is thrown.
		***
		*** @throws	SWTokenizerException	Thrown if no more tokens are available.
		***
		*** @return	The next token in the token set.
		**/
		SWString			nextToken();				// Return the next token or null if no more

		/**
		*** @brief 	Get the nth token
		***
		*** This method allows the caller to retrieve a specific token via a number rather than having to
		*** step through the tokens to get to the required one. If the specified index is out of range a
		*** SWIndexOutOfBoundsException is thrown.
		***
		*** @param[in] n	A zero based index specifiying the token required.
		***
		*** @return			the nth token as a string.
		***
		*** @throws		SWIndexOutOfBoundsException if n is less than zero or greater than the number of available tokens
		**/
		SWString			getToken(int n);			// Return the nth token

		/**
		*** @brief	Reset token retrieval
		***
		*** This method resets the tokenizer to start returning tokens from the beginning
		*** of the internal token array, but does not force a re-tokenization of the
		*** current string. This can be used when the current set of tokens need to
		*** be re-read from the beginning using the nextToken() method or the () operator
		**/
		void				reset();				// Reset the current pointer to the beginning

		/**
		*** @brief	clear the internal token array
		***
		*** This method clears the internal token array and forces the
		*** the tokenizer to re-tokenize the current string on the next call
		*** that requests a token or token count.
		**/
		void				clear();

		/**
		*** @brief	clear the internal quotes array.
		***
		*** This method clears the internal quotes array and forces the
		*** the tokenizer to re-tokenize the current string on the next call
		*** that requests a token or token count.
		**/
		void				clearQuotes();				// Clear the quote vector

		/**
		*** @brief 	Add a pair of delimiting quotes
		***
		*** This method adds a pair of delimiting quotes to the internal
		*** quote array. The quote pairs are used allow quoted strings
		*** that contain delimiters to be ignored.
		***
		*** E.g. If a string to be tokenized using spaces as the delimiter
		*** is specified, but anything contained within double-quotes should
		*** be taken as a single token then using addQuotes('"', '"') will make
		*** the tokenizer work correctly.
		***
		*** The string: <b>This "is a" test</b><br>
		*** would give the following tokens:<br>
		*** No quotes: [This] ["is] [a"] [test]<br>
		*** With quotes: [This] ["is a"] [test]
		***
		*** @param[in] qStart	The quote character marking the start of a token
		*** @param[in] qEnd		The quote character marking the end of a token
		**/
		void				addQuotes(int qStart, int qEnd);

		/**
		*** @brief	Return the number of tokens available.
		***
		*** This method will retrieve the number of tokens available
		*** in the internal token set. If the tokenization has not taken place
		*** or tokenization need to be re-done becuase parameters have changed 
		*** this method will cause tokenization to take place before returning
		*** the internal token count.
		***
		*** @return		The numebr of tokens in the internal token set.
		**/
		int					getTokenCount();

		/**
		*** @brief Set a new string for tokenizing
		***
		*** The setString method allows a new string to be tokenized without
		*** the overhead of destructing and constructing a SWTokenizer object.
		*** This method will not cause immediate tokenization, but will set a
		*** flag indicating the tokenization is required before tokens can be
		*** retrieved.
		***
		*** @param[in] str	String to be tokenized
		**/
		void				setString(const SWString &str);

		/// Returns the next token
		SWString			operator()()				{ SWString tmp; nextToken(tmp); return tmp; }

		/**
		*** @brief	Get the next token if available.
		***
		*** This method attempts to get the next token (if one is available) and store
		*** the result in the specified string.
		***
		*** @param[out]	str		A string that receives the next string token if available.
		***
		*** @return		true if a token was available, false otherwise.
		**/
		bool				operator()(SWString &str)	{ return nextToken(str); }

		/// Sets the meta char
		void				setMetaChar(int m)		{ _meta = m; _doit = true; }

		/// Gets the meta char
		int					getMetaChar() const		{ return _meta; }

		/// Sets the skip flag (if true multiple delimiters are ignored)
		void				setSkipFlag(bool v)		{ _skip = v; _doit = true; }

		/// Gets the skip flag
		bool				getSkipFlag() const		{ return _skip; }

		/// Sets the delimiter chars
		void				setDelimiters(const SWString &str);

		/// Returns the current delimiters
		const SWString &	getDelimiters() const		{ return _chars; }

		/**
		*** @brief	Returns the nth token
		***
		*** This method attempts to directly access the nth token and return
		*** it to the caller. If the specified index is out of bounds an
		*** exception is thrown.
		***
		*** @param[in]	n	The zero-based index of the token to retrieve.
		*** @return			The token
		*** @exception		IndexOutOfBoundsException if n is less than zero or greater than the number of available tokens
		***
		*** @see getToken
		**/
		SWString	operator[](int n)	{ return getToken(n); }

private:
		class QuotePair
				{
		public:
				QuotePair(const QuotePair &other)				{ *this = other; }
				QuotePair(int s=0, int e=0) : start(s), end(e)	{ }
				virtual ~QuotePair()							{ }
				QuotePair &	operator=(const QuotePair &other)	{ start = other.start; end = other.end; return *this; }

				int	start;
				int	end;
				};


private:
		/**
		*** @brief	Internal method that does the actual string tokenizing.
		***
		*** This private method does the hard work of splitting up the string into tokens
		*** and putting them into the internal token array, Once finished the _doit flag
		*** is set to false to indicate that tokenization is up-to-date.
		**/
		void		tokenize();

private:
		SWArray<SWString>	_tokenVec;	///< A vector to hold the tokens
		SWArray<QuotePair>	_quoteVec;	///< A vector to hold the quote pairs
		int					_meta;		///< The metachar which prefixes special chars
		SWString			_string;	///< The string to be tokenized
		SWString			_chars;		///< The chars regarded as separators for this instance
		bool				_skip;		///< If true skip multiple separators
		bool				_doit;		///< If true we need to tokenize _string
		int					_curr;		///< The current position
		};


/// A general tokenizer exception
SW_DEFINE_EXCEPTION(SWTokenizerException, SWException);

#endif
