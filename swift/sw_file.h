/****************************************************************************
**	sw_file.h		Various file utilities
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#ifndef __sw_file_h__
#define __sw_file_h__


#include <swift/SWString.h>
#include <swift/SWStringArray.h>
#include <swift/SWTime.h>
#include <swift/SWFileInfo.h>
#include <swift/SWFilename.h>
#include <swift/SWFolder.h>


// Visual C++ does some strange things with struct stat related functions!

#if (defined(WIN32) || defined(_WIN32)) && (defined(UNICODE) || defined(_UNICODE))
typedef	struct _stat	sw_stat_t;

#else
typedef	struct stat		sw_stat_t;

#endif




/*************************************************************
** Various Local File Operations
*************************************************************/
SWIFT_DLL_EXPORT sw_status_t	sw_file_mkdir(const SWString &path, bool createParent=false, SWFileAttributes *pfa=0);

/*************************************************************
** Various Local File Tests
*************************************************************/
SWIFT_DLL_EXPORT bool			sw_file_isFile(const SWString &path);
SWIFT_DLL_EXPORT bool			sw_file_isDirectory(const SWString &path);
SWIFT_DLL_EXPORT bool			sw_file_isSymlink(const SWString &path);
SWIFT_DLL_EXPORT bool			sw_file_isReadable(const SWString &path);
SWIFT_DLL_EXPORT bool			sw_file_isWriteable(const SWString &path);
SWIFT_DLL_EXPORT bool			sw_file_isExecutable(const SWString &path);
SWIFT_DLL_EXPORT SWTime			sw_file_time(const SWString &filename);
SWIFT_DLL_EXPORT sw_filesize_t	sw_file_size(const SWString &filename);
SWIFT_DLL_EXPORT sw_status_t	sw_file_getFileInfo(const SWString &filename, SWFileInfo &fileinfo);


/*************************************************************
** Filename Maniuplation
*************************************************************/

#define SW_FILE_PLATFORM_PATH	SWFilename::PathTypePlatform
#define SW_FILE_UNIX_PATH		SWFilename::PathTypeUnix
#define SW_FILE_DOS_PATH		SWFilename::PathTypeWindows
typedef SWFilename::PathType	sw_file_pathtype;


SWIFT_DLL_EXPORT bool		sw_file_isRelativePath(const SWString &path);
SWIFT_DLL_EXPORT bool		sw_file_isAbsolutePath(const SWString &path);

SWIFT_DLL_EXPORT SWString	sw_file_extension(const SWString &path);
SWIFT_DLL_EXPORT SWString	sw_file_basename(const SWString &path);
SWIFT_DLL_EXPORT SWString	sw_file_dirname(const SWString &path);

//SWIFT_DLL_EXPORT SWString	sw_file_servername(const SWString &path);
//SWIFT_DLL_EXPORT SWString	sw_file_drivename(const SWString &path);
//SWIFT_DLL_EXPORT SWString	sw_file_sharename(const SWString &path);
SWIFT_DLL_EXPORT SWString	sw_file_fixpath(const SWString &path);



// Split a pathname into its component parts
//SWIFT_DLL_EXPORT void			sw_file_splitRemotePath(const SWString &name, SWString &server, SWString &share, SWString &path);

// Split a filename into base and extension
SWIFT_DLL_EXPORT void			sw_file_splitFilename(const SWString &file, SWString &base, SWString &extension);

// Create a full path name for the given server/file combination
//SWIFT_DLL_EXPORT SWString		sw_file_createRemoteUnixPath(const SWString &server, const SWString &path);

// Create a full path name for the given server/file combination
//SWIFT_DLL_EXPORT SWString		sw_file_createRemoteWindowsPath(const SWString &server, const SWString &path);

#if defined(WIN32) || defined(_WIN32)
#define sw_file_createRemotePath	sw_file_createRemoteWindowsPath
#else
#define sw_file_createRemotePath	sw_file_createRemoteUnixPath
#endif

// Add 2 paths together
SWIFT_DLL_EXPORT SWString		sw_file_concatPaths(const SWString &path, const SWString &path2, SWFilename::PathType type=SWFilename::PathTypeNoChange);


// Get/Set file times
SWIFT_DLL_EXPORT sw_status_t	sw_file_getTimes(const SWString &path, SWTime *lastModTime, SWTime *createTime=NULL, SWTime *lastAccessTime=NULL);
SWIFT_DLL_EXPORT sw_status_t	sw_file_setTimes(const SWString &path, SWTime *lastModTime, SWTime *createTime=NULL, SWTime *lastAccessTime=NULL);

// Get a temp file name or directory
SWIFT_DLL_EXPORT SWString		sw_file_getTempDirectory(const SWString &dir="");
SWIFT_DLL_EXPORT SWString		sw_file_getTempFile(const SWString &prefix=_T("tmp"), const SWString &ext=_T(".tmp"), const SWString &dir="");

// Convert a Unix to a Dos filename (/ -> \)
SWIFT_DLL_EXPORT SWString		sw_file_unixToDosName(const SWString &filename);

// Convert a Dos to a Unix filename (\ -> /)
SWIFT_DLL_EXPORT SWString		sw_file_dosToUnixName(const SWString &filename);

// Perform "stat" on a server file
//SWIFT_DLL_EXPORT int			sw_file_statServer(const SWString &server, const SWString &path, struct _stat *sp);

// Check the server for a file/directory
//SWIFT_DLL_EXPORT bool			sw_file_isServerDirectory(const SWString &server, const SWString &path);
//SWIFT_DLL_EXPORT bool			sw_file_isServerFile(const SWString &server, const SWString &path);

// Get the system root for the specified server
//SWIFT_DLL_EXPORT SWString		sw_file_getServerSystemRoot(const SWString &server);

// Get the system drive for the specified server
//SWIFT_DLL_EXPORT TCHAR		sw_file_getServerSystemDrive(const SWString &server);

// Attempt to map the share on the given server to it's full (local) path
// SWIFT_DLL_EXPORT SWString		sw_file_getServerSharePath(const SWString &server, const SWString &share);

// Directory handling
SWIFT_DLL_EXPORT SWString		sw_file_getCurrentDirectory();
SWIFT_DLL_EXPORT sw_status_t	sw_file_setCurrentDirectory(const SWString &dir);
SWIFT_DLL_EXPORT sw_status_t	sw_file_pushCurrentDirectory(const SWString &dir);
SWIFT_DLL_EXPORT sw_status_t	sw_file_popCurrentDirectory();

// Path handling
SWIFT_DLL_EXPORT SWString		sw_file_absolutePath(const SWString &path, const SWString &todir=sw_file_getCurrentDirectory(), sw_file_pathtype type=SW_FILE_PLATFORM_PATH);
SWIFT_DLL_EXPORT SWString		sw_file_relativePath(const SWString &path, const SWString &todir=_T("."), sw_file_pathtype type=SW_FILE_PLATFORM_PATH);
SWIFT_DLL_EXPORT sw_size_t		sw_file_splitPath(const SWString &path, SWStringArray &sa);
SWIFT_DLL_EXPORT SWString		sw_file_joinPath(SWStringArray &sa, sw_file_pathtype type=SW_FILE_PLATFORM_PATH);
SWIFT_DLL_EXPORT SWString		sw_file_optimizePath(const SWString &path, sw_file_pathtype type=SW_FILE_PLATFORM_PATH);

// Path searching
SWIFT_DLL_EXPORT bool			sw_file_isProgramInPath(const SWString &prog);
SWIFT_DLL_EXPORT SWString		sw_file_pathForProgram(const SWString &prog);
SWIFT_DLL_EXPORT bool			sw_file_isProgramInDirectory(const SWString &prog, const SWString &dir, SWString &filename);

// File access
SWIFT_DLL_EXPORT void			sw_file_grantEverybodyAccessToFile(const SWString &FileName);

// File copy
SWIFT_DLL_EXPORT sw_status_t		sw_file_copyFile(const SWString &fromfile, const SWString &tofile);

/// Get the size of a complete folder
SWIFT_DLL_EXPORT sw_filesize_t		sw_file_getFolderSize(const SWString &path);

/// Get the amount of free space on the device containing the given size
SWIFT_DLL_EXPORT sw_filesize_t		sw_filesystem_getFreeSpace(const SWString &path);

/// Get the amount of space available to the caller on the device containing the given size
SWIFT_DLL_EXPORT sw_filesize_t		sw_filesystem_getAvailableSpace(const SWString &path);

#endif
