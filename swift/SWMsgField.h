/****************************************************************************
**	SWMsgField.h	A class for holding a single field of a SWMsg message
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWMsgField_h__
#define __SWMsgField_h__

#include <swift/SWValue.h>





// Forward declarations

/**
*** A class for holding a single field of a SWMsg message. The field
*** is held in a format designed to save space when transmitting the 
*** message over a network.
***
*** @brief	A class for holding a single field of a SWMsg message
**/
class SWIFT_DLL_EXPORT SWMsgField : public SWValue
		{
public:

		/// Default constructor - constructs an empty value of type VtNone
		SWMsgField();

		/// Virtual destrcutor
		virtual ~SWMsgField();

		/// Copy constructor
		SWMsgField(const SWMsgField &v);

        // Assignment operator
        SWMsgField &    operator=(const SWMsgField &mf);

		SWMsgField(sw_uint32_t id, bool v);				///< Constructs a value with the type VtNone
		SWMsgField(sw_uint32_t id, sw_int8_t v);			///< Constructs a value with the type VtInt8
		SWMsgField(sw_uint32_t id, sw_int16_t v);			///< Constructs a value with the type VtInt16
		SWMsgField(sw_uint32_t id, sw_int32_t v);			///< Constructs a value with the type VtInt16
		SWMsgField(sw_uint32_t id, sw_int64_t v);			///< Constructs a value with the type VtInt16
		SWMsgField(sw_uint32_t id, sw_uint8_t v);			///< Constructs a value with the type VtUInt8
		SWMsgField(sw_uint32_t id, sw_uint16_t v);			///< Constructs a value with the type VtUInt8
		SWMsgField(sw_uint32_t id, sw_uint32_t v);			///< Constructs a value with the type VtUInt8
		SWMsgField(sw_uint32_t id, sw_uint64_t v);			///< Constructs a value with the type VtUInt8
		SWMsgField(sw_uint32_t id, float v);				///< Constructs a value with the type VtFloat
		SWMsgField(sw_uint32_t id, double v);				///< Constructs a value with the type VtDouble
		SWMsgField(sw_uint32_t id, const char *v);			///< Constructs a value with the type VtString
		SWMsgField(sw_uint32_t id, const wchar_t *v);		///< Constructs a value with the type VtWString
		SWMsgField(sw_uint32_t id, const void *, size_t len);	///< Constructs a value with the type VtData from the data pointer and size
		SWMsgField(sw_uint32_t id, const SWBuffer &v);			///< Constructs a value with the type VtData from SWBuffer
		SWMsgField(sw_uint32_t id, const SWString &v);		///< Constructs a value with a string type of VtString or VtWString depending on the SWString
		SWMsgField(sw_uint32_t id, const SWDate &v);		///< Constructs a value with the type VtDate
		SWMsgField(sw_uint32_t id, const SWTime &v);		///< Constructs a value with the type VtTime
		SWMsgField(sw_uint32_t id, const SWMoney &v);		///< Constructs a value with the type VtMoney
		SWMsgField(sw_uint32_t id, const SWValue &v);

#if defined(SW_BUILD_MFC)
		SWMsgField(sw_uint32_t id, const COleVariant &v);
		SWMsgField(sw_uint32_t id, const CString &v);
#endif

		/// Clear/Reset field to NONE
		virtual void	clear();

		/// Sets the id of this field
		void			clearId();

		/// Get the id of this field
		sw_uint32_t		id() const			{ return m_id; }
		
		/// Set the id of this field
		void			id(sw_uint32_t id);


		// to/from memory buffer
		sw_status_t		readFromBuffer(char *mem);
		sw_status_t		writeToBuffer(SWBuffer &buf, sw_size_t offset=0) const;

		// Debug methods
		virtual void		dump();

public: // Virtual overrides
		virtual sw_status_t		readFromStream(SWInputStream &s);
		virtual sw_status_t		writeToStream(SWOutputStream &s) const;

protected:
		virtual bool		clearValue(bool updateChanged);

		/// Returns the length of this field when written to memory (not the same as the data length)
		sw_size_t		fieldLength() const;
		sw_size_t		fieldLengthSize() const;

protected:
		sw_uint32_t		m_id;			// The numeric ID of this field
		};





#endif
