/**
*** @file		SWOutputStream.h
*** @brief		Base object for all non-primary objects in the SWIFT.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWOutputStream class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#ifndef __SWOutputStream_h__
#define __SWOutputStream_h__

#include <swift/SWObject.h>
#include <swift/SWStringReference.h>





// Forward declarations

/**
*** @brief	Generic output stream.
***
*** This object forms the base for general output streams without dictating the
*** format in which the data is written to the stream.
***
*** The generic output stream provides methods to the general caller to write
*** all the basic data types (integers, booleans, floating point, strings and
*** binary data). These are passed to a number of methods (e.g. writeUnsignedInteger)
*** which classes that implement this stream interface are expected to override
*** as necessary.
***
*** @par Creating an output stream object
*** In order to implement an output stream object a developer should derive
*** their stream object from the SWOutputStream object. If the output stream
*** is in a binary format then only the writeData method needs to be
*** implemented as the other methods all call writeData. However for
*** non-binary streams (e.g. text-based or XML-based) all the methods should
*** be overridden.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWOutputStream : public SWObject
		{
friend class SWString;
friend class SWAbstractBuffer;
friend class SWBuffer;
friend class SWDataBuffer;
friend class SWValue;

public:
		/**
		*** @brief		Write a boolean value to the output stream
		*** @param	v	The value to write to the stream.
		**/
		sw_status_t		write(bool v)							{ return writeBoolean(v); }

		/**
		*** @brief		Write an 8-bit signed integer value to the output stream
		*** @param	v	The value to write to the stream.
		**/
		sw_status_t		write(sw_int8_t v)						{ return writeSignedInteger(v, sizeof(v)); }

		/**
		*** @brief		Write a 16-bit signed integer value to the output stream
		*** @param	v	The value to write to the stream.
		**/
		sw_status_t		write(sw_int16_t v)						{ return writeSignedInteger(v, sizeof(v)); }

		/**
		*** @brief		Write a 32-bit signed integer value to the output stream
		*** @param	v	The value to write to the stream.
		**/
		sw_status_t		write(sw_int32_t v)						{ return writeSignedInteger(v, sizeof(v)); }

		/**
		*** @brief		Write a 64-bit signed integer value to the output stream
		*** @param	v	The value to write to the stream.
		**/
		sw_status_t		write(sw_int64_t v)						{ return writeSignedInteger(v, sizeof(v)); }

		/**
		*** @brief		Write an 8-bit unsigned integer value to the output stream
		*** @param	v	The value to write to the stream.
		**/
		sw_status_t		write(sw_uint8_t v)						{ return writeUnsignedInteger(v, sizeof(v)); }

		/**
		*** @brief		Write a 16-bit unsigned integer value to the output stream
		*** @param	v	The value to write to the stream.
		**/
		sw_status_t		write(sw_uint16_t v)					{ return writeUnsignedInteger(v, sizeof(v)); }

		/**
		*** @brief		Write a 32-bit unsigned integer value to the output stream
		*** @param	v	The value to write to the stream.
		**/
		sw_status_t		write(sw_uint32_t v)					{ return writeUnsignedInteger(v, sizeof(v)); }

		/**
		*** @brief		Write a 64-bit unsigned integer value to the output stream
		*** @param	v	The value to write to the stream.
		**/
		sw_status_t		write(sw_uint64_t v)					{ return writeUnsignedInteger(v, sizeof(v)); }

		/**
		*** @brief		Write a float value to the output stream
		*** @param	v	The value to write to the stream.
		**/
		sw_status_t		write(float v)							{ return writeFloat(v); }

		/**
		*** @brief		Write a double value to the output stream
		*** @param	v	The value to write to the stream.
		**/
		sw_status_t		write(double v)							{ return writeDouble(v); }

		/**
		*** @brief		Write a single-byte character string to the output stream
		*** @param	v	The value to write to the stream.
		**/
		sw_status_t		write(const char *v)				{ return write(SWStringReference(v)); }

		/**
		*** @brief		Write a wide character string to the output stream
		*** @param	v	The value to write to the stream.
		**/
		sw_status_t		write(const wchar_t *v)				{ return write(SWStringReference(v)); }

		/**
		*** @brief			Write binary data to the output stream
		*** @param pData	A pointer to the data to write to the stream
		*** @param len		The length of the data (in bytes) to write to the stream.
		**/
		sw_status_t		write(const void *pData, sw_size_t len)	{ return writeBinaryData(pData, len); }


		/**
		*** @brief		Write object to the output stream
		*** @param	v	The value to write to the stream.
		**/
		sw_status_t		write(const SWObject &v)				{ return v.writeToStream(*this); }


		/**
		*** @brief		Flush any data that has been cached.
		**/
		sw_status_t		flush()									{ return flushData(); }


		/**
		*** @brief		Rewind the stream to it's beginning point
		**/
		sw_status_t		rewind()								{ return rewindStream(); }

		/**
		*** @brief			Write raw binary data to the output stream (no encoded data length)
		*** @param pData	A pointer to the data to write to the stream
		*** @param len		The length of the data (in bytes) to write to the stream.
		**/
		sw_status_t		writeRawData(const void *pData, sw_size_t len);

protected: // Deriving classes are expected to override these functions
		virtual sw_status_t		writeSignedInteger(sw_int64_t v, int nbytes);			///< Write a signed number to the output stream.
		virtual sw_status_t		writeUnsignedInteger(sw_uint64_t v, int nbytes);		///< Write an unsigned number to the output stream.
		virtual sw_status_t		writeDouble(double v);									///< Write a double to the output stream.
		virtual sw_status_t		writeFloat(float v);									///< Write a float to the output stream.
		virtual sw_status_t		writeBinaryData(const void *pData, sw_size_t len);	///< Write binary data to the output stream.
		virtual sw_status_t		writeBoolean(bool v);									///< Write a boolean value to the output stream.

		virtual sw_status_t		writeData(const void *pData, sw_size_t len);			///< Write data to the output stream.
		virtual sw_status_t		flushData();											///< Flush any cached data to the output stream.
		virtual sw_status_t		rewindStream();											///< Rewind the stream to restart data output.

protected:
		/// Convert the double to a 64-bit integer representation.
		static sw_int64_t	doubleToLongBits(double d);

		/// Convert the float to a 32-bit integer representation.
		static sw_int32_t	floatToIntBits(float d);
		};




#endif
