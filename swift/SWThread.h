/**
*** @file		SWThread.h
*** @brief		A class to encapsulate a thread
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWThread class.
***
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWThread_h__
#define __SWThread_h__


#include <swift/swift.h>
#include <swift/SWLockableObject.h>
#include <swift/SWException.h>

#include <xplatform/sw_thread.h>




// Forward declarations
class SWThread;

/// The thread function definition. Used when starting traditional style threads
typedef int (*SWThreadObjProc) (SWThread *, void *);


/**
*** @brief	Generic thread object.
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWThread : public SWLockableObject
		{
public:
		/// An enumeration of notification codes
		enum NotificationCode
			{
			ThreadStarted,	///< The Thread has started
			ThreadStopped	///< The Thread has stopped
			};

		/// The thread notification function specification used to provide thread status feedback
		typedef void (*NotificationProc) (SWThread *, NotificationCode);

protected:
		/**
		*** @brief	Protected constructor for derived threads.
		***
		*** @param[in]	createflags		Flags that specify how the type of thread to
		***								to be started. Possible values are:
		***								- SW_THREAD_TYPE_NORMAL - Specify a normal thread
		***								- SW_THREAD_TYPE_DETACHED - Specify a detached thread
		***								- SW_THREAD_TYPE_DAEMON - Specify a daemon thread
		***								- SW_THREAD_TYPE_UNBOUND - Thread is unbound to a LWP (may be ignored by O/S)
		***								- SW_THREAD_TYPE_BOUND - Thread is bound to a LWP (may be ignored by O/S)
		**/
		SWThread(sw_uint32_t createflags=SW_THREAD_TYPE_NORMAL);

public:
		/**
		*** @brief	Constructor for traditional style thread using a function.
		***
		*** This method constructs a SWThread object using the specified function
		*** as the main thread code. The function can be either a function or a static
		*** method of a class.
		***
		*** @param[in]	func			The function to call when the thread starts
		*** @param[in]	param			The parameter to pass to the function on thread start.
		*** @param[in]	createflags		Flags that specify how the type of thread to
		***								to be started. Possible values are:
		***								- SW_THREAD_TYPE_NORMAL - Specify a normal thread
		***								- SW_THREAD_TYPE_DETACHED - Specify a detached thread
		***								- SW_THREAD_TYPE_DAEMON - Specify a daemon thread
		***								- SW_THREAD_TYPE_UNBOUND - Thread is unbound to a LWP (may be ignored by O/S)
		***								- SW_THREAD_TYPE_BOUND - Thread is bound to a LWP (may be ignored by O/S)
		**/
		SWThread(SWThreadObjProc func, void *param, sw_uint32_t createflags=0);

		/// Destructor
		virtual ~SWThread();

		/// Start the thread running.
		void				start();

		/// Stop the thread running.
		virtual void		stop(bool waitForStop=false);

		/// Get the exit status of the thread.
		int					getExitCode();

		/// Get the thread ID for this thread.
		sw_thread_id_t			getThreadId() const;

		/**
		*** @brief	Test to see if the thread is runnable.
		***
		*** Thread functions should call this method periodically to see
		*** if another thread has requested that it stop.
		**/
		bool				runnable() const;

		/// Test to see if the thread has stopped running.
		bool				stopped() const;

		/// Test to see if the thread has started running.
		bool				started() const;

		/// Test to see if the thread is running.
		bool				running() const;

		/// Suspend execution of this thread
		sw_status_t			suspend();

		/// Resume execution of this thread
		sw_status_t			resume();

		/// Set the priority of the thread
		sw_status_t			setPriority(int priority);

		/// Get the priority of the thread
		int					getPriority();

		/// Wait for this thread to stop executing
		sw_status_t			waitUntilStopped(int pollMs=250);

		/// Wait for this thread to start executing
		sw_status_t			waitUntilStarted(int pollMs=250);

		/**
		*** @brief	Sleep for the specified number of milliseconds or until not runnable.
		***
		*** This method will sit in a loop sleeping a short amount of time (as determined by maxsleepms)
		*** until the specified number of milliseconds have passed. If the thread ceases to be runnable
		*** this method will return false, otherwise it returns true.
		**/
		bool				waitFor(int ms, int maxsleepms=100);

public: // STATIC functions

		
		/**
		*** @brief	Temporarily suspend thread execution.
		***
		***	This method suspends the current thread for the specified number
		*** of milliseconds.
		***
		*** @param[in]	ms	The number of milliseconds to sleep.
		**/
		static void			sleep(sw_uint32_t ms);

		/// Return the ID of the currently running thread.
		static sw_thread_id_t	self();

		/// Causes the current thread to yield to anther thread
		static void			yield();

		/**
		*** @brief	Set the thread concurrency value
		***
		*** This method calls the underlying O/S to set the number of threads
		*** that can run concurrently withing this process. This can be used to
		*** allow the runnable threads in a process to share (or compete for)
		*** execution time concurrently. This is implemented by the underlying
		*** O/S and may be ignored on some systems.
		***
		*** @param[in]	v	The number of thread to allow to run concurrently.
		***
		*** @return			SW_STATUS_SUCCESS if successful, or an error code otherwise.
		**/
		static sw_status_t	setConcurrency(int v);

		/// Get the thread concurrency value
		static int			getConcurrency();


protected:	// Virtual Overrides

		/**
		***	@brief	The run method used by derived classes.
		***
		*** The run method should be overridden by anyone deriving a thread class
		*** from SWThread. The derived method should periodically check the run status
		*** by calling the runnable() method and should exit the run method if runnable()
		*** returns false. This mechanism is used to help ensure that all thread resources
		*** are properly returned to the system (e.g. memory, file handles, etc).
		***
		*** The thread should return an integer which will be used as the exit code for
		*** the thread which any other thread can check after the thread has exited.
		***
		*** @return		The thread exit code.
		**/
		virtual int		run();

		/// Set internal flags
		void			setFlags(sw_uint32_t flags);

		/// Clear internal flags
		void			clearFlags(sw_uint32_t flags);
		
		/// test internal flags
		sw_uint32_t		getFlags(sw_uint32_t flags) const;
		
		/**
		*** Flag the thread to indictate that it should stop.
		***
		*** This will cause the runnable method to return false.
		**/
		void				setStopFlag();

public:
		/**
		*** @internal
		*** @brief	Internal run method.
		***
		*** This function is NOT for general use but needs to be public so it is accessible
		*** from an internal function.
		***
		*** @param[in]	data	Receives a pointer to the thread object that started the thread.
		***
		*** @return		The exit code for the thread.
		**/
		static int		irun(void *data);

private:
		sw_thread_t		m_hThread;		///< The handle to the thread.
		sw_uint32_t		m_flags;		///< The flags used for thread creation and monitoring
		sw_thread_id_t	m_tid;			///< A local copy of the thread ID
		int				m_exitcode;		///< The exit code of the thread
		SWThreadObjProc	m_tfunc;		///< The thread function
		void			*m_param;		///< The thread parameter
		int				m_priority;		///< The thread priority
		};



// Exceptions

/// A general thread exception
SW_DEFINE_EXCEPTION(SWThreadException, SWException);

/// An operation has been attempted on a thread object that no longer has a valid thread handle.
SW_DEFINE_EXCEPTION(SWThreadInvalidHandleException, SWThreadException);

/// An attempt to create a thread failed.
SW_DEFINE_EXCEPTION(SWThreadCreationException, SWThreadException);

/// An attempt was made to perform an illegal operation on an already running thread (e.g. start, getExitCode)
SW_DEFINE_EXCEPTION(SWThreadRunningException, SWThreadException);

/// A thread attempted to start with a non-existent function.
SW_DEFINE_EXCEPTION(SWThreadNotFunctionException, SWThreadException);





#endif
