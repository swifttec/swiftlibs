/****************************************************************************
**	SWMsgSocket.h	A TCP based socket for passing SWMsg objects
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#ifndef __SWMsgSocket_h__
#define __SWMsgSocket_h__

#include <swift/SWMsg.h>
#include <swift/SWSocketInputStream.h>
#include <swift/SWSocketOutputStream.h>
#include <swift/SWTcpSocket.h>



// Forward declarations

/**
*** A TCP based socket for passing SWMsg objects
***
*** We encapsulate the SWTcpSocket object, rather than extend it
*** to prevent the user from calling the standard socket methods
*** in an attempt to reduce the risk of corrupting the data stream.
**/
class SWIFT_DLL_EXPORT SWMsgSocket : public SWObject
		{
public:
		/// Default constructor
		SWMsgSocket(SWTcpSocket *sock=NULL, bool delFlag=false);

		/// Destructor
		virtual ~SWMsgSocket();

		// Send the message object
		sw_status_t		send(const SWMsg &msg);

		// Receive a message object
		sw_status_t		receive(SWMsg &msg);

		// Get a pointer to the real socket for those circumstances when it's
		// really needed. Use with caution!
		SWTcpSocket *	getSocket() const	{ return m_pSocket; }

		// Set the socket we are associated with
		void			setSocket(SWTcpSocket *sock, bool delFlag=false);

private:
		SWSocketInputStream		m_is;		// The input streasm
		SWSocketOutputStream	m_os;		// The output streasm
		SWTcpSocket				*m_pSocket;		// A copy of the socket pointer
		bool					m_delFlag;	// If true the socket is destroyed in the destructor
		};



#endif
