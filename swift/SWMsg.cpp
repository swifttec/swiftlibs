/****************************************************************************
**	SWMsg.cpp	A class for encapsulating variable length messages
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWMsg.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





SWMsg::SWMsg(sw_uint32_t id) :
	m_id(id)
		{
		initMsg(id);
		}


SWMsg::SWMsg(const SWMsg &other)
		{
		*this = other;
		}


/**
*** Destructor
**/
SWMsg::~SWMsg()
		{
		// We need to make sure we delete all the memory used for the fields
		clear();
		}


/**
*** Clear the message of all fields
**/
void
SWMsg::clear()
		{
		m_fieldVec.clear();
		}


/**
*** Common init function - used by constructors
**/
void
SWMsg::initMsg(int id)
		{
		m_id = id;
		clear();
		}


/**
*** Memory Buffer Constructor - the message is constructed from the
*** given memory buffer
**/
SWMsg::SWMsg(const char *data)
		{
		readFromBuffer(data);
		}


/**
*** Reads and initialises the message from a memory buffer
**/
sw_status_t
SWMsg::readFromBuffer(const void *buf)
		{
		SWMemoryInputStream	is(buf);
		
		return readFromStream(is);
		}
	

/**
*** Read and initialise the message from the given input stream
**/
sw_status_t
SWMsg::readFromStream(SWInputStream &is)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		sw_uint32_t	nfields=0;

		clear();
		res = is.read(m_id);
		if (res == SW_STATUS_SUCCESS) res = is.read(nfields);
		if (res == SW_STATUS_SUCCESS)
			{
			SWMsgField	mf;
			sw_uint32_t	i;

			for (i=0;res==SW_STATUS_SUCCESS && i<nfields;i++)
				{
				res = mf.readFromStream(is);
				if (res == SW_STATUS_SUCCESS) m_fieldVec.add(mf);
				}
			}

		return res;
		}


/**
*** Writes the message to a memory buffer
***
*** @return	The length of the buffer
**/
sw_status_t
SWMsg::writeToBuffer(SWBuffer &buf) const
		{
		SWMemoryOutputStream	mos(buf);

		return writeToStream(mos);
		}


/**
*** Writes the message to the given stream
**/
sw_status_t
SWMsg::writeToStream(SWOutputStream &os) const
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		sw_uint32_t	nfields=(sw_uint32_t)getFieldCount();

		res = os.write(m_id);
		if (res == SW_STATUS_SUCCESS) res = os.write(nfields);
		if (res == SW_STATUS_SUCCESS)
			{
			sw_uint32_t	i;

			for (i=0;res==SW_STATUS_SUCCESS && i<nfields;i++)
				{
				res = m_fieldVec[i].writeToStream(os);
				}
			}

		return res;
		}


/**
*** Assignment operator
**/
SWMsg &
SWMsg::operator=(const SWMsg &other)
		{
#define COPY(x)	x = other.x
		COPY(m_id);
		COPY(m_fieldVec);
#undef COPY

		return *this;
		}


void
SWMsg::add(const SWMsgField &v)
		{
		m_fieldVec.add(v);
		}


void	
SWMsg::add(sw_uint32_t id, bool v)
		{
		add(SWMsgField(id, v));
		}


void	
SWMsg::add(sw_uint32_t id, sw_int8_t v)
		{
		add(SWMsgField(id, v));
		}


void	
SWMsg::add(sw_uint32_t id, sw_int16_t v)
		{
		add(SWMsgField(id, v));
		}


void	
SWMsg::add(sw_uint32_t id, sw_int32_t v)
		{
		add(SWMsgField(id, v));
		}


void	
SWMsg::add(sw_uint32_t id, sw_int64_t v)
		{
		add(SWMsgField(id, v));
		}


void	
SWMsg::add(sw_uint32_t id, sw_uint8_t v)
		{
		add(SWMsgField(id, v));
		}


void	
SWMsg::add(sw_uint32_t id, sw_uint16_t v)
		{
		add(SWMsgField(id, v));
		}


void	
SWMsg::add(sw_uint32_t id, sw_uint32_t v)
		{
		add(SWMsgField(id, v));
		}


void	
SWMsg::add(sw_uint32_t id, sw_uint64_t v)
		{
		add(SWMsgField(id, v));
		}


void	
SWMsg::add(sw_uint32_t id, float v)
		{
		add(SWMsgField(id, v));
		}


void	
SWMsg::add(sw_uint32_t id, double v)
		{
		add(SWMsgField(id, v));
		}


void	
SWMsg::add(sw_uint32_t id, const char *v)
		{
		add(SWMsgField(id, v));
		}


void	
SWMsg::add(sw_uint32_t id, const wchar_t *v)
		{
		add(SWMsgField(id, v));
		}


void	
SWMsg::add(sw_uint32_t id, const void *pData, size_t len)
		{
		add(SWMsgField(id, pData, len));
		}


void	
SWMsg::add(sw_uint32_t id, const SWBuffer &v)
		{
		add(SWMsgField(id, v));
		}


void	
SWMsg::add(sw_uint32_t id, const SWString &v)
		{
		add(SWMsgField(id, v));
		}


void	
SWMsg::add(sw_uint32_t id, const SWDate &v)
		{
		add(SWMsgField(id, v));
		}


void	
SWMsg::add(sw_uint32_t id, const SWTime &v)
		{
		add(SWMsgField(id, v));
		}


void	
SWMsg::add(sw_uint32_t id, const SWMoney &v)
		{
		add(SWMsgField(id, v));
		}



#if defined(SW_BUILD_MFC)
void	
SWMsg::add(sw_uint32_t id, const COleVariant &v)
		{
		add(SWMsgField(id, v));
		}


void	
SWMsg::add(sw_uint32_t id, const CString &v)
		{
		add(SWMsgField(id, v));
		}
#endif

void	
SWMsg::add(sw_uint32_t id, const SWMsg &v)
		{
		SWBuffer	buf;

		v.writeToBuffer(buf);
		add(SWMsgField(id, buf));
		}


void	
SWMsg::add(sw_uint32_t id, const SWValue &v)
		{
		m_fieldVec.add(SWMsgField(id, v));
		}






/**
*** Return the nth field in this message
**/
SWMsgField
SWMsg::getField(int n) const
		{
		return m_fieldVec[n];
		}


/**
*** Search for the field with the given ID
**/
bool
SWMsg::findField(sw_uint32_t id, SWMsgField &f) const
		{
		bool	found=false;
		
		for (int i=0;i<(int)m_fieldVec.size();i++)
			{
			if (m_fieldVec[i].id() == id)
				{
				f = m_fieldVec[i];
				found = true;
				break;
				}
			}

		return found;
		}


/**
*** Search for the field with the given ID
**/
bool
SWMsg::findField(sw_uint32_t id, SWMsgField &f)
		{
		SWIterator<SWMsgField>	it=iterator();
		SWMsgField				*pField;
		bool					found=false;

		while (!found && it.hasNext())
			{
			pField = it.next();
			if (pField->id() == id)
				{
				f = *pField;
				found = true;
				}
			}

		return found;
		}



/**
*** Dump the message to stdout for debug purposes
**/
void
SWMsg::dump()
		{
		SWMsgField	mf;
		sw_size_t	i;

		printf("------- SWMsg Dump -----------\n");
		printf("id=%d  nfields=%d\n", m_id, (int)m_fieldVec.size());
		printf("--------------------------------\n");
		for (i=0;i<getFieldCount();i++)
			{
			mf = getField((int)i);
			mf.dump();
			}
		printf("--------------------------------\n\n");

		/*
		if (hexdump)
			{
			char	*buf;
			int	len;

			buf = toByteArray(&len);
			for (i=0;i<len;i++) printf("[%x] ", buf[i]&0xff);
			printf("\n");
			delete buf;
			printf("--------------------------------\n\n");
			}
		*/
		}





