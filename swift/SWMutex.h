/**
*** @file		SWMutex.h
*** @brief		An abstract mutex class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWMutex class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWMutex_h__
#define __SWMutex_h__


#include <xplatform/sw_thread.h>
#include <xplatform/sw_mutex.h>

#include <swift/swift.h>
#include <swift/SWObject.h>
#include <swift/SWException.h>





#define SW_MUTEX_DELETE_ON_CLOSE	0x1
#define SW_MUTEX_RECURSIVE			0x2

/**
*** @brief	An abstract mutex class.
***
*** SWMutex is an abstract mutex class that forms the base of all mutex classes.
*** Both recursive and non-recursive mutexes are supported in the SWIFT.
*** 
*** A recursive mutex allows the same thread/process to acquire the mutex more
*** than once by using reference counting. The mutex will be acquired when the
*** lock method is first called. Subsequent calls to the lock method by the
*** same thread/process will only increment the reference count.
*** 
*** Similarly each call to unlock will decrease the reference count. When the 
*** reference count reaches zero the lock will be release.
*** 
*** @author		Simon Sparkes
*** @see		SWThreadMutex, SWProcessMutex, SWRWMutex, SWThreadRWMutex, SWProcessRWMutex
**/
class SWIFT_DLL_EXPORT SWMutex
		{
friend class SWCondVar;
friend class SWLockableObject;

protected:
		/// Protected constructor - to allow construction only by derived classes
		SWMutex(sw_uint32_t flags);

public:
		virtual ~SWMutex();

		/**
		*** @brief	Check to see if this mutex can be acquired recursively (multiple times)
		**/
		bool				isRecursive() const		{ return ((m_flags & SW_MUTEX_RECURSIVE) != 0); }

		/**
		*** @brief	Return the delete-on-close status
		**/
		bool				deleteOnClose() const	{ return ((m_flags & SW_MUTEX_DELETE_ON_CLOSE) != 0); }


		/**
		*** @brief	Check to see if this mutex is locked by someone.
		***
		*** This method checks to see if the mutex is currently in the locked
		*** (acquired) state by someone. If the mutex is currently locked then this
		*** method returns TRUE. If no-one currently owns the mutex then FALSE is
		*** returned.
		***
		*** @note
		*** This method will return TRUE if any thread/process currently owns
		*** the mutex. If a caller wishes to determine if the current thread/process
		*** owns the mutex they should call the hasLock() method.
		**/
		virtual bool		isLocked()=0;

		/**
		*** @brief	Check to see if the current thread/process has locked the mutex.
		***
		*** This method returns TRUE if the current thread/process owns the mutex.
		*** (A thread/process "owns" a mutex when it has performed a successful
		*** acquire operation.
		**/
		virtual bool		hasLock()=0;

		/**
		*** @brief	Lock/Acquire this mutex.
		***
		*** This method causes the calling thread/process to lock/acquire the mutex.
		*** If the mutex is currently owned by another thread/process then the thread
		*** will be blocked until the mutex is released by the other thread/process and
		*** acquired by the current thread/process.
		***
		*** The caller can optionally specify a timeout in milliseconds which defaults
		*** to SW_TIMEOUT_INFINITE. If specified and the mutex cannot be acquire within
		*** the specified time this method will return a status of SW_STATUS_TIMEOUT.
		***
		*** @param[in]	waitms	The time in milliseconds to wait for the mutex before
		***						returning a status SW_STATUS_TIMEOUT.
		***
		*** @return		SW_STATUS_SUCCESS if acquired, SW_STATUS_TIMEOUT if a timeout
		***				occurred or an error code if the mutex could not be acquired
		***				for some other reason.
		**/
		virtual sw_status_t	acquire(sw_uint64_t waitms=SW_TIMEOUT_INFINITE)=0;

		/**
		*** @brief	Unlock/Release this mutex.
		***
		*** This method releases a previously acquired mutex.
		***
		*** @return		SW_STATUS_SUCCESS if acquired, SW_STATUS_TIMEOUT if a timeout
		***				occurred or an error code if the mutex could not be acquired
		***				for some other reason.
		**/
		virtual sw_status_t	release()=0;

		/**
		*** @brief	Acquire the mutex for reading.
		***
		*** This method is provided for compatibility with reader-writer locks
		*** which allows a SWMutex to have a full generic interface. By default
		*** this method is mapped to the acquire method for mutexes that do not
		*** support the acquiring of read and write locks directly.
		***
		*** The caller can optionally specify a timeout in milliseconds which defaults
		*** to SW_TIMEOUT_INFINITE. If specified and the mutex cannot be acquire within
		*** the specified time this method will return a status of SW_STATUS_TIMEOUT.
		***
		*** @param[in]	waitms	The time in milliseconds to wait for the mutex before
		***						returning a status SW_STATUS_TIMEOUT.
		***
		*** @return		SW_STATUS_SUCCESS if acquired, SW_STATUS_TIMEOUT if a timeout
		***				occurred or an error code if the mutex could not be acquired
		***				for some other reason.
		**/
		virtual sw_status_t	acquireReadLock(sw_uint64_t waitms=SW_TIMEOUT_INFINITE);

		/**
		*** @brief	Acquire the mutex for writing
		***
		*** This method is provided for compatibility with reader-writer locks
		*** which allows a SWMutex to have a full generic interface. By default
		*** this method is mapped to the acquire method for mutexes that do not
		*** support the acquiring of read and write locks directly.
		***
		*** The caller can optionally specify a timeout in milliseconds which defaults
		*** to SW_TIMEOUT_INFINITE. If specified and the mutex cannot be acquire within
		*** the specified time this method will return a status of SW_STATUS_TIMEOUT.
		***
		*** @param[in]	waitms	The time in milliseconds to wait for the mutex before
		***						returning a status SW_STATUS_TIMEOUT.
		***
		*** @return		SW_STATUS_SUCCESS if acquired, SW_STATUS_TIMEOUT if a timeout
		***				occurred or an error code if the mutex could not be acquired
		***				for some other reason.
		**/
		virtual sw_status_t	acquireWriteLock(sw_uint64_t waitms=SW_TIMEOUT_INFINITE);

		/**
		*** @brief	Test to see if the current thread/process is holding the read lock.
		***
		*** This method is provided for compatibility with reader-writer locks
		*** which allows a SWMutex to have a full generic interface. By default
		*** this method is mapped to the hasLock method for mutexes that do not
		*** support the acquiring of read and write locks directly.
		**/
		virtual bool		hasReadLock()		{ return hasLock(); }

		/**
		*** @brief	Test to see if the current thread/process is holding the write lock.
		***
		*** This method is provided for compatibility with reader-writer locks
		*** which allows a SWMutex to have a full generic interface. By default
		*** this method is mapped to the hasLock method for mutexes that do not
		*** support the acquiring of read and write locks directly.
		**/
		virtual bool		hasWriteLock()		{ return hasLock(); }

		/**
		*** @brief	Check to see if this mutex is locked by someone for read access.
		***
		*** This method checks to see if the mutex is currently locked for read access
		*** by someone. If the mutex is currently locked for read access then this
		*** method returns TRUE. If no-one currently owns the mutex then FALSE is
		*** returned.
		***
		*** This method is provided for compatibility with reader-writer locks
		*** which allows a SWMutex to have a full generic interface.
		*** This method is mapped to the isLocked method for mutexes that do not
		*** support the acquiring of read and write locks directly.
		***
		*** @note
		*** This method will return TRUE if any thread/process currently has
		*** the mutex locked for read access. If a caller wishes to determine
		*** if the current thread/process owns the mutex for read access they
		*** should call the hasReadLock() method.
		**/
		virtual bool		isLockedForRead()	{ return isLocked(); }

		/**
		*** @brief	Check to see if this mutex is locked by someone for write access.
		***
		*** This method checks to see if the mutex is currently locked for write access
		*** by someone. If the mutex is currently locked for write access then this
		*** method returns TRUE. If no-one currently owns the mutex then FALSE is
		*** returned.
		***
		*** This method is provided for compatibility with reader-writer locks
		*** which allows a SWMutex to have a full generic interface.
		*** This method is mapped to the isLocked method for mutexes that do not
		*** support the acquiring of read and write locks directly.
		***
		*** @note
		*** This method will return TRUE if any thread/process currently has
		*** the mutex locked for write access. If a caller wishes to determine
		*** if the current thread/process owns the mutex for write access they
		*** should call the hasReadLock() method.
		**/
		virtual bool		isLockedForWrite()	{ return isLocked(); }

		/**
		*** Do a full counted release of the mutex.
		***
		*** This method is useful for forcing the release of a mutex when 
		*** it has been (or may have been) recursively acquired. This can
		*** be used in conjunction with the fullAcquire method to make a 
		*** temporary release of the mutex and then re-acquire it to the
		*** same state as it was in.
		***
		*** <b>It is important to restore the mutex to the same state as
		*** doing a release on an unheld mutex may cause an exception</b>.
		***
		*** This method is required for certain SWIFT internal methods but
		*** has been made public for its potential usefulness in solving certain
		*** difficult deadlock situations.
		*** 
		*** It is recommended that this method is used only if really needed and that the
		*** normal methods for acquire and release be used in preference.
		***
		*** @param[out]	readLockCount	Receives the count of the number of times the read lock had been acquired.
		*** @param[out]	writeLockCount	Receives the count of the number of times the write lock had been acquired.
		**/
		virtual void		fullRelease(int &readLockCount, int &writeLockCount);

		/**
		*** Do a full counted acquire of the mutex.
		***
		*** This method is normally used in conjucntion with the fullRelease method
		*** for creating a temporary mutex unlock and restore.
		***
		*** The default behaviour of this method is to acquire the read lock the specified
		*** number of times followed by the write lock. This is because a write lock normally
		*** takes precedence over a read lock. However classes derived from SWMutex
		*** may override the default behaviour.
		*** 
		*** <b>It is important to restore the mutex to the same state as
		*** doing a release on an unheld mutex may cause an exception</b>.
		***
		*** This method is required for certain SWIFT internal methods but
		*** has been made public for its potential usefulness in solving certain
		*** difficult deadlock situations.
		*** 
		*** It is recommended that this method is used only if really needed and that the
		*** normal methods for acquire and release be used in preference.
		***
		*** @param[in]	readLockCount	The number of times to acquire the read lock
		*** @param[in]	writeLockCount	The number of times to acquire the write lock
		**/
		virtual void		fullAcquire(int readLockCount, int writeLockCount);

protected:
		/**
		*** @internal
		*** @brief Internal method used by SWCondVar to get the read and write lock counts, set the counts to zero and release the mutex.
		***
		*** @param[out]	tid				The thread ID holding the mutex
		*** @param[out]	readLockCount	The read lock count
		*** @param[out]	writeLockCount	The write lock count
		**/
		virtual void		getOwnerAndLockCounts(sw_thread_id_t &tid, int &readLockCount, int &writeLockCount);

		/**
		*** @internal
		*** @brief Internal method used by SWCondVar to re-acquire the mutex and reset the read and write lock counts.
		***
		*** @param[in]	tid				The thread ID to set
		*** @param[in]	readLockCount	The read lock count to set
		*** @param[in]	writeLockCount	The write lock count to set
		**/
		virtual void		setOwnerAndLockCounts(sw_thread_id_t tid, int readLockCount, int writeLockCount);

protected:
		sw_uint32_t	m_flags;	///< Various flags
		};


/// General Mutex exception
SW_DEFINE_EXCEPTION(SWMutexException, SWException);

/// Mutex creation exception
SW_DEFINE_EXCEPTION(SWMutexCreateException, SWException);

/// Throw if a process or thread attempts to acquire a non-recusive mutex that it already holds.
SW_DEFINE_EXCEPTION(SWMutexAlreadyHeldException, SWMutexException);

/// Throw if a process or thread attempts to release a mutex that is not held.
SW_DEFINE_EXCEPTION(SWMutexNotHeldException, SWMutexException);




#endif
