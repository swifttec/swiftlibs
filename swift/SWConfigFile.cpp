/****************************************************************************
**	SWConfigFile.cpp	Generic config file implementation
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWConfigFile.h>
#include <swift/SWGuard.h>
#include <swift/SWTokenizer.h>
#include <swift/SWConfigValue.h>
//#include <swift/sw_string.h>




// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif

SWConfigFile::SWConfigFile(const sw_tchar_t *filename, bool readonly, bool woc, bool ignoreCase) :
    SWConfigKey(NULL, filename, ignoreCase),
    m_readOnlyFlag(readonly),
    m_writeOnChangeFlag(woc),
    m_loading(false),
	m_loaded(false)
		{
		}


SWConfigFile::~SWConfigFile()
		{
		}


/**
*** Write the contents to file (if they've changed and we are not readonly)
**/
void
SWConfigFile::flush()
		{
		if (isReadOnly() || !changed()) return;

		writeToFile(_name);
		clearChangedFlag();
		}



/**
*** Loads the config structures from the ini file
**/
bool
SWConfigFile::readFromFile(const SWString &)
		{
		throw SW_EXCEPTION_TEXT(SWConfigFileException, "readFromFile method not support on SWConfigFile class");

		return false;
		}


/**
*** Writes the data structure out to a file
**/
bool
SWConfigFile::writeToFile(const SWString &)
		{
		throw SW_EXCEPTION_TEXT(SWConfigFileException, "writeToFile method not support on SWConfigFile class");

		return false;
		}



// Set the changed flag.
// We must also set the changed flag of our owner
void
SWConfigFile::setChangedFlag(ChangeEventCode code, SWObject *obj)
		{
		if (!m_loading)
			{
			_changed = true;
			if (!m_readOnlyFlag && m_writeOnChangeFlag) flush();
			}
		}


bool
SWConfigFile::isLoaded() const
		{
		return m_loaded;
		}



bool
SWConfigFile::isReadOnly() const
		{
		return m_readOnlyFlag;
		}


bool
SWConfigFile::isWriteOnChange() const
		{
		return m_writeOnChangeFlag;
		}


#ifdef NEVER
// Find the named section
SWConfigSection *
SWConfigFile::findSection(const sw_tchar_t *name)
		{
		Enumeration	e = _sectionVec.elements();

		while (e.hasMoreElements())
			{
			ConfigSection *cs = (ConfigSection *)e.nextElement();

			if (cs->name() == name) return cs;
			}

		return NULL;
		}


// Dump the file to stdout (for debug purposes)
void
SWConfigFile::dump()
		{
		Enumeration	e = _sectionVec.elements();

		t_printf(_T("ConfigFile %s has %d sections (changed=%d)\n"), _name.data(), _sectionVec.size(), _changed);
		while (e.hasMoreElements())
			{
			ConfigSection *cs = (ConfigSection *)e.nextElement();

			cs->dump();
			}
		}

// Clear the changed flag. We must also clear the changed flags
// of all our children
void
SWConfigFile::clearChangedFlag()
		{
		_changed = 0;

		Enumeration	e = _sectionVec.elements();

		while (e.hasMoreElements())
			{
			ConfigSection *cs = (ConfigSection *)e.nextElement();

			cs->clearChangedFlag();
			}
		}


// Add a ConfigSection to this ConfigFile
SWConfigSection *
SWConfigFile::addSection(ConfigSection *section)
		{
		section->setOwner(this);
		_sectionVec.add(section);
		setChangedFlag();

		return section;
		}


// Add a ConfigSection to this ConfigFile (if it is not already there
SWConfigSection *
SWConfigFile::addSection(const sw_tchar_t *name, const sw_tchar_t *comment)
		{
		ConfigSection	*cs;

		if ((cs = findSection(name))) cs->setComment(comment);
		else cs = addSection(new ConfigSection(name, comment, this));

		return cs;
		}


// Remove a ConfigSection
void
SWConfigFile::removeSection(const sw_tchar_t *name)
		{
		ConfigSection	*cs;

		if ((cs = findSection(name))) 
			{
			_sectionVec.remove(cs);
			delete cs;
			setChangedFlag();
			}
		}


// Remove a ConfigSection
void
SWConfigFile::removeEntry(const sw_tchar_t *section, const sw_tchar_t *name)
		{
		ConfigSection	*cs;

		if ((cs = findSection(section))) cs->removeEntry(name);
		}


//--------------------------------------------------------------------------------
// Methods for quick get and set of entry values

// Get a string from the name param of section. Return defval if not found.
SWString
SWConfigFile::getString(const sw_tchar_t *section, const sw_tchar_t *name, const sw_tchar_t *defval)
		{
		ConfigSection	*cs;

		if (!(cs = findSection(section))) return defval;
		return cs->getString(name, defval);
		}


// Get an integer from the name param of section. Return defval if not found.
int
SWConfigFile::getInteger(const sw_tchar_t *section, const sw_tchar_t *name, int defval)
		{
		ConfigSection	*cs;

		if (!(cs = findSection(section))) return defval;
		return cs->getInteger(name, defval);
		}


// Get a bool from the name param of section. Return defval if not found.
bool
SWConfigFile::getBoolean(const sw_tchar_t *section, const sw_tchar_t *name, bool defval)
		{
		ConfigSection	*cs;

		if (!(cs = findSection(section))) return defval;
		return cs->getBoolean(name, defval);
		}



void
SWConfigFile::setString(const sw_tchar_t *section, const sw_tchar_t *name, const sw_tchar_t *value)
		{
		ConfigSection	*cs;

		if (!writeable()) return;

		if (!(cs = findSection(section))) cs = addSection(section, _T(""));
		cs->setString(name, value);
		}


void
SWConfigFile::setInteger(const sw_tchar_t *section, const sw_tchar_t *name, int value)
		{
		ConfigSection	*cs;

		if (!writeable()) return;

		if (!(cs = findSection(section))) cs = addSection(section, _T(""));
		cs->setInteger(name, value);
		}


void
SWConfigFile::setBoolean(const sw_tchar_t *section, const sw_tchar_t *name, bool value)
		{
		ConfigSection	*cs;

		if (!writeable()) return;

		if (!(cs = findSection(section))) cs = addSection(section, _T(""));
		cs->setBoolean(name, value);
		}



// Get a value from the specified section. Returns 0 on OK with value set, -1 on failure with value set to defval
int
SWConfigFile::getValue(SWString &value, const sw_tchar_t *section, const sw_tchar_t *name, const sw_tchar_t *defval)
		{
		ConfigSection	*cs;
		ConfigEntry	*ce;
		int		res = 0;

		if (!(cs = findSection(section))) res = -1;
		else
			{
			if (!(ce = cs->findEntry(name))) res = -1;
			else 
				{
				value = ce->getString();
				if (_expandEnvVars) value.expandEnvVars();
				}
			}
		if (res < 0) value = defval;

		return res;
		}


// Set a value into the specified section. Returns 0 on OK, -1 on failure
// Will not write to a readonly config file
int
SWConfigFile::setValue(const SWString &value, const sw_tchar_t *section, const sw_tchar_t *name)
		{
		ConfigSection	*cs;
		ConfigEntry	*ce;

		if (!writeable()) return -1;

		if (!(cs = findSection(section)))
			{
			cs = addSection(section, _T(""));
			cs->addEntry(name, value);
			}
		else
			{
			if (!(ce = cs->findEntry(name)))
				{
				cs->addEntry(name, value);
				}
			else ce->setString(value);
			}

		return 0;
		}

#endif

