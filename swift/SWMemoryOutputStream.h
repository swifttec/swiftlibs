/**
*** @file		SWMemoryOutputStream.h
*** @brief		A class representing an output stream writing to a memory buffer.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWMemoryOutputStream class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#ifndef __SWMemoryOutputStream_h__
#define __SWMemoryOutputStream_h__

#include <swift/SWOutputStream.h>
#include <swift/SWBuffer.h>





/**
*** @brief	A memory buffer output stream
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWMemoryOutputStream  : public SWOutputStream
		{
public:
		/**
		*** @brief	Default constructor
		***
		*** The default constructor uses the built-in SWBuffer object to
		*** write to
		**/
		SWMemoryOutputStream();

		/// Virtual destructor
		virtual ~SWMemoryOutputStream();

		SWMemoryOutputStream(SWBuffer &buf, sw_size_t offset=0);

		/// Returns the length of the data buffer
		sw_size_t				length() const		{ return m_datalen; }

		/// Returns a pointer to the data buffer
		const void *			data() const		{ return m_pBuffer->data(); }


public: // SWOutputStream Overrides

		///< Write data to the output stream.
		virtual sw_status_t		writeData(const void *pData, sw_size_t len);

		/// Rewind the stream to starting writing at the start of the buffer
		virtual sw_status_t		rewindStream();
	

private:
		SWBuffer	m_defaultbuf;	///< The default buffer used when one is not supplied.
		SWBuffer	*m_pBuffer;		///< A reference to the used buffer
		sw_size_t	m_datalen;
		};





#endif
