/**
*** @file		SWMulticastSocket.h
*** @brief		A TCP socket
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWMulticastSocket class.
**/


#ifndef __SWMulticastSocket_h__
#define __SWMulticastSocket_h__

#include <swift/SWUdpSocket.h>


// Forward declarations

/**
*** @brief	A TCP socket implementation
***
*** The SWMulticastSocket is a TCP specific socket implementation which
*** supports both client and server sockets.
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Safe when synchronized</td>
*** </tr>
*** </table>
***
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWMulticastSocket : public SWUdpSocket
		{
public:
		/// Default Constructor
		SWMulticastSocket();


public: // SWSocket Overrides

		virtual sw_status_t			addMembership(const SWIPAddress &ipaddr, const SWIPAddress &ifaddr=SWIPAddress(""));
		virtual sw_status_t			dropMembership(const SWIPAddress &ipaddr, const SWIPAddress &ifaddr=SWIPAddress(""));
		};


#endif
