/**
*** @file		SWSystemInfo.cpp
*** @brief		A generic value object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWSystemInfo class.
**/

/*********************************************************************************
Copyright (c) 2006-2016 SwiftTec Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWSystemInfo.h>
#include <swift/SWString.h>

#if !defined(SW_PLATFORM_WINDOWS)
	#if defined(SW_PLATFORM_SOLARIS)
		#include <sys/systeminfo.h>
	#endif
	#include <sys/utsname.h>
	#include <unistd.h>
#endif

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif

SWIFT_NAMESPACE_BEGIN

SWSystemInfo::SWSystemInfo()
		{
		}


SWSystemInfo::~SWSystemInfo()
		{
		}


SWSystemInfo::SWSystemInfo(const SWSystemInfo &other) :
	SWInfoObject()
		{
		*this = other;
		}


SWSystemInfo &
SWSystemInfo::operator=(const SWSystemInfo &other)
		{
		m_values = other.m_values;
		m_os = other.m_os;
		m_hardware = other.m_hardware;
		//m_network = other.m_network;

		return *this;
		}


sw_status_t
SWSystemInfo::getLocalInfo(SWSystemInfo &info)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		SWString	hostname;
		sw_uint32_t	hostid=0;

#if defined(SW_PLATFORM_WINDOWS)
		TCHAR	tmpstr[1024];
		DWORD	n=1024;

		GetComputerName(tmpstr, &n);
		hostname = tmpstr;

#else // defined(SW_PLATFORM_WINDOWS)
		struct utsname	u;

		if (uname(&u) >= 0)
			{
			hostname = u.nodename;
			}

	#if defined(SW_PLATFORM_SOLARIS)
		char	buf[1024];

		if (sysinfo(SI_HOSTNAME, buf, sizeof(buf)) > 0) hostname = buf;
	#endif

		hostid = gethostid();

#endif // defined(SW_PLATFORM_WINDOWS)

		info.setValue(HostName, hostname);
		info.setValue(HostID, hostid);

		if (res == SW_STATUS_SUCCESS) res = SWOperatingSystemInfo::getMachineInfo(info.m_os);
		if (res == SW_STATUS_SUCCESS) res = SWHardwareInfo::getMachineInfo(info.m_hardware);
		//if (res == SW_STATUS_SUCCESS) res = SWNetworkInfo::getMachineInfo(info._network);

		return SW_STATUS_SUCCESS;
		}


SWIFT_NAMESPACE_END
