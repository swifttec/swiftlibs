/* Start of generated build info (C/C++ format) */
#define SW_PRODUCT_MAJOR_VERSION	1
#define SW_PRODUCT_MAJOR_VERSION_STR	"1"
#define SW_PRODUCT_MINOR_VERSION	4
#define SW_PRODUCT_MINOR_VERSION_STR	"4"
#define SW_PRODUCT_BUILD	750
#define SW_PRODUCT_BUILD_STR	"750"
#define SW_PRODUCT_REVISION	96
#define SW_PRODUCT_REVISION_STR	"96"
#define SW_PRODUCT_VERSION_FORMAT	"%m.%n.%r.%b"
#define SW_PRODUCT_VERSION	"1.4.96.750"
#define SW_PRODUCT_LIBNAME	"swift"
#define SW_PRODUCT_NAME	"Swift Component Library"
#define SW_PRODUCT_DESCRIPTION	"A collection of classes and functions for SwiftTec products"
#define SW_PRODUCT_COMPANY	"SwiftTec"
#define SW_PRODUCT_AUTHOR	"Simon Sparkes"
#define SW_PRODUCT_COPYRIGHT	"\251 Copyright 2006 SwiftTec"
/* End of generated build info */
