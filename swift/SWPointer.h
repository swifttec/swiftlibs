/**
*** @file		SWPointer.h
*** @brief		An array of Pointers
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWPointer class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWPointer_h__
#define __SWPointer_h__


#include <swift/SWObject.h>




/**
*** @brief	An array of pointers based on the Array class.
***
*** An array of pointers based on the Array class.
***
*** @par Attributes
***
*** @see		SWAbstractArray
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWPointer : public SWObject
		{
public:
		/**
		*** @brief	Construct a pointer array with an optional initial size and increment.
		***
		*** @param[in]	initialSize		The initial size of the array (default=0)
		*** @param[in]	incsize			The amount by which to grow the capacity of the array when a resize is required.
		**/
		SWPointer(void *ptr=NULL);

		/// Virtual destructor
		virtual ~SWPointer();

		/// Convert to a pointer
		operator void * &()		{ return m_ptr; }

public: // virtual overrides

		/// Override for SWObject writeToStream
		virtual sw_status_t		writeToStream(SWOutputStream &stream) const;

		/// Override for SWObject readFromStream
		virtual sw_status_t		readFromStream(SWInputStream &stream);

private:
		void	*m_ptr;		///< The pointer value
		};




#endif
