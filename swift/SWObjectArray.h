/**
*** @file		SWObjectArray.h
*** @brief		An array of Objects
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWObjectArray class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWObjectArray_h__
#define __SWObjectArray_h__



#include <swift/SWObject.h>
#include <swift/SWAbstractArray.h>




/**
*** @brief	An array of Objects based on the SWAbstractArray class
***
*** An array of Objects based on the SWAbstractArray class.
*** The SWObjectArray can optionally delete objects added to it
*** when they are no longer referenced.
***
*** @see		SWAbstractArray, SWObject
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWObjectArray : public SWAbstractArray
		{
public:
		/**
		*** @brief	Construct an object array with the specified parameters.
		***
		*** The constructor allows the initial size, the size increment value
		*** and the delete flag all to be optionally specified on array construction.
		***
		*** @param	initialSize	The initial size of the array
		*** @param	incsize		The capacity increment size (see Buffer)
		*** @param	delFlag		If true objects will be deleted when removed from the array.
		*/
		SWObjectArray(sw_size_t initialSize=0, bool delFlag=false);

		/// Virtual destructor
		virtual ~SWObjectArray();

		/// Copy constructor
		SWObjectArray(const SWObjectArray &ua, bool delflag=false);

		/// Assignment operator
		const SWObjectArray &	operator=(const SWObjectArray &ua);


		/**
		*** @brief	return a reference to the nth element in the array.
		***
		*** If the index specified is negative an exception is thrown.
		*** If the index specified is greater than the number of elements in the
		*** array, the array is automatically extended to encompass the referenced
		*** element.
		***
		*** @param[in]	i	The index of the element to access.
		***
		*** @throws			SWArrayIndexOutOfBoundsException is the index is out of bounds.
		**/
		SWObject * &	operator[](sw_index_t i);

		/**
		*** @brief	Get a reference to the nth element in the array.
		***
		*** If the index specified is negative an exception is thrown.
		***
		*** If the index specified is greater than the number of elements in the
		*** array, the array is automatically extended to encompass the referenced
		*** element.
		***
		*** @param[in]	i	The zero-based index of the element to retrieve.
		**/
		SWObject * &	get(sw_index_t i) const;

		/**
		*** @brief	Empty the array deleting the objects as required.
		**/
		virtual void	clear();

		/**
		*** @brief	Remove elements from the array.
		***
		*** This method removes the specified number of elements from the
		*** starting at the position specified. If the delete flag is set
		*** the objects will be deleted as they are removed from the array.
		***
		*** @param[in]	pos		The position to start removing element from.
		*** @param[in]	count	The number of elememnts to remove.
		**/
		virtual void	remove(sw_index_t pos, sw_size_t count=1);

		/// Add to the end of the array
		void			add(SWObject *pObject);

		/// Add (insert) at the specified position in the array
		void			insert(SWObject *pObject, sw_index_t pos);

		/// Add all elements from the given array
		void			add(const SWObjectArray &ua);

		/// Gets the current state of the delete flag
		bool			getDeleteFlag()			{ return m_delFlag; }

		/**
		*** @brief	Sets the delete flag
		***
		*** If the delete flag is true all objects removed from the array
		*** <b>except via the pop method</b> are deleted. If delete flag is
		*** false, all objects removed must be delete by the caller.
		***
		*** @param[in]	v	The new value of the delete flag.
		**/
		void			setDeleteFlag(bool v)	{ m_delFlag = v; }

		/// Push a value onto the end of the array
		void			push(SWObject *pObject);

		/**
		*** @brief	Pops the last element off of the array (<b>never deletes object</b>).
		***
		*** @note
		*** It is important to note that even if the delete flag is set that the
		*** object will <b>NOT</b> be deleted when popped because otherwise it would
		*** be invalid when returned. Therefore the caller is responsible for deleting
		*** any object popped.
		**/
		SWObject *		pop();

private:
		bool			m_delFlag;	///< A flag to indicate if the objects should be deleted on removal
		};





#endif
