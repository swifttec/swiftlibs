/**
*** @file		SWHostname.h
*** @brief		A class to represent a network hostname.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWHostname class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWHostname_h__
#define __SWHostname_h__

#include <swift/swift.h>
#include <swift/SWIPAddress.h>

#include <swift/SWObject.h>
#include <swift/SWStringArray.h>
#include <swift/SWVector.h>

/**
*** @brief	A class to represent a host name, it's aliases and addresses.
***
*** This class represents a host name along with all it's known aliases and
*** addresses.
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Unsafe</td>
*** </tr>
*** </table>
***
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWHostname : public SWObject
		{
public:
		// Used with the _flags member
		enum
			{
			FLAG_IS_VALID=0x1,
			FLAG_IS_IPADDR=0x2,
			FLAG_INFO_IS_VALID=0x4
			};

public:
		/**
		*** @brief	Default constructor - creates an object with no name.
		**/
		SWHostname();

		/**
		*** @brief	String constructor - can be IP address or a hostname.
		**/
		SWHostname(const SWString &name);

		/**
		*** @brief	IP address constructor.
		**/
		SWHostname(const SWIPAddress &addr);

		/**
		*** @brief	Copy constructor from another SWHostname object.
		**/
		SWHostname(const SWHostname &other);

		/**
		*** @brief	Assignment operator from a string (can be ip address or hostname).
		***
		*** This method examines the string to find out if it is an IP address or a hostname
		*** and then asks the system to resolve the address and aliases for the given host.
		**/
		SWHostname &		operator=(const SWString &host);

		/**
		*** @brief	Assignment operator from another SWHostname object.
		**/
		SWHostname &		operator=(const SWHostname &host);

		/**
		*** @brief	IP address assignment operator.
		***
		*** This method takes the specified IP address and calls the system to attempt to resolve
		*** the given address to a name and aliases.
		**/
		SWHostname &		operator=(const SWIPAddress &addr);
		
		/**
		*** @brief	Clear the hostname object.
		***
		*** This method clears all the fields of the SWHostname object.
		**/
		void				clear();

		/**
		*** @brief Returns the assigned name for this host.
		***
		*** This method returns the name that the caller assigned for this host.
		*** The name is assigned when the caller constructs the object (via IP address
		*** or string). This differs from hostname() which retunrs the principal name
		*** as returned by the lookup of the assigned name.
		**/
		const SWString &	name() const;

		/**
		*** @brief	Returns the prinicpal name for this host.
		***
		*** This method returns the principal name for the host as returned by
		*** the system when the assigned name was looked up.
		***
		*** @see name()
		**/
		const SWString &	hostname() const;


		/// test to see if this is a valid hostname
		bool 				isValid() const;
		bool 				gotHostInfo() const;

		/**
		*** @brief	Check to see if the given name is an alias for this host
		**/
		bool				isAlias(const SWString &name) const;

		// Returns an array of known aliases for this hostname
		SWStringArray		aliases() const;

		/// Return the number of aliases
		int					aliasCount() const;

		/// Return the nth alias
		SWString			alias(int n) const;

		/**
		*** Check to see if the given address is an address for this host
		**/
		bool				isAddress(const SWIPAddress &addr) const;

		/// @brief	Return the number of addresses
		int					addressCount() const;

		/// @brief	Return the nth address
		SWIPAddress			address(int n) const;

		/// @brief	Return the primary IP address
		SWIPAddress			address() const;

		// Comparison Operators
		bool	operator==(const SWString &other);

public: // Static methods
		static bool			getByName(const SWString &name, SWHostname &entry);
		static bool			getByAddress(const SWIPAddress &addr, SWHostname &entry);

protected:
		/**
		*** @brief	Update the host information for the specified string (hostname or IP address).
		**/
		void				getInfoForHost(const SWString &host);

		/**
		*** @brief	Update the host information from the specified host entry.
		**/
		void				setFromHostEntry(const void *pEntry);
		
protected:
		SWString				_name;		///< The given host name
		SWString				_hostname;	///< The offcial host name
		SWIPAddress				_address;	///< The primary IP address
		SWStringArray			_aliases;	///< An array of aliases
		SWVector<SWIPAddress>	_ipaddrs;	///< An array of IP addresses
		int						_flags;		///< Various flags
		};



#endif
