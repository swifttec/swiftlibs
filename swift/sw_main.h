/**
*** @file		sw_main.h
*** @brief		A wrapper for main
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the sw_main wrappers.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#ifndef __sw_main_h__
#define __sw_main_h__

#include <swift/SWString.h>
#include <swift/SWStringArray.h>
//#include <swift/sw_log.h>
//#include <swift/sw_file.h>





// Expands * (0 or more chars)
// Expands ? (any single char)
// Expands [] (character range matching)
#define SW_MAIN_EXPAND_WILDCARDS	0x01

// Expand {a,b} (csh like)
#define SW_MAIN_EXPAND_CURLYBRACES	0x02

// Backslash processing \x
#define SW_MAIN_PROCESS_BACKSLASH	0x04

// Windows command line (no program as first arg)
#define SW_MAIN_WINDOWS_CMDLINE		0x08



SWIFT_DLL_EXPORT void	sw_main_parseCommandLine(const SWString &cmdline, SWString &program, SWStringArray &args, int flags);

// End of namespace




int	sw_main_substitute(const SWString &prog, const SWStringArray &args);

#if defined(SW_PLATFORM_WINDOWS)
	#if defined (_CONSOLE)
		#if defined(UNICODE) || defined(_UNICODE)
			#define SW_MAIN_DEF	wmain(int argc, wchar_t **argv)
		#else
			#define SW_MAIN_DEF	main(int argc, char **argv)
		#endif
	#else
		#define SW_MAIN_DEF	WINAPI	WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
	#endif
#else
	#if defined(UNICODE) || defined(_UNICODE)
		#define SW_MAIN_DEF	wmain(int argc, wchar_t **argv)
	#else
		#define SW_MAIN_DEF	main(int argc, char **argv)
	#endif
#endif


#if defined(_WIN32) || defined(WIN32)
// sw_main definition for win32

	#if !defined(SW_MAIN_CMDLINE_FLAGS)
		#define SW_MAIN_CMDLINE_FLAGS	SW_MAIN_EXPAND_WILDCARDS
		// |SW_MAIN_WINDOWS_CMDLINE
	#endif

	#if defined (_CONSOLE)
		#define	sw_main SW_MAIN_DEF \
			{ \
			SWString	prog = argv[0]; \
			SWStringArray	args; \
			UNREFERENCED_PARAMETER(argc); \
			sw_main_parseCommandLine(GetCommandLine(), prog, args, SW_MAIN_CMDLINE_FLAGS); \
			return sw_main_substitute(prog, args); \
			} \
		int \
		sw_main_substitute
	#else
		#define	sw_main SW_MAIN_DEF \
			{ \
			TCHAR		progpath[_MAX_PATH+1]; \
			GetModuleFileName(NULL, progpath, _MAX_PATH); \
			SWString	prog = progpath; \
			SWStringArray	args; \
			sw_main_parseCommandLine(GetCommandLine(), prog, args, SW_MAIN_CMDLINE_FLAGS); \
			return sw_main_substitute(prog, args); \
			} \
		int \
		sw_main_substitute
	#endif


#else
// sw_main definition for non-win32

#define	sw_main SW_MAIN_DEF \
	{ \
	SWString	prog = argv[0]; \
	SWStringArray	args; \
	for (int i=1;i<argc;i++) args.add(argv[i]); \
	return sw_main_substitute(prog, args); \
	} \
int \
sw_main_substitute
#endif




#endif
