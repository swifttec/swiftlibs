/**
*** @file		SWInputStream.h
*** @brief		A generic input stream.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWInputStream class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWInputStream_h__
#define __SWInputStream_h__

#include <swift/swift.h>
#include <swift/SWObject.h>

/**
*** @brief	A generic input stream
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWInputStream : public SWObject
		{
friend class SWString;
friend class SWAbstractBuffer;
friend class SWBuffer;
friend class SWDataBuffer;
friend class SWValue;

public:
		/**
		*** @brief			Write a boolean value to the output stream
		*** @param[out]	v	Receives the value read from the stream.
		**/
		sw_status_t		read(bool &v);

		/**
		*** @brief			Write an 8-bit signed integer value to the output stream
		*** @param[out]	v	Receives the value read from the stream.
		**/
		sw_status_t		read(sw_int8_t &v);

		/**
		*** @brief			Write a 16-bit signed integer value to the output stream
		*** @param[out]	v	Receives the value read from the stream.
		**/
		sw_status_t		read(sw_int16_t &v);

		/**
		*** @brief			Write a 32-bit signed integer value to the output stream
		*** @param[out]	v	Receives the value read from the stream.
		**/
		sw_status_t		read(sw_int32_t &v);
		
		/**
		*** @brief			Write a 64-bit signed integer value to the output stream
		*** @param[out]	v	Receives the value read from the stream.
		**/
		sw_status_t		read(sw_int64_t &v);
		
		/**
		*** @brief			Write an 8-bit unsigned integer value to the output stream
		*** @param[out]	v	Receives the value read from the stream.
		**/
		sw_status_t		read(sw_uint8_t &v);
		
		/**
		*** @brief			Write a 16-bit unsigned integer value to the output stream
		*** @param[out]	v	Receives the value read from the stream.
		**/
		sw_status_t		read(sw_uint16_t &v);
		
		/**
		*** @brief			Write a 32-bit unsigned integer value to the output stream
		*** @param[out]	v	Receives the value read from the stream.
		**/
		sw_status_t		read(sw_uint32_t &v);
		
		/**
		*** @brief			Write a 64-bit unsigned integer value to the output stream
		*** @param[out]	v	Receives the value read from the stream.
		**/
		sw_status_t		read(sw_uint64_t &v);
		
		/**
		*** @brief			Write a float value to the output stream
		*** @param[out]	v	Receives the value read from the stream.
		**/
		sw_status_t		read(float &v);
		
		/**
		*** @brief			Write a double value to the output stream
		*** @param[out]	v	Receives the value read from the stream.
		**/
		sw_status_t		read(double &v);
		
		/**
		*** @brief				Write a single-byte character string to the output stream
		*** @param[out]	v		A pointer to a buffer that receives the value read from the stream.
		*** @param[in]	buflen	The size of the buffer.
		**/
		sw_status_t		read(char *v, sw_uint32_t buflen);
		
		/**
		*** @brief				Write a wide character string to the output stream
		*** @param[out]	v		A pointer to a buffer that receives the value read from the stream.
		*** @param[in]	buflen	The size of the buffer.
		**/
		sw_status_t		read(wchar_t *v, sw_uint32_t buflen);
		
		/**
		*** @brief				Write binary data to the output stream
		*** @param[out]	pData	A pointer to a buffer that receives the data read from the stream.
		*** @param[out] len		The length of the data (in bytes) read from the stream.
		*** @param[in]	buflen	The size of the buffer.
		**/
		sw_status_t		read(void *pData, sw_size_t &len, sw_uint32_t buflen);
		
		/**
		*** @brief			Write object to the output stream
		*** @param[out]	v	Receives the value read from the stream.
		**/
		sw_status_t		read(SWObject &v)						{ return v.readFromStream(*this); }
		
		
		/**
		*** @brief		Rewind the stream to it's beginning point
		**/
		sw_status_t		rewind()								{ return rewindStream(); }


		/**
		*** @brief			Read raw binary data to the output stream (no encoded data length)
		*** @param pData	A pointer to the data to write to the stream
		*** @param len		The length of the data (in bytes) to write to the stream.
		**/
		sw_status_t		readRawData(void *pData, sw_size_t len);

protected: // Deriving classes are expected to override these functions
		virtual sw_status_t		readSignedInteger(sw_int64_t &v, int nbytes);						///< Write a signed number to the output stream
		virtual sw_status_t		readUnsignedInteger(sw_uint64_t &v, int nbytes);					///< Write an unsigned number to the output stream
		virtual sw_status_t		readDouble(double &v);												///< Write a double to the output stream
		virtual sw_status_t		readFloat(float &v);												///< Write a float to the output stream
		virtual sw_status_t		readBinaryData(void *pData, sw_size_t &len, sw_uint32_t buflen);	///< Write binary data to the output stream.
		virtual sw_status_t		readBoolean(bool &v);												///< Write a boolean value to the output stream.
		virtual sw_status_t		readData(void *pData, sw_size_t len);								///< Write binary data to the output stream.
		virtual sw_status_t		rewindStream();														///< Write binary data to the output stream.

protected:
		/// Convert the bits of the 64-bit integer to a double
		static double			longBitsToDouble(sw_int64_t bits);

		/// Convert the bits of the 32-bit integer to a float
		static float			intBitsToFloat(sw_int32_t bits);
		};




#endif
