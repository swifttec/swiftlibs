/**
*** @file		SWLocalMemoryManager.cpp
*** @brief		An memory allocator that sub-allocates from local memory.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWLocalMemoryManager class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWLocalMemoryManager.h>


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




void *
SWLocalMemoryManager::malloc(sw_size_t nbytes)
		{
		void	*p;

#ifdef SW_DEBUG_NEW
		p = SW_DEBUG_MALLOC(nbytes);
#else
		p = ::malloc(nbytes);
#endif
		if (p == NULL) throw SW_EXCEPTION(SWMemoryException);

		return p;
		}


void *
SWLocalMemoryManager::realloc(void *pOldData, sw_size_t nbytes)
		{
		void	*p;

#ifdef SW_DEBUG_NEW
		p = SW_DEBUG_REALLOC(pOldData, nbytes);
#else
		p = ::realloc(pOldData, nbytes);
#endif
		if (p == NULL) throw SW_EXCEPTION(SWMemoryException);

		return p;
		}


void
SWLocalMemoryManager::free(void *pData)
		{
		if (pData != NULL)
			{
#ifdef SW_DEBUG_NEW
			SW_DEBUG_FREE(pData);
#else
			::free(pData);
#endif
			}
		}




