/**
*** @file		SWSocket.h
*** @brief		A generic socket implementation
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWSocket class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWSocket_h__
#define __SWSocket_h__

#include <swift/swift.h>

#include <swift/SWLockableObject.h>
#include <swift/SWException.h>
#include <swift/SWFlags32.h>
#include <swift/SWValue.h>

#include <swift/SWSocketAddress.h>



/**
*** @brief	A generic socket implementation which handles both client and server connections.
*** 
*** SWSocket is a generic socket implementation which handles both client
*** and server connections.
*** Client connections attempt to make a connection to a listening server connection.
*** Server connections listen for incoming connections from clients.
***
*** A SWSocket is generic in nature and does not provide implementations of or enforce
*** a particular protocol.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWSocket : public SWLockableObject
		{
public:
		/**
		*** @brief	The socket options for all socket types
		***
		*** The following are the various options that can be set on various
		*** socket types. They are gathered together in one place to make it 
		*** easier to maintain the table of options, this is partly due to the
		*** fact that there are several generic options that are applicable to
		*** many socket types.
		***
		*** @note
		*** Not all platforms will have support for all options.
		***/
		enum SocketOption
			{
			// General socket options
			SW_SOCKOPT_DEBUG,					///< bool - enable/disable recording of debugging information.
			SW_SOCKOPT_REUSEADDR,				///< bool - enable/disable local address reuse.
			SW_SOCKOPT_KEEPALIVE,				///< bool - enable/disable keep connections alive.
			SW_SOCKOPT_DONTROUTE,				///< bool - enable/disable routing bypass for outgoing messages.
			SW_SOCKOPT_LINGER,					///< bool - linger on close if data is present.
			SW_SOCKOPT_BROADCAST,				///< bool - enable/disable permission to transmit  broadcast  messages.
			SW_SOCKOPT_OOBINLINE,				///< bool - enable/disable reception of out-of-band data in band.
			SW_SOCKOPT_SEND_BUFFER_SIZE,		///< int - set buffer size for output.
			SW_SOCKOPT_RECEIVE_BUFFER_SIZE,		///< int - set buffer size for input.
			SW_SOCKOPT_DGRAM_ERRIND,			///< bool - application wants delayed error.
			SW_SOCKOPT_SEND_TIMEOUT,			///< int - set transmission timeout in milliseconds.
			SW_SOCKOPT_RECEIVE_TIMEOUT,			///< int - set receive timeout in milliseconds.

			// TCP-specific socket options
			SW_SOCKOPT_TCP_NODELAY		///< bool - enable/disable the TCP transmision delay algorithm (Nagle Algorithm).
			};

		/**
		*** @brief	The socket flags for all socket types
		**/
		enum SocketFlag
			{
			SW_SOCKFLAG_CONNECTED,				///< The socket is connected to another socket
			SW_SOCKFLAG_LISTENING,				///< The socket is listening for an incoming connection
			SW_SOCKFLAG_BOUND,					///< The socket is bound
			SW_SOCKFLAG_STREAM					///< The socket is a stream (e.g. TCP)
			};

protected:
		/// Default constructor (protected)
		SWSocket();

public:
		/// Destructor
		virtual ~SWSocket();

		/**
		*** @brief	Opens/creates the socket.
		***
		*** This method opens (creates) the socket ready for use, but it
		*** does not attempt to make any connection or send any data.
		***
		*** @note
		*** This method must be implemented by a deriving socket implementation.
		***
		*** @return	SW_STATUS_SUCCESS if successful, or an error code on failure.
		**/
		virtual sw_status_t			open()=0;


		/**
		*** @brief	Closes the socket.
		***
		*** This method closes the socket and causes any subsequent methods to
		*** fail (execpt for methods that query status or attempt to re-open
		*** the socket). The socket implementation will attempt a graceful shutdown
		*** of the socket connection (if any).
		***
		*** @note
		*** This method must be implemented by a deriving socket implementation.
		***
		*** @return	SW_STATUS_SUCCESS if successful, or an error code on failure.
		**/
		virtual sw_status_t			close()=0;

		/**
		*** @brief	Make a connection to the specified socket address.
		***
		*** This method opens the socket and attempts to make a connection
		*** to the specified socket address. The actual implementation of
		*** this will vary according to socket type. For example a TCP socket
		*** (SWTcpSocket) will attempt to establish a socket connection, but
		*** a UDP socket (SWUdpSocket) which is not stream-based may only record
		*** the address information for later use.
		***
		*** @param[in]	addr	The socket address to connect to.
		***
		*** @return				SW_STATUS_SUCCESS if successful, or an error code otherwise.
		**/
		virtual sw_status_t			connect(const SWSocketAddress &addr);

		/**
		*** @brief	Listen for a connection to the specified socket address.
		***
		*** This method attempts to set up the socket to listen for incoming
		*** connection requests.
		***
		*** @param[in]	addr		The socket address to accept connection requests from.
		***							The socket address contains address and port information
		***							and can be set to receive from any address on a specific 
		***							port.
		*** @param[in]	maxPending	This value sets the maximum number of pending connection
		***							request that can outstanding on this socket be the connection
		***							is actively refused by the socket. This value is typically
		***							limited to a relatively low value by the underlying O/S
		***							(A limit of 5 is not uncommon).
		***
		*** @return					A status code indicating the success or failure of the operation:
		***							- SW_STATUS_UNSUPPORTED_OPERATION if this socket does not support listening for connections.
		***							- SW_STATUS_SUCCESSFUL if a connection was successfully accepted.
		***							- An error code if some other failure occurred.
		**/
		virtual sw_status_t			listen(const SWSocketAddress &addr, int maxPending);

		/**
		*** Accept an incoming connection on a listening socket.
		***
		*** This method waits for an incoming connection request coming from
		*** another socket and then if successful creates a new SWSocket object
		*** to handle the new socket and returns this to the caller.
		***
		*** This method can be set to optionally timeout if a connection is not
		*** received within the specified amount of time.
		***
		*** @param[out]	pSocket	Receives a pointer to the newly created to socket.
		*** @param[in]	waitms	The number of milliseconds to wait for a connection
		***						before timing out.
		***
		*** @return				A status code indicating the success or failure of the operation:
		***						- SW_STATUS_UNSUPPORTED_OPERATION if this socket does not support listening for connections.
		***						- SW_STATUS_SUCCESSFUL if a connection was successfully accepted.
		***						- SW_STATUS_TIMEOUT if a timeout occurred.
		***						- An error code if some other failure occurred.
		**/
		virtual sw_status_t			accept(SWSocket *&pSocket, sw_uint64_t waitms=SW_TIMEOUT_INFINITE);

		/**
		*** @brief	Bind the socket to the specified socket address.
		***
		*** This method attempts to bind the socket to the specified socket address.
		***
		*** @param[in]	addr		The socket address to bind this socket to.
		***							The socket address contains address and port information
		***							and can be bound to any address on a specific port.
		***
		*** @return					A status code indicating the success or failure of the operation:
		***							- SW_STATUS_UNSUPPORTED_OPERATION if this socket does not support listening for connections.
		***							- SW_STATUS_SUCCESSFUL if a connection was successfully accepted.
		***							- An error code if some other failure occurred.
		**/
		virtual sw_status_t			bind(const SWSocketAddress &addr);

		/**
		*** Read data from a connected socket into the specified buffer.
		***
		*** This method reads data from a connected socket.
		***
		*** @param[in]	pBuffer		A pointer to a buffer that will receive the data.
		*** @param[in]	bytesToRead	The number of bytes to attempt to read from the socket.
		*** @param[out]	pBytesRead	A pointer which if non-NULL points to a
		***							sw_size_t variable which receives a count
		***							of the bytes actually read from the socket.
		***
		*** @return					A status code indicating the success or failure of the operation:
		***							- SW_STATUS_UNSUPPORTED_OPERATION if this socket does not support this operation.
		***							- SW_STATUS_SUCCESSFUL if the read was successful.
		***							- An error code if some other failure occurred.
		**/
		virtual sw_status_t			read(void *pBuffer, sw_size_t bytesToRead, sw_size_t *pBytesRead=NULL);

		/**
		*** Read available data from a connected socket into the specified buffer.
		***
		*** This method reads available data from a connected socket. If no data is available then
		*** none is returned.
		***
		*** @param[in]	pBuffer		A pointer to a buffer that will receive the data.
		*** @param[in]	bytesToRead	The number of bytes to attempt to read from the socket.
		*** @param[out]	pBytesRead	A pointer which if non-NULL points to a
		***							sw_size_t variable which receives a count
		***							of the bytes actually read from the socket.
		***
		*** @return					A status code indicating the success or failure of the operation:
		***							- SW_STATUS_UNSUPPORTED_OPERATION if this socket does not support this operation.
		***							- SW_STATUS_SUCCESSFUL if the read was successful.
		***							- An error code if some other failure occurred.
		**/
		virtual sw_status_t			receive(void *pBuffer, sw_size_t bytesToRead, sw_size_t *pBytesRead=NULL);

		/**
		*** @brief	Write data from the specified buffer to a connected socket.
		***
		*** This method writes data to a connected socket.
		***
		*** @param[in]	pBuffer			A pointer to the data to be written.
		*** @param[in]	bytesToWrite	The number of bytes of data to write.
		*** @param[out]	pBytesWritten	A pointer which if non-NULL points to a
		***								sw_size_t variable which receives a count
		***								of the bytes actually written down the socket.
		***
		*** @return					A status code indicating the success or failure of the operation:
		***							- SW_STATUS_UNSUPPORTED_OPERATION if this socket does not support this operation.
		***							- SW_STATUS_SUCCESSFUL if the write was successful.
		***							- An error code if some other failure occurred.
		**/
		virtual sw_status_t			write(const void *pBuffer, sw_size_t bytesToWrite, sw_size_t *pBytesWritten=NULL);

		/**
		*** @brief	Send data to an unconnected receiver.
		***
		*** This method writes data to the specified socket address. The socket does not
		*** need to be in a connected state in order to use this method.
		***
		*** @param[in]	addr			The socket address to send the data to.
		*** @param[in]	pBuffer			A pointer to the data to be written.
		*** @param[in]	bytesToWrite	The number of bytes of data to write.
		*** @param[out]	pBytesWritten	A pointer which if non-NULL points to a
		***								sw_size_t variable which receives a count
		***								of the bytes actually written down the socket.
		***
		*** @return					A status code indicating the success or failure of the operation:
		***							- SW_STATUS_UNSUPPORTED_OPERATION if this socket does not support this operation.
		***							- SW_STATUS_SUCCESSFUL if the send was successful.
		***							- An error code if some other failure occurred.
		**/
		virtual sw_status_t			sendTo(const SWSocketAddress &addr, const void *pBuffer, sw_size_t bytesToWrite, sw_size_t *pBytesWritten=NULL);

		/**
		*** @brief	Receive data fron an unconnected receiver.
		***
		*** This method reads data from the socket address. The specified
		*** addr parameter receives the address that the data was received from.
		***
		*** @param[out]	addr		Receives the socket address the data was sent from.
		*** @param[in]	pBuffer		A pointer to a buffer that will receive the data.
		*** @param[in]	bytesToRead	The number of bytes to attempt to read from the socket.
		*** @param[out]	pBytesRead	A pointer which if non-NULL points to a
		***							sw_size_t variable which receives a count
		***							of the bytes actually read from the socket.
		***
		*** @return					A status code indicating the success or failure of the operation:
		***							- SW_STATUS_UNSUPPORTED_OPERATION if this socket does not support this operation.
		***							- SW_STATUS_SUCCESSFUL if the read was successful.
		***							- An error code if some other failure occurred.
		***
		*** @note
		*** Due to the nature of UDP sockets (being packet oriented in nature) the amount of data
		*** actually read from the socket is normally less than that of the bytesToRead parameter.
		*** If there is more data in the packet than could be read into the buffer, the additional
		*** data would be lost.
		**/
		virtual sw_status_t			receiveFrom(SWSocketAddress &addr, void *pBuffer, sw_size_t bytesToRead, sw_size_t *pBytesRead=NULL);


		/**
		*** @brief	Return a boolean indicating if this socket is currently in an open state.
		**/
		virtual bool				isOpen() const;

		/**
		*** @brief	Return a boolean indicating if this socket is currently in an connected state.
		**/
		virtual bool				isConnected() const;

		/**
		*** @brief	Return a boolean indicating if this socket is currently listening for connections.
		**/
		virtual bool				isListening() const;

		/**
		*** @brief	Return a boolean indicating if this socket is currently bound
		**/
		virtual bool				isBound() const;

		/**
		*** @brief	Return a boolean indicating if this socket is stream based
		**/
		virtual bool				isStream() const;

		/**
		*** @brief	Gets the address associated with this socket if bound.
		**/
		const SWSocketAddress &		address() const				{ return _addr; }

		/**
		*** @brief	Gets the result of the last operation on this socket
		**/
		sw_status_t					getLastError() const		{ return _lasterror; }

		/**
		*** @brief	Set an option on the socket.
		***
		*** This method attempts to set the specified option on the socket.
		*** The actual value should be set according to the SocketOption list.
		*** The optval parameter is a SWValue object to allow a consistent interface
		*** to the setOption method.
		***
		*** @param[in]	option	The option to set.
		*** @param[in]	optval	The value to assign to the specified option.
		***
		*** @return				SW_STATUS_SUCCESSS if succesful, or an error
		***						code if the option fails to set.
		**/
		virtual sw_status_t			setOption(SocketOption option, const SWValue &optval);

		/**
		*** @brief	Get an option from the socket.
		***
		*** This method attempts to get the value of the specified option from the socket.
		*** The actual value should be interpreted according to the SocketOption list.
		*** The optval parameter is a SWValue object to allow a consistent interface
		*** to the getOption method.
		***
		*** @param[in]	option	The option to get.
		*** @param[out]	optval	Receives the value returned by a successful get.
		***
		*** @return				SW_STATUS_SUCCESSS if succesful, or an error
		***						code if the option fetch fails.
		**/
		virtual sw_status_t			getOption(SocketOption option, SWValue &optval);

		/**
		*** @brief	Wait for input on the socket
		***
		*** This method waits for input activity on the socket.
		***
		*** @param[out]	waitms	The time in milliseconds to wait for input before
		***						timeout occurs.
		***
		*** @return				SW_STATUS_SUCCESSS if succesful, or an error
		***						code if the option fetch fails.
		**/
		sw_status_t					waitForInput(sw_uint64_t waitms=SW_TIMEOUT_INFINITE);

protected: // methods

		/// Internal method to record the result of the last socket operation for later retrieval via the getLastError method.
		sw_status_t					setLastError(sw_status_t e)	{ _lasterror = e; return e; }

protected: // Static methods

/**
*** @name	Internal static socket methods
***
*** The following methods are wrappers to O/S socket API.
**/
//@{
		static sw_status_t		open(int domain, int type, int protocol, sw_sockhandle_t &sock);
		static sw_status_t		shutdown(sw_sockhandle_t socket, int how);
		static sw_status_t		close(sw_sockhandle_t socket);
		static sw_status_t		connect(sw_sockhandle_t sock, const SWSocketAddress &addr);
		static sw_status_t		bind(sw_sockhandle_t sock, const SWSocketAddress &addr);
		static sw_status_t		listen(sw_sockhandle_t sock, int maxPending);
		static sw_status_t		send(sw_sockhandle_t sock, const void *pBuffer, sw_size_t bytesToSend, sw_size_t *pBytesSent=NULL);
		static sw_status_t		recv(sw_sockhandle_t sock, void *pBuffer, sw_size_t bytesToReceive, sw_size_t *pBytesReceived=NULL, bool peek=false, bool waitForAllData=true);
		static sw_status_t		sendto(sw_sockhandle_t sock, const SWSocketAddress &addr, const void *pBuffer, sw_size_t bytesToSend, sw_size_t *pBytesSent=NULL);
		static sw_status_t		recvfrom(sw_sockhandle_t sock, SWSocketAddress &addr, void *pBuffer, sw_size_t bytesToReceive, sw_size_t *pBytesReceived=NULL, bool peek=false);
		static sw_status_t		accept(sw_sockhandle_t sock, sw_sockhandle_t &insock, SWSocketAddress &inaddr);
		static sw_status_t		setOption(sw_sockhandle_t sock, int optlevel, int optname, int optval);
		static sw_status_t		getOption(sw_sockhandle_t sock, int optlevel, int optname, int &optval);
		static sw_status_t		waitForInput(sw_sockhandle_t sock, sw_uint64_t waitms);
//@}

protected: // Members
		sw_sockhandle_t	_fd;		///< The file descriptor for the socket
		SWSocketAddress	_addr;		///< The socket address that is associated with this socket (if any)
		sw_status_t		_lasterror;	///< The last error code returned on this socket
		SWFlags32		_flags;		///< The socket flags (includes connection status)
		};


SW_DEFINE_EXCEPTION(SWSocketException, SWException);



#endif
