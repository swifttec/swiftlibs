/**
*** @file		SWStringArray.h
*** @brief		A string array
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWStringArray class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWStringArray_h__
#define __SWStringArray_h__

#include <swift/SWString.h>
#include <swift/SWAbstractArray.h>





/**
*** @brief	An array template based on the Array class.
***
*** An array of SWString objects based on the Array class.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWStringArray : public SWAbstractArray
		{
public:
		/**
		*** @brief	Construct an array of integers with optional initial size.
		***
		*** @param[in]	initialSize		Specifies the initial size (number of elements) of the array.
		**/
		SWStringArray(sw_size_t initialSize=0);

		/**
		*** @brief	Destructor
		**/
		virtual ~SWStringArray();


		/**
		*** @brief Copy constructor
		***
		*** The copy constructor creates an empty array and the calls the
		*** assignment operator to copy all of the data.
		***
		*** @param[in]	other	The array to copy.
		**/
		SWStringArray(const SWStringArray &other);


		/**
		*** @brief Assignment operator
		***
		*** The assignment operator will clear the existing data from
		*** the array using the clear method and copy all of the values
		*** from the array specified.
		***
		*** @param[in]	other	Th array to copy.
		**/
		SWStringArray &		operator=(const SWStringArray &other);


		/**
		*** @brief Returns the nth element growing the array as required
		***
		*** This method returns a reference to the nth element in the array
		*** which can be modified as required.
		*** If the index specified is greater than the number of elements in the
		*** array, the array is automatically extended to encompass the referenced
		*** element.
		***
		*** If the index specified is negative an exception is thrown.
		***
		*** @param[in]	i	The index of the element to access.
		**/
		SWString &			operator[](sw_index_t i);


		/**
		*** @brief Returns the nth element without growing the array
		***
		*** Gets the nth element without growing the array. If the index is out of bounds
		*** an exception is thrown rather than growing the array automatically. Provided
		*** for use when a const operation is required.
		***
		*** @param[in]	i	The index of the element to access.
		**/
		SWString &				get(sw_index_t i) const;


		/**
		*** @brief	Clear the array of all data and reset the size to zero.
		**/
		virtual void			clear();


		/**
		*** @brief Remove one or more elements from the array.
		***
		*** @param[in]	pos		The position to remove elements from.
		*** @param[in]	count	The number of elements to remove.
		**/
		virtual void			remove(sw_index_t pos, sw_size_t count=1);



		/**
		*** @brief	Add the specified value to the end of the array.
		***
		*** @param[in]	v		The value to add.
		**/
		void					add(const SWString &v);

		/**
		*** @brief	Add the specified value to the end of the array.
		***
		*** @param[in]	ids		The ID of the string resource to load and add.
		**/
		void					add(sw_resourceid_t ids);


		/**
		*** Add (insert) at the specified position in the array
		***
		*** @param[in]	ids		The ID of the string resource to load and add.
		*** @param[in]	pos		The position at which to insert the value.
		**/
		void					insert(sw_resourceid_t ids, sw_index_t pos);


		/**
		*** Add (insert) at the specified position in the array
		***
		*** @param[in]	v		The value to add.
		*** @param[in]	pos		The position at which to insert the value.
		**/
		void					insert(const SWString &v, sw_index_t pos);


		/**
		*** @brief	Push a value onto the end of the array
		***
		*** @param[in]	v	The value to add.
		**/
		void					push(const SWString &v);


		/**
		*** @brief	Pop and return the last element off of the array
		**/
		SWString				pop();


		/**
		*** Turn the the string array into a string with the elements separated 
		*** with the supplied separator character
		***
		*** @param sep		The separator char to be inserted between the elements
		*** @param squote	The char to use to prefix each element
		*** @param equote	The char to use to postfix each element
		*** @param meta		The char to prefix any quote string with
		***
		*** @return The assembled string
		**/
		SWString	convertToString(int sep=',', int squote='"', int equote='"', int meta='\\') const;

		/**
		*** Take the string given and load the array from it by successive
		*** calls to add.
		***
		*** @param	s	The string to be split.
		*** @param	sep	The separator character (default ',')
		*** @param	squote	The start-of-quoted string character (default '"')
		*** @param	equote	The end-of-quoted string character (default '"')
		*** @param	meta	The meta character which can be used to prefix separator and quote chars. (default '\')
		*** @param	append	A flag to indicate if the strings should be appended to the array (true) or if the array 
		***			should be cleared before starting the split (false). The default action is to clear the
		***			array.
		**/
		void		convertFromString(const SWString &s, int sep=',', int squote='"', int equote='"', int meta='\\', bool append=false);


public: // Comparison operators

		/**
		*** @brief	Compare this object with another.
		***
		*** This method compares this object with the one specified and returns a
		*** result indicating if the other object is less than, equal to, or greater
		*** than this object.
		***
		*** - If this object is less than the specified object then a negative result is returned.
		*** - If this object is greater than the specified object then a positive result is returned.
		*** - If this object is equal to the specified object then a zero result is returned.
		***
		*** @note
		*** The compareTo method is not required to return -1 and +1 for the negative and positive
		*** values although this is typically the value returned. Callers should test for \< 0 and \> 0
		*** rather than for specific values of -1 and +1.
		***
		*** @param[in]	other	The data to compare this object to.
		**/
		int			compareTo(const SWStringArray &other) const;

		/**
		*** @brief	Test for equality
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is equal to the other object, false otherwise.
		**/
		bool	operator==(const SWStringArray &other) const	{ return (compareTo(other) == 0); }

		/**
		*** @brief	Test for inequality
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is not equal to the other object, false otherwise.
		**/
		bool	operator!=(const SWStringArray &other) const	{ return (compareTo(other) != 0); }

		/**
		*** @brief	Test for less than.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is less than the other object, false otherwise.
		**/
		bool	operator< (const SWStringArray &other) const	{ return (compareTo(other) <  0); }

		/**
		*** @brief	Test for greater than.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is greater than the other object, false otherwise.
		**/
		bool	operator> (const SWStringArray &other) const	{ return (compareTo(other) >  0); }

		/**
		*** @brief	Test for less than or equal to.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is less than or equal to the other object, false otherwise.
		**/
		bool	operator<=(const SWStringArray &other) const	{ return (compareTo(other) <= 0); }

		/**
		*** @brief	Test for greater than or equal to.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is greater than or equal to the other object, false otherwise.
		**/
		bool	operator>=(const SWStringArray &other) const	{ return (compareTo(other) >= 0); }


public: // Microsoft CStringArray methods
		void		Add(const SWString &s)		{ add(s); }
		SWString &	GetAt(sw_index_t i) const	{ return get(i); }

public: // virtual overrides

		/// Override for SWObject writeToStream
		virtual sw_status_t		writeToStream(SWOutputStream &stream) const;

		/// Override for SWObject readFromStream
		virtual sw_status_t		readFromStream(SWInputStream &stream);

protected:
		virtual void			ensureCapacity(sw_size_t nelems);
		};




#endif
