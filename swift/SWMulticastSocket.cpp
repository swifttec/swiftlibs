/**
*** @file		SWMulticastSocket.cpp
*** @brief		A TCP socket
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWMulticastSocket class.
**/

#include "stdafx.h"

#include <swift/SWGuard.h>

#include <swift/SWMulticastSocket.h>
#include <swift/sw_net.h>


#if !defined(SW_PLATFORM_WINDOWS)
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>
#else
	#include <winsock2.h>
	#include <ws2ipdef.h>
#endif


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif

SWMulticastSocket::SWMulticastSocket()
		{
		}


sw_status_t
SWMulticastSocket::addMembership(const SWIPAddress &ipaddr, const SWIPAddress &ifaddr)
		{
		SWGuard			guard(this);
		sw_status_t		res=SW_STATUS_SUCCESS;
		struct ip_mreq	group;

		/* Join the multicast group 226.1.1.1 on the local 203.106.93.94 */
		/* interface. Note that this IP_ADD_MEMBERSHIP option must be */
		/* called for each local interface over which the multicast */
		/* datagrams are to be received. */
		group.imr_multiaddr.s_addr = ipaddr; // inet_addr("226.1.1.1");
		group.imr_interface.s_addr = ifaddr;

		if (::setsockopt(_fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *)&group, sizeof(group)) < 0) res = sw_net_getLastError();

		return setLastError(res);
		}



sw_status_t
SWMulticastSocket::dropMembership(const SWIPAddress &ipaddr, const SWIPAddress &ifaddr)
		{
		SWGuard			guard(this);
		sw_status_t		res=SW_STATUS_SUCCESS;
		struct ip_mreq	group;

		/* interface. Note that this IP_ADD_MEMBERSHIP option must be */
		/* called for each local interface over which the multicast */
		/* datagrams are to be received. */
		group.imr_multiaddr.s_addr = ipaddr;
		group.imr_interface.s_addr = ifaddr;

		if (::setsockopt(_fd, IPPROTO_IP, IP_DROP_MEMBERSHIP, (char *)&group, sizeof(group)) < 0) res = sw_net_getLastError();

		return setLastError(res);
		}
