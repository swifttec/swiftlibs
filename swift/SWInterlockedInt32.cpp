/**
*** @file		SWInterlockedInt32.cpp
*** @brief		An interlocked 32-bit integer.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWInterlockedInt32 class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <swift/SWInterlockedInt32.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




SWInterlockedInt32::SWInterlockedInt32(sw_int32_t v)
		{
		*this = v;
		}

SWInterlockedInt32::~SWInterlockedInt32()
		{
		}


const sw_int32_t &
SWInterlockedInt32::operator=(sw_int32_t v)
		{
		SWGuard guard(this);
		m_value = v;
		return m_value;
		}

SWInterlockedInt32::operator sw_int32_t() const
		{
		SWGuard guard(this);
		return m_value;
		}

const sw_int32_t &
SWInterlockedInt32::operator++()
		{
		SWGuard guard(this);
		m_value++;
		return m_value;
		}

sw_int32_t
SWInterlockedInt32::operator++(int)	
		{
		SWGuard guard(this);
		sw_int32_t tmp=m_value;
		++m_value;
		return tmp;
		}

const sw_int32_t &
SWInterlockedInt32::operator--()
		{
		SWGuard guard(this);
		m_value--;
		return m_value;
		}

sw_int32_t
SWInterlockedInt32::operator--(int)
		{
		SWGuard guard(this);
		sw_int32_t tmp=m_value;
		--m_value;
		return tmp;
		}


