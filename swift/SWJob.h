/**
*** @file		SWJob.h
*** @brief		A class to represent an abstract job.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWJob class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWJob_h__
#define __SWJob_h__

#include <swift/SWObject.h>
#include <swift/SWThread.h>
#include <swift/SWList.h>

// Forward declarations
class SWJob;

const sw_uint32_t	SW_JOB_STARTED						= 0x00000001;	///< The job has been started
const sw_uint32_t	SW_JOB_RUNNING						= 0x00000002;	///< The job is running
const sw_uint32_t	SW_JOB_IGNORE_ERRORS				= 0x00000004;	///< Errors from sub-jobs should be ignored
const sw_uint32_t	SW_JOB_EXECUTE_SUBJOBS_FIRST		= 0x00000008;	///< Sub-jobs should be executed before any local job.
const sw_uint32_t	SW_JOB_RETURN_FIRST_ERROR			= 0x00000010;	///< The first encountered error should be returned by execute.
const sw_uint32_t	SW_JOB_FUNCTION						= 0x10000000;	///< The main job is a function
const sw_uint32_t	SW_JOB_ERROR						= 0x20000000;	///< The main job had an error
const sw_uint32_t	SW_JOB_SUBJOB_ERROR					= 0x40000000;	///< The one or more subjobs had an error
const sw_uint32_t	SW_JOB_DELETE						= 0x80000000;	///< The job should be deleted by the owner



/**
*** @brief A class to represent a job of work.
***
*** This class represents a single job of work. The SWJob can be used in a number
*** of different ways.
***
*** @par SWJob as an abstract job
*** In this mode a developer will create a class derived from the SWJob class. In
*** doing so they override the run method. This will be called when the job is executed.
***
*** @par SWJob as a function caller
*** In this mode a developer will instantiate a SWJob object directly, passing it the
*** job function (SWJobProc) to be called when the job is executed. The caller can also specify
*** a parameter (void *) to be passed to the job function when it is run.
***
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Safe</td>
*** </tr>
*** </table>
***
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWJob : public SWLockableObject
		{
public:
		/// An enumeration of notification codes
		enum NotificationCode
			{
			JobStarted,	///< The job has started
			JobStopped	///< The job has stopped
			};

		/// The job function specification used when providing a function for the job
		typedef sw_status_t (*JobProc) (void *);

		/// The job notification function specification used to provide job status feedback
		typedef void (*NotificationProc) (SWJob *, NotificationCode);

public:
		/// Default constructor (used when deriving from SWJob)
		SWJob(NotificationProc notify=NULL);

		/// Constructor used to provide a job function instead of a dervied class
		SWJob(JobProc fp, void *param, NotificationProc notify=NULL);

		/// Virtual destructor
		virtual ~SWJob();

		/**
		*** @brief	Add a sub-job
		**/
		void		add(SWJob *pJob, bool delflag);

		/**
		*** @brief	Execute the job.
		***
		*** This method should be called when the job is to be executed. The
		*** run method should not be called directly.
		**/
		sw_status_t	execute();

		/// Check to see if the job is currently running
		bool		isRunning() const	{ return ((m_flags & SW_JOB_RUNNING) != 0); }

		/// Set the notification function
		void		setNotificationFunction(NotificationProc notify);

		/// Set the delete flag
		void		setDeleteFlag(bool v);

		/// Set the delete flag
		bool		getDeleteFlag() const;

		/**
		*** @brief	Get the job status (only valid after execution).
		***
		*** This method returns the status of the job as returned by the
		*** execute method. If the job has been started it will return
		*** a status of SW_STATUS_JOB_NOT_EXECUTED. If the job is currently
		*** running it will return a status of SW_STATUS_JOB_RUNNING.
		***
		*** @return		The status as returned by the execute method or
		***				an error code as described.
		**/
		sw_status_t	status() const;

		/**
		*** @brief	Reset the job to a pre-run status
		***
		*** This method resets the job (and any sub-jobs) to a state as if
		*** it had not yet been run. The internal state flags have the RUNNING
		*** and STARTED attributes cleared and the status is set to 
		*** SW_STATUS_JOB_NOT_EXECUTED.
		***
		*** If this jobs is currently being execute no changes are made and an
		*** an error code is returned.
		***
		*** @return	SW_STATUS_SUCCESS if successful, or an errorcode otherwise.
		**/
		sw_status_t	reset();


		/**
		*** @brief	Set the execution order of the main job and any sub-jobs
		***
		*** @param[in]	subjobsFirst	If true then any subjobs are executed before the main
		***								job. If false then the main job is executed before
		***								any subjobs.
		**/
		void		setExecutionOrder(bool subjobsFirst);

		/**
		*** @brief	Set the ignore errors flag
		***
		*** This method sets the internal flag that specifies how errors from
		*** jobs are handled. If ignoreErrors is true then errors from the main
		*** and sub-jobs are ignored and all jobs and sub-jobs will be executed.
		*** If ignoreErrors is false (the default) then execution will stop on
		*** the first error encountered. An error id determined by the run method
		*** returning a status code other than SW_STATUS_SUCCESS.
		***
		*** @param[in]	ignoreErrors	Specifies is job execution errors are to be ignored.
		**/
		void		setIgnoreErrors(bool ignoreErrors);

		/**
		*** @brief	Set the error return flag (only applicable when ignoring errors)
		***
		*** This method is only applicable when using in conjunction with setting
		*** ignore errors to true. This flag specifies if the first or the last
		*** non-success error code is returned.
		***
		*** @param[in]	returnFirstError	If true the first non-success error code is
		***									returned. If false the last non-success error
		***									code is returned.
		**/
		void		setErrorReturn(bool returnFirstError);

protected: // Overrideable methods

		/**
		*** @brief	Virtual run method (can be overridden)
		***
		*** This method should be overridden by any class deriving from the SWJob class.
		*** It will be called by the execute method to perform the actual job execution.
		*** The method should return a status code indication if the execution was successful.
		*** A dervied class should return SW_STATUS_SUCCESS for a successful job execution or
		*** some other error code for unsuccessful execution.
		***
		*** The error code returned is checked by the execute method and is used to to set the
		*** recorded status code for the job. If the job is a sub-job of another job the status
		*** code is used in conjunction with various flags to determine if execution of following
		*** sub-jobs continues.
		***
		*** @see setExecutionOrder, setIgnoreErrors, setErrorReturn
		**/
		virtual sw_status_t	run();

private:
		/// Internal method to handle notification
		void		notify(NotificationCode code);

private:
		SWList<SWJob *>			*m_pJobList;	///< The list of sub-jobs
		sw_uint32_t				m_flags;		///< Various job flags
		sw_status_t				m_status;		///< The job status
		NotificationProc		m_notify;		///< The notification callback function
		};


/// The job function definition. Used when starting traditional style threads
typedef void (*SWJobNotificationProc) (SWJob *, SWJob::NotificationCode);







#endif
