#ifndef __swift_SWCircularBuffer_h__
#define __swift_SWCircularBuffer_h__

#include <swift/SWLockableObject.h>
#include <swift/SWList.h>
#include <swift/SWDataBuffer.h>

/**
*** @brief		A generic circular buffer class
**/
class SWIFT_DLL_EXPORT SWCircularBuffer : public SWLockableObject
		{
public:
		/// Queue read modes
		enum ReadMode
			{
			Consume=0,
			Peek=1
			};

public:
		/// Constructor
		SWCircularBuffer(int initialSize=16384, int stepSize=16384);
		
		/// Destructor
		virtual ~SWCircularBuffer();

		/// Clear the queue
		virtual void		clear();

		/// Read a single byte of data from the queue
		virtual bool		readByte(sw_byte_t &v, int mode=Consume);

		/// Read data from the queue into an SWDataBuffer object
		virtual bool		readData(SWDataBuffer &data, sw_size_t nbytes, int mode=Consume);

		/// Read data from the queue into the specified buffer
		virtual bool		readData(void *pData, sw_size_t nbytes, int mode=Consume);

		/// Read data from the queue into an SWDataBuffer object
		virtual bool		readAllData(SWDataBuffer &data, int mode=Consume);

		/// Drop the next nbytes of data from the queue
		virtual bool		dropData(sw_size_t nbytes);

		/// Write data to the queue
		virtual bool		writeData(void *pData, sw_size_t nbytes);

		/// Return the size of the data
		sw_size_t			datasize()	{ return m_datasize; }

		/// Ensure the queue can hold at least the specified number of bytes of data
		void				ensureCapacity(sw_size_t nbytes);

protected:
		sw_byte_t	*m_pData;	///< The queue data buffer
		int			m_bufsize;	///< The buffer size
		int			m_datasize;	///< The data size
		int			m_readpos;	///< The next read position
		int			m_writepos;	///< The next write position
		int			m_stepSize;	///< The buffer size step value 
		};

#endif // __swift_SWCircularBuffer_h__
