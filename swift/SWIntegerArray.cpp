/**
*** @file		SWIntegerArray.cpp
*** @brief		An array of Integers
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWIntegerArray class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <swift/SWException.h>
#include <swift/SWIntegerArray.h>
#include <swift/SWInputStream.h>
#include <swift/SWOutputStream.h>
#include <swift/SWGuard.h>


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





SWIntegerArray::SWIntegerArray(sw_size_t initialSize) :
    SWAbstractArray(NULL, sizeof(int))
		{
		ensureCapacity(initialSize);
		}


SWIntegerArray::~SWIntegerArray()
		{
		clear();
		}


/**
*** Get a reference to the nth element in the array.
***
*** If the index specified is negative an exception is thrown.
***
*** If the index specified is greater than the number of elements in the
*** array, the array is automatically extended to encompass the referenced
*** element.
**/
int &	
SWIntegerArray::operator[](int i)
		{
		if (i < 0) throw SW_EXCEPTION(SWArrayIndexOutOfBoundsException);
		ensureCapacity(i+1);
		if (i >= (int)m_nelems)
			{
			memset((int *)((void *)m_pData) + m_nelems, 0, (i + 1 - m_nelems) * sizeof(int));
			m_nelems = i+1;
			}

		int	*ip = (int *)((char *)m_pData + (i * sizeof(int)));

		return *ip;
		}

	
/**
*** Get a reference to the nth element in the array.
***
*** Throws an exception in the referenced element
*** does not exist.
**/
int &	
SWIntegerArray::get(int i) const
		{
		if (i < 0 || i >= (int)m_nelems) throw SW_EXCEPTION(SWArrayIndexOutOfBoundsException);

		int	*ip = (int *)((char *)m_pData + (i * sizeof(int)));

		return *ip;
		}

	
/**
*** Pops the last element off of the array
**/
int
SWIntegerArray::pop()
		{
		if (m_nelems < 1) throw SW_EXCEPTION(SWArrayIndexOutOfBoundsException);

		int	*ip = (int *)((char *)m_pData + ((--m_nelems) * sizeof(int)));

		return *ip;
		}


/**
*** Copy constructor
**/
SWIntegerArray::SWIntegerArray(const SWIntegerArray &ua) :
    SWAbstractArray(NULL, sizeof(int))
		{
		*this = ua;
		}


/**
*** Assignment operator
**/
SWIntegerArray &
SWIntegerArray::operator=(const SWIntegerArray &ua)
		{
		sw_size_t	n = ua.size();

		clear();

		// Do it backwards to prevent ensureCapacity from
		// doing multiple reallocs
		while (n > 0)
			{
			--n;
			(*this)[(int)n] = ua.get((int)n);
			}

		return *this;
		}


sw_status_t
SWIntegerArray::writeToStream(SWOutputStream &stream) const
		{
		sw_status_t	res;
		int			i;

		res = stream.write((sw_int32_t)m_nelems);
		for (i=0;res==SW_STATUS_SUCCESS&&i<(int)m_nelems;i++)
			{
			res = stream.write((sw_int32_t)get(i));
			}

		return res;
		}


sw_status_t
SWIntegerArray::readFromStream(SWInputStream &stream)
		{
		sw_status_t		res;
		sw_uint32_t		i, v, n;

		res = stream.read(n);
		if (res == SW_STATUS_SUCCESS)
			{
			clear();
			ensureCapacity(n);
			for (i=0;res==SW_STATUS_SUCCESS&&i<n;i++)
				{
				res = stream.read(v);
				if (res == SW_STATUS_SUCCESS) (*this)[i] = v;
				}
			}
		
		return res;
		}
	

int
SWIntegerArray::compareTo(const SWIntegerArray &other) const
		{
		SWReadGuard	guard(this);
		int			nelems1=(int)size();
		int			nelems2=(int)other.size();
		int			pos=0;
		int			res=0;
		int			v1, v2;

		while (res == 0 && pos < nelems1 && pos < nelems2)
			{
			v1 = get(pos);
			v2 = other.get(pos);

			if (v1 > v2) res = 1;
			else if (v1 < v2) res = -1;

			pos++;
			}

		if (res == 0)
			{
			if (nelems1 > nelems2) res = 1;
			else if (nelems1 < nelems2) res = -1;
			}

		return res;
		}


void
SWIntegerArray::insert(int v, sw_index_t pos)
		{
		insertElements(pos, 1);
		(*this)[pos] = v;
		}


void
SWIntegerArray::add(int v)
		{
		(*this)[(int)(m_nelems++)] = v;
		}


void
SWIntegerArray::push(int v)
		{
		(*this)[(int)(m_nelems++)] = v;
		}




