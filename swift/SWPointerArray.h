/**
*** @file		SWPointerArray.h
*** @brief		An array of Pointers
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWPointerArray class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWPointerArray_h__
#define __SWPointerArray_h__


#include <swift/SWAbstractArray.h>




/**
*** @brief	An array of pointers based on the Array class.
***
*** An array of pointers based on the Array class.
***
*** @par Attributes
***
*** @see		SWAbstractArray
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWPointerArray : public SWAbstractArray
		{
public:
		/**
		*** @brief	Construct a pointer array with an optional initial size and increment.
		***
		*** @param[in]	initialSize		The initial size of the array (default=0)
		*** @param[in]	incsize			The amount by which to grow the capacity of the array when a resize is required.
		**/
		SWPointerArray(sw_size_t initialSize=0);

		/// Virtual destructor
		virtual ~SWPointerArray();

		/// Copy constructor
		SWPointerArray(const SWPointerArray &ua);

		/// Assignment operator
		SWPointerArray &	operator=(const SWPointerArray &ua);

		/**
		*** @brief	return a reference to the nth element in the array.
		***
		*** If the index specified is negative an exception is thrown.
		*** If the index specified is greater than the number of elements in the
		*** array, the array is automatically extended to encompass the referenced
		*** element.
		***
		*** @param[in]	i	The index of the element to access.
		***
		*** @throws			SWArrayIndexOutOfBoundsException is the index is out of bounds.
		**/
		void * &	operator[](sw_index_t i);

		/**
		*** @brief	Get a reference to the nth element in the array.
		***
		*** This is similar to the index operator, but is a const operation
		*** and will not cause the array to grow in size. If the index
		*** specified is out of bounds an exception will be thrown.
		***
		*** @param[in]	i	The index of the element to access.
		***
		*** @throws			SWArrayIndexOutOfBoundsException is the index is out of bounds.
		**/
		void * &	get(sw_index_t i) const;

		/// Add to the end of the array
		void		add(void *v);

		/// Add (insert) at the specified position in the array
		void		insert(void *v, sw_index_t pos);

		/// Push a value onto the end of the array
		void		push(void *v);

		/// Pop a value from the end of the array
		void *		pop();


public: // Comparison operators

		/**
		*** @brief	Compare this object with another.
		***
		*** This method compares this object with the one specified and returns a
		*** result indicating if the other object is less than, equal to, or greater
		*** than this object.
		***
		*** - If this object is less than the specified object then a negative result is returned.
		*** - If this object is greater than the specified object then a positive result is returned.
		*** - If this object is equal to the specified object then a zero result is returned.
		***
		*** @note
		*** The compareTo method is not required to return -1 and +1 for the negative and positive
		*** values although this is typically the value returned. Callers should test for \< 0 and \> 0
		*** rather than for specific values of -1 and +1.
		***
		*** @param[in]	other	The data to compare this object to.
		**/
		int			compareTo(const SWPointerArray &other) const;

		/**
		*** @brief	Test for equality
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is equal to the other object, false otherwise.
		**/
		bool	operator==(const SWPointerArray &other) const	{ return (compareTo(other) == 0); }

		/**
		*** @brief	Test for inequality
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is not equal to the other object, false otherwise.
		**/
		bool	operator!=(const SWPointerArray &other) const	{ return (compareTo(other) != 0); }

		/**
		*** @brief	Test for less than.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is less than the other object, false otherwise.
		**/
		bool	operator< (const SWPointerArray &other) const	{ return (compareTo(other) <  0); }

		/**
		*** @brief	Test for greater than.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is greater than the other object, false otherwise.
		**/
		bool	operator> (const SWPointerArray &other) const	{ return (compareTo(other) >  0); }

		/**
		*** @brief	Test for less than or equal to.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is less than or equal to the other object, false otherwise.
		**/
		bool	operator<=(const SWPointerArray &other) const	{ return (compareTo(other) <= 0); }

		/**
		*** @brief	Test for greater than or equal to.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is greater than or equal to the other object, false otherwise.
		**/
		bool	operator>=(const SWPointerArray &other) const	{ return (compareTo(other) >= 0); }


public: // virtual overrides

		/// Override for SWObject writeToStream
		virtual sw_status_t		writeToStream(SWOutputStream &stream) const;

		/// Override for SWObject readFromStream
		virtual sw_status_t		readFromStream(SWInputStream &stream);
		};




#endif
