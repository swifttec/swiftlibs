/**
*** @file		SWSize.h
*** @brief		A class to represent a dimension or x/y size
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWSize class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWSize.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif

SWSize::SWSize(sw_uint32_t w, sw_uint32_t h) :
	m_width(w),
	m_height(h)
		{
		}


/// Copy constructor
SWSize::SWSize(const SWSize &other) :
	m_width(other.m_width),
	m_height(other.m_height)
		{
		}


/// Assignment operator
SWSize &
SWSize::operator=(const SWSize &other)
		{
		m_width = other.m_width;
		m_height = other.m_height;

		return *this;
		}


sw_status_t
SWSize::writeToStream(SWOutputStream &stream) const
		{
		sw_status_t	res;

		res = stream.write(m_width);
		if (res == SW_STATUS_SUCCESS) res = stream.write(m_height);

		return res;
		}


sw_status_t
SWSize::readFromStream(SWInputStream &stream)
		{
		sw_status_t	res;
		sw_int32_t	w=0, h=0;

		res = stream.read(w);
		if (res == SW_STATUS_SUCCESS) res = stream.read(h);

		if (res == SW_STATUS_SUCCESS)
			{
			m_width = w;
			m_height = h;
			}

		return res;
		}
