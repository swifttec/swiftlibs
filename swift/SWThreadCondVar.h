/**
*** @file		SWThreadCondVar.h
*** @brief		Thread specific version of a SWCondVar
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWThreadCondVar class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWThreadCondVar_h__
#define __SWThreadCondVar_h__

#include <swift/SWCondVar.h>
#include <swift/SWThreadMutex.h>




/**
*** @brief	Thread specific version of a SWCondVar.
***
*** Thread specific version of a SWCondVar.
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Safe</td>
*** </tr>
*** </table>
***
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWThreadCondVar : public SWCondVar
		{
public:
		/// Default constructor - creates own mutex for locking
		SWThreadCondVar();

		/// Virtual Destructor
		virtual ~SWThreadCondVar();

		/// constructor - uses supplied mutex for locking
		SWThreadCondVar(SWThreadMutex *m);

		/// constructor - uses supplied mutex for locking
		SWThreadCondVar(SWThreadMutex &m);
		};




#endif
