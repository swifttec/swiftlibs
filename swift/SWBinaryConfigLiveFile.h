/****************************************************************************
**	SWBinaryConfigFile.h	Class for handling an ini file (like the windows .ini files)
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2016 SwiftTec Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWBinaryConfigFile_h__
#define __SWBinaryConfigFile_h__

#include <swift/SWString.h>
#include <swift/SWVector.h>
#include <swift/SWValue.h>
#include <swift/SWPagedFile.h>
#include <swift/SWInputStream.h>
#include <swift/SWOutputStream.h>
#include <swift/SWList.h>
#include <swift/SWConfigFile.h>
#include <swift/SWConfigValue.h>

#if defined(SW_PLATFORM_WINDOWS)
	#pragma warning(disable: 4251)
#endif

/***********************************************************************
The binary config file holds 3 variable length records which are always
aligned to a 2 byte boundary to make processing easy:

Key Record
	Record Length		4 bytes
	Name Length		2 bytes
	Name Data		(rounded to 2 byte boundary)
	SubKey Count		2 bytes
	SubKey Pointers		4 bytes each
	Value Count		2 bytes
	Value Pointers		4 bytes each

Value Record
	Record Length		4 bytes
	Name Length		2 bytes
	Name Data		(rounded to 2 byte boundary)
	Type			2 bytes
	Data Length		2 bytes
	Data			(rounded to 2 byte boundary)
	
FreeSpace Record
	Record Length		4 bytes
	Prev Record Pointer	4 bytes
	Next Record Pointer	4 bytes
	
The file has the concept of "free space" which is created when gaps are
made by either:
* deleting values or keys
* updating variable length value with more space than currently available
  forcing the creation of a new value record
***********************************************************************/


// Extended value class with embedded disk record information for easy updates
class SWIFT_DLL_EXPORT SWBinaryConfigValue : public SWConfigValue
		{
friend class SWBinaryConfigFile;
friend class SWBinaryConfigKey;

protected:
		/// Default constructor
		SWBinaryConfigValue(SWConfigKey *owner, const SWString &name);

		void	setFileRecordInfo(int position, int length);
		int	getFileRecordPosition() const			{ return _fileRecordPosition; }
		int	getFileRecordLength() const			{ return _fileRecordLength; }

private:
		sw_int32_t	_fileRecordPosition;	// The file offset of the record on disk
		sw_int32_t	_fileRecordLength;	// The length of the record on disk
		};



// Extended key class with embedded disk record information for easy updates
class SWIFT_DLL_EXPORT SWBinaryConfigKey : public SWConfigKey
		{
friend class SWBinaryConfigFile;
friend class SWBinaryConfigValue;

		// Returns the named key, creates it if not found
		virtual SWConfigKey *		findOrCreateKey(const SWString &name);

		// Returns the named value, creates it if not found
		virtual SWConfigValue *		findOrCreateValue(const SWString &name);

protected:
		/// Default constructor
		SWBinaryConfigKey(SWConfigKey *owner, const SWString &name, bool ignoreCase);

		void	setFileRecordInfo(int position, int length);
		int	getFileRecordPosition() const			{ return _fileRecordPosition; }
		int	getFileRecordLength() const			{ return _fileRecordLength; }

private:
		sw_int32_t	_fileRecordPosition;	// The file offset of the record on disk
		sw_int32_t	_fileRecordLength;	// The length of the record on disk
		};



class SWIFT_DLL_EXPORT SWBinaryConfigFreeRecord : public SWObject
		{
friend class SWBinaryConfigFile;

protected:
		// Construct a free space record by reading the info from the file at the given position
		SWBinaryConfigFreeRecord(SWPagedFile &pf, int position);

		// Construct a new free space record
		SWBinaryConfigFreeRecord(int position, int length, int previous, int next);



		int	position() const	{ return _position; }
		int	length() const		{ return _length; }
		int	nextPosition() const	{ return _nextpos; }
		int	prevPosition() const	{ return _prevpos; }

		void	updateDiskRecord(SWPagedFile &pf);

		void	setPosition(int v)	{ _position = v; }
		void	setNextPosition(int v)	{ _nextpos = v; }
		void	setPrevPosition(int v)	{ _prevpos = v; }
		void	setLength(int v)	{ _length = v; }

private:
		sw_int32_t	_position;	// The offset of the free space record
		sw_int32_t	_length;	// The length of the free space record
		sw_int32_t	_nextpos;	// The position of the next free space record (0=tail)
		sw_int32_t	_prevpos;	// The position of the previous free space record (0=head)
		};


class SWIFT_DLL_EXPORT SWBinaryConfigFile : public SWConfigFile
		{
public:
		SWBinaryConfigFile(const TCHAR *filename, bool readonly=false, int pagesize=1024, bool ignoreCase=false);
		virtual ~SWBinaryConfigFile();

		/// Loads the internal structures from the file
		virtual bool			readFromFile(const SWString &filename);

		/// Saves the internal structures to the file
		virtual bool			writeToFile(const SWString &filename);

		// Returns the named key, creates it if not found
		virtual SWConfigKey *		findOrCreateKey(const SWString &name);

		// Returns the named value, creates it if not found
		virtual SWConfigValue *		findOrCreateValue(const SWString &name);

protected: // Methods

		// Sets the change flag
		virtual void			setChangedFlag(ChangeEventCode code, SWObject *obj);

		// Flushes any changes back to the file
		virtual void	flush();

private: // Methods
		bool				readFromFile(SWPagedFile &pf, bool create);

		SWString			readStringFromStream(SWInputStream &os, int pad=2);
		
		int				readKeyFromFile(SWPagedFile &pf, int pos, SWConfigKey *parent);
		int				readValueFromFile(SWPagedFile &pf, int pos, SWConfigKey *key);

		// Writes a key section to the open file
		int				writeKeyToFile(SWPagedFile &pf, int pos, int len, SWConfigKey *key, bool updateFlag=false);
		int				writeValueToFile(SWPagedFile &pf, int pos, int len, SWConfigValue *cv);
		void				writeValueToStream(SWOutputStream &mos, SWValue *cv);

		int				calculateRecordLength(SWConfigValue *value);
		int				calculateRecordLength(SWConfigKey *key);

		// Free space handling
		void				addFreeSpace(SWPagedFile &pf, int position, int size);
		void				findFreeSpace(SWPagedFile &pf, int wanted, int &position, int &size);
		void				removeFreeSpace(SWPagedFile &pf, SWBinaryConfigFreeRecord *frp);
		void				setFreeChainHead(SWPagedFile &pf, int v);
		void				setFreeChainTail(SWPagedFile &pf, int v);
		SWBinaryConfigFreeRecord *	findFreeChainPosition(int v, int t=0);
		void				dumpFreeSpaceList();


private: // Members
		SWPagedFile	_pagedFile;		// The paged file
		sw_int32_t		_rootKeyPos;		// The position of the root key
		sw_int32_t		_rootKeyLen;		// The record length of the root key
		sw_int32_t		_newWritePos;		// The write position for new data
		sw_int32_t		_pagesize;		// The size of the page to use with the paging file
		SWList<SWBinaryConfigFreeRecord *>	_freeChainList;		// A list of freespace records
		sw_int32_t		_freeChainHead;		// The head of free chain record position on disk
		sw_int32_t		_freeChainTail;		// The tail of free chain record position on disk
		};

#if defined(SW_PLATFORM_WINDOWS)
	#pragma warning(default: 4251)
#endif

// End the namespace
SWIFT_END_NAMESPACE

#endif
