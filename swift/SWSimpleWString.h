/**
*** @file		SWSimpleWString.h
*** @brief		Defines a class for a simple single-byte char string.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWSimpleWString class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWSimpleWString_h__
#define __SWSimpleWString_h__

#include <swift/swift.h>




/**
*** @brief	A simple single-byte character string
**/
class SWIFT_DLL_EXPORT SWSimpleWString
		{
public:
		/**
		*** @brief	Default constructor
		***
		*** Constructs a blank string.
		**/
		SWSimpleWString();

		/**
		*** @brief	Copy single-byte char string constructor
		***
		*** Constructs a string object from the supplied single-byte char string.
		**/
		SWSimpleWString(const char *sp);
		
		/**
		*** @brief	Copy single-byte char string constructor
		***
		*** Constructs a string object from the supplied single-byte char string.
		**/
		SWSimpleWString(const wchar_t *sp);
		
		/**
		*** @brief	Copy constructor
		***
		*** Constructs a string object from the supplied single-byte char string.
		**/
		SWSimpleWString(const SWSimpleWString &sp);

		/**
		*** @brief	Destructor
		**/
		~SWSimpleWString();

		/**
		*** @brief	Assignment operator(single-byte char string)
		**/
		SWSimpleWString &    operator=(const char *s);

		/**
		*** @brief	Assignment operator(single-byte char string)
		**/
		SWSimpleWString &    operator=(const wchar_t *s);

		/**
		*** @brief	Assignment operator(SWSimpleWString)
		**/
		SWSimpleWString &    operator=(const SWSimpleWString &s);


		/**
		*** @brief	Equality operator(single-byte char string)
		**/
		bool				 operator==(const wchar_t *s)		{ return (wcscmp(m_sp, s) == 0); }

		/**
		*** @brief	Equality operator(SWSimpleWString)
		**/
		bool				 operator==(const SWSimpleWString &s)	{ return (wcscmp(m_sp, s.m_sp) == 0); }

		/**
		*** @brief	Cast to single-byte char string.
		**/
		operator			const wchar_t *() const				{ return m_sp; }

		/**
		*** @brief	Return the length of the string.
		**/
		sw_size_t			length() const;

		/**
		*** @brief	Return the string character size.
		**/
		sw_size_t			charSize() const					{ return sizeof(*m_sp); }

private:
		wchar_t	*m_sp;	/// Points to the actual string data
		};





#endif // __SWSimpleWString_h__
