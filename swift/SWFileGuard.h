/**
*** @file		SWFileGuard.h
*** @brief		A generic file guard.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWFileGuard class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#ifndef __swift_SWFileGuard_h__
#define __swift_SWFileGuard_h__

#include <swift/SWFile.h>


/**
***	@brief	A generic file guard.
***	
*** This class locks the specified portion of a file on construction
*** by calling the synchronisation object's lockSection method. On
*** destruction the synchronisation object's unlockSection method is
*** called.
***
***	This object is useful for ensuring that any acquired synchronisation object
*** is release before exiting a function/method even if this is due to an execption
*** or multiple calls to return.
***
*** @author		Simon Sparkes
**/
class SWFileGuard
		{
public:
		/// Destructor
		~SWFileGuard()
				{
				m_pFile->unlockSection(m_offset, m_nbytes);
				}

		/// Guard for an File (pointer form)
		SWFileGuard(SWFile *obj, sw_filepos_t offset, sw_size_t nbytes)
				{
				m_pFile = obj;
				m_offset = offset;
				m_nbytes = nbytes;
				m_pFile->lockSection(m_offset, m_nbytes);
				}

		/// Guard for an File (reference form)
		SWFileGuard(SWFile &obj, sw_filepos_t offset, sw_size_t nbytes)
				{
				m_pFile = &obj;
				m_offset = offset;
				m_nbytes = nbytes;
				m_pFile->lockSection(m_offset, m_nbytes);
				}

private:
		SWFile		*m_pFile;	///< The object to be guarded
		sw_filepos_t m_offset;
		sw_size_t	m_nbytes;
		};

#endif // __swift_SWFileGuard_h__
