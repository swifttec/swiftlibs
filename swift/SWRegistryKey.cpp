/**
*** @file		SWRegistryKey.cpp
*** @brief		A generic value object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWRegistryKey class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <swift/SWRegistryKey.h>
#include <swift/SWFilename.h>
#include <swift/SWFolder.h>
#include <swift/sw_misc.h>
#include <swift/SWArray.h>
#include <swift/SWMemoryInputStream.h>
#include <swift/SWMemoryOutputStream.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif

#ifdef SW_REGISTRY_SUPPORT_EMULATED

class RegistryEmulator
		{
public:
		RegistryEmulator();
		~RegistryEmulator();

		sw_status_t	open(SWRegistryHive hive, const SWString &key, bool readonly);
		sw_status_t	open(const SWString &filename, bool readonly);
		sw_status_t	close();
		sw_status_t	lock();
		sw_status_t	unlock();
		sw_status_t	readValues(SWArray<SWNamedValue> &va);
		sw_status_t	writeValues(SWArray<SWNamedValue> &va);

public:
		static SWString	hiveFolder(SWRegistryHive hive);
		static SWString	hiveFolder(SWRegistryHive hive, const SWString &key);

protected:
		SWFile	m_file;
		bool	m_locked;
		};


SWString
RegistryEmulator::hiveFolder(SWRegistryHive hive)
		{
		SWString	hivepath;

		switch (hive)
			{
			case SW_REGISTRY_CURRENT_USER:
				{
				SWString	userdir;

				if (sw_misc_getEnvironmentVariable("HOME", userdir))
					{
					hivepath = SWFilename::concatPaths(userdir, "/.registry");
					}
				}
				break;
			
			case SW_REGISTRY_LOCAL_MACHINE:
				hivepath = "/var/registry";
				break;
			}
			
		return hivepath;
		}


SWString
RegistryEmulator::hiveFolder(SWRegistryHive hive, const SWString &key)
		{
		SWString	keypath(key), hivepath;

		keypath.MakeLower();

		return SWFilename::concatPaths(hiveFolder(hive), keypath);
		}



RegistryEmulator::RegistryEmulator() :
	m_locked(false)
		{
		}


RegistryEmulator::~RegistryEmulator()
		{
		if (m_locked) unlock();
		close();
		}


sw_status_t
RegistryEmulator::open(SWRegistryHive hive, const SWString &key, bool readonly)
		{
		return open(SWFilename::concatPaths(hiveFolder(hive, key), "__hivedata__"), readonly);
		}


sw_status_t
RegistryEmulator::open(const SWString &filename, bool readonly)
		{
		sw_status_t	res;

		if (readonly)
			{
			res = m_file.open(filename, SWFile::ModeBinary|SWFile::ModeReadOnly);
			}
		else
			{
			SWString	folder=SWFilename::dirname(filename);

			if (!SWFileInfo::isFolder(folder)) SWFolder::create(folder, true);

			res = m_file.open(filename, SWFile::ModeBinary|SWFile::ModeReadWrite|SWFile::ModeCreate);
			if (res != SW_STATUS_SUCCESS)
				{
				// Try read-only mode
				res = m_file.open(filename, SWFile::ModeBinary|SWFile::ModeReadOnly);
				}
			}
		
		// printf("registry open %s = %d\n", filename.c_str(), res);

		return res;
		}


sw_status_t
RegistryEmulator::close()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (m_file.isOpen())
			{
			res = m_file.close();
			}

		return res;
		}


sw_status_t
RegistryEmulator::lock()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (!m_locked)
			{
			res = m_file.lock(10000);
			if (res == SW_STATUS_SUCCESS) m_locked = true;
			}

		return res;
		}



sw_status_t
RegistryEmulator::unlock()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (m_locked)
			{
			res = m_file.unlock();
			if (res == SW_STATUS_SUCCESS) m_locked = false;
			}

		return res;
		}


sw_status_t
RegistryEmulator::readValues(SWArray<SWNamedValue> &va)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		bool		locked=m_locked;

		va.clear();
		if (!locked) res = lock();
		if (res == SW_STATUS_SUCCESS)
			{
			sw_size_t	nbytes=m_file.size();

			if (nbytes > sizeof(sw_int32_t))
				{
				sw_byte_t	*bp=new sw_byte_t[nbytes];

				res = m_file.lseek(0, SWFile::SeekSet);
				if (res == SW_STATUS_SUCCESS) res = m_file.read(bp, nbytes);

				if (!locked) unlock();

				if (res == SW_STATUS_SUCCESS)
					{
					SWMemoryInputStream	stream(bp, nbytes);
					sw_int32_t			nvalues=0;

					res = stream.read(nvalues);
					for (int i=0;res==SW_STATUS_SUCCESS&&i<nvalues;i++)
						{
						SWNamedValue	v;

						res = stream.read(v);
						if (res == SW_STATUS_SUCCESS) va.add(v);
						}
					}

				safe_delete_array(bp);
				}
			}

		return res;
		}


sw_status_t
RegistryEmulator::writeValues(SWArray<SWNamedValue> &va)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (m_file.isReadOnly())
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}
		else
			{
			SWMemoryOutputStream	stream;
			sw_int32_t				nvalues=(sw_int32_t)va.size();
			
			res = stream.write(nvalues);
			for (int i=0;res==SW_STATUS_SUCCESS&&i<nvalues;i++)
				{
				res = stream.write(va[i]);
				}

			bool		locked=m_locked;

			if (res == SW_STATUS_SUCCESS)
				{
				if (!locked) res = lock();

				if (res == SW_STATUS_SUCCESS)
					{
					sw_size_t	nbytes=stream.length();

					res = m_file.lseek(0, SWFile::SeekSet);
					if (res == SW_STATUS_SUCCESS) res = m_file.write(stream.data(), nbytes);
					if (res == SW_STATUS_SUCCESS) res = m_file.truncate(nbytes);

					if (!locked) unlock();
					}
				}
			}
		return res;
		}

#endif // SW_REGISTRY_SUPPORT_EMULATED

SWRegistryKey::SWRegistryKey() :
	m_pdk(SW_REGISTRY_LOCAL_MACHINE)
#ifdef SW_REGISTRY_SUPPORT_NATIVE
	, m_hKey(0)
#endif
		{
		}


SWRegistryKey::SWRegistryKey(SWRegistryHive pdk, const SWString &key, bool createflag) :
	m_pdk(pdk)
#ifdef SW_REGISTRY_SUPPORT_NATIVE
	, m_hKey(0)
#endif
		{
		open(pdk, key, createflag);
		}


#ifdef SW_REGISTRY_SUPPORT_NETWORK_ACCESS
SWRegistryKey::SWRegistryKey(const SWString &hostname, SWRegistryHive pdk, const SWString &key, bool createflag) :
	m_pdk(pdk)
#ifdef SW_REGISTRY_SUPPORT_NATIVE
	, m_hKey(0)
#endif
		{
		open(hostname, pdk, key, createflag);
		}
#endif // SW_REGISTRY_SUPPORT_NETWORK_ACCESS


SWRegistryKey::~SWRegistryKey()
		{
		close();
		}


SWRegistryKey::SWRegistryKey(const SWRegistryKey &other) :
	m_pdk(SW_REGISTRY_LOCAL_MACHINE)
#ifdef SW_REGISTRY_SUPPORT_NATIVE
	, m_hKey(0)
#endif
		{
		*this = other;
		}


SWRegistryKey &
SWRegistryKey::operator=(const SWRegistryKey &other)
		{
		close();
		if (other.isOpen())
			{
#ifdef SW_REGISTRY_SUPPORT_NETWORK_ACCESS
			open(other.m_hostname, other.m_pdk, other.m_name, false);
#else // SW_REGISTRY_SUPPORT_NETWORK_ACCESS
			open(other.m_pdk, other.m_name, false);
#endif // SW_REGISTRY_SUPPORT_NETWORK_ACCESS
			}

		return *this;
		}



bool
SWRegistryKey::isRemote() const
		{
#ifdef SW_REGISTRY_SUPPORT_NETWORK_ACCESS
		return !m_hostname.isEmpty();
#else // SW_REGISTRY_SUPPORT_NETWORK_ACCESS
		return false;
#endif // SW_REGISTRY_SUPPORT_NETWORK_ACCESS
		}


bool
SWRegistryKey::isLocal() const
		{
#ifdef SW_REGISTRY_SUPPORT_NETWORK_ACCESS
		return m_hostname.isEmpty();
#else // SW_REGISTRY_SUPPORT_NETWORK_ACCESS
		return true;
#endif // SW_REGISTRY_SUPPORT_NETWORK_ACCESS
		}


sw_status_t
SWRegistryKey::open(SWRegistryHive pdk, const SWString &keyname, bool createflag)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		SWString	key=SWFilename::fix(keyname, SWFilename::PathTypePlatform);

#ifdef SW_REGISTRY_SUPPORT_NATIVE
		HKEY	hive=0;
		HKEY	hResult=0;
		LONG	rc=0;

		switch (pdk)
			{
			case SW_REGISTRY_CLASSES_ROOT:	hive = HKEY_CLASSES_ROOT;	break;
			case SW_REGISTRY_CURRENT_USER:	hive = HKEY_CURRENT_USER;	break;
			case SW_REGISTRY_LOCAL_MACHINE:	hive = HKEY_LOCAL_MACHINE;	break;
			}

		DWORD	disp;
		
		// First try opening for read-write access
		rc = ::RegOpenKeyEx(hive, key, 0, KEY_ALL_ACCESS, &hResult);

		// If that fails try opening for read-only access (if not creating)
		if (rc != ERROR_SUCCESS && !createflag)
			{
			rc = RegOpenKeyEx(hive, key, 0, KEY_READ, &hResult);
			}

		// Try creating the key if requested
		if (rc != ERROR_SUCCESS && createflag)
			{
			rc = RegCreateKeyEx(hive, key, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hResult, &disp);
			}

		if (rc != ERROR_SUCCESS) 
			{
			res = rc;
			}
		else
			{
			m_hKey = hResult;
			}

#else // SW_REGISTRY_SUPPORT_NATIVE
		SWString	hivepath=RegistryEmulator::hiveFolder(pdk);

		if (!hivepath.isEmpty())
			{
			if (!SWFileInfo::isFolder(hivepath))
				{
				res = SWFolder::create(hivepath, true);
				}

			if (SWFileInfo::isFolder(hivepath))
				{
				res = SW_STATUS_SUCCESS;
				}
			else
				{
				res = SW_STATUS_NOT_FOUND;
				}
			}
		else
			{
			res = SW_STATUS_UNSUPPORTED_OPERATION;
			}
#endif // SW_REGISTRY_SUPPORT_NATIVE

		if (res == SW_STATUS_SUCCESS)
			{
			m_name = key;
#ifdef SW_REGISTRY_SUPPORT_NETWORK_ACCESS
			m_hostname.clear();
#endif // SW_REGISTRY_SUPPORT_NETWORK_ACCESS
			m_pdk = pdk;
			}

		return res;
		}



#ifdef SW_REGISTRY_SUPPORT_NETWORK_ACCESS
sw_status_t
SWRegistryKey::open(const SWString &hostname, SWRegistryHive pdk, const SWString &keyname, bool createflag)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		SWString	key=SWFilename::fix(keyname, SWFilename::PathTypePlatform);

#ifdef SW_REGISTRY_SUPPORT_NATIVE
		SWString	machine;
		const TCHAR	*mptr=NULL;

		if (!hostname.isEmpty())
			{
			if (!hostname.startsWith("\\\\"))  machine = "\\\\";
			machine += hostname;
			mptr = machine;
			}

		HKEY	hive=0;
		HKEY	hRegistry=0;
		HKEY	hResult=0;
		LONG	rc=0;

		switch (pdk)
			{
			case SW_REGISTRY_CLASSES_ROOT:	hive = HKEY_CLASSES_ROOT;	break;
			case SW_REGISTRY_CURRENT_USER:	hive = HKEY_CURRENT_USER;	break;
			case SW_REGISTRY_LOCAL_MACHINE:	hive = HKEY_LOCAL_MACHINE;	break;
			}

		rc = ::RegConnectRegistry(mptr, hive, &hRegistry);
		if (rc == ERROR_SUCCESS && hRegistry != 0)
			{
			DWORD	disp;
			
			// First try opening for read-write access
			rc = ::RegOpenKeyEx((HKEY)hRegistry, key, 0, KEY_ALL_ACCESS, &hResult);

			// If that fails try opening for read-only access (if not creating)
			if (rc != ERROR_SUCCESS && !createflag)
				{
				rc = RegOpenKeyEx((HKEY)hRegistry, key, 0, KEY_READ, &hResult);
				}

			// Try creating the key if requested
			if (rc != ERROR_SUCCESS && createflag)
				{
				rc = RegCreateKeyEx((HKEY)hRegistry, key, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hResult, &disp);
				}

			if (rc != ERROR_SUCCESS) 
				{
				res = rc;
				}
			else
				{
				m_hKey = hResult;
				}

			::RegCloseKey((HKEY)hRegistry);
			}
		else
			{
			res = rc;
			}
#else // SW_REGISTRY_SUPPORT_NATIVE
		res = SW_STATUS_UNSUPPORTED_OPERATION;
#endif // SW_REGISTRY_SUPPORT_NATIVE

		if (res == SW_STATUS_SUCCESS)
			{
			m_name = key;
			m_hostname = hostname;
			m_pdk = pdk;
			}

		return res;
		}
#endif // SW_REGISTRY_SUPPORT_NETWORK_ACCESS



bool
SWRegistryKey::isOpen() const
		{
		bool	res=false;

#ifdef SW_REGISTRY_SUPPORT_NATIVE
		res = (m_hKey != 0);
#else // SW_REGISTRY_SUPPORT_NATIVE
		res = (m_pdk != SW_REGISTRY_NONE && !m_name.isEmpty());
#endif // SW_REGISTRY_SUPPORT_NATIVE

		return res;
		}


sw_status_t
SWRegistryKey::close()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (isOpen())
			{
#ifdef SW_REGISTRY_SUPPORT_NATIVE
			::RegCloseKey((HKEY)m_hKey);
			m_hKey =0;
#endif // SW_REGISTRY_SUPPORT_NATIVE
#ifdef SW_REGISTRY_SUPPORT_NETWORK_ACCESS
			m_hostname.clear();
#endif // SW_REGISTRY_SUPPORT_NETWORK_ACCESS

			m_name.clear();
			m_pdk = SW_REGISTRY_NONE;
			}

		return res;
		}


sw_status_t
SWRegistryKey::flush()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (isOpen())
			{
#ifdef SW_REGISTRY_SUPPORT_NATIVE
			LONG		rc;

			rc = ::RegFlushKey((HKEY)m_hKey);
			if (rc != ERROR_SUCCESS)
				{
				res = rc;
				}
#else // SW_REGISTRY_SUPPORT_NATIVE
			// This has no meaning in the emulator
			res = SW_STATUS_SUCCESS;
#endif // SW_REGISTRY_SUPPORT_NATIVE
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}

		return res;
		}


sw_status_t
SWRegistryKey::keyNames(SWStringArray &sa)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		sa.clear();

		if (isOpen())
			{
#ifdef SW_REGISTRY_SUPPORT_NATIVE
			DWORD		maxNameLen=0;
			LONG		rc;
			SWString	str;
			
			rc = ::RegQueryInfoKey((HKEY)m_hKey, NULL, NULL, NULL, NULL, &maxNameLen, NULL, NULL, NULL, NULL, NULL, NULL);
			
			if (rc == ERROR_SUCCESS)
				{
				int		i;

				// Enumerate all the keys
				i = 0;
				rc = ERROR_SUCCESS;
				while (rc == ERROR_SUCCESS)
					{
					DWORD	namelen=maxNameLen+1;

					rc = ::RegEnumKey((HKEY)m_hKey, i++, (LPTSTR)str.lockBuffer(namelen, sizeof(sw_tchar_t)), namelen);
					str.unlockBuffer();

					if (rc == ERROR_SUCCESS) sa.add(str);
					}
				}
			else res = rc;
#else // SW_REGISTRY_SUPPORT_NATIVE
			SWFolder		folder;

			if (folder.open(RegistryEmulator::hiveFolder(m_pdk, m_name)) == SW_STATUS_SUCCESS)
				{
				SWFileInfo	entry;

				while (folder.read(entry) == SW_STATUS_SUCCESS)
					{
					if (entry.isFolder())
						{
						SWString	name=entry.getName();
						
						if (name != "." && name != "..") sa.add(name);
						}
					}

				folder.close();
				res = SW_STATUS_SUCCESS;
				}
			else
				{
				res = SW_STATUS_NOT_FOUND;
				}

#endif // SW_REGISTRY_SUPPORT_NATIVE
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}

		return res;
		}


sw_status_t
SWRegistryKey::valueNames(SWArray<SWString> &sa)
		{
		sw_status_t		res=SW_STATUS_SUCCESS;
		SWStringArray	ta;
		sa.clear();

		res = valueNames(ta);
		for (int i=0;i<(int)ta.size();i++) sa.add(ta[i]);
		
		return res;
		}


sw_status_t
SWRegistryKey::keyNames(SWArray<SWString> &sa)
		{
		sw_status_t		res=SW_STATUS_SUCCESS;
		SWStringArray	ta;
		sa.clear();

		res = keyNames(ta);
		for (int i=0;i<(int)ta.size();i++) sa.add(ta[i]);
		
		return res;
		}

sw_status_t
SWRegistryKey::valueNames(SWStringArray &sa)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		sa.clear();

		if (isOpen())
			{
#ifdef SW_REGISTRY_SUPPORT_NATIVE
			DWORD		maxNameLen=0;
			LONG		rc;
			SWString	str;
			
			rc = ::RegQueryInfoKey((HKEY)m_hKey, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &maxNameLen, NULL, NULL, NULL);
			
			if (rc == ERROR_SUCCESS)
				{
				int		i;

				// Enumerate all the values
				i = 0;
				rc = ERROR_SUCCESS;
				while (rc == ERROR_SUCCESS)
					{
					DWORD	namelen=maxNameLen+1;

					rc = ::RegEnumValue((HKEY)m_hKey, i++, (LPTSTR)str.lockBuffer(namelen, sizeof(sw_tchar_t)), &namelen, NULL, NULL, NULL, NULL);
					str.unlockBuffer();

					if (rc == ERROR_SUCCESS) sa.add(str);
					}
				}
			else res = rc;
#else // SW_REGISTRY_SUPPORT_NATIVE
			RegistryEmulator	emulator;

			res = emulator.open(m_pdk, m_name, true);
			if (res == SW_STATUS_SUCCESS)
				{
				SWArray<SWNamedValue>	va;

				res = emulator.readValues(va);
				emulator.close();

				if (res == SW_STATUS_SUCCESS)
					{
					for (int i=0;i<(int)va.size();i++)
						{
						sa.add(va[i].name());
						}
					}
				}

#endif // SW_REGISTRY_SUPPORT_NATIVE
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}

		return res;
		}


bool
SWRegistryKey::valueExists(const SWString &name)
		{
		bool	res=false;

		if (isOpen())
			{
#ifdef SW_REGISTRY_SUPPORT_NATIVE
			LONG		rc;
			SWString	str;
			
			rc = ::RegQueryValueEx((HKEY)m_hKey, name, NULL, NULL, NULL, NULL);
			res = (rc == ERROR_SUCCESS);
#else // SW_REGISTRY_SUPPORT_NATIVE
			RegistryEmulator	emulator;
			sw_status_t			r;

			r = emulator.open(m_pdk, m_name, true);
			if (r == SW_STATUS_SUCCESS)
				{
				SWArray<SWNamedValue>	va;

				r = emulator.readValues(va);
				emulator.close();

				if (r == SW_STATUS_SUCCESS)
					{
					for (int i=0;i<(int)va.size();i++)
						{
						if (va[i].name().compareNoCase(name) == 0)
							{
							res = true;
							break;
							}
						}
					}
				}
#endif // SW_REGISTRY_SUPPORT_NATIVE
			}

		return res;
		}


SWRegistryValue
SWRegistryKey::getValue(const SWString &name)
		{
		SWRegistryValue	v;

		getValue(name, v);

		return v;
		}


sw_status_t
SWRegistryKey::getValue(const SWString &name, SWRegistryValue &value)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		value.clear();

		if (isOpen())
			{
#ifdef SW_REGISTRY_SUPPORT_NATIVE
			DWORD		vtype, maxDataLen;
			LONG		rc;
			SWString	str;
			
			// First of all get the maximum data size required
			rc = ::RegQueryInfoKey((HKEY)m_hKey, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &maxDataLen, NULL, NULL);
			if (rc == ERROR_SUCCESS)
				{
				DWORD		datalen=maxDataLen;
				SWBuffer	data;

				data.data(datalen);
				rc = ::RegQueryValueEx((HKEY)m_hKey, name, NULL, &vtype, data, &datalen);

				if (rc == ERROR_SUCCESS)
					{
					value.setFromWindowsRegistryData(name, vtype, data, datalen);
					}
				else
					{
					res = rc;
					}
				}
			else
				{
				res = rc;
				}
#else // SW_REGISTRY_SUPPORT_NATIVE
			RegistryEmulator	emulator;

			res = emulator.open(m_pdk, m_name, true);
			if (res == SW_STATUS_SUCCESS)
				{
				SWArray<SWNamedValue>	va;

				res = emulator.readValues(va);
				emulator.close();

				if (res == SW_STATUS_SUCCESS)
					{
					res = SW_STATUS_NOT_FOUND;
					for (int i=0;i<(int)va.size();i++)
						{
						if (va[i].name().compareNoCase(name) == 0)
							{
							res = SW_STATUS_SUCCESS;
							value = va[i];
							break;
							}
						}
					}
				}
#endif // SW_REGISTRY_SUPPORT_NATIVE
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}

		return res;
		}


sw_status_t
SWRegistryKey::setValue(const SWRegistryValue &value)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (isOpen())
			{
#ifdef SW_REGISTRY_SUPPORT_NATIVE
			LONG		rc;
			
			switch (value.regtype())
				{
				case SWRegistryValue::RtNone:				
					rc = ::RegSetValueEx((HKEY)m_hKey, value.name(), 0, REG_NONE, NULL, 0);
					break;

				case SWRegistryValue::RtString:
					{
					SWString	s=value.toString();

					rc = ::RegSetValueEx((HKEY)m_hKey, value.name(), 0, REG_SZ, (BYTE *)(const sw_tchar_t *)s, (DWORD)(s.length()+1)*sizeof(sw_tchar_t));
					}
					break;

				case SWRegistryValue::RtExpandableString:
					{
					SWString	s=value.toString();

					rc = ::RegSetValueEx((HKEY)m_hKey, value.name(), 0, REG_EXPAND_SZ, (BYTE *)(const sw_tchar_t *)s, (DWORD)(s.length()+1)*sizeof(sw_tchar_t));
					}
					break;

				case SWRegistryValue::RtBinary:
					rc = ::RegSetValueEx((HKEY)m_hKey, value.name(), 0, REG_BINARY, (BYTE *)value.data(), (DWORD)value.length());
					break;

				case SWRegistryValue::RtMultiString:
					rc = ::RegSetValueEx((HKEY)m_hKey, value.name(), 0, REG_MULTI_SZ, (BYTE *)value.data(), (DWORD)value.length());
					break;

				case SWRegistryValue::RtInt32:
					{
					sw_int32_t	v=value.toInt32();

					rc = ::RegSetValueEx((HKEY)m_hKey, value.name(), 0, REG_DWORD, (BYTE *)&v, (DWORD)sizeof(v));
					}
					break;

				case SWRegistryValue::RtInt64:
					{
					sw_int64_t	v=value.toInt64();

					rc = ::RegSetValueEx((HKEY)m_hKey, value.name(), 0, REG_QWORD, (BYTE *)&v, (DWORD)sizeof(v));
					}
					break;

				default:
					rc = SW_STATUS_INVALID_PARAMETER;
					break;
				}

			if (rc != ERROR_SUCCESS)
				{
				res = rc;
				}

#else // SW_REGISTRY_SUPPORT_NATIVE
			RegistryEmulator	emulator;

			res = emulator.open(m_pdk, m_name, false);
			if (res == SW_STATUS_SUCCESS)
				{
				SWArray<SWNamedValue>	va;

				res = emulator.lock();
				if (res == SW_STATUS_SUCCESS)
					{
					res = emulator.readValues(va);

					if (res == SW_STATUS_SUCCESS)
						{
						bool	found=false;

						for (int i=0;i<(int)va.size();i++)
							{
							if (va[i].name().compareNoCase(value.name()) == 0)
								{
								found = true;
								va[i] = value;
								break;
								}
							}

						if (!found) va.add(value);
						}
					
					res = emulator.writeValues(va);
					emulator.unlock();
					}
				
				emulator.close();
				}
#endif // SW_REGISTRY_SUPPORT_NATIVE
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}

		return res;
		}


sw_status_t
SWRegistryKey::deleteValue(const SWString &name)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (isOpen())
			{
#ifdef SW_REGISTRY_SUPPORT_NATIVE
			LONG		rc;

			rc = ::RegDeleteValue((HKEY)m_hKey, name);
			if (rc != ERROR_SUCCESS)
				{
				res = rc;
				}
#else // SW_REGISTRY_SUPPORT_NATIVE
			RegistryEmulator	emulator;

			res = emulator.open(m_pdk, m_name, false);
			if (res == SW_STATUS_SUCCESS)
				{
				SWArray<SWNamedValue>	va;

				res = emulator.lock();
				if (res == SW_STATUS_SUCCESS)
					{
					res = emulator.readValues(va);

					if (res == SW_STATUS_SUCCESS)
						{
						res = SW_STATUS_NOT_FOUND;

						for (int i=0;i<(int)va.size();i++)
							{
							if (va[i].name().compareNoCase(name) == 0)
								{
								res = SW_STATUS_SUCCESS;
								va.remove(i);
								break;
								}
							}
						}
					
					if (res == SW_STATUS_SUCCESS) res = emulator.writeValues(va);
					emulator.unlock();
					}
				
				emulator.close();
				}
#endif // SW_REGISTRY_SUPPORT_NATIVE
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}

		return res;
		}


sw_status_t
SWRegistryKey::renameValue(const SWString &oldname, const SWString &newname)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (isOpen())
			{
#ifdef SW_REGISTRY_SUPPORT_NATIVE
			if (valueExists(newname))
				{
				// The new name already exists
				res = SW_STATUS_ALREADY_EXISTS;
				}
			else
				{
				DWORD		vtype, maxDataLen;
				LONG		rc;
				SWString	str;
			
				// First of all get the maximum data size required
				rc = ::RegQueryInfoKey((HKEY)m_hKey, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, &maxDataLen, NULL, NULL);
				if (rc == ERROR_SUCCESS)
					{
					DWORD		datalen=maxDataLen;
					SWBuffer	data;

					data.data(datalen);
					rc = ::RegQueryValueEx((HKEY)m_hKey, oldname, NULL, &vtype, data, &datalen);
					if (rc == ERROR_SUCCESS)
						{
						rc = ::RegSetValueEx((HKEY)m_hKey, newname, 0, REG_QWORD, (BYTE *)data, (DWORD)datalen);

						if (rc == ERROR_SUCCESS)
							{
							rc = ::RegDeleteValue((HKEY)m_hKey, oldname);
							}
						}
					}

				if (rc != ERROR_SUCCESS) res = rc;
				}
#else // SW_REGISTRY_SUPPORT_NATIVE
			RegistryEmulator	emulator;

			res = emulator.open(m_pdk, m_name, false);
			if (res == SW_STATUS_SUCCESS)
				{
				SWArray<SWNamedValue>	va;

				res = emulator.lock();
				if (res == SW_STATUS_SUCCESS)
					{
					res = emulator.readValues(va);

					if (res == SW_STATUS_SUCCESS)
						{
						int		idxOldName=-1, idxNewName=-1;

						for (int i=0;i<(int)va.size();i++)
							{
							if (va[i].name().compareNoCase(oldname) == 0)
								{
								idxOldName = i;
								}
							if (va[i].name().compareNoCase(newname) == 0)
								{
								idxNewName = i;
								}
							}

						if (idxNewName >= 0)
							{
							res = SW_STATUS_ALREADY_EXISTS;
							}
						else if (idxOldName < 0)
							{
							res = SW_STATUS_NOT_FOUND;
							}
						else
							{
							va[idxOldName].name(newname);
							}
					
						res = emulator.writeValues(va);
						}
					emulator.unlock();
					}
				
				emulator.close();
				}
#endif // SW_REGISTRY_SUPPORT_NATIVE
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}

		return res;
		}


bool
SWRegistryKey::keyExists(const SWString &name)
		{
		bool	res=false;

		if (isOpen())
			{
#ifdef SW_REGISTRY_SUPPORT_NATIVE
			LONG		rc;
			HKEY		hk;
			
			rc = ::RegOpenKeyEx((HKEY)m_hKey, name, 0, KEY_READ, &hk);
			if (rc == ERROR_SUCCESS)
				{
				res = true;
				::RegCloseKey(hk);
				}
#else // SW_REGISTRY_SUPPORT_NATIVE
			res = SWFileInfo::isFolder(SWFilename::concatPaths(RegistryEmulator::hiveFolder(m_pdk, m_name), name));
#endif // SW_REGISTRY_SUPPORT_NATIVE
			}

		return res;
		}


sw_status_t
SWRegistryKey::createKey(const SWString &name, SWRegistryKey &key)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (isOpen())
			{
			key.close();

#ifdef SW_REGISTRY_SUPPORT_NATIVE
			LONG		rc;
			HKEY		hk;
			
			rc = ::RegCreateKeyEx((HKEY)m_hKey, name, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hk, NULL);
			if (rc == ERROR_SUCCESS)
				{
				key.m_name = SWFilename::concatPaths(m_name, name);
				key.m_hKey = hk;
				}
			else
				{
				res = rc;
				}
#else // SW_REGISTRY_SUPPORT_NATIVE
			res = key.open(m_pdk, SWFilename::concatPaths(m_name, name), true);
#endif // SW_REGISTRY_SUPPORT_NATIVE
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}

		return res;
		}


sw_status_t
SWRegistryKey::openKey(const SWString &name, SWRegistryKey &key)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (isOpen())
			{
			key.close();

#ifdef SW_REGISTRY_SUPPORT_NATIVE
			LONG		rc;
			HKEY		hk;
			
			rc = ::RegOpenKeyEx((HKEY)m_hKey, name, 0, KEY_ALL_ACCESS, &hk);
			if (rc != ERROR_SUCCESS) rc = ::RegOpenKeyEx((HKEY)m_hKey, name, 0, KEY_READ, &hk);
			if (rc == ERROR_SUCCESS)
				{
				key.m_name = SWFilename::concatPaths(m_name, name);
				key.m_hKey = hk;
				key.m_pdk = m_pdk;
				}
			else
				{
				res = rc;
				}
#else // SW_REGISTRY_SUPPORT_NATIVE
			res = key.open(m_pdk, SWFilename::concatPaths(m_name, name), false);
#endif // SW_REGISTRY_SUPPORT_NATIVE
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}

		return res;
		}


sw_status_t
SWRegistryKey::deleteKey(const SWString &name)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (isOpen())
			{
#ifdef SW_REGISTRY_SUPPORT_NATIVE
			LONG		rc;

			rc = ::RegDeleteKey((HKEY)m_hKey, name);
			if (rc != ERROR_SUCCESS)
				{
				// Try deleting all subkeys and then try the delete again
				SWRegistryKey   sk;

				res = openKey(name, sk);
				if (res == SW_STATUS_SUCCESS)
					{
					SWArray<SWString>	sa;

					sk.keyNames(sa);
					for (int i=0;res == SW_STATUS_SUCCESS && i<(int)sa.size();i++)
						{
						res = sk.deleteKey(sa[i]);
						}
					}

				rc = ::RegDeleteKey((HKEY)m_hKey, name);
				if (rc != ERROR_SUCCESS) res = rc;
				}
#else // SW_REGISTRY_SUPPORT_NATIVE
			SWString	folder=SWFilename::concatPaths(RegistryEmulator::hiveFolder(m_pdk, m_name), name);
			
			res = SWFolder::remove(folder, true);
#endif // SW_REGISTRY_SUPPORT_NATIVE
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}

		return res;
		}


#ifdef SW_REGISTRY_SUPPORT_NATIVE
static DWORD
windows_copyKeysAndValues(HKEY from, HKEY to)
		{
		DWORD	maxValueLen=0, maxDataLen=0, maxKeyLen=0;
		LONG	rc;
		
		rc = RegQueryInfoKey(from, NULL, NULL, NULL, NULL, &maxKeyLen, NULL, NULL, &maxValueLen, &maxDataLen, NULL, NULL);
		if (rc == ERROR_SUCCESS)
			{
			SWBuffer	buf;

			buf.data(maxDataLen+1);

			BYTE	*pBuffer=buf;
			int		i;

			// Copy all the values
			i = 0;
			rc = ERROR_SUCCESS;
			while (rc == ERROR_SUCCESS)
				{
				SWString	value;
				DWORD		type=0, vlen=maxValueLen+1, buflen=maxDataLen+1;

				rc = RegEnumValue(from, i++, (LPTSTR)value.lockBuffer(vlen, sizeof(sw_tchar_t)), &vlen, NULL, &type, pBuffer, &buflen);
				value.unlockBuffer();

				if (rc == ERROR_SUCCESS)
					{
					rc = RegSetValueEx(to, value, NULL, type, pBuffer, buflen);
					}
				}

			// Copy the keys
			i = 0;
			rc = ERROR_SUCCESS;
			while (rc == ERROR_SUCCESS)
				{
				SWString	key;
				DWORD		klen=maxKeyLen+1;

				rc = RegEnumKey(from, i++, (LPTSTR)key.lockBuffer(klen, sizeof(sw_tchar_t)), klen);
				key.unlockBuffer();

				if (rc == ERROR_SUCCESS)
					{
					HKEY	fromsub, tosub;

					rc = RegOpenKeyEx(from, key, 0, KEY_ALL_ACCESS, &fromsub);
					if (rc == ERROR_SUCCESS)
						{
						DWORD	disp=0;

						rc = RegCreateKeyEx(to, key, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &tosub, &disp);
						if (rc == ERROR_SUCCESS)
							{
							rc = windows_copyKeysAndValues(fromsub, tosub);
							RegCloseKey(tosub);
							}

						RegCloseKey(fromsub);
						}
					}
				}
			}

		if (rc == ERROR_NO_MORE_ITEMS) rc = ERROR_SUCCESS;

		return rc;
		}
#endif // SW_REGISTRY_SUPPORT_NATIVE
 

sw_status_t
SWRegistryKey::renameKey(const SWString &oldname, const SWString &newname)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (keyExists(newname))
			{
			// The new name already exists
			res = SW_STATUS_ALREADY_EXISTS;
			}
		else if (isOpen())
			{
#ifdef SW_REGISTRY_SUPPORT_NATIVE
			HKEY	from, to;
			DWORD	rc;

			rc = ::RegOpenKeyEx((HKEY)m_hKey, oldname, 0, KEY_ALL_ACCESS, &from);
			if (rc == ERROR_SUCCESS)
				{
				rc = ::RegOpenKeyEx((HKEY)m_hKey, newname, 0, KEY_ALL_ACCESS, &to);
				if (rc != ERROR_SUCCESS)
					{
					DWORD	disp=0;

					rc = RegCreateKeyEx((HKEY)m_hKey, newname, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &to, &disp);
					if (rc == ERROR_SUCCESS)
						{
						rc = windows_copyKeysAndValues(from, to);
						::RegCloseKey(to);
						}
					}
				else
					{
					::RegCloseKey(to);
					}

				::RegCloseKey(from);
				}

			if (rc == ERROR_SUCCESS)
				{
				res = deleteKey(oldname);
				}
			else
				{
				res = rc;
				}
#else // SW_REGISTRY_SUPPORT_NATIVE
			res = SWFile::move(SWFilename::concatPaths(RegistryEmulator::hiveFolder(m_pdk, m_name), oldname), SWFilename::concatPaths(RegistryEmulator::hiveFolder(m_pdk, m_name), newname));
#endif // SW_REGISTRY_SUPPORT_NATIVE
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}

		return res;
		}


sw_status_t
SWRegistryKey::copyKeysAndValues(SWRegistryKey &other)
		{
		sw_status_t	res;

#ifdef SW_REGISTRY_SUPPORT_NATIVE
		res = windows_copyKeysAndValues((HKEY)other.m_hKey, (HKEY)m_hKey);
#else // SW_REGISTRY_SUPPORT_NATIVE
		res = SW_STATUS_UNSUPPORTED_OPERATION;
#endif // SW_REGISTRY_SUPPORT_NATIVE

		return res;
		}


SWString
SWRegistryKey::hiveAsString(SWRegistryHive hive)
		{
		SWString	res;

		switch (hive)
			{
			case SW_REGISTRY_CLASSES_ROOT:	res = "HKEY_CLASSES_ROOT";	break;
			case SW_REGISTRY_CURRENT_USER:	res = "HKEY_CURRENT_USER";	break;
			case SW_REGISTRY_LOCAL_MACHINE:	res = "HKEY_LOCAL_MACHINE";	break;
			}

		return res;
		}


SWRegistryHive
SWRegistryKey::hiveFromString(const SWString &name)
		{
		SWRegistryHive	res=SW_REGISTRY_CURRENT_USER;

		if (name.compareNoCase("HKEY_CLASSES_ROOT") == 0) res = SW_REGISTRY_CLASSES_ROOT;
		else if (name.compareNoCase("HKEY_CURRENT_USER") == 0) res = SW_REGISTRY_CURRENT_USER;
		else if (name.compareNoCase("HKEY_LOCAL_MACHINE") == 0) res = SW_REGISTRY_LOCAL_MACHINE;

		return res;
		}
