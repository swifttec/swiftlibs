/**
*** @file		SWIterator.h
*** @brief		Templated iterator class for walking templated collections
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWIterator class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWIterator_h__
#define __SWIterator_h__

#include <swift/SWAbstractIterator.h>



/**
*** @brief	Template form of SWIterator.
***
*** SWIterator is a templated extension of the SWIterator.
*** The primary difference between a SWIterator and a SWIterator is that the
*** templated SWIterator will correctly handle the SWCollection delete flag.
***
*** This iterator can be used with both templated and non-templated collections
*** as it simply performs a pointer cast internally.
***
*** Care should be excercised not to use a templated iterator of the wrong type.
*** i.e. A SWIterator<SWInteger> should not be used to iterate a collection
*** of type SWList<SWDouble>.
***
*** @see		SWIterator, SWCollection
*** @author		Simon Sparkes
**/
template<class T>
class SWIterator : public SWAbstractIterator
		{
public:
		/// Default constructor
		SWIterator();

		/// Copy constructor
		SWIterator(const SWIterator<T> &other);

		/// Assignment operator
		SWIterator<T> &	operator=(const SWIterator<T> &other);

		/**
		*** @brief	Returns the current element in the iteration of the collection.
		***
		*** This method returns a pointer to the data in the current position of the iterator.
		***
		*** <b>Care should be take when modifying data stored in an ordered collection
		*** as changing the hash value (for hash based collections) or the ordering
		*** value (for ordered collections) will prevent the collection from working
		*** correctly</b>
		***
		*** If there is no next element in the collection this method will throw
		*** a SWNoSuchElementException. If the collection does not support the 
		*** next operation this method will throw a SWUnsupportedOperationException.
		***
		*** @return		The data pointer stored in the next position in the collection.
		***
		*** @throws SWNoSuchElementException, SWUnsupportedOperationException
		**/
		T *			current();

		/**
		*** @brief	Returns the next element in the collection.
		***
		*** This method moves the iterator to point at the next element in the
		*** collection and then returns a pointer to the data in the collection.
		***
		*** <b>Care should be take when modifying data stored in an ordered collection
		*** as changing the hash value (for hash based collections) or the ordering
		*** value (for ordered collections) will prevent the collection from working
		*** correctly</b>
		***
		*** If there is no next element in the collection this method will throw
		*** a SWNoSuchElementException. If the collection does not support the 
		*** next operation this method will throw a SWUnsupportedOperationException.
		***
		*** @return		The data pointer stored in the next position in the collection.
		***
		*** @throws SWNoSuchElementException, SWUnsupportedOperationException
		**/
		T *			next();


		/**
		*** @brief	Returns the previous element in the collection.
		***
		*** This method moves the iterator to point at the previous element in the
		*** collection and then returns a pointer to the data stored in the collection.
		***
		*** <b>Care should be take when modifying data stored in an ordered collection
		*** as changing the hash value (for hash based collections) or the ordering
		*** value (for ordered collections) will prevent the collection from working
		*** correctly</b>
		***
		*** If there is no previous element in the collection this method will throw
		*** a SWNoSuchElementException. If the collection does not support the 
		*** previous operation this method will throw a SWUnsupportedOperationException.
		***
		*** @return		The data pointer stored in the previous position in the collection.
		***
		*** @throws SWNoSuchElementException, SWUnsupportedOperationException
		**/
		T *			previous();


		/**
		*** @brief	Adds the specified element into the collection.
		***
		*** This method will add the specified data into the collection
		*** at the position specified by the where parameter. If the collection
		*** is ordered the collection may ignore the where parameter when storing
		*** the data pointer.
		***
		*** If the collection does not support the add operation this method
		*** will throw a SWUnsupportedOperationException.
		***
		*** @param[in]	data	The data to insert into the collection.
		*** @param[in]	where	Specifies where the data will be added in relation to
		***						the current element.
		***
		*** @throws SWUnsupportedOperationException
		**/
		bool		add(const T &data, SWAbstractIterator::AddLocation where=AddBeforeOrAfter);


		/**
		*** @brief	Replace the current element in the collection.
		***
		*** Replaces the current element (the one returned by the last call to next or previous
		*** with the specified element.
		***
		*** If the collection does not support the set operation this method
		*** will throw a SWUnsupportedOperationException.
		***
		*** @param[in]	data		The data to insert into the collection.
		*** @param[out]	pOldData	If this parameter is not NULL it will receive
		***							the data that was removed from the collection.
		***
		***
		*** @return					true if successful, false otherwise.
		***
		*** @throws SWUnsupportedOperationException
		**/
		bool		set(const T &data, T *pOldData=NULL);
	

		/**
		*** @brief	Remove the current element from the collection.
		***
		*** This method removes from the underlying collection the current element,
		*** i.e. the last element returned by the iterator.
		*** After calling this method the current element will be invalid until
		*** either next or previous is called.
		***
		*** If the collection does not support the remove operation this method
		*** will throw a SWUnsupportedOperationException. If there is no current
		*** element this method will throw a SWNoSuchElementException.
		***
		*** @param[out]	pOldData	If this parameter is not NULL it will receive
		***							the data that was removed from the collection.
		***
		*** @return					true if successful, false otherwise.
		***
		*** @throws SWNoSuchElementException, SWUnsupportedOperationException
		**/
		bool		remove(T *pOldData=NULL);
		};




// Include the implementation
#include <swift/SWIterator.cpp>

#endif
