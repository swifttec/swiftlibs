/**
*** @file		SWRegistryValue.h
*** @brief		An object representing a single value contained in a registry key.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWRegistryValue class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWRegistryValue_h__
#define __SWRegistryValue_h__

#include <swift/swift.h>
#include <swift/SWNamedValue.h>



// Forward declarations

/**
*** @brief	An object representing a single value contained in a registry key.
***
*** This object is used to represent a value that has been extracted from the registry
*** or a value to be written to the registry. As all registry values have names this
*** class is dervied from the SWNamedValue class. It extends the SWNamedValue class by
*** adding to it a registry value type (enum RegValueType) which defines the type as
*** extracted from the registry, or that should be written to it.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWRegistryValue : public SWNamedValue
		{
		friend class SWRegistryKey;

public:
		/// An enumeration of supported registry types
		enum RegValueType
			{
			RtNone=0,			///< No value type
			RtString,			///< Unicode nul terminated string
			RtExpandableString,	///< Unicode nul terminated string (with environment variable references)
			RtBinary,			///< Free form binary
			RtMultiString,		///< Multiple Unicode strings
			RtInt32,			///< 32-bit number
			RtInt64,			///< 64-bit number
			RtSymbolicLink,		///< A symbolic link (stored as binary data)
			RtUnsupported		///< Any other native registry type (cannot be used to set values)
			};


public:
		/**
		*** @brief	Default constructor
		***
		*** The default constructor constructs an empty value which has the SWValue type VtNone,
		*** the registry type RtNone and a blank name.
		**/
		SWRegistryValue();

		/**
		*** @brief	Name constructor
		***
		*** The name constructor constructs an empty value which has the SWValue type VtNone,
		*** the registry type RtNone and the name as specified by the parameter.
		***
		*** @param[in]	name	The name of the registry value.
		**/
		SWRegistryValue(const SWString &name);

		/**
		*** @brief	Name and value constructor
		***
		*** The name constructor constructs a registry value which has the value and name
		*** as specified by the parameters.
		***
		*** @param[in]	name	The name of the registry value.
		*** @param[in]	regtype	The type to be used when storing the value in the registry.
		*** @param[in]	value	The data value.
		**/
		SWRegistryValue(const SWString &name, RegValueType regtype, const SWValue &value);

		/// Virtual destrcutor
		virtual ~SWRegistryValue();

		/// Copy constructor
		SWRegistryValue(const SWRegistryValue &other);

		/// @brief	Constructs a named value given the integer value and related integer type.
		SWRegistryValue(const SWString &name, int v, ValueType type);

		SWRegistryValue(const SWString &name, bool v);						///< Constructs a named value with the type VtNone
		SWRegistryValue(const SWString &name, sw_int8_t v);					///< Constructs a named value with the type VtInt8
		SWRegistryValue(const SWString &name, sw_int16_t v);				///< Constructs a named value with the type VtInt16
		SWRegistryValue(const SWString &name, sw_int32_t v);				///< Constructs a named value with the type VtInt16
		SWRegistryValue(const SWString &name, sw_int64_t v);				///< Constructs a named value with the type VtInt16
		SWRegistryValue(const SWString &name, sw_uint8_t v);				///< Constructs a named value with the type VtUInt8
		SWRegistryValue(const SWString &name, sw_uint16_t v);				///< Constructs a named value with the type VtUInt8
		SWRegistryValue(const SWString &name, sw_uint32_t v);				///< Constructs a named value with the type VtUInt8
		SWRegistryValue(const SWString &name, sw_uint64_t v);				///< Constructs a named value with the type VtUInt8
		SWRegistryValue(const SWString &name, float v);						///< Constructs a named value with the type VtFloat
		SWRegistryValue(const SWString &name, double v);					///< Constructs a named value with the type VtDouble
		SWRegistryValue(const SWString &name, const char *v);				///< Constructs a named value with the type VtString
		SWRegistryValue(const SWString &name, const wchar_t *v);			///< Constructs a named value with the type VtQString
		SWRegistryValue(const SWString &name, const void *, size_t len);	///< Constructs a named value with the type VtData from the data pointer and size
		SWRegistryValue(const SWString &name, SWBuffer &v);					///< Constructs a named value with the type VtData from SWBuffer

		/// Assignment operator
		SWRegistryValue &	operator=(const SWRegistryValue &other);

		/// Assignment Operator (copies both name and value)
		SWRegistryValue &	operator=(const SWNamedValue &v);

		/// Assignment from SWValue object (no name change - value only!)
		SWRegistryValue &	operator=(const SWValue &v);

		/// Returns the registry-specific type of this value
		RegValueType		regtype() const		{ return m_regtype; }

		virtual float			toFloat() const;									///< Convert this object to a float value.
		virtual double			toDouble() const;									///< Convert this object to a double value.

public: // STATIC methods

		/// Get the SWRegistryValue type for the given SWValue type
		static RegValueType		getRegValueTypeFromValueType(ValueType vt);


protected: // Methods

#if defined(SW_PLATFORM_WINDOWS)
		/**
		*** @brief	Special method for use with windows
		***
		*** @param[in]	name		The name of the value
		*** @param[in]	type		The windows registry data type (REG_DWORD, REG_SZ, etc)
		*** @param[in]	data		A pointer to the retrieved data
		*** @param[in]	datalen		The length of the data.
		**/
		void		setFromWindowsRegistryData(const SWString &name, DWORD type, const char *data, int datalen);
#endif

protected: // Members
		RegValueType	m_regtype;		///< The registry type of this value
		};


#endif
