/**
*** @file		SWTempFile.cpp
*** @brief		Basic temporary file handling class.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWTempFile class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




// Needed for precompiled header support
#include "stdafx.h"

#include <swift/swift.h>
#include <swift/SWTempFile.h>
#include <swift/SWFileInfo.h>

#include <swift/SWTString.h>
#include <swift/SWFilename.h>

#include <xplatform/sw_thread.h>
#include <xplatform/sw_process.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





/*
** Default constructor
*/
SWTempFile::SWTempFile(const SWString &dir, bool persistent, const SWString &prefix, const SWString &ext)
		{
		SWString	filename=getTempFileName(prefix, ext, dir);

		int	mode=ModeReadWrite | ModeBinary | ModeCreate | ModeTruncate | ModeTemporary;

		if (!persistent) mode |= ModeDeleteOnClose;
		open(filename, mode);
		}


/*
** Destructor
*/
SWTempFile::~SWTempFile()
		{
		}


/*
** Returns the name of a directory in which temporary files can be created.
*/
SWString
SWTempFile::getTempDirectory(const SWString &dir)
		{
		SWTString	tmpdir;

		if (dir.length() && SWFileInfo::isDirectory(dir))
			{
			// The caller has specified an existing directory - use it!
			tmpdir = dir;
			}
		else
			{
			// Try to work out a temp. dir to use.
#if defined(WIN32) || defined(_WIN32)
			// Windows provides a nice syscall to do the work for us
			GetTempPath(MAX_PATH, (TCHAR *)tmpdir.lockBuffer(MAX_PATH, sizeof(TCHAR)));
			tmpdir.unlockBuffer();
			if (tmpdir.isEmpty()) tmpdir = ".";
#else
			// Try the following: $TMP, $TEMP, /tmp, /var/tmp, and .
			if (getenv("TMP") && SWFileInfo::isDirectory(getenv("TMP"))) tmpdir = getenv("TMP");
			else if (getenv("TEMP") && SWFileInfo::isDirectory(getenv("TEMP"))) tmpdir = getenv("TEMP");
			else if (SWFileInfo::isDirectory("/tmp")) tmpdir = "/tmp";
			else if (SWFileInfo::isDirectory("/var/tmp")) tmpdir = "/var/tmp";
			else tmpdir = ".";
#endif
			}

		return SWFilename::fix(tmpdir);
		}



/*
** Creates a temporary file name
*/
SWString
SWTempFile::getTempFileName(const SWString &prefix, const SWString &ext, const SWString &dir)
		{
		SWString	tmpfile, tmpPrefix(prefix);
		static int	tmpno=0;

		tmpfile.format("%s%d_%d_%d%s", (const char *)tmpPrefix, sw_process_self(), sw_thread_self(), tmpno++, ext.c_str());

		return SWFilename::concatPaths(getTempDirectory(dir), tmpfile);
		}




