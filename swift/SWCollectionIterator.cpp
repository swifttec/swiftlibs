/**
*** @file		SWCollectionIterator.cpp
*** @brief		SWCollectionIterator Class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWCollectionIterator class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <swift/SWCollectionIterator.h>
#include <swift/SWCollection.h>
#include <swift/SWException.h>
#include <swift/SWIterator.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




// Constructor
SWCollectionIterator::SWCollectionIterator(SWCollection *c) :
    m_pCollection(c),
    m_refcount(0),
    m_modcount(0)
		{
		if (m_pCollection != NULL) m_modcount = m_pCollection->getModCount();
		}

sw_size_t
SWCollectionIterator::addRef()
		{
		return ++m_refcount;
		}


sw_size_t
SWCollectionIterator::dropRef()
		{
		if (m_refcount == 0) throw SW_EXCEPTION(SWIllegalStateException);
		return --m_refcount;
		}


sw_size_t
SWCollectionIterator::refCount()
		{
		return m_refcount;
		}


void
SWCollectionIterator::checkForComodification() 
		{
		if (m_pCollection != NULL && m_modcount != m_pCollection->getModCount()) throw SW_EXCEPTION(SWConcurrentModificationException);
		}



