/**
*** @file		SWCString.cpp
*** @brief		A single-byte string.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWCString class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <swift/SWCString.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





// Constructor: Blank string
SWCString::SWCString()
		{
		setCharSize(sizeof(char));
		m_flags |= SW_STRING_CHARSIZE_FIXED;
		}


// Constructor: Copy of char string s
SWCString::SWCString(const char *s)
		{
		setCharSize(sizeof(char));
		m_flags |= SW_STRING_CHARSIZE_FIXED;
		*this = s;
		}


// Constructor: Copy of wchar_t string s
SWCString::SWCString(const wchar_t *s)
		{
		setCharSize(sizeof(char));
		m_flags |= SW_STRING_CHARSIZE_FIXED;
		*this = s;
		}


// Constructor: Copy of char string s
SWCString::SWCString(const char *s, sw_size_t n)
		{
		setCharSize(sizeof(char));
		m_flags |= SW_STRING_CHARSIZE_FIXED;
		*this = SWString(s, n);
		}


// Constructor: Copy of wchar_t string s
SWCString::SWCString(const wchar_t *s, sw_size_t n)
		{
		setCharSize(sizeof(char));
		m_flags |= SW_STRING_CHARSIZE_FIXED;
		*this = SWString(s, n);
		}


SWCString::SWCString(int c, sw_size_t n)
		{
		setCharSize(sizeof(char));
		m_flags |= SW_STRING_CHARSIZE_FIXED;
		append(c, n);
		}


// Copy Constructor (reference)
SWCString::SWCString(const SWString &s) :
	SWString()
		{
		setCharSize(sizeof(char));
		m_flags |= SW_STRING_CHARSIZE_FIXED;
		if (s.length()) SWString::operator=(s);
		}


// Copy Constructor (reference)
SWCString::SWCString(const SWCString &s) :
	SWString()
		{
		setCharSize(sizeof(char));
		m_flags |= SW_STRING_CHARSIZE_FIXED;
		if (s.length()) SWString::operator=(s);
		}




