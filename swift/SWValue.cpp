/**
*** @file		SWValue.cpp
*** @brief		A generic value object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWValue class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <swift/SWValue.h>
#include <swift/SWLocale.h>
#include <swift/sw_printf.h>
#include <swift/SWLocalMemoryManager.h>
#include <swift/SWString.h>
#include <swift/SWCString.h>
#include <swift/SWWString.h>
#include <swift/SWInputStream.h>
#include <swift/SWOutputStream.h>
#include <swift/SWFilename.h>
#include <swift/SWFileInfo.h>
#include <swift/SWFile.h>


#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif

// Forward declarations
#if defined(SW_BUILD_MFC)
#include <afxdb.h>
#endif


// Should be last include
// #include <swift/sw_memory.h>



SWMemoryManager			*SWValue::__pMemoryManager=NULL;

void
SWValue::init(ValueType ft, size_t datalen, bool changed)
		{
		if (__pMemoryManager == NULL)
			{
			__pMemoryManager = SWMemoryManager::getDefaultMemoryManager();
			}

		memset(&_u, 0, sizeof(_u));
		_type = ft;
		_datalen = datalen;
		_changed = changed;
		}


bool
SWValue::clearValue(bool updateChanged)
		{
		switch (_type)
			{
			case VtCString:	delete [] _u.cstr;					break;
			case VtWString:	delete [] _u.wstr;					break;
			case VtData:	__pMemoryManager->free(_u.pData);	break;

			default:
				// Do nothing
				break;
			}

		if (updateChanged && _type != VtNone) setChangedFlag(true);

		memset(&_u, 0, sizeof(_u));
		_type = VtNone;
		_datalen = 0;

		return true;
		}


SWValue::~SWValue()
		{
		clearValue(false);
		}



SWValue::SWValue(const SWString &v, const SWString &t)
		{
		init(VtNone, 0, false);
		setValue(v, t);
		_changed = false;
		}


SWValue::SWValue()											{	init(VtNone, 0, false);													}
SWValue::SWValue(const SWValue &v) : SWObject()				{	init(VtNone, 0, false);	*this = v;			_changed = false;			}
SWValue::SWValue(int v, ValueType type)						{	init(VtNone, 0, false);	setValue(v, type);	_changed = false;			}
SWValue::SWValue(bool v)									{	init(VtNone, 0, false);	setValue(v);		_changed = false;			}
SWValue::SWValue(sw_int8_t v)								{	init(VtNone, 0, false);	setValue(v);		_changed = false;			}
SWValue::SWValue(sw_int16_t v)								{	init(VtNone, 0, false);	setValue(v);		_changed = false;			}
SWValue::SWValue(sw_int32_t v)								{	init(VtNone, 0, false);	setValue(v);		_changed = false;			}
SWValue::SWValue(sw_int64_t v)								{	init(VtNone, 0, false);	setValue(v);		_changed = false;			}
SWValue::SWValue(sw_uint8_t v)								{	init(VtNone, 0, false);	setValue(v);		_changed = false;			}
SWValue::SWValue(sw_uint16_t v)								{	init(VtNone, 0, false);	setValue(v);		_changed = false;			}
SWValue::SWValue(sw_uint32_t v)								{	init(VtNone, 0, false);	setValue(v);		_changed = false;			}
SWValue::SWValue(sw_uint64_t v)								{	init(VtNone, 0, false);	setValue(v);		_changed = false;			}
SWValue::SWValue(float v)									{	init(VtNone, 0, false);	setValue(v);		_changed = false;			}
SWValue::SWValue(double v)									{	init(VtNone, 0, false);	setValue(v);		_changed = false;			}
SWValue::SWValue(const char *v)								{	init(VtNone, 0, false);	setValue(v);		_changed = false;			}
SWValue::SWValue(const wchar_t *v)							{	init(VtNone, 0, false);	setValue(v);		_changed = false;			}
SWValue::SWValue(const SWString &v)							{	init(VtNone, 0, false);	setValue(v);		_changed = false;			}
SWValue::SWValue(const void *p, size_t len, bool copyData)	{	init(VtNone, 0, false);	setValue(p, len, copyData);	_changed = false;	}
SWValue::SWValue(const SWBuffer &v)							{	init(VtNone, 0, false);	setValue(v);		_changed = false;			}
SWValue::SWValue(const SWDate &v)							{	init(VtNone, 0, false);	setValue(v);		_changed = false;			}
SWValue::SWValue(const SWTime &v)							{	init(VtNone, 0, false);	setValue(v);		_changed = false;			}
SWValue::SWValue(const SWMoney &v)							{	init(VtNone, 0, false);	setValue(v);		_changed = false;			}

#if defined(SW_BUILD_MFC)
SWValue::SWValue(const COleVariant &v)						{	init(VtNone, 0, false);	setValue(v);		_changed = false;			}
SWValue::SWValue(const CDBVariant &v)						{	init(VtNone, 0, false);	setValue(v);		_changed = false;			}
SWValue::SWValue(const CString &v)							{	init(VtNone, 0, false);	setValue(v);		_changed = false;			}
#endif

SWValue &	SWValue::operator=(bool v)						{ setValue(v); return *this; }
SWValue &	SWValue::operator=(sw_int8_t v)					{ setValue(v); return *this; }
SWValue &	SWValue::operator=(sw_int16_t v)				{ setValue(v); return *this; }
SWValue &	SWValue::operator=(sw_int32_t v)				{ setValue(v); return *this; }
SWValue &	SWValue::operator=(sw_int64_t v)				{ setValue(v); return *this; }
SWValue &	SWValue::operator=(sw_uint8_t v)				{ setValue(v); return *this; }
SWValue &	SWValue::operator=(sw_uint16_t v)				{ setValue(v); return *this; }
SWValue &	SWValue::operator=(sw_uint32_t v)				{ setValue(v); return *this; }
SWValue &	SWValue::operator=(sw_uint64_t v)				{ setValue(v); return *this; }
SWValue &	SWValue::operator=(float v)						{ setValue(v); return *this; }
SWValue &	SWValue::operator=(double v)					{ setValue(v); return *this; }
SWValue &	SWValue::operator=(const char *v)				{ setValue(v); return *this; }
SWValue &	SWValue::operator=(const wchar_t *v)			{ setValue(v); return *this; }
SWValue &	SWValue::operator=(const SWString &v)			{ setValue(v); return *this; }
SWValue &	SWValue::operator=(const SWBuffer &v)			{ setValue(v); return *this; }
SWValue &	SWValue::operator=(const SWDate &v)				{ setValue(v); return *this; }
SWValue &	SWValue::operator=(const SWTime &v)				{ setValue(v); return *this; }
SWValue &	SWValue::operator=(const SWMoney &v)			{ setValue(v); return *this; }

#if defined(SW_BUILD_MFC)
SWValue &	SWValue::operator=(const COleVariant &v)		{ setValue(v); return *this; }
SWValue &	SWValue::operator=(const CDBVariant &v)		{ setValue(v); return *this; }
SWValue &	SWValue::operator=(const CString &v)			{ setValue(v); return *this; }
#endif


SWValue &
SWValue::operator=(const SWValue &mf)
		{
		// Record the change info BEFORE making any changes
		bool cflag = (_type != mf._type || _datalen != mf._datalen || memcmp(data(), mf.data(), _datalen) != 0);

		if (cflag)
			{
			clearValue(false);

			init(mf._type, mf._datalen, false);
			_u = mf._u;

			switch (_type)
				{
				case VtCString:	_u.cstr = strnew(mf._u.cstr);	break;
				case VtWString:	_u.wstr = wcsnew(mf._u.wstr);	break;

				case VtData:		
					_u.pData = (char *)__pMemoryManager->malloc(_datalen);
					memcpy(_u.pData, mf._u.pData, _datalen);
					break;

				default:
					// Do nothing
					break;
				}

			// Finally update the changed flag before returning
			setChangedFlag(cflag);
			}

		return *this;
		}


bool	SWValue::setValue(sw_int8_t v)		{ return setValue(v, VtInt8); }
bool	SWValue::setValue(sw_int16_t v)		{ return setValue(v, VtInt16); }
bool	SWValue::setValue(sw_int32_t v)		{ return setValue(v, VtInt32); }
bool	SWValue::setValue(sw_uint8_t v)		{ return setValue(v, VtUInt8); }
bool	SWValue::setValue(sw_uint16_t v)	{ return setValue(v, VtUInt16); }
bool	SWValue::setValue(sw_uint32_t v)	{ return setValue(v, VtUInt32); }

bool
SWValue::setValue(const SWValue &v)
		{
		*this = v;

		return true;
		}


// VtBoolean setValue
bool
SWValue::setValue(bool v)	
		{
		if (_type != VtBoolean || _u.b != v)
			{
			clearValue(false);

			init(VtBoolean, sizeof(_u.b), true);
			_u.b = v;	
			setChangedFlag(true);
			}

		return true;
		}
	

bool
SWValue::setValue(const SWDate &v)
		{
		// Check for changes before doing them
		if (_type != VtDate || _u.julian != v.julian())
			{
			clearValue(false);

			init(VtDate, sizeof(_u.julian), true);
			_u.julian = v.julian();
			setChangedFlag(true);
			}

		return true;
		}


bool
SWValue::setValue(const SWTime &v)
		{
		// Check for changes before doing them
		sw_internal_time_t	ts;

		v.toInternalTime(ts);

		if (_type != VtTime || memcmp(&ts, &_u.ts, sizeof(ts)) != 0)
			{
			clearValue(false);

			init(VtTime, sizeof(_u.ts), true);
			_u.ts = ts;
			setChangedFlag(true);
			}

		return true;
		}


bool
SWValue::setValue(sw_int64_t v)
		{
		// Check for changes before doing them
		if (_type != VtInt64 || _u.i64 != v)
			{
			clearValue(false);

			init(VtInt64, sizeof(v), true);
			_u.i64 = v;
			setChangedFlag(true);
			}

		return true;
		}


bool
SWValue::setValue(sw_uint64_t v)
		{
		// Check for changes before doing them
		if (_type != VtUInt64 || _u.ui64 != v)
			{
			clearValue(false);

			init(VtUInt64, sizeof(v), true);
			_u.ui64 = v;
			setChangedFlag(true);
			}

		return true;
		}


bool
SWValue::setValue(float v)
		{
		// Check for changes before doing them
		if (_type != VtFloat || _u.f != v)
			{
			clearValue(false);

			init(VtFloat, sizeof(v), true);
			_u.f = v;
			setChangedFlag(true);
			}

		return true;
		}


/// Double Constructor
bool
SWValue::setValue(double v)
		{
		// Check for changes before doing them
		if (_type != VtDouble || _u.d != v)
			{
			clearValue(false);

			init(VtDouble, sizeof(v), true);
			_u.d = v;
			setChangedFlag(true);
			}

		return true;
		}


bool
SWValue::setValue(const SWMoney &v)
		{
		// Check for changes before doing them
		if (_type != VtMoney || _u.money != v)
			{
			clearValue(false);

			init(VtMoney, sizeof(sw_money_t), true);
			_u.money = v;
			setChangedFlag(true);
			}

		return true;
		}


bool
SWValue::setValue(const char *v)
		{
		size_t	len = (strlen(v)+1)*sizeof(char);

		// Check for changes before doing them
		if (_type != VtCString || len != _datalen || strcmp(_u.cstr, v) != 0)
			{
			clearValue(false);

			init(VtCString, len, true);
			_u.cstr = strnew(v);
			setChangedFlag(true);
			}

		return true;
		}


bool
SWValue::setValue(const wchar_t *v)
		{
		size_t	len = (wcslen(v)+1)*sizeof(wchar_t);

		// Check for changes before doing them
		if (_type != VtWString || len != _datalen || wcscmp(_u.wstr, v) != 0)
			{
			clearValue(false);

			init(VtWString, len, true);
			_u.wstr = wcsnew(v);
			setChangedFlag(true);
			}

		return true;
		}


bool
SWValue::setValue(const SWString &v)
		{
		bool	res=false;

		switch (v.charSize())
			{
			case sizeof(wchar_t):
				res = setValue(v.w_str());
				break;

			default:
				res = setValue(v.c_str());
				break;
			}

		return res;
		}


bool
SWValue::setValue(const SWBuffer &v)
		{
		return setValue(v.data(), v.size(), true);
		}


bool
SWValue::setValue(const void *v, size_t len, bool copyData)
		{
		// Check for changes before doing them
		ValueType	vt=(copyData?VtData:VtPointer);
		bool		changed;

		changed = (_type != vt || len != _datalen);
		if (!changed)
			{
			if (vt == VtData) changed = (memcmp(_u.pData, v, len) != 0);
			else changed = (_u.pData != v);
			}

		if (changed)
			{
			clearValue(false);

			init(vt, len, true);
			if (vt == VtData)
				{
				// Copy the data
				_u.pData = (char *)__pMemoryManager->malloc(len);
				memcpy(_u.pData, v, len);
				}
			else
				{
				_u.pData = (void *)v;
				}

			setChangedFlag(true);
			}

		return true;
		}



/**
*** Special multi-type setValue method for integer based components
*** CHAR, BYTE, WCHAR, SHORT, INTEGER
**/
bool
SWValue::setValue(int v, ValueType type)
		{
		switch (type)
			{
			case VtInt8:
				if (_type != type || _u.i8 != v)
					{
					clearValue(false);

					init(type, sizeof(_u.i8), true);
					_u.i8 = (sw_int8_t)v;	
					setChangedFlag(true);
					}
				break;

			case VtInt16:
				if (_type != type || _u.i16 != v)
					{
					clearValue(false);

					init(type, sizeof(_u.i16), true);
					_u.i16 = (sw_int16_t)v;	
					setChangedFlag(true);
					}
				break;

			case VtInt32:
				if (_type != type || _u.i32 != v)
					{
					clearValue(false);

					init(type, sizeof(_u.i32), true);
					_u.i32 = (sw_int32_t)v;	
					setChangedFlag(true);
					}
				break;

			case VtUInt8:
				if (_type != type || _u.ui8 != (sw_uint8_t)v)
					{
					clearValue(false);

					init(type, sizeof(_u.ui8), true);
					_u.ui8 = (sw_uint8_t)v;	
					setChangedFlag(true);
					}
				break;

			case VtUInt16:
				if (_type != type || _u.ui16 != (sw_uint16_t)v)
					{
					clearValue(false);

					init(type, sizeof(_u.ui16), true);
					_u.ui16 = (sw_uint16_t)v;	
					setChangedFlag(true);
					}
				break;

			case VtUInt32:
				if (_type != type || _u.ui32 != (sw_uint32_t)v)
					{
					clearValue(false);

					init(type, sizeof(_u.ui32), true);
					_u.ui32 = (sw_uint32_t)v;	
					setChangedFlag(true);
					}
				break;

			case VtCChar:	
				if (_type != type || _u.cc != v)
					{
					clearValue(false);

					init(type, sizeof(char), true);
					_u.cc = (char)v;	
					setChangedFlag(true);
					}
				break;

			case VtWChar:
				if (_type != type || _u.wc != v)
					{
					clearValue(false);

					init(type, sizeof(wchar_t), true);
					_u.wc = (wchar_t)v;	
					setChangedFlag(true);
					}
				break;

			case VtBoolean:
				if (_type != type || _u.b != (v!=0))
					{
					clearValue(false);

					init(type, sizeof(_u.b), true);
					_u.b = (v!=0);	
					setChangedFlag(true);
					}
				break;

			default:
				if (_type != type || _u.i32 != v)
					{
					clearValue(false);

					init(VtInt32, sizeof(_u.i32), true);
					_u.i32 = v;	
					setChangedFlag(true);
					}
				break;
			}

		return true;
		}



#if defined(SW_BUILD_MFC)
bool
SWValue::setValue(const COleVariant &v)
		{
		switch (v.vt)
			{
			case VT_UI1:	setValue(v.bVal, VtUInt8);		break;
			case VT_UI2:	setValue(v.uiVal, VtUInt16);	break;
			case VT_UI4:	setValue(v.ulVal, VtUInt32);	break;
			case VT_UI8:	setValue(v.ullVal);				break;
			case VT_UINT:	setValue(v.uintVal, VtUInt32);	break;
			case VT_I1:		setValue(v.cVal, VtInt8);		break;
			case VT_I2:		setValue(v.iVal, VtInt16);		break;
			case VT_I4:		setValue(v.lVal, VtInt32);		break;
			case VT_I8:		setValue(v.llVal);				break;
			case VT_INT:	setValue(v.intVal, VtInt32);	break;
			case VT_R4:		setValue(v.fltVal);				break;
			case VT_R8:		setValue(v.dblVal);				break;
			//case VT_CY:		setValue(v.cyVal);				break;

			case VT_DATE:	setValue(SWTime(v.date, true));		break;
			case VT_BOOL:	setValue(v.bVal != 0);				break;

			case VT_BSTR:
				if (v.bstrVal != NULL) setValue((const sw_tchar_t *)v.bstrVal);
				break;

			case VT_ARRAY|VT_UI1: // An array of bytes!
				{
				CByteArray	ba;
				COleVariant	tv=v;

				tv.GetByteArrayFromVariantArray(ba);
				setValue(ba.GetData(), ba.GetSize(), true);
				}
				break;
			}

		return true;
		}


bool
SWValue::setValue(const CDBVariant &v)
		{
		switch (v.m_dwType)
			{
			case DBVT_BOOL:		setValue(v.m_boolVal != 0);		break;
			case DBVT_UCHAR:	setValue(v.m_chVal, VtInt8);		break;
			case DBVT_SHORT:	setValue(v.m_iVal, VtInt16);	break;
			case DBVT_LONG:		setValue(v.m_lVal, VtInt32);	break;
			case DBVT_SINGLE:	setValue(v.m_fltVal);				break;
			case DBVT_DOUBLE:	setValue(v.m_dblVal);				break;

			case DBVT_BINARY: // An array of bytes!
				{
				void	*p;

				p = GlobalLock(v.m_pbinary->m_hData);
				if (p != NULL)
					{
					setValue(p, v.m_pbinary->m_dwDataLength, true);
					GlobalUnlock(v.m_pbinary->m_hData);
					}
				}
				break;

			case DBVT_DATE:
				{
				SWTime	t;

				t.set(
					v.m_pdate->year,
					v.m_pdate->month,
					v.m_pdate->day,
					v.m_pdate->hour,
					v.m_pdate->minute,
					v.m_pdate->second,
					v.m_pdate->fraction
					);

				setValue(t);	
				}
				break;

			case DBVT_STRING:	setValue(*v.m_pstring);						break;
			case DBVT_ASTRING:	setValue((const char *)*v.m_pstringA);		break;
			case DBVT_WSTRING:	setValue((const wchar_t *)*v.m_pstringW);	break;
#define DBVT_DATE       7
#define DBVT_BINARY     9

			default:
				clear();
				break;
			}

		return true;
		}


bool
SWValue::setValue(const CString &v)
		{
		return setValue((const sw_tchar_t *)v);
		}
#endif


SWString
SWValue::typeAsString() const
		{
		return typeAsString(type());
		}



static const char *
getnum(const char *sp, int ndigits, int &value)
		{
		int	v = 0;

		while (ndigits > 0 && *sp)
			{
			if (*sp >= '0' && *sp <= '9')
				{
				v = v * 10 + (*sp - '0');
				sp++;
				--ndigits;
				}
			else break;
			}

		if (ndigits != 0)
			{
			value = 0;
			sp = NULL;
			}
		else
			{
			value = v;
			}

		return sp;
		}


bool
SWValue::setValue(const SWString &v, const SWString &t)
		{
		bool	res=false;

		if (t.compareNoCase("boolean") == 0 || t.compareNoCase("bool") == 0)
			{
			bool		c=false;

			if (v.length() > 0) c = charToBoolean(v[0]);

			res = setValue(c);
			}
		else if (t.compareNoCase("char") == 0 || t.compareNoCase("character") == 0)
			{
			int		c=0;

			if (v.length() > 0) c = v[0];

			ValueType vt=VtCChar;

			if (c > 0xff) vt = VtWChar;
			res = setValue(c, vt);
			}
		else if (t.compareNoCase("int") == 0 || t.compareNoCase("integer") == 0)	res = setValue(v.toInt32());
		else if (t.compareNoCase("long") == 0)								res = setValue(v.toInt64());
		else if (t.compareNoCase("int8") == 0)								res = setValue((sw_int8_t)v.toInt32());
		else if (t.compareNoCase("int16") == 0)								res = setValue((sw_int16_t)v.toInt32());
		else if (t.compareNoCase("int32") == 0)								res = setValue((sw_int32_t)v.toInt32());
		else if (t.compareNoCase("int64") == 0)								res = setValue((sw_int64_t)v.toInt64());
		else if (t.compareNoCase("uint8") == 0)								res = setValue((sw_uint8_t)v.toUInt32());
		else if (t.compareNoCase("uint16") == 0)								res = setValue((sw_uint16_t)v.toUInt32());
		else if (t.compareNoCase("uint32") == 0)								res = setValue((sw_uint32_t)v.toUInt32());
		else if (t.compareNoCase("uint64") == 0)								res = setValue((sw_uint64_t)v.toUInt64());
		else if (t.compareNoCase("float") == 0)								res = setValue((float)v.toDouble());
		else if (t.compareNoCase("double") == 0)								res = setValue(v.toDouble());
		else if (t.compareNoCase("uint") == 0 || t.compareNoCase("unsigned") == 0 || t.compareNoCase("unsigned integer") == 0)
			{
			res = setValue(v.toUInt32());
			}
		else if (t.compareNoCase("ulong") == 0 || t.compareNoCase("unsigned long") == 0)
			{
			res = setValue(v.toUInt64());
			}
		else if (t.compareNoCase("money") == 0)
			{
			res = setValue(SWMoney(v));
			}
		else if (t.compareNoCase("date") == 0
				|| t.compareNoCase("datetime") == 0
				|| t.compareNoCase("datetimems") == 0
				|| t.compareNoCase("time") == 0
				|| t.startsWith("date:", SWString::ignoreCase)
				|| t.startsWith("time:", SWString::ignoreCase))
			{
			SWString	format;
			SWString	tmpv(v);
			const char	*vp=tmpv;

			if (t.startsWith("date:", SWString::ignoreCase) || t.startsWith("time:", SWString::ignoreCase))
				{
				// Use the format supplied
				format = t.mid(5);
				}
			else if (t.compareNoCase("date") == 0) format = "YYYYMMDD";
			else if (t.compareNoCase("time") == 0) format = "hh:mm:ss";
			else if (t.compareNoCase("datetime") == 0) format = "YYYYMMDD hh:mm:ss";
			else if (t.compareNoCase("datetimems") == 0) format = "YYYYMMDD hh:mm:ss.ms";

			if (!format.isEmpty())
				{
				// Supported formats
				// YYYY 4-digit year
				// YY 2-digit year (00-99)
				// MM 2-digit month (01-12)
				// DD 2-digit day (01-31)
				// hh 2-digit hour (00-23)
				// mm 2-digit minute (00-59)
				// ss 2-digit second (00-61)
				// ms milliseconds (000-999)
				// us milliseconds (000000-999999)
				// ns milliseconds (000000000-999999999)

				const char	*fp=format;
				int			year=0, month=0, day=0, hour=0, minute=0, second=0, nanosecond=0;
				bool		hastime=false;

				vp = tmpv;
				while (vp != NULL && *fp && *vp)
					{
					if (strncmp(fp, "YYYY", 4) == 0)
						{
						vp = getnum(vp, 4, year);
						fp += 4;
						}
					else if (strncmp(fp, "YY", 2) == 0)
						{
						vp = getnum(vp, 2, year);
						if (vp != NULL)
							{
							SWDate	today;
							int		y;

							today.update();
							y = today.year() % 100;
							year += (today.year() - y) + (y >= 50?100:0) - (year >=50?100:0);
							}
						fp += 2;
						}
					else if (strncmp(fp, "MM", 2) == 0)
						{
						vp = getnum(vp, 2, month);
						fp += 2;
						}
					else if (strncmp(fp, "DD", 2) == 0)
						{
						vp = getnum(vp, 2, day);
						fp += 2;
						}
					else if (strncmp(fp, "hh", 2) == 0)
						{
						vp = getnum(vp, 2, hour);
						fp += 2;
						hastime = true;
						}
					else if (strncmp(fp, "mm", 2) == 0)
						{
						vp = getnum(vp, 2, minute);
						fp += 2;
						hastime = true;
						}
					else if (strncmp(fp, "ss", 2) == 0)
						{
						vp = getnum(vp, 2, second);
						fp += 2;
						hastime = true;
						}
					else if (strncmp(fp, "ms", 2) == 0)
						{
						vp = getnum(vp, 3, nanosecond);
						fp += 2;
						hastime = true;
						nanosecond *= 1000000;
						}
					else if (strncmp(fp, "us", 2) == 0)
						{
						vp = getnum(vp, 6, nanosecond);
						fp += 2;
						hastime = true;
						nanosecond *= 1000;
						}
					else if (strncmp(fp, "ns", 2) == 0)
						{
						vp = getnum(vp, 9, nanosecond);
						fp += 2;
						hastime = true;
						}
					else if (*fp == *vp)
						{
						fp++;
						vp++;
						}
					else
						{
						// Invalid string - does not match pattern
						vp = NULL;
						}
					}

				if (vp != NULL)
					{
					if (year != 0 && month != 0 && day != 0)
						{
						// This is at least a date
						if (hastime)
							{
							sw_tm_t	tm;

							memset(&tm, 0, sizeof(tm));
							tm.tm_sec = second;     /* seconds after the minute [0, 61]  */
							tm.tm_min = minute;     /* minutes after the hour [0, 59] */
							tm.tm_hour = hour;    /* hour since midnight [0, 23] */
							tm.tm_mday = day;    /* day of the month [1, 31] */
							tm.tm_mon = month-1;     /* months since January [0, 11] */
							tm.tm_year = year-1900;    /* years since 1900 */

							res = setValue(tm);

							// tm will leave the ns portion of time at 0 so we post-adjust it
							_u.ts.tv_nsec = nanosecond;
							}
						else
							{
							res = setValue(SWDate(day, month, year));
							}
						}
					else
						{
						res = false;
						}
					}
				}
			}
		
		if (!res)
			{
			// Default is a string
			res = setValue(v);
			}

		return res;
		}



// STATIC version
SWString
SWValue::typeAsString(ValueType vt)
		{
		SWString	ftype="Unknown";

		switch (vt)
			{
			case VtNone:		ftype = "None";			break;
			case VtBoolean:		ftype = "Boolean";		break;
			case VtInt8:		ftype = "Int8";			break;
			case VtInt16:		ftype = "Int16";		break;
			case VtInt32:		ftype = "Int32";		break;
			case VtInt64:		ftype = "Int64";		break;
			case VtUInt8:		ftype = "UInt8";		break;
			case VtUInt16:		ftype = "UInt16";		break;
			case VtUInt32:		ftype = "UInt32";		break;
			case VtUInt64:		ftype = "UInt64";		break;
			case VtFloat:		ftype = "Float";		break;
			case VtDouble:		ftype = "Double";		break;
			case VtCChar:		ftype = "CChar";		break;
			case VtWChar:		ftype = "WChar";		break;
			case VtCString:		ftype = "CString";		break;
			case VtWString:		ftype = "WString";		break;
			case VtData:		ftype = "Data";			break;
			case VtPointer:		ftype = "Pointer";		break;
			case VtDate:		ftype = "Date";			break;
			case VtTime:		ftype = "Time";			break;
			case VtMoney:		ftype = "Money";		break;
			case VtCurrency:	ftype = "Currency";		break;
			}

		return ftype;
		}


void
SWValue::dump()
		{
		SWString	ftype=typeAsString();
		bool		showbytes=false;

		printf("Field: Type=%s  DataSize=%zd  %s\n", (const char *)ftype, _datalen, _changed?"*** CHANGED ***":"");

		printf("     : ");

		switch (type())
			{
			case VtNone:	sw_printf("No data");							break;
			case VtBoolean:	sw_printf("%s", _u.b?"TRUE":"FALSE");	break;
			case VtInt8:	sw_printf("%d", _u.i8);							break;
			case VtInt16:	sw_printf("%d", _u.i16);							break;
			case VtInt32:	sw_printf("%d", _u.i32);							break;
			case VtInt64:	sw_printf("%lld", _u.i64);							break;
			case VtUInt8:	sw_printf("%u", _u.ui8);							break;
			case VtUInt16:	sw_printf("%u", _u.ui16);							break;
			case VtUInt32:	sw_printf("%u", _u.ui32);							break;
			case VtUInt64:	sw_printf("%llu", _u.ui64);							break;
			case VtFloat:	sw_printf("%f", _u.f);	break;
			case VtDouble:	sw_printf("%f", _u.d);	break;
			case VtCChar:	sw_printf("%c", _u.cc);	break;
			case VtWChar:	sw_printf("%lc", _u.wc);	break;
			case VtCString:	sw_printf("%s", _u.cstr);	break;
			case VtWString:	sw_printf("%ls", _u.wstr);	break;
			case VtPointer:	sw_printf("%p", _u.pData);	break;
			case VtData:
				sw_printf("<Binary Data>");
				if (!showbytes)
					{
					putchar('\n');
					for (size_t i=0;i<_datalen;i++)
						{
						sw_printf("%02x ", ((char *)_u.pData)[i]&0xff);
						}
					}
				break;
			case VtDate:
				{
				SWDate	d(_u.julian, true);

				sw_printf("%d (%02d/%02d/%04d)", _u.julian, d.day(), d.month(), d.year());
				}
				break;

			case VtTime:
				{
				SWTime	t(_u.ts);

				sw_printf("%lld/%u (%s)", _u.ts.tv_sec, _u.ts.tv_nsec, (const char *)t.format("%c"));
				}
				break;

			case VtMoney:
				{
				SWMoney	t(_u.money);

				sw_printf("%lld (%s)", _u.money, (const char *)t.toString());
				}
				break;
			}
		putchar('\n');

#define BYTES_PER_ROW	32

		if (showbytes)
			{
			char	*tmp = (char *)data();
			size_t		i, l = length();

			sw_printf("------------------------------------------------------------\n");
			for (i=0;i<l;i++)
				{
				printf("%02x ", tmp[i]&0xff);
				if ((i % BYTES_PER_ROW) == (BYTES_PER_ROW - 1)) putchar('\n');
				}
			if ((i % BYTES_PER_ROW) != 0) putchar('\n');
			sw_printf("------------------------------------------------------------\n");
			}

#undef BYTES_PER_ROW
		}




	





bool
SWValue::charToBoolean(int c)
		{
		SWString	tchars=SWLocale::getCurrentLocale().booleanChars(true);
		int			i;

		if (c == 1 || (c >= '1' && c <= '9')) return true;
		for (i=0;i<(int)tchars.length();i++) if (c == tchars[i]) return true;

		return false;
		}



int
SWValue::booleanToChar(bool v)
		{
		SWString	chars=SWLocale::getCurrentLocale().booleanChars(v);
		int			c=v?'y':'n';

		if (!chars.isEmpty()) c = chars[0];

		return c;
		}


SWString
SWValue::booleanToString(bool v)
		{
		SWString	s=SWLocale::getCurrentLocale().booleanString(v);
		
		if (s.isEmpty()) s = v?"true":"false";

		return s;
		}


/*
** Type converstions cast operators
*/
bool
SWValue::toBoolean() const
		{
		bool	res=false;

		switch (_type)
			{
			case VtBoolean:	res = _u.b;									break;
			case VtInt8:	res = (_u.i8 != 0);								break;
			case VtInt16:	res = (_u.i16 != 0);							break;
			case VtInt32:	res = (_u.i32 != 0);							break;
			case VtInt64:	res = (_u.i64 != 0);							break;
			case VtUInt8:	res = (_u.ui8 != 0);							break;
			case VtUInt16:	res = (_u.ui16 != 0);							break;
			case VtUInt32:	res = (_u.ui32 != 0);							break;
			case VtUInt64:	res = (_u.ui64 != 0);							break;
			case VtFloat:	res = (_u.f != 0.0);							break;
			case VtDouble:	res = (_u.d != 0.0);							break;
			case VtCChar:	res = charToBoolean(_u.cc);						break;
			case VtWChar:	res = charToBoolean(_u.wc);						break;
			case VtCString:	res = charToBoolean(*_u.cstr);					break;
			case VtWString:	res = charToBoolean(*_u.wstr);					break;
			case VtPointer: res = (_u.pData != NULL);						break;
			case VtData:	res = (_datalen > 0 && *(char *)_u.pData != 0);	break;
			case VtDate:	res = (_u.julian != 0);							break;
			case VtTime:	res = (_u.ts.tv_sec != 0 || _u.ts.tv_nsec != 0);	break;
			case VtMoney:	res = (_u.money != 0);							break;

			default:		res = false;							break;
			}

		return res;
		}

sw_int8_t
SWValue::toInt8() const
		{
		sw_int8_t	res=0;

		switch (_type)
			{
			case VtBoolean:	res = (sw_int8_t)_u.b;				break;
			case VtInt8:	res = (sw_int8_t)_u.i8;				break;
			case VtInt16:	res = (sw_int8_t)_u.i16;			break;
			case VtInt32:	res = (sw_int8_t)_u.i32;			break;
			case VtInt64:	res = (sw_int8_t)_u.i64;			break;
			case VtUInt8:	res = (sw_int8_t)_u.ui8;			break;
			case VtUInt16:	res = (sw_int8_t)_u.ui16;			break;
			case VtUInt32:	res = (sw_int8_t)_u.ui32;			break;
			case VtUInt64:	res = (sw_int8_t)_u.ui64;			break;
			case VtFloat:	res = (sw_int8_t)_u.f;				break;
			case VtDouble:	res = (sw_int8_t)_u.d;				break;
			case VtCChar:	res = (sw_int8_t)_u.cc;				break;
			case VtWChar:	res = (sw_int8_t)_u.wc;				break;
			case VtCString:	res = (sw_int8_t)SWString::toInt32(_u.cstr);		break;
			case VtWString:	res = (sw_int8_t)SWString::toInt32(_u.wstr);		break;
			case VtDate:	res = (sw_int8_t)_u.julian;			break;
			case VtTime:	res = (sw_int8_t)_u.ts.tv_sec;	break;
			case VtMoney:	res = (sw_int8_t)_u.money;	break;

			default:		res = 0;							break;
			}

		return res;
		}



sw_int16_t
SWValue::toInt16() const
		{
		sw_int16_t	res=0;

		switch (_type)
			{
			case VtBoolean:	res = (sw_int16_t)_u.b;			break;
			case VtInt8:	res = (sw_int16_t)_u.i8;			break;
			case VtInt16:	res = (sw_int16_t)_u.i16;			break;
			case VtInt32:	res = (sw_int16_t)_u.i32;			break;
			case VtInt64:	res = (sw_int16_t)_u.i64;			break;
			case VtUInt8:	res = (sw_int16_t)_u.ui8;			break;
			case VtUInt16:	res = (sw_int16_t)_u.ui16;			break;
			case VtUInt32:	res = (sw_int16_t)_u.ui32;			break;
			case VtUInt64:	res = (sw_int16_t)_u.ui64;			break;
			case VtFloat:	res = (sw_int16_t)_u.f;				break;
			case VtDouble:	res = (sw_int16_t)_u.d;				break;
			case VtCChar:	res = (sw_int16_t)_u.cc;			break;
			case VtWChar:	res = (sw_int16_t)_u.wc;			break;
			case VtCString:	res = (sw_int16_t)SWString::toInt32(_u.cstr);	break;
			case VtWString:	res = (sw_int16_t)SWString::toInt32(_u.wstr);	break;
			case VtDate:	res = (sw_int16_t)_u.julian;			break;
			case VtTime:	res = (sw_int16_t)_u.ts.tv_sec;	break;
			case VtMoney:	res = (sw_int16_t)_u.money;	break;

			default:		res = 0;							break;
			}

		return res;
		}



sw_int32_t
SWValue::toInt32() const
		{
		sw_int32_t	res=0;

		switch (_type)
			{
			case VtBoolean:	res = (sw_int32_t)_u.b;			break;
			case VtInt8:	res = (sw_int32_t)_u.i8;			break;
			case VtInt16:	res = (sw_int32_t)_u.i16;			break;
			case VtInt32:	res = (sw_int32_t)_u.i32;			break;
			case VtInt64:	res = (sw_int32_t)_u.i64;			break;
			case VtUInt8:	res = (sw_int32_t)_u.ui8;			break;
			case VtUInt16:	res = (sw_int32_t)_u.ui16;			break;
			case VtUInt32:	res = (sw_int32_t)_u.ui32;			break;
			case VtUInt64:	res = (sw_int32_t)_u.ui64;			break;
			case VtFloat:	res = (sw_int32_t)_u.f;				break;
			case VtDouble:	res = (sw_int32_t)_u.d;				break;
			case VtCChar:	res = (sw_int32_t)_u.cc;			break;
			case VtWChar:	res = (sw_int32_t)_u.wc;			break;
			case VtCString:	res = (sw_int32_t)SWString::toInt32(_u.cstr);	break;
			case VtWString:	res = (sw_int32_t)SWString::toInt32(_u.wstr);	break;
			case VtDate:	res = (sw_int32_t)_u.julian;			break;
			case VtTime:	res = (sw_int32_t)_u.ts.tv_sec;	break;
			case VtMoney:	res = (sw_int32_t)_u.money;	break;

			default:		res = 0;							break;
			}

		return res;
		}



sw_int64_t
SWValue::toInt64() const
		{
		sw_int64_t	res=0;

		switch (_type)
			{
			case VtBoolean:	res = (sw_int64_t)_u.b;			break;
			case VtInt8:	res = (sw_int64_t)_u.i8;			break;
			case VtInt16:	res = (sw_int64_t)_u.i16;			break;
			case VtInt32:	res = (sw_int64_t)_u.i32;			break;
			case VtInt64:	res = (sw_int64_t)_u.i64;			break;
			case VtUInt8:	res = (sw_int64_t)_u.ui8;			break;
			case VtUInt16:	res = (sw_int64_t)_u.ui16;			break;
			case VtUInt32:	res = (sw_int64_t)_u.ui32;			break;
			case VtUInt64:	res = (sw_int64_t)_u.ui64;			break;
			case VtFloat:	res = (sw_int64_t)_u.f;				break;
			case VtDouble:	res = (sw_int64_t)_u.d;				break;
			case VtCChar:	res = (sw_int64_t)_u.cc;			break;
			case VtWChar:	res = (sw_int64_t)_u.wc;			break;
			case VtCString:	res = (sw_int64_t)SWString::toInt64(_u.cstr);	break;
			case VtWString:	res = (sw_int64_t)SWString::toInt64(_u.wstr);	break;
			case VtDate:	res = (sw_int64_t)_u.julian;		break;
			case VtTime:	res = (sw_int64_t)_u.ts.tv_sec;	break;
			case VtMoney:	res = (sw_int64_t)_u.money;	break;

			default:		res = 0;							break;
			}

		return res;
		}




sw_uint8_t
SWValue::toUInt8() const
		{
		sw_uint8_t	res=0;

		switch (_type)
			{
			case VtBoolean:	res = (sw_uint8_t)_u.b;				break;
			case VtInt8:	res = (sw_uint8_t)_u.i8;				break;
			case VtInt16:	res = (sw_uint8_t)_u.i16;			break;
			case VtInt32:	res = (sw_uint8_t)_u.i32;			break;
			case VtInt64:	res = (sw_uint8_t)_u.i64;			break;
			case VtUInt8:	res = (sw_uint8_t)_u.ui8;			break;
			case VtUInt16:	res = (sw_uint8_t)_u.ui16;			break;
			case VtUInt32:	res = (sw_uint8_t)_u.ui32;			break;
			case VtUInt64:	res = (sw_uint8_t)_u.ui64;			break;
			case VtFloat:	res = (sw_uint8_t)_u.f;				break;
			case VtDouble:	res = (sw_uint8_t)_u.d;				break;
			case VtCChar:	res = (sw_uint8_t)_u.cc;				break;
			case VtWChar:	res = (sw_uint8_t)_u.wc;				break;
			case VtCString:	res = (sw_uint8_t)SWString::toUInt32(_u.cstr);		break;
			case VtWString:	res = (sw_uint8_t)SWString::toUInt32(_u.wstr);		break;
			case VtDate:	res = (sw_uint8_t)_u.julian;		break;
			case VtTime:	res = (sw_uint8_t)_u.ts.tv_sec;	break;
			case VtMoney:	res = (sw_uint8_t)_u.money;	break;

			default:		res = 0;							break;
			}

		return res;
		}



sw_uint16_t
SWValue::toUInt16() const
		{
		sw_uint16_t	res=0;

		switch (_type)
			{
			case VtBoolean:	res = (sw_uint16_t)_u.b;			break;
			case VtInt8:	res = (sw_uint16_t)_u.i8;			break;
			case VtInt16:	res = (sw_uint16_t)_u.i16;			break;
			case VtInt32:	res = (sw_uint16_t)_u.i32;			break;
			case VtInt64:	res = (sw_uint16_t)_u.i64;			break;
			case VtUInt8:	res = (sw_uint16_t)_u.ui8;			break;
			case VtUInt16:	res = (sw_uint16_t)_u.ui16;			break;
			case VtUInt32:	res = (sw_uint16_t)_u.ui32;			break;
			case VtUInt64:	res = (sw_uint16_t)_u.ui64;			break;
			case VtFloat:	res = (sw_uint16_t)_u.f;				break;
			case VtDouble:	res = (sw_uint16_t)_u.d;				break;
			case VtCChar:	res = (sw_uint16_t)_u.cc;			break;
			case VtWChar:	res = (sw_uint16_t)_u.wc;			break;
			case VtCString:	res = (sw_uint16_t)SWString::toUInt32(_u.cstr);	break;
			case VtWString:	res = (sw_uint16_t)SWString::toUInt32(_u.wstr);	break;
			case VtDate:	res = (sw_uint16_t)_u.julian;		break;
			case VtTime:	res = (sw_uint16_t)_u.ts.tv_sec;	break;
			case VtMoney:	res = (sw_uint16_t)_u.money;	break;

			default:		res = 0;							break;
			}

		return res;
		}



sw_uint32_t
SWValue::toUInt32() const
		{
		sw_uint32_t	res=0;

		switch (_type)
			{
			case VtBoolean:	res = (sw_uint32_t)_u.b;			break;
			case VtInt8:	res = (sw_uint32_t)_u.i8;			break;
			case VtInt16:	res = (sw_uint32_t)_u.i16;			break;
			case VtInt32:	res = (sw_uint32_t)_u.i32;			break;
			case VtInt64:	res = (sw_uint32_t)_u.i64;			break;
			case VtUInt8:	res = (sw_uint32_t)_u.ui8;			break;
			case VtUInt16:	res = (sw_uint32_t)_u.ui16;			break;
			case VtUInt32:	res = (sw_uint32_t)_u.ui32;			break;
			case VtUInt64:	res = (sw_uint32_t)_u.ui64;			break;
			case VtFloat:	res = (sw_uint32_t)_u.f;				break;
			case VtDouble:	res = (sw_uint32_t)_u.d;				break;
			case VtCChar:	res = (sw_uint32_t)_u.cc;			break;
			case VtWChar:	res = (sw_uint32_t)_u.wc;			break;
			case VtCString:	res = (sw_uint32_t)SWString::toUInt32(_u.cstr);	break;
			case VtWString:	res = (sw_uint32_t)SWString::toUInt32(_u.wstr);	break;
			case VtDate:	res = (sw_uint32_t)_u.julian;		break;
			case VtTime:	res = (sw_uint32_t)_u.ts.tv_sec;	break;
			case VtMoney:	res = (sw_uint32_t)_u.money;	break;

			default:		res = 0;							break;
			}

		return res;
		}



sw_uint64_t
SWValue::toUInt64() const
		{
		sw_uint64_t	res=0;

		switch (_type)
			{
			case VtBoolean:	res = (sw_uint64_t)_u.b;			break;
			case VtInt8:	res = (sw_uint64_t)_u.i8;			break;
			case VtInt16:	res = (sw_uint64_t)_u.i16;			break;
			case VtInt32:	res = (sw_uint64_t)_u.i32;			break;
			case VtInt64:	res = (sw_uint64_t)_u.i64;			break;
			case VtUInt8:	res = (sw_uint64_t)_u.ui8;			break;
			case VtUInt16:	res = (sw_uint64_t)_u.ui16;			break;
			case VtUInt32:	res = (sw_uint64_t)_u.ui32;			break;
			case VtUInt64:	res = (sw_uint64_t)_u.ui64;			break;
			case VtFloat:	res = (sw_uint64_t)_u.f;				break;
			case VtDouble:	res = (sw_uint64_t)_u.d;				break;
			case VtCChar:	res = (sw_uint64_t)_u.cc;			break;
			case VtWChar:	res = (sw_uint64_t)_u.wc;			break;
			case VtCString:	res = (sw_uint64_t)SWString::toUInt64(_u.cstr);	break;
			case VtWString:	res = (sw_uint64_t)SWString::toUInt64(_u.wstr);	break;
			case VtDate:	res = (sw_uint64_t)_u.julian;		break;
			case VtTime:	res = (sw_uint64_t)_u.ts.tv_sec;	break;
			case VtMoney:	res = (sw_uint64_t)_u.money;	break;

			default:		res = 0;							break;
			}

		return res;
		}







short
SWValue::toShort() const
		{
		short	res=0;

		switch (_type)
			{
			case VtBoolean:	res = (short)_u.b;			break;
			case VtInt8:	res = (short)_u.i8;			break;
			case VtInt16:	res = (short)_u.i16;			break;
			case VtInt32:	res = (short)_u.i32;			break;
			case VtInt64:	res = (short)_u.i64;			break;
			case VtUInt8:	res = (short)_u.ui8;			break;
			case VtUInt16:	res = (short)_u.ui16;			break;
			case VtUInt32:	res = (short)_u.ui32;			break;
			case VtUInt64:	res = (short)_u.ui64;			break;
			case VtFloat:	res = (short)_u.f;				break;
			case VtDouble:	res = (short)_u.d;				break;
			case VtCChar:	res = (short)_u.cc;			break;
			case VtWChar:	res = (short)_u.wc;			break;
			case VtCString:	res = (short)SWString::toInt32(_u.cstr);	break;
			case VtWString:	res = (short)SWString::toInt32(_u.wstr);	break;
			case VtDate:	res = (short)_u.julian;		break;
			case VtTime:	res = (short)_u.ts.tv_sec;	break;
			case VtMoney:	res = (short)_u.money;	break;

			default:		res = 0;							break;
			}

		return res;
		}

unsigned short
SWValue::toUShort() const
		{
		unsigned short	res=0;

		switch (_type)
			{
			case VtBoolean:	res = (unsigned short)_u.b;			break;
			case VtInt8:	res = (unsigned short)_u.i8;			break;
			case VtInt16:	res = (unsigned short)_u.i16;			break;
			case VtInt32:	res = (unsigned short)_u.i32;			break;
			case VtInt64:	res = (unsigned short)_u.i64;			break;
			case VtUInt8:	res = (unsigned short)_u.ui8;			break;
			case VtUInt16:	res = (unsigned short)_u.ui16;			break;
			case VtUInt32:	res = (unsigned short)_u.ui32;			break;
			case VtUInt64:	res = (unsigned short)_u.ui64;			break;
			case VtFloat:	res = (unsigned short)_u.f;				break;
			case VtDouble:	res = (unsigned short)_u.d;				break;
			case VtCChar:	res = (unsigned short)_u.cc;			break;
			case VtWChar:	res = (unsigned short)_u.wc;			break;
			case VtCString:	res = (unsigned short)SWString::toInt32(_u.cstr);	break;
			case VtWString:	res = (unsigned short)SWString::toInt32(_u.wstr);	break;
			case VtDate:	res = (unsigned short)_u.julian;		break;
			case VtTime:	res = (unsigned short)_u.ts.tv_sec;	break;
			case VtMoney:	res = (unsigned short)_u.money;	break;

			default:		res = 0;							break;
			}

		return res;
		}


int
SWValue::toInteger() const
		{
		int	res=0;

		switch (_type)
			{
			case VtBoolean:	res = (int)_u.b;			break;
			case VtInt8:	res = (int)_u.i8;			break;
			case VtInt16:	res = (int)_u.i16;			break;
			case VtInt32:	res = (int)_u.i32;			break;
			case VtInt64:	res = (int)_u.i64;			break;
			case VtUInt8:	res = (int)_u.ui8;			break;
			case VtUInt16:	res = (int)_u.ui16;			break;
			case VtUInt32:	res = (int)_u.ui32;			break;
			case VtUInt64:	res = (int)_u.ui64;			break;
			case VtFloat:	res = (int)_u.f;				break;
			case VtDouble:	res = (int)_u.d;				break;
			case VtCChar:	res = (int)_u.cc;			break;
			case VtWChar:	res = (int)_u.wc;			break;
			case VtCString:	res = (int)SWString::toInt32(_u.cstr);	break;
			case VtWString:	res = (int)SWString::toInt32(_u.wstr);	break;
			case VtDate:	res = (int)_u.julian;		break;
			case VtTime:	res = (int)_u.ts.tv_sec;	break;
			case VtMoney:	res = (int)_u.money;	break;

			default:		res = 0;							break;
			}

		return res;
		}



unsigned int
SWValue::toUInteger() const
		{
		unsigned int	res=0;

		switch (_type)
			{
			case VtBoolean:	res = (unsigned int)_u.b;			break;
			case VtInt8:	res = (unsigned int)_u.i8;			break;
			case VtInt16:	res = (unsigned int)_u.i16;			break;
			case VtInt32:	res = (unsigned int)_u.i32;			break;
			case VtInt64:	res = (unsigned int)_u.i64;			break;
			case VtUInt8:	res = (unsigned int)_u.ui8;			break;
			case VtUInt16:	res = (unsigned int)_u.ui16;			break;
			case VtUInt32:	res = (unsigned int)_u.ui32;			break;
			case VtUInt64:	res = (unsigned int)_u.ui64;			break;
			case VtFloat:	res = (unsigned int)_u.f;				break;
			case VtDouble:	res = (unsigned int)_u.d;				break;
			case VtCChar:	res = (unsigned int)_u.cc;			break;
			case VtWChar:	res = (unsigned int)_u.wc;			break;
			case VtCString:	res = (unsigned int)SWString::toInt32(_u.cstr);	break;
			case VtWString:	res = (unsigned int)SWString::toInt32(_u.wstr);	break;
			case VtDate:	res = (unsigned int)_u.julian;		break;
			case VtTime:	res = (unsigned int)_u.ts.tv_sec;	break;
			case VtMoney:	res = (unsigned int)_u.money;	break;

			default:		res = 0;							break;
			}

		return res;
		}



long
SWValue::toLong() const
		{
		long	res=0;

		switch (_type)
			{
			case VtBoolean:	res = (long)_u.b;			break;
			case VtInt8:	res = (long)_u.i8;			break;
			case VtInt16:	res = (long)_u.i16;			break;
			case VtInt32:	res = (long)_u.i32;			break;
			case VtInt64:	res = (long)_u.i64;			break;
			case VtUInt8:	res = (long)_u.ui8;			break;
			case VtUInt16:	res = (long)_u.ui16;			break;
			case VtUInt32:	res = (long)_u.ui32;			break;
			case VtUInt64:	res = (long)_u.ui64;			break;
			case VtFloat:	res = (long)_u.f;				break;
			case VtDouble:	res = (long)_u.d;				break;
			case VtCChar:	res = (long)_u.cc;			break;
			case VtWChar:	res = (long)_u.wc;			break;
			case VtCString:	res = (long)SWString::toInt32(_u.cstr);	break;
			case VtWString:	res = (long)SWString::toInt32(_u.wstr);	break;
			case VtDate:	res = (long)_u.julian;		break;
			case VtTime:	res = (long)_u.ts.tv_sec;	break;
			case VtMoney:	res = (long)_u.money;	break;

			default:		res = 0;							break;
			}

		return res;
		}


unsigned long
SWValue::toULong() const
		{
		unsigned long	res=0;

		switch (_type)
			{
			case VtBoolean:	res = (unsigned long)_u.b;			break;
			case VtInt8:	res = (unsigned long)_u.i8;			break;
			case VtInt16:	res = (unsigned long)_u.i16;			break;
			case VtInt32:	res = (unsigned long)_u.i32;			break;
			case VtInt64:	res = (unsigned long)_u.i64;			break;
			case VtUInt8:	res = (unsigned long)_u.ui8;			break;
			case VtUInt16:	res = (unsigned long)_u.ui16;			break;
			case VtUInt32:	res = (unsigned long)_u.ui32;			break;
			case VtUInt64:	res = (unsigned long)_u.ui64;			break;
			case VtFloat:	res = (unsigned long)_u.f;				break;
			case VtDouble:	res = (unsigned long)_u.d;				break;
			case VtCChar:	res = (unsigned long)_u.cc;			break;
			case VtWChar:	res = (unsigned long)_u.wc;			break;
			case VtCString:	res = (unsigned long)SWString::toInt32(_u.cstr);	break;
			case VtWString:	res = (unsigned long)SWString::toInt32(_u.wstr);	break;
			case VtDate:	res = (unsigned long)_u.julian;		break;
			case VtTime:	res = (unsigned long)_u.ts.tv_sec;	break;
			case VtMoney:	res = (unsigned long)_u.money;	break;

			default:		res = 0;							break;
			}

		return res;
		}


float
SWValue::toFloat() const
		{
		float	res=0;

		switch (_type)
			{
			case VtBoolean:	res = (float)_u.b;			break;
			case VtInt8:	res = (float)_u.i8;			break;
			case VtInt16:	res = (float)_u.i16;			break;
			case VtInt32:	res = (float)_u.i32;			break;
			case VtInt64:	res = (float)_u.i64;			break;
			case VtUInt8:	res = (float)_u.ui8;			break;
			case VtUInt16:	res = (float)_u.ui16;			break;
			case VtUInt32:	res = (float)_u.ui32;			break;
#if _MSC_VER < 0x700
			// The older MS compilers (VS6) must convert to signed int before conversion to double
			case VtUInt64:	res = (float)(sw_int64_t)_u.ui64;	break;
#else
			case VtUInt64:	res = (float)_u.ui64;			break;
#endif
			case VtFloat:	res = (float)_u.f;				break;
			case VtDouble:	res = (float)_u.d;				break;
			case VtCChar:	res = (float)_u.cc;			break;
			case VtWChar:	res = (float)_u.wc;			break;
			case VtCString:	res = (float)SWString::toDouble(_u.cstr);	break;
			case VtWString:	res = (float)SWString::toDouble(_u.wstr);	break;
			case VtDate:	res = (float)_u.julian;		break;
			case VtTime:	res = (float)_u.ts.tv_sec + ((float)_u.ts.tv_nsec / (float)1000000000.0);	break;
			case VtMoney:	res = (float)_u.money / (float)100.0;	break;

			default:		res = 0;							break;
			}

		return res;
		}


double
SWValue::toDouble() const
		{
		double	res=0;

		switch (_type)
			{
			case VtBoolean:	res = (double)_u.b;			break;
			case VtInt8:	res = (double)_u.i8;			break;
			case VtInt16:	res = (double)_u.i16;			break;
			case VtInt32:	res = (double)_u.i32;			break;
			case VtInt64:	res = (double)_u.i64;			break;
			case VtUInt8:	res = (double)_u.ui8;			break;
			case VtUInt16:	res = (double)_u.ui16;			break;
			case VtUInt32:	res = (double)_u.ui32;			break;
#if _MSC_VER < 0x700
			// The older MS compilers (VS6) must convert to signed int before conversion to double
			case VtUInt64:	res = (double)(sw_int64_t)_u.ui64;	break;
#else
			case VtUInt64:	res = (double)_u.ui64;			break;
#endif

			case VtFloat:	res = (double)_u.f;				break;
			case VtDouble:	res = (double)_u.d;				break;
			case VtCChar:	res = (double)_u.cc;			break;
			case VtWChar:	res = (double)_u.wc;			break;
			case VtCString:	res = (double)SWString::toDouble(_u.cstr);	break;
			case VtWString:	res = (double)SWString::toDouble(_u.wstr);	break;
			case VtDate:	res = (double)_u.julian;		break;
			case VtTime:	res = (double)_u.ts.tv_sec + ((double)_u.ts.tv_nsec / 1000000000.0);	break;
			case VtMoney:	res = (double)_u.money / 100.0;	break;

			default:		res = 0;							break;
			}

		return res;
		}


SWString
SWValue::toHexDataString() const
		{
		SWString	res="";
		sw_byte_t	*bp;
		int			nbytes=(int)_datalen;

		bp = (sw_byte_t *)data();

		for (int i=0;i<nbytes;i++)
			{
			char	hexstr[8];

			sw_snprintf(hexstr, sizeof(hexstr), "%02X", *bp++);
			if (i > 0) res += " ";
			res += hexstr;
			}

		return res;
		}



SWString
SWValue::toString() const
		{
		SWString	res;

		switch (_type)
			{
			case VtBoolean:	res = booleanToString(_u.b);	break;
			case VtInt8:	res.format("%d", _u.i8);		break;
			case VtInt16:	res.format("%d", _u.i16);		break;
			case VtInt32:	res.format("%d", _u.i32);		break;
			case VtInt64:	res.format("%lld", _u.i64);		break;
			case VtUInt8:	res.format("%u", _u.ui8);		break;
			case VtUInt16:	res.format("%u", _u.ui16);		break;
			case VtUInt32:	res.format("%u", _u.ui32);		break;
			case VtUInt64:	res.format("%llu", _u.ui64);	break;
			case VtFloat:	res.format("%f", _u.f);			break;
			case VtDouble:	res.format("%f", _u.d);			break;
			case VtCChar:	res = _u.cc;					break;
			case VtWChar:	res = _u.wc;					break;
			case VtCString:	res = _u.cstr;					break;
			case VtWString:	res = _u.wstr;					break;
			case VtPointer: res = "";						break;
			case VtData:	res = "";						break;
			case VtDate:	res = SWTime(SWDate(_u.julian, true)).format("%Y%m%d");	break;
			case VtTime:	res = SWTime(_u.ts).format("%Y%m%d %T.%f");	break;
			case VtMoney:	res = SWMoney(_u.money).toString();	break;

			default:
				// Do nothing
				break;
			}

		return res;
		}



int
SWValue::toChar() const
		{
		int		res=0;

		switch (_type)
			{
			case VtBoolean:	res = booleanToChar(_u.b);		break;
			case VtCString:	res = _u.cstr[0];				break;
			case VtWString:	res = _u.wstr[0];				break;
			case VtCChar:	res = _u.cc;					break;
			case VtWChar:	res = _u.wc;					break;
			default: 		res = ' ';						break;
			}

		return res;
		}



SWDate
SWValue::toDate() const
		{
		SWDate	res;

		switch (_type)
			{
			case VtBoolean:	res = SWDate((int)_u.b, true);			break;
			case VtInt8:	res = SWDate((int)_u.i8, true);			break;
			case VtInt16:	res = SWDate((int)_u.i16, true);			break;
			case VtInt32:	res = SWDate((int)_u.i32, true);			break;
			case VtInt64:	res = SWDate((int)_u.i64, true);			break;
			case VtUInt8:	res = SWDate((int)_u.ui8, true);			break;
			case VtUInt16:	res = SWDate((int)_u.ui16, true);			break;
			case VtUInt32:	res = SWDate((int)_u.ui32, true);			break;
			case VtUInt64:	res = SWDate((int)_u.ui64, true);			break;
			case VtFloat:	res = SWDate((int)_u.f, true);				break;
			case VtDouble:	res = SWDate((int)_u.d, true);				break;
			case VtCChar:	res = SWDate((int)_u.cc, true);			break;
			case VtWChar:	res = SWDate((int)_u.wc, true);			break;
			case VtCString:	res = SWDate(SWString::toInt32(_u.cstr), true);	break;
			case VtWString:	res = SWDate(SWString::toInt32(_u.wstr), true);	break;
			case VtDate:	res = SWDate(_u.julian, true);		break;
			case VtTime:	res = SWDate(SWTime(_u.ts));	break;

			default:
				// Do nothing
				break;
			}

		return res;
		}



SWTime
SWValue::toTime() const
		{
		SWTime	res;

		switch (_type)
			{
			case VtBoolean:	res = SWTime((double)_u.b);			break;
			case VtInt8:	res = SWTime((double)_u.i8);			break;
			case VtInt16:	res = SWTime((double)_u.i16);			break;
			case VtInt32:	res = SWTime((double)_u.i32);			break;
			case VtInt64:	res = SWTime((double)_u.i64);			break;
			case VtUInt8:	res = SWTime((double)_u.ui8);			break;
			case VtUInt16:	res = SWTime((double)_u.ui16);			break;
			case VtUInt32:	res = SWTime((double)_u.ui32);			break;
#if _MSC_VER < 0x700
			// The older MS compilers (VS6) must convert to signed int before conversion to double
			case VtUInt64:	res = SWTime((double)(sw_int64_t)_u.ui64);			break;
#else
			case VtUInt64:	res = SWTime((double)_u.ui64);			break;
#endif

			case VtFloat:	res = SWTime((double)_u.f);				break;
			case VtDouble:	res = SWTime((double)_u.d);				break;
			case VtCChar:	res = SWTime((double)_u.cc);			break;
			case VtWChar:	res = SWTime((double)_u.wc);			break;
			case VtCString:	res = SWTime(SWString::toDouble(_u.cstr));	break;
			case VtWString:	res = SWTime(SWString::toDouble(_u.wstr));	break;
			case VtDate:	res = SWDate(_u.julian, true);		break;
			case VtTime:	res = _u.ts;					break;

			default:
				// Do nothing
				break;
			}

		return res;
		}

SWMoney
SWValue::toMoney() const
		{
		SWMoney	res;

		switch (_type)
			{
			case VtBoolean:	res = (sw_money_t)_u.b;			break;
			case VtInt8:	res = (sw_money_t)_u.i8;			break;
			case VtInt16:	res = (sw_money_t)_u.i16;			break;
			case VtInt32:	res = (sw_money_t)_u.i32;			break;
			case VtInt64:	res = (sw_money_t)_u.i64;			break;
			case VtUInt8:	res = (sw_money_t)_u.ui8;			break;
			case VtUInt16:	res = (sw_money_t)_u.ui16;			break;
			case VtUInt32:	res = (sw_money_t)_u.ui32;			break;
			case VtUInt64:	res = (sw_money_t)_u.ui64;			break;
			case VtFloat:	res = (sw_money_t)_u.f;				break;
			case VtDouble:	res = (sw_money_t)_u.d;				break;
			case VtCChar:	res = (sw_money_t)_u.cc;			break;
			case VtWChar:	res = (sw_money_t)_u.wc;			break;
			case VtCString:	res = SWMoney(_u.cstr);	break;
			case VtWString:	res = SWMoney(_u.wstr);	break;
			case VtMoney:	res = _u.money;	break;

			default:		res = 0;							break;
			}

		return res;
		}


size_t			
SWValue::length() const
		{
		size_t	res=0;

		switch (_type)
			{
			case VtNone:		res = 0;						break;
			case VtBoolean:		res = sizeof(_u.b);				break;
			case VtInt8:		res = sizeof(_u.i8);			break;
			case VtInt16:		res = sizeof(_u.i16);			break;
			case VtInt32:		res = sizeof(_u.i32);			break;
			case VtInt64:		res = sizeof(_u.i64);			break;
			case VtUInt8:		res = sizeof(_u.ui8);			break;
			case VtUInt16:		res = sizeof(_u.ui16);			break;
			case VtUInt32:		res = sizeof(_u.ui32);			break;
			case VtUInt64:		res = sizeof(_u.ui64);			break;
			case VtFloat:		res = sizeof(_u.f);				break;
			case VtDouble:		res = sizeof(_u.d);				break;
			case VtCChar:		res = sizeof(_u.cc);			break;
			case VtWChar:		res = sizeof(_u.wc);			break;
			case VtDate:		res = sizeof(_u.julian);		break;
			case VtTime:		res = sizeof(_u.ts);			break;
			case VtMoney:		res = sizeof(_u.money);			break;
			case VtCurrency:	res = sizeof(_u.money);			break;

			default:			res = _datalen;					break;
			}

		return res;
		}


const void *
SWValue::data() const
		{
		const void	*pData=&_u;

		switch (_type)
			{
			case VtCString:	pData = _u.cstr;	break;
			case VtWString:	pData = _u.wstr;	break;
			case VtPointer:	pData = _u.pData;	break;
			case VtData:	pData = _u.pData;	break;

			default:
				// Point to the union itself
				pData = &_u;
				break;
			}

		return pData;
		}

bool
SWValue::isNumeric() const
		{
		bool	res=false;

		switch (_type)
			{
			case VtInt8:
			case VtInt16:
			case VtInt32:
			case VtInt64:
			case VtUInt8:
			case VtUInt16:
			case VtUInt32:
			case VtUInt64:
			case VtFloat:
			case VtDouble:
				res = true;
				break;

			default:
				res = false;
				break;
			}

		return res;
		}


bool
SWValue::isString() const
		{
		bool	res=false;

		switch (_type)
			{
			case VtCString:
			case VtWString:
				res = true;
				break;

			default:
				res = false;
				break;
			}

		return res;
		}


int
SWValue::compareTo(const SWValue &other, bool checkTypes) const
		{
		int		res=0;

		if (_type == other._type)
			{
#define COMPARE(x)	(x > other.x?1:(x < other.x?-1:0))
			switch (_type)
				{
				case VtBoolean:	res = COMPARE(_u.b);							break;
				case VtInt8:	res = COMPARE(_u.i8);							break;
				case VtInt16:	res = COMPARE(_u.i16);							break;
				case VtInt32:	res = COMPARE(_u.i32);							break;
				case VtInt64:	res = COMPARE(_u.i64);							break;
				case VtUInt8:	res = COMPARE(_u.ui8);							break;
				case VtUInt16:	res = COMPARE(_u.ui16);							break;
				case VtUInt32:	res = COMPARE(_u.ui32);							break;
				case VtUInt64:	res = COMPARE(_u.ui64);							break;
				case VtFloat:	res = COMPARE(_u.f);							break;
				case VtDouble:	res = COMPARE(_u.d);							break;
				case VtCChar:	res = COMPARE(_u.cc);							break;
				case VtWChar:	res = COMPARE(_u.wc);							break;
				case VtCString:	res = strcmp(_u.cstr, other._u.cstr);			break;
				case VtWString:	res = wcscmp(_u.wstr, other._u.wstr);			break;
				case VtMoney:	res = COMPARE(_u.money);						break;

				case VtPointer:
					res = COMPARE(_u.pData);
					break;

				case VtData:
					{
					size_t	minlen=_datalen;

					if (other._datalen < minlen) minlen = other._datalen;
					res = memcmp(data(), other.data(), minlen);
					if (res == 0) res = COMPARE(_datalen);
					}
					break;

				case VtDate:	res = COMPARE(_u.julian);						break;
				case VtTime:
					res = COMPARE(_u.ts.tv_sec);
					if (res == 0) res = COMPARE(_u.ts.tv_nsec);
					break;

				case VtNone:	res = 0;										break;
				}

			}
		else if (checkTypes)
			{
			// Types are not the same and caller has not requested "smart" comparision
			// so return the result of comparing the types.
			res = (int)_type - (int)other._type;
			}
		else
			{
			// Types are not the same, but caller has requested "smart" comparision
			// so we need to do our best.
			if (isString() && other.isString())
				{
				// We can so a string-like comparison.
				res = toString().compareTo(other.toString());
				}
			else if (isNumeric() && other.isNumeric())
				{
				// Numeric comparison
				if (_type == VtUInt64 && _u.ui64 > SW_INT64_MAX)
					{
					// We know that other._type is NOT a 64-bit int since types are not the same
					// therefore we can assume this is bigger than other
					res = 1;
					}
				else if (other._type == VtUInt64 && other._u.ui64 > SW_INT64_MAX)
					{
					// We know that _type is NOT a 64-bit int since types are not the same
					// therefore we can assume this is less than other
					res = -1;
					}
				else
					{
					// We can use conversion to sw_int64_t to do the hard work for us.
					sw_int64_t	v=toInt64()-other.toInt64();

					if (v < 0) res = -1;
					else if (v > 0) res = 0;
					}
				}
			else
				{
				// Default - use toInteger
				res = toInteger() - other.toInteger();
				}
			}

		return res;
		}


sw_status_t
SWValue::writeToStream(SWOutputStream &stream) const
		{
		sw_status_t		res=SW_STATUS_SUCCESS;

		// We write a buffer as follows:
		// Type (2 bytes)
		// Buffer Length (4 bytes)
		// Data
		res = stream.write((sw_uint16_t)_type);
		switch (_type)
			{
			case VtNone:
				break;

			case VtBoolean:
				res = stream.write(_u.b);
				break;

			case VtInt8:
				res = stream.write(_u.i8);
				break;

			case VtInt16:
				res = stream.write(_u.i16);
				break;

			case VtInt32:
				res = stream.write(_u.i32);
				break;

			case VtInt64:
				res = stream.write(_u.i64);
				break;

			case VtUInt8:
				res = stream.write(_u.ui8);
				break;

			case VtUInt16:
				res = stream.write(_u.ui16);
				break;

			case VtUInt32:
				res = stream.write(_u.ui32);
				break;

			case VtUInt64:
				res = stream.write(_u.ui64);
				break;

			case VtFloat:
				res = stream.write(_u.f);
				break;

			case VtDouble:
				res = stream.write(_u.d);
				break;

			case VtCChar:
				res = stream.write((sw_uint8_t)_u.cc);
				break;

			case VtWChar:
				res = stream.write((sw_uint32_t)_u.wc);
				break;

			case VtCString:
				// Write as a SWString object to make reading-back easy
				res = stream.write(SWStringReference(_u.cstr));
				break;

			case VtWString:
				// Write as a SWString object to make reading-back easy
				res = stream.write(SWStringReference(_u.wstr));
				break;

			case VtDate:
				res = stream.write(_u.julian);
				break;

			case VtTime:
				res = stream.write((sw_int64_t)_u.ts.tv_sec);
				if (res == SW_STATUS_SUCCESS) res = stream.write((sw_uint32_t)_u.ts.tv_nsec);
				break;

			case VtMoney:
				res = stream.write(_u.money);
				break;

			case VtPointer:
				{
				sw_uint64_t	tmp;

#if defined(SW_PLATFORM_WINDOWS)
	// The windows compiler normally (and rightly so) warns about the cast from
	// pointer to integer. However in this case we know better.
	#pragma warning(disable: 4311)
#endif

				tmp = (sw_uint64_t)(sw_uintptr_t)_u.pData;

#if defined(SW_PLATFORM_WINDOWS)
	// The windows compiler normally (and rightly so) warns about the cast from
	// pointer to integer. However in this case we know better.
	#pragma warning(default: 4311)
#endif

				res = stream.write(tmp);
				}
				break;

			case VtData:
				res = stream.write((sw_uint32_t)length());
				if (res == SW_STATUS_SUCCESS) res = stream.writeData(data(), (sw_uint32_t)length());
				break;
			}

		return res;
		}


sw_status_t
SWValue::readFromStream(SWInputStream &stream)
		{
		sw_status_t		res=SW_STATUS_SUCCESS;
		sw_uint16_t		type;

		clear();
		res = stream.read(type);
		if (res == SW_STATUS_SUCCESS)
			{
			switch (type)
				{
				case VtNone:
					break;

				case VtBoolean:
					{
					bool	v;
					
					res = stream.read(v);
					if (res == SW_STATUS_SUCCESS) setValue(v);
					}
					break;

				case VtInt8:
					{
					sw_int8_t	v;
					
					res = stream.read(v);
					if (res == SW_STATUS_SUCCESS) setValue(v);
					}
					break;

				case VtInt16:
					{
					sw_int16_t	v;
					
					res = stream.read(v);
					if (res == SW_STATUS_SUCCESS) setValue(v);
					}
					break;

				case VtInt32:
					{
					sw_int32_t	v;
					
					res = stream.read(v);
					if (res == SW_STATUS_SUCCESS) setValue(v);
					}
					break;

				case VtInt64:
					{
					sw_int64_t	v;
					
					res = stream.read(v);
					if (res == SW_STATUS_SUCCESS) setValue(v);
					}
					break;

				case VtUInt8:
					{
					sw_uint8_t	v;
					
					res = stream.read(v);
					if (res == SW_STATUS_SUCCESS) setValue(v);
					}
					break;

				case VtUInt16:
					{
					sw_uint16_t	v;
					
					res = stream.read(v);
					if (res == SW_STATUS_SUCCESS) setValue(v);
					}
					break;

				case VtUInt32:
					{
					sw_uint32_t	v;
					
					res = stream.read(v);
					if (res == SW_STATUS_SUCCESS) setValue(v);
					}
					break;

				case VtUInt64:
					{
					sw_uint64_t	v;
					
					res = stream.read(v);
					if (res == SW_STATUS_SUCCESS) setValue(v);
					}
					break;

				case VtFloat:
					{
					float	v;
					
					res = stream.read(v);
					if (res == SW_STATUS_SUCCESS) setValue(v);
					}
					break;

				case VtDouble:
					{
					double	v;
					
					res = stream.read(v);
					if (res == SW_STATUS_SUCCESS) setValue(v);
					}
					break;

				case VtCChar:
					{
					sw_uint8_t	v;
					
					res = stream.read(v);
					if (res == SW_STATUS_SUCCESS) setValue(v, (ValueType)type);
					}
					break;

				case VtWChar:
					{
					sw_uint32_t	v;
					
					res = stream.read(v);
					if (res == SW_STATUS_SUCCESS) setValue(v, (ValueType)type);
					}
					break;

				case VtCString:
					{
					SWCString	v;
					
					res = stream.read(v);
					if (res == SW_STATUS_SUCCESS) setValue(v);
					}
					break;

				case VtWString:
					{
					SWWString	v;
					
					res = stream.read(v);
					if (res == SW_STATUS_SUCCESS) setValue(v);
					}
					break;

				case VtDate:
					{
					sw_int32_t	v;
					
					res = stream.read(v);
					if (res == SW_STATUS_SUCCESS) setValue(SWDate(v, true));
					}
					break;

				case VtTime:
					{
					sw_internal_time_t	v;

					res = stream.read(v.tv_sec);
					if (res == SW_STATUS_SUCCESS) res = stream.read(v.tv_nsec);
					if (res == SW_STATUS_SUCCESS)
						{
						setValue(v);
						}
					}
					break;

				case VtMoney:
					{
					sw_money_t	v;
					
					res = stream.read(v);
					if (res == SW_STATUS_SUCCESS) setValue(SWMoney(v));
					}
					break;

				case VtPointer:
					{
					sw_uint64_t	v;

					res = stream.read(v);
					if (res == SW_STATUS_SUCCESS)
						{
#if defined(SW_PLATFORM_WINDOWS)
	// The windows compiler normally (and rightly so) warns about the cast from
	// pointer to integer. However in this case we know better.
	#pragma warning(disable: 4312)
#endif
						setValue((void *)(sw_uintptr_t)v, 0, false);
#if defined(SW_PLATFORM_WINDOWS)
	// The windows compiler normally (and rightly so) warns about the cast from
	// pointer to integer. However in this case we know better.
	#pragma warning(default: 4312)
#endif


						}
					}
					break;

				case VtData:
					{
					sw_uint32_t	datalen;

					res = stream.read(datalen);
					if (res == SW_STATUS_SUCCESS)
						{
						init(VtData, datalen, true);
						_u.pData = (char *)__pMemoryManager->malloc(datalen);
						if (_u.pData == NULL) res = SW_STATUS_OUT_OF_MEMORY;
						else res = stream.readData(_u.pData, datalen);
						}
					}
					break;

				default:
					res = SW_STATUS_INVALID_DATA;
					break;
				}
			}

		return res;
		}



sw_status_t
SWValue::readDataFromFile(const SWString &filename)
		{
		SWFile		file;
		sw_status_t	res;

		res = file.open(filename, SWFile::ModeBinary | SWFile::ModeReadOnly);
		if (res == SW_STATUS_SUCCESS)
			{
			sw_size_t	filesize=(sw_size_t)file.size();

			clearValue(false);

			init(VtData, filesize, true);

			// Copy the data
			_u.pData = (char *)__pMemoryManager->malloc(filesize);
			file.read(_u.pData, filesize);
			file.close();

			setChangedFlag(true);
			}

		return res;
		}


sw_status_t
SWValue::writeDataToFile(const SWString &filename)
		{
		SWFile		file;
		sw_status_t	res;

		res = file.open(filename, SWFile::ModeBinary | SWFile::ModeCreate | SWFile::ModeTruncate | SWFile::ModeWriteOnly);
		if (res == SW_STATUS_SUCCESS)
			{
			file.write(data(), length());
			file.close();
			}

		return res;
		}





