/**
*** @file		SWFileAttributes.h
*** @brief		A class to hold the information attributes of a file.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWFileAttributes class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWFileAttributes_h__
#define __SWFileAttributes_h__

#include <swift/swift.h>

#include <swift/SWObject.h>
#include <swift/SWArray.h>
#include <swift/SWString.h>
#include <swift/SWTime.h>






#if defined(SW_PLATFORM_WINDOWS)
	#pragma warning(disable: 4245)
#endif


// Forward declarations
class SWFileSecurity;


/**
*** @brief	A portable representation of file attributes.
***
*** This class provides a portable (O/S independent) representation of
*** a file's attributes.
***
*** @see		SWFolder, SWFileInfo, SWFile.
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWFileAttributes
		{
friend class SWFile;
friend class SWFileInfo;
friend class SWFolder;

public:
		/// File types
		enum FileType
			{
			FileTypeUnknown			= 0x0,	///< File type is unknown
			FileTypeFile			= 0x1,	///< File is a file
			FileTypeFolder			= 0x2,	///< File is a folder or directory
			FileTypeDirectory		= 0x2,	///< File is a folder or directory
			FileTypeDevice			= 0x3,	///< File is a device
			FileTypeCharDevice		= 0x4,	///< File is a character device
			FileTypeBlockDevice		= 0x5,	///< File is a block device
			FileTypeSocket			= 0x6,	///< File is a socket
			FileTypeDoor			= 0x7,	///< File is a door
			FileTypeFifo			= 0x8,	///< File is a FIFO
			FileTypeXenixNamedFile	= 0x9,	///< File is a Xenix named file
			FileTypeNetworkSpecial	= 0xa,	///< File is a network special file.
			FileTypePipe			= 0xb,	///< File is a pipe
			FileTypeSymbolicLink	= 0xc	///< File is a symbolic link
			};

		/// File attributes
		enum FileAttr
			{
			// Unix-style attributes
			FileAttrOwnerRead		= 0x00000100,	///< File is readable by the owner
			FileAttrOwnerWrite		= 0x00000080,	///< File is writeable by the owner
			FileAttrOwnerExecute	= 0x00000040,	///< File is executeable by the owner
			FileAttrGroupRead		= 0x00000020,	///< File is readable by the group
			FileAttrGroupWrite		= 0x00000010,	///< File is writeable by the group
			FileAttrGroupExecute	= 0x00000008,	///< File is executeable by the group
			FileAttrWorldRead		= 0x00000004,	///< File is readable by the everyone else
			FileAttrWorldWrite		= 0x00000002,	///< File is writeable by the everyone else
			FileAttrWorldExecute	= 0x00000001,	///< File is executeable by the everyone else

			FileAttrSetUserID		= 0x00000800,
			FileAttrSetGroupID		= 0x00000400,
			FileAttrSticky			= 0x00000200,

			/// The file or directory is read-only. Applications can read the file but
			/// cannot write to it or delete it. In the case of a directory, applications
			/// cannot delete it. 
			FileAttrReadOnly		= 0x00001124,

			/// The file or directory is hidden. It is not included in an ordinary directory listing. 
			FileAttrHidden			= 0x00002000,

			/// The file or directory is part of, or is used exclusively by, the operating system. 
			FileAttrSystem			= 0x00004000,

			/// The file or directory is an archive file or directory. Applications use
			/// this attribute to mark files for backup or removal. 
			FileAttrArchive			= 0x00008000,

			/// The file is being used for temporary storage. File systems attempt to keep
			/// all of the data in memory for quicker access rather than flushing the data
			/// back to mass storage. A temporary file should be deleted by the application
			/// as soon as it is no longer needed. 
			FileAttrTemporary		= 0x00010000,

			/// The file is a sparse file. 
			FileAttrSparse			= 0x00020000,

			/// The file or directory is compressed. For a file, this means that all of the
			/// data in the file is compressed. For a directory, this means that compression
			/// is the default for newly created files and subdirectories. 
			FileAttrCompressed		= 0x00040000,

			/// The data of the file is not immediately available. Indicates that the file
			/// data has been physically moved to offline storage. 
			FileAttrOffline			= 0x00080000,

			/// The file is excluded from system indexing.
			FileAttrNotIndexed		= 0x00100000,

			/// The file or directory is encrypted. For a file, this means that all data
			/// streams are encrypted. For a directory, this means that encryption is the
			/// default for newly created files and subdirectories. 
			FileAttrEncrypted		= 0x00200000,

			/// The file is a reparse point
			FileAttrReparsePoint	= 0x00800000,

			/// The file is a symbolic link
			FileAttrSymbolicLink			= 0x01000000,

			/// Record locking is enforced on the file
			FileAttrRecordLocking	= 0x02000000,

			/// The file or directory has no other attributes set.
			FileAttrNormal			= 0x80000000
			};

public:
		/// Default constructor
		SWFileAttributes();

		/// Constructor for file creation to specify the file type, attributes
		/// and security attributes
		SWFileAttributes(FileType type, sw_fileattr_t attribs, SWFileSecurity *psd=0);

		/// Copy constructor
		SWFileAttributes(const SWFileAttributes &other);

		/// Assignment operator
		SWFileAttributes &	operator=(const SWFileAttributes &other);

		/// Return the type of file
		FileType			type() const		{ return m_type; }

		/// Return the file attribute flags
		sw_fileattr_t		attributes() const	{ return m_attribs; }

		/*
		*** @brief	Return the attributes as a string (unix ls style)
		***
		*** This method returns the attributes as an ls-like string.
		***
		*** The first char indicates the file type.
		*** - d     the entry is a directory;
		*** - D     the entry is a door;
		*** - l     the entry is a symbolic link;
		*** - b     the entry is a block special file;
		*** - c     the entry is a character special file;
		*** - p     the entry is a fifo (or "named pipe") special file;
		*** - s     the entry is an AF_UNIX address family socket;
		*** - -     the entry is an ordinary file;
		***
		*** The next 3 groups of 3 characters are Read/Write/Execute permissions for
		*** Owner, Group, and World respectively.
		***
		*** Finally a plus character is appended if there is an acl
		*** associated with the entry.
		**/
		SWString			getUnixModeString() const;

		/// Return the attributes as a string
		SWString			getAttributesString() const;

		/**
		*** @brief	Test for the specified attributes
		***
		*** This method tests the specified attributes against those held in this object
		*** and returns a boolean to indicate a successful match. The wantAny parameter
		*** specifies if a match is successful only when all of the attributes match (wantAny=false)
		*** or when at least one match is found (wantAny=true).
		***
		*** @param[in]	wanted		The attributes to test for.
		*** @param[in]	wantAny		A flag indicating if matching will be success for any match (true)
		***							or all matching (false).
		***
		*** @return					A boolean indicating the result of the match process.
		**/
		bool				hasAttributes(sw_fileattr_t wanted, bool wantAny=true);

		/**
		*** @name	Test for the specified attribute or type
		*** The following method test if the file attributes has a specific attribute
		*** or type as indicated by the method name.
		**/
		//@{

		// Some useful shortcuts
		bool				isHidden() const			{ return ((m_attribs & FileAttrHidden) != 0); }
		bool				isSystem() const			{ return ((m_attribs & FileAttrSystem) != 0); }
		bool				isReadOnly() const			{ return ((m_attribs & FileAttrReadOnly) != 0); }
		bool				isArchive() const			{ return ((m_attribs & FileAttrArchive) != 0); }
		bool				isNormal() const			{ return ((m_attribs & FileAttrNormal) != 0); }
		bool				isTemporary() const			{ return ((m_attribs & FileAttrTemporary) != 0); }
		bool				isSparse() const			{ return ((m_attribs & FileAttrSparse) != 0); }
		bool				isCompressed() const		{ return ((m_attribs & FileAttrCompressed) != 0); }
		bool				isOffline() const			{ return ((m_attribs & FileAttrOffline) != 0); }
		bool				isNotIndexed() const		{ return ((m_attribs & FileAttrNotIndexed) != 0); }
		bool				isEncrypted() const			{ return ((m_attribs & FileAttrEncrypted) != 0); }
		bool				isSymbolicLink() const		{ return ((m_attribs & FileAttrSymbolicLink) != 0); }
		bool				isReparsePoint() const		{ return ((m_attribs & FileAttrReparsePoint) != 0); }

		// Read/Write/Execute etc
		bool				isSticky() const			{ return ((m_attribs & FileAttrSticky) != 0); }
		bool				isSetUserID() const			{ return ((m_attribs & FileAttrSetUserID) != 0); }
		bool				isSetGroupID() const		{ return ((m_attribs & FileAttrSetGroupID) != 0); }
		bool				isOwnerRead() const			{ return ((m_attribs & FileAttrOwnerRead) != 0); }
		bool				isOwnerWrite() const		{ return ((m_attribs & FileAttrOwnerWrite) != 0); }
		bool				isOwnerExecute() const		{ return ((m_attribs & FileAttrOwnerExecute) != 0); }
		bool				isGroupRead() const			{ return ((m_attribs & FileAttrGroupRead) != 0); }
		bool				isGroupWrite() const		{ return ((m_attribs & FileAttrGroupWrite) != 0); }
		bool				isGroupExecute() const		{ return ((m_attribs & FileAttrGroupExecute) != 0); }
		bool				isWorldRead() const			{ return ((m_attribs & FileAttrWorldRead) != 0); }
		bool				isWorldWrite() const		{ return ((m_attribs & FileAttrWorldWrite) != 0); }
		bool				isWorldExecute() const		{ return ((m_attribs & FileAttrWorldExecute) != 0); }

		// File Type
		bool				isFifo() const				{ return (type() == FileTypeFifo); }
		bool				isPipe() const				{ return (type() == FileTypePipe); }
		bool				isDevice() const			{ return (type() == FileTypeDevice || type() == FileTypeCharDevice || type() == FileTypeBlockDevice); }
		bool				isCharDevice() const		{ return (type() == FileTypeCharDevice); }
		bool				isBlockDevice() const		{ return (type() == FileTypeBlockDevice); }
		bool				isSocket() const			{ return (type() == FileTypeSocket); }
		bool				isDirectory() const			{ return (type() == FileTypeDirectory); }
		bool				isFile() const				{ return (type() == FileTypeFile); }
		bool				isDoor() const				{ return (type() == FileTypeDoor); }
		bool				isXenixNamedFile() const	{ return (type() == FileTypeXenixNamedFile); }
		bool				isNetworkSpecial() const	{ return (type() == FileTypeNetworkSpecial); }
		//@}

protected:
		void				set(const SWString &name, sw_uint32_t unixmode, sw_uint32_t winattrs, sw_uint_t symlink);
		void				clear()						{ m_attribs = 0; m_type = FileTypeUnknown; }

protected:
		FileType		m_type;		///< The type of the file.
		sw_fileattr_t	m_attribs;	///< The file attributes (containing the type and attributes combined)
		};


#if defined(SW_PLATFORM_WINDOWS)
	#pragma warning(default: 4245)
#endif




#endif
