/**
*** @file		SWMemoryOutputStream.cpp
*** @brief		A class representing a stream of bytes being written to a memory buffer.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWMemoryOutputStream class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWMemoryOutputStream.h>
#include <memory.h>



// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





/*
** Memory output stream constructor
*/
SWMemoryOutputStream::SWMemoryOutputStream() :
	m_pBuffer(&m_defaultbuf),
	m_datalen(0)
		{
		}


/*
** Memory output stream constructor with supplied buffer.
**
** The buffer size is set to zero ready for writing if append
** is false.
SWMemoryOutputStream::SWMemoryOutputStream(SWBuffer &buf, bool append) :
	m_pBuffer(&buf),
	m_datalen(0)
		{
		if (!append) m_pBuffer->clear();
		}
*/


/*
** Memory output stream constructor with supplied buffer.
**
** The buffer size is set to zero ready for writing if append
** is false.
*/
SWMemoryOutputStream::SWMemoryOutputStream(SWBuffer &buf, sw_size_t offset) :
	m_pBuffer(&buf),
	m_datalen(offset)
		{
		}


/**
*** Destructor
**/
SWMemoryOutputStream::~SWMemoryOutputStream()
		{
		}





sw_status_t
SWMemoryOutputStream::rewindStream()
		{
		m_datalen = 0;
		return SW_STATUS_SUCCESS;
		}


sw_status_t
SWMemoryOutputStream::writeData(const void *pData, sw_size_t len)
		{
		sw_byte_t	*p, *q;

		/*
		p = (sw_byte_t *)(m_pBuffer->data());
		printf("\nBEFORE: p=%lx, datalen=%d\n", p, m_datalen);
		for (sw_size_t i=0;i<m_datalen;i++)
			{
			printf("%02X ", p[i]);
			}
		*/

		p = (sw_byte_t *)(m_pBuffer->data(m_datalen + len));

		/*
		printf("\nRESIZE: p=%lx, datalen=%d\n", p, m_datalen);
		for (sw_size_t i=0;i<m_datalen;i++)
			{
			printf("%02X ", p[i]);
			}
		printf("\n");
		*/

		p += m_datalen;
		q = (sw_byte_t *)pData;
		
		/*
		printf("write %ld bytes to %lx\n", len, p);
		for (sw_size_t i=0;i<len;i++)
			{
			printf("%02X ", q[i]);
			}
		printf("\n");
		*/

		memcpy(p, pData, len);
		m_datalen += len;
		m_pBuffer->size(m_datalen);
		
		/*
		p = (sw_byte_t *)(m_pBuffer->data());
		printf("\nAFTER: p=%lx, datalen=%d\n", p, m_datalen);
		for (sw_size_t i=0;i<m_datalen;i++)
			{
			printf("%02X ", p[i]);
			}
		printf("\n");
		*/

		return SW_STATUS_SUCCESS;
		}




