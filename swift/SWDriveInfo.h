/**
*** @file		SWDriveInfo.h
*** @brief		A class to hold the information for a drive entry
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWDriveInfo class.
**/

/*********************************************************************************
Copyright (c) 2006-2017 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWDriveInfo_h__
#define __SWDriveInfo_h__

#include <swift/swift.h>

#include <swift/SWObject.h>
#include <swift/SWString.h>
#include <swift/SWTime.h>
#include <swift/SWFileAttributes.h>
#include <swift/SWFileRange.h>





#if defined(WIN32) || defined(_WIN32)

#else // if defined(WIN32) || defined(_WIN32)

// Unix Code
#include <dirent.h>

#define INVALID_FILE_ATTRIBUTES				0xffffffff

#endif // if defined(WIN32) || defined(_WIN32)



/**
*** @brief A class holding information for a drive.
***
*** A SWDriveInfo object represents the information about a drive
*** that can be gathered from the operating system. A drive could be
*** a hard drive, USB drive, optical drive or some other device.
***
*** Windows and Unix handle drives very differently so this class
*** attempts to represent the information in a platform independent way, yet without
*** losing information for each platform.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWDriveInfo : public SWObject
		{
friend class SWFolder;

public:
		/// flags used for type value
		enum TypeFlags
			{
			TypeUnknown				= 0x00000000,	///< An unknown drive type
			TypeOptical				= 0x00000001,
			TypeFlashDrive			= 0x00000002,
			TypeHardDrive			= 0x00000004,
			TypeRamDisk				= 0x00000008,
			TypeNetwork				= 0x00000010,
			TypeRemovable			= 0x00000020,
			TypeFixed				= 0x00000040,

			// Bus types (bits 24-31)
			TypeBusMask				= 0xff000000,
			TypeScsi				= 0x01000000,
			TypeAtapi				= 0x02000000,
			TypeAta					= 0x03000000,
			Type1394				= 0x04000000,
			TypeSsa					= 0x05000000,
			TypeFibre				= 0x06000000,
			TypeUsb					= 0x07000000,
			TypeRAID				= 0x08000000,
			TypeiScsi				= 0x09000000,
			TypeSas					= 0x0a000000,
			TypeSata				= 0x0b000000,
			TypeSd					= 0x0c000000,
			TypeMmc					= 0x0d000000,
			TypeVirtual				= 0x0e000000,
			TypeFileBackedVirtual	= 0x0f000000,
			TypeSpaces				= 0x10000000,
			TypeNvme				= 0x11000000,
			};

		enum FileSystemFlags
			{
			FileSystemCaseSensitiveSearch			= 0x00000001,
			FileSystemCasePreservedNames			= 0x00000002, 
			FileSystemUnicodeOnDisk					= 0x00000004,
			FileSystemPersistenACLS					= 0x00000008,
			FileSystemSupportsFileCompression		= 0x00000010,
			FileSystemSupportsQuotas				= 0x00000020,
			FileSystemSupportsSparseFiles			= 0x00000040,
			FileSystemSupportsReparsePoints			= 0x00000080,
			FileSystemSupportsRemoteStorage			= 0x00000100,
			FileSystemIsCompressed					= 0x00008000,
			FileSystemSupportsObjectIDs				= 0x00010000,
			FileSystemSupportsEncryption			= 0x00020000,
			FileSystemSupportsNamedStreams			= 0x00040000,
			FileSystemIsReadOnly					= 0x00080000,
			FileSystemIsSequentialWriteOnce			= 0x00100000,
			FileSystemSupportsTransactions			= 0x00200000,
			FileSystemSupportsHardLinks				= 0x00400000,
			FileSystemSupportsExtendedAttributes	= 0x00800000,
			FileSystemSupportsOpenByFileID			= 0x01000000,
			FileSystemSupportsUsnJournal			= 0x02000000,
			FileSystemSupportsIntegrityStreams		= 0x04000000
			};

		enum VolumeFlags
			{
			VolumeReadable		= 0x01,
			VolumeWriteable		= 0x02,
			VolumeEraseable		= 0x04,
			VolumeRemoveable	= 0x08,
			VolumeHotplugable	= 0x10,
			VolumeMounted		= 0x80
			};

public:
		/// Default constructor
		SWDriveInfo();

		/// Copy constructor
		SWDriveInfo(const SWDriveInfo &v);

		/// Assignment operator
		const SWDriveInfo &	operator=(const SWDriveInfo &v);

		/// Comparison operator (SWDriveInfo)
		bool operator==(const SWDriveInfo &v)					{ return (m_devicePath == v.m_devicePath); }

		/// Comparison operator (SWDriveInfo)
		bool operator<(const SWDriveInfo &v)					{ return (m_devicePath < v.m_devicePath); }

		/// Comparison operator (SWString) - compares equality with device name
		bool operator==(const SWString &s)						{ return (m_devicePath == s); }


		/// Get the drive type flags
		sw_uint32_t			type() const						{ return m_typeFlags; }

		/// Set the drive type flags
		void				type(sw_uint32_t v)					{ m_typeFlags = v; }

		/// Get the drive type as a string
		SWString			typeAsString() const;

		/// Get the drive filesystem flags
		sw_uint32_t			fileSystemFlags() const				{ return m_fileSystemFlags; }

		/// Set the drive filesystem flags
		void				fileSystemFlags(sw_uint32_t v)		{ m_fileSystemFlags = v; }

		/// Get the device name
		const SWString &	fileSystemName() const				{ return m_fileSystemName; }

		/// Set the device name
		void				fileSystemName(const SWString &v)	{ m_fileSystemName = v; }

		/// Get the device name
		const SWString &	devicePath() const					{ return m_devicePath; }

		/// Set the device name
		void				devicePath(const SWString &v)		{ m_devicePath = v; }

		/// Get the device name
		const SWString &	deviceSerialNo() const				{ return m_deviceSerialNo; }

		/// Set the device name
		void				deviceSerialNo(const SWString &v)	{ m_deviceSerialNo = v; }

		/// Get the volume name
		const SWString &	volumeName() const					{ return m_volumeName; }

		/// Set the volume name
		void				volumeName(const SWString &v)		{ m_volumeName = v; }

		/// Get the drive filesystem flags
		sw_uint32_t			volumeFlags() const					{ return m_volumeFlags; }

		/// Set the drive filesystem flags
		void				volumeFlags(sw_uint32_t v)			{ m_volumeFlags = v; }

		/// Get the drive type as a string
		SWString			volumeFlagsAsString() const;

		/// Get the drive mount point
		int					getMountPointCount() const			{ return (int)m_mountPointList.size(); }

		/// Get the drive mount point
		SWString			getMountPoint(int idx) const		{ return m_mountPointList.get(idx); }

		/// Set the drive mount point
		void				addMountPoint(const SWString &v)	{ m_mountPointList.add(v); }

		/// Test to see if the drive is removeable
		bool				isRemovable() const					{ return (((m_typeFlags & TypeRemovable) != 0) || ((m_volumeFlags & (VolumeRemoveable|VolumeHotplugable)) != 0)); }

		/// Test to see if the drive is optical
		bool				isOptical() const					{ return ((m_typeFlags & TypeOptical) != 0); }

public: // SWObject overrides

		/// Clear the info
		virtual void		clear();

public: // Static methods
		static void			getDriveList(SWArray<SWDriveInfo> &drives);

protected:
		sw_uint32_t		m_typeFlags;		///< The drive type
		sw_uint32_t		m_fileSystemFlags;	///< The file system flags
		SWString		m_fileSystemName;	///< The file system name
		SWString		m_devicePath;		///< The device path or name
		SWString		m_deviceSerialNo;	///< The device serial no
		SWString		m_volumeName;		///< The volume name or label
		sw_uint32_t		m_volumeFlags;		///< The volume flags
		SWStringArray	m_mountPointList;	///< The mount point or drive name list
		};




#endif
