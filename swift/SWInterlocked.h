/**
*** @file		SWInterlocked.h
*** @brief		A class to implement an object interlock
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWInterlocked class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWInterlocked_h__
#define __SWInterlocked_h__

#include <swift/swift.h>
#include <swift/SWGuard.h>
#include <swift/SWLockableObject.h>



// Forward declarations

/**
*** @brief	An abstract class to interlock a value
***
*** This class provides an object interlock which protects the value contained
*** in the derived class. The class does not provide value storage directly
*** so developers should use the SWInterlocked_T template class.
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Safe</td>
*** </tr>
*** </table>
***
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWInterlocked : public SWLockableObject
		{
public:
		/// Default constructor (specifiying number of bits and optional fixed size)
		SWInterlocked();

		/// Destructor
		virtual ~SWInterlocked();

		/**
		*** Lock the object (if synchronised).
		***
		*** If the object is synchronised (i.e. has an associated ObjectMutex) the lock
		*** method of the mutex is called.
		***
		*** If the object is not synchronised then this method does nothing.
		**/
		virtual sw_status_t		lock(sw_uint64_t=SW_TIMEOUT_INFINITE) const;

		/**
		*** Lock the object for reading (if synchronised).
		***
		*** If the object is synchronised (i.e. has an associated ObjectMutex) the lock
		*** method of the mutex is called.
		***
		*** If the object is not synchronised then this method does nothing.
		**/
		virtual sw_status_t		lockForReading(sw_uint64_t=SW_TIMEOUT_INFINITE) const;

		/**
		*** Lock the object for reading (if synchronised).
		***
		*** If the object is synchronised (i.e. has an associated ObjectMutex) the lock
		*** method of the mutex is called.
		***
		*** If the object is not synchronised then this method does nothing.
		**/
		virtual sw_status_t		lockForWriting(sw_uint64_t=SW_TIMEOUT_INFINITE) const;

		/**
		*** Unlock the object (if synchronised).
		***
		*** If the object is synchronised (i.e. has an associated ObjectMutex) the unlock
		*** method of the mutex is called.
		***
		*** If the object is not synchronised then this method does nothing.
		**/
		virtual sw_status_t		unlock() const;

private:
		static SWMutex	*__mutex;		///< The mutex used for interlocking
		static int		__refcount;		///< The numebr of references to the mutex
		};






#endif
