/****************************************************************************
**	SWService.cpp	A class for testing and controlling services (primarily Win32)
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


// Needed for precompiled header support
#include "stdafx.h"

#include <swift/SWServiceApp.h>
#include <swift/sw_file.h>
#include <swift/SWStringArray.h>
#include <swift/SWProcess.h>
#include <xplatform/sw_clock.h>


// The one and only service
static SWServiceApp	*pService=NULL;

#if defined(SW_PLATFORM_WINDOWS)
void WINAPI
SWServiceAppCtrlHandler(DWORD opcode)
		{
		if (pService != NULL)
			{
			pService->control(opcode);
			}
		}


void WINAPI
SWServiceAppStart(DWORD argc, LPTSTR *argv)
		{
		if (pService != NULL)
			{
			pService->start(argc, argv);
			}
		}
#endif // defined(SW_PLATFORM_WINDOWS)



SWServiceApp::SWServiceApp(const SWString &name, const SWString &title) :
	m_name(name),
	m_title(title),
#if defined(SW_PLATFORM_WINDOWS)
	m_statusHandle(0),
	m_hManager(0),
#endif // defined(SW_PLATFORM_WINDOWS)
	m_run(false),
	m_pause(false),
	m_serviceflag(true),
	m_pLog(NULL)
		{
		synchronize();

#if defined(SW_PLATFORM_WINDOWS)
		m_hManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
		memset(&m_status, 0, sizeof(m_status));
#endif // defined(SW_PLATFORM_WINDOWS)
		}


SWServiceApp::~SWServiceApp()
		{
#if defined(SW_PLATFORM_WINDOWS)
		if (m_hManager != NULL)
			{
			CloseServiceHandle(m_hManager);
			}
#endif // defined(SW_PLATFORM_WINDOWS)
		}


#if defined(SW_PLATFORM_WINDOWS)
void
SWServiceApp::control(DWORD opcode) 
		{ 
		if (m_pLog != NULL) m_pLog->debug("SWServiceApp::control - Handling control code %d\n", opcode);

		switch (opcode) 
			{ 
			case SERVICE_CONTROL_PAUSE: 
				if (m_pLog != NULL) m_pLog->debug("SWServiceApp::control - SERVICE_CONTROL_PAUSE\n");
				if (suspend() == SW_STATUS_SUCCESS)
					{
					// Do whatever it takes to pause here. 
					m_status.dwCurrentState = SERVICE_PAUSED; 
					}
				break; 

			case SERVICE_CONTROL_CONTINUE: 
				if (m_pLog != NULL) m_pLog->debug("SWServiceApp::control - SERVICE_CONTROL_CONTINUE\n");
				if (resume() == SW_STATUS_SUCCESS)
					{
					// Do whatever it takes to continue here. 
					m_status.dwCurrentState = SERVICE_RUNNING;
					}
				break; 

			case SERVICE_CONTROL_STOP: 
				if (m_pLog != NULL) m_pLog->debug("SWServiceApp::control - SERVICE_CONTROL_STOP\n");
				if (stop(true) == SW_STATUS_SUCCESS)
					{
					// Do whatever it takes to stop here. 
					m_status.dwWin32ExitCode = 0; 
					m_status.dwCurrentState  = SERVICE_STOPPED; 
					m_status.dwCheckPoint    = 0; 
					m_status.dwWaitHint      = 0; 
					}
				break;
			} 

		if (m_pLog != NULL) m_pLog->debug("SWServiceApp::control - setting status %d\n", m_status.dwCurrentState);

		// Send current status. 
		SetServiceStatus(m_statusHandle, &m_status);
		}


void
SWServiceApp::start(DWORD /*argc*/, LPTSTR * /*argv*/)
		{
		if (m_pLog != NULL) m_pLog->debug("SWServiceApp::start - starting\n");

		DWORD status; 
		//DWORD specificError; 

		m_status.dwServiceType				= SERVICE_WIN32; 
		m_status.dwCurrentState				= SERVICE_START_PENDING; 
		m_status.dwControlsAccepted			= SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_PAUSE_CONTINUE; 
		m_status.dwWin32ExitCode			= 0; 
		m_status.dwServiceSpecificExitCode	= 0; 
		m_status.dwCheckPoint				= 0; 
		m_status.dwWaitHint					= 0; 

		if (m_pLog != NULL) m_pLog->debug("SWServiceApp::start - registering service control handler\n");
		m_statusHandle = RegisterServiceCtrlHandler(m_name, SWServiceAppCtrlHandler); 
		// sw_registry_setInteger(SW_REGISTRY_LOCAL_MACHINE, key, "state", 1);

		if (m_statusHandle == (SERVICE_STATUS_HANDLE)0) 
			{ 
			if (m_pLog != NULL) m_pLog->error("SWServiceApp::start - RegisterServiceCtrlHandler failed %d\n", GetLastError()); 
			return; 
			} 
		// sw_registry_setInteger(SW_REGISTRY_LOCAL_MACHINE, key, "state", 2);

		if (m_pLog != NULL) m_pLog->debug("SWServiceApp::start - calling init\n");

		// Initialization code goes here. 
		status = init(); // MyServiceInitialization(argc,argv, &specificError); 

		if (m_pLog != NULL) m_pLog->debug("SWServiceApp::start - status = %d\n", status);
		// sw_registry_setInteger(SW_REGISTRY_LOCAL_MACHINE, key, "state", 3);

		// Handle error condition 
		if (status != NO_ERROR) 
			{ 
			m_status.dwCurrentState				= SERVICE_STOPPED; 
			m_status.dwCheckPoint				= 0; 
			m_status.dwWaitHint					= 0; 
			m_status.dwWin32ExitCode			= status; 
			m_status.dwServiceSpecificExitCode	= 0; //specificError; 

			SetServiceStatus(m_statusHandle, &m_status); 
			return; 
			} 

		// sw_registry_setInteger(SW_REGISTRY_LOCAL_MACHINE, key, "state", 4);
		
		// Initialization complete - report running status. 
		m_status.dwCurrentState       = SERVICE_RUNNING; 
		m_status.dwCheckPoint         = 0; 
		m_status.dwWaitHint           = 0; 
		SetServiceStatus(m_statusHandle, &m_status); 

		// sw_registry_setInteger(SW_REGISTRY_LOCAL_MACHINE, key, "state", 5);
		
		m_running = true;
		m_paused = false;
		m_pause = false;
		m_run = true;
		
		if (m_pLog != NULL) m_pLog->debug("SWServiceApp::start - calling run\n", status);
		run();
		if (m_pLog != NULL) m_pLog->debug("SWServiceApp::start - after run\n", status);
		
		m_running = false;

		// sw_registry_setInteger(SW_REGISTRY_LOCAL_MACHINE, key, "state", 6);
		
		return; 
		} 
#endif // defined(SW_PLATFORM_WINDOWS)
 
void
SWServiceApp::execute()
		{
		m_serviceflag = false;
		m_running = true;
		m_paused = false;
		m_pause = false;
		m_run = true;
		
		if (init() == SW_STATUS_SUCCESS)
			{
			run();
			}

		m_running = false;
		}


sw_status_t
SWServiceApp::install(const SWString &name, const SWString &title, int type)
		{
		SWString	path;
#if defined(SW_PLATFORM_WINDOWS)
		TCHAR		exe[1024];

		::GetModuleFileName(NULL, exe, 1024);
		path = exe;
#else
		sw_pid_t	pid=SWProcess::self();
		SWFileInfo	fi;

		if (fi.getInfoForFile("/proc/" + SWString::valueOf(pid) + "/exe") == SW_STATUS_SUCCESS)
			{
			path = fi.getLinkValue();
			}
#endif
		
		
		return install(name, title, path, type);
		}


sw_status_t
SWServiceApp::uninstall()
		{
		return uninstall(m_name);
		}


sw_status_t
SWServiceApp::install(const SWString &name, const SWString &title, const SWString &path, int starttype)
		{
		sw_status_t	res=SW_STATUS_SYSTEM_ERROR;

#if defined(SW_PLATFORM_WINDOWS)
		SC_HANDLE	hService, hManager;
		
		hManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
		if (hManager != NULL)
			{
			DWORD	type=SERVICE_DEMAND_START;

			switch (starttype)
				{
				case StartDisabled:	type = SERVICE_DISABLED;		break;
				case StartAuto:		type = SERVICE_AUTO_START;		break;
				case StartOnDemand:	type = SERVICE_DEMAND_START;	break;
				}

			/*
			#define SERVICE_BOOT_START             0x00000000
			#define SERVICE_SYSTEM_START           0x00000001
			#define SERVICE_AUTO_START             0x00000002
			#define SERVICE_DEMAND_START           0x00000003
			#define SERVICE_DISABLED               0x00000004
			*/

			hService = CreateService(hManager, name, title,
				SERVICE_ALL_ACCESS, // desired access 
				SERVICE_WIN32_OWN_PROCESS, // service type 
				type, // start type 
				SERVICE_ERROR_NORMAL, // error control type 
				path, // service's binary 
				NULL, // no load ordering group 
				NULL, // no tag identifier 
				NULL, // no dependencies
				NULL, // LocalSystem account
				NULL); // no password

			if (hService != NULL)
				{
				res = SW_STATUS_SUCCESS;
				CloseServiceHandle(hService);
				}

			CloseServiceHandle(hManager);
			}
#else // defined(SW_PLATFORM_WINDOWS)
#endif // defined(SW_PLATFORM_WINDOWS)

		return res;
		}



sw_status_t
SWServiceApp::uninstall(const SWString &name)
		{
		sw_status_t	res=SW_STATUS_SYSTEM_ERROR;

#if defined(SW_PLATFORM_WINDOWS)
		SC_HANDLE	hService, hManager;
		
		hManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
		if (hManager != NULL)
			{
			hService = OpenService(hManager, name, SERVICE_ALL_ACCESS);
		
			if (hService != NULL)
				{
				if (DeleteService(hService) != 0) res = SW_STATUS_SUCCESS;
				CloseServiceHandle(hService);
				}
			}
#else // defined(SW_PLATFORM_WINDOWS)
#endif // defined(SW_PLATFORM_WINDOWS)

		return res;
		}


#if defined(SW_PLATFORM_WINDOWS)
bool
SWServiceApp::dispatch()
		{
		SWString	key;
		
		key = "Software/SwiftTec/";
		key += m_name;
		
		// sw_registry_setInteger(SW_REGISTRY_LOCAL_MACHINE, key, "dispatch", 0);

		pService = this;

		SERVICE_TABLE_ENTRY   dt[2];
		
		dt[0].lpServiceName = (sw_tchar_t *)m_name.t_str();
		dt[0].lpServiceProc = SWServiceAppStart;

		dt[1].lpServiceName = NULL;
		dt[1].lpServiceProc = NULL;

		bool res = (StartServiceCtrlDispatcher(dt) != FALSE);

		// sw_registry_setInteger(SW_REGISTRY_LOCAL_MACHINE, key, "dispatch", 1);

		return res;
		}
#endif // defined(SW_PLATFORM_WINDOWS)


sw_status_t
SWServiceApp::init()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

#if !defined(SW_PLATFORM_WINDOWS)
		// Call setsid and setpgid to detach this service from it's parent
		setsid();
		setpgid(0, 0);
#endif
		
		return res;
		}


sw_status_t
SWServiceApp::suspend()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		m_pause = true;

		SWTime		t1, t2;

		t1.update();

		while (!paused())
			{
			t2.update();
			if ((t2.toDouble() - t1.toDouble()) > 30.0)
				{
				res = SW_STATUS_TIMEOUT;
				break;
				}

			SWThread::sleep(100);
			}

		return res;
		}


sw_status_t
SWServiceApp::resume()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		
		m_pause = false;

		SWTime		t1, t2;

		t1.update();

		while (paused())
			{
			t2.update();
			if ((t2.toDouble() - t1.toDouble()) > 30.0)
				{
				res = SW_STATUS_TIMEOUT;
				break;
				}

			SWThread::sleep(100);
			}

		return res;
		}


sw_status_t
SWServiceApp::stop(bool waitfor)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		
		m_run = false;
		
		if (waitfor)
			{
			SWTime		t1, t2;

			t1.update();

			while (running())
				{
				t2.update();
				if ((t2.toDouble() - t1.toDouble()) > 30.0)
					{
					res = SW_STATUS_TIMEOUT;
					break;
					}

				SWThread::sleep(100);
				}
			}

		return res;
		}


sw_status_t
SWServiceApp::start()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		
		return res;
		}



bool
SWServiceApp::waitFor(int ms, int maxsleepms)
		{
		sw_clockms_t	now;

		if (maxsleepms < 1) maxsleepms = 1;

		now = sw_clock_getCurrentTimeMS();

		while (runnable() && ms > 0)
			{
			if (ms > maxsleepms)
				{
				SWThread::sleep(maxsleepms);
				ms -= maxsleepms;
				}
			else
				{
				SWThread::sleep(ms);
				ms = 0;
				}
			}

		return runnable();
		}

