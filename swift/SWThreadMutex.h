/**
*** @file		SWThreadMutex.h
*** @brief		A thread mutex.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWThreadMutex class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWThreadMutex_h__
#define __SWThreadMutex_h__

#include <swift/SWMutex.h>
#include <swift/SWThread.h>
#include <swift/SWThreadIdCount.h>


/**
*** @brief	Inter-thread (within a single process) non-recusrsive mutex.
***
*** Thread-specific version of a Mutex. This is a non-recursive mutex implementation
*** which means that any attempt for a mutex to be re-acquired by the same thread would
*** block indefinitely. To prevent this situation the Thread class will throw a 
*** MutexAlreadyHeldException if this is detected.
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Safe</td>
*** </tr>
*** </table>
***
***
*** @see		Mutex
*** @author		Simon Sparkes
*/
class SWIFT_DLL_EXPORT SWThreadMutex : public SWMutex
		{
friend class SWThreadCondVar;

public:
		/// Constructor
		SWThreadMutex(bool recursive=false);

		/// Virtual destructor
		virtual ~SWThreadMutex();

		/// Lock/Acquire this mutex. The same thread can acquire the mutex more than once.
		virtual sw_status_t	acquire(sw_uint64_t waitms=SW_TIMEOUT_INFINITE);

		/// Unlock/Release this mutex. If the mutex has been recursively acquired the mutex
		/// will only be unlocked when the recursive lock count reaches zero.
		virtual sw_status_t	release();

		/**
		*** @brief	Check to see if the current thread/process has locked the mutex.
		***
		*** This method returns true if the current thread/process owns the mutex.
		*** (A thread/process "owns" a mutex when it has performed a successful
		*** acquire operation.
		**/
		virtual bool		hasLock();

		/**
		*** @brief	Check to see if this mutex is locked by someone.
		***
		*** This method checks to see if the mutex is currently in the locked
		*** (acquired) state by someone. If the mutex is currently locked then this
		*** method returns true. If no-one currently owns the mutex then false is
		*** returned.
		***
		*** @note
		*** This method will return true if any thread/process currently owns
		*** the mutex. If a caller wishes to determine if the current thread/process
		*** owns the mutex they should call the hasLock() method.
		**/
		virtual bool		isLocked();

protected:
		sw_mutex_t		m_hMutex;		///< The internal mutex handle
		};




#endif
