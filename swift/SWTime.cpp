/**
*** @file		SWTime.cpp
*** @brief		A class to represent a time in seconds and microseconds  since 1st Jan 1900
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWTime class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <math.h>

#if !defined(WIN32) && !defined(_WIN32)
	#include <sys/time.h>
#endif

#include <swift/SWTime.h>
#include <swift/SWDate.h>
#include <swift/SWLocale.h>
#include <swift/sw_printf.h>
#include <swift/SWInputStream.h>
#include <swift/SWOutputStream.h>


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




// The number of seconds between 1/1/1900 and 1/1/1970
// Our time and Unix time


// Construct with today's date
SWTime::SWTime(bool setToCurrentTime) :
    m_seconds(0),
    m_nanoseconds(0)
		{
		if (setToCurrentTime) update();
		}


sw_status_t
SWTime::gettimeofday(sw_timeval_t &tv)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

#if defined(SW_PLATFORM_WINDOWS)
		FILETIME	ft;

		/*
		SYSTEMTIME	st;

		GetSystemTime(&st);
		if (st.wHour < 0) st.wHour = 0;
		if (st.wHour > 23) st.wHour = 23;
		if (st.wMinute < 0) st.wMinute = 0;
		if (st.wMinute > 59) st.wMinute = 59;
		if (st.wSecond < 0) st.wMilliseconds = 0;
		if (st.wSecond > 59) st.wSecond = 59;
		if (st.wMilliseconds < 0) st.wMilliseconds = 0;
		if (st.wMilliseconds > 999) st.wMilliseconds = 999;

		SystemTimeToFileTime(&st, &ft);
		*/
		GetSystemTimeAsFileTime(&ft);

		sw_int64_t	t;

		t = (((sw_int64_t)ft.dwHighDateTime << 32) | (sw_int64_t)ft.dwLowDateTime) - 116444736000000000;
		tv.tv_sec = (int)(t / 10000000);
		tv.tv_usec = (int)((t % 10000000) / 10);
#else
		struct timeval	tmp;
		int				r;

		r = ::gettimeofday(&tmp, NULL);
		if (r == 0)
			{
			tv.tv_sec = tmp.tv_sec;
			tv.tv_usec = tmp.tv_usec;
			}
		else
			{
			res = errno;
			}
#endif

		return res;
		}


// Get the time now
void
SWTime::update()
		{
		sw_timeval_t	catv;

		gettimeofday(catv);

		*this = catv;
		}


// Construct the time from a Unix clock
SWTime::SWTime(time_t clock) :
    m_seconds(0),
    m_nanoseconds(0)
		{
		*this = clock;
		}


SWTime::SWTime(const FILETIME &ft) :
    m_seconds(0),
    m_nanoseconds(0)
		{
		*this = ft;
		}


SWTime::SWTime(const SYSTEMTIME &ft) :
    m_seconds(0),
    m_nanoseconds(0)
		{
		*this = ft;
		}


// Construct the time from a timeval
SWTime::SWTime(const sw_internal_time_t &tv) :
    m_seconds(0),
    m_nanoseconds(0)
		{
		*this = tv;
		}



// Construct the time from a timeval
SWTime::SWTime(const sw_timespec_t &tv) :
    m_seconds(0),
    m_nanoseconds(0)
		{
		*this = tv;
		}



// Construct the time from a timeval
SWTime::SWTime(const sw_timeval_t &tv) :
    m_seconds(0),
    m_nanoseconds(0)
		{
		*this = tv;
		}


// Construct from a double
SWTime::SWTime(double v, bool isVariantTime)
		{
		set(v, isVariantTime);
		}


void
SWTime::set(double v, bool isVariantTime)
		{
		if (isVariantTime)
			{
			m_seconds = (sw_int64_t)((v - 1.0) * SECONDS_IN_DAY);
			m_nanoseconds = 0;
			}
		else
			{
			double	w, f;

			// Split v into whole(w) and fractional(f) parts
			f = modf(v, &w);

			m_seconds = (sw_int64_t)w;
			m_nanoseconds = (sw_uint32_t)(f * 1000000000.0);
			}

		calculateDST();
		}


SWTime::SWTime(const SWDate &dt)
		{
		*this = dt;
		}


SWTime::SWTime(const sw_tm_t &tm)
		{
		*this = tm;
		}


SWTime::SWTime(sw_int64_t secs, sw_uint32_t nsecs, TimeType tt)
		{
		set(secs, nsecs, tt);
		}


SWTime::SWTime(const SWString &value, const SWString &format) :
    m_seconds(0),
    m_nanoseconds(0)
		{
		set(value, format);
		}

static const char *
getnum(const char *sp, int ndigits, int &value)
		{
		int	v = 0;

		while (ndigits > 0 && *sp)
			{
			if (*sp >= '0' && *sp <= '9')
				{
				v = v * 10 + (*sp - '0');
				sp++;
				--ndigits;
				}
			else break;
			}

		if (ndigits != 0)
			{
			value = 0;
			sp = NULL;
			}
		else
			{
			value = v;
			}

		return sp;
		}


bool
SWTime::adjust(double v)
		{
		double	w, f;

		// Split v into whole(w) and fractional(f) parts
		f = modf(v, &w);

		sw_int64_t adjsecs=(sw_int64_t)w;
		sw_int32_t adjnsecs=(sw_int32_t)(f * 1000000000.0);

		return adjust(adjsecs, adjnsecs);
		}


bool
SWTime::adjust(sw_int64_t adjsecs, sw_int32_t adjnsecs)
		{
		m_seconds += adjsecs;

		sw_int64_t	nsecs=(sw_int64_t)m_nanoseconds + (sw_int64_t)adjnsecs;

		while (nsecs < 0)
			{
			m_seconds--;
			nsecs += 1000000000;
			}

		while (nsecs > 1000000000)
			{
			m_seconds++;
			nsecs -= 1000000000;
			}

		m_nanoseconds = (sw_uint32_t)nsecs;

		calculateDST();

		return true;
		}


bool
SWTime::set(sw_int64_t secs, sw_uint32_t nsecs, TimeType tt)
		{
		m_nanoseconds = nsecs;
		switch (tt)
			{
			case UnixTime:
				m_seconds = secs + SECONDS_SWTIME_UNTIL_UNIXTIME;
				break;

			case WindowsFileTime:
				{
				sw_int64_t	t;

				t = secs - (SECONDS_FILETIME_UNTIL_SWTIME * 10000000UL);
				m_seconds = t / 10000000UL;
				m_nanoseconds += (int)(t % 10000000UL) * 100;
				}
				break;

			default:
				m_seconds = secs;
				break;
			}

		while (m_nanoseconds > 1000000000)
			{
			m_seconds++;
			m_nanoseconds -= 1000000000;
			}

		calculateDST();

		return true;
		}

bool
SWTime::set(int nYear, int nMonth, int nDay, int nHour, int nMinute, int nSeconds, int nNanoseconds)
		{
		sw_tm_t	tm;

		memset(&tm, 0, sizeof(tm));
		tm.tm_sec = nSeconds;     /* seconds after the minute [0, 61]  */
		tm.tm_min = nMinute;     /* minutes after the hour [0, 59] */
		tm.tm_hour = nHour;    /* hour since midnight [0, 23] */
		tm.tm_mday = nDay;    /* day of the month [1, 31] */
		tm.tm_mon = nMonth-1;     /* months since January [0, 11] */
		tm.tm_year = nYear-1900;    /* years since 1900 */

		*this = tm;

		// tm will leave the ns portion of time at 0 so we post-adjust it
		m_nanoseconds = nNanoseconds;

		return true;
		}


bool
SWTime::set(const SWString &value, const SWString &format)
		{
		SWString	tmpf(format);
		SWString	tmpv(value);
		bool		res=false;

		if (!tmpf.isEmpty())
			{
			// Supported formats
			// YYYY 4-digit year
			// YY 2-digit year (00-99)
			// MM 2-digit month (01-12)
			// DD 2-digit day (01-31)
			// hh 2-digit hour (00-23)
			// mm 2-digit minute (00-59)
			// ss 2-digit second (00-61)
			// ms milliseconds (000-999)
			// us milliseconds (000000-999999)
			// ns milliseconds (000000000-999999999)

			const char	*fp=tmpf;
			const char	*vp=tmpv;
			int			year=0, month=0, day=0, hour=0, minute=0, second=0, nanosecond=0;

			vp = tmpv;
			while (vp != NULL && *fp && *vp)
				{
				if (strncmp(fp, "YYYY", 4) == 0)
					{
					vp = getnum(vp, 4, year);
					fp += 4;
					}
				else if (strncmp(fp, "YY", 2) == 0)
					{
					vp = getnum(vp, 2, year);
					if (vp != NULL)
						{
						SWDate	today;
						int		y;

						today.update();
						y = today.year() % 100;
						year += (today.year() - y) + (y >= 50?100:0) - (year >=50?100:0);
						}
					fp += 2;
					}
				else if (strncmp(fp, "MM", 2) == 0)
					{
					vp = getnum(vp, 2, month);
					fp += 2;
					}
				else if (strncmp(fp, "DD", 2) == 0)
					{
					vp = getnum(vp, 2, day);
					fp += 2;
					}
				else if (strncmp(fp, "hh", 2) == 0)
					{
					vp = getnum(vp, 2, hour);
					fp += 2;
					}
				else if (strncmp(fp, "mm", 2) == 0)
					{
					vp = getnum(vp, 2, minute);
					fp += 2;
					}
				else if (strncmp(fp, "ss", 2) == 0)
					{
					vp = getnum(vp, 2, second);
					fp += 2;
					}
				else if (strncmp(fp, "ms", 2) == 0)
					{
					vp = getnum(vp, 3, nanosecond);
					fp += 2;
					nanosecond *= 1000000;
					}
				else if (strncmp(fp, "us", 2) == 0)
					{
					vp = getnum(vp, 6, nanosecond);
					fp += 2;
					nanosecond *= 1000;
					}
				else if (strncmp(fp, "ns", 2) == 0)
					{
					vp = getnum(vp, 9, nanosecond);
					fp += 2;
					}
				else if (*fp == *vp)
					{
					fp++;
					vp++;
					}
				else
					{
					// Invalid string - does not match pattern
					vp = NULL;
					}
				}

			if (vp != NULL)
				{
				// This is at least a date
				sw_tm_t	tm;

				memset(&tm, 0, sizeof(tm));
				tm.tm_sec = second;     /* seconds after the minute [0, 61]  */
				tm.tm_min = minute;     /* minutes after the hour [0, 59] */
				tm.tm_hour = hour;    /* hour since midnight [0, 23] */
				tm.tm_mday = day;    /* day of the month [1, 31] */
				tm.tm_mon = month-1;     /* months since January [0, 11] */
				tm.tm_year = year-1900;    /* years since 1900 */

				*this = tm;

				// tm will leave the ns portion of time at 0 so we post-adjust it
				m_nanoseconds = nanosecond;
				res = true;
				}
			}

		calculateDST();

		return res;
		}


// set the time from a timeval
SWTime &
SWTime::operator=(const sw_internal_time_t &tv)
		{
		m_seconds = tv.tv_sec;
		m_nanoseconds = tv.tv_nsec;

		calculateDST();

		return *this;
		}


// set the time from a timeval
SWTime &
SWTime::operator=(const sw_timespec_t &tv)
		{
		m_seconds = (sw_int64_t)tv.tv_sec + SECONDS_SWTIME_UNTIL_UNIXTIME;
		m_nanoseconds = tv.tv_nsec;

		calculateDST();

		return *this;
		}


// set the time from a timeval
SWTime &
SWTime::operator=(const sw_timeval_t &tv)
		{
		m_seconds = (sw_int64_t)tv.tv_sec + SECONDS_SWTIME_UNTIL_UNIXTIME;
		m_nanoseconds = tv.tv_usec * 1000;

		calculateDST();

		return *this;
		}


// set the time
SWTime &
SWTime::operator=(time_t clock)
		{
		m_seconds = (sw_int64_t)clock + SECONDS_SWTIME_UNTIL_UNIXTIME;
		m_nanoseconds = 0;

		calculateDST();

		return *this;
		}


// set the time
SWTime &
SWTime::operator=(double v)
		{
		double	w, f;

		// Split v into whole(w) and fractional(f) parts
		f = modf(v, &w);

		m_seconds = (sw_int64_t)w;
		m_nanoseconds = (sw_uint32_t)(f * 1000000000.0);

		calculateDST();

		return *this;
		}


// Set the time from a FILETIME
// Microsoft documentation states that the FILETIME structure
// is a 64-bit value representing the number of 100-nanosecond intervals since January 1, 1601. 
SWTime &
SWTime::operator=(const FILETIME &ft)
		{
		// t is the number of 100-nanosecond intervals since January 1, 1900
		sw_int64_t	 t;

		t = (((sw_int64_t)ft.dwHighDateTime << 32) | (sw_int64_t)ft.dwLowDateTime) - (SECONDS_FILETIME_UNTIL_SWTIME * 10000000UL);
		m_seconds = t / 10000000UL;
		m_nanoseconds = (int)(t % 10000000UL) * 100;

		calculateDST();

		return *this;
		}


FILETIME
SWTime::toFILETIME() const
		{
		FILETIME	ft;

		if (!toFILETIME(ft)) throw SW_EXCEPTION(SWTimeConversionException);

		return ft;
		}


bool
SWTime::toFILETIME(FILETIME &v) const
		{
		bool	res=true;
		sw_int64_t	t;

		t = ((sw_int64_t)m_seconds * 10000000UL) + ((sw_int64_t)m_nanoseconds / 100UL) + (SECONDS_FILETIME_UNTIL_SWTIME * 10000000UL);

		if (t >= 0)
			{
			v.dwHighDateTime = (sw_uint32_t)(t >> 32);
			v.dwLowDateTime = (sw_uint32_t)(t & 0xffffffff);
			}
		else res = false;

		return res;
		}



// Set the time from a SYSTEMTIME
// Microsoft documentation states that the SYSTEMTIME structure
// is a 64-bit value representing the number of 100-nanosecond intervals since January 1, 1601. 
SWTime &
SWTime::operator=(const SYSTEMTIME &ft)
		{
		SWDate	d(ft.wDay, ft.wMonth, ft.wYear);

		if (d.isValid()
			&& ft.wHour < 24
			&& ft.wMinute < 60
			&& ft.wSecond < 62	// Can have leap seconds
			)
			{
			m_seconds = (sw_int64_t)(d.julian() - JULIAN_DAY_31ST_DEC_1899) * SECONDS_IN_DAY + (sw_int64_t)(ft.wHour * 3600) + (sw_int64_t)(ft.wMinute * 60) + (sw_int64_t)ft.wSecond;
			m_nanoseconds = ft.wMilliseconds * 1000000UL;
			}
		else throw SW_EXCEPTION(SWTimeConversionException);

		return *this;
		}


SYSTEMTIME
SWTime::toSYSTEMTIME() const
		{
		SYSTEMTIME	ft;

		if (!toSYSTEMTIME(ft)) throw SW_EXCEPTION(SWTimeConversionException);

		return ft;
		}


/*
System time members as follows:
typedef struct _SYSTEMTIME {  WORD wYear;  WORD wMonth;  WORD wDayOfWeek;  WORD wDay;  WORD wHour;  WORD wMinute;  WORD wSecond;  WORD wMilliseconds;
} SYSTEMTIME, *PSYSTEMTIME;

wYear Current year. The year must be greater than 1601.  Windows Server 2003, Windows XP:  The year cannot be greater than 30827.
wMonth Current month; January = 1, February = 2, and so on. 
wDayOfWeek Current day of the week; Sunday = 0, Monday = 1, and so on. 
wDay Current day of the month. 
wHour Current hour. 
wMinute Current minute. 
wSecond Current second. 
wMilliseconds Current millisecond. 
*/
#if !defined(WIN32) && !defined(_WIN32)
	/// Local typedef of WORDfor non-windows builds.
	typedef sw_uint16_t	WORD;
#endif

bool
SWTime::toSYSTEMTIME(SYSTEMTIME &v) const
		{
		bool	res=true;
		SWDate	d(*this);
		int		y=d.year();

		if (y > 1601 && y < 30828)
			{
			v.wYear = (WORD)d.year();
			v.wMonth = (WORD)d.month();
			v.wDayOfWeek = (WORD)d.dayOfWeek();
			v.wDay = (WORD)d.day();
			v.wHour = (WORD)hour();
			v.wMinute = (WORD)minute();
			v.wSecond = (WORD)second();
			v.wMilliseconds = (WORD)millisecond();
			}
		else res = false;

		return res;
		}



// Set the time from a struct tm
SWTime &
SWTime::operator=(const sw_tm_t &tm)
		{
		SWDate	d(tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900);

		if (d.isValid()
			&& tm.tm_hour >= 0 && tm.tm_hour < 24
			&& tm.tm_min >= 0 && tm.tm_min < 60
			&& tm.tm_sec >= 0 && tm.tm_sec < 62	// Can have leap seconds
			)
			{
			m_seconds = (sw_int64_t)(d.julian() - JULIAN_DAY_31ST_DEC_1899) * SECONDS_IN_DAY + (sw_int64_t)(tm.tm_hour * 3600) + (sw_int64_t)(tm.tm_min * 60) + (sw_int64_t)tm.tm_sec;
			m_nanoseconds = 0;
			}
		else throw SW_EXCEPTION(SWTimeConversionException);

		calculateDST();

		return *this;
		}


sw_tm_t
SWTime::toTM() const
		{
		sw_tm_t	tm;

		if (!toTM(tm)) throw SW_EXCEPTION(SWTimeConversionException);

		return tm;
		}


bool
SWTime::toTM(sw_tm_t &v) const
		{
		bool	res=true;
		SWDate	d(*this);
		int		y=d.year();

		if (y >= 1900)
			{
			v.tm_sec  = second();			// Seconds after minute (0 - 59). 
			v.tm_min = minute();			// Minutes after hour (0 - 59). 
			v.tm_hour = hour();				// Hours since midnight (0 - 23). 
			v.tm_mday = d.day();			// Day of month (1 - 31). 
			v.tm_mon = d.month() - 1;		// Month (0 - 11; January = 0). 
			v.tm_year = d.year() - 1900;	// Year (current year minus 1900). 
			v.tm_wday = d.dayOfWeek();		// Day of week (0 - 6; Sunday = 0). 
			v.tm_yday = d.dayOfYear();		// Day of year (0 - 365; January 1 = 0). 
			v.tm_isdst = isDST();			// Always 0 for gmtime. 
			}
		else res = false;

		return res;
		}




time_t
SWTime::toUnixTime() const
		{
		time_t	t;

		if (!toUnixTime(t)) throw SW_EXCEPTION(SWTimeConversionException);

		return t;
		}


bool
SWTime::toUnixTime(time_t &t) const
		{
		bool	res=true;
		sw_int64_t	actual, maxtime;

		switch (sizeof(time_t))
			{
			case 4:		maxtime = 0x7fffffffUL;			break;
			case 8:		maxtime = 0x7fffffffffffffffUL;	break;

			// Assume 4 bytes for time_t
			default:	maxtime = 0x7fffffffUL;			break;
			}

		actual  = m_seconds - SECONDS_SWTIME_UNTIL_UNIXTIME;
		if (actual < 0 || actual > maxtime)
			{
			// The time_t result is out of range
			res = false;
			}
		else t = (time_t)(m_seconds - SECONDS_SWTIME_UNTIL_UNIXTIME);

		return res;
		}


sw_internal_time_t
SWTime::toInternalTime() const
		{
		sw_internal_time_t	tv;

		if (!toInternalTime(tv)) throw SW_EXCEPTION(SWTimeConversionException);

		return tv;
		}


bool
SWTime::toInternalTime(sw_internal_time_t &tv) const
		{
		bool	res=true;

		tv.tv_sec = m_seconds;
		tv.tv_nsec = m_nanoseconds;

		return res;
		}




sw_timespec_t
SWTime::toTimeSpec() const
		{
		sw_timespec_t	tv;

		if (!toTimeSpec(tv)) throw SW_EXCEPTION(SWTimeConversionException);

		return tv;
		}


bool
SWTime::toTimeSpec(sw_timespec_t &tv) const
		{
		bool	res=true;
		time_t	t;

		if (toUnixTime(t))
			{
			tv.tv_sec = t;
			tv.tv_nsec = nanosecond();
			}
		else res = false;

		return res;
		}




sw_timeval_t
SWTime::toTimeVal() const
		{
		sw_timeval_t	tv;

		if (!toTimeVal(tv)) throw SW_EXCEPTION(SWTimeConversionException);

		return tv;
		}


bool
SWTime::toTimeVal(sw_timeval_t &tv) const
		{
		bool	res=true;
		time_t	t;

		if (toUnixTime(t))
			{
			tv.tv_sec = t;
			tv.tv_usec = microsecond();
			}
		else res = false;

		return res;
		}



// Set the time from a struct tm
SWTime &
SWTime::operator=(const SWDate &d)
		{
		if (d.isValid())
			{
			m_seconds = (sw_int64_t)(d.julian() - JULIAN_DAY_31ST_DEC_1899) * SECONDS_IN_DAY;
			m_nanoseconds = 0;
			}
		else throw SW_EXCEPTION(SWTimeConversionException);

		calculateDST();

		return *this;
		}


SWDate
SWTime::toDate() const
		{
		SWDate	d;

		if (!toDate(d)) throw SW_EXCEPTION(SWTimeConversionException);

		return d;
		}


bool
SWTime::toDate(SWDate &v) const
		{
		v = SWDate(*this);

		return true;
		}



int
SWTime::compareTo(const SWTime &other) const
		{
		int		r=0;

		if (m_seconds == other.m_seconds)
			{
			if (m_nanoseconds < other.m_nanoseconds) r = -1;
			else if (m_nanoseconds > other.m_nanoseconds) r = 1;
			}
		else if (m_seconds < other.m_seconds) r = -1;
		else if (m_seconds > other.m_seconds) r = 1;

		return r;
		}



SWIFT_DLL_EXPORT SWTime
operator+(const SWTime &a, const SWTime &b)
		{
		SWTime	tmp;

		tmp.m_nanoseconds = a.m_nanoseconds + b.m_nanoseconds;
		tmp.m_seconds = a.m_seconds + b.m_seconds;

		while (tmp.m_nanoseconds >= 1000000000)
			{
			tmp.m_seconds++;
			tmp.m_nanoseconds -= 1000000000;
			}

		return tmp;
		}  


SWIFT_DLL_EXPORT SWTime
operator-(const SWTime &a, const SWTime &b)
		{
		SWTime	tmp;

		tmp.m_seconds = a.m_seconds - b.m_seconds;
		if (a.m_nanoseconds >= b.m_nanoseconds)
			{
			tmp.m_nanoseconds = a.m_nanoseconds - b.m_nanoseconds;
			}
		else
			{
			tmp.m_seconds--;
			tmp.m_nanoseconds = a.m_nanoseconds + 1000000000 - b.m_nanoseconds;
			}

		return tmp;
		}  


// Return the hour (0-23)
int
SWTime::hour() const
		{
		return (int)((m_seconds/3600) % 24);
		}

// Return the minute (0-59)
int
SWTime::minute() const
		{
		return (int)((m_seconds/60) % 60);
		}

// Return the second (0-59)
int
SWTime::second() const
		{
		return (int)(m_seconds % 60);
		}

// Return the fractional portion of the time in milliseconds
int
SWTime::millisecond() const
		{
		return m_nanoseconds / 1000000;
		}

// Return the fractional portion of the time in microseconds
int
SWTime::microsecond() const
		{
		return m_nanoseconds / 1000;
		}

// Return the fractional portion of the time in nanoseconds
int
SWTime::nanosecond() const
		{
		return m_nanoseconds;
		}

// Return the day (1-31)
int
SWTime::day() const
		{
		return SWDate(*this).day();
		}

// Return the day (1-12)
int
SWTime::month() const
		{
		return SWDate(*this).month();
		}

// Return the year
int
SWTime::year() const
		{
		return SWDate(*this).year();
		}

// Return the dayOfWeek (0=Sunday, 1=Monday, ....)
int
SWTime::dayOfWeek() const
		{
		return SWDate(*this).dayOfWeek();
		}

// Return the day of year (1-366 if dayOneIsZero is false) or (0-365 if dayOneIsZero is true)
int
SWTime::dayOfYear(bool dayOneIsZero) const
		{
		return SWDate(*this).dayOfYear(dayOneIsZero);
		}

// Return the julian day number
int
SWTime::julian() const
		{
		return (int)(m_seconds / SECONDS_IN_DAY) + JULIAN_DAY_31ST_DEC_1899;
		}



// Return the time as a double where the whole part is the number of seconds
// since midnight 31st Dec 1899 and the fraction part is the fraction of the second
double
SWTime::toDouble() const
		{
		return (double)m_seconds + ((double)m_nanoseconds / 1000000000.0);
		}

// Return the time as a 64-bit integer where the representing the number of seconds
// since midnight 31st Dec 1899.
sw_int64_t
SWTime::toInt64(TimeType tt) const
		{
		sw_int64_t	res=0;

		switch (tt)
			{
			case UnixTime:
				res = m_seconds - SECONDS_SWTIME_UNTIL_UNIXTIME;
				break;

			case WindowsFileTime:
				res = ((sw_int64_t)m_seconds * 10000000UL) + ((sw_int64_t)m_nanoseconds / 100UL) + (SECONDS_FILETIME_UNTIL_SWTIME * 10000000UL);
				break;

			default:
				res = m_seconds;
				break;
			}

		return res;
		}

// Return the Julian Day Number for this time
int
SWTime::toJulian() const
		{
		return julian();
		}



/// Return the name of the timezone
SWString
SWTime::tzName(int dst) const
		{
		SWString	s;

		s = (dst > 0)?"":"GMT";

		return s;
		}



void
SWTime::calculateDST()
		{
		// For a standard SWTime we do nothing
		}


int
SWTime::dst() const
		{
		return 0;
		}



/*
From the cftime man page

     %%      same as %
     %a      locale's abbreviated weekday name
     %A      locale's full weekday name
     %b      locale's abbreviated month name
     %B      locale's full month name
     %c      locale's appropriate date and time representation
  Default
     %C      locale's date and time representation as produced by
             date(1)
 Standard-conforming
     %C      century number (the year divided by  100  and  trun-
             cated  to  an  integer  as a decimal number [1,99]);
             single digits are preceded by 0; see standards(5)
     %d      day of month [1,31]; single digits are preceded by 0
     %D      date as %m/%d/%y
     %e      day of month [1,31]; single digits are preceded by a
             space
     %h      locale's abbreviated month name
     %H      hour (24-hour clock) [0,23]; single digits are  pre-
             ceded by 0
     %I      hour (12-hour clock) [1,12]; single digits are  pre-
             ceded by 0
     %j      day number of year [1,366]; single digits  are  pre-
             ceded by 0
     %k      hour (24-hour clock) [0,23]; single digits are  pre-
             ceded by a blank
     %l      hour (12-hour clock) [1,12]; single digits are  pre-
             ceded by a blank
     %m      month number [1,12]; single digits are preceded by 0
     %M      minute [00,59]; leading zero is  permitted  but  not
             required
     %n      insert a newline
     %p      locale's equivalent of either a.m. or p.m.
     %r      appropriate time  representation  in  12-hour  clock
             format with %p
     %R      time as %H:%M
     %S      seconds [00,61]
     %t      insert a tab
     %T      time as %H:%M:%S
     %u      weekday as a decimal number [1,7], with 1 represent-
             ing Sunday
     %U      week number of year as  a  decimal  number  [00,53],
             with Sunday as the first day of week 1
     %V      week number of the year as a decimal number [01,53],
             with  Monday  as  the first day of the week.  If the
             week containing 1 January has four or more  days  in
             the  new  year, then it is considered week 1; other-
             wise, it is week 53 of the previous  year,  and  the
             next week is week 1.
     %w      weekday as a decimal number [0,6], with 0 represent-
             ing Sunday
     %W      week number of year as  a  decimal  number  [00,53],
             with Monday as the first day of week 1
     %x      locale's appropriate date representation
     %X      locale's appropriate time representation
     %y      year within century [00,99]
     %Y      year, including the century (for example 1993)
     %Z      time zone name or abbreviation, or no  bytes  if  no
             time zone information exists


*/


SWString
SWTime::format(const SWString &fmt) const
		{
		SWString	outstr;
		SWString	formatString(fmt);
		int			c, v, p=0, q;
		SWDate		calendar(*this);
		SWLocale	&loc=SWLocale::getCurrentLocale();
		char		tmp[80];

		while ((q = formatString.indexOf('%', p)) >= 0)
			{
			// If we have any chars copy them into outstr
		  	if (q > p) outstr += formatString.mid(p, q-p);

			// Now do the substitution on the %
			q++;
			if (q >= (int)formatString.length()) break;

			c = formatString[q++];
			switch (c)
				{
				case '%':	// same as %
					outstr += _T("%");
					break;

				case 'a':	// locale's abbreviated weekday name
					outstr += loc.dayName(calendar.dayOfWeek(), true);
					break;

				case 'A':	// locale's full weekday name
					outstr += loc.dayName(calendar.dayOfWeek(), false);
					break;

				case 'b':	// locale's abbreviated month name
					outstr += loc.monthName(calendar.month()-1, true);
					break;

				case 'B':	// locale's full month name
					outstr += loc.monthName(calendar.month()-1, false);
					break;

				case 'c':	// locale's appropriate date and time representation
					outstr += format(_T("%a %b %d %T %Y"));
					break;

				case 'C':	// Default: locale's date and time representation as produced by date(1)
					outstr += format(_T("%a %b %e %T %Z %Y"));
					break;

				/*
				case 'C':	// Standard-conforming: century number (the year divided by  100  and  trun- cated  to  an  integer  as a decimal number [1,99]); single digits are preceded by 0; see standards(5)
					break;
				*/

				case 'D':	// date as %m/%d/%y
					outstr += format(_T("%m/%d/%y"));
					break;

				case 'd':	// day of month [1,31]; single digits are preceded by 0
				case 'e':	// day of month [1,31]; single digits are preceded by a space
					sw_snprintf(tmp, sizeof(tmp), c=='d'?"%02d":"%2d", calendar.dayOfMonth());
					outstr += tmp;
					break;

				case 'f':
				case 'F':	// Time fraction in ms
					sw_snprintf(tmp, sizeof(tmp), c=='f'?"%03d":"%3d", millisecond());
					outstr += tmp;
					break;

				case 'g':	// Week-based year within century [00,99].
					sw_snprintf(tmp, sizeof(tmp), "%02d", calendar.year() % 100);
					outstr += tmp;
					break;

				case 'G':	// Week-based year, including the century [0000,9999].
					sw_snprintf(tmp, sizeof(tmp), "%04d", calendar.year() % 10000);
					outstr += tmp;
					break;

				case 'h':	// locale's abbreviated month name
					outstr += loc.monthName(calendar.month()-1, true);
					break;

				case 'H':	// hour (24-hour clock) [0,23]; single digits are  preceded by 0
				case 'k':	// hour (24-hour clock) [0,23]; single digits are  preceded by a blank
					sw_snprintf(tmp, sizeof(tmp), c=='H'?"%02d":"%2d", hour());
					outstr += tmp;
					break;

				case 'I':	// hour (12-hour clock) [1,12]; single digits are  preceded by 0
				case 'l':	// hour (12-hour clock) [1,12]; single digits are  preceded by a blank
					v = hour() % 12;

					if (v == 0) v = 12;
					sw_snprintf(tmp, sizeof(tmp), c=='I'?"%02d":"%2d", v);
					outstr += tmp;
					break;

				case 'j':	// day number of year [1,366]; single digits  are  preceded by 0
					sw_snprintf(tmp, sizeof(tmp), "%03d", calendar.dayOfYear());
					outstr += tmp;
					break;

				case 'm':	// month number [1,12]; single digits are preceded by 0
					sw_snprintf(tmp, sizeof(tmp), "%02d", calendar.month());
					outstr += tmp;
					break;

				case 'M':	// minute [00,59]; leading zero is  permitted  but  not required
					sw_snprintf(tmp, sizeof(tmp), "%02d", minute());
					outstr += tmp;
					break;

				case 'n':	// insert a newline
					outstr += "\n";
					break;

				case 'p':	// locale's equivalent of either a.m. or p.m.
					outstr += isAM()?loc.amString(true):loc.pmString(true);
					break;

				case 'P':	// locale's equivalent of either a.m. or p.m.
					outstr += isAM()?loc.amString(false):loc.pmString(false);
					break;

				case 'r':	// appropriate time  representation  in  12-hour  clock format with %p
					outstr += format("%I:%M:%S %p");
					break;

				case 'R':	// time as %H:%M
					outstr += format("%H:%M");
					break;

				case 'S':	// seconds [00,61]
					sw_snprintf(tmp, sizeof(tmp), "%02d", second());
					outstr += tmp;
					break;

				case 't':	// insert a tab
					outstr += _T("\t");
					break;

				case 'T':	// time as %H:%M:%S
					outstr += format("%H:%M:%S");
					break;

				case 'u':	// weekday as a decimal number [1,7], with 1 represent- ing Sunday
					v = calendar.dayOfWeek();
					if (v == 0) v = 7;
					sw_snprintf(tmp, sizeof(tmp), "%d", v);
					outstr += tmp;
					break;

				case 'U':	// week number of year as  a  decimal  number  [00,53], with Sunday as the first day of week 1
					sw_snprintf(tmp, sizeof(tmp), "%02d", calendar.weekOfYear());
					outstr += tmp;
					break;

				case 'V':	// week number of the year as a decimal number [01,53], with  Monday  as  the first day of the week.  If the week containing 1 January has four or more  days  in the  new  year, then it is considered week 1; other- wise, it is week 53 of the previous  year,  and  the next week is week 1.
					sw_snprintf(tmp, sizeof(tmp), "%02d", calendar.weekOfYear());
					outstr += tmp;
					break;

				case 'w':	// weekday as a decimal number [0,6], with 0 represent- ing Sunday
					sw_snprintf(tmp, sizeof(tmp), "%d", calendar.dayOfWeek());
					outstr += tmp;
					break;

				case 'W':	// week number of year as  a  decimal  number  [00,53], with Monday as the first day of week 1
					sw_snprintf(tmp, sizeof(tmp), "%02d", calendar.weekOfYear());
					outstr += tmp;
					break;

				case 'x':	// locale's appropriate date representation
					outstr += format("%d");
					outstr += loc.dateSeparator();
					outstr += format("%m");
					outstr += loc.dateSeparator();
					outstr += format("%Y");
					break;

				case 'X':	// locale's appropriate time representation
					outstr += format("%H");
					outstr += loc.timeSeparator();
					outstr += format("%M");
					outstr += loc.timeSeparator();
					outstr += format("%S");
					break;

				case 'y':	// year within century [00,99]
					sw_snprintf(tmp, sizeof(tmp), "%02d", calendar.year() % 100);
					outstr += tmp;
					break;

				case 'Y':	// year, including the century (for example 1993)
					sw_snprintf(tmp, sizeof(tmp), "%d", calendar.year());
					outstr += tmp;
					break;

				case 'z':	// time zone name or abbreviation, or no  bytes  if  no time zone information exists
				case 'Z':	// time zone name or abbreviation, or no  bytes  if  no time zone information exists
					{
					outstr += tzName(dst());
					}

					break;

				default:
					// Don't know so keep it!
					outstr += "%";
					outstr += c;
					break;
				}

			p = q;
			}

		// Add any remaining chars
		outstr += formatString.mid(p);

		return outstr;
		}


// Return the current time.
SWTime
SWTime::now()
		{
		SWTime	t;

		t.update();

		return t;
		}



// Prefix increment operator.
const SWTime &
SWTime::operator++()
		{
		m_seconds++;
		calculateDST();

		return *this;
		}


// Postfix increment operator.
SWTime
SWTime::operator++(int)
		{
		SWTime tmp(*this);

		++m_seconds;
		calculateDST();

		return tmp;
		}


// Prefix decrement operator.
const SWTime &
SWTime::operator--()
		{
		m_seconds--;
		calculateDST();

		return *this;
		}


// Postfix decrement operator.
SWTime
SWTime::operator--(int)
		{
		SWTime tmp(*this);

		--m_seconds;
		calculateDST();

		return tmp;
		}

// += operator (int)
SWTime &
SWTime::operator+=(int v)
		{
		m_seconds += v;
		calculateDST();

		return *this;
		}


// -= operator (int)
SWTime &
SWTime::operator-=(int v)
		{
		m_seconds -= v;
		calculateDST();

		return *this;
		}

// += operator (double)
SWTime &
SWTime::operator+=(double v)
		{
		adjust(v);

		return *this;
		}


// -= operator (double)
SWTime &
SWTime::operator-=(double v)
		{
		adjust(-v);

		return *this;
		}

sw_status_t
SWTime::writeToStream(SWOutputStream &stream) const
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		res = stream.write(m_seconds);
		if (res == SW_STATUS_SUCCESS) res = stream.write(m_nanoseconds);

		return res;
		}


sw_status_t
SWTime::readFromStream(SWInputStream &stream)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		sw_int64_t		seconds=0;
		sw_uint32_t		nanoseconds=0;

		res = stream.read(seconds);
		if (res == SW_STATUS_SUCCESS) res = stream.read(nanoseconds);
		if (res == SW_STATUS_SUCCESS)
			{
			m_seconds = seconds;
			m_nanoseconds = nanoseconds;
			}

		return res;
		}




sw_status_t
SWTime::setSystemTime(const SWTime &t)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

#if defined(SW_PLATFORM_WINDOWS)
		SYSTEMTIME	systime;

		t.toSYSTEMTIME(systime);
				
		// We need to get a special privilege to adjust the system time
		HANDLE				hToken;	/* process token */
		TOKEN_PRIVILEGES	tp;		/* token provileges */
		TOKEN_PRIVILEGES	oldtp;	/* old token privileges */
		DWORD				dwSize = sizeof (TOKEN_PRIVILEGES);          
		LUID				luid;    
		bool				ok=true;

		/* now, set the SE_SYSTEMTIME_NAME privilege to our current
		*  process, so we can call SetSystemTime()
		*/

		if (!OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
			{
			res = GetLastError();
			// printf("OpenProcessToken() failed with code %d\n", GetLastError());
			ok = false;
			}
				
		if (ok && !LookupPrivilegeValue(NULL, SE_SYSTEMTIME_NAME, &luid))
			{
			res = GetLastError();
			// printf("LookupPrivilege() failed with code %d\n", GetLastError());
			ok = false;
			CloseHandle(hToken);
			}

		ZeroMemory(&tp, sizeof (tp));

		tp.PrivilegeCount = 1;
		tp.Privileges[0].Luid = luid;
		tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

		/* Adjust Token privileges */
		if (ok && !AdjustTokenPrivileges(hToken, FALSE, &tp, sizeof(TOKEN_PRIVILEGES), &oldtp, &dwSize))
			{
			res = GetLastError();
			// printf("AdjustTokenPrivileges() failed with code %d\n", GetLastError());
			CloseHandle(hToken);
			ok = false;
			}
      
		/* Set time */
		if (ok && !SetSystemTime(&systime))
			{
			res = GetLastError();
			// printf("SetSystemTime() failed with code %d\n", GetLastError());
			}

		if (ok)
			{
			/* disable SE_SYSTEMTIME_NAME again */
			AdjustTokenPrivileges(hToken, FALSE, &oldtp, dwSize, NULL, NULL);
			if (GetLastError() != ERROR_SUCCESS)
				{
				res = GetLastError();
				// printf("AdjustTokenPrivileges() failed with code %d\n", GetLastError());
				}
   
			CloseHandle(hToken);
			}
#else
		sw_timeval_t	sv=t.toTimeVal();
		struct timeval	tv;

		tv.tv_sec = sv.tv_sec;
		tv.tv_usec = sv.tv_usec;

		if (settimeofday(&tv, NULL) < 0) res = errno;
#endif
		
		return res;
		}


