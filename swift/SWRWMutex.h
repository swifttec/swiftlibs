/**
*** @file		SWRWMutex.h
*** @brief		An abstract reader-writer mutex class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWRWMutex class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWRWMutex_h__
#define __SWRWMutex_h__



#include <swift/SWMutex.h>




/**
*** @brief An abstract reader-writer mutex class.
***
*** SWRWMutex is an abstract reader-writer mutex class that forms the base of
*** all reader-write mutex classes.
*** 
*** A reader-writer mutex allows several readers to lock a resource and access
*** is simultaneously. A writer can lock a resource for writing which will then
*** prevent all other readers and writers access until it is released.
***
*** As with other SWMutex derived classes there are Thread-based and process-based
*** derivatives. Currently there are no recursive implementations.
*** 
*** @see		SWThreadRWMutex, SWProcessRWMutex
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWRWMutex : public SWMutex
		{
protected:
		/// Protected constructor - to allow construction only by derived classes
		SWRWMutex(sw_uint32_t flags);

public:
		virtual ~SWRWMutex();

		/// Returns TRUE if this thread/process already has this mutex locked.
		virtual bool		hasLock()		{ return (hasReadLock() || hasWriteLock()); }

		/// Returns TRUE if this thread/process already has this mutex locked.
		virtual bool		hasReadLock()=0;

		/// Returns TRUE if this thread/process already has this mutex locked.
		virtual bool		hasWriteLock()=0;

		/// Check to see if this mutex is locked by someone.
		virtual bool		isLocked()		{ return (isLockedForRead() || isLockedForWrite()); }

		/// Check to see if this mutex is locked by someone for read access.
		virtual bool		isLockedForRead()=0;

		/// Check to see if this mutex is locked by someone for write access.
		virtual bool		isLockedForWrite()=0;

		/// Unlock/Release this mutex.
		virtual sw_status_t	release()=0;

		/**
		*** @brief	Lock/Acquire this mutex.
		***
		*** This method is provided for compatability with the SWMutex class from
		*** which it is derived. The action of doing a simple acquire on a SWRWMutex
		*** is to acquire the write lock, i.e. an exclusive lock.
		**/
		virtual sw_status_t	acquire(sw_uint64_t waitms=SW_TIMEOUT_INFINITE);

		/**
		*** @brief	Acquire the mutex for reading.
		***
		*** This method acquires the read part of the SWRWMutex. Multiple readers can
		*** acquire the read lock simultaneously. If a writer is waiting on a lock 
		*** the reader should be blocked until the writer has obtained the lock.
		**/
		virtual sw_status_t	acquireReadLock(sw_uint64_t waitms=SW_TIMEOUT_INFINITE)=0;

		/**
		*** @brief	Acquire the mutex for writing
		***
		*** This will block the until all the readers and any other writer have released
		*** their locks.
		**/
		virtual sw_status_t	acquireWriteLock(sw_uint64_t waitms=SW_TIMEOUT_INFINITE)=0;
		};





#endif
