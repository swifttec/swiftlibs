/**
*** @file		SWProcessMutex.h
*** @brief		A process mutex
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWProcessMutex class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWProcessMutex_h__
#define __SWProcessMutex_h__

#include <swift/SWMutex.h>
#include <swift/SWString.h>
#include <swift/SWThreadMutex.h>




/**
*** @brief	Inter-process version of a Mutex.
***
*** Process-specific version of a Mutex. This is a non-recursive mutex implementation
*** which means that any attempt for the mutex to be re-acquired by the any thread within the process
*** would block indefinitely. To prevent this situation the SWProcessMutex class will throw a 
*** MutexAlreadyHeldException if this is detected.
***
*** Process mutexes are associated with a name which allows
***
*** @see		Mutex
*** @author		Simon Sparkes
***
*** @todo		Improve support for isLocked
*/
class SWIFT_DLL_EXPORT SWProcessMutex : public SWMutex
		{
public:
		/// Constructor allowing the caller to determine if this mutex can be called recursively.
		SWProcessMutex(const SWString &name, bool recursive=false);

		/// Virtual destructor
		virtual ~SWProcessMutex();

		/// Lock/Acquire this mutex. The same thread can acquire the mutex more than once.
		virtual sw_status_t		acquire(sw_uint64_t waitms=SW_TIMEOUT_INFINITE);

		/// Unlock/Release this mutex. If the mutex has been recursively acquired the mutex
		/// will only be unlocked when the recursive lock count reaches zero.
		virtual sw_status_t		release();

		/// Returns true if this thread already has this mutex locked.
		virtual bool			hasLock();

		/// Check to see if this mutex is locked by someone.
		virtual bool			isLocked();

private:
		SWThreadMutex	m_tguard;		///< A thread guard
		sw_mutex_t		m_hMutex;		///< The actual process mutex
		};




#endif
