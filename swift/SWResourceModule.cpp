/**
*** @file		SWResourceModule.cpp
*** @brief		An object to represent a single resource module (e.g. a DLL)
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWResourceModule class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




// Needed for precompiled header support
#include "stdafx.h"

#include <swift/SWResourceModule.h>
#include <swift/SWLoadableModule.h>
#include <swift/SWGuard.h>
#include <swift/SWTString.h>
#include <swift/SWLocale.h>

#if defined(SW_PLATFORM_WINDOWS)
	#pragma comment(lib, "user32.lib")
#else
/*
	// Dynamic file linking
	#include <dlfcn.h>
	#include <link.h>
*/
#endif


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif



/**
*** Default constructor with optional filename
**/
SWResourceModule::SWResourceModule(const SWString &filename, SWLoadableModule::LibType type) :
	m_hModule(0),
	m_type(None)
		{
		if (!filename.isEmpty()) load(filename, type);
		}


/**
*** Destructor
**/
SWResourceModule::~SWResourceModule()
		{
		unload();
		}



#if defined(WIN32) || defined(_WIN32)
SWResourceModule::SWResourceModule(HINSTANCE hInst) :
	m_hModule(hInst),
	m_type(WindowsHandle)
		{
		}
#endif // WIN32



/**
*** Unload the resource module
**/
sw_status_t
SWResourceModule::unload()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (m_hModule != NULL)
			{
			switch (m_type)
				{
				case LoadableModule:
					res = ((SWLoadableModule *)m_hModule)->close();
					delete (SWLoadableModule *)m_hModule;
					break;

				case Windows:
					break;

				case WindowsHandle:
					break;

				case None:
					break;
				}

			m_hModule = 0;
			m_type = None;
			}

		return res;
		}


/**
*** Load the resource module from the given file
**/
sw_status_t
SWResourceModule::load(const SWString &filename, SWLoadableModule::LibType type)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		unload();

		SWLoadableModule	*plm=new SWLoadableModule;
		
		if (plm->open(filename, type) == SW_STATUS_SUCCESS)
			{
			m_hModule = plm;
			m_type = LoadableModule;
			}
		else
			{
			delete plm;
			plm = NULL;
			}

		if (m_hModule != NULL)
			{
			m_filename = filename;
			}
		else
			{
#if defined(WIN32) || defined(_WIN32)
			res = GetLastError();
#else
			res = errno;
#endif
			}

		return res;
		}


#if defined(SW_PLATFORM_WINDOWS)

static bool
getStringFromStringTable(UINT uStringID, HINSTANCE hModule, LANGID langid, SWString &res)
		{
		bool		loaded=false;
		wchar_t		*pwchMem, *pwchCur;
		UINT		idRsrcBlk = uStringID / 16 + 1;
		int			strIndex  = uStringID % 16;
		HRSRC		hResource = NULL;

		res.clear();
		hResource = FindResourceEx(hModule, RT_STRING, MAKEINTRESOURCE(idRsrcBlk), langid);
		// hResource = FindResourceEx(hModule, RT_STRING, MAKEINTRESOURCE(idRsrcBlk), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT));

		if (hResource != NULL)
			{
			pwchMem = (wchar_t *)LoadResource(hModule, hResource);

			if (pwchMem != NULL)
				{
				pwchCur = pwchMem;
				for (int i=0;i<16;i++)
					{
					if (*pwchCur)
						{
						int cchString = *pwchCur;  // String size in characters.
						pwchCur++;
						if( i == strIndex )
							{
#pragma warning(disable: 4996)
							// The string has been found in the string table.
							wchar_t *pwchTemp;

							pwchTemp = (wchar_t *)res.lockBuffer(cchString+1, sizeof(wchar_t));
							wcsncpy(pwchTemp, pwchCur, cchString );
							pwchTemp[cchString] = 0;
							res.unlockBuffer();
#pragma warning(default: 4996)
							
							return true;
							}
						pwchCur += cchString;
						}
					else pwchCur++;
					}
				}
			}

		return loaded;
		}


static bool
loadStringFromWindowsHandle(HINSTANCE hInst, sw_resourceid_t id, SWResource::ResourceType rtype, SWString &str, LANGID lang)
		{
		if (rtype == SWResource::String)
			{
			return getStringFromStringTable(id, hInst, lang, str);
			}

		bool		loaded=false;
		SWString	tstr;
		sw_status_t	res=SW_STATUS_BUFFER_TOO_SMALL;
		int			bufsize=256;
		DWORD		nb;

		while (res == SW_STATUS_BUFFER_TOO_SMALL)
			{
			bufsize *= 2;

			/*
			if (rtype == SWResource::String)
				{
#if defined(SW_BUILD_UNICODE)
				nb = LoadStringW(hInst, id, (LPTSTR)tstr.lockBuffer(bufsize, sizeof(TCHAR)), bufsize);
#else
				nb = LoadStringA(hInst, id, (LPTSTR)tstr.lockBuffer(bufsize, sizeof(TCHAR)), bufsize);
#endif
				}
			else
				{
			*/
				nb = FormatMessage(FORMAT_MESSAGE_IGNORE_INSERTS|FORMAT_MESSAGE_FROM_HMODULE, hInst, id, lang, (LPTSTR)tstr.lockBuffer(bufsize, sizeof(TCHAR)), bufsize, 0);

			if (nb == 0) res = SW_STATUS_NOT_FOUND;
			else if (nb == (DWORD)bufsize) res = SW_STATUS_BUFFER_TOO_SMALL;
			else res = SW_STATUS_SUCCESS;

			tstr.unlockBuffer();
			}

		if (res == SW_STATUS_SUCCESS)
			{
			str = tstr;
			loaded = true;
			}

		return loaded;
		}

#endif // defined(SW_PLATFORM_WINDOWS)



/**
*** Load the string ID from this resource module.
***
*** @return	true if the string was successfully loaded, false otherwise.
**/
bool
SWResourceModule::loadString(sw_resourceid_t id, SWString &str, SWResource::ResourceType rtype, sw_uint32_t lang)
		{
		bool loaded=false;

		str.clear();
		if (m_hModule != NULL)
			{
			//loaded = v.LoadString(id, m_hModule);
			switch (m_type)
				{
				case WindowsHandle:
					{
#if defined(SW_PLATFORM_WINDOWS)
					loaded = loadStringFromWindowsHandle((HINSTANCE)m_hModule, id, rtype, str, (LANGID)lang);
#endif
					break;
					}

				case LoadableModule:
					{
#if defined(SW_PLATFORM_WINDOWS)
					SWLoadableModule	*pModule = (SWLoadableModule *)m_hModule;

					loaded = loadStringFromWindowsHandle((HINSTANCE)pModule->m_hModule, id, rtype, str, (LANGID)lang);
#endif
					}
					break;

				case Windows:
					break;

				case None:
					break;
				}
			}

		return loaded;
		}


bool
SWResourceModule::loadString(sw_resourceid_t id, SWString &str, SWResource::ResourceType rtype)
		{
#if defined(SW_PLATFORM_WINDOWS)
		bool		r=false;
		SWLocale	&clc=SWLocale::getCurrentLocale();
		SWLocale	&ulc=SWLocale::getUserLocale();
		SWLocale	&slc=SWLocale::getSystemLocale();

		// Check the current locale
		if (!r) r = loadString(id, str, rtype, clc.id());
		if (!r) r = loadString(id, str, rtype, MAKELANGID(PRIMARYLANGID(clc.id()), SUBLANG_NEUTRAL));

		// Check the user locale if different
		if (ulc.id() != clc.id())
			{
			if (!r) r = loadString(id, str, rtype, ulc.id());
			if (!r) r = loadString(id, str, rtype, MAKELANGID(PRIMARYLANGID(ulc.id()), SUBLANG_NEUTRAL));
			}

		// Check the system locale if different from the previous 2
		if (slc.id() != clc.id() && slc.id() != ulc.id())
			{
			if (!r) r = loadString(id, str, rtype, slc.id());
			if (!r) r = loadString(id, str, rtype, MAKELANGID(PRIMARYLANGID(slc.id()), SUBLANG_NEUTRAL));
			}

		/*
		lid = GetUserDefaultLangID();
		if (!r) r = loadString(id, str, rtype, lid);
		if (!r) r = loadString(id, str, rtype, MAKELANGID(PRIMARYLANGID(lid), SUBLANG_NEUTRAL));

		lid = GetSystemDefaultLangID();
		if (!r) r = loadString(id, str, rtype, lid);
		if (!r) r = loadString(id, str, rtype, MAKELANGID(PRIMARYLANGID(lid), SUBLANG_NEUTRAL));
		*/

		// Finally check all neutral
		if (!r) r = loadString(id, str, rtype, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT));

		return r;
#else
		return loadString(id, str, rtype, 0);
#endif
		}




