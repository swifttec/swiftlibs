/**
*** A collection of miscellaneous functions
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#ifndef __sw_misc_h__
#define __sw_misc_h__

#include <swift/SWString.h>
#include <swift/SWTime.h>

SWIFT_DLL_EXPORT bool		sw_misc_getEnvironmentVariable(const SWString &var, SWString &value);
SWIFT_DLL_EXPORT bool		sw_misc_setEnvironmentVariable(const SWString &var, const SWString &value);

//SWIFT_DLL_EXPORT void 	sw_misc_runProgram(SWString cmdline, bool waitForProcess=true, int *exitcode=NULL);

#define SW_CONVERTBYTES_COMPACT_FORMAT		0x01
#define SW_CONVERTBYTES_NO_INSERT_SPACE		0x02
#define SW_CONVERTBYTES_USE_PREFERRED_NAME	0x04
#define SW_CONVERTBYTES_USE_LONG_NAME		0x08
#define SW_CONVERTBYTES_USE_SHORT_NAME		0x10
#define SW_CONVERTBYTES_USE_NO_NAME			0x20

SWIFT_DLL_EXPORT void		sw_misc_getDefaultBytesIndexAndPrecision(sw_uint64_t num, int &index, int &precision);
SWIFT_DLL_EXPORT SWString	sw_misc_convertNumberToBytesString(sw_uint64_t num, int idx=-1, int precision=-1, int flags=0);

SWIFT_DLL_EXPORT bool		sw_misc_isWindows2000OrGreater();
SWIFT_DLL_EXPORT bool		sw_misc_isWindows2000();
SWIFT_DLL_EXPORT bool		sw_misc_isWindowsXP();
SWIFT_DLL_EXPORT bool		sw_misc_isWindowsVista();


SWIFT_DLL_EXPORT int		sw_misc_expandSize(int nbytes, int align);

// Set or clear the flags in value according to the boolean b
SWIFT_DLL_EXPORT void		sw_misc_setFlag(sw_uint32_t &value, sw_uint32_t flags, bool b);

// Test to see the given flags are set in the value
SWIFT_DLL_EXPORT bool		sw_misc_isFlagSet(const sw_uint32_t &value, sw_uint32_t flags, bool allflags=true);


SWIFT_DLL_EXPORT void		sw_misc_clear(bool &v);

SWIFT_DLL_EXPORT void		sw_misc_clear(sw_int8_t &v);
SWIFT_DLL_EXPORT void		sw_misc_clear(sw_int16_t &v);
SWIFT_DLL_EXPORT void		sw_misc_clear(sw_int32_t &v);
SWIFT_DLL_EXPORT void		sw_misc_clear(sw_int64_t &v);

SWIFT_DLL_EXPORT void		sw_misc_clear(sw_uint8_t &v);
SWIFT_DLL_EXPORT void		sw_misc_clear(sw_uint16_t &v);
SWIFT_DLL_EXPORT void		sw_misc_clear(sw_uint32_t &v);
SWIFT_DLL_EXPORT void		sw_misc_clear(sw_uint64_t &v);

SWIFT_DLL_EXPORT void		sw_misc_clear(float &v);
SWIFT_DLL_EXPORT void		sw_misc_clear(double &v);

SWIFT_DLL_EXPORT void		sw_misc_clear(SWObject &v);
SWIFT_DLL_EXPORT void		sw_misc_clear(SWTime &v);

SWIFT_DLL_EXPORT int		sw_misc_compare(sw_int8_t v1, sw_int8_t v2);
SWIFT_DLL_EXPORT int		sw_misc_compare(sw_int16_t v1, sw_int16_t v2);
SWIFT_DLL_EXPORT int		sw_misc_compare(sw_int32_t v1, sw_int32_t v2);
SWIFT_DLL_EXPORT int		sw_misc_compare(sw_int64_t v1, sw_int64_t v2);

SWIFT_DLL_EXPORT int		sw_misc_compare(sw_uint8_t v1, sw_uint8_t v2);
SWIFT_DLL_EXPORT int		sw_misc_compare(sw_uint16_t v1, sw_uint16_t v2);
SWIFT_DLL_EXPORT int		sw_misc_compare(sw_uint32_t v1, sw_uint32_t v2);
SWIFT_DLL_EXPORT int		sw_misc_compare(sw_uint64_t v1, sw_uint64_t v2);

SWIFT_DLL_EXPORT int		sw_misc_compare(float v1, float v2);
SWIFT_DLL_EXPORT int		sw_misc_compare(double v1, double v2);

#endif // __sw_misc_h__
