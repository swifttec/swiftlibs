/**
*** @file		SWSocketAddress.h
*** @brief		A object to hold the address for a socket
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWSocketAddress class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWSocketAddress_h__
#define __SWSocketAddress_h__

#include <swift/swift.h>
#include <swift/SWIPAddress.h>

#include <swift/SWString.h>



// Forward declarations

/**
*** @brief	A object to hold the address for a socket
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWSocketAddress
		{
public:
		/// An enumeration of possible SWSocketAddress types
		enum AddressType
			{
			Unknown=0,	///< Unknown address type
			IP4=1		///< An IP4 socket Address
			};

public:
		/// Default constructor
		SWSocketAddress();

		/// Hostname and port constructor (IP4)
		SWSocketAddress(const SWString &hostname, int port);

		/// Copy constructor
		SWSocketAddress(const SWSocketAddress &other);

		/// Assignment operator
		SWSocketAddress &	operator=(const SWSocketAddress &other);

		/// Destructor
		virtual ~SWSocketAddress();

		/// Clear this address - zeros the address information and sets the size to zero.
		virtual void	clear();

		/// Sets the address from the given data and length
		void			set(const void *pVAddr, size_t len);

		/// Get the address size
		size_t			length() const					{ return _len; }

		/// Get the address family.
		sw_uint16_t		family() const					{ return _addr.family; }

		/// Return the type of this address
		AddressType		type() const					{ return _type; }

		// Cast operator for sockaddr
		//operator const struct sockaddr *() const		{ return (struct sockaddr *)&_addr; }


		/**
		*** @brief	Converts the address part to a SWIPAddress.
		***
		*** This method attempts to convert the address part of this socket address
		*** to a SWIPAddress. This will only work if the address type of this object
		*** is IP4 or IP6. In all other cases this method will thrown an execption.
		**/
		SWIPAddress		toIPAddress() const;

		/// Return the IP port
		int				toIPPort() const;

		/// Return a pointer to the address data
		const sw_sockaddr_t *	data() const			{ return &_addr; }

protected:
		AddressType		_type;	///< The type of this address
		sw_sockaddr_t	_addr;	///< The socket address data
		size_t			_len;	///< The size of the data in the socket address
		};



#endif
