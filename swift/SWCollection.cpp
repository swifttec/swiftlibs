/**
*** @file		SWCollection.cpp
*** @brief		Common code for ALL collections
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWCollection class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <swift/SWCollection.h>
#include <swift/SWGuard.h>


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




int
SWCollection::compareData(const void *pData1, const void *pData2) const
		{
		int	res=0;

		if (pData1 < pData2) res = -1;
		else if (pData1 > pData2) res = 1;

		return res;
		}



void
SWCollection::initIterator(SWAbstractIterator &it, SWCollectionIterator *pCollectionIterator)
		{
		it.setIterator(pCollectionIterator);
		}


/*
// Adds all the objects in the collection to this collection
bool		
SWCollection::addAll(SWCollection *coll)
		{
		SWGuard		guard(this);
		SWIterator	it = coll->iterator();
		bool		added = false;

		while (it.hasNext())
			{
			add(it.next());
			added = true;
			}

		return added;
		}


// Returns true if this list contains the specified element.
bool		
SWCollection::contains(SWObject *obj)
		{
		SWGuard		guard(this);
		SWIterator	it = iterator();
		bool		ans = false;

		while (it.hasNext()) 
			{
			if (it.next() == obj)
				{
				ans = true;
				break;
				}
			}

		return ans;
		}


// Returns true if this list contains all the  element in the collection.
bool		
SWCollection::containsAll(SWCollection *coll)
		{
		SWGuard		guard(this);
		SWIterator	it = coll->iterator();
		bool		ans = true;

		while (it.hasNext())
			{
			if (!(contains(it.next()))) 
				{
				ans = false;
				break;
				}
			}

		return ans;
		}


// Returns an iterator over the elements of this collection
SWIterator	
SWCollection::iterator()
		{
		SWIterator	it;

		return it;
		}


// Removes the first occurrence of the specified element in this list.
bool		
SWCollection::remove(SWObject *o)
		{
		SW_THROW(UnsupportedOperationException);
		return false;
		}


// Removes all elements contained in the given collection
bool		
SWCollection::removeAll(SWCollection *coll)
		{
		bool		modified=false;
		SWIterator	e=iterator();

		lock();
		while (e.hasNext()) 
			{
			if (coll->contains(e.next())) 
			{
			e.remove();
			modified = true;
			}
			}
		unlock();

		return modified;
		}


// Removes all elements NOT contained in the given collection
bool		
SWCollection::retainAll(SWCollection *coll)
		{
		SWIterator	it = iterator();
		bool		removed = false;

		lock();
		while (it.hasNext())
			{
			SWObject	*obj = it.next();

			if (!(coll->contains(obj))) 
			{
			it.remove();
			removed = true;
			}
			}
		unlock();

		return removed;
		}
*/


