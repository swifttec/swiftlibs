/****************************************************************************
**	SWServiceApp.h	A class for testing and controlling services (primarily Win32)
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWServiceApp_h__
#define __SWServiceApp_h__

#include <swift/SWObject.h>
#include <swift/SWStringArray.h>
#include <swift/SWLog.h>

class SWIFT_DLL_EXPORT SWServiceApp : public SWLockableObject
		{
public:
		enum StartType
			{
			StartDisabled=0,
			StartAuto,
			StartOnDemand,
			};
			
public:
		SWServiceApp(const SWString &name, const SWString &title);
		virtual ~SWServiceApp();
		
		virtual sw_status_t	init();
		virtual sw_status_t	run()=0;
		virtual sw_status_t	suspend();
		virtual sw_status_t	resume();
		virtual sw_status_t	stop(bool waitfor=false);
		virtual sw_status_t	start();
		virtual sw_status_t	install(const SWString &name, const SWString &title, int starttype);
		virtual sw_status_t	uninstall();
		
		const SWString &	name() const			{ return m_name; }
		
		const SWString &	title() const			{ return m_title; }
		
		/// Determine if we should keep running
		bool				runnable() const		{ return m_run; }

		/// Determine if we should pause
		bool				pauseflag() const		{ return m_pause; }

		/// Determine if we are paused
		bool				paused() const			{ return m_paused; }

		/// Determine if we are running
		bool				running() const			{ return m_running; }

		/// Set the paused flag
		void				setPausedFlag(bool v)	{ m_paused = v; }

		/// Set the paused flag
		void				setRunningFlag(bool v)	{ m_run = v; }

		/// Check to see if we are running in test mode (i.e. not a real service)
		bool				isTestMode() const		{ return !m_serviceflag; }

		/// Check to see if we are running in service mode (i.e. a real service)
		bool				isServiceMode() const	{ return m_serviceflag; }

		/// Set the log handler
		void				setLog(SWLog *pLog)		{ m_pLog = pLog; }

		/// Execute this service in non-service / test mode
		void				execute();

		/**
		*** @brief	Sleep for the specified number of milliseconds or until not runnable.
		***
		*** This method will sit in a loop sleeping a short amount of time (as determined by maxsleepms)
		*** until the specified number of milliseconds have passed. If the service ceases to be runnable
		*** this method will return false, otherwise it returns true.
		**/
		bool				waitFor(int ms, int maxsleepms=100);

public:
		/// Install a service
		static sw_status_t	install(const SWString &name, const SWString &title, const SWString &path, int starttype=StartOnDemand);

		/// Uninstall this service
		static sw_status_t	uninstall(const SWString &name);

public: // INTERNAL methods which have to be public
#if defined(SW_PLATFORM_WINDOWS)
		bool	dispatch();
		void	start(DWORD argc, LPTSTR *argv);
		void	control(DWORD opcode);
#endif

protected:
		SWString				m_name;
		SWString				m_title;
#if defined(SW_PLATFORM_WINDOWS)
		SERVICE_STATUS          m_status; 
		SERVICE_STATUS_HANDLE   m_statusHandle; 
		SC_HANDLE				m_hManager;
#endif
		SWStringArray			m_args;
		bool					m_run;
		bool					m_pause;
		bool					m_running;
		bool					m_paused;
		bool					m_serviceflag;
		SWLog					*m_pLog;
		};


#endif
