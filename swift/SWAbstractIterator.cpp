/**
*** @file		SWAbstractIterator.cpp
*** @brief		Implementation of Iterator class used for iterating through collections
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWAbstractIterator class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWAbstractIterator.h>
#include <swift/SWCollection.h>
#include <swift/SWCollectionIterator.h>
#include <swift/SWException.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif



// Constructor
SWAbstractIterator::SWAbstractIterator() :
	SWObject(),
	m_pIterator(NULL)
		{
		}


// Constructor
SWAbstractIterator::SWAbstractIterator(const SWAbstractIterator &other) :
	SWObject(),
	m_pIterator(NULL)
		{
		*this = other;
		}



// Destructor
SWAbstractIterator::~SWAbstractIterator()
		{
		setIterator(NULL);
		}


// Assignment
SWAbstractIterator &
SWAbstractIterator::operator=(const SWAbstractIterator &other)
		{
		setIterator(other.m_pIterator);

		return *this;
		}


// Returns true if the iteration has more elements.
bool
SWAbstractIterator::hasCurrent()
		{
		return (m_pIterator?m_pIterator->hasCurrent():false);
		}


// Returns true if the iteration has more elements.
bool
SWAbstractIterator::hasNext()
		{
		return (m_pIterator?m_pIterator->hasNext():false);
		}


// Returns true if the iteration has more elements.
bool
SWAbstractIterator::hasPrevious()
		{
		return (m_pIterator?m_pIterator->hasPrevious():false);
		}




SWCollection *
SWAbstractIterator::collection() const
		{
		if (!m_pIterator) throw SW_EXCEPTION(SWIllegalStateException);
		return m_pIterator->m_pCollection;
		}


void
SWAbstractIterator::setIterator(SWCollectionIterator *pCollectionIterator)
		{
		if (m_pIterator != NULL)
			{
			m_pIterator->dropRef();
			if (m_pIterator->refCount() == 0) 
				{
				delete m_pIterator;
				m_pIterator = 0;
				}
			}

		m_pIterator = pCollectionIterator;
		
		if (m_pIterator != NULL)
			{
			m_pIterator->addRef();
			}
		}



