/*
*** @file		SWQueue.h
*** @brief		Generic synchronised queue based on the SWList class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWQueue class.
***
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWQueue_h__
#define __SWQueue_h__

#include <swift/swift.h>
#include <swift/SWThreadCondVar.h>
#include <swift/SWThreadMutex.h>
#include <swift/SWAbstractQueue.h>
#include <swift/SWIterator.h>




/**
*** @brief	A thread-safe synchronised object/pointer queue.
***
*** A thread-safe queue class that can be used to pass objects
*** between threads. Typically used for events and other messaging
*** type functions.
***
*** The queue supports the concept of being active (enabled). If the queue
*** is active (the normal state) operations (add/get/peek) work as normal.
*** However setting the queue to an inactive state will cause operations
*** (add/get/peek) to throw a QueueInactiveException.
***
*** In addition to support multiple threads disabling or shutting down a queue
*** will wake up any thread in a waiting state in get.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWQueue : public SWAbstractQueue
		{
public:
		/**
		*** @brief	Default constructor - creates a queue using it's own mutex
		**/
		SWQueue();


		/**
		*** @brief	Constructor with supplied mutex
		**/
		SWQueue(SWThreadMutex *mu);

		/**
		*** @brief	Constructor with supplied mutex
		**/
		SWQueue(SWThreadMutex &mu);

		/**
		*** @brief	Virtual destructor
		**/
		virtual ~SWQueue();


		/**
		*** @brief	Append a message on the queue
		***
		*** This method adds the specified message to the queue. If the queue is inactive a 
		*** SWQueueInactiveException will be thrown.
		***
		*** @param[in]	pMsg	A pointer to the message to add to the queue
		***
		*** @return				A bool indicating success or failure.
		**/
		bool		add(void *pMsg);

		/**
		*** @brief	Gets a message from the queue
		***
		*** Gets and removes a message from the queue. If the queue is inactive a 
		*** SWQueueInactiveException will be thrown. If no object is available
		*** the thread will block until a message is available or the queue
		*** is interrupted, or a timeout occurs.
		***
		*** @param[in]	waitms		The time in milliseconds to wait until timeout occurs.
		***							Can be specified as SW_TIMEOUT_INFINITE
		*** @param[in]	peekflag	If true the message returned is not removed from the queue.
		***
		*** @return		The messahe found on the queue or NULL if no objects are available
		***				and a timeout occurs.
		***
		*** @throws		SWQueueInactiveException	If the queue is inactive
		**/
		void *		get(sw_uint64_t waitms=SW_TIMEOUT_INFINITE, bool peekflag=false);

		/**
		*** @brief	Gets a message from the queue without removing it
		***
		*** Gets an object from the queue without removing it. If the queue is inactive a 
		*** SWQueueInactiveException will be thrown. If no object is available
		*** the thread will block until a message is available or the queue
		*** is interrupted, or a timeout occurs.
		***
		*** @param[in]	waitms		The time in milliseconds to wait until timeout occurs.
		***							Can be specified as SW_TIMEOUT_INFINITE
		***
		*** @return		The messahe found on the queue or NULL if no objects are available
		***				and a timeout occurs.
		***
		*** @throws		SWQueueInactiveException	If the queue is inactive
		**/
		void *		peek(sw_uint64_t waitms=SW_TIMEOUT_INFINITE);

		/**
		*** @brief	Returns an iterator on the queue.
		**/
		SWIterator<void *>	iterator(bool iterateFromEnd=false) const	{ return m_list.iterator(iterateFromEnd); }

public: // Overrides for SWAbstractQueue methods

		/**
		*** @brief	Enable the specified operations on the queue.
		***
		*** By default this method enables all queue operations, but the
		*** queue operations can selectively enabled by specifying a combination
		*** of the SW_QUEUE_OPERATION_XXX flags.
		***
		*** @param[in]	flags	Flags specifying the queue operations to enable (default SW_QUEUE_OPERATION_ALL).
		**/
		virtual void		enable(int flags=SW_QUEUE_OPERATION_ALL);

		/**
		*** @brief	Enable the specified operations on the queue.
		***
		*** By default this method disables all queue operations, but the
		*** queue operations can selectively disabled by specifying a combination
		*** of the SW_QUEUE_OPERATION_XXX flags.
		***
		*** @param[in]	flags	Flags specifying the queue operations to disable (default SW_QUEUE_OPERATION_ALL).
		**/
		virtual void		disable(int flags=SW_QUEUE_OPERATION_ALL);

		/**
		*** @brief	Shutdown operations on the queue.
		***
		*** By default shutdown shuts down both the reader and writer of
		*** the queue (flag disableReaderWhenEmpty=false).
		*** If the disableReaderWhenEmpty flag is true then the queue is disabled
		*** only when the queue empties.
		***
		*** @param[in]	disableReaderWhenEmpty	Specifies when to disable queue reads
		**/
		virtual void		shutdown(bool disableReaderWhenEmpty=false);

		/**
		*** @brief	Interupt any thread waiting on the queue conditional var.
		***
		*** By default interrupt any readers immediately (flag interruptReaderWhenEmpty=false).
		*** If the interruptReaderWhenEmpty flag is true then the readers are interrupted
		*** only when the queue empties.
		***
		*** @param[in]	interruptReaderWhenEmpty	A flag to indicate when the interrupt should occur.
		**/
		virtual void		interrupt(bool interruptReaderWhenEmpty=false);

		/// Returns the number of elements in the queue.
		virtual sw_size_t			size() const;

		/**
		*** @brief	Empty the queue of all messages
		***
		*** @param[in]	emptyInactiveQueue	If true this method will bypass the
		***									queue inactive checks in order to force the
		***									queue to an empty state.
		***
		*** @throws SWQueueInactiveException	If the queue is inactive
		**/
		virtual void		clear(bool emptyInactiveQueue);

public: // Overrides for SWCollection methods
		virtual void		clear()		{ clear(false); }

private:
		/// Internal initialisation
		void		init(SWThreadMutex *mu, bool ownMutex);

private:
		SWList<void *>	m_list;		///< The linked list that backs the queue.
		SWThreadCondVar	*m_pcv;		///< Pointer to the conditional var associated with the queue.
		SWThreadMutex	*m_pMutex;	///< Pointer to the thread mutex
		bool			m_ownMutex;	///< A boolean to indicate if m_pMutex was generated by this object
		};



#endif
