/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"

#include <swift/SWStringAssocArray.h>
#include <swift/SWGuard.h>



SWStringAssocArray::SWStringAssocArray(bool ignoreCase) :
	m_ignoreCase(ignoreCase)
		{
		}


SWString &	
SWStringAssocArray::operator[](const SWString &key)
		{
		SWGuard								guard(this);
		SWHashMapEntry<SWString,SWString>	*pEntry;

		pEntry = m_strings.getMapEntry(key);
		if (pEntry == NULL)
			{
			void				*p;

			p = m_strings.m_pElementMemoryManager->malloc(m_strings.m_elemsize);
			if (p == NULL) throw SW_EXCEPTION(SWMemoryException);
			pEntry = new (p) SWHashMapEntry<SWString,SWString>(key, "");
			m_strings.addEntry(pEntry);
			m_strings.incSize();

			//printf("New SWString at %x\n", vp);
			}

		return pEntry->m_value;
		}


SWString	
SWStringAssocArray::get(const SWString &key) const
		{
		SWGuard								guard(this);
		SWHashMapEntry<SWString,SWString>	*pEntry;
		SWString							res;

		pEntry = m_strings.getMapEntry(key);
		if (pEntry != NULL) res = pEntry->m_value;

		return res;
		}


// Copy constructor
SWStringAssocArray::SWStringAssocArray(SWStringAssocArray &other)
		{
		// Do copy via assignment operator
		*this = other;
		}



SWStringAssocArray &
SWStringAssocArray::operator=(const SWStringAssocArray &other)
		{
		SWGuard		guard(this);

		clear();
		m_ignoreCase = other.m_ignoreCase;

		SWIterator< SWHashMapEntry<SWString,SWString> >	it = other.iterator();
		SWHashMapEntry<SWString,SWString>				*me;

		while (it.hasNext())
			{
			me = it.next();
			
			(*this)[me->key()] = me->value();
			}

		return *this;
		}


// Returns an array of the keys contained in this map.
SWStringArray
SWStringAssocArray::keysArray() const
		{
		SWGuard					guard(this);
		SWStringArray			sa;
		SWIterator<SWString>	it;
		bool					done=false;
		
		while (!done)
			{
			try
				{
				it = keys();

				while (it.hasNext())
					{
					sa.add(*(SWString *)it.next());
					}

				done = true;
				}
			catch (SWConcurrentModificationException &)
				{
				sa.clear();
				done = false;
				}
			}

		return sa;
		}


// Returns an array of the values contained in this map.
SWStringArray
SWStringAssocArray::valuesArray() const
		{
		SWGuard					guard(this);
		SWStringArray			sa;
		SWIterator<SWString>	it;
		bool					done=false;
		
		while (!done)
			{
			try
				{
				it = values();

				while (it.hasNext())
					{
					sa.add(*(SWString *)it.next());
					}
				
				done = true;
				}
			catch (SWConcurrentModificationException &)
				{
				sa.clear();
				done = false;
				}
			}

		return sa;
		}


bool
SWStringAssocArray::defined(const SWString &key) const
		{
		SWString	kstr(key);

		if (m_ignoreCase) kstr.toLower();

		return m_strings.containsKey(key);
		}


bool
SWStringAssocArray::remove(const SWString &key)
		{
		SWString	kstr(key);

		if (m_ignoreCase) kstr.toLower();

		return m_strings.remove(key);
		}


void
SWStringAssocArray::clear()
		{
		m_strings.clear();
		}



// Returns an iterator of MapEntry objects
SWIterator< SWHashMapEntry<SWString,SWString> >
SWStringAssocArray::iterator() const
		{
		return m_strings.iterator();
		}


// Returns an array of values
SWIterator<SWString>
SWStringAssocArray::values() const
		{
		return m_strings.values();
		}


// Returns an array of keys
SWIterator<SWString>
SWStringAssocArray::keys() const
		{
		return m_strings.keys();
		}


bool
SWStringAssocArray::lookup(const SWString &key, SWString &value) const
		{
		SWGuard								guard(this);
		SWHashMapEntry<SWString,SWString>	*pEntry;
		bool	res;

		pEntry = m_strings.getMapEntry(key);
		if (pEntry != NULL)
			{
			value = pEntry->value();
			res = true;
			}
		else
			{
			res = false;
			}

		return res;
		}


void
SWStringAssocArray::set(const SWString &key, const SWString &value)
		{
		(*this)[key] = value;
		}


sw_size_t
SWStringAssocArray::size() const
		{
		return m_strings.size();
		}



