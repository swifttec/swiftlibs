/**
*** @file		SWOperatingSystemInfo.cpp
*** @brief		A generic value object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWOperatingSystemInfo class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWOperatingSystemInfo.h>
#include <swift/SWString.h>
#include <swift/SWTokenizer.h>

#if defined(SW_PLATFORM_WINDOWS)
	#include <VersionHelpers.h>
#else
	#if defined(SW_PLATFORM_SOLARIS)
		#include <sys/systeminfo.h>
	#endif
	#include <sys/utsname.h>
	#include <unistd.h>
#endif

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif



SWOperatingSystemInfo::SWOperatingSystemInfo()
		{
		}


SWOperatingSystemInfo::~SWOperatingSystemInfo()
		{
		}


SWOperatingSystemInfo::SWOperatingSystemInfo(const SWOperatingSystemInfo &other) :
	SWInfoObject()
		{
		*this = other;
		}


SWOperatingSystemInfo &
SWOperatingSystemInfo::operator=(const SWOperatingSystemInfo &other)
		{
		m_values = other.m_values;

		return *this;
		}


#if defined(SW_PLATFORM_WINDOWS)
static BOOL
EqualsMajorVersion(DWORD majorVersion)
		{
		OSVERSIONINFOEX osVersionInfo;
		
		::ZeroMemory(&osVersionInfo, sizeof(OSVERSIONINFOEX));
		osVersionInfo.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
		osVersionInfo.dwMajorVersion = majorVersion;
		ULONGLONG maskCondition = ::VerSetConditionMask(0, VER_MAJORVERSION, VER_EQUAL);
		
		return ::VerifyVersionInfo(&osVersionInfo, VER_MAJORVERSION, maskCondition);
		}


static BOOL
EqualsMinorVersion(DWORD minorVersion)
		{
		OSVERSIONINFOEX osVersionInfo;
		
		::ZeroMemory(&osVersionInfo, sizeof(OSVERSIONINFOEX));
		osVersionInfo.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
		osVersionInfo.dwMinorVersion = minorVersion;
		ULONGLONG maskCondition = ::VerSetConditionMask(0, VER_MINORVERSION, VER_EQUAL);
		
		return ::VerifyVersionInfo(&osVersionInfo, VER_MINORVERSION, maskCondition);
		}

static BOOL
EqualsServicePack(WORD servicePackMajor)
		{
		OSVERSIONINFOEX osVersionInfo;
		
		::ZeroMemory(&osVersionInfo, sizeof(OSVERSIONINFOEX));
		osVersionInfo.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
		osVersionInfo.wServicePackMajor = servicePackMajor;
		ULONGLONG maskCondition = ::VerSetConditionMask(0, VER_SERVICEPACKMAJOR, VER_EQUAL);
		
		return ::VerifyVersionInfo(&osVersionInfo, VER_SERVICEPACKMAJOR, maskCondition);
		}

static BOOL
EqualsProductType(BYTE productType)
		{
		OSVERSIONINFOEX osVersionInfo;
		
		::ZeroMemory(&osVersionInfo, sizeof(OSVERSIONINFOEX));
		osVersionInfo.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);
		osVersionInfo.wProductType = productType;
		ULONGLONG maskCondition = ::VerSetConditionMask(0, VER_PRODUCT_TYPE, VER_EQUAL);
		
		return ::VerifyVersionInfo(&osVersionInfo, VER_PRODUCT_TYPE, maskCondition);
		}


static BYTE
GetProductType()
		{
		if (EqualsProductType(VER_NT_WORKSTATION))
			{
			return VER_NT_WORKSTATION;
			}
		else if (EqualsProductType(VER_NT_SERVER))
			{
			return VER_NT_SERVER;
			}
		else if (EqualsProductType(VER_NT_DOMAIN_CONTROLLER))
			{
			return VER_NT_DOMAIN_CONTROLLER;
			}
		
		return 0;
		} 

struct WindowsNTOSInfo
	{
    DWORD dwMajorVersion;
    DWORD dwMinorVersion;
    WORD wServicePackMajor;    
    BYTE ProductType;
    //const TCHAR * pcszOSDisplayName;
	};

const WindowsNTOSInfo KnownVersionsOfWindows[] =
{
    { 6, 3, 0,   },//win8.1,server2012 r2
    { 6, 2, 0,   },//win8,server2012

    { 6, 1, 1,  },//win7,win2008r2 sp1
    { 6, 1, 0,  },//win7,win2008r2

    { 5, 1, 3,  },//winxp sp3
    { 5, 1, 2,  },//winxp sp2
    { 5, 1, 1,  },//winxp sp1
    { 5, 1, 0,  },//winxp

    { 6, 0, 2,  },//WinVista,server2008 SP2
    { 6, 0, 1,  },//WinVista,Server2008 Sp1
    { 6, 0, 0,  },//WinVista,Server2008

    { 5, 2, 2,  },//Windows Server 2003 Sp2
    { 5, 2, 1,  },//Windows Server 2003 Sp1
    { 5, 2, 0,  },//Windows Server 2003


    { 5, 1, 4,  }, //Windows Server 2000 Sp4
    { 5, 1, 3,  }, //Windows Server 2000 Sp3
    { 5, 1, 2,  }, //Windows Server 2000 Sp2
    { 5, 1, 2,  }, //Windows Server 2000 Sp1
    { 5, 1, 0,  }, //Windows Server 2000
};

const size_t n_KnownVersionofWindows = sizeof(KnownVersionsOfWindows) / sizeof(WindowsNTOSInfo);

static bool
GetKnownWindowsVersion(WindowsNTOSInfo &osInfo)
		{
		for (size_t i = 0; i < n_KnownVersionofWindows; i++)
			{
			if (EqualsMajorVersion(KnownVersionsOfWindows[i].dwMajorVersion))
				{
				if (EqualsMinorVersion(KnownVersionsOfWindows[i].dwMinorVersion))
					{
					if (EqualsServicePack(KnownVersionsOfWindows[i].wServicePackMajor))
						{
						osInfo.dwMajorVersion = KnownVersionsOfWindows[i].dwMajorVersion;
						osInfo.dwMinorVersion = KnownVersionsOfWindows[i].dwMinorVersion;
						osInfo.wServicePackMajor = KnownVersionsOfWindows[i].wServicePackMajor;
						osInfo.ProductType = GetProductType();
						
						return true;
						}
					}
				}
			}
		
		return false;
		}

const DWORD MajorVersion_Start = 6;
const DWORD MinorVersion_Start = 3;
const WORD ServicePackVersion_Start = 1;

const DWORD MajorVersion_Max = 10;
const DWORD MinorVersion_Max = 5;
const WORD ServicePackVersion_Max = 4;

static bool
GetUnknownWindowsVersion(WindowsNTOSInfo &osInfo)
		{
		DWORD minorVersionCounterSeed = MinorVersion_Start;
		WORD servicePackCounterSeed = ServicePackVersion_Start;

		//by design, if we can't find the right service pack we will return true;
		for (DWORD majorVersion = MajorVersion_Start; majorVersion <= MajorVersion_Max; majorVersion++)
			{
			if (EqualsMajorVersion(majorVersion))
				{
				for (DWORD minorVersion = minorVersionCounterSeed; 
						minorVersion <= MinorVersion_Max; minorVersion++)
					{
					if (EqualsMinorVersion(minorVersion))
						{
						osInfo.dwMajorVersion = majorVersion;
						osInfo.dwMinorVersion = minorVersion;
						osInfo.ProductType = GetProductType();
						for (WORD servicePack = servicePackCounterSeed; servicePack <= ServicePackVersion_Max; servicePack++)
							{
							if (EqualsServicePack(servicePack))
								{                            
								osInfo.wServicePackMajor = servicePack;
								break;
								}
							}
						return true;
						}
					else
						{
						//reset servicepack version counter to 0 because
						//we are going to increment our minor version.
						servicePackCounterSeed = 0;
						}                
					}
				}
			else
				{
				//reset minor version to start from 0 because we are going to increment majorVersion;
				minorVersionCounterSeed = 0;
				}
			}
		
		return false;
		}

static bool
GetWindowsVersionInfo(WindowsNTOSInfo &osInfo)
		{
		bool res = GetKnownWindowsVersion(osInfo);

		if (!res)
			{
			res = GetUnknownWindowsVersion(osInfo);
			}
		
		return res;
		}

#endif


sw_status_t
SWOperatingSystemInfo::getMachineInfo(SWOperatingSystemInfo &info)
		{
		OSFamily	family=OS_FAMILY_UNKNOWN;
		OSType		type=OS_TYPE_UNKNOWN;
		SWString	name, common, desc, sysversion, version, release, familystr, build, patchlevel, revision;
		int			majver=0, minver=0;

		name = "Unknown";
		familystr = "Unknown";

#if defined(SW_PLATFORM_WINDOWS)
		family = OS_FAMILY_WINDOWS;
		familystr = "Windows";
		name = "Windows";
		type = OS_TYPE_WINDOWS;

		WindowsNTOSInfo	ver;
		bool			ok=GetWindowsVersionInfo(ver);

		if (ok)
			{
			majver = ver.dwMajorVersion;
			minver = ver.dwMinorVersion;

#ifdef DISABLED_CODE
			DWORD buildno = ver.dwBuildNumber;

			switch (ver.dwPlatformId)
				{
				case VER_PLATFORM_WIN32s:
					// Win 3.1
					type = OS_TYPE_WIN31;
					desc = "Windows 3.1";
					break;

				case VER_PLATFORM_WIN32_WINDOWS:
					// Win95/98/Me
					switch (minver)
						{
						case 0:
							type = OS_TYPE_WIN95;
							desc = "Windows 95";
							if (ver.szCSDVersion[1] == 'C' || ver.szCSDVersion[1] == 'B') desc += " OSR2";
							break;

						case 10:
							type = OS_TYPE_WIN98;
							desc = "Windows 98";
							if (ver.szCSDVersion[1] == 'A') desc += " SE";
							break;

						case 90:
							type = OS_TYPE_WINME;
							desc = "Windows Me";
							break;
						}
					break;

				case VER_PLATFORM_WIN32_NT:
					switch (majver)
						{
						case 3:
							type = OS_TYPE_WINNT351;
							desc.format("Windows NT %d.%d", majver, minver);
							break;

						case 4:
							type = OS_TYPE_WINNT4;
							desc.format("Windows NT %d.%d", majver, minver);
							if (!extended && ver.szCSDVersion)
								{
								desc += " ";
								desc += ver.szCSDVersion;
								}
							break;

						case 5:
							switch (minver)
								{
								case 0:
									type = OS_TYPE_WIN2K;
									desc = "Windows 2000";
									break;

								case 1:
									type = OS_TYPE_WINXP;
									desc = "Windows XP";
									break;

								case 2:
									type = OS_TYPE_WIN2003;
									desc = "Windows 2003";
									break;
								}
							break;

						case 6:
							switch (ver.wProductType)
								{
								case VER_NT_WORKSTATION:
									type = OS_TYPE_WINVISTA;
									desc = "Windows Vista";
									break;

								case VER_NT_DOMAIN_CONTROLLER:
									// fall into server

								case VER_NT_SERVER:
									type = OS_TYPE_WIN2008;
									desc = "Windows Server 2008";
									break;
								}
							break;

						case 7:
							switch (ver.wProductType)
								{
								case VER_NT_WORKSTATION:
									type = OS_TYPE_WIN7;
									desc = "Windows 7";
									break;

								case VER_NT_DOMAIN_CONTROLLER:
									// fall into server

								case VER_NT_SERVER:
									type = OS_TYPE_WIN2008;
									desc = "Windows Server 2008 R2";
									break;
								}
							break;
						}

					if (extended)
						{
						switch (ver.wProductType)
							{
							case VER_NT_WORKSTATION:

								if ((ver.wSuiteMask & VER_SUITE_PERSONAL) != 0 && type == OS_TYPE_WINXP) desc += " Home";
								else if ((ver.wSuiteMask & VER_SUITE_PERSONAL) == 0 && type == OS_TYPE_WINVISTA) desc += " Business";
								else desc += " Professional";
								break;

							case VER_NT_DOMAIN_CONTROLLER:
								// fall into server

							case VER_NT_SERVER:
								if (type == OS_TYPE_WINXP)
									{
									type = OS_TYPE_WINDOTNET;
									desc = "Windows .NET";
									if ((ver.wSuiteMask & VER_SUITE_ENTERPRISE) != 0) desc += " Enterprise";
									}
								else if (type == OS_TYPE_WIN2K)
									{
									desc = "Windows 2000";
									if ((ver.wSuiteMask & VER_SUITE_DATACENTER) != 0) desc += " DataCenter";
									else if ((ver.wSuiteMask & VER_SUITE_ENTERPRISE) != 0) desc += " Advanced";
									else if ((ver.wSuiteMask & VER_SUITE_SMALLBUSINESS) != 0) desc += " Small Business";
									else if ((ver.wSuiteMask & VER_SUITE_ENTERPRISE) != 0) desc += " Enterprise";
									}
								else if (type == OS_TYPE_WIN2003)
									{
									desc = "Windows 2003";
									if ((ver.wSuiteMask & VER_SUITE_ENTERPRISE) != 0) desc += " Enterprise";
									}

								desc += " Server";
								break;
							}

						if (ver.wServicePackMajor != 0)
							{
							patchlevel.format("SP%d", ver.wServicePackMajor);

							if (ver.wServicePackMinor != 0)
								{
								SWString	tmp;

								tmp.format(".%d", ver.wServicePackMinor);
								patchlevel += tmp;
								}

							desc += " ";
							desc += patchlevel;
							}

						}
					else
						{
						/*
						SWString	tmp;

						tmp = sw_registry_getString(HKEY_LOCAL_MACHINE, "SYSTEM\\CurrentControlSet\\Control\\ProductOptions", "ProductType");

						if (tmp.CompareNoCase("WINNT") == 0)
							{
							desc += " Professional";
							}
						else if (tmp.CompareNoCase("LANMANNT") == 0)
							{
							desc += " Server";
							}
						else if (tmp.CompareNoCase("SERVERNT") == 0)
							{
							desc += " Advanced Server";
							}
						*/
						}
					break;
				}
#endif // DISABLED_CODE

			version = desc;
			if (version.startsWith("Windows ")) version.remove(0, 8);

			release.format("%d.%d", majver, minver);
			}
#else // defined(SW_PLATFORM_WINDOWS)
		struct utsname	u;

		family = OS_FAMILY_UNIX;
		familystr = "Unix";
		desc = "Unknown Unix";

		if (uname(&u) >= 0)
			{
			name = u.sysname;
			release = u.release;
#if defined(SW_PLATFORM_AIX)
			SWString	vtmp;

			version = u.version;
			version += ".";
			version += u.release;
			sysversion = u.version;

			//printf("!!! u.release=%s, u.version=%s, version=%s, sysversion=%s\n", u.release, u.version, version.c_str(), sysversion.c_str());

#else
			version = sysversion = u.version;
#endif
			}

#if defined(SW_PLATFORM_SOLARIS)
		char	buf[1024];

		if (sysinfo(SI_SYSNAME, buf, sizeof(buf)) > 0) name = buf;
		if (sysinfo(SI_VERSION, buf, sizeof(buf)) > 0) version = sysversion = buf;
		if (sysinfo(SI_RELEASE, buf, sizeof(buf)) > 0) release = buf;
#endif

		if (release.first('.') > 0)
			{
			SWTokenizer	tok(release, ".");

			if (tok.getTokenCount() > 0) majver = tok[0].toInt32();
			if (tok.getTokenCount() > 1) minver = tok[1].toInt32();
			for (int idx=2;idx<tok.getTokenCount();idx++)
				{
				if (idx > 2) revision += ".";
				revision += tok[idx];
				}
			}

#if defined(SW_PLATFORM_SOLARIS)
		if (majver < 5)
			{
			common = "SunOS";
			desc = name;
			desc += " ";
			desc += release;
			type = OS_TYPE_SUNOS;
			}
		else
			{
			common = "Solaris";
			type = OS_TYPE_SOLARIS;

			if (minver < 7)
				{
				desc = "Solaris ";
				desc += release;
				}
			else
				{
				desc.format("Solaris %d", minver);
				}
			}
#endif // SW_PLATFORM_SOLARIS

#endif // defined(SW_PLATFORM_WINDOWS)

		if (common.isEmpty()) common = name;

		if (desc.isEmpty())
			{
			desc = info.getString(CommonName);
			desc += " ";
			desc += info.getString(Release);
			}

		if (majver || minver)
			{
			version.format("%u.%u", majver, minver);
			}

		info.setValue(Name, name);
		info.setValue(Type, type);
		info.setValue(CommonName, common);
		info.setValue(Description, desc);
		info.setValue(Family, family);
		info.setValue(FamilyString,	familystr);
		info.setValue(Version, version);
		info.setValue(SysVersion, sysversion);
		info.setValue(Release, release);
		info.setValue(VersionMajor, majver);
		info.setValue(VersionMinor, minver);
		info.setValue(VersionRevision, revision);
		info.setValue(BuildNumber, build);
		info.setValue(PatchLevel, patchlevel);

		return SW_STATUS_SUCCESS;
		}



