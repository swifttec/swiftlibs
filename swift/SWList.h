/**
*** @file		SWList.h
*** @brief		A templated double-linked list implementation.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWList class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWList_h__
#define __SWList_h__

#include <swift/SWGuard.h>
#include <swift/SWIterator.h>
#include <swift/SWMemoryManager.h>



// Forward Declarations
template<class T> class SWList;
template<class T> class SWListIterator;
template<class T> class SWOrderedList;
template<class T> class SWOrderedListIterator;
template<class T> class SWHashTable;
template<class T> class SWHashTableIterator;

/**
*** @internal
*** @brief	An internal object used to combine a list element and the templated data type.
**/
template<class T>
class SWListElement
		{
friend class SWList<T>;
friend class SWListIterator<T>;
friend class SWOrderedList<T>;
friend class SWOrderedListIterator<T>;
friend class SWHashTable<T>;
friend class SWHashTableIterator<T>;

protected:
		/// Default constructor
		SWListElement()											{ }

		/// Data copy constructor
		SWListElement(const T &data) : m_data(data)				{ }

		/// Custom new operator
		void *			operator new(size_t /*nbytes*/, void *p)	{ return p; }

		/// Custom delete operator
		void			operator delete(void *, void *)				{ }

		/// Custom delete operator
		void			operator delete(void *)						{ }

		/// Get a reference to the encapsualted data
		T &				data()										{ return m_data; }

protected:
		SWListElement<T>	*next;		///< A pointer to the next element in the chain.
		SWListElement<T>	*prev;		///< A pointer to the previous element in the chain.
		T					m_data;		///< The actual data
		};



/**
*** @brief	Templated form of the SWList class.
***
*** SWList is a templated version of the SWList class.
*** The primary difference between a SWList and a SWList object is that the
*** templated SWList will correctly handle the SWCollection delete flag.
***
*** In order to be stored in a SWList any data type specified by T must have the == and = operators
*** defined.
***
*** @see		SWList, SWCollection
*** @author		Simon Sparkes
**/
template<class T>
class SWList : public SWCollection
		{
friend class SWListIterator<T>;

public:
		/// Position enumeration
		enum ListPosition
			{
			ListHead,
			ListTail
			};


public:
		/// @brief	Default constructor - constructs an empty list
		SWList();

		/// Copy constructor
		SWList(const SWList<T> &other);

		/// @brief	Virtual destructor
		virtual ~SWList();

		/// Assignment operator
		SWList<T> &		operator=(const SWList<T> &other);

		/**
		*** @brief	Clears the list
		**/
		virtual void	clear();

		/**
		*** @brief	Adds a copy of the given data to the head or tail of the list.
		***
		*** The copy of the specified element is made using the copy constructor
		*** of the element. The insert will only succeed if the delete flag is set
		*** to true. If the delete flag is false this method will do nothing and
		*** return false;
		***
		*** @param	data	The data element to copy and add.
		***
		*** @return	true if successful, false otherwise.
		**/
		void			add(const T &data, ListPosition pos=ListTail);

		/// Add to list head (alternate name)
		void			addHead(const T &data);

		/// Add to list tail (alternate name)
		void			addTail(const T &data);
		
		/**
		*** @brief	Returns a pointer to the data at the head of the list
		**/
		bool			get(ListPosition pos, T &data) const;


		/// Get list head (alternate name)
		bool			getHead(T &data) const;

		/// Get list tail (alternate name)
		bool			getTail(T &data) const;

		/**
		*** @brief				Removes and returns the first element from this list.
		*** @param[in]	pData	If not NULL receives a copy of the data removed.
		**/
		bool			remove(ListPosition pos, T *pData=NULL);


		/// Remove list head (alternate name)
		bool			removeHead(T *pData=NULL);

		/// Remove list tail (alternate name)
		bool			removeTail(T *pData=NULL);

		/**
		*** @brief		Return true if the list contains the data specified (tested using operator==).
		*** @param[in]	data	The data to search for.
		**/
		bool			contains(const T &data) const;


		/**
		*** @brief		Find and return the specified data.
		***
		*** @param[in]	search	The data to search for.
		*** @param[out]	data	Receives the stored data if a match is found.
		***
		*** @return	true if found, false otherwise.
		**/
		bool			find(const T &search, T &data) const;


		/**
		*** @brief		Removes the first occurrence of the specified data (tested using operator==).
		*** @param[in]	search	The data to search for and remove if found.
		*** @param[in]	pData	If not NULL receives a copy of the data removed.
		**/
		bool			remove(const T &search, T *pData=NULL);


		/**
		*** @brief	Returns an iterator over the elements of this collection.
		***
		*** @param	iterateFromEnd	If false (default) the iterator starts
		***							at the beginning of the vector. If true
		***							the iterator starts at the end of the
		***							vector.
		**/
		SWIterator<T>	iterator(bool iterateFromEnd=false) const;


		/**
		*** @brief	Sort the data in the list.
		***
		*** This method uses a sort algorithm to sort the elements in the vector.
		*** The caller is required supply a comparision function to specify a
		*** sort order.
		***
		*** @param[in]	pCompareFunc	A custom compare function.
		**/
		void				sort(sw_collection_compareDataFunction *pCompareFunc);


protected:
		virtual SWCollectionIterator *	getIterator(bool iterateFromEnd=false) const;
		virtual int						compareData(const void *pData1, const void *pData2) const;
		void							removeElement(SWListElement<T> *pElement);
		void							addElement(SWListElement<T> *pElement, bool addToHead, sw_collection_compareDataFunction *pCompareFunc);

protected:
		SWMemoryManager		*m_pMemoryManager;	///< The memory manager
		SWListElement<T>	*m_head;			///< A pointer to the start if the list
		SWListElement<T>	*m_tail;			///< Pointer to the end of the list
		sw_size_t			m_elemsize;			///< The size of each element to be stored
		};




// Include the implementation
#include <swift/SWList.cpp>

#endif
