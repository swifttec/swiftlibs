/**
*** @file		SWStopWatch.cpp
*** @brief		A generic value object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWStopWatch class.
**/

/*********************************************************************************
Copyright (c) 2006-2016 SwiftTec Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWStopWatch.h>
#include <swift/SWString.h>
#include <swift/SWGuard.h>
#include <swift/SWOutputStream.h>
#include <swift/SWInputStream.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif

SWIFT_NAMESPACE_BEGIN


SWStopWatch::SWStopWatch() :
	m_runflag(false)
		{
		}


SWStopWatch::SWStopWatch(const SWStopWatch &other) :
	m_runflag(false)
		{
		*this = other;
		}


SWStopWatch::~SWStopWatch()
		{
		}


SWStopWatch &
SWStopWatch::operator=(const SWStopWatch &other)
		{
		reset();
		m_runflag = other.m_runflag;

		for (int i=0;i<other.count();i++)
			{
			m_timelist.add(other.m_timelist[i]);
			}


		return *this;
		}


bool
SWStopWatch::start()
		{
		SWGuard	guard(this);
		bool	res=true;

		if (!m_runflag && m_timelist.size() == 0)
			{
			m_timelist.add(SWTime(true));
			m_runflag = true;
			}
		else
			{
			res = false;
			}

		return res;
		}


bool
SWStopWatch::stop()
		{
		SWGuard	guard(this);
		bool	res=true;

		if (m_runflag)
			{
			m_timelist.add(SWTime(true));
			m_runflag = false;
			}
		else
			{
			res = false;
			}

		return res;
		}


bool
SWStopWatch::reset()
		{
		SWGuard	guard(this);
		bool	res=true;

		m_runflag = false;
		m_timelist.clear();

		return res;
		}


bool
SWStopWatch::checkpoint()
		{
		SWGuard	guard(this);
		bool	res=true;

		if (m_runflag)
			{
			m_timelist.add(SWTime(true));
			}
		else
			{
			res = false;
			}

		return res;
		}


void
SWStopWatch::diff(const SWTime &a, const SWTime &b, sw_timespec_t &tv) const
		{
		sw_int64_t		sa, sb;
		sw_uint32_t		na, nb;

		sa = a.toInt64();
		sb = b.toInt64();
		na = a.nanosecond();
		nb = b.nanosecond();

		tv.tv_sec = sa - sb;
		if (na >= nb)
			{
			tv.tv_nsec = na - nb;
			}
		else
			{
			--tv.tv_sec;
			tv.tv_nsec = 1000000000 + na - nb;
			}
		}


double
SWStopWatch::elapsed() const
		{
		SWGuard	guard(this);
		SWTime	now, then;

		if (m_runflag)
			{
			now.update();
			then = m_timelist[0];
			}
		else if (m_timelist.size() > 0)
			{
			now = m_timelist[m_timelist.size()-1];
			then = m_timelist[0];
			}
		else
			{
			now.update();
			then = now;
			}

		return ((double)now - (double)then);
		}


void
SWStopWatch::elapsed(sw_timespec_t &tv) const
		{
		SWGuard	guard(this);
		SWTime	now, then;

		if (m_runflag)
			{
			now.update();
			then = m_timelist[0];
			}
		else if (m_timelist.size() > 0)
			{
			now = m_timelist[m_timelist.size()-1];
			then = m_timelist[0];
			}
		else
			{
			now.update();
			then = now;
			}

		diff(now, then, tv);
		}


double
SWStopWatch::elapsed(int idx) const
		{
		return elapsed(0, idx);
		}


void
SWStopWatch::elapsed(int idx, sw_timespec_t &tv) const
		{
		elapsed(0, idx, tv);
		}


double
SWStopWatch::elapsed(int start, int finish) const
		{
		SWGuard	guard(this);

		if (start < 0 || finish < start || start >= count() || finish >= count())
			{
			throw SW_EXCEPTION(SWIndexOutOfBoundsException);
			}

		SWTime	now, then;

		then = m_timelist[start];
		now = m_timelist[finish];

		return ((double)now - (double)then);
		}


void
SWStopWatch::elapsed(int start, int finish, sw_timespec_t &tv) const
		{
		SWGuard	guard(this);

		if (start < 0 || finish < start || start >= count() || finish >= count())
			{
			throw SW_EXCEPTION(SWIndexOutOfBoundsException);
			}

		diff(m_timelist[finish], m_timelist[start], tv);
		}


SWTime
SWStopWatch::timeAt(int idx) const
		{
		SWGuard	guard(this);

		if (idx < 0 || idx >= count())
			{
			throw SW_EXCEPTION(SWIndexOutOfBoundsException);
			}

		return m_timelist[idx];
		}


int
SWStopWatch::compareTo(const SWStopWatch &other) const
		{
		// We use sw_timespec_t for comparision as the comparision
		// is more reliable than with floating point when using ==
		sw_timespec_t	this_tv, other_tv;
		int				r=0;
		
		elapsed(this_tv);
		other.elapsed(other_tv);

		if (this_tv.tv_sec == other_tv.tv_sec)
			{
			if (this_tv.tv_nsec < other_tv.tv_nsec) r = -1;
			else if (this_tv.tv_nsec > other_tv.tv_nsec) r = 1;
			}
		else if (this_tv.tv_sec < other_tv.tv_sec) r = -1;
		else if (this_tv.tv_sec > other_tv.tv_sec) r = 1;

		return r;
		}


sw_status_t
SWStopWatch::writeToStream(SWOutputStream &stream) const
		{
		SWGuard			guard(this);
		sw_status_t		res;
		sw_uint32_t		i, nelems=count();

		res = stream.write(nelems);
		for (i=0;res == SW_STATUS_SUCCESS && i<nelems;i++)
			{
			res = m_timelist[i].writeToStream(stream);
			}

		return res;
		}


sw_status_t
SWStopWatch::readFromStream(SWInputStream &stream)
		{
		SWGuard				guard(this);
		sw_status_t			res;
		sw_uint32_t			i, nelems=count();
		SWStopWatch			tmp;
		SWTime				t;

		res = stream.read(nelems);
		for (i=0;res == SW_STATUS_SUCCESS && i<nelems;i++)
			{
			res = t.readFromStream(stream);
			if (res == SW_STATUS_SUCCESS) tmp.m_timelist.add(t);
			}

		if (res == SW_STATUS_SUCCESS)
			{
			*this = tmp;
			}

		return res;
		}
	




SWIFT_NAMESPACE_END
