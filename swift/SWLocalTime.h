/**
*** @file		SWLocalTime.h
*** @brief		A class to represent a time adjusted for the current (local) timezone.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWLocalTime class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWLocalTime_h__
#define __SWLocalTime_h__

#include <swift/SWAdjustedTime.h>





/**
*** @brief	A class to represent a time in seconds and nanoseconds adjusted for the local timezone.
***
*** A SWLocalTime object represents a time in seconds and nanoseconds adjusted for the local timezone.
*** It is based on the SWAdjustedTime class to perform the time adjustments.
***
*** @see		SWAdjustedTime, SWTime
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWLocalTime : public SWAdjustedTime
		{
public:
		/// Construct the time object with the current time
		SWLocalTime();

		/// Copy constructor
		SWLocalTime(const SWLocalTime &other);

		/// SWTime constructor
		SWLocalTime(const SWTime &other);


/**
*** @name Assignment operators
**/
//@{
		/// Assignment operator
		SWLocalTime &	operator=(const SWLocalTime &other);

		/// Assignmemt from a unix timespec
		SWLocalTime &	operator=(const sw_timespec_t &tv);

		/// Assignmemt from a unix timeval
		SWLocalTime &	operator=(const sw_timeval_t &tv);

		/// Assignment from a unix time
		SWLocalTime &	operator=(time_t clock);

		/// Assignment from a double (SWTime format only)
		SWLocalTime &	operator=(double v);

		/// Assignment from a windows FILETIME
		SWLocalTime &	operator=(const FILETIME &v);

		/// Assignment from a windows SYSTEMTIME
		SWLocalTime &	operator=(const SYSTEMTIME &v);

		/// Assignment from a SWDate
		SWLocalTime &	operator=(const SWDate &v);

		/// Assignment from a SWTime
		SWLocalTime &	operator=(const SWTime &v);

		/// Assignment from a struct tm (unix)
		SWLocalTime &	operator=(const sw_tm_t &v);
//@}

		};





#endif
