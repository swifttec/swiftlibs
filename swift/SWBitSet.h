/**
*** @file		SWBitSet.h
*** @brief		A class to represent a set of boolean flags.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWBitSet class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWBitSet_h__
#define __SWBitSet_h__

#include <swift/swift.h>
#include <swift/SWString.h>


/**
*** @brief An abstract class to represent a set of boolean flags.
***
*** This is an abstract class to represent a set of boolean flags.
*** The flags can be modified through easy-to-use methods allowing
*** the manipulation of single bits with requiring the developer to
*** do the AND and OR operations normally required.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWBitSet : public SWObject
		{
protected:
		SWBitSet()				{ }

		/// Virtual destructor
		virtual ~SWBitSet()		{ }

public:
		/**
		*** Set the specified bit to the specified value (default true)
		**/
		virtual void		setBit(int n, bool v=true)=0;

		/**
		*** Get the specified bit
		**/
		virtual bool		getBit(int n) const=0;

		/**
		*** Get the number of bits
		**/
		virtual int			size() const=0;

		/**
		*** Return the fixed size flag
		**/
		virtual bool		isFixedSize() const=0;

		/**
		*** @brief Clear the entire bit set.
		***
		*** This has slightly different effects
		*** depending on whether or not the bit set has a fixed length.
		***
		*** - If the bit set is of a fixed length then all the bits are
		*** set to zero but the length is not changed.
		***
		*** - If the bit set is not of a fixed length then the length is
		*** set to 0
		**/
		virtual void		clear()=0;

		/**
		*** Set all the bits in this set to 0. The length of the set is not changed.
		**/
		virtual void		clearAllBits()=0;

		/**
		*** Set all the bits in this set to 1. The length of the set is not changed.
		**/
		virtual void		setAllBits()=0;

		/**
		*** Toggle all the bits in this set to (1->0 or 0->1). The length of the set is not changed.
		**/
		virtual void		toggleAllBits()=0;

		/**
		*** Clear the specified bit
		**/
		void				clearBit(int n)		{ setBit(n, false); }

		/**
		*** Toggle the specified bit (1->0 or 0->1)
		**/
		void				toggleBit(int n)	{ setBit(n, !getBit(n)); }

		/**
		*** Index operator
		**/
		bool				operator[](int i)	{ return getBit(i); }

		/**
		*** Return a representation of this bit set as a string
		**/
		SWString			toString();
		};

#endif
