/**
*** @file		SWFileInfo.h
*** @brief		A class to hold the information for a file entry
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWFileInfo class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWFileInfo_h__
#define __SWFileInfo_h__

#include <swift/swift.h>

#include <swift/SWObject.h>
#include <swift/SWString.h>
#include <swift/SWTime.h>
#include <swift/SWFileAttributes.h>
#include <swift/SWFileRange.h>





#if defined(WIN32) || defined(_WIN32)

#else // if defined(WIN32) || defined(_WIN32)

// Unix Code
#include <dirent.h>

#define INVALID_FILE_ATTRIBUTES				0xffffffff

#endif // if defined(WIN32) || defined(_WIN32)



/**
*** @brief A class holding information for a file.
***
*** A SWFileInfo object represents the information about a file
*** that can be gathered from the file system. The information
*** is gathered from system calls such as stat (unix) and
*** GetFileAttributes(windows).
***
*** Because Windows and Unix represent files very differently
*** effort has been put in to this class to represent the
*** information in a platform independent way, yet without
*** losing information for each platform.
***
*** Internally the SWFileInfo object uses a set of validate flags
*** to determine which information about the file is valid. Using this technique
*** the SWFileInfo object only gathers information about attributes accessed thus
*** potentially reducing the number of calls to the underlying O/S.
***
*** @see		SWFolder
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWFileInfo : public SWObject
		{
friend class SWFolder;

public:
		/// Internal flags used for information validation.
		enum ValidateType
			{
			FileNone=0x00,			///< No file information valid
			FileAttributes=0x01,	///< File attributes valid
			FileTime=0x04,			///< File time information valid
			FileSize=0x08,			///< File size information valid
			FileName=0x10,			///< File name information valid
			FileBlocks=0x20,	///< File compression information valid
			FileAll=0x3f			///< All File information valid
			};

public:
		/// Default constructor
		SWFileInfo(const SWString &filename=_T(""));

		/// Copy constructor
		SWFileInfo(const SWFileInfo &v);

		/// Assignment operator
		const SWFileInfo &	operator=(const SWFileInfo &v);

		/// Virtual destructor
		virtual ~SWFileInfo()								{ }

		/// Comparison operator (SWFileInfo)
		bool operator==(const SWFileInfo &v)				{ return (_name == v._name); }

		/// Comparison operator (SWFileInfo)
		bool operator<(const SWFileInfo &v)					{ return (_name < v._name); }

		/// Comparison operator (SWString) - compares equality with name name (no directory)
		bool operator==(const SWString &s)					{ return (_name == s); }

		/// Return the file name
		const SWString &	getName() const					{ return _name; }

		/// Return the directory name
		const SWString &	getDirectory() const			{ return _directory; }

		/// Return the directory name in Dos format (8.3)
		const SWString &	getDosDirectory()				{ validate(FileName); return _dosdir; }

		/// Return the full path name
		SWString			getFullPathname() const;

		/**
		*** @brief	Return the full path name in DOS (8.3) format.
		***
		*** This method returns the filename in dos format on WIN32 platform.
		*** but may not return a DOS format name on non-windows systems.
		***
		*** @note
		*** This method is included for completeness and support for windows systems,
		*** but it is recommended that this only be used where strictly necessary.
		**/
		SWString			getDosFullPathname() const;

		/**
		*** @brief	Return the filename in DOS (8.3) format.
		***
		*** This method returns the filename in dos format on WIN32 platform.
		*** but may not return a DOS format name on non-windows systems.
		***
		*** @note
		*** This method is included for completeness and support for windows systems,
		*** but it is recommended that this only be used where strictly necessary.
		**/
		SWString 			getDosName()					{ validate(FileName); return _dosname; }

		/// Return the file mode (see mknod(2)) as a string
		SWString			getUnixModeString();

		/// Return the Inode number for this file
		sw_inode_t			getInode()						{ validate(FileAttributes); return _inode; }

		/// Return the ID of device containing a directory entry for this file
		sw_dev_t			getDirectoryDeviceNo()			{ validate(FileAttributes); return _dirdevno; }

		/// Return the ID of device This entry is defined only for char special or block special files
		sw_dev_t			getDeviceNo()					{ validate(FileAttributes); return _devno; }

		/**
		*** @brief	Return the number of hard links to the file.
		***
		*** On most files and on file systems where file hard links are unsupported this
		*** will return a value of 1. However on unix systems and other systems where
		*** hard links are supported this will return the number of directory entries
		*** that actually refer to this file.
		**/
		sw_uint32_t			getLinkCount()					{ validate(FileAttributes); return _linkcount; }

		// User/Group IDs
		//sw_uid_t			getOwner()						{ validate(FileAttributes); return _uid; }
		//sw_gid_t			getGroup()						{ validate(FileAttributes); return _gid; }

		/**
		*** @brief	Return the size of the file.
		***
		*** This method returns the size of the file as a 64-bit number. A 64-bit number
		*** is used here, regardless of whether or not the underlying O/S supports this
		*** to allow for future changes.
		***
		*** If file systems emerge that use enable even larger file size the sw_filesize_t will 
		*** increased to cope with these, perhaps to 128 bits or more!
		**/
		sw_filesize_t		getSize()						{ validate(FileSize); return _size; }
		
		/**
		*** @brief	Return the size on disk of the file.
		***
		*** This method attempts to return the disk space as actually used by the file
		*** as opposed to the actual size of the file. This can result in values either 
		*** less than, equal to or greater than the actual file size as returned by the
		*** getSize method.
		***
		*** If the value is less than the actual size the file is probabbly compressed.
		**/
		sw_filesize_t		getSizeOnDisk()					{ validate(FileBlocks); return _sizeOnDisk; }

/**
*** @name	File time information
***
*** The various file time information can be collected via these methods.
*** Information that is not natively available on a platform is emulated.
**/
///@{
		SWTime				getLastAccessTime()				{ validate(FileTime); return _lastAccessTime; }
		SWTime				getLastModificationTime()		{ validate(FileTime); return _lastModificationTime; }
		SWTime				getLastWriteTime()				{ validate(FileTime); return _lastModificationTime; }
		SWTime				getCreateTime()					{ validate(FileTime); return _createTime; }
		SWTime				getLastStatusChangeTime()		{ validate(FileTime); return _lastStatusChangeTime; }
///@}

		/// Return the preferred I/O block size
		sw_uint32_t			getPreferredIoBlockSize()		{ validate(FileBlocks); return _preferredIoBlockSize; }
		
		/// Return the Number of 512 byte blocks allocated
		sw_filesize_t		getAllocatedBlockCount()		{ validate(FileBlocks); return _blocks; }

		/// Return the file attributes as a SWFileAttributes object.
		SWFileAttributes	getAttributes()					{ validate(FileAttributes); return _attributes; }

		/// Return the file attributes as a string.
		SWString			getAttributesString();

/**
*** @name	File DOS/Windows Attribute infomation
***
*** The following methods test the file information to see if the
*** specified file attribute is set. Most of the tests are self-explanatory
*** from the method name so no further explanation is given.
***
*** On non-windows systems information about these attributes is emulated
*** based on other file information gathered from the O/S.
**/
///@{
		bool						isValid();
		bool						hasAttributes(sw_fileattr_t wanted, bool wantAny=true);
		bool						isHidden()			{ return (hasAttributes(SWFileAttributes::FileAttrHidden)); }
		bool						isSystem()			{ return (hasAttributes(SWFileAttributes::FileAttrSystem)); }
		bool						isReadOnly()		{ return (hasAttributes(SWFileAttributes::FileAttrReadOnly)); }
		bool						isArchive()			{ return (hasAttributes(SWFileAttributes::FileAttrArchive)); }
		bool						isNormal()			{ return (hasAttributes((sw_fileattr_t)SWFileAttributes::FileAttrNormal)); }
		bool						isTemporary()		{ return (hasAttributes(SWFileAttributes::FileAttrTemporary)); }
		bool						isSparse()			{ return (hasAttributes(SWFileAttributes::FileAttrSparse)); }
		bool						isCompressed()		{ return (hasAttributes(SWFileAttributes::FileAttrCompressed)); }
		bool						isOffline()			{ return (hasAttributes(SWFileAttributes::FileAttrOffline)); }
		bool						isNotIndexed()		{ return (hasAttributes(SWFileAttributes::FileAttrNotIndexed)); }
		bool						isEncrypted()		{ return (hasAttributes(SWFileAttributes::FileAttrEncrypted)); }
		bool						isReparsePoint()	{ return (hasAttributes(SWFileAttributes::FileAttrReparsePoint)); }
		bool						isSymbolicLink()	{ return (hasAttributes(SWFileAttributes::FileAttrSymbolicLink)); }
///@}


/**
*** @name	File Unix Attribute infomation
***
*** The following methods test the file information to see if the
*** specified file attribute is set. Most of the tests are self-explanatory
*** from the method name so no further explanation is given.
***
*** On non-unix systems information about these attributes is emulated
*** based on other file information gathered from the O/S.
***
*** Unix attributes include basic file access permissions in 3 groups
***
*** - File owner access
*** - File group access
*** - File world access (everyone else)
***
*** Each group then has 3 access bits for
***
*** - Read access
*** - Write access
*** - Execute access
***
*** Hence the 3x3 sets of isXxxYyy methods
**/
///@{
		bool						isSticky()			{ return (hasAttributes(SWFileAttributes::FileAttrSticky)); }
		bool						isSetUserID()		{ return (hasAttributes(SWFileAttributes::FileAttrSetUserID)); }
		bool						isSetGroupID()		{ return (hasAttributes(SWFileAttributes::FileAttrSetGroupID)); }
		bool						isOwnerRead()		{ return (hasAttributes(SWFileAttributes::FileAttrOwnerRead)); }
		bool						isOwnerWrite()		{ return (hasAttributes(SWFileAttributes::FileAttrOwnerWrite)); }
		bool						isOwnerExecute()	{ return (hasAttributes(SWFileAttributes::FileAttrOwnerExecute)); }
		bool						isGroupRead()		{ return (hasAttributes(SWFileAttributes::FileAttrGroupRead)); }
		bool						isGroupWrite()		{ return (hasAttributes(SWFileAttributes::FileAttrGroupWrite)); }
		bool						isGroupExecute()	{ return (hasAttributes(SWFileAttributes::FileAttrGroupExecute)); }
		bool						isWorldRead()		{ return (hasAttributes(SWFileAttributes::FileAttrWorldRead)); }
		bool						isWorldWrite()		{ return (hasAttributes(SWFileAttributes::FileAttrWorldWrite)); }
		bool						isWorldExecute()	{ return (hasAttributes(SWFileAttributes::FileAttrWorldExecute)); }
///@}


/// @name File Type Information
///@{

		/// @brief	Returns the type of the file.
		SWFileAttributes::FileType	getType()			{ validate(FileAttributes); return _attributes.type(); }

		/// @brief	Returns true if the file is a device.
		bool						isDevice()			{ validate(FileAttributes); return _attributes.isDevice(); }

		/// @brief	Returns true if the file is a FIFO.
		bool						isFifo()			{ validate(FileAttributes); return _attributes.isFifo(); }

		/// @brief	Returns true if the file is a pipe.
		bool						isPipe()			{ validate(FileAttributes); return _attributes.isPipe(); }

		/// @brief	Returns true if the file is a character (raw) device.
		bool						isCharDevice()		{ validate(FileAttributes); return _attributes.isCharDevice(); }

		/// @brief	Returns true if the file is a block device.
		bool						isBlockDevice()		{ validate(FileAttributes); return _attributes.isBlockDevice(); }

		/// @brief	Returns true if the file is a socket.
		bool						isSocket()			{ validate(FileAttributes); return _attributes.isSocket(); }

		/// @brief	Returns true if the file is a folder or directory.
		bool						isDirectory()		{ validate(FileAttributes); return _attributes.isDirectory(); }

		/// @brief	Returns true if the file is a folder or directory.
		bool						isFolder()			{ validate(FileAttributes); return _attributes.isDirectory(); }

		/// @brief	Returns true if the file is a regular file.
		bool						isFile()			{ validate(FileAttributes); return _attributes.isFile(); }

		/// @brief	Returns true if the file is a door.
		bool						isDoor()			{ validate(FileAttributes); return _attributes.isDoor(); }

		/// @brief	Returns true if the file is a Xenix named file.
		bool						isXenixNamedFile()	{ validate(FileAttributes); return _attributes.isXenixNamedFile(); }

		/// @brief	Returns true if the file is a network special device.
		bool						isNetworkSpecial()	{ validate(FileAttributes); return _attributes.isNetworkSpecial(); }
///@}


		/// @brief Refreshes the file info.
		virtual void		refresh();

		/// @brief	Return the value of a symbolic link.
		SWString			getLinkValue();

		virtual sw_status_t	getInfoForFile(const SWString &filename);


public:	// STATIC methods
		/// Returns true if the specified filename is a file.
		static bool				isFile(const SWString &filename);

		/// Returns true if the specified filename is a folder/directory.
		static bool				isDirectory(const SWString &filename);

		/// Returns true if the specified filename is a folder/directory.
		static bool				isFolder(const SWString &filename);

		/// Returns true if the specified filename is a symbolic link.
		static bool				isSymbolicLink(const SWString &filename);

		/// Returns true if the specified filename is readable.
		static bool				isReadable(const SWString &filename);

		/// Returns true if the specified filename is writeable.
		static bool				isWriteable(const SWString &filename);

		/// Returns true if the specified filename is executeable.
		static bool				isExecutable(const SWString &filename);	

		/// Returns the size of the specified file in bytes or -1 on error.
		static sw_filesize_t	size(const SWString &filename);

		/**
		*** Retrieves the various file times for the specified file.
		***
		*** This function gets the file times for the specified path.
		*** The time parameters are optional and if they are specifed
		*** as NULL then that time element is not retrieved.
		***
		*** Note that Unix does not have the concept of creation time
		*** so it is simulated using last modification time.
		***
		*** @param path				The filename to set the times on.
		*** @param lastModTime		If not null a pointer to a Time object to receive
		***							the last modification time of the file.
		*** @param createTime		If not null a pointer to a Time object to receive
		***							the creation time of the file.
		*** @param lastAccessTime	If not null a pointer to a Time object to receive
		***							the last access time of the file.
		**/
		static sw_status_t		getFileTimes(const SWString &path, SWTime *lastModTime, SWTime *createTime, SWTime *lastAccessTime);

		/**
		*** This function sets the file times for the specified path.
		*** The time parameters are optional and if they are specifed
		*** as NULL then that time element is not modified.
		***
		*** Note that Unix does not have the concept of creation time
		*** so it is ignored.
		***
		*** @brief	Retrieves the various file times from the file
		***
		*** @param path				The filename to set the times on.
		*** @param lastModTime		If not null a pointer to a Time object to containing
		***							the last modification time of the file.
		*** @param createTime		If not null a pointer to a Time object to containing
		***							the creation time of the file.
		*** @param lastAccessTime	If not null a pointer to a Time object to containing
		***							the last access time of the file.
		**/
		static sw_status_t		setFileTimes(const SWString &path, SWTime *lastModTime, SWTime *createTime, SWTime *lastAccessTime);

protected:
		virtual void		reset();
		virtual void		validate(ValidateType type);

		virtual bool		getAttributeInfo(const SWString &filename);
		virtual bool		getTimeInfo(const SWString &filename);
		virtual bool		getSizeInfo(const SWString &filename);
		virtual bool		getNameInfo(const SWString &filename);
		virtual bool		getBlockInfo(const SWString &filename);

protected:
		SWString			_name;					///< The filename (no path)
		SWString			_directory;				///< The directory in which the entry is found
		SWString			_dosname;				///< The filename (no path) in Dos (8.3) format
		SWString			_dosdir;				///< The directory in which the entry is found in Dos format
		int					_validData;				///< Flags indicating which parts of the file info have been validated.
		SWFileAttributes	_attributes;			///< The file attributes
		sw_filesize_t		_size;					///< The actual file size
		sw_filesize_t		_sizeOnDisk;			///< The size of the file on disk (e.g. useful when a compressed file)
		sw_filesize_t		_blocks;				///< Number of 512 byte blocks allocated
		sw_inode_t			_inode;					///< The inode number
		sw_dev_t			_devno;					///< The device number (valid only for devices)
		sw_dev_t			_dirdevno;				///< The device number of the directories device
		sw_uint32_t			_linkcount;				///< The number of links to this file
		SWTime				_createTime;			///< The creation time of the file.
		SWTime				_lastAccessTime;		///< The time the file was last accessed
		SWTime				_lastModificationTime;	///< The time the file was last modified
		SWTime				_lastStatusChangeTime;	///< The time the status of the file last changed
		sw_uint32_t				_preferredIoBlockSize;	///< The preferred I/O block size
		};




#endif
