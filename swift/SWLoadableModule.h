/**
*** @file		SWLoadableModule.h
*** @brief		A class for handling shared libraries
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWLoadableModule class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




#ifndef __SWLoadableModule_h__
#define __SWLoadableModule_h__

#include <swift/SWObject.h>
#include <swift/SWString.h>



// Forward declarations

/**
*** @brief	A class for handling shared libraries or DLLs.
***
*** A shared library (unix) or DLL (windows) is simply a library or executeable
*** that can be dynamically loaded, i.e. at an arbitary point during runtime. It
*** may contain code, resources or data. This class also handles programs in the
*** same way as shared libraries.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWLoadableModule : public SWObject
		{
		friend class SWResourceModule;

public:
		/// An enumeration of shared library types used for
		/// auto-name construction in open.
		enum LibType
				{
				DLL=1,				///< A shared library (libX.so or X.dll)
				SharedLibrary=1,	///< Same as DLL
				Program=2			///< A program (X or X.exe)
				};

			enum OpenMode
				{
				Normal=0,
				Lazy,
				ResolveAll
				};

public:
		/// Default constructor
		SWLoadableModule(const SWString &name="");

		/// Name and type constructor (see open)
		SWLoadableModule(const SWString &name, LibType libtype, OpenMode mode);

		/// Virtual destructor
		virtual ~SWLoadableModule();

		/// Copy constructor
		SWLoadableModule(const SWLoadableModule &other);

		/// Assignment operator
		SWLoadableModule &	operator=(const SWLoadableModule &other);

		/**
		*** Load the specified module.
		***
		*** The name specified must match exactly the filename of the module
		*** to be opened (e.g libX.so or X.dll) and should include the path
		*** if not in the current directory.
		**/
		sw_status_t	load(const SWString &name, OpenMode mode);

		/**
		*** Open the specified module in a platform independent manner.
		***
		*** The name specified should not include platform dependent prefixes
		*** or extensions. The libtype parameter gives the open method a hint
		*** on how to form the module name correctly.
		***
		*** e.g To open the shared library X the developer would call:
		***
		*** <code><pre>
		*** open("X", SWLoadableModule::SharedLibrary);
		*** </pre></code>
		***
		*** The open method would try to open the file X.dll on a windows platform
		*** and libX.so on a unix platform.
		**/
		sw_status_t	open(const SWString &name, LibType type=SharedLibrary, OpenMode mode=Lazy);

		/**
		*** Search for the specified symbol in the loaded module.
		***
		*** @param	name			The name of the symbol to search for.
		*** @param	allowNameMods	If true the method is allowed try common modifications
		***							to the name (e.g. prefix with an underscore as done by some
		***							C compilers for C functions) if the specified name is not 
		***							found directly.
		***
		*** @return	If the specified name is found in the module a pointer to it is 
		***			returned to the caller. If the symbol is not found a NULL pointer
		***			is returned.
		**/
		void *		symbol(const SWString &name, bool allowNameMods=true);

		/// Close the module
		sw_status_t	close();

		/// Test to see if this module is open.
		bool		isOpen() const;

protected: // Methods
		bool		find(const SWString &name, SWString &path);

protected: // Members
		sw_handle_t	m_hModule;	///< The handle to the module
		SWString	m_name;		///< The name of the shared library
		OpenMode	m_mode;		///< The open mode
		};



#endif
