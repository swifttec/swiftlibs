/**
*** @file		SWIPAddress.cpp
*** @brief		A class to represent an IP address.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWIPAddress class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


// Needed for precompiled header support
#include "stdafx.h"

#include <swift/SWIPAddress.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif



#if !defined(SW_PLATFORM_WINDOWS)
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>
#else
	#if defined(SW_BUILD_MFC)
		#include <winsock2.h>
	#endif
		
	#include <Ws2tcpip.h>
#endif


/**
*** Private initialiser
**/
void
SWIPAddress::set(IPAddressType type, int len, const void *data)
		{
		_type = type;
		_len = len;
		if (data != NULL) memcpy(_data, data, _len);
		else memset(_data, 0, _len);
		}


// Protected constructor for friends
SWIPAddress::SWIPAddress(const void *pData, int len, int family)
		{
		if (family == AF_INET) set(IP4, len, pData);
		else throw SW_EXCEPTION_TEXT(SWException, "Unknown address family");
		}



SWIPAddress::SWIPAddress()
		{
		clear();
		}


void
SWIPAddress::clear()
		{
		set(Unknown, sizeof(_data), NULL);
		}


/*
** String constructor takes a string in the format x.x.x.x
** and creates an IP address from it
*/
SWIPAddress::SWIPAddress(const SWString &str)
		{
		SWString	tmp(str);
		sw_uint32_t	addr;
		
		// was addr = inet_addr(tmp);
#if defined(SW_PLATFORM_WINDOWS)
		InetPton(AF_INET, tmp, &addr);
#else
		inet_pton(AF_INET, tmp, &addr);
#endif

		set(IP4, sizeof(addr), &addr);
		}
	

// Copy constructor
SWIPAddress::SWIPAddress(const SWIPAddress &other) :
	SWObject()
    	{
		*this = other;
		}


// Assignment operator
SWIPAddress &	
SWIPAddress::operator=(const SWIPAddress &other)
		{
		_type = other._type;
		_len = other._len;
		memcpy(_data, other._data, _len);
		return *this;
		}


// Assignment operator
SWIPAddress &	
SWIPAddress::operator=(const SWString &other)
		{
		SWString	tmp(other);
		sw_uint32_t	addr;
		
		// was addr = inet_addr(tmp);
#if defined(SW_PLATFORM_WINDOWS)
		InetPton(AF_INET, tmp, &addr);
#else
		inet_pton(AF_INET, tmp, &addr);
#endif

		set(IP4, sizeof(addr), &addr);

		return *this;
		}

SWString
SWIPAddress::toString() const
		{
		SWString	s;
		
		// was s = inet_ntoa(*(struct in_addr *)_data);

#if defined(SW_PLATFORM_WINDOWS)
		TCHAR	tmp[64];

		InetNtop(AF_INET, (struct in_addr *)_data, tmp, countof(tmp));
#else
		char	tmp[64];
		
		inet_ntop(AF_INET, (struct in_addr *)_data, tmp, countof(tmp));
#endif
		
		s = tmp;
		return s;
		}


sw_uint32_t
SWIPAddress::toHostInteger() const
		{
		return htonl(*(sw_uint32_t *)_data);
		}


bool
SWIPAddress::operator==(const SWIPAddress &other)
		{
		return (_len == other._len && memcmp(_data, other._data, _len) == 0);
		}


SWIPAddress::SWIPAddress(sw_uint32_t addr)
		{
		set(IP4, sizeof(addr), &addr);
		}


/*
SWIPAddress::SWIPAddress(const struct in_addr &addr)
		{
		set(IP4, sizeof(addr), &addr);
		}
*/


SWIPAddress &	
SWIPAddress::operator=(sw_uint32_t addr)
		{
		set(IP4, sizeof(addr), &addr);
		return *this;
		}

/*
SWIPAddress &
SWIPAddress::operator=(const struct in_addr &addr)
		{
		set(IP4, sizeof(addr), &addr);
		return *this;
		}
*/

bool
SWIPAddress::isIPAddress(const SWString &s)
		{
		int		n, a, b, c, d;
		bool	ok=false;

		n = sscanf(s, "%d.%d.%d.%d", &a, &b, &c, &d);
		if (n == 4)
			{
			if (a >=0 && a <= 255 && b >=0 && b <= 255 && c >=0 && c <= 255 && d >=0 && d <= 255) ok = true;
			}

		return ok;
		}


int
SWIPAddress::family() const
		{
		int	f=0;

		if (_type == IP4) f = AF_INET;

		return f;
		}



