/**
*** @file		SWAdjustedTime.h
*** @brief		A class to implement a time with a timezone adjustment
*** @version	1.0.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWAdjustedTime class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWAdjustedTime_h__
#define __SWAdjustedTime_h__


#include <swift/SWTime.h>
#include <swift/SWTimeZone.h>






/**
*** @brief A class to represent an adjusted time (as an offset from GMT).
***
***	The SWTime class is a portable representation of time which 
*** makes no adjustments for timezone and works in UTC time. This class
*** extends SWTime by adding an adjustment as specified by the timezone
*** or locale information supplied.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWAdjustedTime : public SWTime
		{
public:
		/// Default constuctor has no timezone info.
		SWAdjustedTime();

		/**
		*** @brief	Construct an adjusted time using the specified timezone.
		***
		*** Construct the time object with the current time and the given time adjustment in seconds
		*** for timezone and DST as specified by the timezone.
		***
		*** @param[in]	tz		The time zone object containing the adjustments.
		**/
		SWAdjustedTime(const SWTimeZone &tz);

		/**
		*** @brief	Construct an adjusted time using the specified time and timezone.
		***
		*** Construct the time object with the specified time and the given time adjustment in seconds
		*** for timezone and DST as specified by the timezone.
		***
		*** @param[in]	other	The time object containing the time to copy.
		*** @param[in]	tz		The time zone object containing the adjustments.
		**/
		SWAdjustedTime(const SWTime &other, const SWTimeZone &tz);

		
		/// Copy constructor
		SWAdjustedTime(const SWAdjustedTime &other);

/**
*** @name Assignment operators
**/
//@{
		/// Assignment operator
		SWAdjustedTime &	operator=(const SWAdjustedTime &other);

		/// Assignmemt from a unix timespec
		SWAdjustedTime &	operator=(const sw_timespec_t &tv);

		/// Assignmemt from a unix timeval
		SWAdjustedTime &	operator=(const sw_timeval_t &tv);

		/// Assignment from a unix time
		SWAdjustedTime &	operator=(time_t clock);

		/// Assignment from a double (SWTime format only)
		SWAdjustedTime &	operator=(double v);

		/// Assignment from a windows FILETIME
		SWAdjustedTime &	operator=(const FILETIME &v);

		/// Assignment from a windows SYSTEMTIME
		SWAdjustedTime &	operator=(const SYSTEMTIME &v);

		/// Assignment from a SWDate
		SWAdjustedTime &	operator=(const SWDate &v);

		/// Assignment from a sw_tm_t (unix)
		SWAdjustedTime &	operator=(const sw_tm_t &v);

		/// Set from a localised variant time
		void				setFromLocalVariantTime(double clock);

		virtual bool		set(const SWString &value, const SWString &format="YYYYMMDDhhmmss");
		virtual void		set(double clock, bool isVariantTime=false);
		virtual bool		set(int nYear, int nMonth, int nDay, int nHour, int nMinute, int nSeconds, int nNanoseconds);
		virtual bool		set(sw_int64_t secs, sw_uint32_t nsecs, TimeType tt);


//@}


/**
*** @name Member access
**/
//@{

		/// Return the hour (0-23)
		virtual int			hour() const;

		/// Return the minute (0-59)
		virtual int			minute() const;

		/// Return the second (0-59)
		virtual int			second() const;

		/// Return the julian day number
		virtual int			julian() const;
//@}


		/**
		*** Check to see if the time is in DST (Daylight Savings Time)
		***
		*** @return	0 if DST is not in effect, 1 if DST is in effect or
		***			-1 if DST information is unavailable
		**/
		virtual int			dst() const;

		/// Return the name of the timezone
		virtual SWString	tzName(int dst) const;


		/**
		*** Sets the internal parameters used by the class from the
		*** timezone object.
		***
		*** @param	timezone	The SWTimeZone object used for adjusting the reported time.
		**/
		void				setTimeZone(const SWTimeZone &timezone);

		/**
		*** Sets the internal parameters used by the class
		***
		*** @param	tzAdjustment	The adjustment in seconds for the timezone (from GMT)
		*** @param	dstAdjustment	The adjustment in seconds to be applied be DST is in effect
		***	@param	dstAvailable	Indicates if DST is available or not (see isDST for more)
		*** @param	stdName			The timezone name when DST is not in effect (e.g. GMT)
		*** @param	dstName			The timezone name when DST is in effect (e.g. BST)
		**/
		void				setAdjustments(int tzAdjustment, int dstAdjustment, int dstAvailable, const SWString &stdName, const SWString &dstName);

protected:
		/// Return the time value in seconds adjusted for DST and timezone
		sw_int64_t			adjustedSeconds() const;

		/// Virtual method to calculate if daylight savings is in effect for the current time value.
		virtual void		calculateDST();

protected:
		int			m_dst;				///< Indicates if the current time is in DST or not
		SWTimeZone	m_timezone;			///< The timezone information
		};





#endif
