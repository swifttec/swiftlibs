/**
*** @file		SWArrayS.cpp
*** @brief		An array template
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWArrayS class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWArrayS_cpp__
#define __SWArrayS_cpp__

#include <swift/SWGuard.h>
#include <swift/SWException.h>




template<class T>
SWArrayS<T>::SWArrayS() :
	m_pMemoryManager(NULL),
	m_pData(NULL),
	m_capacity(0),
	m_elemsize(0),
	m_nelems(0)
		{
		m_pMemoryManager = SWMemoryManager::getDefaultMemoryManager();

		// Calculate the element size
		m_elemsize = ((sizeof(SWArraySElement<T>) + sizeof(void *) - 1) / sizeof(void *)) * sizeof(void *);

		// Make sure we have space for some (8) elements
		ensureCapacity(8);
		}


template<class T>
SWArrayS<T>::~SWArrayS()
		{
		clear();
		m_pMemoryManager->free(m_pData);
		}


template<class T>
SWArrayS<T>::SWArrayS(const SWArrayS<T> &other) :
	m_pMemoryManager(NULL),
	m_pData(NULL),
	m_capacity(0),
	m_elemsize(0),
	m_nelems(0)
		{
		m_pMemoryManager = SWMemoryManager::getDefaultMemoryManager();

		// Calculate the element size
		m_elemsize = ((sizeof(SWArraySElement<T>) + sizeof(void *) - 1) / sizeof(void *)) * sizeof(void *);

		// Make sure we have space for some (8) elements
		ensureCapacity(8);

		// Copy the data via the assigment operator
		*this = other;
		}


template<class T> SWArrayS<T> &
SWArrayS<T>::operator=(const SWArrayS<T> &other)
		{
		sw_index_t	n = (sw_index_t)other.size();

		clear();

		// Do it backwards to avoid doing multiple reallocs
		while (n > 0)
			{
			--n;
			(*this)[n] = other.get(n);
			}

		return *this;
		}


template<class T> void
SWArrayS<T>::ensureCapacity(sw_size_t nelems)
		{
		SWWriteGuard	guard(this);	// Guard this object
		sw_size_t		wanted=nelems * m_elemsize;

		if (wanted > m_capacity)
			{
			if (m_capacity == 0) m_capacity = 128;

			while (m_capacity < wanted)
				{
				m_capacity <<= 1;
				}

			char	*pOldData = m_pData;

			m_pData = (char *)m_pMemoryManager->malloc(m_capacity);
			if (m_pData == NULL)
				{
				m_pData = pOldData;
				throw SW_EXCEPTION(SWMemoryException);
				}

			if (pOldData != NULL)
				{
				// We must "new" all the existing data elements
				char		*pTo=(char *)m_pData;
				char		*pFrom=(char *)pOldData;
				sw_size_t	i, n=size();

				for (i=0;i<n;i++)
					{
					new (pTo) SWArraySElement<T>(((SWArraySElement<T> *)pFrom)->data());
					delete (SWArraySElement<T> *)pFrom;
					pTo += m_elemsize;
					pFrom += m_elemsize;
					}

				m_pMemoryManager->free(pOldData);
				}
			}
		}


template<class T> T &
SWArrayS<T>::operator[](sw_index_t pos)
		{
		if (pos < 0) throw SW_EXCEPTION(SWIndexOutOfBoundsException);

		SWWriteGuard	guard(this);

		ensureCapacity((sw_size_t)pos + 1);

		while (m_nelems <= (sw_size_t)pos)
			{
			// Make sure all of the interim elements are valid
			new (m_pData + (m_nelems * m_elemsize)) SWArraySElement<T>;
			m_nelems++;
			}

		return ((SWArraySElement<T> *)(m_pData + (pos * m_elemsize)))->data();
		}


template<class T> T &
SWArrayS<T>::get(sw_index_t pos) const
		{
		if (pos < 0 || pos >= (sw_index_t)m_nelems) throw SW_EXCEPTION(SWIndexOutOfBoundsException);

		SWReadGuard	guard(this);

		return ((SWArraySElement<T> *)(m_pData + (pos * m_elemsize)))->data();
		}


template<class T> void
SWArrayS<T>::clear()
		{
		SWWriteGuard		guard(this);
		SWArraySElement<T>	*pElement;
		sw_size_t			pos=m_nelems;

		while (pos > 0)
			{
			--pos;
			pElement = ((SWArraySElement<T> *)(m_pData + (pos * m_elemsize)));
			delete pElement;
			}

		m_nelems = 0;
		}


template<class T> void
SWArrayS<T>::remove(sw_index_t pos, sw_size_t count)
		{
		if (count > 0)
			{
			SWWriteGuard	guard(this);

			if (pos >= 0 && pos < (sw_index_t)m_nelems)
				{
				if (pos + count > m_nelems) count = m_nelems - pos;

				if (count > 0)
					{
					sw_index_t	startpos=pos, endpos=pos+(sw_index_t)count;
					char		*p = m_pData + (startpos * m_elemsize);
					char		*q = m_pData + (endpos * m_elemsize);
					sw_size_t	oldn = m_nelems;

					// Reduce the number of elems and shift all the elems down
					for (pos=endpos;pos<(sw_index_t)size();pos++)
						{
						((SWArraySElement<T> *)p)->data() = ((SWArraySElement<T> *)q)->data();
						p += m_elemsize;
						q += m_elemsize;
						}

					 // Blank out the rest
					pos = (sw_index_t)(m_nelems - count);
					p = m_pData + (pos * m_elemsize);
					while (pos < (sw_index_t)oldn)
						{
						delete (SWArraySElement<T> *)p;
						p += m_elemsize;
						pos++;
						}

					m_nelems -= count;
					}
				}
			else throw SW_EXCEPTION(SWIndexOutOfBoundsException);
			}
		}



template<class T> void
SWArrayS<T>::add(const T &v)
		{
		SWWriteGuard	guard(this);

		insert(v, (sw_index_t)m_nelems);
		}


template<class T> void
SWArrayS<T>::insert(const T &v, sw_index_t pos)
		{
		SWWriteGuard	guard(this);

		if (pos >= 0 && pos <= (sw_index_t)m_nelems)
			{
			ensureCapacity(m_nelems+1);

			sw_index_t	ipos = (sw_index_t)m_nelems;
			char		*p = m_pData + (ipos * m_elemsize);
			char		*q = p - m_elemsize;
			bool		exists=false;

			// Increase the number of elems by one and shift all the elems up by 1
			// not forgetting to make sure we have enough space first!
			if (ipos > pos)
				{
				new (p) SWArraySElement<T>((((SWArraySElement<T> *)q)->data()));
				p -= m_elemsize;
				q -= m_elemsize;
				ipos--;
				exists = true;
				}
			
			while (ipos > pos)
				{
				(((SWArraySElement<T> *)p)->data()) = (((SWArraySElement<T> *)q)->data());
				p -= m_elemsize;
				q -= m_elemsize;
				ipos--;
				}
				
			if (exists) ((SWArraySElement<T> *)p)->data() = v;
			else new (p) SWArraySElement<T>(v);

			m_nelems++;
			}
		else throw SW_EXCEPTION(SWIndexOutOfBoundsException);
		}


template<class T> void
SWArrayS<T>::push(const T &v)
		{
		add(v);
		}


template<class T> sw_size_t
SWArrayS<T>::size() const
		{
		return m_nelems;
		}


template<class T> T
SWArrayS<T>::pop()
		{
		if (m_nelems < 1) throw SW_EXCEPTION(SWIndexOutOfBoundsException);

		SWWriteGuard	guard(this);

		T	v=get((sw_index_t)m_nelems-1);

		remove((sw_index_t)m_nelems-1, 1);

		return v;
		}


template<class T> sw_status_t
SWArrayS<T>::writeToStream(SWOutputStream & stream) const
		{
		SWReadGuard	guard(this);
		sw_status_t	res;

		res = stream.write((sw_uint32_t)size());
		for (int i=0;res==SW_STATUS_SUCCESS&&i<(int)size();i++)
			{
			res = stream.write(get(i));
			}

		return res;
		}


template<class T> sw_status_t
SWArrayS<T>::readFromStream(SWInputStream & stream)
		{
		SWWriteGuard	guard(this);
		sw_status_t	res;
		sw_uint32_t	i, nelems;

		clear();
		res = stream.read(nelems);
		for (i=0;res==SW_STATUS_SUCCESS&&i<nelems;i++)
			{
			if (res == SW_STATUS_SUCCESS) res = stream.read((*this)[i]);
			}

		return res;
		}



#endif
