/**
*** @file		SWInt32.h
*** @brief		A class to represent a date
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWInt32 class.
***
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWInt32_h__
#define __SWInt32_h__

#include <swift/swift.h>
#include <swift/SWObject.h>
#include <swift/SWException.h>
#include <swift/SWString.h>




// Forward declarations
class SWTime;

/**
*** @brief	A generic date class based on Julian Day Numbers.
***
***	This is a generic date class that represents dates as Julian Day Numbers.
*** This gives a enormous range of dates that are stored in a 32-bit integer value.
***
*** The class stores date information only and has no time component. However it
*** can easily be converted both to and from a SWTime value. Where both date and
*** time information are required developers should use the SWTime class.
***
*** @author		Simon Sparkes
*/
class SWIFT_DLL_EXPORT SWInt32
		{
public:
		SWInt32(sw_int32_t amount=0) : m_amount(amount)		{ }
		SWInt32(const char *s);
		SWInt32(const wchar_t *s);
		SWInt32(const SWString &s);

		SWInt32 &	operator=(const char *s);
		SWInt32 &	operator=(const wchar_t *s);
		SWInt32 &	operator=(const SWString &s);
		SWInt32 &	operator=(int amount)						{ m_amount = (sw_int32_t)amount; return *this; }

		operator sw_int32_t() const								{ return m_amount; }
		operator sw_int32_t &()									{ return m_amount; }

		SWString		toString(const SWString &fmt="%d") const;

private:
		sw_int32_t	m_amount;
		};





#endif
