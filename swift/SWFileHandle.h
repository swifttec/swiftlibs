/**
*** @file		SWFileHandle.h
*** @brief		A class to represent a file handle
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWFileHandle class.
**/

/*********************************************************************************
Copyright (c) 2006-2016 SwiftTec Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




#ifndef __SWFileHandle_h__
#define __SWFileHandle_h__

#include <swift/swift.h>


// Begin the namespace
SWIFT_BEGIN_NAMESPACE

/**
*** @brief	Represent a file handle
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWFileHandle
		{
public:
#if defined(SW_PLATFORM_WINDOWS)
		SWFileHandle(HANDLE hf=INVALID_HANDLE_VALUE) : m_handle(hf)	{ }
		operator HANDLE &() const									{ return m_handle; }
#else
		SWFileHandle(int hf=-1) : m_handle(hf)						{ }
		operator int &() const										{ return m_handle; }
#endif


protected: // Members

#if defined(SW_PLATFORM_WINDOWS)
		HANDLE	m_handle;
#else
		int		m_handle;
#endif
		};


// End the namespace
SWIFT_END_NAMESPACE

#endif
