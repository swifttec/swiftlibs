/****************************************************************************
**	SWNetworkAdapter.cpp	A class to represent a network adapter
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


// Needed for precompiled header support
#include "stdafx.h"

//#include <base/types.h>
#include <xplatform/sw_types.h>
#include <swift/sw_file.h>
#include <swift/sw_net.h>
#include <swift/SWOperatingSystemInfo.h>
#include <swift/sw_printf.h>
#include <swift/sw_misc.h>

#include <swift/sw_registry.h>
#include <swift/SWMultiString.h>

#include <swift/SWNetworkAdapter.h>
#include <swift/SWSocketAddress.h>

#if defined(SW_PLATFORM_WINDOWS)
	#include <windows.h>
	#include <windns.h>

	#include <Iphlpapi.h>
	#include <Iptypes.h>
	#include <IPIfCons.h>
	#include <lm.h>
	#include <NtSecApi.h>

	#pragma comment(lib, "netapi32.lib")
#else
	#include <sys/types.h>
	#include <sys/socket.h>
	#if defined(SW_PLATFORM_MACH)
		#include <net/if.h>
	#else
		#include <linux/if.h>
	#endif
	#include <ifaddrs.h>
#endif

#include <stdio.h>


SWNetworkAdapter::SWNetworkAdapter() :
    m_index(0),
    m_type(Other),
    m_currAddr(-1),
    m_dhcp(false),
    m_wins(false),
    m_dnsRegisterAdapterHostName(false),
    m_dnsRegisterHostName(false)
		{
		}


SWNetworkAdapter::SWNetworkAdapter(const SWNetworkAdapter &v) :
    m_index(0),
    m_type(Other),
    m_currAddr(-1),
    m_dhcp(false),
    m_wins(false),
    m_dnsRegisterAdapterHostName(false),
    m_dnsRegisterHostName(false)
		{
		*this = v;
		}


SWNetworkAdapter &
SWNetworkAdapter::operator=(const SWNetworkAdapter &v)
		{
#define COPY(x)	x = v.x
		COPY(m_name);
		COPY(m_desc);
		COPY(m_displayName);
		COPY(m_address);
		COPY(m_index);
		COPY(m_type);
		COPY(m_currAddr);
		COPY(m_gateway);

		COPY(m_ipaddrs);
		COPY(m_ipmasks);

		// DNS
		COPY(m_dnsServers);
		COPY(m_dnsRegisterAdapterHostName);
		COPY(m_dnsRegisterHostName);
		COPY(m_dnsAdapterDomainName);
		COPY(m_dnsPrimaryDomainName);

		// DHCP
		COPY(m_dhcp);
		COPY(m_dhcpServer);
		COPY(m_dhcpLeaseObtained);
		COPY(m_dhcpLeaseExpires);

		// WINS
		COPY(m_wins);
		COPY(m_winsPrimaryServer);
		COPY(m_winsSecondaryServer);

#undef COPY

		return *this;
		}

void
SWNetworkAdapter::clear()
		{
#define CLEAR(x)	sw_misc_clear(x)
		CLEAR(m_name);
		CLEAR(m_desc);
		CLEAR(m_displayName);
		CLEAR(m_address);
		CLEAR(m_index);
		CLEAR(m_type);
		CLEAR(m_currAddr);
		CLEAR(m_gateway);

		CLEAR(m_ipaddrs);
		CLEAR(m_ipmasks);

		// DNS
		CLEAR(m_dnsServers);
		CLEAR(m_dnsRegisterAdapterHostName);
		CLEAR(m_dnsRegisterHostName);
		CLEAR(m_dnsAdapterDomainName);
		CLEAR(m_dnsPrimaryDomainName);

		// DHCP
		CLEAR(m_dhcp);
		CLEAR(m_dhcpServer);
		CLEAR(m_dhcpLeaseObtained);
		CLEAR(m_dhcpLeaseExpires);

		// WINS
		CLEAR(m_wins);
		CLEAR(m_winsPrimaryServer);
		CLEAR(m_winsSecondaryServer);

#undef CLEAR
		}


#if defined(WIN32) || defined(_WIN32)
SWNetworkAdapter::SWNetworkAdapter(const IP_ADAPTER_INFO &adapter) :
    m_index(0),
    m_type(Other),
    m_currAddr(-1),
    m_dhcp(false),
    m_wins(false),
    m_dnsRegisterAdapterHostName(false),
    m_dnsRegisterHostName(false)
		{
		*this = adapter;
		}


/**
*** Get the DNS config info from the DNSAPI DLL
**/
typedef DNS_STATUS (WINAPI *FuncDnsQueryConfig)
    (
    IN      DNS_CONFIG_TYPE     Config,
    IN      DWORD               Flag,
    IN      PWSTR               pwsAdapterName,
    IN      PVOID               pReserved,
    OUT     PVOID               pBuffer,
    IN OUT  PDWORD              pBufferLength
    );

static HMODULE			dnsDll=0;
static FuncDnsQueryConfig	pDnsQueryConfig=0;

void
SWNetworkAdapter::getDnsInfo()
		{
		m_dnsPrimaryDomainName = sw_registry_getString(SW_REGISTRY_LOCAL_MACHINE, _T("SYSTEM\\CurrentControlSet\\Services\\Tcpip\\Parameters"), _T("Domain"));

		// Default values
		m_dnsRegisterAdapterHostName = m_dnsRegisterHostName = false;
		m_dnsAdapterDomainName.empty();

		// Get the DNS info from the registry
		SWString		key;

		key = _T("SYSTEM\\CurrentControlSet\\Services\\Tcpip\\Parameters\\DNSRegisteredAdapters\\");
		key += m_name;

		SWRegistryKey	rk(SW_REGISTRY_LOCAL_MACHINE, key, false);

		m_dnsRegisterHostName = rk.isOpen();

		key = _T("SYSTEM\\CurrentControlSet\\Services\\Tcpip\\Parameters\\Interfaces\\");
		key += m_name;

		rk = SWRegistryKey(SW_REGISTRY_LOCAL_MACHINE, key, false);

		if (rk.isOpen())
			{
			SWRegistryValue	v;

			if (rk.getValue(_T("RegistrationEnabled"), v) == SW_STATUS_SUCCESS) m_dnsRegisterHostName = v.toBoolean();
			if (rk.getValue(_T("Domain"), v) == SW_STATUS_SUCCESS) m_dnsAdapterDomainName = v.toString();

			if (rk.getValue(_T("RegisterAdapterName"), v) == SW_STATUS_SUCCESS) m_dnsRegisterAdapterHostName = v.toBoolean();
			else if (rk.getValue(_T("EnableAdapterDomainNameRegistration"), v) == SW_STATUS_SUCCESS) m_dnsRegisterAdapterHostName = v.toBoolean();
			}
		}


/**
*** Assignment operator - from a win32 IP_ADAPTER_INFO struct
**/
SWNetworkAdapter &
SWNetworkAdapter::operator=(const IP_ADAPTER_INFO &adapter)
		{
		SWString	key, value;
		int		i;

		m_address.clear();
		m_ipaddrs.clear();
		m_ipmasks.clear();
		m_dnsServers.clear();

		m_name = adapter.AdapterName;
		m_desc = adapter.Description;

		// given the adapter name (a GUID on NT5) return the friendly name
		key = _T("SYSTEM\\CurrentControlSet\\Control\\Network\\{4D36E972-E325-11CE-BFC1-08002BE10318}\\");

		key += m_name;
		key += _T("\\Connection");
		m_displayName = sw_registry_getString(SW_REGISTRY_LOCAL_MACHINE, key, _T("Name"), _T("unknown"));

		m_index = adapter.Index;

		switch (adapter.Type)
			{
			case MIB_IF_TYPE_ETHERNET:	m_type = Ethernet;	break;
			case MIB_IF_TYPE_TOKENRING:	m_type = TokenRing;	break;
			case MIB_IF_TYPE_FDDI:		m_type = FDDI;		break;
			case MIB_IF_TYPE_PPP:		m_type = PPP;		break;
			case MIB_IF_TYPE_LOOPBACK:	m_type = Loopback;	break;
			case MIB_IF_TYPE_SLIP:		m_type = Slip;		break;
			case MIB_IF_TYPE_OTHER:		m_type = Other;		break;
			default:					m_type = Other;		break;
			}

		m_address.write(&adapter.Address, adapter.AddressLength, 0);
		
		m_currAddr = -1;
		m_ipaddrs.clear();

		const IP_ADDR_STRING	*ip;
		bool			found=false;

		for (i=0,ip=&adapter.IpAddressList;ip!=NULL;i++)
			{
			if (sw_net_inet_addr(ip->IpAddress.String) != 0)
				{
				m_ipaddrs.add(SWIPAddress(ip->IpAddress.String));
				m_ipmasks.add(SWIPAddress(ip->IpMask.String));

				if (adapter.CurrentIpAddress != NULL && strcmp(ip->IpAddress.String, adapter.CurrentIpAddress->IpAddress.String) == 0)
					{
					m_currAddr = i;
					found = true;
					}
				}

			ip = ip->Next;
			}

		if (!found && m_ipaddrs.size() > 0) m_currAddr = 0;

		m_gateway = adapter.GatewayList.IpAddress.String;	// The default gateway for this adapter
		if ((in_addr_t)m_gateway == (in_addr_t)0xffffffff) m_gateway = (in_addr_t)0;

		// Init DHCP info
		m_dhcp = (adapter.DhcpEnabled != 0);
		m_dhcpServer = adapter.DhcpServer.IpAddress.String;
		m_dhcpLeaseObtained = adapter.LeaseObtained;
		m_dhcpLeaseExpires = adapter.LeaseExpires;

		// Init WINS info
		m_wins = (adapter.HaveWins != 0);
		m_winsPrimaryServer = adapter.PrimaryWinsServer.IpAddress.String;
		m_winsSecondaryServer = adapter.SecondaryWinsServer.IpAddress.String;

		// Get DNS and other registry based info
		key = _T("SYSTEM\\CurrentControlSet\\Services\\Tcpip\\Parameters\\Interfaces\\");
		key += m_name;

		SWRegistryKey	rk(SW_REGISTRY_LOCAL_MACHINE, key);

		if (rk.valueExists(_T("NameServer")))
			{
			SWStringArray	sa;
			SWRegistryValue	rv;

			if (rk.getValue("NameServer", rv) == SW_STATUS_SUCCESS)
				{
				sa.convertFromString(rv.toString());

				for (i=0;i<(int)sa.size();i++) m_dnsServers.add(SWIPAddress(sa[i]));
				}
			}

		getDnsInfo();

		return *this;
		}


/**
*** Registry constructor for NT4 support
***
*** @param pname	The product name
*** @param sname	The service name
*** @param dname	The display name
**/
SWNetworkAdapter::SWNetworkAdapter(SWRegistryKey &nk)
		{
		m_address.clear();
		m_ipaddrs.clear();
		m_ipmasks.clear();
		m_dnsServers.clear();

		m_name = nk.getValue(_T("ServiceName")).toString();
		m_displayName = nk.getValue(_T("Title")).toString();
		m_desc = nk.getValue(_T("Description")).toString();
		m_index = t_atoi(sw_file_basename(nk.name()));

	#define REGISTRY_SERVICES_PREFIX		L"SYSTEM\\CurrentControlSet\\Services\\" 
	#define REGISTRY_CONTROL_PREFIX			L"SYSTEM\\CurrentControlSet\\Control" 
	#define REGISTRY_ADAPTER_TCPIP_PARMS_POSTFIX    L"\\Parameters\\Tcpip" 

		SWString	key;

		key = _T("SYSTEM\\CurrentControlSet\\Services\\");
		key += m_name;
		key += _T("\\Parameters");

		SWRegistryKey	rk(SW_REGISTRY_LOCAL_MACHINE, key);
		SWRegistryKey	rk2;

		rk.openKey("Tcpip", rk2);

		// need to find out the card type mappings
		switch (rk.getValue(_T("CardType")).toInteger())
			{
			case MIB_IF_TYPE_ETHERNET:	m_type = Ethernet;	break;
			case MIB_IF_TYPE_TOKENRING:	m_type = TokenRing;	break;
			case MIB_IF_TYPE_FDDI:		m_type = FDDI;		break;
			case MIB_IF_TYPE_PPP:		m_type = PPP;		break;
			case MIB_IF_TYPE_LOOPBACK:	m_type = Loopback;	break;
			case MIB_IF_TYPE_SLIP:		m_type = Slip;		break;
			case MIB_IF_TYPE_OTHER:		m_type = Other;		break;
			default:					m_type = Other;		break;
			}

		// Don't have address info yet!
		// _address.write(&adapter.Address, adapter.AddressLength, 0);
		
		m_currAddr = -1;

		SWMultiString	ms;
		SWStringArray	addrs, masks;

		ms = SWMultiString((TCHAR *)rk2.getValue(_T("IPAddress")).data());
		ms.toStringArray(addrs);

		ms = SWMultiString((TCHAR *)rk2.getValue(_T("SubnetMask")).data());
		ms.toStringArray(masks);

		int			i;

		for (i=0;i<(int)addrs.size();i++)
			{
			if (sw_net_inet_addr((const char *)(addrs[i])) != 0)
				{
				m_ipaddrs.add(SWIPAddress(addrs[i]));
				m_ipmasks.add(SWIPAddress(masks[i]));
				}
			}

		if (m_ipaddrs.size() > 0) m_currAddr = 0;

		// The default gateway for this adapter
		addrs.clear();
		ms = SWMultiString((TCHAR *)rk2.getValue(_T("DefaultGateway")).data());
		ms.toStringArray(addrs);

		m_gateway = addrs[0];
		if ((in_addr_t)m_gateway == (in_addr_t)0xffffffff) m_gateway = (in_addr_t)0;

		// Init DHCP info
		SWTime	t;

		m_dhcp = rk2.getValue(_T("EnableDHCP")).toBoolean();
		m_dhcpServer = _T("");
		m_dhcpLeaseObtained = t;
		m_dhcpLeaseExpires = t;

		// Init WINS info
		m_wins = false;
		m_winsPrimaryServer = _T("");
		m_winsSecondaryServer = _T("");

		// Get DNS and other registry based info
		key = _T("SYSTEM\\CurrentControlSet\\Services\\Tcpip\\Parameters\\Interfaces\\");
		key += m_name;

		rk = SWRegistryKey(SW_REGISTRY_LOCAL_MACHINE, key);

		if (rk.valueExists(_T("NameServer")))
			{
			SWStringArray	sa;

			sa.convertFromString(rk.getValue(_T("NameServer")).toString());

			for (i=0;i<(int)sa.size();i++) m_dnsServers.add(SWIPAddress(sa[i]));
			}

		getDnsInfo();
		}


#endif


/**
*** Returns the given network adpater type as a string.
**/
SWString
SWNetworkAdapter::typeAsString(int t)
		{
		SWString	res("Unknown");

		switch (t)
			{
			case Ethernet:	res = "Ethernet";	break;
			case TokenRing:	res = "TokenRing";	break;
			case FDDI:		res = "FDDI";		break;
			case PPP:		res = "PPP";		break;
			case Loopback:	res = "Loopback";	break;
			case Slip:		res = "Slip";		break;
			case Other:		res = "Other";		break;
			default:		res = "Unknown";	break;
			}

		return res;
		}


SWString
SWNetworkAdapter::addressAsString()
		{
		SWString	res;
		char		tmp[8];
		const char	*bp = m_address;
		int		i, n=(int)m_address.size();

		for (i=0;i<n;i++,bp++)
			{
			sw_snprintf(tmp, sizeof(tmp), "%s%02x", i==0?"":":", *bp & 0xff);
			res += tmp;
			}

		return res;
		}


SWIPAddress		
SWNetworkAdapter::currentIPAddress()
		{
		SWIPAddress	res;

		if (m_currAddr >= 0) res = m_ipaddrs[m_currAddr];

		return res;
		}


SWIPAddress		
SWNetworkAdapter::currentIPAddressMask()
		{
		SWIPAddress	res;

		if (m_currAddr >= 0) res = m_ipmasks[m_currAddr];

		return res;
		}



SWIPAddress		
SWNetworkAdapter::ipAddress(int n)
		{
		SWIPAddress	res;

		if (n >= 0 && n < (int)m_ipaddrs.size()) res = m_ipaddrs[n];

		return res;
		}


SWIPAddress		
SWNetworkAdapter::ipAddressMask(int n)
		{
		SWIPAddress	res;

		if (n >= 0 && n < (int)m_ipmasks.size()) res = m_ipmasks[n];

		return res;
		}


SWIPAddress
SWNetworkAdapter::dnsServer(int n)
		{
		SWIPAddress	res;

		if (n >= 0 && n < (int)m_dnsServers.size()) res = m_dnsServers[n];

		return res;
		}


bool
SWNetworkAdapter::getLocalAdapters(SWArray<SWNetworkAdapter> &aa)
		{
		aa.clear();

		bool	result = false;

		aa.clear();

#if defined(WIN32) || defined(_WIN32)
		if (!sw_net_initialised()) sw_net_init();

		SWOperatingSystemInfo	vi;
		
		vi.getMachineInfo(vi);

		if (vi.type() == SWOperatingSystemInfo::OS_TYPE_WINNT4 || vi.type() == SWOperatingSystemInfo::OS_TYPE_WINNT351)
			{
			// GetAdaptersInfo does not work on this platform, try the registry
			SWRegistryKey		rk(SW_REGISTRY_LOCAL_MACHINE, _T("Software\\Microsoft\\Windows NT\\CurrentVersion\\NetworkCards"));
			SWArray<SWString>	sa;
			int					k;

			rk.keyNames(sa);
			for (k=0;k<(int)sa.size();k++)
				{
				SWRegistryKey	rk2;

				rk.openKey(sa[k], rk2);
				aa.add(SWNetworkAdapter(rk2));
				}

			result = true;
			}
		else
			{
			ULONG	bufsize=0;
			DWORD	dwStatus=0;

			// Get how much memory is needed
			dwStatus = GetAdaptersInfo(NULL, &bufsize);			// get buffer size required
			if (dwStatus != NO_ERROR && bufsize > 0)
				{
				SWBuffer	buf(bufsize);

				// allocate the required memory
				IP_ADAPTER_INFO *pIpArray = (IP_ADAPTER_INFO *)(void *)buf;

				// get the raw adapters info (not unicode)
				dwStatus = GetAdaptersInfo(pIpArray, &bufsize);
				if (dwStatus == NO_ERROR)
					{
					// find out how many structures there are in the array
					const IP_ADAPTER_INFO	*ip = pIpArray;

					while (ip != NULL)
						{
						aa.add(SWNetworkAdapter(*ip));
						ip = ip->Next;
						}
					}

				result = true;
				}
			}
#else
		struct ifaddrs	*addrlist, *paddr;
		int				index=0;

		getifaddrs(&addrlist);
		paddr = addrlist;

		while (paddr)
			{
			if (paddr->ifa_addr != NULL && (paddr->ifa_flags & IFF_LOOPBACK) == 0)
				{
				// printf("Adapter %d = %s (%d)\n", index, paddr->ifa_name, paddr->ifa_addr->sa_family);

				SWNetworkAdapter	*pAdapter=NULL;

				// Linux holds several entries for the same interface
				// First of all look for the adapter by name
				for (int i=0;i<(int)aa.size();i++)
					{
					if (aa[i].m_name == paddr->ifa_name)
						{
						pAdapter = &aa[i];
						}
					}

				if (pAdapter == NULL)
					{
					pAdapter = &aa[aa.size()];
					}

				SWSocketAddress	saddr;

				pAdapter->m_name = paddr->ifa_name;				// the adpater name
				// pAdapter->m_desc;				// the adapter description
				pAdapter->m_displayName = paddr->ifa_name;			// the adapter display name
				// pAdapter->m_address;			// the adapter hardware address
				pAdapter->m_type = Ethernet;				// the adapter type
				pAdapter->m_currAddr = 0;			// The current ip addr (if any)

#if defined(SW_PLATFORM_MACH)
	#define PF_PACKET	PF_LOCAL
#endif
				if (paddr->ifa_addr->sa_family == PF_PACKET)
					{
					pAdapter->m_index = index;	// the adapter index
					}

				if (paddr->ifa_addr->sa_family == AF_INET)
					{
					saddr.set(paddr->ifa_addr, sizeof(*paddr->ifa_addr));
					pAdapter->m_ipaddrs.add(saddr.toIPAddress());			// A list of IP addresses for this adapter	
					saddr.set(paddr->ifa_netmask, sizeof(*paddr->ifa_netmask));
					pAdapter->m_ipmasks.add(saddr.toIPAddress());			// A list of IP address masks for this adapter	
					}

				// pAdapter->XIPAddress			m_gateway;			// The default gateway for this adapter

				// DNS
				// pAdapter->m_dnsServers;					// A list of DNS servers for this adapter
				// pAdapter->m_dnsRegisterAdapterHostName;	// A flag indicating that the adapter host name will be registered with DNS
				// pAdapter->m_dnsRegisterHostName;			// A flag indicating that the adpater will register with DNS
				// pAdapter->m_dnsAdapterDomainName;			// The DNS domain specifically for this adapter
				// pAdapter->m_dnsPrimaryDomainName;			// The primary DNS domain for this machine

				// DHCP
				pAdapter->m_dhcp = false;				// the dhcp flag (true if enabled)
				// pAdapter->m_dhcpServer;			// the dhcp server
				// pAdapter->m_dhcpLeaseObtained;		// when the dhcp lease was obtained
				// pAdapter->m_dhcpLeaseExpires;		// when the dhcp lease will expire

				// WINS
				pAdapter->m_wins = false;				// the wins flag (true if available)
				// pAdapter->m_winsPrimaryServer;		// the primary wins server
				// pAdapter->m_winsSecondaryServer;		// the secondary wins server
				}

			paddr = paddr->ifa_next;
			index++;
			}

		freeifaddrs(addrlist);
		result = true;
#endif
		
		return result;
		}


sw_status_t
SWNetworkAdapter::getBestRouteForAddr(const SWIPAddress &addr, SWNetworkAdapter &adapter, int &ipaddrindex)
		{
		sw_status_t	res=SW_STATUS_NOT_FOUND;
		sw_uint32_t	raddr=addr.toHostInteger();

		if (res == SW_STATUS_NOT_FOUND)
			{
			SWArray<SWNetworkAdapter>	alist;

			SWNetworkAdapter::getLocalAdapters(alist);

			for (int i=0;i<(int)alist.size();i++)
				{
				SWNetworkAdapter	&a=alist[i];
				int					naddrs=a.ipAddressCount();

				for (int j=0;j<naddrs;j++)
					{
					SWIPAddress		addr=a.ipAddress(j), mask=a.ipAddressMask(j);
					sw_uint32_t		uaddr=a.ipAddress(j).toHostInteger(), umask=a.ipAddressMask(j).toHostInteger();
					sw_uint32_t		umasked=(uaddr & umask), rmasked=(raddr & umask);
				
					if (umasked == rmasked)
						{
						adapter = a;
						ipaddrindex = j;
						res = SW_STATUS_SUCCESS;
						}
					}
				}
			}

#if defined(SW_PLATFORM_WINDOWS)
		if (res == SW_STATUS_NOT_FOUND)
			{
			DWORD	idx = 0;

			if (GetBestInterface(raddr, &idx) == NO_ERROR)
				{
				ULONG	bufsize = 0;
				DWORD	dwStatus = 0;

				// Get how much memory is needed
				dwStatus = GetAdaptersInfo(NULL, &bufsize);			// get buffer size required
				if (dwStatus != NO_ERROR && bufsize > 0)
					{
					SWBuffer	buf(bufsize);

					// allocate the required memory
					IP_ADAPTER_INFO *pIpArray = (IP_ADAPTER_INFO *)(void *)buf;

					// get the raw adapters info (not unicode)
					dwStatus = GetAdaptersInfo(pIpArray, &bufsize);
					if (dwStatus == NO_ERROR)
						{
						// find out how many structures there are in the array
						const IP_ADAPTER_INFO	*ip = pIpArray;

						while (ip != NULL)
							{
							if (idx == ip->Index)
								{
								adapter = SWNetworkAdapter(*ip);
								ipaddrindex = 0;
								res = SW_STATUS_SUCCESS;
								break;
								}

							ip = ip->Next;
							}
						}
					}
				}
			}
#endif
		if (res == SW_STATUS_NOT_FOUND && raddr == 0x7f000001)
			{
			// Quick and dirty return a pseudo interface for 127.0.0.1

			SWSocketAddress	saddr;

			adapter.clear();
			adapter.m_name = "lo";				// the adpater name
			// adapter.m_desc;				// the adapter description
			adapter.m_displayName = "Loopback";			// the adapter display name
			// adapter.m_address;			// the adapter hardware address
			adapter.m_type = Ethernet;				// the adapter type
			adapter.m_currAddr = 0;			// The current ip addr (if any)

			adapter.m_index = -1;	// the adapter index

			adapter.m_ipaddrs.add(SWIPAddress("127.0.0.1"));			// A list of IP addresses for this adapter	
			adapter.m_ipmasks.add(SWIPAddress("255.0.0.0"));			// A list of IP address masks for this adapter

			ipaddrindex = 0;
			res = SW_STATUS_SUCCESS;
			}
		
		return res;
		}

