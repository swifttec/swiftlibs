/**
*** @file		SWUInt32.cpp
*** @brief		A representation of date based on Julian day numbers
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWUInt32 class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <swift/SWUInt32.h>
#include <swift/sw_printf.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif


SWUInt32::SWUInt32(const char *s)
		{
		*this = s;
		}


SWUInt32 &
SWUInt32::operator=(const char *s)
		{
		m_amount = atoi(s);

		return *this;
		}


SWUInt32::SWUInt32(const wchar_t *s)
		{
		*this = s;
		}


SWUInt32 &
SWUInt32::operator=(const wchar_t *s)
		{
		m_amount = wtoi(s);

		return *this;
		}




SWUInt32::SWUInt32(const SWString &s)
		{
		*this = s;
		}


SWUInt32 &
SWUInt32::operator=(const SWString &s)
		{
		if (s.charSize() == sizeof(char)) *this = s.c_str();
		else if (s.charSize() == sizeof(wchar_t)) *this = s.w_str();
		else m_amount = 0;

		return *this;
		}


SWString
SWUInt32::toString(const SWString &fmt) const
		{
		SWString	s;
		char		nstr[256];

		sw_snprintf(nstr, sizeof(nstr), fmt, m_amount);
		s = nstr;

		return s;
		}
