/**
*** @file		SWString.cpp
*** @brief		A comprehensive string class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWString class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWString.h>
#include <swift/SWSimpleCString.h>
#include <swift/SWSimpleWString.h>
#include <swift/SWStringPtr.h>
#include <swift/SWOutputStream.h>
#include <swift/SWInputStream.h>
#include <swift/SWValue.h>
#include <swift/SWTextFile.h>
#include <swift/SWResource.h>

#include <xplatform/sw_process.h>

#include "sw_internal_printf.h"


#if !defined(SW_PLATFORM_WINDOWS)
	#include <ctype.h>
#endif

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





SWMemoryManager			*SWString::sm_pMemoryManager=NULL;
sw_size_t				SWString::sm_zero=0;

void
SWString::init()
		{
		if (sm_pMemoryManager == NULL)
			{
			sm_pMemoryManager = SWMemoryManager::getDefaultMemoryManager();
			}

		m_length = 0;
		m_flags = 0;
		}


SWString::SWString()
		{
		init();
		}


SWString::SWString(const char *s)
		{
		init();
		if (*s) bufCopy(s, strlen(s), sizeof(*s));
		}


SWString::SWString(const wchar_t *s)
		{
		init();
		bufCopy(s, wcslen(s), sizeof(*s));
		}


SWString::SWString(const char *s, sw_size_t n)
		{
		sw_size_t	l=strlen(s);

		init();
		if (n > l) n = l;
		if (*s) bufCopy(s, n, sizeof(*s));
		}


SWString::SWString(const wchar_t *s, sw_size_t n)
		{
		sw_size_t	l=wcslen(s);

		init();
		if (n > l) n = l;
		bufCopy(s, n, sizeof(*s));
		}


SWString::SWString(const SWString &s) :
	SWObject()
		{
		init();
		if (s.length())
			{
			bufCopy(s.m_data, s.length(), s.charSize());
			}
		}


SWString::SWString(void *pData, sw_size_t slen, sw_size_t cs) :
	m_data(pData, (slen + 1) * cs),
	m_length(slen),
	m_flags(SW_STRING_LOCKED)
		{
		setCharSize(cs);
		m_flags |= SW_STRING_CHARSIZE_FIXED;
		}


SWString::SWString(int c, sw_size_t n, sw_size_t cs, bool fixedCharSize)
		{
		init();
		if (cs == 0)
			{
			if (c > 255) cs = sizeof(wchar_t);
			else cs = sizeof(char);
			}
			
		setCharSize(cs);
		if (fixedCharSize) m_flags |= SW_STRING_CHARSIZE_FIXED;

		if (c != 0)
			{
			m_length = n;
			if (cs == sizeof(char))
				{
				char	*cp=reinterpret_cast<char *>(ensureCapacity(n, false));

				while (n > 0)
					{
					*cp++ = (char)c;
					--n;
					}

				*cp = 0;
				}
			else if (cs == sizeof(wchar_t))
				{
				wchar_t	*cp=reinterpret_cast<wchar_t *>(ensureCapacity(n, false));

				while (n > 0)
					{
					*cp++ = (wchar_t)c;
					--n;
					}

				*cp = 0;
				}
			}
		}


SWString::~SWString()
		{
		}


void
SWString::readyForUpdate()
		{
		if (isLocked()) throw SW_EXCEPTION(SWStringException);
		}

		
void
SWString::setCharSize(sw_size_t cs)
		{
		if (isFixedCharSize() && cs != charSize()) throw SW_EXCEPTION(SWStringException);

		m_flags &= ~SW_STRING_CHARSIZE_MASK;
		m_flags |= (cs & SW_STRING_CHARSIZE_MASK);
		}


sw_size_t
SWString::capacity() const
		{
		sw_size_t	v=0;

		if (charSize() != 0) v = m_data.capacity() / charSize();

		return v;
		}



void
SWString::capacity(sw_size_t nchars, bool freeExtraMem)
		{
		readyForUpdate();

		// Make sure we have a charsize set
		if (charSize() == 0)
			{
#if defined(SW_BUILD_UNICODE)
			setCharSize(sizeof(wchar_t));
#elif defined(SW_BUILD_MCBS)
			setCharSize(sizeof(char));
#else
	#error Neither SW_BUILD_UNICODE nor SW_BUILD_MCBS are set.
#endif
			}

		ensureCapacity(nchars, freeExtraMem);
		}


void *
SWString::ensureCapacity(sw_size_t nchars, bool freeExtraMem)
		{
		sw_size_t	cs=charSize();

		if (cs == 0) throw SW_EXCEPTION(SWStringException);

		void		*pOldData, *pNewData;

		// Make sure we have enough space for the nul char
		// and adjust for charsize
		sw_size_t	wanted = (nchars + 1) * cs;

		if (freeExtraMem && (wanted < ((m_length + 1) *cs)))
			{
			wanted = (m_length + 1) * cs;
			}

		pOldData = m_data.data();
		pNewData = m_data.data(wanted, freeExtraMem);

		if (pOldData == NULL && pNewData != NULL)
			{
			// Must zero terminate the buffer
			memset(pNewData, 0, cs);
			}

		return pNewData;
		}


void
SWString::bufCopy(const void *pData, sw_size_t nchars, sw_size_t charsize)
		{
		// Clear the current string, record the char size,
		// ensure we have enough buffer space, copy and zero-terminate the data.
		setCharSize(charsize);
        size_t		nbytes=nchars * charsize;
		sw_byte_t	*cp=reinterpret_cast<sw_byte_t *>(ensureCapacity(nchars, false));

		if (nbytes > 0) memcpy(cp, pData, nbytes);
        memset(cp + nbytes, 0, charsize);

        m_length = nchars;
		}


void
SWString::bufAppend(const void *pData, sw_size_t nchars, sw_size_t charsize)
		{
		if (nchars > 0)
			{
			if (charsize != charSize()) throw SW_EXCEPTION(SWStringException);

			// ensure we have enough buffer space, copy and zero-terminate the data.
			sw_byte_t	*cp=reinterpret_cast<sw_byte_t *>(ensureCapacity(nchars + m_length, false));

			memcpy(cp + (m_length * charsize), pData, nchars * charsize);
			m_length += nchars;
            memset(cp + (m_length * charsize), 0, charsize);
			
			clearConversion();
			}
		}


void
SWString::bufInsert(sw_size_t pos, const void *pData, sw_size_t nchars, sw_size_t charsize)
		{
		if (pos <= m_length && nchars > 0)
			{
			if (charsize != charSize()) throw SW_EXCEPTION(SWStringException);

			// Make sure we have enough space
			sw_byte_t	*cp=reinterpret_cast<sw_byte_t *>(ensureCapacity(nchars + m_length, false));

			sw_size_t	nbytes=0;

			// Work out how much we must preserve if any
			nbytes = (m_length - pos + 1) * charsize;
			if (nbytes > 0)
				{
				// Use memmove to safely move the data when overlaps occur
				memmove(cp + ((pos + nchars) * charsize), cp + (pos * charsize), nbytes);
				}

			if (pData != NULL) memcpy(cp + (pos * charsize), pData, nchars * charsize);
			else memset(cp + (pos * charsize), 0, nchars * charsize);

			m_length += nchars;
            memset(cp + (m_length * charsize), 0, charsize);

			clearConversion();
			}
		}


void
SWString::bufRemove(sw_size_t pos, sw_size_t nchars, sw_size_t charsize)
		{
		if (pos < m_length && nchars > 0)
			{
			if (charsize != charSize()) throw SW_EXCEPTION(SWStringException);

			if ((nchars + pos) > m_length) nchars = m_length - pos;
			if (nchars > 0)
				{
				// Just change the string and keep the capacity as it
				sw_byte_t	*to = reinterpret_cast<sw_byte_t *>(m_data.data()) + (pos * charsize);
				sw_byte_t	*from = reinterpret_cast<sw_byte_t *>(m_data.data()) + ((pos + nchars) * charsize);
				sw_size_t	nc = (m_length - (pos + nchars) + 1) * charsize;

				memmove(to, from, nc);
				m_length -= nchars;

				clearConversion();
				}
			}
		}


void
SWString::bufSetChars(int c, sw_size_t pos, sw_size_t nchars, bool checkCapacity, bool addNullChar)
		{
		// Make sure we have enough buffer space
		if (checkCapacity) ensureCapacity(pos + nchars, false);

		sw_size_t	i;
		sw_size_t	cs=charSize();
		sw_byte_t	*bp = reinterpret_cast<sw_byte_t *>(m_data.data()) + (pos * cs);

		if (cs == 1 || c == 0)
			{
			// Use memset as it works on chars, the rest have to do it "manually"
			if (addNullChar && c == 0) nchars++;
			memset(bp, c, nchars * cs);
			if (addNullChar && c != 0) bp[nchars] = 0;
			}
		else if (cs == sizeof(wchar_t))
			{
			wchar_t	*wp=(wchar_t *)bp;

			for (i=0;i<nchars;i++) *wp++ = (wchar_t)c;
			if (addNullChar) *wp = 0;
			}

		clearConversion();
		}


int
SWString::bufCharAt(sw_size_t pos) const
		{
		sw_size_t	cs=charSize();
		int			c;
		
		if (cs == 0) throw SW_EXCEPTION(SWStringException);

		if (cs == sizeof(char))
			{
			char	*sp=m_data;
			
			c = sp[pos] & 0xff;
			}
		else
			{
			wchar_t	*sp=m_data;
			
			c = sp[pos] & 0xffff;
			}
			
		return c;
		}
		
		
void
SWString::clearConversion()
		{
		m_flags &= ~SW_STRING_CONVERSION_VALID;
		}


const char *
SWString::c_str() const
		{
		char	*sp=reinterpret_cast<char *>(&sm_zero);

		if (m_length)
			{
			if (charSize() == sizeof(char))
				{
				sp = m_data;
				}
			else
				{
				if ((m_flags & SW_STRING_CONVERSION_VALID) == 0)
					{
					// Must make the conversion from wchar_t to char
					sw_size_t	n=sw_wcstombs(NULL, m_data, m_length+1, CP_UTF8);

					if (n != (sw_size_t)-1)
						{
						char	*ts=(char *)m_altdata.data(n+1);

						sw_wcstombs(ts, m_data, n, CP_UTF8);
						ts[n] = 0;
						}
					}

				sp = m_altdata;
				}
			}

		return sp;
		}


const wchar_t *
SWString::w_str() const
		{
		wchar_t	*sp=reinterpret_cast<wchar_t *>(&sm_zero);

		if (m_length)
			{
			if (charSize() == sizeof(wchar_t))
				{
				sp = m_data;
				}
			else
				{
				if ((m_flags & SW_STRING_CONVERSION_VALID) == 0)
					{
					// Must make the conversion from char to wchar_t
					sw_mbstowcs((wchar_t *)m_altdata.data(sizeof(wchar_t) * (m_length+1)), m_data, m_length+1, CP_ACP);
					}

				sp = m_altdata;
				}
			}

		return sp;
		}


SWString &
SWString::operator+=(int c)
		{
		return append(c);
		}


SWString &
SWString::operator+=(const SWString &s)
		{
		return replace((sw_index_t)length(), 0, s);
		}


SWString &
SWString::operator+=(const char *s)
		{
		return replace((sw_index_t)length(), 0, s);
		}


SWString &
SWString::operator+=(const wchar_t *s)
		{
		return replace((sw_index_t)length(), 0, s);
		}


SWString	SWString::operator+(int v) const					{ SWString	res(*this);	res += v;	return res;	}
SWString	SWString::operator+(const SWString &v) const		{ SWString	res(*this);	res += v;	return res; }
SWString	SWString::operator+(const char *v) const			{ SWString	res(*this);	res += v;	return res;	}
SWString	SWString::operator+(const wchar_t *v) const			{ SWString	res(*this);	res += v;	return res;	}

// GLobal operators
//SWString	operator+(const SWString &lhs, int rhs)				{ SWString	res(lhs);	res += rhs;	return res; }
//SWString	operator+(const SWString &lhs, const char *rhs)		{ SWString	res(lhs);	res += rhs;	return res; }
//SWString	operator+(const SWString &lhs, const wchar_t *rhs)	{ SWString	res(lhs);	res += rhs;	return res; }
//SWString	operator+(const SWString &lhs, const SWString &rhs)	{ SWString	res(lhs);	res += rhs;	return res; }

SWIFT_DLL_EXPORT SWString	operator+(int lhs, const SWString &rhs)				{ SWString	res(lhs);	res += rhs;	return res; }
SWIFT_DLL_EXPORT SWString	operator+(const char *lhs, const SWString &rhs)		{ SWString	res(lhs);	res += rhs;	return res; }
SWIFT_DLL_EXPORT SWString	operator+(const wchar_t *lhs, const SWString &rhs)	{ SWString	res(lhs);	res += rhs;	return res; }


void
SWString::clear()
		{
		readyForUpdate();

		sw_size_t	cs=charSize();

		if (cs)
			{
			bufSetChars(0, 0, 1, true, false);
			}

		m_flags &= ~SW_STRING_CONVERSION_VALID;
		m_length = 0;
		}


void
SWString::setCharAt(sw_index_t pos, int c)
		{
		readyForUpdate();

		if (pos >= 0 && pos < (sw_index_t)m_length)
			{
			bufSetChars(c, pos, 1, false, false);
			}
		else
			{
			throw SW_EXCEPTION(SWIndexOutOfBoundsException);
			}
		}


int
SWString::getCharAt(sw_index_t pos) const
		{
		int		c=0;
		
		if (pos >= 0 && pos < (sw_index_t)m_length)
			{
			c = bufCharAt(pos);
			}
		else
			{
			throw SW_EXCEPTION(SWIndexOutOfBoundsException);
			}
			
		return c;
		}


int
SWString::compareTo(const SWString &other, caseCompare cc) const
		{
		int		r=0;

		if (charSize() == sizeof(char) && other.charSize() == sizeof(char))
			{
			// No conversion required as both are char strings
			r = compareTo(other.c_str(), cc);
			}
		else
			{
			// Always convert up to wchar to retain accuracy
			r = compareTo(other.w_str(), cc);
			}

		return r;
		}


int
SWString::compareTo(const char *other, caseCompare cc) const
		{
		int		r=0;

		if (charSize() == sizeof(char))
			{
			if (cc == exact) r = strcmp(c_str(), other);
			else r = stricmp(c_str(), other);
			}
		else
			{
			// Always convert up to wchar
			if (cc == exact) r = wcscmp(w_str(), SWString(other).w_str());
			else  r = wcsicmp(w_str(), SWString(other).w_str());
			}

		return r;
		}


int
SWString::compareTo(const wchar_t *other, caseCompare cc) const
		{
		int		r=0;

		if (charSize() == sizeof(char))
			{
			if (cc == exact) r = wcscmp(w_str(), other);
			else  r = wcsicmp(w_str(), other);
			}
		else
			{
			if (cc == exact) r = wcscmp(w_str(), other);
			else  r = wcsicmp(w_str(), other);
			}

		return r;
		}


SWString &
SWString::operator=(int c)
		{
		readyForUpdate();

		if (isWideChar(c))
			{
			wchar_t	s[2];
			
			s[0] = (wchar_t)c;
			s[1] = 0;
			operator=(s);
			}
		else
			{
			char	s[2];
			
			s[0] = (char)c;
			s[1] = 0;
			operator=(s);
			}
			
		return *this;
		}


SWString &
SWString::operator=(const SWString &s)
		{
		if (s.length() == 0)
			{
			empty();
			}
		else
			{
			readyForUpdate();

			if (!isFixedCharSize() || s.charSize() == charSize())
				{
				bufCopy(s.m_data, s.length(), s.charSize());
				}
			else
				{
				if (charSize() == sizeof(char))
					{
					const char	*sp=s.c_str();
					sw_size_t	len=strlen(sp);

					bufCopy(sp, len, sizeof(*sp));
					}
				else if (charSize() == sizeof(wchar_t))
					{
					const wchar_t	*sp=s.w_str();
					sw_size_t		len=wcslen(sp);

					bufCopy(sp, len, sizeof(*sp));
					}
				}
			}

		return *this;
		}


SWString &
SWString::operator=(const char *str)
		{
		readyForUpdate();

		if (!isFixedCharSize() || charSize() == sizeof(*str))
			{
			if (str != NULL)
				{
				bufCopy(str, strlen(str), sizeof(*str));
				}
			else
				{
				bufCopy("", 0, sizeof(char));
				}
			}
		else
			{
			if (str != NULL)
				{
				SWSimpleWString	s(str);

				bufCopy(s, s.length(), s.charSize());
				}
			else
				{
				bufCopy(L"", 0, sizeof(wchar_t));
				}
			}

		return *this;
		}


SWString &
SWString::operator=(const wchar_t *str)
		{
		readyForUpdate();

		if (!isFixedCharSize() || charSize() == sizeof(*str))
			{
			if (str != NULL)
				{
				bufCopy(str, wcslen(str), sizeof(*str));
				}
			else
				{
				bufCopy(L"", 0, sizeof(wchar_t));
				}
			}
		else
			{
			if (str != NULL)
				{
				SWSimpleCString	s(str);

				bufCopy(s, s.length(), s.charSize());
				}
			else
				{
				bufCopy("", 0, sizeof(char));
				}
			}

		return *this;
		}


SWString
SWString::left(sw_size_t n) const
		{
		return substring(0, (sw_index_t)(n - 1));
		}


SWString
SWString::right(sw_size_t n) const
		{
		return substring((sw_index_t)length() - (sw_index_t)n, (sw_index_t)(length() - 1));
		}


SWString
SWString::mid(sw_index_t pos) const
		{
		return substring(pos, (sw_index_t)(length() - 1));
		}


SWString
SWString::mid(sw_index_t pos, sw_size_t n) const
		{
		return substring(pos, pos + (sw_index_t)n - 1);
		}
		

SWString
SWString::substring(sw_index_t beginIndex) const
		{
		return substring(beginIndex, (sw_index_t)length() - 1);
		}


SWString
SWString::substring(sw_index_t start, sw_index_t end) const
		{
		SWString	s;
		sw_size_t	len;

		if (start < 0) start = 0;
		if (end >= (sw_index_t)m_length) end = (sw_index_t)m_length - 1;
		len = (sw_size_t)(end - start + 1);
		if (start < (sw_index_t)m_length && len > 0)
			{
			if (len > (m_length - start)) len = m_length - start;

			if (charSize() == sizeof(wchar_t)) s = SWString((const wchar_t *)m_data + start, len);
			else if (charSize() == sizeof(char)) s = SWString((const char *)m_data + start, len);
			}

		return s;
		}


int
SWString::getCharFromBuffer(const void *bp, sw_index_t offset, sw_size_t charsize)
		{
		int		c=0;

		if (charsize == sizeof(wchar_t)) c = *((const wchar_t *)bp+offset);
		else c = *((const char *)bp+offset);

		return c;
		}



// Pattern matching. Starting at position pos i, searches for the first occurrence of the first patlen bytes from pat in self
// and returns the index of the start of the match. Returns -1 if there is no such pattern. Case sensitivity is
// according to the caseCompare argument.
int
SWString::indexOfString(
	const void *pat,	// The patten to look for
	sw_size_t patlen,			// The length of the pattern
	sw_size_t patcsize,		// The charsize of the pattern
	const void *str,	// The string to search
	sw_size_t slen,			// The length of the string buffer
	sw_size_t strcsize,		// The string charsize
	sw_index_t pos,			// The position to start within the string
	caseCompare cmp,	// Case comparison type
	bool reverse)		// The reverse flag
		{
		int			c1, c2, res=-1;
		sw_index_t	patpos;

		if (reverse)
			{
			// Adjust pos before we start
			if (pos + patlen > slen) pos = (int)(slen - patlen);
			}

		while ((reverse && pos >= 0) || (!reverse && pos+patlen <= slen))
			{
			for (patpos=0;patpos<(sw_index_t)patlen;patpos++)
				{
				c1 = getCharFromBuffer(str, pos + patpos, strcsize);
				c2 = getCharFromBuffer(pat, patpos, patcsize);
				if (cmp != exact)
					{
					c1 = tolower(c1);
					c2 = tolower(c2);
					}

				if (c1 != c2) break;
				}

			if (patpos == (sw_index_t)patlen)
				{
				// We've got a match, return the starting index
				res = pos;
				break;
				}

			// Start again from next char
			pos += (reverse?-1:1);
			}

		return res;
		}


int
SWString::indexOf(int c) const
		{
		int		r;

		if (isWideChar(c))
			{
			wchar_t	str[2];

			str[0] = (wchar_t)c;
			str[1] = 0;
			r = indexOfString(str, 1, sizeof(*str), m_data, length(), charSize(), 0, exact, false);
			}
		else
			{
			char	str[2];

			str[0] = (char)c;
			str[1] = 0;
			r = indexOfString(str, 1, sizeof(*str), m_data, length(), charSize(), 0, exact, false);
			}

		return r;
		}


int
SWString::indexOf(int c, sw_index_t pos) const
		{
		int		r;

		if (isWideChar(c))
			{
			wchar_t	str[2];

			str[0] = (wchar_t)c;
			str[1] = 0;
			r = indexOfString(str, 1, sizeof(*str), m_data, length(), charSize(), pos, exact, false);
			}
		else
			{
			char	str[2];

			str[0] = (char)c;
			str[1] = 0;
			r = indexOfString(str, 1, sizeof(*str), m_data, length(), charSize(), pos, exact, false);
			}

		return r;
		}


int
SWString::indexOf(const char *str, caseCompare cmp) const
		{
		return indexOfString(str, strlen(str), sizeof(*str), m_data, length(), charSize(), 0, cmp, false);
		}


int
SWString::indexOf(const wchar_t *str, caseCompare cmp) const
		{
		return indexOfString(str, wcslen(str), sizeof(*str), m_data, length(), charSize(), 0, cmp, false);
		}


int
SWString::indexOf(const char *str, sw_index_t pos, caseCompare cmp) const
		{
		return indexOfString(str, strlen(str), sizeof(*str), m_data, length(), charSize(), pos, cmp, false);
		}


int
SWString::indexOf(const wchar_t *str, sw_index_t pos, caseCompare cmp) const
		{
		return indexOfString(str, wcslen(str), sizeof(*str), m_data, length(), charSize(), pos, cmp, false);
		}


int
SWString::indexOf(const SWString &str, caseCompare cmp) const
		{
		int		r=0;

		if (str.length())
			{
			r = indexOfString(str.m_data, str.length(), str.charSize(), m_data, length(), charSize(), 0, cmp, false);
			}

		return r;
		}


int
SWString::indexOf(const SWString &str, sw_index_t pos, caseCompare cmp) const
		{
		int		r=0;

		if (str.length())
			{
			r = indexOfString(str.m_data, str.length(), str.charSize(), m_data, length(), charSize(), pos, cmp, false);
			}

		return r;
		}


int
SWString::lastIndexOf(int c) const
		{
		int		r;

		if (isWideChar(c))
			{
			wchar_t	str[2];

			str[0] = (wchar_t)c;
			str[1] = 0;
			r = indexOfString(str, 1, sizeof(*str), m_data, length(), charSize(), (sw_index_t)length()-1, exact, true);
			}
		else
			{
			char	str[2];

			str[0] = (char)c;
			str[1] = 0;
			r = indexOfString(str, 1, sizeof(*str), m_data, length(), charSize(), (sw_index_t)length()-1, exact, true);
			}

		return r;
		}


int
SWString::lastIndexOf(int c, sw_index_t pos) const
		{
		int		r;

		if (isWideChar(c))
			{
			wchar_t	str[2];

			str[0] = (wchar_t)c;
			str[1] = 0;
			r = indexOfString(str, 1, sizeof(*str), m_data, length(), charSize(), pos, exact, true);
			}
		else
			{
			char	str[2];

			str[0] = (char)c;
			str[1] = 0;
			r = indexOfString(str, 1, sizeof(*str), m_data, length(), charSize(), pos, exact, true);
			}

		return r;
		}


int
SWString::lastIndexOf(const char *str, caseCompare cmp) const
		{
		return indexOfString(str, strlen(str), sizeof(*str), m_data, length(), charSize(), (sw_index_t)length()-1, cmp, true);
		}


int
SWString::lastIndexOf(const wchar_t *str, caseCompare cmp) const
		{
		return indexOfString(str, wcslen(str), sizeof(*str), m_data, length(), charSize(), (sw_index_t)length()-1, cmp, true);
		}


int
SWString::lastIndexOf(const char *str, sw_index_t pos, caseCompare cmp) const
		{
		return indexOfString(str, strlen(str), sizeof(*str), m_data, length(), charSize(), pos, cmp, true);
		}


int
SWString::lastIndexOf(const wchar_t *str, sw_index_t pos, caseCompare cmp) const
		{
		return indexOfString(str, wcslen(str), sizeof(*str), m_data, length(), charSize(), pos, cmp, true);
		}


int
SWString::lastIndexOf(const SWString &str, caseCompare cmp) const
		{
		int		r=0;

		if (str.length())
			{
			r = indexOfString(str.m_data, str.length(), str.charSize(), m_data, length(), charSize(), (sw_index_t)length()-1, cmp, true);
			}

		return r;
		}


int
SWString::lastIndexOf(const SWString &str, sw_index_t pos, caseCompare cmp) const
		{
		int		r=0;

		if (str.length())
			{
			r = indexOfString(str.m_data, str.length(), str.charSize(), m_data, length(), charSize(), pos, cmp, true);
			}

		return r;
		}






int
SWString::firstOf(const char *str) const
		{
		return findOneOf(str, strlen(str), sizeof(*str), m_data, length(), charSize(), 0, exact, false);
		}
		

int
SWString::firstOf(const wchar_t *str) const
		{
		return findOneOf(str, wcslen(str), sizeof(*str), m_data, length(), charSize(), 0, exact, false);
		}
		

int
SWString::firstOf(const SWString &str) const
		{
		return findOneOf(str.m_data, str.length(), str.charSize(), m_data, length(), charSize(), 0, exact, false);
		}
		

int
SWString::lastOf(const char *str) const
		{
		return findOneOf(str, strlen(str), sizeof(*str), m_data, length(), charSize(), (sw_index_t)length()-1, exact, true);
		}
		

int
SWString::lastOf(const wchar_t *str) const
		{
		return findOneOf(str, wcslen(str), sizeof(*str), m_data, length(), charSize(), (sw_index_t)length()-1, exact, true);
		}
		

int
SWString::lastOf(const SWString &str) const
		{
		return findOneOf(str.m_data, str.length(), str.charSize(), m_data, length(), charSize(), (sw_index_t)length()-1, exact, true);
		}
		

int
SWString::findOneOf(
	const void *pat,	// The patten to look for
	sw_size_t patlen,			// The length of the pattern
	sw_size_t patcsize,		// The charsize of the pattern
	const void *str,	// The string to search
	sw_size_t slen,			// The length of the string buffer
	sw_size_t strcsize,		// The string charsize
	sw_index_t pos,			// The position to start within the string
	caseCompare cmp,	// Case comparison type
	bool reverse)		// The reverse flag
		{
		int			c1, c2, res=-1;
		sw_index_t	patpos;

		if (reverse)
			{
			// Adjust pos before we start
			if (pos > (sw_index_t)slen) pos = (sw_index_t)slen;
			}

		while ((reverse && pos >= 0) || (!reverse && pos < (sw_index_t)slen))
			{
			c1 = getCharFromBuffer(str, pos, strcsize);
			
			for (patpos=0;patpos<(sw_index_t)patlen;patpos++)
				{
				c2 = getCharFromBuffer(pat, patpos, patcsize);
				if (cmp != exact)
					{
					c1 = tolower(c1);
					c2 = tolower(c2);
					}

				if (c1 == c2)
					{
					res = pos;
					break;
					}
				}

			if (res != -1)
				{
				// We've got a match, return the starting index
				res = pos;
				break;
				}

			// Start again from next char
			pos += (reverse?-1:1);
			}

		return res;
		}



SWString &
SWString::remove(sw_index_t pos)
		{
		readyForUpdate();

		if (pos >= 0 && pos < (sw_index_t)m_length)
			{
			m_length = pos;
			bufSetChars(0, pos, 1, false, false);
			}

		return *this;
		}


SWString &
SWString::remove(sw_index_t pos, sw_size_t nchars)
		{
		readyForUpdate();

		if (pos >= 0 && pos < (sw_index_t)m_length && nchars > 0)
			{
			sw_size_t	n=nchars;

			if (pos + n > m_length) n = m_length - pos;

			if (n > 0)
				{
				bufRemove(pos, nchars, charSize());
				}
			}

		return *this;
		}


sw_size_t
SWString::count(const SWString &charSet, incType itype, sw_index_t pos, bool reverse) const
		{
		sw_size_t	nchars=0;
		int			r;

		for (;;)
			{
			if (pos < 0 || pos >= (sw_index_t)m_length) break;

			r = charSet.first((*this)[pos]);
			if ((r < 0 && itype == including) || (r >= 0 && itype == excluding))
				{
				// We have reached the end of the set
				break;
				}

			nchars++;
			if (!reverse) pos++;
			else --pos;
			}

		return nchars;
		}



SWString
SWString::span(const SWString &charSet, incType itype, sw_index_t pos, bool reverse) const
		{
		SWString	s;

		if (reverse) s = right(count(charSet, itype, pos, reverse));
		else s = left(count(charSet, itype, pos, reverse));

		return s;
		}


SWString &
SWString::replaceAll(int oldchar, int newchar, int count)
		{
		readyForUpdate();

		count = 0;
		if (m_length > 0)
			{
			if (charSize() == sizeof(wchar_t))
				{
				wchar_t	*sp=m_data;

				while (*sp)
					{
					if (*sp == (wchar_t)oldchar) *sp = (wchar_t)newchar;
					sp++;
					count++;
					}
				}
			else
				{
				char	*sp=m_data;

				while (*sp)
					{
					if (*sp == (char)oldchar) *sp = (char)newchar;
					sp++;
					count++;
					}
				}
			}

		return *this;
		}


SWString &
SWString::replaceAll(const char *oldstr, const char *newstr, caseCompare cmp, int count)
		{
		readyForUpdate();
		
		int			pos=0;
		sw_size_t	oldlen = strlen(oldstr);
		sw_size_t	newlen = strlen(newstr);

		count = 0;
		while (pos < (int)length() && pos >= 0)
			{
			pos = indexOf(oldstr, pos, cmp);
			if (pos >= 0)
				{
				replace(pos, oldlen, newstr);

				// Skip over the newly inserted string
				pos += (sw_index_t)newlen;
				count++;
				}
			}

		return *this;
		}



SWString &
SWString::replaceAll(const wchar_t *oldstr, const wchar_t *newstr, caseCompare cmp, int count)
		{
		readyForUpdate();
		
		sw_index_t	pos=0;
		sw_size_t	oldlen = wcslen(oldstr);
		sw_size_t	newlen = wcslen(newstr);

		count = 0;
		while (pos < (sw_index_t)length() && pos >= 0)
			{
			pos = indexOf(oldstr, pos, cmp);
			if (pos >= 0)
				{
				replace(pos, oldlen, newstr);

				// Skip over the newly inserted string
				pos += (sw_index_t)newlen;
				count++;
				}
			}

		return *this;
		}


SWString &
SWString::replaceAll(const SWString &oldstr, const SWString &newstr, caseCompare cmp, int count)
		{
		if (charSize() == sizeof(wchar_t))
			{
			replaceAll(oldstr.w_str(), newstr.w_str(), cmp, count);
			}
		else
			{
			replaceAll(oldstr.c_str(), newstr.c_str(), cmp, count);
			}

		return *this;
		}




SWString &
SWString::replace(sw_index_t pos, sw_size_t n1, const char *str)
		{
		sw_size_t	n2=strlen(str);
		sw_size_t	cs=charSize();

		if (pos >= 0 && pos <= (sw_index_t)length())
			{
			readyForUpdate();

			if (cs == 0 || (m_length == 0 && !isFixedCharSize()))
				{
				cs = sizeof(*str);
				setCharSize(cs);
				}


			if (pos + n1 > m_length) n1 = m_length - pos;

			if (n1 > n2)
				{
				// Remove some chars first
				bufRemove(pos, n1 - n2, cs);
				}
			else if (n1 < n2)
				{
				// insert some room by calling insert
				bufInsert(pos, NULL, n2-n1, cs);
				}

			// Now copy the data in
			if (charSize() == sizeof(wchar_t))
				{
				SWSimpleWString	tmp(str);

				memcpy((wchar_t *)m_data + pos, tmp, n2 * sizeof(*tmp));
				}
			else
				{
				memcpy((char *)m_data + pos, str, n2 * sizeof(*str));
				}
			}

		return *this;
		}



SWString &
SWString::replace(sw_index_t pos, sw_size_t n1, const wchar_t *str)
		{
		readyForUpdate();

		sw_size_t	n2=wcslen(str);
		sw_size_t	cs=charSize();


		if (pos >= 0 && pos <= (sw_index_t)length())
			{
			readyForUpdate();

			if (cs == 0 || (m_length == 0 && !isFixedCharSize()))
				{
				cs = sizeof(*str);
				setCharSize(cs);
				}

			if (pos + n1 > m_length) n1 = m_length - pos;

			if (n1 > n2)
				{
				// Remove some chars first
				bufRemove(pos, n1 - n2, cs);
				}
			else if (n1 < n2)
				{
				// insert some room by calling insert
				bufInsert(pos, NULL, n2-n1, cs);
				}

			// Now copy the data in
			if (charSize() == sizeof(char))
				{
				SWSimpleCString	tmp(str);

				memcpy((char *)m_data + pos, tmp, n2 * sizeof(*tmp));
				}
			else
				{
				memcpy((wchar_t *)m_data + pos, str, n2 * sizeof(*str));
				}
			}

		return *this;
		}


SWString &
SWString::replace(sw_index_t pos, sw_size_t n, const SWString &s)
		{
		if (s.charSize() == sizeof(wchar_t))
			{
			replace(pos, n, s.w_str());
			}
		else
			{
			replace(pos, n, s.c_str());
			}

		return *this;
		}


SWString &
SWString::insert(sw_index_t pos, const char *s)
		{
		return replace(pos, 0, s);
		}


SWString &
SWString::insert(sw_index_t pos, const wchar_t *s)
		{
		return replace(pos, 0, s);
		}


SWString &
SWString::insert(sw_index_t pos, const SWString &s)
		{
		return replace(pos, 0, s);
		}



SWString &
SWString::prepend(int c)
		{
		readyForUpdate();

		if (isWideChar(c))
			{
			wchar_t	s[2];

			s[0] = (wchar_t)c;
			s[1] = 0;
			insert(0, s);
			}
		else
			{
			char	s[2];

			s[0] = (char)c;
			s[1] = 0;
			insert(0, s);
			}

		return *this;
		}


SWString &
SWString::prepend(const char *s)
		{
		return replace(0, 0, s);
		}


SWString &
SWString::prepend(const wchar_t *s)
		{
		return replace(0, 0, s);
		}


SWString &
SWString::prepend(const SWString &s)
		{
		return replace(0, 0, s);
		}


SWString &
SWString::append(int c, sw_size_t count)
		{
		readyForUpdate();

		if (isWideChar(c))
			{
			wchar_t	s[2];

			s[0] = (wchar_t)c;
			s[1] = 0;
			while (count-- > 0) *this += s;
			}
		else
			{
			char	s[2];

			s[0] = (char)c;
			s[1] = 0;
			while (count-- > 0) *this += s;
			}

		return *this;
		}


SWString &
SWString::append(const char *s)
		{
		return replace((sw_index_t)length(), 0, s);
		}


SWString &
SWString::append(const wchar_t *s)
		{
		return replace((sw_index_t)length(), 0, s);
		}


SWString &
SWString::append(const SWString &s)
		{
		return replace((sw_index_t)length(), 0, s);
		}




SWString &
SWString::strip(int c, stripType type)
		{
		readyForUpdate();

		if (isWideChar(c))
			{
			wchar_t	str[2];

			str[0] = (wchar_t)c;
			str[1] = 0;
			strip(str, type);
			}
		else
			{
			char	str[2];

			str[0] = (char)(c & 0xff);
			str[1] = 0;
			strip(str, type);
			}

		return *this;
		}


SWString &
SWString::strip(const SWString &s, stripType type)
		{
		readyForUpdate();

		if (m_length > 0 && (type == leading || type == both))
			{
			sw_size_t	nchars=count(s, including, 0, false);

			if (nchars > 0) remove(0, nchars);
			}

		if (m_length > 0 && (type == trailing || type == both))
			{
			sw_size_t	nchars=count(s, including, (sw_index_t)m_length-1, true);

			if (nchars > 0) remove((sw_index_t)(m_length - nchars));
			}

		return *this;
		}


// Return true if the prefix matches the string starting the comparsion at the offset given
bool
SWString::startsWith(const SWString &prefix, caseCompare cmp) const
		{
		return regionMatches(0, m_length, prefix, 0, cmp);
		}


// Return true if the suffix matches the end of the string
bool
SWString::endsWith(const SWString &suffix, caseCompare cmp) const
		{
		return regionMatches((sw_index_t)(m_length - suffix.length()), m_length, suffix, 0, cmp);
		}


// Tests if two string regions are equal.
bool
SWString::regionMatches(sw_index_t pos, sw_size_t len, const SWString &other, sw_index_t otherpos, caseCompare cmp) const
		{
		// If the match string is longer than this string we will never match
		if (other.length() > m_length) return false;
		
		// Some safety checks, since someone is bound to get it wrong and expect it to work
		if (otherpos < 0) otherpos = 0;
		if (pos < 0) pos = 0;
		if (len > (other.length() - (sw_size_t)otherpos)) len = other.length() - (sw_size_t)otherpos;

		// If len == 0 it's always true!
		if (!len) return true;

		// Check we are within the bounds of this string
		if (pos + len > length()) return false;

		SWString	thisRegionStr=mid(pos, len);
		SWString	otherRegionStr=other.mid(otherpos, len);

		return (thisRegionStr.compareTo(otherRegionStr, cmp) == 0);
		}


void *
SWString::lockBuffer(sw_size_t minlen, sw_size_t cs)
		{
		readyForUpdate();

		if (cs != sizeof(char) && cs != sizeof(wchar_t)) throw SW_EXCEPTION(SWStringException);

		m_flags |= SW_STRING_LOCKED;
		setCharSize(cs);
		ensureCapacity(minlen, false);

		return (void *)m_data;
		}


void
SWString::unlockBuffer()
		{
		if (!isLocked()) throw SW_EXCEPTION(SWStringException);

		m_flags &= ~SW_STRING_LOCKED;
		if (charSize() == sizeof(char)) m_length = strlen(m_data);
		else m_length = wcslen(m_data);
		}


// The guts of the printf formatting.
SWString &
SWString::formatv(const char *fmt, va_list ap)
		{
		readyForUpdate();
		clear();
		
		// If we have no char size yet use the format string as a guide.
		if (!isFixedCharSize())
			{
			setCharSize(sizeof(*fmt));
			}
		
		// Ensure we have at least enough space to output the format string (and then some)
		ensureCapacity(strlen(fmt)*2, false);
		
		sw_size_t	cs=charSize();

		outputinfo	info;

		memset(&info, 0, sizeof(info));
		info.inputtype = sizeof(*fmt);
		info.flags = SW_PRINTF_FLAG_REALLOC;
		info.in.cp = fmt;
		info.out.cp = (char *)m_data.m_pData;
		info.limit = (int)(m_data.m_capacity / cs);
		info.nulterminate = true;
		info.outputtype = (int)cs;
		info.pMemoryManager = m_data.m_pMemoryManager;
		info.pBuffer = info.out.cp;

		sw_internal_doprintf(&info, ap);

		m_length = info.outpos;
		m_data.m_capacity = info.limit * cs;
		m_data.m_pData = (sw_byte_t *)info.pBuffer;

		return *this;
		}


SWString &
SWString::formatv(const wchar_t *fmt, va_list ap)
		{
		readyForUpdate();
		clear();
		
		// If we have no char size yet use the format string as a guide.
		if (!isFixedCharSize())
			{
			setCharSize(sizeof(*fmt));
			}
		
		
		// Ensure we have at least enough space to output the format string (and then some)
		ensureCapacity(wcslen(fmt)*2, false);
		
		sw_size_t	cs=charSize();

		outputinfo	info;

		memset(&info, 0, sizeof(info));
		info.inputtype = sizeof(*fmt);
		info.flags = SW_PRINTF_FLAG_REALLOC;
		info.in.wp = fmt;
		info.out.wp = (wchar_t *)m_data.m_pData;
		info.limit = (int)(m_data.m_capacity / cs);
		info.nulterminate = true;
		info.outputtype = (int)cs;
		info.pMemoryManager = m_data.m_pMemoryManager;
		info.pBuffer = info.out.wp;

		sw_internal_doprintf(&info, ap);

		m_length = info.outpos;
		m_data.m_capacity = info.limit * cs;
		m_data.m_pData = (sw_byte_t *)info.pBuffer;

		return *this;
		}


SWString &
SWString::format(sw_uint32_t id, ...)
		{
		SWString	fmt;

		fmt.load(id);

		va_list ap;
		va_start(ap, id);

		if (fmt.charSize() == sizeof(wchar_t)) formatv(fmt.w_str(), ap);
		else formatv(fmt.c_str(), ap);

		va_end(ap);

		return *this;
		}



SWString &
SWString::format(const char *fmt, ...)
		{
		va_list ap;

		va_start(ap, fmt);
		formatv(fmt, ap);
		va_end(ap);

		return *this;
		}


SWString &
SWString::format(const wchar_t *fmt, ...)
		{
		va_list ap;

		va_start(ap, fmt);
		formatv(fmt, ap);
		va_end(ap);

		return *this;
		}


SWString &
SWString::load(sw_uint32_t id)
		{
		clear();

		SWResource::loadString(id, *this);

		return *this;
		}


// sp - pointer to arbitary string (variable char size)
// cs - char size in bytes
// base - numeric base
// uflag - unsigned number expected flag
unsigned long
SWString::convertStringToULong(const void *sp, int cs, int base, bool uflag)
		{
		SWStringPtr		strp(sp, cs);
		unsigned long	res=0, vc, maxval;
		int				c;
		bool			negative=false, overflow=false;

		for (;;)
			{
			c = *strp;
			if (c == 0 || !isSpaceChar(c)) break;
			strp++;
			}

		if (c == '-')
			{
			negative = true;
			strp++;
			}
		else if (c == '+')
			{
			strp++;
			}

		if (base == 0)
			{
			c = *strp;
			if (c == '0')
				{
				strp++;
				c = *strp;
				if (c == 'x' || c == 'X')
					{
					base = 16;
					strp++;
					}
				else base = 8;
				}
			else base = 10;
			}
		
		if (base == 16)
			{
			c = *strp;
			if (c == '0')
				{
				if (strp[1] == 'x')
					{
					// Skip the "0x"
					strp += 2;
					}
				}
			}

		if (base >= 2 && base <= 36)
			{
			// if our number exceeds this, we will overflow on multiply
			maxval = SW_ULONG_MAX / base;
			
			for (;;)
				{
				c = *strp;
				if (c == 0) break;

				strp++;
				if (c >= '0' && c <= '9') vc = c - '0';
				else if (c >= 'a' && c <= 'z') vc = c - 'a' + 10;
				else if (c >= 'A' && c <= 'Z') vc = c - 'A' + 10;
				else break;

				if ((int)vc >= base) break;

                // compute number = number * base + digval
                // with a pre-check for overflow situation
                if (!(res < maxval || (res == maxval && (unsigned long)vc <= (SW_ULONG_MAX % base))))
					{
                    overflow = true;
                    break;
					}
                				
                res = (res * base) + vc;
				}
			
			if (overflow || (!uflag && ((negative && res > (unsigned long)(SW_LONG_MAX)+1) || (!negative && res > SW_LONG_MAX))))
				{
				if (uflag) res = SW_ULONG_MAX;
				else if (negative)
					{
#if defined(_MSC_VER)
	#pragma warning(disable: 4146)	// unary minus operator applied to unsigned type, result still unsigned
#endif
					res = (unsigned long)(SW_LONG_MIN)+1;
#if defined(_MSC_VER)
	#pragma warning(default: 4146)	// unary minus operator applied to unsigned type, result still unsigned
#endif
					}
				else res = SW_LONG_MAX;
				}
			else if (negative) res = (unsigned long)-(long)res;
			}

		return res;
		}


// sp - pointer to arbitary string (variable char size)
// cs - char size in bytes
// base - numeric base
// uflag - unsigned number expected flag
sw_uint64_t
SWString::convertStringToUInt64(const void *sp, int cs, int base, bool uflag)
		{
		SWStringPtr		strp(sp, cs);
		sw_uint64_t		res=0, vc, maxval;
		int				c;
		bool			negative=false, overflow=false;

		for (;;)
			{
			c = *strp;
			if (c == 0 || !isSpaceChar(c)) break;
			strp++;
			}

		if (c == '-')
			{
			negative = true;
			strp++;
			}
		else if (c == '+')
			{
			strp++;
			}

		if (base == 0)
			{
			c = *strp;
			if (c == '0')
				{
				strp++;
				c = *strp;
				if (c == 'x' || c == 'X')
					{
					base = 16;
					strp++;
					}
				else base = 8;
				}
			else base = 10;
			}
		
		if (base == 16)
			{
			c = *strp;
			if (c == '0')
				{
				if (strp[1] == 'x')
					{
					// Skip the "0x"
					strp += 2;
					}
				}
			}

		if (base >= 2 && base <= 36)
			{
			// if our number exceeds this, we will overflow on multiply
			maxval = SW_UINT64_MAX / base;
			
			for (;;)
				{
				c = *strp;
				if (c == 0) break;

				strp++;
				if (c >= '0' && c <= '9') vc = c - '0';
				else if (c >= 'a' && c <= 'z') vc = c - 'a' + 10;
				else if (c >= 'A' && c <= 'Z') vc = c - 'A' + 10;
				else break;

				if ((int)vc >= base) break;

                // compute number = number * base + digval
                // with a pre-check for overflow situation
                if (!(res < maxval || (res == maxval && (sw_uint64_t)vc <= (SW_UINT64_MAX % base))))
					{
                    overflow = true;
                    break;
					}
                				
                res = (res * base) + vc;
				}
			
			if (overflow || (!uflag && ((negative && res > (sw_uint64_t)(SW_INT64_MAX)+1) || (!negative && res > SW_INT64_MAX))))
				{
				if (uflag) res = SW_UINT64_MAX;
				else if (negative) res = (sw_uint64_t)(SW_INT64_MAX)+1;
				else res = SW_INT64_MAX;
				}
			else if (negative) res = (sw_uint64_t)-(sw_int64_t)res;
			}

		return res;
		}




sw_int32_t
SWString::toInt32(const char *str, int base)
		{
		return (sw_int32_t)convertStringToULong(str, sizeof(*str), base, false);
		}


sw_int32_t
SWString::toInt32(const wchar_t *str, int base)
		{
		return (sw_int32_t)convertStringToULong(str, sizeof(*str), base, false);
		}



sw_uint32_t
SWString::toUInt32(const char *str, int base)
		{
		return (sw_uint32_t)convertStringToULong(str, sizeof(*str), base, true);
		}


sw_uint32_t
SWString::toUInt32(const wchar_t *str, int base)
		{
		return (sw_uint32_t)convertStringToULong(str, sizeof(*str), base, true);
		}



sw_int64_t
SWString::toInt64(const char *str, int base)
		{
		return (sw_int64_t)convertStringToUInt64(str, sizeof(*str), base, false);
		}


sw_int64_t
SWString::toInt64(const wchar_t *str, int base)
		{
		return (sw_int64_t)convertStringToUInt64(str, sizeof(*str), base, false);
		}



sw_uint64_t
SWString::toUInt64(const char *str, int base)
		{
		return (sw_uint64_t)convertStringToUInt64(str, sizeof(*str), base, true);
		}


sw_uint64_t
SWString::toUInt64(const wchar_t *str, int base)
		{
		return (sw_uint64_t)convertStringToUInt64(str, sizeof(*str), base, true);
		}


double
SWString::toDouble(const char *str)
		{
		return atof(str);
		}


double
SWString::toDouble(const wchar_t *str)
		{
		double	v;

		v = wtof((const wchar_t *)str);

		return v;
		}



sw_int32_t
SWString::toInt32(int base) const
		{
		sw_int32_t	v=0;

		if (charSize() == sizeof(wchar_t)) v = toInt32(w_str(), base);
		else v = toInt32(c_str(), base);

		return v;
		}


sw_uint32_t
SWString::toUInt32(int base) const
		{
		sw_uint32_t	v=0;

		if (charSize() == sizeof(wchar_t)) v = toUInt32(w_str(), base);
		else v = toUInt32(c_str(), base);

		return v;
		}


sw_int64_t
SWString::toInt64(int base) const
		{
		sw_int64_t	v=0;

		if (charSize() == sizeof(wchar_t)) v = toInt64(w_str(), base);
		else v = toInt64(c_str(), base);

		return v;
		}


sw_uint64_t
SWString::toUInt64(int base) const
		{
		sw_uint64_t	v=0;

		if (charSize() == sizeof(wchar_t)) v = toUInt64(w_str(), base);
		else v = toUInt64(c_str(), base);

		return v;
		}


double
SWString::toDouble() const
		{
		double	v=0;

		if (charSize() == sizeof(wchar_t)) v = toDouble(w_str());
		else v = toDouble(c_str());

		return v;
		}



sw_uint32_t
SWString::hash() const
		{
		return hash(exact);
		}


sw_uint32_t
SWString::hash(caseCompare cmp) const
		{
		sw_uint32_t		hv=0;
		sw_size_t		charsize=charSize();
		int				pos, hlen, v=0, nbits, mask;
		int				c;

		pos = 0;
		hlen = sizeof(hv)*8;
		while (hlen > 0 && pos < (int)length())
			{
			c = bufCharAt(pos++);
			if (cmp != exact) c = tolower(c);

			nbits = 6;
			mask = 0x3f;

			if (c >= 'A' && c <= 'Z') v = c-'A';		// 00..25
			else if (c >= 'a' && c <= 'z') v = 26+c-'a';	// 26..51
			else if (c >= '0' && c <= '9') v = 52+c-'0';	// 52..61
			else if (c < 128) v = 62;				// 62
			else if (charsize == 1)
				{
				v = 63;					// 63
				}
			else
				{
				// Use the value to determine how many bits we use
				if (c < 0) c = -c;

				mask = 0xffffffff;
				nbits = 0;

				while ((c & mask) != 0)
					{
					mask <<= 1;
					nbits++;
					}

				if (nbits) mask = ~mask;
				}

			// printf("%c -> %c = %x", *dp, c, v);

			if (nbits)
				{
				if (hlen < nbits)
					{
					hv <<= hlen;
					v >>= nbits-hlen;
					}
				else
					{
					hv <<= nbits;
					hlen -= nbits;
					}

				// printf(" (%x -> ", hv);
				hv |= (v & mask);
				// printf("%x)\n", hv);
				}
			}

		return hv;
		}



SWString &
SWString::toLower()
		{
		sw_size_t		cs=charSize();

		if (cs == sizeof(char))
			{
			char		*sp=reinterpret_cast<char *>(m_data.data());
			sw_size_t	len=m_length;

			while (len > 0)
				{
				*sp = (char)tolower(*sp);
				sp++;
				len--;
				}
			}
		else if (cs == sizeof(wchar_t))
			{
			wchar_t		*sp=reinterpret_cast<wchar_t *>(m_data.data());
			sw_size_t	len=m_length;

			while (len > 0)
				{
				*sp = (wchar_t)tolower(*sp);
				sp++;
				len--;
				}
			}

		return *this;
		}


SWString &
SWString::toUpper()
		{
		sw_size_t		cs=charSize();

		if (cs == sizeof(char))
			{
			char		*sp=reinterpret_cast<char *>(m_data.data());
			sw_size_t	len=m_length;

			while (len > 0)
				{
				*sp = (char)toupper(*sp);
				sp++;
				len--;
				}
			}
		else if (cs == sizeof(wchar_t))
			{
			wchar_t		*sp=reinterpret_cast<wchar_t *>(m_data.data());
			sw_size_t	len=m_length;

			while (len > 0)
				{
				*sp = (wchar_t)toupper(*sp);
				sp++;
				len--;
				}
			}

		return *this;
		}


// Read a line from the file
bool
SWString::readLine(FILE *f, bool stripNL)
		{
		int		c;

		clear();
		for (;;)
			{
			c = getc(f);
			if (c == '\r' && stripNL) continue;
			if (c == '\n')
				{
				if (!stripNL) *this += c;
				break;
				}
			else if (c == EOF)
				{
				if (isEmpty()) return false;
				break;
				}
			else *this += c;
			}

		return true;
		}



bool
SWString::readLine(SWTextFile &f, bool stripNL)
		{
		return (f.readLine(*this, stripNL) == SW_STATUS_SUCCESS);
		}



#if defined(SW_BUILD_MFC)
SWString::SWString(const CString &s)
		{
		init();
		if (s.GetLength()) bufCopy((const sw_tchar_t *)s, s.GetLength(), sizeof(sw_tchar_t));
		}


SWString::operator CString() const
		{
		CString	tmp=(const sw_tchar_t *)(*this);
		return tmp;
		}


SWString &
SWString::operator=(const CString &str)
		{
		return operator=((const sw_tchar_t *)str);
		}


SWString &
SWString::operator+=(const CString &str)
		{
		return operator+=((const sw_tchar_t *)str);
		}

int
SWString::compareTo(const CString &other, caseCompare cc) const
		{
		return compareTo((const sw_tchar_t *)other, cc);
		}


#endif


const sw_tchar_t *
SWString::t_str() const
		{
#if defined(SW_BUILD_UNICODE)
		return w_str();
#else
		return c_str();
#endif
		}


sw_status_t
SWString::writeToStream(SWOutputStream &stream) const
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		// We write a string as follows:
		// CharSize (1 byte)
		// String Length in characters (not chars) (4 bytes)
		// String Data
		sw_uint32_t		datalen=(sw_uint32_t)m_length;
		sw_uint8_t		cs=(sw_uint8_t)charSize();

		res = stream.write(cs);
		if (res == SW_STATUS_SUCCESS) res = stream.write(datalen);
		if (res == SW_STATUS_SUCCESS && datalen != 0 && cs != 0) res = stream.writeData(m_data, datalen * cs);

		return res;
		}


sw_status_t
SWString::readFromStream(SWInputStream &stream)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		sw_uint32_t	charcount=0;
		sw_uint8_t	charsize=0;

		res = stream.read(charsize);
		if (res == SW_STATUS_SUCCESS) res = stream.read(charcount);

		// wchar_t may be 2 or 4 bytes depending on platform.
		if (res == SW_STATUS_SUCCESS && charsize != 1 && charsize != 2 && charsize != 4 && charcount != 0)
			{
			// We have a corrupted data stream
			res = SW_STATUS_INVALID_DATA;
			}

		if (res == SW_STATUS_SUCCESS)
			{
			readyForUpdate();
			empty();

			// printf("SWString::readFromStream - %d x %d byte chars\n", charcount, charsize);

			if (charcount > 0)
				{
				if (isFixedCharSize() && charsize != charSize())
					{
					// We must read into a temporary buffer and then do an assignment
					if (charsize == sizeof(char))
						{
						char	*buf;

						buf = new char[charcount+1];
						res = stream.readData(buf, charcount * charsize);
						buf[charcount] = 0;
						*this = SWString(buf, charcount, charsize);
						delete buf;
						}
					else if (charsize == sizeof(wchar_t))
						{
						wchar_t	*buf;

						buf = new wchar_t[charcount+1];
						res = stream.readData(buf, charcount * charsize);
						buf[charcount] = 0;
						*this = SWString(buf, charcount, charsize);
						delete buf;
						}
					else if (charsize == 2)
						{
						// Convert to wchar_t first
						sw_int16_t	*ibuf;
						wchar_t		*buf;

						ibuf = new sw_int16_t[charcount+1];
						buf = new wchar_t[charcount+1];
						res = stream.readData(ibuf, charcount * charsize);

						for (sw_uint32_t i=0;i<charcount;i++) buf[i] = ibuf[i];
						buf[charcount] = 0;
						*this = SWString(buf, charcount, sizeof(wchar_t));
						delete buf;
						delete ibuf;
						}
					else if (charsize == 4)
						{
						// Convert to wchar_t first
						sw_int32_t	*ibuf;
						wchar_t		*buf;

						ibuf = new sw_int32_t[charcount+1];
						buf = new wchar_t[charcount+1];
						res = stream.readData(ibuf, charcount * charsize);

						for (sw_uint32_t i=0;i<charcount;i++) buf[i] = ibuf[i];
						buf[charcount] = 0;
						*this = SWString(buf, charcount, sizeof(wchar_t));
						delete buf;
						delete ibuf;
						}
					}
				else if (charsize != 1 && charsize != sizeof(wchar_t))
					{
					if (charsize == 2)
						{
						// Convert to wchar_t first
						sw_int16_t	*ibuf;
						wchar_t		*buf;

						ibuf = new sw_int16_t[charcount+1];
						buf = new wchar_t[charcount+1];
						res = stream.readData(ibuf, charcount * charsize);

						for (sw_uint32_t i=0;i<charcount;i++) buf[i] = ibuf[i];
						buf[charcount] = 0;
						*this = SWString(buf, charcount, sizeof(wchar_t));
						delete buf;
						delete ibuf;
						}
					else if (charsize == 4)
						{
						// Convert to wchar_t first
						sw_int32_t	*ibuf;
						wchar_t		*buf;

						ibuf = new sw_int32_t[charcount+1];
						buf = new wchar_t[charcount+1];
						res = stream.readData(ibuf, charcount * charsize);

						for (sw_uint32_t i=0;i<charcount;i++) buf[i] = ibuf[i];
						buf[charcount] = 0;
						*this = SWString(buf, charcount, sizeof(wchar_t));
						delete buf;
						delete ibuf;
						}
					}
				else
					{
					setCharSize(charsize);
					
					// We can do an assignment directly into our buffer
					ensureCapacity(charcount, false);

					res = stream.readData(m_data, charcount * charsize);
					if (res == SW_STATUS_SUCCESS)
						{
						// Setup the string parameters
						bufSetChars(0, charcount, 1, false, false);
						m_length = charcount;
						}
					else
						{
						// Clear out the string for safety
						empty();
						}
					}
				}
			}

		return res;
		}

SWString::operator const char *() const				{ return c_str(); } /// Cast operator (const char *)
SWString::operator const wchar_t *() const			{ return w_str(); } /// operator (const wchar_t *)


// Index operator - Returns the <i>i</i>th character of the string
int
SWString::operator[](int i) const
		{
		return getCharAt(i);
		}


// Index operator - Returns the <i>i</i>th character of the string
int
SWString::operator()(int i) const
		{
		return getCharAt(i);
		}


// Substring operator - Returns a string which is part of the current string
SWString
SWString::operator()(int pos, int len) const
		{
		return substring(pos, pos + len - 1);
		}



// Adds quotes to the string according to parameters.
void
SWString::quote(int qchar, bool onlyIfWhiteSpace, int backslash)
		{
		readyForUpdate();

		int				len=(int)length();

		if (len > 1 && bufCharAt(0) == qchar && bufCharAt(len-1) == qchar)
			{
			// Already quoted - don't bother
			return;
			}

		if (onlyIfWhiteSpace && firstOf(" \t\n\r") < 0)
			{
			// No white space and not always wanted - don't bother
			return;
			}

		SWString	qstr;

		qstr = qchar;

		// Now we know we must insert the quotes
		// but before we do check to see if we must quote
		// any embedded quotes with the given backslash char
		if (backslash && first(qchar) >= 0)
			{
			SWString	tmp;

			tmp = backslash;
			tmp += qchar;

			replaceAll(qstr, tmp);
			}

		insert(0, qstr);
		append(qstr);
		}


// Remove start and end quotes from the string.
void
SWString::unquote(int qchar, int backslash)
		{
		readyForUpdate();

		int				len=(int)length();

		if (len > 1 && bufCharAt(0) == qchar && bufCharAt(len-1) == qchar)
			{
			remove(len-1, 1);
			remove(0, 1);
			}

		// Now we know we must insert the quotes
		// but before we do check to see if we must quote
		// any embedded quotes with the given backslash char
		if (backslash)
			{
			SWString	qstr, tmp;

			qstr = qchar;
			tmp = backslash;
			tmp += qchar;

			replaceAll(tmp, qstr);
			}
		}


// Returns a quoted version of the string
SWString
SWString::toQuotedString(int qchar, bool onlyIfWhiteSpace, int backslash) const
		{
		SWString	s(*this);

		s.quote(qchar, onlyIfWhiteSpace, backslash);

		return s;
		}


// Returns a unquoted version of the string
SWString
SWString::toUnquotedString(int qchar, int backslash) const
		{
		SWString	s(*this);

		s.unquote(qchar, backslash);

		return s;
		}


SWString
SWString::quote(const SWString &str, int qchar, bool onlyIfWhiteSpace, int backslash)
		{
		SWString	s(str);

		s.quote(qchar, onlyIfWhiteSpace, backslash);

		return s;
		}


SWString
SWString::unquote(const SWString &str, int qchar, int backslash)
		{
		SWString	s(str);

		s.unquote(qchar, backslash);

		return s;
		}


// Returns a version of str where all upper-case characters have been replaced with lower-case characters. Uses
// the standard C library function tolower(). This function is incompatible with MBCS strings.
SWString
SWString::toLower(const SWString &str)
		{
		SWString	tmp=str;

		tmp.toLower();

		return tmp;
		}


// Returns a version of str where all lower-case characters have been replaced with upper-case characters. Uses
// the standard C library function toupper(). This function is incompatible with MBCS strings. 
SWString
SWString::toUpper(const SWString &str)
		{
		SWString	tmp = str;

		tmp.toUpper();

		return tmp;
		}


// Returns TRUE if self contains no bytes with the high bit set.
bool
SWString::isAscii() const
		{
		bool	res=true;
		int		i, c;
		
		for (i=0;i<(int)m_length;i++)
			{
			c = bufCharAt(i);

			if (c < 0 || c > 127)
				{
				res = false;
				break;
				}
			}

		return res;
		}


// Reverse the order of the chars in the string
SWString &
SWString::reverse()
		{
		readyForUpdate();

		size_t		l=length();
		size_t		cs=charSize();

		if (cs == sizeof(char))
			{
			char	c, *sp, *ep;

			sp = m_data;
			ep = sp+l-1;

			while (sp < ep)
				{
				c = *sp;
				*sp = *ep;
				*ep = c;
				sp++;
				ep--;
				}
			}
		else if (cs == sizeof(wchar_t))
			{
			wchar_t	c, *sp, *ep;

			sp = m_data;
			ep = sp+l-1;

			while (sp < ep)
				{
				c = *sp;
				*sp = *ep;
				*ep = c;
				sp++;
				ep--;
				}
			}

		return *this;
		}


// Remove all occurences of the specified char and return the count
SWString &
SWString::removeAll(int c)
		{
		readyForUpdate();
		
		int	count=0, index;

		while ((index = first(c)) >= 0)
			{
			remove(index, 1);
			count++;
			}
		
		return *this;
		}


//Changes the length of self to n bytes, adding blanks or truncating as necessary.
SWString &
SWString::resize(size_t nchars, int pad)
		{
		if (nchars < length())
			{
			// Truncate
			remove((int)nchars);
			}
		else if (nchars > length())
			{
			if (pad == 0)
				{
				capacity(nchars);
				}
			else
				{
				// Grow (add blanks)
				append(pad, nchars-length()); 
				}
			}

		return *this;
		}


SWString	SWString::valueOf(const SWValue &v)	{ return v.toString(); }
SWString	SWString::valueOf(bool v)				{ SWValue val(v); return val.toString(); }
SWString	SWString::valueOf(sw_int8_t v)		{ SWValue val(v); return val.toString(); }
SWString	SWString::valueOf(sw_int16_t v)		{ SWValue val(v); return val.toString(); }
SWString	SWString::valueOf(sw_int32_t v)		{ SWValue val(v); return val.toString(); }
SWString	SWString::valueOf(sw_int64_t v)		{ SWValue val(v); return val.toString(); }
SWString	SWString::valueOf(sw_uint8_t v)		{ SWValue val(v); return val.toString(); }
SWString	SWString::valueOf(sw_uint16_t v)		{ SWValue val(v); return val.toString(); }
SWString	SWString::valueOf(sw_uint32_t v)		{ SWValue val(v); return val.toString(); }
SWString	SWString::valueOf(sw_uint64_t v)		{ SWValue val(v); return val.toString(); }
SWString	SWString::valueOf(float v)			{ SWValue val(v); return val.toString(); }
SWString	SWString::valueOf(double v)			{ SWValue val(v); return val.toString(); }


// Expand all environment variables in the given string
void
SWString::expandEnvVars(int flags)
		{
		SWString		var;
		const sw_tchar_t *	value;
		int				p, q;
		int				startpos=0;

		while (startpos < (int)length())
			{
			if ((p = indexOf("$", startpos)) >= 0 && ((flags & unixVarTypes) != 0))
				{
				if (p > startpos && (*this)[p-1] == '\\')
					{
					// Ignore \$
					remove(p-1, 1);
					startpos = p;
					continue;
					}
				remove(p, 1);
				if (p >= (int)length()) break;
				if ((*this)[p] == '$')
					{
					// Substitute pid for $$
					char	tmp[10];

					remove(p, 1);
					sprintf(tmp, "%d", sw_process_self());
					insert(p, tmp);
					startpos = p;
					continue;
					}
				else if ((*this)[p] == _T('{'))
					{
					// Expand ${ENV}

					remove(p, 1);
					if (p >= (int)length()) break;

					q = indexOf("}", p);
					if (q < 0) q = (int)length();	 // A malformed string ${ENV (no closing brace) so we'll do our best
					else remove(q, 1);
					}
				else
					{
					//Expand $ENV
					// Search until we find a non-alphanumeric or non-underscore TCHAR
					for (q=p;q<(int)length()&&(isalnum((*this)[q])||(*this)[q]=='_');q++)
						{
						// Do nothing
						}
					}
				var = (*this)(p, q-p);
				if ((value = t_getenv(var)) == NULL) value = _T("");

				int	l1 = (int)length();
				replace(p, q-p, value);
				int	l2 = (int)length();
				startpos = q + l2 - l1;
				}
			else if ((p = indexOf("%", startpos)) >= 0 && ((flags & windowsVarTypes) != 0))
				{
				if (p > startpos && (*this)[p-1] == '\\')
					{
					// Ignore \$
					remove(p-1, 1);
					startpos = p;
					continue;
					}
				remove(p, 1);
				if (p >= (int)length()) break;
				if ((*this)[p] == '%')
					{
					// Just a percent char so leave it there
					startpos = p;
					continue;
					}
				else
					{
					// Expand %ENV%
					if (p >= (int)length()) break;

					q = indexOf("%", p);
					if (q < 0) q = (int)length();	 // A malformed string %ENV (no closing percent) so we'll do our best
					else remove(q, 1);
					}

				var = (*this)(p, q-p);
				if ((value = t_getenv(var)) == NULL) value = _T("");

				int	l1 = (int)length();
				replace(p, q-p, value);
				int	l2 = (int)length();
				startpos = q + l2 - l1;
				}
			else
				{
				break;
				}
			}
		}




// Remove all occurences of the specified char and return the count
int
SWString::Remove(sw_tchar_t c)
		{
		int	count=0, index;

		while ((index = first(c)) >= 0)
			{
			remove(index, 1);
			count++;
			}

		return count;
		}


bool
SWString::isSpaceChar(int c)
		{
		return (c == ' ' || (c >= 0x9 && c <= 0xd));
		}


bool
SWString::isAscii(const char *s)
		{
		bool	res=true;

		while (res && *s)
			{
			int		c=*s++;

			if (c < 0 || c > 127) res = false;
			}

		return res;
		}




