/**
*** A collection of functions for tracking memory leaks
*** and buffer overruns
**/

/*********************************************************************************
Copyright (c) 2006-2016 SwiftTec Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#ifndef __sw_memory_h__
#define __sw_memory_h__

// Override the standard new and delete
#ifdef _DEBUG

#define SW_MEMORY_TRACE_NEW				0x0001
#define SW_MEMORY_TRACE_DELETE			0x0002
#define SW_MEMORY_CHECKBLOCKS_ON_NEW	0x0004
#define SW_MEMORY_CHECKBLOCKS_ON_DELETE	0x0008

SWIFT_DLL_EXPORT void *	sw_memory_addBlock(size_t size, int type, const char *fname, int line);
SWIFT_DLL_EXPORT void *	sw_memory_resizeBlock(void *ptr, size_t size, int type, const char *fname, int line);
SWIFT_DLL_EXPORT void	sw_memory_removeBlock(void *ptr, int type, const char *fname, int line);
SWIFT_DLL_EXPORT void	sw_memory_showBlockList();
SWIFT_DLL_EXPORT void	sw_memory_checkBlockList();
SWIFT_DLL_EXPORT void	sw_memory_setTraceFlags(int tflags);

inline void * __cdecl 
operator new(size_t size)
		{
		return sw_memory_addBlock(size, 1, "", 0);
		};


inline void * __cdecl 
operator new[](size_t size)
		{
		return sw_memory_addBlock(size, 2, "", 0);
		};


inline void * __cdecl 
operator new(size_t size, const char *file, int line)
		{
		return sw_memory_addBlock(size, 1, file, line);
		};


inline void * __cdecl 
operator new[](size_t size, const char *file, int line)
		{
		return sw_memory_addBlock(size, 2, file, line);
		};


inline void __cdecl
operator delete(void *p)
		{
		sw_memory_removeBlock(p, 1, "", 0);
		};

inline void __cdecl
operator delete[](void *p)
		{
		sw_memory_removeBlock(p, 2, "", 0);
		};

#undef SW_DEBUG_NEW
/*
#define SW_DEBUG_NEW				new(__FILE__, __LINE__)
#define SW_DEBUG_DELETE				delete
#define SW_DEBUG_MALLOC(size)		sw_memory_addBlock(size, 10, __FILE__, __LINE__)
#define SW_DEBUG_REALLOC(ptr, size)	sw_memory_resizeBlock(ptr, size, 10, __FILE__, __LINE__)
#define SW_DEBUG_FREE(ptr)			sw_memory_removeBlock(ptr, 10, __FILE__, __LINE__)
*/

#endif // _DEBUG

#endif // __sw_memory_h__
