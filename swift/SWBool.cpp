/**
*** @file		SWBool.cpp
*** @brief		A representation of date based on Julian day numbers
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWBool class.
**/

/*********************************************************************************
Copyright (c) 2006-2016 SwiftTec Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <swift/SWBool.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif

// Begin the namespace
SWIFT_BEGIN_NAMESPACE


// Construct with today's date
SWBool::SWBool(bool amount) :
	m_amount(amount)
		{
		}


SWBool::SWBool(const char *s)
		{
		*this = s;
		}


SWBool &
SWBool::operator=(const char *s)
		{
		switch (*s)
			{
			case 'y':
			case 'Y':
			case 't':
			case 'T':
				m_amount = true;
				break;

			default:
				if (*s >= '0' && *s <= '9') m_amount = (*s != '0');
				else m_amount = false;
				break;
			}

		return *this;
		}


SWBool::SWBool(const wchar_t *s)
		{
		*this = s;
		}


SWBool &
SWBool::operator=(const wchar_t *s)
		{
		switch (*s)
			{
			case 'y':
			case 'Y':
			case 't':
			case 'T':
				m_amount = true;
				break;

			default:
				if (*s >= '0' && *s <= '9') m_amount = (*s != '0');
				else m_amount = false;
				break;
			}

		return *this;
		}




SWBool::SWBool(const SWString &s)
		{
		*this = s;
		}


SWBool &
SWBool::operator=(const SWString &s)
		{
		if (s.charSize() == sizeof(char)) *this = s.c_str();
		else if (s.charSize() == sizeof(wchar_t)) *this = s.w_str();
		else m_amount = 0;

		return *this;
		}


SWString
SWBool::toString(const SWString &truestr, const SWString &falsestr) const
		{
		if (m_amount) return truestr;
		else return falsestr;
		}


// End the namespace
SWIFT_END_NAMESPACE
