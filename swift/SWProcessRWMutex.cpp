/**
*** @file		SWProcessRWMutex.cpp
*** @brief		A process reader-writer mutex implementation
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWProcessRWMutex class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <swift/SWProcessRWMutex.h>
#include <swift/SWGuard.h>
#include <swift/SWTString.h>
#include <swift/sw_rwlock.h>



// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




// Constructor
SWProcessRWMutex::SWProcessRWMutex(const SWString &name) :
	SWRWMutex(0),
	m_tguard(true),
	m_lockedForRead(false),
	m_lockedForWrite(false),
	m_hMutex(0)
		{
		sw_status_t	res=0;
		sw_rwlock_t	*pMutex;

		res = sw_rwlock_create(pMutex, SW_RWLOCK_PROCESS, name);
		if (res != SW_STATUS_SUCCESS)
			{
			if (res == SW_STATUS_OUT_OF_MEMORY) throw SW_EXCEPTION(SWMemoryAllocationException);
			else throw SW_EXCEPTION(SWMutexCreateException);
			}
		else m_hMutex = pMutex;
		}


// Destructor
SWProcessRWMutex::~SWProcessRWMutex()
		{
		sw_rwlock_destroy((sw_rwlock_t *)m_hMutex);
		}


// Acquire the mutex for reading
sw_status_t
SWProcessRWMutex::acquireReadLock(sw_uint64_t waitms)
		{
		SWMutexGuard	guard(m_tguard);
		sw_status_t		res=SW_STATUS_SUCCESS;

		if (hasLock()) throw SW_EXCEPTION(SWMutexAlreadyHeldException);

		res = sw_rwlock_acquire((sw_rwlock_t *)m_hMutex, SW_RWLOCK_READ, waitms);
		if (res == SW_STATUS_SUCCESS) m_lockedForRead = true;

		return res;
		}

// Acquire the mutex for writing
sw_status_t
SWProcessRWMutex::acquireWriteLock(sw_uint64_t waitms)
		{
		SWMutexGuard	guard(m_tguard);
		sw_status_t		res=SW_STATUS_SUCCESS;

		if (hasLock()) throw SW_EXCEPTION(SWMutexAlreadyHeldException);

		res = sw_rwlock_acquire((sw_rwlock_t *)m_hMutex, SW_RWLOCK_WRITE, waitms);
		if (res == SW_STATUS_SUCCESS) m_lockedForWrite = true;

		return res;
		}


// Release the mutex
sw_status_t
SWProcessRWMutex::release()
		{
		SWMutexGuard	guard(m_tguard);
		sw_status_t		res=SW_STATUS_SUCCESS;

		m_lockedForRead = m_lockedForWrite = false;
		res = sw_rwlock_release((sw_rwlock_t *)m_hMutex);

		return res;
		}


bool
SWProcessRWMutex::hasReadLock()
		{
		return m_lockedForRead;
		}

bool
SWProcessRWMutex::hasWriteLock()
		{
		return m_lockedForWrite;
		}


bool
SWProcessRWMutex::isLockedForRead()
		{
		bool	res;

		res = hasReadLock();
		if (!res)
			{
			// Try acquiring the write lock with 0ms timeout.
			// If we succeed then no one else has the mutex.
			if (acquireWriteLock(0) == SW_STATUS_SUCCESS)
				{
				release();
				res = false;
				}
			else
				{
				res = true;
				}
			}

		return res;
		}

bool
SWProcessRWMutex::isLockedForWrite()
		{
		bool	res;

		res = hasWriteLock();
		if (!res)
			{
			// Try acquiring the write lock with 0ms timeout.
			// If we succeed then no one else has the mutex.
			if (acquireWriteLock(0) == SW_STATUS_SUCCESS)
				{
				release();
				res = false;
				}
			else
				{
				res = true;
				}
			}

		return res;
		}




