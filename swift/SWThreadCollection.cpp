/**
*** @file		SWThreadCollection.cpp
*** @brief		A generic value object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWThreadCollection class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWThreadCollection.h>
#include <swift/SWString.h>
#include <swift/SWGuard.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




#define SW_TPFLAG_ADDTHREAD		0x00000001		///< threads can be added
#define SW_TPFLAG_REMOVETHREAD	0x00000002		///< threads can be removed
#define SW_TPFLAG_SHUTDOWN		0x80000000		///< Shutting down.


SWThreadCollectionEntry::SWThreadCollectionEntry(SWThread *pThread, bool delflag, SWThread::NotificationProc notify) :
	m_pThread(pThread),
	m_delflag(delflag),
	m_notify(notify)
		{
		}



SWThreadCollectionEntry::SWThreadCollectionEntry(const SWThreadCollectionEntry &other)
		{
		*this = other;
		}


SWThreadCollectionEntry::~SWThreadCollectionEntry()
		{
		if (m_delflag && m_pThread->stopped())
			{
			delete m_pThread;
			}
		}


SWThreadCollectionEntry &
SWThreadCollectionEntry::operator=(const SWThreadCollectionEntry &other)
		{
		m_delflag = other.m_delflag;
		m_pThread = other.m_pThread;
		m_notify = other.m_notify;

		return *this;
		}


bool
SWThreadCollectionEntry::operator==(const SWThreadCollectionEntry &other) const
		{
		return (m_pThread == other.m_pThread);
		}









SWThreadCollection::SWThreadCollection() :
	m_flags(0)
		{
		// Make this a synchronised object
		synchronize();

		m_flags = SW_TPFLAG_ADDTHREAD | SW_TPFLAG_REMOVETHREAD;
		}


SWThreadCollection::~SWThreadCollection()
		{
		shutdown();
		}


sw_status_t
SWThreadCollection::add(SWThread *pThread, bool delflag, SWThread::NotificationProc notify)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if ((m_flags & SW_TPFLAG_ADDTHREAD) != 0)
			{
			SWGuard	guard(this);

			// add the job to the internal queue.
			m_threadlist.add(SWThreadCollectionEntry(pThread, delflag, notify));
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}

		return res;
		}


sw_status_t
SWThreadCollection::remove(SWThread *pThread)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if ((m_flags & SW_TPFLAG_REMOVETHREAD) != 0)
			{
			SWGuard			guard(this);
			SWThreadCollectionEntry	search(SWThreadCollectionEntry(pThread, false, NULL));

			if (m_threadlist.contains(search))
				{
				if (!pThread->running())
					{
					m_threadlist.remove(search);
					}
				else
					{
					res = SW_STATUS_ILLEGAL_OPERATION;
					}
				}
			else
				{
				res = SW_STATUS_NOT_FOUND;
				}
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}


		return res;
		}


sw_status_t
SWThreadCollection::enable(Operation op, bool opval)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if ((m_flags & SW_TPFLAG_SHUTDOWN) == 0)
			{
			sw_uint32_t	flags=0;

			switch (op)
				{
				case AddThread:	flags = SW_TPFLAG_ADDTHREAD;		break;
				case RemoveThread:	flags = SW_TPFLAG_REMOVETHREAD;	break;
				}

			SWGuard	guard(this);

			if (opval)
				{
				m_flags |= flags;
				}
			else
				{
				m_flags &= ~flags;
				}

			res = (flags?SW_STATUS_SUCCESS:SW_STATUS_INVALID_PARAMETER);
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}

		return res;
		}


void
SWThreadCollection::waitUntilAllThreadsComplete()
		{
		SWIterator<SWThreadCollectionEntry>	it;
		SWThreadCollectionEntry				*pEntry;

		while (m_threadlist.size() > 0)
			{
			it = m_threadlist.iterator();
			while (it.hasNext())
				{
				pEntry = it.next();
				if (pEntry->thread()->stopped())
					{
					it.remove();
					}
				}

			if (m_threadlist.size() > 0)
				{
				SWThread::sleep(100);
				}
			}
		}


void
SWThreadCollection::shutdown()
		{
		// Set the shutdown flag and prevent further jobs from being added.
		m_flags |= SW_TPFLAG_SHUTDOWN;
		m_flags &= ~SW_TPFLAG_ADDTHREAD;

		SWIterator<SWThreadCollectionEntry>	it=m_threadlist.iterator();
		SWThreadCollectionEntry				*pEntry;

		while (it.hasNext())
			{
			pEntry = it.next();
			pEntry->thread()->stop();
			}

		waitUntilAllThreadsComplete();

		while (m_threadlist.size() > 0)
			{
			m_threadlist.removeHead();
			}
		}



