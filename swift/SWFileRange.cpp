/**
*** @file		SWFileRange.cpp
*** @brief		A class to hold the information about a range in a file
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWFileRange class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWFileRange.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





SWFileRange::SWFileRange() :
	m_offset(0),
	m_length(0)
		{
		}


SWFileRange::SWFileRange(sw_filepos_t offset, sw_filesize_t length) :
	m_offset(offset),
	m_length(length)
		{
		}


SWFileRange::SWFileRange(const SWFileRange &other) :
	SWObject()
		{
		*this = other;
		}


SWFileRange &
SWFileRange::operator=(const SWFileRange &other)
		{
		m_offset = other.m_offset;
		m_length = other.m_length;

		return *this;
		}


int
SWFileRange::compareTo(const SWFileRange &other) const
		{
		int		r=0;

		if (m_offset == other.m_offset)
			{
			if (m_length < other.m_length) r = -1;
			else if (m_length > other.m_length) r = 1;
			}
		else if (m_offset < other.m_offset) r = -1;
		else if (m_offset > other.m_offset) r = 1;

		return r;
		}




