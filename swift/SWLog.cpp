/**
*** @file		SWLog.cpp
*** @brief		A class to represent a single log message.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWLog class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <swift/SWLog.h>
#include <swift/SWLogStdioFileHandler.h>
#include <swift/SWLogSyslogHandler.h>

#if defined(SW_BUILD_LOG)
	#include <swift/SWLogConsoleHandler.h>
#endif

#include <swift/SWGuard.h>
#include <swift/SWTime.h>
#include <swift/SWThread.h>
#include <swift/SWTokenizer.h>
#include <swift/SWIterator.h>
#include <xplatform/sw_process.h>

#if !defined(SW_PLATFORM_WINDOWS)
	#if defined(SW_PLATFORM_SOLARIS)
		#include <sys/systeminfo.h>
	#endif
	#include <sys/utsname.h>
	#include <unistd.h>
	#include <stdarg.h>
#endif

#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif








SWLog::SWLog(const SWString &handlers) :
	m_pLogThread(0),
	m_autoflush(false),
	m_mutex(true)
		{
		synchronize(&m_mutex, false);

#if defined(SW_PLATFORM_WINDOWS)
		TCHAR	tmpstr[1024];
		DWORD	n=1024;

		GetComputerName(tmpstr, &n);
		m_hostname = tmpstr;

#else // defined(SW_PLATFORM_WINDOWS)
		struct utsname	u;

		if (uname(&u) >= 0)
			{
			m_hostname = u.nodename;
			}

	#if defined(SW_PLATFORM_SOLARIS)
		char	buf[1024];

		if (sysinfo(SI_HOSTNAME, buf, sizeof(buf)) > 0) m_hostname = buf;
	#endif

#endif // defined(SW_PLATFORM_WINDOWS)

		m_pid = sw_process_self();

		// Set up a log format.
		format(SW_LOG_FORMAT_DEFAULT);

		setHandler(handlers);

#if defined(SW_BUILD_MFC)
		bgMode(false);
#else
		bgMode(false);
#endif

		}


SWLog::~SWLog()
		{
		close();

		if (m_pLogThread != NULL)
			{
			m_pLogThread->shutdown();
			m_pLogThread->waitUntilStopped();
			delete m_pLogThread;
			m_pLogThread = NULL;
			}
		}



// Add the given function to the list of log function callbacks
sw_status_t
SWLog::setHandler(SWLogMessageHandler *pHandler, bool deleteFlag)
		{
		SWGuard	guard(this);

		// Flush any pending message before changing the handlers
		flush();

		removeAllHandlers();
		return addHandler(pHandler, deleteFlag);
		}


// Add the given function to the list of log function callbacks
sw_status_t
SWLog::addHandler(SWLogMessageHandler *pHandler, bool deleteFlag)
		{
		SWGuard	guard(this);

		// Flush any pending message before changing the handlers
		flush();

		// Record the owner information.
		pHandler->owner(this);

		sw_status_t	res = pHandler->open();
		if (res == SW_STATUS_SUCCESS)
			{
			SWLogMessageHandlerEntry	tmp(pHandler, deleteFlag);

			if (!m_handlers.contains(tmp)) m_handlers.add(tmp, SWList<SWLogMessageHandlerEntry>::ListTail);
			}
		else
			{
			// The handler open failed - remove it!
			if (deleteFlag) delete pHandler;
			}

		return res;
		}


// Remove the given function from the list of log function callbacks
void
SWLog::removeHandler(SWLogMessageHandler *pHandler)
		{
		SWLogMessageHandlerEntry	entry;
		SWGuard						guard(this);

		// Flush any pending messages before changing the handlers
		flush();

		m_handlers.remove(SWLogMessageHandlerEntry(pHandler, false), &entry);

		// Signal to the handler to close
		entry.handler()->close();

		if (entry.deleteFlag())
			{
			delete entry.handler();
			}
		}


// Remove the given function from the list of log function callbacks
void
SWLog::removeAllHandlers()
		{
		SWLogMessageHandlerEntry	entry;
		SWGuard						guard(this);

		// Flush any pending messages before changing the handlers
		flush();

		// Flush any pending messages before changing the handlers
		while (m_handlers.remove(SWList<SWLogMessageHandlerEntry>::ListHead, &entry))
			{
			// Signal to the handler to close
			entry.handler()->close();

			if (entry.deleteFlag())
				{
				delete entry.handler();
				}
			}
		}


// First form of message using printf like strings
void
SWLog::report(sw_uint32_t msgflags, sw_uint16_t category, const SWString &file, sw_int32_t lineno, sw_uint32_t id, sw_uint64_t feature, const char *fmt, ...)
		{
		sw_uint32_t	msgtype = (msgflags & SW_LOG_TYPE_MASK);

		if ((msgflags & SW_LOG_LEVEL_MASK) <= m_logLevel && (msgtype == 0 || (msgtype & m_typeMask) != 0) && (feature == 0 || (feature & m_featureMask) != 0))
			{
			SWString	str;
			va_list		ap; 

			va_start(ap, fmt); 
			str.formatv(fmt, ap);
			va_end(ap);

			SWTime			now(true);
			sw_thread_id_t		tid = SWThread::self();

			SWString		hostname;

#if defined(SW_PLATFORM_WINDOWS)
#else
#endif

			// Now call the SWLogMessage object form of the log method
			bool		done=false;
			SWString	substr;
			int			p;

			// Guard the next section to prevent the message parts from being separated
			SWGuard	guard(this);

			while (!done)
				{
				p = str.firstOf("\r\n");
				if (p < 0)
					{
					substr = str;
					str.empty();
					done = true;
					}
				else
					{
					substr = str.left(p);
					if (str[p] == '\r' && (p+1) < (int)str.length() && str[p+1] == '\n') p++;
					str.remove(0, p+1);
					done = str.isEmpty();
					}

				process(SWLogMessage(now, m_pid, tid, msgflags, category, m_hostname, m_appName, file, lineno, substr, id, feature, NULL, 0));
				}
			}
		}


void
SWLog::report(sw_uint32_t msgflags, sw_uint16_t category, const SWString &file, sw_int32_t lineno, sw_uint32_t id, sw_uint64_t feature, const wchar_t *fmt, ...)
		{
		sw_uint32_t	msgtype = (msgflags & SW_LOG_TYPE_MASK);

		if ((msgflags & SW_LOG_LEVEL_MASK) <= m_logLevel && (msgtype == 0 || (msgtype & m_typeMask) != 0) && (feature == 0 || (feature & m_featureMask) != 0))
			{
			SWString	str;
			va_list		ap; 

			va_start(ap, fmt); 
			str.formatv(fmt, ap);
			va_end(ap);

			SWTime			now(true);
			sw_thread_id_t		tid = SWThread::self();

			// Now call the SWLogMessage object form of the log method
			bool		done=false;
			SWString	substr;
			int			p;

			// Guard the next section to prevent the message parts from being separated
			SWGuard	guard(this);

			while (!done)
				{
				p = str.firstOf("\r\n");
				if (p < 0)
					{
					substr = str;
					str.empty();
					done = true;
					}
				else
					{
					substr = str.left(p);
					if (str[p] == '\r' && (p+1) < (int)str.length() && str[p+1] == '\n') p++;
					str.remove(0, p+1);
					done = str.isEmpty();
					}

				process(SWLogMessage(now, m_pid, tid, msgflags, category, m_hostname, m_appName, file, lineno, substr, id, feature, NULL, 0));
				}
			}
		}


void
SWLog::report(const SWLogMessage &msg)
		{
		process(msg);
		}


sw_status_t
SWLog::process(const SWLogMessage &msg)
		{
		SWGuard	guard(this);

		if (filter(msg))
			{
			if (m_pLogThread != NULL)
				{
				// If in background mode add the message to the 
				// log thread queue
				m_pLogThread->add(msg);
				}
			else
				{
				// In foreground mode call the send message method directly
				// from the calling thread to ensure synchronisation
				sendMessageToHandlers(msg);

				if (m_autoflush)
					{
					// The caller has requested we call flush every message is processed.
					flush();
					}
				}
			}

		return SW_STATUS_SUCCESS;
		}


// Send the message to all registered listeners.
// Called from SWLogThread::run and SWLog::message
void
SWLog::sendMessageToHandlers(const SWLogMessage &msg)
		{
		SWGuard									guard(this);	// Guard against multiple threads
		SWIterator<SWLogMessageHandlerEntry>	it;	

		it = m_handlers.iterator();
		while (it.hasNext())
			{
			it.next()->handler()->process(msg);
			}
		}


// Close the log file (if it is open)
sw_status_t
SWLog::close()
		{
		removeAllHandlers();

		return SW_STATUS_SUCCESS;
		}


sw_status_t
SWLog::open()
		{
		return SW_STATUS_SUCCESS;
		}


// flush the log file (if it is open)
sw_status_t
SWLog::flush()
		{
		SWGuard		guard(this);	// Guard against multiple threads

		if (m_pLogThread != NULL)
			{
			m_pLogThread->flush();
			}

		SWIterator<SWLogMessageHandlerEntry>	it;	

		it = m_handlers.iterator();
		while (it.hasNext())
			{
			it.next()->handler()->flush();
			}

		return SW_STATUS_SUCCESS;
		}



void
SWLog::bgMode(bool v)
		{
		SWGuard		guard(this);	// Guard against multiple threads

		// For now do nothing
		if (v && m_pLogThread == NULL)
			{
			flush();

			// Start the BG thread processing
			m_pLogThread = new SWLogThread(this);
			m_pLogThread->start();
			m_pLogThread->waitUntilStarted();
			}
		else if (!v && m_pLogThread != NULL)
			{
			flush();

			m_pLogThread->shutdown();
			delete m_pLogThread;
			m_pLogThread = NULL;
			}
		}



// Function
sw_status_t
SWLog::addHandler(const SWString &target)
		{
		SWGuard				guard(this);	// Guard against multiple threads
		sw_status_t			res=SW_STATUS_SUCCESS;
		SWString			fmt;
		SWLogMessageHandler	*pHandler=NULL;

		// Make sure all pending entries are flushed before continuing
		flush();

		if (target.startsWith("stdout:"))
			{
			pHandler = new SWLogStdioFileHandler(stdout, false);
			fmt = target.mid(7);
			}
		else if (target.startsWith("stderr:"))
			{
			pHandler = new SWLogStdioFileHandler(stderr, false);
			fmt = target.mid(7);
			}
		else if (target.startsWith("console:"))
			{
#if defined(SW_BUILD_LOG)
			pHandler = new SWLogConsoleHandler();
			fmt = target.mid(8);
#else // defined(SW_BUILD_LOG)
			pHandler = new SWLogStdioFileHandler(stdout, false);
			fmt = target.mid(7);
#endif // defined(SW_BUILD_LOG)
			}
		else if (target.startsWith("syslog:"))
			{
			pHandler = new SWLogSyslogHandler();
			fmt = target.mid(7);
			}
		else if (target.startsWith("file:"))
			{
			// Expect string "file:filename[;maxsize;backupcount]"
			// where maxsize and backup count are an optional pair
			SWTokenizer	tok(target.mid(5), ";");

			if (tok.getTokenCount() > 0)
				{
				SWString	filename;
				sw_size_t	maxsize=0;
				int			bcount=0;

				filename = tok[0];
				if (tok.getTokenCount() > 1) maxsize = atoi(tok[1]);
				if (tok.getTokenCount() > 2) bcount = atoi(tok[2]);
				if (tok.getTokenCount() > 3) fmt = tok[3];
				res = addHandler(new SWLogStdioFileHandler(filename, maxsize, bcount), true);
				}
			else
				{
				res = SW_STATUS_INVALID_PARAMETER;
				}
			}
		else 
			{
			res = SW_STATUS_INVALID_PARAMETER;
			}

		if (res == SW_STATUS_SUCCESS && pHandler != NULL)
			{
			if (!fmt.isEmpty())
				{
				// Set the handler local format
				pHandler->format(fmt);
				}

			res = addHandler(pHandler, true);
			}

		return res;
		}



// Set the logfile target(s)
// target will be a string of command separated values:
// stdout,stderr,syslog or an arbitary file name
sw_status_t
SWLog::setHandler(const SWString &handlers)
		{
		SWGuard		guard(this);	// Guard against multiple threads

		flush();
		removeAllHandlers();

		SWTokenizer	tok(handlers, ",");

		sw_status_t	res=SW_STATUS_SUCCESS;

		while (res == SW_STATUS_SUCCESS && tok.hasMoreTokens())
			{
			res = addHandler(tok.nextToken());
			}

		return res;
		}



#define DECLARE_LOG_METHOD_CTYPE(name, flags, ctype, rfmt, conv) \
void \
SWLog::name(const ctype *fmt, ...) \
		{ \
		SWString	str; \
		va_list ap; \
		va_start(ap, fmt); \
		str.formatv(fmt, ap); \
		va_end(ap); \
		report(flags, 0, "unknown", -1, 0, SW_CONST_UINT64(~0), rfmt, str.conv()); \
		} 

#define DECLARE_LOG_METHOD(name, flags) \
	DECLARE_LOG_METHOD_CTYPE(name, flags, char, "%s", c_str) \
	DECLARE_LOG_METHOD_CTYPE(name, flags, wchar_t, L"%ls", w_str)

DECLARE_LOG_METHOD(alert, SW_LOG_MSG_ALERT)
DECLARE_LOG_METHOD(emergency, SW_LOG_MSG_EMERGENCY)
DECLARE_LOG_METHOD(critical, SW_LOG_MSG_CRITICAL)
DECLARE_LOG_METHOD(notice, SW_LOG_MSG_NOTICE)
DECLARE_LOG_METHOD(success, SW_LOG_MSG_SUCCESS)
DECLARE_LOG_METHOD(fatal, SW_LOG_MSG_FATAL)
DECLARE_LOG_METHOD(error, SW_LOG_MSG_ERROR)
DECLARE_LOG_METHOD(warning, SW_LOG_MSG_WARNING)
DECLARE_LOG_METHOD(info, SW_LOG_MSG_INFO)
DECLARE_LOG_METHOD(detail, SW_LOG_MSG_DETAIL)
DECLARE_LOG_METHOD(debug, SW_LOG_MSG_DEBUG)
DECLARE_LOG_METHOD(trace, SW_LOG_MSG_TRACE)


#define DECLARE_LOG_METHOD_LEVEL_CTYPE(name, flags, ctype, rfmt, conv) \
void \
SWLog::name(sw_uint32_t level, const ctype *fmt, ...) \
		{ \
		SWString	str; \
		va_list ap; \
		va_start(ap, fmt); \
		str.formatv(fmt, ap); \
		va_end(ap); \
		report(level | flags, 0, "unknown", -1, 0, SW_CONST_UINT64(~0), rfmt, str.conv()); \
		} 

#define DECLARE_LOG_METHOD_LEVEL(name, flags) \
	DECLARE_LOG_METHOD_LEVEL_CTYPE(name, flags, char, "%s", c_str) \
	DECLARE_LOG_METHOD_LEVEL_CTYPE(name, flags, wchar_t, L"%ls", w_str)



DECLARE_LOG_METHOD_LEVEL(alert, SW_LOG_TYPE_ALERT)
DECLARE_LOG_METHOD_LEVEL(emergency, SW_LOG_TYPE_EMERGENCY)
DECLARE_LOG_METHOD_LEVEL(critical, SW_LOG_TYPE_CRITICAL)
DECLARE_LOG_METHOD_LEVEL(notice, SW_LOG_TYPE_NOTICE)
DECLARE_LOG_METHOD_LEVEL(success, SW_LOG_TYPE_SUCCESS)
DECLARE_LOG_METHOD_LEVEL(fatal, SW_LOG_TYPE_FATAL)
DECLARE_LOG_METHOD_LEVEL(error, SW_LOG_TYPE_ERROR)
DECLARE_LOG_METHOD_LEVEL(warning, SW_LOG_TYPE_WARNING)
DECLARE_LOG_METHOD_LEVEL(info, SW_LOG_TYPE_INFO)
DECLARE_LOG_METHOD_LEVEL(debug, SW_LOG_TYPE_DEBUG)



// for the SWObjectQueue mutex
SWLogThread::SWLogThread(SWLog *pOwner) :
	SWThread(SW_THREAD_TYPE_BOUND),
	m_pOwner(pOwner),
    m_cond(pOwner->m_mutex),
    m_logQ(pOwner->m_mutex)
		{
		}


SWLogThread::~SWLogThread()
		{
		shutdown();
		waitUntilStopped();
		}


int
SWLogThread::run()
		{
		m_flushFlag = false;

		while (runnable())
			{
			if (m_flushFlag)
				{
				int	sz = (int)m_logQ.size();

				if (sz == 0) 
					{
					m_flushFlag = false;
					m_cond.broadcast();
					}
				}

			try
				{
				SWLogMessage	*lm = (SWLogMessage *)m_logQ.get();

				m_pOwner->sendMessageToHandlers(*lm);
				delete lm;
				}
			catch (SWQueueInterruptedException &)
				{
				// Do nothing
				m_flushFlag = true;
				}
			catch (SWQueueInactiveException &)
				{
				// Time to quit the loop
				break;
				}
			}

		return 0;
		}


// Flush all pending messages.
void
SWLogThread::flush()
		{
		SWMutexGuard	guard(m_pOwner->m_mutex);

		try
			{
			if (m_logQ.size() != 0 && !m_flushFlag)
				{
				m_flushFlag = true;
				m_logQ.interrupt();
				while (m_flushFlag) m_cond.wait();
				}
			}
		catch (...)
			{
			}
		}


void
SWLogThread::add(const SWLogMessage &lm)
		{
		try
			{
			m_logQ.add(new SWLogMessage(lm));
			}
		catch (...)
			{
			}
		}



void
SWLogThread::shutdown()
		{
		SWMutexGuard	guard(m_pOwner->m_mutex);

		try
			{
			flush();
			m_logQ.shutdown();
			stop(false);
			}
		catch (...)
			{
			}
		}







