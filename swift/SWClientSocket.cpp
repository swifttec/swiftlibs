/**
*** @file		SWClientSocket.cpp
*** @brief		A generic socket implementation
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWClientSocket class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <swift/SWClientSocket.h>
#include <swift/SWSocket.h>
#include <swift/SWTcpSocket.h>
#include <swift/SWException.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




/// Default constructor
SWClientSocket::SWClientSocket() :
	_pSocket(NULL),
	_delFlag(false)
		{
		}


SWClientSocket::SWClientSocket(SWSocket *pSocket, bool delflag) :
	_pSocket(pSocket),
	_delFlag(delflag)
		{
		}

/// Virtual destructor
SWClientSocket::~SWClientSocket()
		{
		close();
		if (_delFlag)
			{
			delete _pSocket;
			_pSocket = 0;
			}
		}


/// Create a socket connection to a listening socket
sw_status_t
SWClientSocket::connect(const SWSocketAddress &addr)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (_pSocket == NULL)
			{
			// Pick an appropriate socket for the address family.
			switch (addr.type())
				{
				case SWSocketAddress::IP4:
					_pSocket = new SWTcpSocket();
					_delFlag = true;
					break;

				default:
					// Do nothing
					break;
				}
			}

		if (_pSocket != NULL)
			{
			if (_pSocket->address().family() == SWSocketAddress::Unknown || _pSocket->address().family() == addr.family())
				{
				res = _pSocket->connect(addr);
				}
			else
				{
				// The socket and address familes do not match
				res = SW_STATUS_INVALID_PARAMETER;
				}
			}
		else
			{
			// An invalid socket address was passed
			res = SW_STATUS_INVALID_PARAMETER;
			}

		return res;
		}


/// Close the socket
sw_status_t
SWClientSocket::close()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (isConnected()) res = _pSocket->close();

		return res;
		}


/// Read data from the socket
sw_status_t
SWClientSocket::read(void *pBuffer, sw_size_t nbytes, sw_size_t *pBytesRead)
		{
		sw_status_t	res;

		if (!isConnected())
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}
		else
			{
			res =  _pSocket->read(pBuffer, nbytes, pBytesRead);
			}

		return res;
		}


/// Write data to the socket
sw_status_t
SWClientSocket::write(const void *pBuffer, sw_size_t nbytes, sw_size_t *pBytesWritten)
		{
		sw_status_t	res;

		if (!isConnected())
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}
		else
			{
			res = _pSocket->write(pBuffer, nbytes, pBytesWritten);
			}

		return res;
		}


sw_status_t
SWClientSocket::waitForInput(sw_int64_t waitms)
		{
		sw_status_t	res;

		if (!isConnected())
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}
		else
			{
			res = _pSocket->waitForInput(waitms);
			}

		return res;
		}


/// Check if this socket is connected or not
bool
SWClientSocket::isConnected() const
		{
		bool	res=false;

		if (_pSocket != NULL) res = _pSocket->isConnected();

		return res;
		}


void
SWClientSocket::setSocket(SWSocket *pSocket, bool delflag)
		{
		if (_pSocket != NULL && pSocket != _pSocket && _delFlag)
			{
			// Delete the previously held socket
			delete _pSocket;
			}

		_pSocket = pSocket;
		_delFlag = delflag;
		}




