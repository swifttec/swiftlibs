/**
*** @file		SWHostname.cpp
*** @brief		Represents a hostname
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWHostname class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



// Needed for precompiled header support
#include "stdafx.h"

#include <swift/SWThreadMutex.h>

#include <swift/SWHostname.h>
#include <swift/sw_net.h>

#if !defined(SW_PLATFORM_WINDOWS)
	#include <netdb.h>
#else
	#if defined(SW_BUILD_MFC)
		#include <winsock2.h>
	#endif
#endif



// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




static bool
internal_copyhostent(struct hostent *hp, struct hostent *result, char *bp, int buflen)
		{
		int		i, len, naddrs, naliases;
		char		**sp;

		memset(result, 0, sizeof(struct hostent));
		result->h_addrtype = hp->h_addrtype;
		result->h_length = hp->h_length;

		// Count the aliases and addresses
		for (naddrs=0,sp=hp->h_addr_list;*sp;sp++) naddrs++;
		for (naliases=0,sp=hp->h_aliases;*sp;sp++) naliases++;

		// We assign space for the pointers from the buffer first
		// so they are properly aligned
		len = (naddrs + naliases + 2) * sizeof(char *);
		if (len > buflen) return false;

		memset(bp, 0, len);
		result->h_aliases = (char **)bp;
		bp += (naliases + 1) * sizeof(char *);
		result->h_addr_list = (char **)bp;
		bp += (naddrs + 1) * sizeof(char *);
		buflen -= naddrs + naliases;

		// char	**h_addr_list;
		for (i=0,sp=hp->h_addr_list;i<naddrs;i++,sp++)
			{
			len = hp->h_length;
			if (len > buflen) return false;
			result->h_addr_list[i] = bp;
			memcpy(bp, *sp, len);
			bp += len;
			buflen -= len;
			}

		result->h_addr_list[i] = NULL;

		// char	**h_aliases;
		for (i=0,sp=hp->h_aliases;i<naliases;i++,sp++)
			{
			len = (int)strlen(*sp) + 1;
			if (len > buflen) return false;
			result->h_aliases[i] = bp;
			memcpy(bp, *sp, len);
			bp += len;
			buflen -= len;
			}

		result->h_aliases[i] = NULL;

		// char *h_name
		len = (int)strlen(hp->h_name) + 1;
		if (len > buflen) return false;
		
		result->h_name = bp;
		memcpy(bp, hp->h_name, len);
		bp += len;
		buflen -= len;

		return true;
		}


//static SWThreadMutex	internal_gethostby_mutex;

struct hostent *
internal_gethostbyname_r(const char *name, struct hostent *result, char *buffer, int buflen, int *h_errnop)
		{
		if (!sw_net_initialised()) sw_net_init();

		struct hostent	*hp;

#if defined(SW_PLATFORM_WINDOWS) || defined(SW_PLATFORM_MACH)
		hp = gethostbyname(name);
		if (hp == NULL) 
			{
			*h_errnop = h_errno;
			return NULL;
			}

		// Copy the data and return a pointer to it
		if (!internal_copyhostent(hp, result, buffer, buflen))
			{
			*h_errnop = ERANGE;
			return NULL;
			}

		*h_errnop = 0;

		hp = result;
#else
		// Use the native one!
	#if defined(SW_PLATFORM_LINUX)
		if (::gethostbyname_r(name, result, buffer, buflen, &hp, h_errnop) != 0) hp = NULL;
	#elif defined(SW_PLATFORM_AIX)
		struct hostent_data	hdata;

		if (::gethostbyname_r(name, result, &hdata) != 0) hp = NULL;
	#else
		hp = ::gethostbyname_r(name, result, buffer, buflen, h_errnop);
	#endif
#endif

		return hp;
		}


static struct hostent *
internal_gethostbyaddr_r(const char *addr, int length, int type, struct hostent *result, char *buffer, int buflen, int *h_errnop)
		{
		if (!sw_net_initialised()) sw_net_init();

		struct hostent	*hp;

#if defined(SW_PLATFORM_WINDOWS) || defined(SW_PLATFORM_MACH)
		//SWMutexGuard	guard(internal_gethostby_mutex);

		hp = gethostbyaddr(addr, length, type);
		if (hp == NULL) 
			{
			*h_errnop = h_errno;
			return NULL;
			}

		// Copy the data and return a pointer to it
		if (!internal_copyhostent(hp, result, buffer, buflen))
			{
			*h_errnop = ERANGE;
			return NULL;
			}

		*h_errnop = 0;

		hp = result;
#else
		// Use the native one!
	#if defined(SW_PLATFORM_LINUX)
		if (gethostbyaddr_r(addr, length, type, result, buffer, buflen, &hp, h_errnop) != 0) hp = NULL;
	#elif defined(SW_PLATFORM_AIX)
		struct hostent_data	hdata;

		if (::gethostbyaddr_r((char *)addr, length, type, result, &hdata) != 0) hp = NULL;
	#else
		hp = gethostbyaddr_r(addr, length, type, result, buffer, buflen, h_errnop);
	#endif
#endif

		return hp;
		}



// re-define the standard gethostbyXXX calls so we don't accidentally use them
#define gethostbyname	use_internal_gethostbyname_r_instead_of_gethostbyname
#define gethostbyname_r	use_internal_gethostbyname_r_instead_of_gethostbyname_r
#define gethostbyaddr	use_internal_gethostbyaddr_r_instead_of_gethostbyaddr
#define gethostbyaddr_r	use_internal_gethostbyaddr_r_instead_of_gethostbyaddr_r


SWHostname::SWHostname() :
    _flags(0)
		{
		}


SWHostname::SWHostname(const SWHostname &other) :
	SWObject(),
    _flags(0)
    	{
		*this = other;
		}


SWHostname::SWHostname(const SWString &name) :
    _flags(0)
		{
		*this = name;
		}


SWHostname &
SWHostname::operator=(const SWString &name)
		{
		clear();
		getInfoForHost(name);
		return *this;
		}


SWHostname::SWHostname(const SWIPAddress &addr) :
    _name(addr.toString()),
    _flags(0)
		{
		*this = addr;
		}


void
SWHostname::getInfoForHost(const SWString &host)
		{
#if defined(WIN32) || defined(_WIN32)
		if (!sw_net_initialised()) sw_net_init();
#endif

		struct hostent	*hp=NULL, he;
		char			hbuf[1024];
		int				herrno;
		int				tries=0;
		
		clear();
		_name = host;
		_flags = 0;

		// First check for an ip address!
		if (SWIPAddress::isIPAddress(_name))
			{
			SWIPAddress	addr(_name);

/*
#if defined(linux)
			while (::gethostbyaddr_r(addr.data(), addr.length(), addr.family(), &he, hbuf, sizeof(hbuf), &hp, &herrno) != 0 && herrno == TRY_AGAIN && tries < 5)
#else
			while ((hp = ::gethostbyaddr_r(addr.data(), addr.length(), addr.family(), &he, hbuf, sizeof(hbuf), &herrno)) == NULL && herrno == TRY_AGAIN && tries < 5)
#endif
*/
			while ((hp = internal_gethostbyaddr_r(addr.data(), addr.length(), addr.family(), &he, hbuf, sizeof(hbuf), &herrno)) == NULL && herrno == TRY_AGAIN && tries < 5)
				{
				tries++;
				}

			_flags |= FLAG_IS_IPADDR | FLAG_IS_VALID;
			}
		else
			{
/*
#if defined(linux)
			while (::gethostbyname_r(_name, &he, hbuf, sizeof(hbuf), &hp, &herrno) != 0 && herrno == TRY_AGAIN && tries < 5)
#else
			while ((hp = ::gethostbyname_r(_name, &he, hbuf, sizeof(hbuf), &herrno)) == NULL && herrno == TRY_AGAIN && tries < 5)
#endif
*/
			while ((hp = internal_gethostbyname_r(_name, &he, hbuf, sizeof(hbuf), &herrno)) == NULL && herrno == TRY_AGAIN && tries < 5)
				{
				tries++;
				}
			}

		if (hp != NULL)
			{
			// Set the flags here to prevent recursion.
			_flags |= FLAG_IS_VALID;

			// We need to save the IP addr info since the hostent 
			// assignment will drop this from the flags.
			bool isip = ((_flags & FLAG_IS_IPADDR) != 0);

			setFromHostEntry(hp);

			_name = host;
			if (isip) _flags |= FLAG_IS_IPADDR;

			if ((_flags & FLAG_IS_IPADDR) == 0 && !isAlias(_name))
				{
				// If we we're given a name to start with and it's not one of the official
				// alias names we add it to the alias list anyway
				_aliases.add(_name);
				}

			}
		else if ((_flags & FLAG_IS_IPADDR) != 0)
			{
			// Do the best we can
			_ipaddrs.add(SWIPAddress(_name));
			}

		_flags |= FLAG_INFO_IS_VALID;
		}


SWHostname &
SWHostname::operator=(const SWIPAddress &addr)
		{
		clear();
		getInfoForHost(addr.toString());
		return *this;
		}


/**
*** Clears this hostname to be empty or invalid.
**/
void
SWHostname::clear()
		{
		_aliases.clear();
		_ipaddrs.clear();
		_name.empty();
		_address.clear();
		_flags = 0;
		}


void
SWHostname::setFromHostEntry(const void *pVEntry)
		{
		const struct hostent	*pEntry=(struct hostent *)pVEntry;
		char	**sp;

		clear();

		_name = pEntry->h_name;
		_hostname = pEntry->h_name;
		// _addrtype = pEntry->h_addrtype;
		// _addrlength = pEntry->h_length;

		// Add all the aliases to the alias array (strings)
		for (sp=pEntry->h_aliases;*sp;sp++)
			{
			_aliases.add(*sp);
			}

		// Add all the ipaddrs to the IP address array
		for (sp=pEntry->h_addr_list;*sp;sp++) 
			{
			_ipaddrs.add(SWIPAddress(*sp, pEntry->h_length, pEntry->h_addrtype));
			}

		// Set the primary IP address
		if (addressCount() > 0) _address = address(0);
		else _address.clear();

		// Now check all the names returned to see if there are any other
		// aliases we can derive.
		SWString		tmp;
		struct hostent	*hp, hent;
		char			hbuf[1024];
		int				herrno;
		int				p;

		if ((p = _name.first('.')) > 0)
			{
			tmp = _name.substring(0, p-1);
			int tries=0;
/*
#if defined(linux)
			while (::gethostbyname_r(tmp, &hent, hbuf, sizeof(hbuf), &hp, &herrno) != 0 && herrno == TRY_AGAIN && tries < 5)
#else
			while ((hp = ::gethostbyname_r(tmp, &hent, hbuf, sizeof(hbuf), &herrno)) == NULL && herrno == TRY_AGAIN && tries < 5)
#endif
*/
			while ((hp = internal_gethostbyname_r(tmp, &hent, hbuf, sizeof(hbuf), &herrno)) == NULL && herrno == TRY_AGAIN && tries < 5)
				{
				tries++;
				}

			if (hp != NULL)
				{
				// It's a valid name. Check to see if we've got it already
				for (sp=pEntry->h_addr_list;*sp;sp++) 
					{
					SWIPAddress	tmpaddr(*sp, pEntry->h_length, pEntry->h_addrtype);

					if (isAddress(tmpaddr) && !isAlias(tmp))
						{
						// It's a valid address for this host
						_aliases.add(_name);
						_name = tmp;
						}
					}

				}
			}

		_flags = FLAG_IS_VALID | FLAG_INFO_IS_VALID;
		}


/**
*** Assignment operator from another SWHostname
**/
SWHostname &
SWHostname::operator=(const SWHostname &h)
		{
		clear();

		_name = h._name;
		_aliases = h._aliases;

		for (int i=0;i<(int)h._ipaddrs.size();i++)
			{
			_ipaddrs.add(SWIPAddress(h._ipaddrs[i]));
			}

		_flags = h._flags;

		return *this;
		}


bool
SWHostname::isAddress(const SWIPAddress &addr) const
		{
		int	i, n=(int)_ipaddrs.size();

		for (i=0;i<n;i++)
			{
			if (_ipaddrs[i] == addr) return true;
			}

		return false;
		}


bool
SWHostname::isAlias(const SWString &name) const
		{
//		if (name.compareTo(_name, SWString::ignoreCase) == 0) return true;

		int	i, n=(int)_aliases.size();

		for (i=0;i<n;i++)
			{
			if (name.compareTo(_aliases.get(i), SWString::ignoreCase) == 0) return true;
			}

		return false;
		}


int
SWHostname::addressCount() const
		{
		return (int)_ipaddrs.size();
		}


SWIPAddress
SWHostname::address(int n) const
		{
		return _ipaddrs[n];
		}



const SWString &	SWHostname::name() const		{ return _name; }
const SWString &	SWHostname::hostname() const	{ return _hostname; }
bool 				SWHostname::isValid() const 		{ return ((_flags & FLAG_IS_VALID) == FLAG_IS_VALID); }
bool 				SWHostname::gotHostInfo() const 	{ return ((_flags & FLAG_INFO_IS_VALID) == FLAG_INFO_IS_VALID); }
int					SWHostname::aliasCount() const		{ return (int)_aliases.size(); }
SWString			SWHostname::alias(int n) const		{ return _aliases.get(n); }
SWIPAddress			SWHostname::address() const			{ return _address; }
bool	SWHostname::operator==(const SWString &other)	{ return isAlias(other); }



