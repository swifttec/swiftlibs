/**
*** @file		SWThreadCollection.h
*** @brief		A thread collection object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWThreadCollection class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWThreadCollection_h__
#define __SWThreadCollection_h__


#include <swift/SWOrderedList.h>
#include <swift/SWThread.h>
#include <swift/SWThreadEvent.h>
#include <swift/SWLockableObject.h>




#if defined(_MSC_VER)
	 // class xxxx<>' needs to have dll-interface to be used by clients
#endif


// Forward declarations
class SWThreadCollection;
class SWThreadCollectionThread;


/**
*** @brief	A class to represent a job in the thread pool
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWThreadCollectionEntry : public SWObject
		{
public:
		/// Default constructor
		SWThreadCollectionEntry(SWThread *pThread, bool delflag, SWThread::NotificationProc notify);

		/// Virtual destructor
		virtual ~SWThreadCollectionEntry();

		/// Copy constructor
		SWThreadCollectionEntry(const SWThreadCollectionEntry &other);

		/// Assignment operator
		SWThreadCollectionEntry &	operator=(const SWThreadCollectionEntry &other);

		/// Comparision operator for list searching
		bool	operator==(const SWThreadCollectionEntry &other) const;

		/// Return the actual thread
		SWThread *	thread()	{ return m_pThread; }

protected:
		SWThread					*m_pThread;
		bool						m_delflag;
		SWThread::NotificationProc	m_notify;
		};


/**
*** @brief	A class to handle a collection of SWThread objects.
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWThreadCollection : public SWLockableObject
		{
friend class SWThreadCollectionEntry;

public:
		/// A enumeration of thread pool operations
		enum Operation
			{
			AddThread,		///< Threads can be added to the pool
			RemoveThread	///< Threads can be removed from the pool
			};

public:
		/**
		*** @brief	Construct a thread collection object with an optional thread count
		***
		*** This constructor allows the caller to create a thread collection object and
		*** optionally specify the number of threads.
		**/
		SWThreadCollection();

		/// Virtual destrcutor
		virtual ~SWThreadCollection();

		/**
		*** @brief	Add a thread object to the collection.
		***
		*** @param[in]	pThread			A pointer to the job object to be added to the job queue.
		*** @param[in]	delflag			A boolean value indicating if the thread pool (delflag=true)
		***								of the caller (deflag=false) is responsible for deleting the
		***								job object.
		*** @param[in]	notify			This optional parameter can be used to override the notification value
		***								for the job. If this is NULL the job notification function is not
		***								changed, but if this parameter is not NULL the job notification function
		***								is replaced by the one specified.
		***
		*** @return		SW_STATUS_SUCCESS if successful, SW_STATUS_ILLEGAL_OPERATION if the queue is shutting
		***				down or AddThread operations are disabled.
		**/
		sw_status_t		add(SWThread *pThread, bool delflag, SWThread::NotificationProc notify=NULL);

		/**
		*** @brief	Remove a job from the queue.
		***
		*** This method removes a job from the queue if the job is not currently
		*** running. If the job is already active this method will not remove the
		*** job but will return an error code.
		***
		*** @note
		*** If the thread pool was responsible for deleting the job it will be 
		*** deleted when it is removed and cease to be a valid object.
		***
		*** @param[in]	pThread			A pointer to the job object to be removed to the job queue.
		***
		*** @return		SW_STATUS_SUCCESS if successful, SW_STATUS_ILLEGAL_OPERATION if the queue is shutting
		***				down or AddThread operations are disabled.
		**/
		sw_status_t		remove(SWThread *pThread);



		/**
		*** @brief	Enable/disable thread pool operations
		***
		*** This method enables or disables various internal operations according to the
		*** parameters specified. The op parameter specifies the parameter to be acted upon
		*** and the opval specifies if the operation is to be enabled (opval=true) or disabled
		*** (opval=false).
		***
		*** The values for op are taken from the SWThreadCollection::Operation enumeration and they
		*** have the following meanings:
		*** - AddThread -		If enabled jobs can be added to the thread pool queue. This can be set to
		***					false when shutting down the pool to make sure that no more jobs are added.
		*** - RemoveThread -	If enabled inactive jobs can be removed from the internal job queue. Setting
		***					this operation to false (i.e. disabled) only prevents jobs from being removed
		***					by the external interface and does not prevent completed jobs from being
		***					removed by the internal threads.
		*** - RunThread -		If enabled jobs can be executed by the internal threads. If disabled the internal
		***					threads will not execute threads until the RunThread operation is re-enabled.
		***
		*** @param[in]	op		Specifies the operation to enable or disable.
		*** @param[in]	opval	A boolean indicating if the operation is to be enabled(true) or disabled(false).
		***
		*** @return		SW_STATUS_SUCCESS if successful or an error code otherwise.
		**/
		sw_status_t		enable(Operation op, bool opval);


		/**
		*** @brief	Shutdown the job pool
		***
		*** This method shuts down the job pool by disabling the addjob operation
		*** and waiting until all jobs have completed. When all the jobs are completed
		*** all the active threads are stopped.
		***
		*** This method will block until shutdown is complete and all threads have
		*** exited.
		**/
		void			shutdown();

		/**
		*** @brief	Blocks until all jobs are complete.
		***
		*** This method blocks until all the queued jobs are complete and then
		*** returns to the caller.
		**/
		void			waitUntilAllThreadsComplete();


		/**
		*** @brief	Get the number of threads
		**/
		sw_size_t		getThreadCount() const		{ return m_threadlist.size(); }

protected:
		SWList<SWThreadCollectionEntry>	m_threadlist;		///< The list of running threads
		sw_uint32_t						m_flags;			///< Internal flags
		};






#endif
