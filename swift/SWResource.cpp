/**
*** @file		SWResource.cpp
*** @brief		An object to represent a single resource entity
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWResource class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




// Needed for precompiled header support
#include "stdafx.h"

#include <swift/SWGuard.h>
#include <swift/SWResource.h>
#include <swift/SWResourceModule.h>


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif



SWResource::SWResource(sw_resourceid_t id, ResourceType type, void *data, int size) :
    m_id(id),
    m_type(type),
    m_size(size),
    m_pData(data)
		{
		}


SWResource::SWResource(const SWResource &other) :
	SWObject()
		{
		*this = other;
		}


SWResource &
SWResource::operator=(const SWResource &other)
		{
		m_id = other.m_id;
		m_type = other.m_type;
		m_size = other.m_size;
		m_pData = other.m_pData;

		return *this;
		}


SWResource::~SWResource()
		{
		}

/*********************************************************************************
** STATIC MEMBERS
*********************************************************************************/
SWObjectArray	SWResource::sm_moduleArray(0, true);
bool			SWResource::sm_initdone=false;



void
SWResource::initModuleList()
		{
		// Don't init twice
		if (sm_initdone) return;

		sm_initdone = true;

		// Make the array synchronised
		sm_moduleArray.synchronize();

		SWGuard	guard(sm_moduleArray);

#if defined(__base_SWAFX_H__) || defined(_AFXDLL) || defined(AFXAPI)
		// Add the MFC resources first

		// Can't use AfxGetResourceHandle as it will assert if no handle
		// has been set. Therefore we use afxCurrentResourceHandle as that 
		// is what the macro uses.
		if (afxCurrentResourceHandle != 0)
			{
			insertModule(new SWResourceModule(afxCurrentResourceHandle), 0);
			}
#endif

		// Add as a last handle the current module
		//addModule();
		}


// Add a module to the global module list
void
SWResource::addModule(SWResourceModule *pModule)
		{
		if (!sm_initdone) initModuleList();

		SWGuard	guard(sm_moduleArray);

		sm_moduleArray.add(pModule);
		}


// Insert a module into the global module list at the specified position
void
SWResource::insertModule(SWResourceModule *pModule, int position)
		{
		if (!sm_initdone) initModuleList();

		SWGuard	guard(sm_moduleArray);

		sm_moduleArray.insert(pModule, position);
		}


// Remove the specified module from the global module list
void
SWResource::removeModule(SWResourceModule *pModule)
		{
		if (!sm_initdone) initModuleList();

		SWGuard	guard(sm_moduleArray);
		int		i, n=(int)sm_moduleArray.size();
		
		for (i=0;i<n;i++)
			{
			if ((SWResourceModule *)(sm_moduleArray[i]) == pModule)
				{
				sm_moduleArray.remove(i);
				--i;
				--n;
				}
			}
		}


// Remove the module from the specified position of the global module list
void
SWResource::removeModule(int pos)
		{
		if (!sm_initdone) initModuleList();

		SWGuard	guard(sm_moduleArray);
		sm_moduleArray.remove(pos);
		}


// Return a count of the modules in the global list
sw_size_t
SWResource::getModuleCount()
		{
		if (!sm_initdone) initModuleList();

		return sm_moduleArray.size();
		}


// Return a pointer to the specified module in the global list.
SWResourceModule *
SWResource::getModule(int pos)
		{
		if (!sm_initdone) initModuleList();

		// Use get here so it throws an exception if pos is out of range.
		return (SWResourceModule *)sm_moduleArray.get(pos);
		}



bool
SWResource::loadString(sw_resourceid_t id, SWString &str, ResourceType rtype)
		{
		if (!sm_initdone) initModuleList();

		bool	res=false;

		if (rtype == String || rtype == MessageTable)
			{
			SWGuard	guard(sm_moduleArray);
			int		i;

			for (i=0;!res && i<(int)sm_moduleArray.size();i++)
				{
				SWResourceModule	*pModule=(SWResourceModule *)(sm_moduleArray[i]);

				res = pModule->loadString(id, str, rtype);
				}
			}

		return res;
		}

// Special support for resource guard
SWIFT_DLL_EXPORT SWResourceModule *
sw_resource_push_state()
		{
		SWResourceModule *p=NULL;
		
#if defined(SW_BUILD_MFC)
		p = new SWResourceModule(AfxGetInstanceHandle());
		SWResource::insertModule(p, 0);
#endif

		return p;
		}


SWIFT_DLL_EXPORT void
sw_resource_pop_state(SWResourceModule *p)
		{
#if defined(SW_BUILD_MFC)
		SWResource::removeModule(p);
#endif
		}



