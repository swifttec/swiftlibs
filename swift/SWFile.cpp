/**
*** @file		SWFile.cpp
*** @brief		Basic file handling class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWFile class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




// Needed for precompiled header support
#include "stdafx.h"

#include <swift/swift.h>
#include <swift/SWFile.h>
#include <swift/SWFolder.h>
#include <swift/SWGuard.h>

#if defined(SW_PLATFORM_WINDOWS)
	#include <io.h>
	#include <fcntl.h>
	#include <share.h>
	#include <sys/locking.h>

	#include <winioctl.h>
#else
	#include <fcntl.h>
	#include <sys/stat.h>
	#define GetLastError()	(errno)
#endif

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





#if defined(SW_PLATFORM_WINDOWS) && _WIN32_WINNT < 0x0500
/*
** These functions are only available on Windows 2000 and later
** so for special builds we need to emulate the calls using 
** the non-Ex functions
*/
#define SetFilePointerEx InternalSetFilePointerEx
#define GetFileSizeEx InternalGetFileSizeEx

static BOOL
InternalSetFilePointerEx(
  HANDLE hFile,                    // handle to file
  LARGE_INTEGER liDistanceToMove,  // bytes to move pointer
  PLARGE_INTEGER lpNewFilePointer, // new file pointer
  DWORD dwMoveMethod               // starting point
)
		{
		LONG	low, high;
		BOOL	res;

		low = liDistanceToMove.LowPart;
		high = liDistanceToMove.HighPart;

		low = SetFilePointer(hFile, low, &high, dwMoveMethod);

		if (low != INVALID_SET_FILE_POINTER || GetLastError() == NO_ERROR)
			{
			lpNewFilePointer->LowPart = low;
			lpNewFilePointer->HighPart = high;
			res = TRUE;
			}
		else
			{
			lpNewFilePointer->LowPart = 0;
			lpNewFilePointer->HighPart = 0;
			res = FALSE;
			}

		return res;
		}



static BOOL
InternalGetFileSizeEx(
  HANDLE hFile,              // handle to file
  PLARGE_INTEGER lpFileSize  // file size
)
		{
		DWORD	low, high;
		BOOL	res;

		low = GetFileSize(hFile, &high);

		if (low != INVALID_FILE_SIZE || GetLastError() == NO_ERROR)
			{
			lpFileSize->LowPart = low;
			lpFileSize->HighPart = high;
			res = TRUE;
			}
		else
			{
			lpFileSize->LowPart = 0;
			lpFileSize->HighPart = 0;
			res = FALSE;
			}

		return res;
		}

#endif // defined(SW_PLATFORM_WINDOWS) && WINVER < 0x0500


/*
** Default constructor
*/
SWFile::SWFile() :
	_hFile(0),
    _mode(0)
		{
#if defined(SW_PLATFORM_WINDOWS)
		// Use the Windows-style interface
		_hFile = INVALID_HANDLE_VALUE;
#else
		// Use the Unix-style interface
		_hFile = -1;
#endif // defined(SW_FILE_USE_NATIVE_FILEIO)
		}


/*
** Destructor
*/
SWFile::~SWFile()
		{
		close();
		}


/*
** Opens the specified file.
**
** The open method is the equivalent of the open(2) unix system call
**
** @return true if successful, false otherwise
*/
sw_status_t
SWFile::open(const SWString &filename, int mode, SWFileAttributes *pfa)
		{
		if (isOpen())
			{
			_lastErrno = SW_STATUS_ALREADY_OPEN;
			}
		else
			{
			SWGuard	guard(this);

			_lastErrno = SW_STATUS_SUCCESS;

			int	oflags=0;

			// Do initial checks to make sure we have only specified one access mode.
			switch (mode & 3)
				{
				case ModeReadOnly:
					oflags = O_RDONLY;
					break;

				case ModeWriteOnly:
					oflags = O_WRONLY;
					break;

				case ModeReadWrite:
					oflags = O_RDWR;
					break;

				default:
					// This is invalid
					_lastErrno = SW_STATUS_INVALID_PARAMETER;
					break;
				}

			if (_lastErrno == SW_STATUS_SUCCESS)
				{
	#if defined(SW_PLATFORM_WINDOWS)
				DWORD	dwDesiredAccess=0;
				DWORD	dwShareMode=0;
				DWORD	dwCreationDisposition=0;
				DWORD	dwFlagsAndAttributes=0;

				if (oflags == O_RDONLY || oflags == O_RDWR) dwDesiredAccess |= GENERIC_READ;
				if (oflags == O_WRONLY || oflags == O_RDWR) dwDesiredAccess |= GENERIC_WRITE;

				if ((mode & ModeCreate) != 0)
					{
					if ((mode & ModeExclusive) != 0)
						{
						dwCreationDisposition = CREATE_NEW;
						}
					else
						{
						dwCreationDisposition = OPEN_ALWAYS;
						}
					}
				else
					{
					if ((mode & ModeTruncate) != 0)
						{
						dwCreationDisposition = TRUNCATE_EXISTING;
						}
					else
						{
						dwCreationDisposition = OPEN_EXISTING;
						}
					}


				if (pfa != NULL)
					{
					dwFlagsAndAttributes = (pfa->attributes() & 0222)?FILE_ATTRIBUTE_NORMAL:FILE_ATTRIBUTE_READONLY;
					}
				else dwFlagsAndAttributes = FILE_ATTRIBUTE_NORMAL;


				/*
				These are not directly supported by the windows interface and will be ignored
				or simulated

				if ((mode & ModeAppend) != 0) dwFlagsAndAttributes |= O_APPEND;
				if ((mode & ModeNonBlocking) != 0) dwFlagsAndAttributes |= O_NDELAY;
				if ((mode & ModeBinary) != 0) dwFlagsAndAttributes |= O_BINARY;
				if ((mode & ModeText) != 0) dwFlagsAndAttributes |= O_TEXT;
				if ((mode & ModeNoInherit) != 0) dwFlagsAndAttributes |= O_NOINHERIT;
				if ((mode & ModeNoCtrlTTY) != 0) dwFlagsAndAttributes |= O_NOCTTY;
				if ((mode & ModeShortLived) != 0) dwFlagsAndAttributes |= FILE_ATTRIBUTE_SHORT_LIVED;
				*/

				if ((mode & ModeTemporary) != 0) dwFlagsAndAttributes |= FILE_ATTRIBUTE_TEMPORARY;
				if ((mode & ModeSynchronised) != 0) dwFlagsAndAttributes |= FILE_FLAG_WRITE_THROUGH;
				if ((mode & ModeRandom) != 0) dwFlagsAndAttributes |= FILE_FLAG_RANDOM_ACCESS;
				if ((mode & ModeSequential) != 0) dwFlagsAndAttributes |= FILE_FLAG_SEQUENTIAL_SCAN;
				if ((mode & ModeDeleteOnClose) != 0) dwFlagsAndAttributes |= FILE_FLAG_DELETE_ON_CLOSE;

				// Deal with the share flags
				switch (mode & ShareDenyAll)
					{
					case ShareDenyRead:
						dwShareMode = FILE_SHARE_WRITE | FILE_SHARE_DELETE;
						break;

					case ShareDenyWrite:
						dwShareMode = FILE_SHARE_READ | FILE_SHARE_DELETE;
						break;

					case ShareDenyAll:
						dwShareMode = 0;
						break;

					default:
						dwShareMode = FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE;
						break;
					}

				SWString	tfname=filename.w_str();

				// tfname.replaceAll('/', '\\');
				if ((mode & ModeNoFixPath) != 0) tfname = filename.w_str();
				else tfname = SWFilename::optimizePath(filename.w_str(), SWFilename::PathTypePlatform);

				if (SWFilename::isAbsolutePath(tfname))
					{
					if (!tfname.startsWith(L"\\\\?\\") && !tfname.startsWith(L"\\\\.\\"))
						{
						if (tfname.startsWith(L"\\"))
							{
							tfname.insert(1, L"\\?\\UNC");
							}
						else
							{
							tfname.insert(0, L"\\\\?\\");
							}
						}
					}

				_hFile = CreateFileW(tfname, dwDesiredAccess, dwShareMode, NULL, dwCreationDisposition, dwFlagsAndAttributes, NULL);
				if (_hFile != INVALID_HANDLE_VALUE)
					{
					if (dwCreationDisposition == OPEN_ALWAYS && (mode & ModeTruncate) != 0)
						{
						// We must truncate the file.
						truncate(0);
						}
					else if ((mode & ModeAppend) != 0)
						{
						lseek(0, SeekEnd);
						}
					}

				if (_hFile == INVALID_HANDLE_VALUE) _lastErrno = GetLastError();

	#else // defined(SW_PLATFORM_WINDOWS)
				// Always want largefile support if available

		#ifdef O_LARGEFILE
				oflags |= O_LARGEFILE;
		#endif

				// Deal with the rest of the mode flags
				if ((mode & ModeCreate) != 0) oflags |= O_CREAT;
				if ((mode & ModeTruncate) != 0) oflags |= O_TRUNC;
		#ifdef O_APPEND
				if ((mode & ModeAppend) != 0) oflags |= O_APPEND;
		#endif
		#ifdef O_EXCL
				if ((mode & ModeExclusive) != 0) oflags |= O_EXCL;
		#endif
		#ifdef O_TEMPORARY
				if ((mode & ModeTemporary) != 0) oflags |= O_TEMPORARY;
		#endif
		#ifdef O_SHORT_LIVED
				if ((mode & ModeShortLived) != 0) oflags |= O_SHORT_LIVED;
		#endif
		#ifdef O_NDELAY
				if ((mode & ModeNonBlocking) != 0) oflags |= O_NDELAY;
		#endif
		#ifdef O_SYNC
				if ((mode & ModeSynchronised) != 0) oflags |= O_SYNC;
		#endif
		#ifdef O_RANDOM
				if ((mode & ModeRandom) != 0) oflags |= O_RANDOM;
		#endif
		#ifdef O_SEQUENTIAL
				if ((mode & ModeSequential) != 0) oflags |= O_SEQUENTIAL;
		#endif
		#ifdef O_BINARY
				if ((mode & ModeBinary) != 0) oflags |= O_BINARY;
		#endif
		#ifdef O_TEXT
				if ((mode & ModeText) != 0) oflags |= O_TEXT;
		#endif
		#ifdef O_NOINHERIT
				if ((mode & ModeNoInherit) != 0) oflags |= O_NOINHERIT;
		#endif
		#ifdef O_NOCTTY
				if ((mode & ModeNoCtrlTTY) != 0) oflags |= O_NOCTTY;
		#endif

				_hFile = ::open((const char *)SWString(filename), oflags, 0666);
				if (_hFile < 0) _lastErrno = errno;

	#endif // defined(SW_PLATFORM_WINDOWS)
				}

			if (_lastErrno == SW_STATUS_SUCCESS)
				{
				_mode = mode;
				_name = filename;
				_dir = SWFolder::getCurrentDirectory();
				}
			}

		return _lastErrno;
		}


/*
** Close the file
**
** @return true if successful, false otherwise
*/
sw_status_t
SWFile::close()
		{
		SWGuard		guard(this);

		_lastErrno = SW_STATUS_SUCCESS;

#if defined(SW_PLATFORM_WINDOWS)
		if (_hFile != INVALID_HANDLE_VALUE)
			{
			if (CloseHandle(_hFile))
				{
				_hFile = INVALID_HANDLE_VALUE;
				}
			else _lastErrno = GetLastError();
			}
#else
		if (_hFile >= 0)
			{
			int	r;

			r = ::close(_hFile);
			if (r >= 0) _hFile = -1;
			else _lastErrno = errno;
			}

		if ((_mode & ModeDeleteOnClose) != 0)
			{
			// Unix doesn't have direct support for this so we must do it ourselves
			::unlink(getFullPath());
			}
#endif

		if (_lastErrno == SW_STATUS_SUCCESS)
			{
			_mode = 0;
			_name.clear();
			_dir.clear();
			}

		return _lastErrno;
		}


/*
** Read len bytes of data into the provided buffer.
**
** @return the number of bytes actually read or -1 on failure.
*/
sw_status_t
SWFile::read(void *data, sw_size_t nbytes, sw_size_t *pBytesRead)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (isOpen())
			{
			SWGuard	guard(this);

	#if defined(SW_PLATFORM_WINDOWS)
			DWORD	count;

			if (!ReadFile(_hFile, data, (DWORD)nbytes, &count, NULL)) res = GetLastError();
			if (pBytesRead != NULL) *pBytesRead = count;
	#else
			int		r;

			r = ::read(_hFile, data, nbytes);
			if (r < 0)
				{
				res = errno;
				if (pBytesRead != NULL) *pBytesRead = 0;
				}
			else if (pBytesRead != NULL) *pBytesRead = r;
	#endif

			_lastErrno = res;
			}
		else
			{
			res = SW_STATUS_INVALID_HANDLE;
			}

		return res;
		}


/*
** Write len bytes of data from the provided buffer.
**
** @return the number of bytes actually written or -1 on failure.
*/
sw_status_t
SWFile::write(const void *data, sw_size_t nbytes, sw_size_t *pBytesWritten)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (isOpen())
			{
			SWGuard	guard(this);

	#if defined(SW_PLATFORM_WINDOWS)
			DWORD	count;

			if (!WriteFile(_hFile, data, (DWORD)nbytes, &count, NULL)) res = GetLastError();
			if (pBytesWritten != NULL) *pBytesWritten = count;
	#else
			int		r;
			r = ::write(_hFile, data, nbytes);

			if (r < 0)
				{
				res = errno;
				if (pBytesWritten != NULL) *pBytesWritten = 0;
				}
			else if (pBytesWritten != NULL) *pBytesWritten = r;
	#endif

			_lastErrno = res;
			}
		else
			{
			res = SW_STATUS_INVALID_HANDLE;
			}

		return res;
		}



/*
** Location seek
**
** Sets the file position.
*/
sw_status_t
SWFile::lseek(sw_filepos_t offset, SeekMethod whence, sw_filepos_t *pCurrentOffset)
		{
		sw_status_t		res=SW_STATUS_SUCCESS;
		sw_filepos_t	filepos=(sw_filepos_t)-1;

		if (isOpen())
			{
			SWGuard	guard(this);

	#if defined(SW_PLATFORM_WINDOWS)
			LARGE_INTEGER	liOffset, liPosition;
			DWORD			dwMoveMethod=0;
			
			switch (whence)
				{
				case SeekSet:		dwMoveMethod = FILE_BEGIN;		break;
				case SeekRelative:	dwMoveMethod = FILE_CURRENT;	break;
				case SeekEnd:		dwMoveMethod = FILE_END;		break;
				}

			liOffset.QuadPart = offset;
			liPosition.QuadPart = 0;

			if (SetFilePointerEx(_hFile, liOffset, &liPosition, dwMoveMethod))
				{
				filepos = liPosition.QuadPart;
				}
			else
				{
				filepos = (sw_filepos_t)-1;
				res = GetLastError();
				}
	#else
			int		seekmode=0;

			switch (whence)
				{
				case SeekSet:		seekmode = SEEK_SET;	break;
				case SeekRelative:	seekmode = SEEK_CUR;	break;
				case SeekEnd:		seekmode = SEEK_END;	break;
				}

		#if defined(SW_USE_UNIX_FILES)
			#if defined(SW_USE_UNIX_64BIT_FILES)
				filepos = ::lseek64(_hFile, offset, seekmode);
			#else
				filepos = ::lseek(_hFile, offset, seekmode);
			#endif
		#endif

			if (filepos == (sw_filepos_t)-1) res = errno;
	#endif
			}
		else
			{
			res = SW_STATUS_INVALID_HANDLE;
			}

		if (pCurrentOffset != NULL) *pCurrentOffset = filepos;

		_lastErrno = res;
		return res;
		}


/*
** Returns the current file position
*/
sw_filepos_t
SWFile::tell()
		{
		sw_filepos_t	filepos=(sw_filepos_t)-1;

		if (isOpen())
			{ // BEGIN THREAD GUARD
			SWGuard	guard(this);

	#if defined(SW_PLATFORM_WINDOWS)
			LARGE_INTEGER	liOffset, liPosition;
			
			liOffset.QuadPart = 0;
			liPosition.QuadPart = 0;

			if (SetFilePointerEx(_hFile, liOffset, &liPosition, FILE_CURRENT))
				{
				filepos = liPosition.QuadPart;
				}
			else
				{
				filepos = -1;
				}
	#else
		#if defined(SW_USE_UNIX_FILES)
			#if defined(SW_USE_UNIX_64BIT_FILES)
				#if defined(SW_USE_UNIX_TELL)
					filepos = ::tell64(_hFile);
				#else
					filepos = ::lseek64(_hFile, 0, SEEK_CUR);
				#endif
			#else
				#if defined(SW_USE_UNIX_TELL)
					filepos = ::tell(_hFile, 0, SEEK_CUR);
				#else
					filepos = ::lseek(_hFile, 0, SEEK_CUR);
				#endif
			#endif
		#endif
	#endif

			if (filepos == (sw_filepos_t)-1) _lastErrno = errno;
			else _lastErrno = 0;
			} // END THREAD GUARD

		return filepos;
		}


sw_status_t
SWFile::truncate(sw_filepos_t size)
		{
		_lastErrno = SW_STATUS_SUCCESS;

		if (isOpen())
			{ // BEGIN THREAD GUARD
			SWGuard	guard(this);

	#if defined(SW_USE_UNIX_FILES)
		#if defined(SW_USE_UNIX_64BIT_FILES)
			if (::ftruncate64(_hFile, size) == 0) _lastErrno = 0;
			else _lastErrno = errno;
		#else
			if (::ftruncate(_hFile, size) == 0) _lastErrno = 0;
			else _lastErrno = errno;
		#endif
	#else
			sw_status_t	tmp;
			sw_filepos_t		currpos = tell();

			lseek(size, SeekSet);
			if (_lastErrno == SW_STATUS_SUCCESS)
				{
				if (SetEndOfFile(_hFile)) tmp = 0;
				else tmp = GetLastError();

				lseek(currpos, SeekSet);

				_lastErrno = tmp;
				SetLastError(_lastErrno);
				}
	#endif
			} // END THREAD GUARD
		else
			{
			_lastErrno = SW_STATUS_INVALID_HANDLE;
			}

		return _lastErrno;
		}



sw_status_t
SWFile::lockSection(const SWFileRange &range)
		{
		if (isOpen())
			{
			SWGuard	guard(this);

			_lastErrno = SW_STATUS_SUCCESS;

	#if defined(SW_USE_UNIX_FILES)
		#if defined(SW_USE_UNIX_64BIT_FILES)
			struct flock64	fl;
			int	r;

			memset(&fl, 0, sizeof(fl));
			fl.l_type = F_WRLCK;
			fl.l_whence = SEEK_SET;
			fl.l_start = range.offset();
			fl.l_len = range.length();
			r = ::fcntl(_hFile, F_SETLKW64, &fl);
			if (r != 0) _lastErrno = errno;
		#else
			struct flock	fl;
			int	r;

			memset(&fl, 0, sizeof(fl));
			fl.l_type = F_WRLCK;
			fl.l_whence = SEEK_SET;
			fl.l_start = range.offset();
			fl.l_len = range.length();
			r = ::fcntl(_hFile, F_SETLKW, &fl);
			if (r != 0) _lastErrno = errno;
		#endif
	#elif defined(SW_PLATFORM_WINDOWS)
			OVERLAPPED	ol;
			LARGE_INTEGER	li;

			memset(&ol, 0, sizeof(ol));
			li.QuadPart = range.offset();
			ol.Offset = li.LowPart;
			ol.OffsetHigh = li.HighPart;

			li.QuadPart = range.length();
			if (!LockFileEx(_hFile, LOCKFILE_EXCLUSIVE_LOCK, 0, li.LowPart, li.HighPart, &ol)) _lastErrno = GetLastError();
	#endif
			}
		else
			{
			_lastErrno = SW_STATUS_INVALID_HANDLE;
			}

		return _lastErrno;
		}


sw_status_t
SWFile::unlockSection(const SWFileRange &range)
		{
		if (isOpen())
			{
			SWGuard	guard(this);

			_lastErrno = SW_STATUS_SUCCESS;

	#if defined(SW_USE_UNIX_FILES)
		#if defined(SW_USE_UNIX_64BIT_FILES)
			struct flock64	fl;
			int	r;

			memset(&fl, 0, sizeof(fl));
			fl.l_type = F_UNLCK;
			fl.l_whence = SEEK_SET;
			fl.l_start = range.offset();
			fl.l_len = range.length();
			r = ::fcntl(_hFile, F_SETLKW64, &fl);
			if (r != 0) _lastErrno = errno;
		#else
			struct flock	fl;
			int	r;

			memset(&fl, 0, sizeof(fl));
			fl.l_type = F_UNLCK;
			fl.l_whence = SEEK_SET;
			fl.l_start = range.offset();
			fl.l_len = range.length();
			r = ::fcntl(_hFile, F_SETLKW, &fl);
			if (r != 0) _lastErrno = errno;
		#endif
	#elif defined(SW_PLATFORM_WINDOWS)
			OVERLAPPED	ol;
			LARGE_INTEGER	li;

			memset(&ol, 0, sizeof(ol));
			li.QuadPart = range.offset();
			ol.Offset = li.LowPart;
			ol.OffsetHigh = li.HighPart;

			li.QuadPart = range.length();
			if (!UnlockFileEx(_hFile, 0, li.LowPart, li.HighPart, &ol)) _lastErrno = GetLastError();
	#endif
			}
		else
			{
			_lastErrno = SW_STATUS_INVALID_HANDLE;
			}

		return _lastErrno;
		}

sw_status_t
SWFile::freeSection(const SWFileRange &range)
		{
		if (isOpen())
			{
			SWGuard	guard(this);

			_lastErrno = SW_STATUS_SUCCESS;

	#if defined(SW_USE_UNIX_FILES)
		#if defined(SW_USE_UNIX_64BIT_FILES)
			#if defined(F_FREESP)
				struct flock64	fl;
				int		r;

				memset(&fl, 0, sizeof(fl));
				fl.l_whence = SEEK_SET;
				fl.l_start = range.offset();
				fl.l_len = range.length();
				r = ::fcntl(_hFile, F_FREESP, &fl);
				if (r == 0) _lastErrno = errno;
			#else
				SW_UNREFERENCED_PARAMETER(range);
				_lastErrno = SW_STATUS_UNSUPPORTED_OPERATION;
			#endif
		#else
			#if defined(F_FREESP)
				struct flock	fl;
				int		r;

				memset(&fl, 0, sizeof(fl));
				fl.l_whence = SEEK_SET;
				fl.l_start = range.offset();
				fl.l_len = range.length();
				r = ::fcntl(_hFile, F_FREESP, &fl);
				if (r == 0) _lastErrno = errno;
			#else
				SW_UNREFERENCED_PARAMETER(range);
				_lastErrno = SW_STATUS_UNSUPPORTED_OPERATION;
			#endif
		#endif
	#elif defined(SW_PLATFORM_WINDOWS)
		#if _WIN32_WINNT >= 0x0500
			FILE_ZERO_DATA_INFORMATION	fzdi;
			DWORD				bytecount;

			fzdi.FileOffset.QuadPart = range.offset();
			fzdi.BeyondFinalZero.QuadPart = range.offset() + range.length();

			// Force the sparse flag on for this file.
			DeviceIoControl(_hFile, FSCTL_SET_SPARSE, NULL, 0, NULL, 0, &bytecount, NULL);

			if (!DeviceIoControl(_hFile, FSCTL_SET_ZERO_DATA, &fzdi, sizeof(fzdi), NULL, 0, &bytecount, NULL)) _lastErrno = GetLastError();
		#else
			// Not supported in earlier versions but this is a runtime error
			// not a compile-time error.
			_lastErrno = ERROR_NOT_SUPPORTED;
			SetLastError(_lastErrno);
		#endif
	#endif
			}
		else
			{
			_lastErrno = SW_STATUS_INVALID_HANDLE;
			}

		return _lastErrno;
		}


sw_status_t
SWFile::flush()
		{
		if (isOpen())
			{
			SWGuard	guard(this);

			_lastErrno = SW_STATUS_SUCCESS;

	#if defined(SW_USE_UNIX_FILES)
			if (fsync(_hFile) != 0) _lastErrno = errno;
	#elif defined(SW_PLATFORM_WINDOWS)
			if (!FlushFileBuffers(_hFile)) _lastErrno = GetLastError();
	#endif
			}
		else
			{
			_lastErrno = SW_STATUS_INVALID_HANDLE;
			}

		return _lastErrno;
		}



bool
SWFile::isOpen() const
		{
#if defined(SW_USE_UNIX_FILES)
		return (_hFile >= 0);
#elif defined(SW_PLATFORM_WINDOWS)
		return (_hFile != INVALID_HANDLE_VALUE);
#endif
		}



/*
** Returns the size of the file
*/
sw_filesize_t
SWFile::size()
		{
		sw_filesize_t	filesize=0;
		bool			res=false;
	
		if (isOpen())
			{
			SWGuard	guard(this);

	#if defined(SW_USE_UNIX_FILES)
		#if defined(SW_USE_UNIX_64BIT_FILES)
			struct stat64	sb;

			res = (::fstat64(_hFile, &sb) == 0);
			if (res) filesize = sb.st_size;
		#else
			struct stat	sb;

			res = (::fstat(_hFile, &sb) == 0);
			if (res) filesize = sb.st_size;
		#endif
	#elif defined(SW_PLATFORM_WINDOWS)
			LARGE_INTEGER	li;

			res = (GetFileSizeEx(_hFile, &li) == TRUE);
			if (res) filesize = li.QuadPart;
	#endif
			}

		return filesize;
		}


/*
** Returns the full path of the file opened.
*/
SWString
SWFile::getFullPath() const
		{
		if (SWFilename::isAbsolutePath(_name)) return _name;
		else return SWFilename::concatPaths(_dir, _name);
		}


sw_status_t
SWFile::remove(const SWString &filename)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

#if defined(SW_USE_UNIX_FILES)
		if (::unlink((const char *)SWString(filename)) != 0) res = errno;
#elif defined(SW_PLATFORM_WINDOWS)
		if (!::DeleteFile(filename)) res = ::GetLastError();
#endif

		return res;
		}


sw_status_t
SWFile::move(const SWString &from, const SWString &to)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

#if defined(SW_USE_UNIX_FILES)

		#if !defined(O_BINARY)
			#define O_BINARY	0
		#endif

		if (rename(from, to) != 0 && errno == EXDEV)
			{
			// On different logical devices. Copy and Delete!
			int		in, out;
			bool	ok=false;

			in = ::open(from, O_RDONLY|O_BINARY, 0);
			if (in < 0)
				{
				res = SW_STATUS_ILLEGAL_OPERATION;
				}
			else
				{
				struct stat	st;

				stat(from, &st);
				out = ::open(to, O_WRONLY|O_TRUNC|O_CREAT|O_BINARY, st.st_mode);
				if (out < 0)
					{
					::unlink(to);
					out = ::open(to, O_WRONLY|O_TRUNC|O_CREAT|O_BINARY, st.st_mode);
					}

				if (out < 0)
					{
					::close(in);
					res = SW_STATUS_ILLEGAL_OPERATION;
					}

				if (out >= 0)
					{
					// OK now do the copy
					#define COPYBUFSIZE 0x8000

					int		n;
					char	*buf;
				
					buf = new char[COPYBUFSIZE];

					for (;;)
						{
						n = ::read(in, buf, COPYBUFSIZE);
						if (n < 0)
							{
							break;
							}

						if (n > 0)
							{
							int r = ::write(out, buf, n);
							if (r != n)
								{
								break;
								}
							}
						
						if (n != COPYBUFSIZE)
							{
							// All done
							ok = true;
							break;
							}
						}

					::close(out);

					delete buf;
					}

				::close(in);

				if (ok)
					{
					::unlink(from);
					}
				}
			}

#elif defined(SW_PLATFORM_WINDOWS)
		if (!::MoveFileEx(from, to, MOVEFILE_COPY_ALLOWED)) res = ::GetLastError();
#endif

		return res;
		}


sw_status_t
SWFile::copy(const SWString &from, const SWString &to)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

#if defined(SW_USE_UNIX_FILES)

		#if !defined(O_BINARY)
			#define O_BINARY	0
		#endif

		int		in, out;
		bool	ok=false;

		in = ::open(from, O_RDONLY|O_BINARY, 0);
		if (in < 0)
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}
		else
			{
			struct stat	st;

			stat(from, &st);
			out = ::open(to, O_WRONLY|O_TRUNC|O_CREAT|O_BINARY, st.st_mode);
			if (out < 0)
				{
				::unlink(to);
				out = ::open(to, O_WRONLY|O_TRUNC|O_CREAT|O_BINARY, st.st_mode);
				}

			if (out < 0)
				{
				::close(in);
				res = SW_STATUS_ILLEGAL_OPERATION;
				}

			if (out >= 0)
				{
				// OK now do the copy
				#define COPYBUFSIZE 0x8000

				int		n;
				char	*buf;
				
				buf = new char[COPYBUFSIZE];

				for (;;)
					{
					n = ::read(in, buf, COPYBUFSIZE);
					if (n < 0)
						{
						break;
						}

					if (n > 0)
						{
						int r = ::write(out, buf, n);
						if (r != n)
							{
							break;
							}
						}
						
					if (n != COPYBUFSIZE)
						{
						// All done
						ok = true;
						break;
						}
					}

				::close(out);

				delete buf;
				}

			::close(in);
			}

#elif defined(SW_PLATFORM_WINDOWS)
		if (!::CopyFile(from, to, FALSE)) res = ::GetLastError();
#endif

		return res;
		}



/*
** Detach and return the file handle for this file.
**
** This is similar to the getFileHandle method except that the file
** handle information is removed from the object and subsequent file operations
** can only be done directly on the file handle via the O/S interface.
*/
sw_status_t
SWFile::detach(sw_filehandle_t &fh)
		{
		if (isOpen())
			{
			SWGuard			guard(this);
			
			_lastErrno = SW_STATUS_SUCCESS;
			fh = _hFile;

			// Invalidate, but don't close, the file handle.
	#if defined(SW_USE_UNIX_FILES)
			if (_hFile >= 0)
				{
				_hFile = -1;
				}
	#elif defined(SW_PLATFORM_WINDOWS)
			if (_hFile != INVALID_HANDLE_VALUE)
				{
				_hFile = INVALID_HANDLE_VALUE;
				}
	#endif
			}
		else
			{
			_lastErrno = SW_STATUS_INVALID_HANDLE;
			}

		return _lastErrno;
		}



/*
** Attaches a native O/S file handle to this object.
**
** This method attempts to attach the specified file handle to this object.
** If the file is already open an exception is thrown.
**
** <b>When using this method it is important to note that filename and directory
** name information may not be available as availability is platform dependent.</b>
**
** @return	SW_STATUS_SUCCESS if successful, or an error code otherwise.
**
** @throws SWFileException if the file is already open.
*/
sw_status_t
SWFile::attach(sw_filehandle_t fh)
		{
		_lastErrno = SW_STATUS_SUCCESS;
		_hFile = fh;
		_name.clear();
		_dir.clear();
		_mode = 0;

		return _lastErrno;
		}



sw_status_t
SWFile::duplicate(SWFile &fh)
		{
		sw_status_t	_lastErrno=SW_STATUS_SUCCESS;

		if (!isOpen() || fh.isOpen())
			{
			_lastErrno = SW_STATUS_ILLEGAL_OPERATION;
			}
		else
			{
	#if defined(SW_USE_UNIX_FILES)
			_hFile = ::dup(fh._hFile);
	#elif defined(SW_PLATFORM_WINDOWS)
			HANDLE	myproc=::GetCurrentProcess();

			if (!::DuplicateHandle(myproc, fh._hFile, myproc, &_hFile, 0, TRUE, DUPLICATE_SAME_ACCESS))
				{
				_lastErrno = GetLastError();
				}
	#endif
			}

		if (_lastErrno == SW_STATUS_SUCCESS)
			{
			_name = fh._name;
			_dir = fh._dir;
			_mode = fh._mode;
			_lastErrno = fh._lastErrno;	
			}

		return _lastErrno;
		}



