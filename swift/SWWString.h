/**
*** @file		SWWString.h
*** @brief		A string based on single-byte character.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWWString class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWWString_h__
#define __SWWString_h__

#ifndef __SWString_h__
#include <swift/SWString.h>
#endif




/**
*** @brief A string based on single-byte characters.
***
*** SWWString is a string object based on the SWString object but unlike
*** SWString has a fixed character size. Whenever a string is assigned to
*** a SWWString object it will be converted to single-byte character format
*** if required.
***
*** This object should be used whenever a string with a known character
*** size of 1 byte is required.
*** 
*** @author		Simon Sparkes
*** @see		SWString
**/
class SWIFT_DLL_EXPORT SWWString : public SWString
		{
public:
		/// Default constructor - a blank string
		SWWString();

		/// Constructor - copy of the given char string
		SWWString(const char *s);

		/// Constructor - copy of the given wide-character string
		SWWString(const wchar_t *s);

		/// Constructor - copy of the given string
		SWWString(const SWString &s);

		/// Copy constructor
		SWWString(const SWWString &s);

		/// Constructor - copy of the first n characters of the given single-byte string
		SWWString(const char *s, sw_size_t n);

		/// Constructor - copy of the first n characters of the given wide-character string
		SWWString(const wchar_t *s, sw_size_t n);

		/// Constructor - copy of n characters of the given character
		SWWString(int c, sw_size_t n=1);

		SWWString &		operator=(int c)				{ SWString::operator=(c); return *this; }
		SWWString &		operator=(const char *s)		{ SWString::operator=(s); return *this; }
		SWWString &		operator=(const wchar_t *s)		{ SWString::operator=(s); return *this; }
		SWWString &		operator=(const SWString &s)	{ SWString::operator=(s); return *this; }
		SWWString &		operator=(const SWWString &s)	{ SWString::operator=(s); return *this; }
		//char * 	GetBuffer(int minlen) 						{ return (char *)SWString::GetBuffer(minlen); }
		//char * 	GetBufferSetLength(int len)					{ return (char *)SWString::GetBufferSetLength(len); }
		};




#endif
