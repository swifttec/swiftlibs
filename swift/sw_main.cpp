/**
*** @file		sw_main.cpp
*** @brief		A wrapper for main
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the sw_main wrappers.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <swift/swift.h>
#include <swift/SWStringArray.h>
#include <swift/sw_file.h>
#include <swift/SWFilteredFolder.h>
#include <swift/SWTokenizer.h>
#include <swift/sw_main.h>


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




/**
*** Internal function for expanding curly braces (csh style)
**/
static void
expandCurlyBraces(const SWString &str, int start, SWStringArray &sa)
		{
		int				len = (int)str.length();
		int				count, spos, epos;
		int				tc;
		SWString		tmpstr, left, mid, right;

		if (str != _T("{}") && (spos = str.indexOf('{', start)) >= 0)
			{
			epos = spos + 1;
			count = 1;

			while (epos < len)
				{
				tc = str[(int)epos];
				if (tc == '{') count++;
				else if (tc == '}' && --count == 0) break;
				epos++;
				}

			if (count == 0)
				{
				if ((epos - spos) >= 0)
					{
					left = str.left(spos);
					mid = str.mid((sw_index_t)spos+1, epos-spos-1);
					right = str.mid((sw_index_t)epos+1);

					while (mid.length() > 0)
						{
						int	bpos, cpos;

						bpos = mid.first('{');
						cpos = mid.first(',');

						if (bpos < 0 && cpos < 0)
							{
							// The final string
							tmpstr = left;
							tmpstr += mid;
							tmpstr += right;
							expandCurlyBraces(tmpstr, 0, sa);
							break;
							}

						// Check for comma first
						if (cpos >= 0 && bpos >= 0 && cpos < bpos) bpos = -1;

						if (bpos >= 0)
							{
							tmpstr = left;
							tmpstr += mid;
							tmpstr += right;
							expandCurlyBraces(tmpstr, 0, sa);
							break;
							}
						else
							{
							tmpstr = left;
							tmpstr += mid.left(cpos);
							tmpstr += right;
							mid.remove(0, cpos+1);
							expandCurlyBraces(tmpstr, 0, sa);
							}
						}
					}
				else
					{
					left = str.left(spos);
					right = str.mid((sw_index_t)epos+1);

					tmpstr = left;
					tmpstr += right;
					sa.add(tmpstr);
					}
				}
			else sa.add(str);
			}
		else sa.add(str);
		}


/**
*** Internal function for expanding wildcards (*, ?, [..])
**/
static void
expandWildcards(const SWString &arg, SWStringArray &args)
		{
		int	p;

		p = arg.firstOf(_T("*?["));
		if (p >= 0)
			{
			SWString	first, dir, filter, rest;

			first = arg.left(p);
			if (first.first('/') < 0 && first.first('\\') < 0)
				{
				first.empty();
				dir = _T(".");
				}
			else
				{
				dir = sw_file_dirname(first);
				
				int q, w;

				q = first.last('/');
				w = first.last('\\');
				if (w > q) q = w;

				if (q >= 0) first.remove(q+1, first.length()-q-1);
				}

			int slash_pos = arg.indexOf('/', p);
			int bslash_pos = arg.indexOf('\\', p);

			if (slash_pos < 0 && bslash_pos < 0)
				{
				filter = sw_file_basename(arg);
				}
			else
				{
				int	q=slash_pos;

				if (q < 0 || (bslash_pos >= 0 && bslash_pos < q)) q = bslash_pos;
				filter = sw_file_basename(arg.left(q));
				rest = arg.mid(q);
				}

			// printf("arg='%s'  dir='%s'  filter='%s'  rest='%s'\n", arg.data(), dir.data(), filter.data(), rest.data());

			SWFilteredFolder	dh(dir, filter);
			SWFileInfo			de;
			SWString			s;

			while (dh.read(de) == SW_STATUS_SUCCESS)
				{
				s = de.getName();
				if (s == _T(".") || s == _T("..")) continue;

				s = first;
				s += de.getName();

				if (rest.isEmpty()) args.add(s);
				else 
					{
					SWString	path(s);

					path += rest;
					expandWildcards(path, args);
					}
				}
			}
		else args.add(arg);
		}


/**
*** Parse the given command line and extract the program name and an array of args.
***
*** This function takes care of arguments contained within pairs of quotes ("..." or
*** '...') and expansion does not take place for these arguments.
***
*** The action for expansion of command line arguments can be controlled by specify
*** 0 or more of the following flags:
***
*** SW_MAIN_EXPAND_WILDCARDS expands any wildcards found to match file names.
***
***	* matches and expands to 0 or more chars
***	? matches and expands to any single char
***	[] matched and expands to the given character range.
***
*** SW_MAIN_EXPAND_CURLYBRACES enables csh-like curly brace expansion.
*** The text between matching braces is expected to be a comma separated list
*** of alternate texts.
***
*** E.g. the string "fred.{cpp,h}" will expand to two strings "fred.cpp" and "fred.h"
***
*** @param	cmdline		The command line to parse
*** @param	program		Receives the value of program
*** @param	args		Receives the expanded args
*** @param	flags		Processing flags (see description above)
**/
SWIFT_DLL_EXPORT void
sw_main_parseCommandLine(const SWString &cmdline, SWString &program, SWStringArray &args, int flags)
		{
		const sw_tchar_t	*sp;
		bool				quote=false;
		bool				gotprog=((flags & SW_MAIN_WINDOWS_CMDLINE) != 0);

		for (sp=cmdline;*sp;)
			{
			// Skip white space
			while (*sp == ' ' || *sp == '\t') sp++;

			if (!*sp) break;

			SWString	arg;
			sw_tchar_t	qc=0, quotechar=0;
			int			count=0;
			int			squote=-1, equote=-1;

			while (*sp)
				{
				if (*sp == qc)
					{
					// End of quoted section
					quotechar = qc;
					qc = 0;
					sp++;
					equote = count;
					continue;
					}
				
				if ((*sp == '"' || *sp == '\'') && qc == 0)
					{
					qc = *sp++;
					squote = count;
					continue;
					}

				if ((*sp == ' ' || *sp == '\t') && qc == 0) break;

				if (*sp == '\\')
					{
					if ((flags & SW_MAIN_PROCESS_BACKSLASH) == SW_MAIN_PROCESS_BACKSLASH || *(sp+1) == '"' || *(sp+1) == '\'')
						{
						sp++;
						}
					}

				arg += *sp++;
				count++;
				}

			quote = (squote == 0 && equote == count);

			bool	added=false;

			if (!gotprog)
				{
				program = arg;
				gotprog = true;
				}
			else
				{
				if (quote && quotechar == '"')
					{
					quote = false;
					}

				if (!quote)
					{
					SWStringArray	sa;
					int		i, n;

					sa.add(arg);

					if ((flags & SW_MAIN_EXPAND_CURLYBRACES) == SW_MAIN_EXPAND_CURLYBRACES && arg.first('{') >= 0)
						{
						SWStringArray	ta(sa);

						n = (int)ta.size();
						sa.clear();

						for (i=0;i<n;i++) 
							{
							expandCurlyBraces(ta[i], 0, sa);
							}
						}

					if ((flags & SW_MAIN_EXPAND_WILDCARDS) == SW_MAIN_EXPAND_WILDCARDS && arg.firstOf(_T("*?[")) >= 0)
						{
						// Expand wildcards
						SWStringArray	ta(sa);

						n = (int)ta.size();
						sa.clear();

						for (i=0;i<n;i++) 
							{
							expandWildcards(ta[i], sa);
							}
						}

					n = (int)sa.size();
					for (i=0;i<n;i++) args.add(sa[i]);
					added = true;
					}

				// Catch statement to make sure we don't lose any args
				if (!added) args.add(arg);
				}
			}
		}



