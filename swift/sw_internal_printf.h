/**
*** @internal
*** @file		sw_internal_printf.h
*** @brief		Used internally by sw_printf and SWString
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains definitions used internally by sw_printf and SWString::format.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#ifndef __sw_internal_printf_h__
#define __sw_internal_printf_h__

#include <swift/swift.h>
#include <swift/sw_printf.h>
#include <swift/SWString.h>
#include <swift/SWMemoryManager.h>

/// Structure for %k conversion
typedef struct kentry
	{
	const char	*preferredname;		///< The preferred name
	const char	*shortname;			///< The short name
	const char	*longname;			///< The long name
	int			defaultprecision;	///< The default precision
	} KEntry;

/// Internal table used for %k conversion
static KEntry	klist[] =
{
	//	Preferred	Short		Long		Precision
	{	"Bytes",	"B",		"Bytes",		0	},
	{	"KB",		"KB",		"Kilobytes",	1	},
	{	"MB",		"MB",		"Megabytes",	1	},
	{	"GB",		"GB",		"Gigabytes",	2	},
	{	"TB",		"TB",		"Terabytes",	2	},
	{	"PB",		"PB",		"Petabytes",	2	},
	{	"EB",		"EB",		"Exabytes",		3	},
	{	"ZB",		"ZB",		"Zettabytes",	3	},
	{	"YB",		"YB",		"Yottabytes",	3	},
	{	"BB",		"BB",		"Brontobytes",	4	},
	{	0,			0,			0,				0	}
};

// suffixes and default precisions


// I/O types
#define IO_TYPE_CHAR		sizeof(char)	///< Normal char
#define IO_TYPE_WCHAR		sizeof(wchar_t)	///< Wide char

#define IO_TYPE_FILE		100					///< FILE output (output only)
#define IO_TYPE_CUSTOM		101					///< Custom output via function (output only)
#define IO_TYPE_TEXTFILE	102					///< SWTextFile output (output only)

// Flags
#define SW_PRINTF_FLAG_TSTRING		0x1			///< Use TCHAR mode (changes the meaning of %s and %c)
#define SW_PRINTF_FLAG_LIMIT		0x2			///< The output size (buffer) is limited
#define SW_PRINTF_FLAG_REALLOC		0x4			///< The buffer can be realloc.


/**
*** @brief	Internal structure used to control the main prinf processing function.
**/
typedef struct _outputinfo_
	{
	int		inputtype;				///< the input type (see above)
	int		outputtype;				///< the output type (see above)
	int		flags;					///< various flags to control input and output (see above)
	int		count;					///< the number of chars output (used by %n)
	int		limit;					///< the output limit for limited length output types
	bool	nulterminate;			///< a null-teminate flag (if true output is terminated)

	/// A union of input string pointers
	union
	    {
	    const char		*cp;	///< A char string pointer
	    const wchar_t	*wp;	///< A quad-byte char string pointer
	    } in;

	/// A union of output methods
	union
	    {
	    char		*cp;						///< A char string buffer
	    wchar_t		*wp;						///< A quad-byte char string buffer
	    FILE		*fp;						///< A FILE
	    SWTextFile	*tp;						///< A SWTextFile pointer
	    struct
			{
			sw_printf_outputfunction	*func;	///< A custom output function
			void						*data;	///< Data parameter to be passed to the custom function
			} custom;
	    } out;

	// Added to support SWString
	SWMemoryManager	*pMemoryManager;	///< The memory manager to use for reallocs
	int				outpos;				///< the output position (not the same as count when using multibyte strings)
	void			*pBuffer;			///< The memory buffer (passed to realloc)
	} outputinfo;


/// Internal printf function
int		sw_internal_doprintf(outputinfo *info, va_list ap);

#endif
