/**
*** @file		SWDataBuffer.h
*** @brief		A general purpose self-contained memory buffer
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWDataBuffer class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWDataBuffer_h__
#define __SWDataBuffer_h__

#include <swift/SWBuffer.h>




class SWIFT_DLL_EXPORT SWDataBuffer : public SWBuffer
		{
friend class SWString;

public:
		/// Default constructor
		SWDataBuffer(SWMemoryManager *pMemoryManager=NULL);

		/// Sized
		SWDataBuffer(sw_size_t bufsize, SWMemoryManager *pMemoryManager=NULL);

		/// Fixed buffer constructor
		SWDataBuffer(void *pBuf, sw_size_t bufsize);

		/// Fixed buffer constructor
		SWDataBuffer(const void *pBuf, sw_size_t bufsize);

		/// Virtual destructor
		virtual ~SWDataBuffer();

		/// Copy constructor
		SWDataBuffer(const SWDataBuffer &other, SWMemoryManager *pMemoryManager=NULL);

		/// Assignment operator.
		SWDataBuffer &	operator=(const SWDataBuffer &other);

		/// Clear the buffer
		virtual void		clear();

		/// Return the size of the buffer in bytes
		virtual sw_size_t	size() const;

		/// Set the size of the buffer in bytes
		virtual void		size(sw_size_t size, bool preserveData=true, bool clearNewData=true, bool freeExtraMemory=false);


protected:
		sw_size_t		m_size;			///< The size of the memory buffer
		};




#endif
