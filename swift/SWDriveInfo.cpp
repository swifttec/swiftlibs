/**
*** @file		SWDriveInfo.cpp
*** @brief		A class representing a drive entry
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWDriveInfo class.
**/

/*********************************************************************************
Copyright (c) 2006-2017 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWFilename.h>
#include <swift/SWDriveInfo.h>
#include <swift/sw_misc.h>

#if defined(SW_PLATFORM_WINDOWS)
	#include <winioctl.h>
	#include <io.h>
#else
	#include <unistd.h>

	#if defined(SW_PLATFORM_LINUX) || defined (SW_PLATFORM_MACH)
		#include <utime.h>
		#include <sys/time.h>
	#endif
#endif

#include <sys/stat.h>



SWDriveInfo::SWDriveInfo()
		{
		clear();
		}


// Copy constructor
SWDriveInfo::SWDriveInfo(const SWDriveInfo &v) :
	SWObject()
		{
		*this = v;
		}


const SWDriveInfo &
SWDriveInfo::operator=(const SWDriveInfo &v)
		{
#define COPY(x)	x = v.x
		COPY(m_typeFlags);
		COPY(m_fileSystemFlags);
		COPY(m_fileSystemName);
		COPY(m_devicePath);
		COPY(m_deviceSerialNo);
		COPY(m_volumeName);
		COPY(m_volumeFlags);
		COPY(m_mountPointList);
#undef COPY

		return *this;
		}


void
SWDriveInfo::clear()
		{
		SWObject::clear();

		sw_misc_clear(m_typeFlags);
		sw_misc_clear(m_fileSystemFlags);
		sw_misc_clear(m_fileSystemName);
		sw_misc_clear(m_devicePath);
		sw_misc_clear(m_deviceSerialNo);
		sw_misc_clear(m_volumeName);
		sw_misc_clear(m_volumeFlags);
		sw_misc_clear(m_mountPointList);
		}


SWString
SWDriveInfo::volumeFlagsAsString() const
		{
		SWString	res;

		if ((m_volumeFlags & VolumeReadable) != 0)
			{
			if (!res.isEmpty()) res += ", ";
			res += "Readable";
			}

		if ((m_volumeFlags & VolumeWriteable) != 0)
			{
			if (!res.isEmpty()) res += ", ";
			res += "Writeable";
			}

		if ((m_volumeFlags & VolumeEraseable) != 0)
			{
			if (!res.isEmpty()) res += ", ";
			res += "Eraseable";
			}

		if ((m_volumeFlags & VolumeRemoveable) != 0)
			{
			if (!res.isEmpty()) res += ", ";
			res += "Removeable";
			}

		if ((m_volumeFlags & VolumeHotplugable) != 0)
			{
			if (!res.isEmpty()) res += ", ";
			res += "Hotplugable";
			}

		if ((m_volumeFlags & VolumeMounted) != 0)
			{
			if (!res.isEmpty()) res += ", ";
			res += "Mounted";
			}

		return res;
		}


SWString
SWDriveInfo::typeAsString() const
		{
		SWString	res;

#define TESTFLAG(x) if ((m_typeFlags & Type##x) != 0) { if (!res.isEmpty()) { res += ", "; } res += #x; }
#define TESTBUS(x) if ((m_typeFlags & TypeBusMask) == Type##x) { if (!res.isEmpty()) { res += ", "; } res += #x; }

		TESTFLAG(Fixed);
		TESTFLAG(Removable);

		TESTBUS(Scsi);
		TESTBUS(Atapi);
		TESTBUS(Ata);
		TESTBUS(1394);
		TESTBUS(Ssa);
		TESTBUS(Fibre);
		TESTBUS(Usb);
		TESTBUS(RAID);
		TESTBUS(iScsi);
		TESTBUS(Sas);
		TESTBUS(Sata);
		TESTBUS(Sd);
		TESTBUS(Mmc);
		TESTBUS(Virtual);
		TESTBUS(FileBackedVirtual);
		TESTBUS(Spaces);
		TESTBUS(Nvme);

		TESTFLAG(Optical);
		TESTFLAG(Network);

		if ((m_typeFlags & TypeFlashDrive) != 0)
			{
			if (!res.isEmpty()) res += ", ";
			res += "Flash Drive";
			}

		if ((m_typeFlags & TypeHardDrive) != 0)
			{
			if (!res.isEmpty()) res += ", ";
			res += "Hard Drive";
			}

		if (res.isEmpty()) res = "Unknown";

		return res;
		}


#ifdef SW_PLATFORM_WINDOWS


#ifdef NEVER
bool GetMountedVolumes(std::vector<std::wstring> &Volumes,
    unsigned int Flags = VolumeReadable | VolumeWriteable,
    unsigned int Mask = VolumeReadable | VolumeWriteable) {

    wchar_t Volume[MAX_PATH] = {0};
    wchar_t* VolumeEndPtr = Volume;

    Flags |= VolumeMounted;
    Mask |= VolumeMounted;
    Flags &= Mask;

    HANDLE hFind = FindFirstVolume(Volume, sizeof(Volume) / sizeof(wchar_t));
    if (hFind != INVALID_HANDLE_VALUE) {
        do {
            bool IsMatching = false;
            VolumeEndPtr = &Volume[wcslen(Volume) - 1];
            *VolumeEndPtr = L'\0';

            HANDLE hDevice = CreateFile(Volume, 0, 0, 0, OPEN_EXISTING, FILE_FLAG_NO_BUFFERING, 0);
            if (hDevice != INVALID_HANDLE_VALUE) {

                unsigned int CurrentFlags = 0;

                DWORD ReturnedSize;
                STORAGE_HOTPLUG_INFO Info = {0};

                if (DeviceIoControl(hDevice, IOCTL_STORAGE_GET_HOTPLUG_INFO, 0, 0, &Info, sizeof(Info), &ReturnedSize, NULL)) {
                    if (Info.MediaRemovable) {
                        CurrentFlags |= VolumeRemoveable;
                    }
                    if (Info.DeviceHotplug) {
                        CurrentFlags |= VolumeHotplugable;
                    }
                }

                DWORD MediaTypeSize = sizeof(GET_MEDIA_TYPES);
                GET_MEDIA_TYPES* MediaType = (GET_MEDIA_TYPES*) new char[MediaTypeSize];

                while (DeviceIoControl(hDevice, IOCTL_STORAGE_GET_MEDIA_TYPES_EX, 0, 0, MediaType, MediaTypeSize, &ReturnedSize, NULL) == FALSE && GetLastError() == ERROR_INSUFFICIENT_BUFFER) {
                    delete [] (char*) MediaType;
                    MediaTypeSize *= 2;
                    MediaType = (GET_MEDIA_TYPES*) new char[MediaTypeSize];
                }

                if (MediaType->MediaInfoCount > 0) {
                    DWORD Characteristics = 0;
                    // Supports: Disk, CD, DVD
                    if (MediaType->DeviceType == FILE_DEVICE_DISK || MediaType->DeviceType == FILE_DEVICE_CD_ROM || MediaType->DeviceType == FILE_DEVICE_DVD) {
                        if (Info.MediaRemovable) {
                            Characteristics = MediaType->MediaInfo[0].DeviceSpecific.RemovableDiskInfo.MediaCharacteristics;
                        } else {
                            Characteristics = MediaType->MediaInfo[0].DeviceSpecific.DiskInfo.MediaCharacteristics;
                        }

                        if (Characteristics & MEDIA_CURRENTLY_MOUNTED) {
                            CurrentFlags |= VolumeMounted;
                        }
                        if (Characteristics & (MEDIA_READ_ONLY | MEDIA_READ_WRITE)) {
                            CurrentFlags |= VolumeReadable;
                        }
                        if (((Characteristics & MEDIA_READ_WRITE) != 0 || (Characteristics & MEDIA_WRITE_ONCE) != 0) && (Characteristics & MEDIA_WRITE_PROTECTED) == 0 && (Characteristics & MEDIA_READ_ONLY) == 0) {
                            CurrentFlags |= VolumeWriteable;
                        }
                        if (Characteristics & MEDIA_ERASEABLE) {
                            CurrentFlags |= VolumeEraseable;
                        }
                    }
                }

                delete [] (char*) MediaType;

                CloseHandle(hDevice);

                CurrentFlags &= Mask;

                if (CurrentFlags == Flags) {
                    *VolumeEndPtr = L'\\';                  
                    wchar_t VolumePaths[MAX_PATH] = {0};
                    if (GetVolumePathNamesForVolumeName(Volume, VolumePaths, MAX_PATH, &ReturnedSize)) {
                        if (*VolumePaths) {
                            Volumes.push_back(VolumePaths);
                        }
                    }
                }

            }
        } while (FindNextVolume(hFind, Volume, sizeof(Volume) / sizeof(wchar_t)));
        FindVolumeClose(hFind);
    }
#endif // NEVER






void
SWDriveInfo::getDriveList(SWArray<SWDriveInfo> &drives)
		{
		drives.clear();

		HANDLE	h;
		WCHAR	vpath[1024], vname[1024], mount[1024];
		DWORD	szNameList=2048, szNeeded=0;
		WCHAR	*pNameList=new WCHAR[szNameList];

		h = FindFirstVolumeW(vpath, 1024);
		if (h != INVALID_HANDLE_VALUE)
			{
			do
				{
				SWDriveInfo	info;
				sw_uint32_t	vflags=0, tflags=0;
				HANDLE		hDevice;

				info.devicePath(vpath);

				hDevice = CreateFile(SWString(vpath).left(wcslen(vpath)-1), 0, 0, 0, OPEN_EXISTING, FILE_FLAG_NO_BUFFERING, 0);
				if (hDevice != INVALID_HANDLE_VALUE)
					{
					DWORD					ReturnedSize;
					STORAGE_HOTPLUG_INFO	hotplugInfo;

					memset(&hotplugInfo, 0, sizeof(hotplugInfo));

					if (DeviceIoControl(hDevice, IOCTL_STORAGE_GET_HOTPLUG_INFO, 0, 0, &hotplugInfo, sizeof(hotplugInfo), &ReturnedSize, NULL))
						{
						if (hotplugInfo.MediaRemovable) vflags |= VolumeRemoveable;
						if (hotplugInfo.DeviceHotplug) vflags |= VolumeHotplugable;
						}

					STORAGE_PROPERTY_QUERY		query;
					STORAGE_ADAPTER_DESCRIPTOR	adapter;

					memset(&query, 0, sizeof(query));
					query.PropertyId = StorageAdapterProperty;
					query.QueryType = PropertyStandardQuery;
					
					if (DeviceIoControl(hDevice, IOCTL_STORAGE_QUERY_PROPERTY, &query, sizeof(query), &adapter, sizeof(adapter), &ReturnedSize, NULL))
						{
						switch (adapter.BusType)
							{
							case BusTypeScsi:				tflags |= TypeScsi;					break;
							case BusTypeAtapi:				tflags |= TypeAtapi;				break;
							case BusTypeAta:				tflags |= TypeAta;					break;
							case BusType1394:				tflags |= Type1394;					break;
							case BusTypeSsa:				tflags |= TypeSsa;					break;
							case BusTypeFibre:				tflags |= TypeFibre;				break;
							case BusTypeUsb:				tflags |= TypeUsb;					break;
							case BusTypeRAID:				tflags |= TypeRAID;					break;
							case BusTypeiScsi:				tflags |= TypeiScsi;				break;
							case BusTypeSas:				tflags |= TypeSas;					break;
							case BusTypeSata:				tflags |= TypeSata;					break;
							case BusTypeSd:					tflags |= TypeSd;					break;
							case BusTypeMmc:				tflags |= TypeMmc;					break;
							case BusTypeVirtual:			tflags |= TypeVirtual;				break;
							case BusTypeFileBackedVirtual:	tflags |= TypeFileBackedVirtual;	break;
							case BusTypeSpaces:				tflags |= TypeSpaces;				break;
							case BusTypeNvme:				tflags |= TypeNvme;					break;
							}
						}

					DWORD				MediaTypeSize = sizeof(GET_MEDIA_TYPES);
					GET_MEDIA_TYPES		*MediaType = (GET_MEDIA_TYPES *) new char[MediaTypeSize];

					while (DeviceIoControl(hDevice, IOCTL_STORAGE_GET_MEDIA_TYPES_EX, 0, 0, MediaType, MediaTypeSize, &ReturnedSize, NULL) == FALSE && GetLastError() == ERROR_INSUFFICIENT_BUFFER)
						{
						delete [] (char*) MediaType;
						MediaTypeSize *= 2;
						MediaType = (GET_MEDIA_TYPES*) new char[MediaTypeSize];
						}

					if (MediaType->MediaInfoCount > 0)
						{
						DWORD Characteristics = 0;

						// Supports: Disk, CD, DVD
						if (MediaType->DeviceType == FILE_DEVICE_DISK || MediaType->DeviceType == FILE_DEVICE_CD_ROM || MediaType->DeviceType == FILE_DEVICE_DVD)
							{
							if (hotplugInfo.MediaRemovable)
								{
								Characteristics = MediaType->MediaInfo[0].DeviceSpecific.RemovableDiskInfo.MediaCharacteristics;
								}
							else
								{
								Characteristics = MediaType->MediaInfo[0].DeviceSpecific.DiskInfo.MediaCharacteristics;
								}

							if (Characteristics & MEDIA_CURRENTLY_MOUNTED) vflags |= VolumeMounted;
							if (Characteristics & (MEDIA_READ_ONLY | MEDIA_READ_WRITE)) vflags |= VolumeReadable;
							if (((Characteristics & MEDIA_READ_WRITE) != 0
								|| (Characteristics & MEDIA_WRITE_ONCE) != 0)
								&& (Characteristics & MEDIA_WRITE_PROTECTED) == 0
								&& (Characteristics & MEDIA_READ_ONLY) == 0)
								{
								vflags |= VolumeWriteable;
								}
				
							if (Characteristics & MEDIA_ERASEABLE) vflags |= VolumeEraseable;
							}
						}

					delete [] (char*) MediaType;

					CloseHandle(hDevice);
					}

				BOOL	b;

				b = GetVolumePathNamesForVolumeNameW(vpath, pNameList, szNameList, &szNeeded);
				if (!b && GetLastError() == ERROR_MORE_DATA)
					{
					delete [] pNameList;
					szNameList = szNeeded + 1;
					pNameList=new WCHAR[szNameList];
					
					b = GetVolumePathNamesForVolumeNameW(vpath, pNameList, szNameList, &szNeeded);
					}

				if (b)
					{
					WCHAR	*pName=pNameList;

					while (*pName != 0)
						{
						info.addMountPoint(pName);
						pName += wcslen(pName) + 1;
						}
					}

				HANDLE	m=FindFirstVolumeMountPointW(vpath, mount, 1024);

				if (m != INVALID_HANDLE_VALUE)
					{
					do
						{
						info.addMountPoint(mount);
						}
					while (FindNextVolumeMountPointW(m, mount, 1024));
					FindVolumeMountPointClose(m);
					}

				UINT	dt=GetDriveType(vpath);

				switch (dt)
					{
					case DRIVE_UNKNOWN:
					case DRIVE_NO_ROOT_DIR:
						// ignore these two
						break;

					case DRIVE_REMOVABLE:
						tflags |= TypeRemovable;
						break;

					case DRIVE_FIXED:
						if ((vflags & (VolumeRemoveable | VolumeHotplugable)) != 0)
							{
							tflags |= TypeRemovable;
							}
						else
							{
							tflags |= TypeFixed;
							}
						break;

					case DRIVE_REMOTE:
						tflags |= TypeNetwork;
						break;

					case DRIVE_CDROM:
						tflags |= TypeOptical;
						break;

					case DRIVE_RAMDISK:
						tflags |= TypeRamDisk;
						break;
					}


				DWORD	volumeSerialNumber, maximumComponentLength, fileSystemFlags;
				WCHAR	fileSystemNameBuffer[1024];

				if (GetVolumeInformation(vpath, vname, 1024, &volumeSerialNumber, &maximumComponentLength, &fileSystemFlags, fileSystemNameBuffer, 1024))
					{
					sw_uint32_t	flags=0;

					if ((fileSystemFlags & FILE_CASE_SENSITIVE_SEARCH) != 0) flags |= FileSystemCaseSensitiveSearch;
					if ((fileSystemFlags & FILE_CASE_PRESERVED_NAMES) != 0) flags |= FileSystemCasePreservedNames;
					if ((fileSystemFlags & FILE_UNICODE_ON_DISK) != 0) flags |= FileSystemUnicodeOnDisk;
					if ((fileSystemFlags & FILE_PERSISTENT_ACLS) != 0) flags |= FileSystemPersistenACLS;
					if ((fileSystemFlags & FILE_FILE_COMPRESSION) != 0) flags |= FileSystemSupportsFileCompression;
					if ((fileSystemFlags & FILE_VOLUME_QUOTAS) != 0) flags |= FileSystemSupportsQuotas;
					if ((fileSystemFlags & FILE_SUPPORTS_SPARSE_FILES) != 0) flags |= FileSystemSupportsSparseFiles;
					if ((fileSystemFlags & FILE_SUPPORTS_REPARSE_POINTS) != 0) flags |= FileSystemSupportsReparsePoints;
					if ((fileSystemFlags & FILE_SUPPORTS_REMOTE_STORAGE) != 0) flags |= FileSystemSupportsRemoteStorage;
					if ((fileSystemFlags & FILE_VOLUME_IS_COMPRESSED) != 0) flags |= FileSystemIsCompressed;
					if ((fileSystemFlags & FILE_SUPPORTS_OBJECT_IDS) != 0) flags |= FileSystemSupportsObjectIDs;
					if ((fileSystemFlags & FILE_SUPPORTS_ENCRYPTION) != 0) flags |= FileSystemSupportsEncryption;
					if ((fileSystemFlags & FILE_NAMED_STREAMS) != 0) flags |= FileSystemSupportsNamedStreams;
					if ((fileSystemFlags & FILE_READ_ONLY_VOLUME) != 0) flags |= FileSystemIsReadOnly;
					if ((fileSystemFlags & FILE_SEQUENTIAL_WRITE_ONCE) != 0) flags |= FileSystemIsSequentialWriteOnce;
					if ((fileSystemFlags & FILE_SUPPORTS_TRANSACTIONS) != 0) flags |= FileSystemSupportsTransactions;
					if ((fileSystemFlags & FILE_SUPPORTS_HARD_LINKS) != 0) flags |= FileSystemSupportsHardLinks;
					if ((fileSystemFlags & FILE_SUPPORTS_EXTENDED_ATTRIBUTES) != 0) flags |= FileSystemSupportsExtendedAttributes;
					if ((fileSystemFlags & FILE_SUPPORTS_OPEN_BY_FILE_ID) != 0) flags |= FileSystemSupportsOpenByFileID;
					if ((fileSystemFlags & FILE_SUPPORTS_USN_JOURNAL) != 0) flags |= FileSystemSupportsUsnJournal;
					if ((fileSystemFlags & FILE_SUPPORTS_INTEGRITY_STREAMS) != 0) flags |= FileSystemSupportsIntegrityStreams;

					info.fileSystemFlags(flags);
					info.fileSystemName(fileSystemNameBuffer);
					info.deviceSerialNo(SWString().format("%08x", volumeSerialNumber));
					info.volumeName(vname);
					}

				if (tflags != 0)
					{
					info.volumeFlags(vflags);
					info.type(tflags);
					drives.add(info);
					}
				}
			while (FindNextVolumeW(h, vpath, 1024));

			FindVolumeClose(h);
			}
		}
#else
void
SWDriveInfo::getDriveList(SWArray<SWDriveInfo> &drives)
		{
		drives.clear();
		}
#endif


