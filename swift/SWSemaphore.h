/**
*** @file		SWSemaphore.h
*** @brief		An abstract semaphore class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWSemaphore class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWSemaphore_h__
#define __SWSemaphore_h__


#include <swift/SWException.h>
#include <swift/SWMutex.h>




/**
*** @brief	An abstract Dijkstra style general semaphore class.
***
*** SWSemaphore is an abstract  Dijkstra style general semaphore class that
*** forms the base of all semaphore classes.
*** 
*** An example of semaphore usage would be to use it to assist in allocating a limited
*** of resources. The initial count would be set to the number of available resources.
*** Each thread/process that takes a resource would first acquire the semaphore, causing
*** the semaphore count to decrement. If the semaphore count was zero (i.e. when no
*** resource was available) the thread/process would block until a thread/process
*** released a semaphore (incrementing the semaphore count) to indicate that a resource
*** was available again.
***
*** As with other SWMutex derived classes there are Thread-based and process-based
*** derivatives.
***
*** @author		Simon Sparkes
*** @see		SWThreadSemaphore, SWProcessSemaphore
**/
class SWIFT_DLL_EXPORT SWSemaphore : public SWMutex
		{
protected:
		/// Protected constructor - to allow construction only by derived classes
		SWSemaphore();

public:
		virtual ~SWSemaphore();

		/// This has no meaning for a semaphore and always returns false
		virtual bool	hasLock()		{ return false; }

		/// This has no meaning for a semaphore and always returns false
		virtual bool	isLocked()		{ return false; }
		};





#endif
