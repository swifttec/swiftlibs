/**
*** @file		SWStringArray.cpp
*** @brief		An array template
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWStringArray class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWGuard.h>
#include <swift/SWException.h>
#include <swift/SWStringArray.h>
#include <swift/SWInputStream.h>
#include <swift/SWOutputStream.h>
#include <swift/SWTokenizer.h>




class SWStringArrayElement
		{
public:
		/// Default Constructor
		SWStringArrayElement()										{ }

		/// Data copy constructor
		SWStringArrayElement(const SWString &data) : m_data(data)	{ }

		/// Custom new operator
		void *			operator new(size_t nbytes, sw_size_t &res)	{ res = nbytes; return malloc(nbytes); }

		/// Custom new operator
		void *			operator new(size_t /*nbytes*/, void *p)	{ return p; }

		/// Custom delete operator
		void			operator delete(void *, sw_size_t &)		{ }

		/// Custom delete operator
		void			operator delete(void *, void *)				{ }

		/// Custom delete operator
		void			operator delete(void *)						{ }

		/// Get data reference
		SWString &				data()										{ return m_data; }

private:
		SWString	m_data;		///< The actual data
		};


SWStringArray::SWStringArray(sw_size_t size) :
	SWAbstractArray(NULL, sizeof(SWStringArrayElement))
		{
		// Make sure we have space for some (8) elements
		ensureCapacity(size);
		}


SWStringArray::~SWStringArray()
		{
		clear();
		}


SWStringArray::SWStringArray(const SWStringArray &other) :
	SWAbstractArray(NULL, sizeof(SWStringArrayElement))
		{
		// Copy the data via the assigment operator
		*this = other;
		}


SWStringArray &
SWStringArray::operator=(const SWStringArray &other)
		{
		sw_index_t	n = (sw_index_t)other.size();

		clear();

		// Do it backwards to avoid doing multiple reallocs
		while (n > 0)
			{
			--n;
			(*this)[n] = other.get(n);
			}

		return *this;
		}


void
SWStringArray::ensureCapacity(sw_size_t nelems)
		{
		sw_size_t		wanted=nelems * m_elemsize;

		if (wanted > m_capacity)
			{
			SWWriteGuard	guard(this);	// Guard this object

			if (m_capacity == 0) m_capacity = 128;

			while (m_capacity < wanted)
				{
				m_capacity <<= 1;
				}

			char	*pOldData = m_pData;

			m_pData = (char *)m_pMemoryManager->malloc(m_capacity);
			if (m_pData == NULL)
				{
				m_pData = pOldData;
				throw SW_EXCEPTION(SWMemoryException);
				}

			if (pOldData != NULL)
				{
				// We must "new" all the existing data elements
				char		*pTo=(char *)m_pData;
				char		*pFrom=(char *)pOldData;
				sw_size_t	i, n=size();

				for (i=0;i<n;i++)
					{
					new (pTo) SWStringArrayElement(((SWStringArrayElement *)pFrom)->data());
					delete (SWStringArrayElement *)pFrom;
					pTo += m_elemsize;
					pFrom += m_elemsize;
					}

				m_pMemoryManager->free(pOldData);
				}
			}
		}


SWString &
SWStringArray::operator[](sw_index_t pos)
		{
		if (pos < 0) throw SW_EXCEPTION(SWIndexOutOfBoundsException);

		SWWriteGuard	guard(this);

		ensureCapacity((sw_size_t)pos + 1);

		while (m_nelems <= (sw_size_t)pos)
			{
			// Make sure all of the interim elements are valid
			new (m_pData + (m_nelems * m_elemsize)) SWStringArrayElement;
			m_nelems++;
			}

		return ((SWStringArrayElement *)(m_pData + (pos * m_elemsize)))->data();
		}


SWString &
SWStringArray::get(sw_index_t pos) const
		{
		if (pos < 0 || pos >= (sw_index_t)m_nelems) throw SW_EXCEPTION(SWIndexOutOfBoundsException);

		SWReadGuard	guard(this);

		return ((SWStringArrayElement *)(m_pData + (pos * m_elemsize)))->data();
		}


void
SWStringArray::clear()
		{
		SWWriteGuard		guard(this);
		SWStringArrayElement	*pElement;
		sw_size_t			pos=m_nelems;

		while (pos > 0)
			{
			--pos;
			pElement = ((SWStringArrayElement *)(m_pData + (pos * m_elemsize)));
			delete pElement;
			}

		m_nelems = 0;
		}


void
SWStringArray::remove(sw_index_t pos, sw_size_t count)
		{
		if (count > 0)
			{
			SWWriteGuard	guard(this);

			if (pos >= 0 && pos < (sw_index_t)m_nelems)
				{
				if (pos + count > m_nelems) count = m_nelems - pos;

				if (count > 0)
					{
					sw_index_t	startpos=pos, endpos=pos+(sw_index_t)count;
					char		*p = m_pData + (startpos * m_elemsize);
					char		*q = m_pData + (endpos * m_elemsize);
					sw_size_t	oldn = m_nelems;

					// Reduce the number of elems and shift all the elems down
					for (pos=endpos;pos<(sw_index_t)size();pos++)
						{
						((SWStringArrayElement *)p)->data() = ((SWStringArrayElement *)q)->data();
						p += m_elemsize;
						q += m_elemsize;
						}

					 // Blank out the rest
					pos = (sw_index_t)(m_nelems - count);
					p = m_pData + (pos * m_elemsize);
					while (pos < (sw_index_t)oldn)
						{
						delete (SWStringArrayElement *)p;
						p += m_elemsize;
						pos++;
						}

					m_nelems -= count;
					}
				}
			else throw SW_EXCEPTION(SWIndexOutOfBoundsException);
			}
		}



void
SWStringArray::add(const SWString &v)
		{
		insert(v, (sw_index_t)m_nelems);
		}


void
SWStringArray::add(sw_resourceid_t ids)
		{
		SWString	s;

		s.load(ids);
		insert(s, (sw_index_t)m_nelems);
		}


void
SWStringArray::insert(sw_resourceid_t ids, sw_index_t pos)
		{
		SWString	s;

		s.load(ids);
		insert(s, pos);
		}


void
SWStringArray::insert(const SWString &v, sw_index_t pos)
		{
		SWWriteGuard	guard(this);

		if (pos >= 0 && pos <= (sw_index_t)m_nelems)
			{
			ensureCapacity(m_nelems+1);

			sw_index_t	ipos = (sw_index_t)m_nelems;
			char		*p = m_pData + (ipos * m_elemsize);
			char		*q = p - m_elemsize;
			bool		exists=false;

			// Increase the number of elems by one and shift all the elems up by 1
			// not forgetting to make sure we have enough space first!
			if (ipos > pos)
				{
				new (p) SWStringArrayElement((((SWStringArrayElement *)q)->data()));
				p -= m_elemsize;
				q -= m_elemsize;
				ipos--;
				exists = true;
				}
			
			while (ipos > pos)
				{
				(((SWStringArrayElement *)p)->data()) = (((SWStringArrayElement *)q)->data());
				p -= m_elemsize;
				q -= m_elemsize;
				ipos--;
				}
				
			if (exists) ((SWStringArrayElement *)p)->data() = v;
			else new (p) SWStringArrayElement(v);

			m_nelems++;
			}
		else throw SW_EXCEPTION(SWIndexOutOfBoundsException);
		}


void
SWStringArray::push(const SWString &v)
		{
		add(v);
		}


SWString
SWStringArray::pop()
		{
		if (m_nelems < 1) throw SW_EXCEPTION(SWIndexOutOfBoundsException);

		SWWriteGuard	guard(this);

		SWString	v=get((sw_index_t)m_nelems-1);

		remove((sw_index_t)m_nelems-1, 1);

		return v;
		}


int
SWStringArray::compareTo(const SWStringArray &other) const
		{
		SWReadGuard	guard(this);
		int			nelems1=(int)size();
		int			nelems2=(int)other.size();
		int			pos=0;
		int			res=0;

		while (res == 0 && pos < nelems1 && pos < nelems2)
			{
			res = get(pos).compareTo(other.get(pos));
			pos++;
			}

		if (res == 0)
			{
			if (nelems1 > nelems2) res = 1;
			else if (nelems1 < nelems2) res = -1;
			}

		return res;
		}




sw_status_t
SWStringArray::writeToStream(SWOutputStream &stream) const
		{
		SWReadGuard	guard(this);
		sw_status_t	res;

		res = stream.write((sw_uint32_t)size());
		for (int i=0;res==SW_STATUS_SUCCESS&&i<(int)size();i++)
			{
			res = get(i).writeToStream(stream);
			}

		return res;
		}


sw_status_t
SWStringArray::readFromStream(SWInputStream &stream)
		{
		SWWriteGuard	guard(this);
		sw_status_t		res;
		sw_uint32_t		i, nelems;
		SWString		s;

		clear();
		res = stream.read(nelems);
		for (i=0;res==SW_STATUS_SUCCESS&&i<nelems;i++)
			{
			res = s.readFromStream(stream);
			if (res == SW_STATUS_SUCCESS) add(s);
			}

		return res;
		}


/*
** Turn the the string array into a string with the elements separated 
** with the supplied separator character
**
** @param sep		The separator char to be inserted between the elements
** @param squote	The char to use to prefix each element
** @param equote	The char to use to postfix each element
** @param meta		The char to prefix any quote string with
**
** @return The assembled string
*/
SWString
SWStringArray::convertToString(int sep, int squote, int equote, int meta) const
		{
		SWString	str, tmp;
		SWString	rsquote, requote, rmeta, rsep;	// The replacement strings

		if (sep) rsep.format(_T("%c%c"), meta, sep);
		if (squote) rsquote.format(_T("%c%c"), meta, squote);
		if (equote) requote.format(_T("%c%c"), meta, equote);
		if (meta) rmeta.format(_T("%c%c"), meta, meta);

		for (int i=0;i<(int)size();i++)
			{
			if (i) str += sep;

			tmp = get(i);

			// First if we have a quote string replace all instances of itself
			// with a double version of itself.
			if (meta) tmp.replaceAll(SWString(meta), rmeta);

			// if we have a separator string and don't have quote strings
			// then replace all the separator chars in the strings
			if (sep && meta && !squote && !equote) tmp.replaceAll(SWString(sep), rsep);

			// Finally replace all the contained quotes
			if (squote && meta) tmp.replaceAll(SWString(squote), rsquote);
			if (equote && meta && squote != equote) tmp.replaceAll(SWString(equote), requote);

			// Now we can append to the result string
			if (squote) str += squote;
			str += tmp;
			if (equote) str += equote;
			}
		
		return str;
		}



/*
** Take the string given and load the array from it by successive
** calls to add.
**
** @param	s	The string to be split.
** @param	sep	The separator character (default ',')
** @param	squote	The start-of-quoted string character (default '"')
** @param	equote	The end-of-quoted string character (default '"')
** @param	meta	The meta character which can be used to prefix separator and quote chars. (default '\')
** @param	append	A flag to indicate if the strings should be appended to the array (true) or if the array 
**			should be cleared before starting the split (false). The default action is to clear the
**			array.
*/
void
SWStringArray::convertFromString(const SWString &s, int sep, int squote, int equote, int meta, bool append)
		{
		SWString	tmp(s);
		SWString	sepChars;

		sepChars = sep;

		SWTokenizer tok(tmp, sepChars, false, false);

		tok.addQuotes(squote, equote);
		tok.setMetaChar(meta);

		if (!append) clear();
		while (tok.hasMoreTokens()) add(tok.nextToken());
		}





