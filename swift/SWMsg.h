/****************************************************************************
**	SWMsg.h	A class for encapsulating variable length messages
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWMsg_h__
#define __SWMsg_h__

#include <swift/SWObject.h>
#include <swift/SWVector.h>
#include <swift/SWMemoryInputStream.h>
#include <swift/SWMemoryOutputStream.h>
#include <swift/SWValue.h>
#include <swift/SWMsgField.h>




// Forward declarations

#if defined(SW_PLATFORM_WINDOWS)
		// class needs to have dll-interface to be used by clients of other class
#endif

/**
*** A class for encapsulating variable length messages
**/
class SWIFT_DLL_EXPORT SWMsg : public SWObject
		{
public:
		// Default constructor - a blank message with no fields and an id of 0
		SWMsg(sw_uint32_t id=0);

		// Memory buffer constructor - builds a message from the memory buffer
		SWMsg(const char *buf);

		// Copy constructor
		SWMsg(const SWMsg &m);

		// Assignment operator
		SWMsg &	operator=(const SWMsg &m);

		virtual ~SWMsg();


		// Reset message to empty state
		void		clear();			

		// Add a field to this message
		void		add(const SWMsgField &f);		

		void		add(sw_uint32_t id, bool v);				///< Constructs a value with the type VtNone
		void		add(sw_uint32_t id, sw_int8_t v);			///< Constructs a value with the type VtInt8
		void		add(sw_uint32_t id, sw_int16_t v);			///< Constructs a value with the type VtInt16
		void		add(sw_uint32_t id, sw_int32_t v);			///< Constructs a value with the type VtInt16
		void		add(sw_uint32_t id, sw_int64_t v);			///< Constructs a value with the type VtInt16
		void		add(sw_uint32_t id, sw_uint8_t v);			///< Constructs a value with the type VtUInt8
		void		add(sw_uint32_t id, sw_uint16_t v);			///< Constructs a value with the type VtUInt8
		void		add(sw_uint32_t id, sw_uint32_t v);			///< Constructs a value with the type VtUInt8
		void		add(sw_uint32_t id, sw_uint64_t v);			///< Constructs a value with the type VtUInt8
		void		add(sw_uint32_t id, float v);				///< Constructs a value with the type VtFloat
		void		add(sw_uint32_t id, double v);				///< Constructs a value with the type VtDouble
		void		add(sw_uint32_t id, const char *v);			///< Constructs a value with the type VtString
		void		add(sw_uint32_t id, const wchar_t *v);		///< Constructs a value with the type VtWString
		void		add(sw_uint32_t id, const void *, size_t len);	///< Constructs a value with the type VtData from the data pointer and size
		void		add(sw_uint32_t id, const SWBuffer &v);			///< Constructs a value with the type VtData from SWBuffer
		void		add(sw_uint32_t id, const SWString &v);		///< Constructs a value with a string type of VtString or VtWString depending on the SWString
		void		add(sw_uint32_t id, const SWDate &v);		///< Constructs a value with the type VtDate
		void		add(sw_uint32_t id, const SWTime &v);		///< Constructs a value with the type VtTime
		void		add(sw_uint32_t id, const SWMoney &v);		///< Constructs a value with the type VtMoney

#if defined(SW_BUILD_MFC)
		void		add(sw_uint32_t id, const COleVariant &v);
		void		add(sw_uint32_t id, const CString &v);
#endif

		void		add(sw_uint32_t id, const SWMsg &);
		void		add(sw_uint32_t id, const SWValue &);


		// Update data
		void		id(sw_uint32_t v)			{ m_id = v; }

		// Retrieve data
		sw_uint32_t		id() const					{ return m_id; }
		//int			length() const				{ return m_length; }
		//char *		toByteArray(int *l);


		// Retrieve fields
		sw_size_t	getFieldCount() const		{ return m_fieldVec.size(); }

		SWMsgField	getField(int n) const;
		bool		findField(sw_uint32_t id, SWMsgField &mf) const;
		bool		findField(sw_uint32_t id, SWMsgField &mf);

		/// Return an iterator of SWMsgField pointers
		SWIterator<SWMsgField>	iterator()		{ return m_fieldVec.iterator(); }

		// To/From memory
		sw_status_t				readFromBuffer(const void *mem);
		sw_status_t				writeToBuffer(SWBuffer &buf) const;

		// Debug
		virtual void	dump();


public: // Virtual overrides
		virtual sw_status_t		readFromStream(SWInputStream &s);
		virtual sw_status_t		writeToStream(SWOutputStream &s) const;


private: // Methods
		void		initMsg(int id);

private: // Vars
		sw_uint32_t				m_id;		// The id of this message
		//sw_uint32_t				m_length;	// The total length of the data of the message and fields
		SWVector<SWMsgField>	m_fieldVec;	// The vector of fields
		};




#endif
