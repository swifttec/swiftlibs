/**
*** @file		SWUdpSocket.cpp
*** @brief		A TCP socket
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWUdpSocket class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <swift/SWGuard.h>

#include <swift/SWUdpSocket.h>
#include <swift/sw_net.h>


#if !defined(SW_PLATFORM_WINDOWS)
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>
#else
	#if defined(SW_BUILD_MFC)
		#include <winsock2.h>
	#endif
#endif


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




SWUdpSocket::SWUdpSocket()
		{
		}


SWUdpSocket::~SWUdpSocket()
		{
		close();
		}


sw_status_t
SWUdpSocket::close()
		{
		SWGuard		guard(this);
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (isListening() || isConnected())
			{
			res = SWSocket::shutdown(_fd, 2);
			}

		if (_fd != SW_NET_INVALID_SOCKET)
			{
			res = SWSocket::close(_fd);
			_fd = SW_NET_INVALID_SOCKET;
			_flags.clear();
			}

		return setLastError(res);
		}


sw_status_t
SWUdpSocket::open()
		{
		SWGuard		guard(this);

		// Attempt to create the socket
		return setLastError(SWSocket::open(AF_INET, SOCK_DGRAM, 0, _fd));
		}



sw_status_t
SWUdpSocket::sendTo(const SWSocketAddress &addr, const void *pBuffer, sw_size_t bytesToWrite, sw_size_t *pBytesWritten)
		{
		SWGuard		guard(this);
		sw_status_t	res=SW_STATUS_SUCCESS;

		res = SWSocket::sendto(_fd, addr, pBuffer, bytesToWrite, pBytesWritten);

		return setLastError(res);
		}


sw_status_t
SWUdpSocket::receiveFrom(SWSocketAddress &addr, void *pBuffer, sw_size_t bytesToRead, sw_size_t *pBytesRead)
		{
		SWGuard		guard(this);
		sw_status_t	res=SW_STATUS_SUCCESS;

		res = SWSocket::recvfrom(_fd, addr, pBuffer, bytesToRead, pBytesRead);

		return setLastError(res);
		}




