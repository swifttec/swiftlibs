/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <swift/SWThreadSemaphore.h>
#include <swift/SWTime.h>

#if defined(SW_USE_WINDOWS_SEMAPHORES)
	#define PSEMAPHORE	(m_hSemaphore)
#elif defined(SW_USE_SOLARIS_SEMAPHORES)
	#include <synch.h>

	typedef sema_t		sw_thread_semaphore_t;
	#define PSEMAPHORE	((sema_t *)m_hSemaphore)
#elif defined(SW_USE_SYSV_SEMAPHORES)
	#include <sys/sem.h>
	typedef int		sw_thread_semaphore_t;
	#define PSEMAPHORE	((int *)m_hSemaphore)
#elif defined(SW_USE_POSIX_SEMAPHORES)
	#include <semaphore.h>

	typedef sem_t		sw_thread_semaphore_t;
	#define PSEMAPHORE	((sem_t *)m_hSemaphore)
#else
	#error Unsupported platform - please update this file and/or sw_platform.h
#endif


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif






// Constructor
SWThreadSemaphore::SWThreadSemaphore(int count, int maxcount) :
	m_hSemaphore(0)
		{
#if defined(SW_USE_POSIX_SEMAPHORES)
		int		r=0;

		m_hSemaphore = new sem_t;
		if (m_hSemaphore == NULL) throw SW_EXCEPTION(SWMemoryAllocationException);

		r = sem_init(PSEMAPHORE, 0, count);
		if (r != 0) throw SW_EXCEPTION(SWMutexCreateException);
#elif defined(SW_USE_SYSV_SEMAPHORES)
		m_hSemaphore = new int;
		if (m_hSemaphore == NULL) throw SW_EXCEPTION(SWMemoryAllocationException);

		int	r;
		r = semget(IPC_PRIVATE, 1, SEM_A);
		if (r == -1) throw SW_EXCEPTION(SWMutexCreateException);
		else
			{
			*PSEMAPHORE = r;

			struct sembuf	sb;

			memset(&sb, 0, sizeof(sb));
			sb.sem_num = 0;
			sb.sem_op = count;
			sb.sem_flg = 0;

			semop(*PSEMAPHORE, &sb, 1);
			}
#elif defined(SW_USE_WINDOWS_SEMAPHORES)
		m_hSemaphore = ::CreateSemaphore(NULL, count, maxcount, NULL);
#else
		#error Unsupported platform - please update SWThreadSemaphore.cpp
#endif
		}

// Destructor
SWThreadSemaphore::~SWThreadSemaphore()
		{
#if defined(SW_USE_POSIX_SEMAPHORES)
		int		r=0;

		r = sem_destroy(PSEMAPHORE);
		delete PSEMAPHORE;
#elif defined(SW_USE_SYSV_SEMAPHORES)
		semctl(*PSEMAPHORE, 0, IPC_RMID);
		delete PSEMAPHORE;
#elif defined(SW_USE_WINDOWS_SEMAPHORES)
		::CloseHandle(PSEMAPHORE);
#else
		#error Unsupported platform - please update SWThreadSemaphore.cpp
#endif
		}


// Acquire the semaphore
sw_status_t
SWThreadSemaphore::acquire(sw_uint64_t waitms)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

#if defined(SW_USE_POSIX_SEMAPHORES) || defined(SW_USE_SYSV_SEMAPHORES)
		if (waitms == SW_TIMEOUT_INFINITE)
			{
#if defined(SW_USE_POSIX_SEMAPHORES)
			if (sem_wait(PSEMAPHORE) != 0)
#else
			struct sembuf	sb;

			memset(&sb, 0, sizeof(sb));
			sb.sem_num = 0;
			sb.sem_op = -1;
			sb.sem_flg = SEM_UNDO;

			if (semop(*PSEMAPHORE, &sb, 1) != 0)
#endif
				{
				res = errno;
				}
			}
		else
			{
			SWTime	stime, etime;

			stime.update();
			for (;;)
				{
#if defined(SW_USE_POSIX_SEMAPHORES)
				if (sem_trywait(PSEMAPHORE) != 0)
#else
				struct sembuf	sb;

				memset(&sb, 0, sizeof(sb));
				sb.sem_num = 0;
				sb.sem_op = -1;
				sb.sem_flg = SEM_UNDO|IPC_NOWAIT;

				if (semop(*PSEMAPHORE, &sb, 1) != 0)
#endif
					{
					if (errno == EBUSY || errno == EAGAIN || errno == 0)
						{
						etime.update();

						sw_uint64_t	elapsed = (sw_uint64_t)(((double)etime - (double)stime) * 1000.0);
						if (elapsed >= waitms)
							{
							res = SW_STATUS_TIMEOUT;
							break;
							}
						else
							{
							// Sleep for 1ms and try again
							SWThread::sleep(1);
							}
						}
					else
						{
						res = errno;
						break;
						}
					}
				else
					{
					// We have the lock!
					break;
					}
				}
			}
#elif defined(SW_USE_WINDOWS_SEMAPHORES)
		DWORD	t, r;

		t = (waitms == SW_TIMEOUT_INFINITE)?INFINITE:(DWORD)waitms;
		r = WaitForSingleObject(PSEMAPHORE, t);
		if (r == WAIT_TIMEOUT) res = SW_STATUS_TIMEOUT;
		else if (r == WAIT_OBJECT_0) res = SW_STATUS_SUCCESS;
		else res = GetLastError();
#else
		#error Unsupported platform - please update SWThreadSemaphore.cpp
#endif

		return res;
		}

// Release the semaphore
sw_status_t
SWThreadSemaphore::release()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

#if defined(SW_USE_POSIX_SEMAPHORES)
		if (sem_post(PSEMAPHORE) != 0) res = errno;
#elif defined(SW_USE_SYSV_SEMAPHORES)
		struct sembuf	sb;

		memset(&sb, 0, sizeof(sb));
		sb.sem_num = 0;
		sb.sem_op = 1;
		sb.sem_flg = 0;

		if (semop(*PSEMAPHORE, &sb, 1) != 0) res = errno;
#elif defined(SW_USE_WINDOWS_SEMAPHORES)
		if (!::ReleaseSemaphore(PSEMAPHORE, 1, NULL)) res = GetLastError();
#else
		#error Unsupported platform - please update SWThreadSemaphore.cpp
#endif

		return res;
		}





