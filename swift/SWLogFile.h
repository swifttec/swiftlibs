/**
*** @file		SWLogFile.h
*** @brief		A simple logging class to write messages to a file.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWLogFile class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWLogFile_h__
#define __SWLogFile_h__

#include <swift/SWString.h>
#include <swift/SWThread.h>
#include <swift/SWList.h>


/**
*** @brief		A simple logging class to write messages to a file using a background thread to minimize the impact on the calling thread.
***
*** When a filename is specified this handler can optionally limit the size of a
*** logfile and how many backup copies are made. This is described in detail in
*** the filename constructor.
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Unsafe</td>
*** </tr>
*** </table>
***
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWLogFile : public SWLockableObject
		{
public:
		enum SWLogFileFlags
			{
			KeepOpen	=	0x0001,
			AutoFlush	=	0x0002
			};

		class SWLogFileThread : public SWThread
			{
		public:
			SWLogFileThread(SWLogFile *pLogFile);

			virtual int	run();

		protected:
			SWLogFile	*m_pLogFile;
			};

public:
		/**
		*** @brief	Constuct a log messagge handler that writes messages to the specified file.
		***
		*** This constructor creates a handler that will write to the specified filename. The
		*** caller can specify an optional maximum size and backup count. If both of these 
		*** parameters are specified with non-zero value they are used as follows:
		***
		*** When the current size of the file (in bytes) reaches the specified maximum file size
		*** (maxsize parameter) the file is closed and renamed. The renaming scheme is dependent
		*** on the backupcount parameter. If backupcount has the value 1 the file is renamed to 
		*** filename.old and a new file is created.
		***
		*** If the backupcount is greater than 1 then the file is renamed as filename.2 (filename.2
		*** renamed as filename.3 etc) thus ending with a maximum of backupcount files.
		**/
		SWLogFile(int flags=0, const SWString &filename="", sw_size_t maxsize=0, int backupcount=0);

		/// Virtual destructor
		virtual ~SWLogFile();

		sw_status_t		open(const SWString &filename);
		sw_status_t		open(const SWString &filename, sw_size_t maxsize, int backupcount);
		sw_status_t		flush();
		sw_status_t		close();

		/// Add the following string to the log
		sw_status_t		writeLine(const SWString &text);

		/// Set the max file size (in bytes)
		void			maxFileSize(sw_size_t v)		{ m_maxFileSize = v; }

		/// Get the max file size (in bytes)
		sw_size_t		maxFileSize() const				{ return m_maxFileSize; }

		/// Set the backup file count
		void			backupCount(int v)				{ m_backupFileCount = v; }

		/// Get the backup file count
		int				backupCount() const				{ return m_backupFileCount; }

		/// Set the flags
		void			flags(int v)					{ m_flags = v; }

		/// Get the flags
		int				flags() const					{ return m_flags; }

		bool			autoFlush() const				{ return ((m_flags & AutoFlush) != 0); }
		bool			keepOpen() const				{ return ((m_flags & KeepOpen) != 0); }

protected:
		bool			getNextLine(SWString &line);
		sw_status_t		writeLineToFile(const SWString &line);
		sw_status_t		flushFile();
		sw_status_t		closeFile();

private:
		SWString			m_filename;			///< The file name
		FILE				*m_file;			///< The output stream for the file
		sw_size_t			m_maxFileSize;		///< The maximum size of a single log file.
		int					m_backupFileCount;	///< The number of backup files to create
		int					m_flags;
		SWList<SWString>	m_data;
		SWLogFileThread		m_bgthread;
		bool				m_flush;
		};




#endif
