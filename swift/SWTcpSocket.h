/**
*** @file		SWTcpSocket.h
*** @brief		A TCP socket
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWTcpSocket class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWTcpSocket_h__
#define __SWTcpSocket_h__

#include <swift/SWSocket.h>




// Forward declarations

/**
*** @brief	A TCP socket implementation
***
*** The SWTcpSocket is a TCP specific socket implementation which
*** supports both client and server sockets.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWTcpSocket : public SWSocket
		{
public:
		/// Default Constructor
		SWTcpSocket();

		/// Virtual destructor
		virtual ~SWTcpSocket();

		// Alternative connect method specific to TCP socket
		// Attempts to connect to the specified hostname and port
		//sw_status_t					connect(const SWString &hostname, int port);

		// Alternative listen method specific to TCP socket
		// Attempts to listen for incoming connection on the specified port
		//sw_status_t					listen(int port, int maxPending=5);

public: // SWSocket Overrides

		/**
		*** Opens/creates the socket without making a connection.
		**/
		virtual sw_status_t			open();

		/**
		*** Closes the socket.
		**/
		virtual sw_status_t			close();

		/**
		*** @brief	Listen for a connection to the specified socket address.
		***
		*** This method sets up the socket to listen for incoming connection requests.
		***
		*** @param[in]	addr		The socket address to accept connection requests from.
		***							The socket address contains address and port information
		***							and can be set to receive from any address on a specific 
		***							port.
		*** @param[in]	maxPending	This value sets the maximum number of pending connection
		***							request that can outstanding on this socket be the connection
		***							is actively refused by the socket. This value is typically
		***							limited to a relatively low value by the underlying O/S
		***							(A limit of 5 is not uncommon).
		***
		*** @return					A status code indicating the success or failure of the operation:
		***							- SW_STATUS_SUCCESSFUL if a connection was successfully accepted.
		***							- An error code if some other failure occurred.
		**/
		virtual sw_status_t			listen(const SWSocketAddress &addr, int maxPending);

		/**
		*** @brief	Accept an incoming connection on a listening socket.
		***
		*** This method waits for an incoming connection request coming from
		*** another socket and then if successful creates a new SWTcpSocket object
		*** to handle the new socket and returns this to the caller.
		***
		*** This method can be set to optionally timeout if a connection is not
		*** received within the specified amount of time.
		***
		*** @param[out]	pSocket	Receives a pointer to the newly created to socket.
		*** @param[in]	waitms	The number of milliseconds to wait for a connection
		***						before timing out.
		***
		*** @return				A status code indicating the success or failure of the operation:
		***						- SW_STATUS_SUCCESSFUL if a connection was successfully accepted.
		***						- SW_STATUS_TIMEOUT if a timeout occurred.
		***						- An error code if some other failure occurred.
		**/
		virtual sw_status_t			accept(SWSocket *&pSocket, sw_uint64_t waitms=SW_TIMEOUT_INFINITE);
		};




#endif
