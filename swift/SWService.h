/****************************************************************************
**	SWService.h	A class for testing and controlling services (primarily Win32)
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWService_h__
#define __SWService_h__


#include <swift/swift.h>
#include <swift/SWObject.h>
#include <swift/SWStringArray.h>
#include <swift/SWThreadMutex.h>
#include <swift/SWArray.h>

// Forward declarations

#if !defined(SW_PLATFORM_WINDOWS)
typedef void *	SERVICE_STATUS;

#define SERVICE_STOPPED                        0x00000001
#define SERVICE_START_PENDING                  0x00000002
#define SERVICE_STOP_PENDING                   0x00000003
#define SERVICE_RUNNING                        0x00000004
#define SERVICE_CONTINUE_PENDING               0x00000005
#define SERVICE_PAUSE_PENDING                  0x00000006
#define SERVICE_PAUSED                         0x00000007

#endif

enum SWServiceState
	{
	SvcStateUnknown=0,
	SvcStateStopped,
	SvcStateStartPending,
	SvcStateStopPending,
	SvcStateRunning,
	SvcStateContinuePending,
	SvcStatePausePending,
	SvcStatePaused
	};

enum SWServiceStartup
	{
	SvcStartDisabled=0,
	SvcStartManual,
	SvcStartAuto,
	SvcStartAutoDelayed
	};


class SWIFT_DLL_EXPORT SWServiceEntry
		{
public:
		SWString	name;
		SWString	title;
		int			state;
		};


/**
*** A class for testing and controlling services (primarily Win32)
**/
class SWIFT_DLL_EXPORT SWService : public SWObject
		{
public:
		/// Default constructor
		SWService(const SWString &name="", bool lookForName=false);
		SWService(const SWService &other);
		virtual ~SWService();

		SWService &   operator=(const SWService &other);

		sw_status_t			open(const SWString &name, bool lookForName=false);
		sw_status_t			close();

		const SWString &	name() const				{ return m_name; }

		// Get the service status
		SWServiceState		state() const;

		bool				isOpen() const;
		bool				isValid() const			{ return (state() != SvcStateUnknown); }
		bool				isRunning() const		{ return (state() == SvcStateRunning); }
		bool				isStopped() const		{ return (state() == SvcStateStopped); }
		bool				isPaused() const		{ return (state() == SvcStatePaused); }

		// Service control
		bool				stop(sw_uint64_t waitms=SW_TIMEOUT_INFINITE);
		bool				start(sw_uint64_t waitms=SW_TIMEOUT_INFINITE);
		bool				pause(sw_uint64_t waitms=SW_TIMEOUT_INFINITE);

		// Get Info
		int					getType() const;
		int					getStartType() const;
		int					getErrorControl() const;
		SWString			getProgramName() const;
		SWString			getLoadOrderGroup() const;
		int					getTagId() const;
		SWString			getServiceStartName() const;
		SWString			getDisplayName() const;

		// Returns a list of services this service depends upon
		void				getDependencies(SWStringArray &sa) const;

		// Returns a list of services that depend upon this service
		void				getDependents(SWStringArray &sa) const;

		static SWString		actualServiceName(const SWString &name);

public:
		static void			getServiceList(SWArray<SWServiceEntry> &list, int type=0);
		static bool			openManager();
		static bool			closeManager();
		static bool			canControlServices();

private:
		sw_status_t			control(SWServiceState wanted, sw_uint64_t waitSecs);


private:
		SWString				m_name;		// The Service name

#if defined(SW_PLATFORM_WINDOWS)
		QUERY_SERVICE_CONFIG	*m_config;	// The service config info
		TCHAR					m_configBuf[1024];
		SC_HANDLE				m_svcHandle;	// A handle to the service itself

		static SC_HANDLE		m_mgrHandle;	// A handle to the service manager
#endif
private:
		static SWThreadMutex	m_mutex;		
		static int				m_mgrRefCount;
		static int				m_mgrType;
		};


#endif
