/**
*** @file		SWException.h
*** @brief		The standard exception class for all base library exceptions.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWException class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWException_h__
#define __SWException_h__

#include <swift/swift.h>
#include <xplatform/sw_status.h>




/**
*** @brief	Base exception class.
***
*** SWException is the base exception class and is used to derive all
*** exceptions used by the base libraries. Using this technique a program
*** perform a catch (SWException &) to catch all base library exceptions.
*** The Exception class records the file, line number and current errno value
*** when thrown in conjunction with the SW_EXCEPTION macros.
*** 
*** - To define an exception class use the SW_DEFINE_EXCEPTION macro
*** - To throw an exception use the SW_EXCEPTION or SW_EXCEPTION_TEXT macro with the throw keyword.
*** E.g. throw SW_EXCEPTION(IndexOutOfBoundsException);
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Safe</td>
*** </tr>
*** </table>
***
*** @see		SW_DEFINE_EXCEPTION, SW_EXCEPTION
*** @author		Simon Sparkes
***/
class SWIFT_DLL_EXPORT SWException
		{
public:
		/// The exception name
		const char *	name() const		{ return m_name; }

		/// The filename where the exception was generated
		const char *	file() const		{ return m_file; }

		/// The line number where the exception was generated
		int				line() const		{ return m_line; }

		/// The value of errno when the exception was generated (Note: no errno method available!)
		sw_status_t		status() const		{ return m_status; }

		/// Convert the exception to a string
		const char *	toString() const;

		/// Conversion to a string using the specified format
		const char *	toFormattedString(const char *fmt="%n thrown at line %l of %f (errno=%e)%T");

		/// Cast to a char *
		operator const char *() const		{ return toString(); }

public: // Although you can use these methods it is better to use the macros defined below
		SWException(const char *file, int line, sw_status_t status, const char *text);

		/// Virtual destructor
		virtual ~SWException();

		/// Assignment operator
		SWException &	operator=(const SWException &e);

		/// Copy constructor
		SWException(const SWException &e);

protected:
		/// Internal constructor
		SWException(const char *name, const char *file, int line, sw_status_t status, const char *text);

		/// Common initialisation for internal use.
		void	init(const char *name, const char *file, int line, sw_status_t status, const char *text);

		/// String duplication for internal use.
		char *	dupstr(const char *str);

protected:
		char			*m_name;		///< The exception name as a string
		char			*m_file;		///< The filename the exception occurred in (when available)
		int				m_line;			///< The line number of the the exception in file (when available)
		sw_status_t		m_status;		///< The status code at the time of the exception
		char			*m_text;		///< The filename the exception occurred in (when available)
		mutable char	*m_outstr;		///< The last outputted string
		};

/**
*** @example	exception.cpp
***
*** An example of how to throw, catch and interpret exceptions thrown using
*** the SWException class and it's associated macros.
**/

//--------------------------------------------------------------------------------
// Some useful macros for declaring and throwing exceptions

/**
*** @brief	Define an exeception
***
*** This macro is used to define an exception. It will declare a class that is
*** throwable using the SW_EXCEPTION macro.
***
*** @param e	The execption name
*** @param pe	The parent exception name. For top-level exceptions use SWException
**/
#define SW_DEFINE_EXCEPTION(e, pe) \
	class e : public pe  \
		{ \
public: \
		e(const char *file, int line, int eno, const char *text): pe(#e, file, line, eno, text) {} \
		e(const e &other) : pe(other) {} \
protected: \
		e(const char *name, const char *file, int line, int eno, const char *text): pe(name, file, line, eno, text) {} \
		}

/**
*** Use this macro to throw an exeception from an object. The macro fills in the
*** the file and line information for the execption.
***
*** To use this macro use the following code:
*** <code>
*** 	throw SW_EXCEPTION(ExceptionName);
*** </code>
***
*** @brief		Throw the specified exception
*** @param e	The execption to throw
*/
#define SW_EXCEPTION(e)				e(__FILE__, __LINE__, sw_status_getLastError(), "")


/**
*** Use this macro to throw an exeception from an object. The macro fills in the
*** the file and line information for the execption.
***
*** To use this macro use the following code:
*** <code>
*** 	throw SW_EXCEPTION_TEXT(ExceptionName, "This is a message");
*** </code>
***
*** @brief		Throw the specified exception
*** @param e	The execption to throw
*** @param t	The text to associate with exception
*/
#define SW_EXCEPTION_TEXT(e, t)				e(__FILE__, __LINE__, sw_status_getLastError(), t)



/// A general memory related exception
SW_DEFINE_EXCEPTION(SWMemoryException, SWException);

/// This exception is thrown when a call to new or malloc has failed.
SW_DEFINE_EXCEPTION(SWMemoryAllocationException, SWMemoryException);

/// This exception is thrown when an index is out of bounds.
SW_DEFINE_EXCEPTION(SWIndexOutOfBoundsException, SWException);

/// An array index is out of bounds
SW_DEFINE_EXCEPTION(SWArrayIndexOutOfBoundsException, SWIndexOutOfBoundsException);

/// This exception is thrown when an unsupported method of an object has been called
SW_DEFINE_EXCEPTION(SWUnsupportedOperationException, SWException);

/// This exception is thrown when a method has been called when the object is in an unusable state
SW_DEFINE_EXCEPTION(SWIllegalStateException, SWException);	

/// This exception is thrown when a method has been called for an invalid element
SW_DEFINE_EXCEPTION(SWNoSuchElementException, SWException);	

/// This exception is thrown when an object has been modified unexpectedly
SW_DEFINE_EXCEPTION(SWConcurrentModificationException, SWException);

/// This exception is thrown when a method has been called with an bad parameter
SW_DEFINE_EXCEPTION(SWIllegalArgumentException, SWException);

/// This exception is thrown when an attempt to clone an uncloneable object has occurred
SW_DEFINE_EXCEPTION(SWCloneNotSupportedException, SWException);


//--------------------------------------------------------------------------------
// IO Exceptions
//--------------------------------------------------------------------------------

/// Generic IO exception
SW_DEFINE_EXCEPTION(SWIOException, SWException);

/// End-of-file encoutered exception
SW_DEFINE_EXCEPTION(SWEOFException, SWIOException);

/// File not found exception
SW_DEFINE_EXCEPTION(SWFileNotFoundException, SWIOException);




#endif
