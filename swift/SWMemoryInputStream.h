/**
*** @file		SWMemoryInputStream.h
*** @brief		A class representing an input stream coming from a memory buffer.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWBuffer class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWMemoryInputStream_h__
#define __SWMemoryInputStream_h__

#include <swift/SWInputStream.h>




/**
*** @brief	A memory input stream
***
*** This class takes a memory block/buffer and uses as the input to
*** an input stream.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWMemoryInputStream : public SWInputStream
		{
public:

		/**
		*** @brief	Construct a memory input stream from a buffer of unknown size.
		***
		*** Constructs a memory input stream from an existing buffer, without
		*** a size limit. This constructor can take a NULL pointer as the buffer
		*** pointer, which is the default constructor. WHen the memory buffer is
		*** NULL all read operations will return errors.
		***
		*** @note
		*** The memory buffer is NOT copied so it must remain valid
		*** until the memory input stream is no longer active
		***
		*** @param[in] pBuffer	A pointer to the memory buffer to read from which can
		***						be NULL.
		**/
		SWMemoryInputStream(const void *pBuffer=NULL);

		/**
		*** @brief	Construct a memory input stream from a buffer of limited size.
		***
		*** Constructs a memory input stream from an existing buffer, with
		*** a size limit to prevent buffer overruns.
		***
		*** @note
		*** The memory buffer is NOT copied so it must remain valid
		*** until the memory input stream is no longer active
		***
		*** @param[in] pBuffer	A pointer to the memory buffer to read from.
		*** @param[in] buflen	The length of the memory buffer.
		**/
		SWMemoryInputStream(const void *pBuffer, sw_size_t buflen);

		/**
		*** @brief	Point the memory stream to a new buffer of unknown size.
		***
		*** This method points the memory input stream to a new buffer
		*** from where the next read operation will take place.
		***
		*** @note
		*** @note
		*** The memory buffer is NOT copied so it must remain valid
		*** until the memory input stream is no longer active
		***
		*** @param[in] pBuffer	A pointer to the memory buffer to read from.
		**/
		void		setBuffer(const void *pBuffer);

		/**
		*** @brief	Point the memory stream to a new buffer of limited size.
		***
		*** This method points the memory input stream to a new buffer
		*** from where the next read operation will take place. The buflen
		*** specifies the size of the buffer. Attempts to read more data than
		*** is available will result in errors.
		***
		*** @note
		*** The memory buffer is NOT copied so it must remain valid
		*** until the memory input stream is no longer active
		***
		*** @param[in] pBuffer	A pointer to the memory buffer to read from.
		*** @param[in] buflen	The length of the memory buffer.
		**/
		void		setBuffer(const void *pBuffer, sw_size_t buflen);



		/**
		*** @brief	Return the amount of available data (in bytes)
		***
		*** This method returns the number of bytes of data available to
		*** read from the current stream. This can only be calculated on a buffer
		*** of known size. If the memory stream points to an unlimited buffer
		*** it will always return a value of zero.
		***
		*** @return		The amount of data available to read in bytes, or zero
		***				for an unlimited memory stream.
		**/
		sw_size_t	available() const	{ return _limitedSize?0:_datalen-(_bp-_buf); }


		/**
		*** @brief	Return a boolean indicating if this stream points to a buffer of unlimited/unknown size.
		**/
		bool		unlimited() const	{ return !_limitedSize; }


public: // SWInputStream overrides
		virtual sw_status_t		rewindStream();
		virtual sw_status_t		readData(void *pData, sw_size_t len);	///< Write binary data to the output stream.

private:
		const sw_byte_t	*_buf;			///< A pointer to the start of the memory buffer
		const sw_byte_t	*_bp;			///< The current buffer pointer for the next read operation
		sw_size_t		_datalen;		///< The length of the data buffer
		bool			_limitedSize;	///< A flag indicating if this is a buffer of known/limited size.
		};





#endif
