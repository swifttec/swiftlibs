/**
*** @file		SWIntegerArray.h
*** @brief		An array of Integers
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWIntegerArray class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWIntegerArray_h__
#define __SWIntegerArray_h__

#include <swift/SWAbstractArray.h>




/**
*** @brief	A integer array of arbitary size.
***
*** This object is an array integers which can be expanded to any size
*** required at runtime.
**/
class SWIFT_DLL_EXPORT SWIntegerArray : public SWAbstractArray
		{
public:
		/**
		*** @brief	Construct an array of integers with optional initial size.
		***
		*** @param[in]	initialSize		Specifies the initial size (number of elements) of the array.
		**/
		SWIntegerArray(sw_size_t initialSize=0);

		/**
		*** @brief	Destructor
		**/
		virtual ~SWIntegerArray();

		/**
		*** @brief	Copy constructor
		**/
		SWIntegerArray(const SWIntegerArray &ua);

		/**
		*** @brief	Assignment operator
		**/
		SWIntegerArray &	operator=(const SWIntegerArray &ua);

		/**
		*** @brief	Returns a reference to the nth element growing the array as required.
		***
		*** This non-const operator returns a reference to the nth integer in the array.
		*** If the array currently contains less than n elements the array is expanded to
		*** contain sufficient elements to allow access to the nth element and the added
		*** elements are all initialized to zero.
		***
		*** @param[in]	n	The zero-based index of the element to reference.
		***
		*** @return			A reference to the nth element in the array.
		***
		*** @throws			SWIndexOutOfBoundsException if a negative value for n is supplied.
		**/
		int &		operator[](int n);

		/**
		*** @brief	Returns a reference to the nth element without growing the array.
		***
		*** This non-const operator returns a reference to the nth integer in the array.
		*** If the array currently contains less than n elements the array is expanded to
		*** contain sufficient elements to allow access to the nth element and the added
		*** elements are all initialized to zero.
		***
		*** @param[in]	i	The zero-based index of the element to reference.
		***
		*** @return			A reference to the nth element in the array.
		***
		*** @throws			SWIndexOutOfBoundsException if a value for n is supplied
		***					that is negative of >= size().
		**/
		int &		get(int i) const;

		/**
		*** @brief	Add an element to the end of the array.
		**/
		void		add(int v);

		/**
		*** @brief	Inserts an element at the specified position in the array
		**/
		void		insert(int v, sw_index_t pos);

		/**
		*** @brief	Push a value onto the end of the array.
		**/
		void		push(int v);

		/**
		*** @brief	Pop a value from the end of the array
		**/
		int			pop();


public: // Comparison operators

		/**
		*** @brief	Compare this object with another.
		***
		*** This method compares this object with the one specified and returns a
		*** result indicating if the other object is less than, equal to, or greater
		*** than this object.
		***
		*** - If this object is less than the specified object then a negative result is returned.
		*** - If this object is greater than the specified object then a positive result is returned.
		*** - If this object is equal to the specified object then a zero result is returned.
		***
		*** @note
		*** The compareTo method is not required to return -1 and +1 for the negative and positive
		*** values although this is typically the value returned. Callers should test for \< 0 and \> 0
		*** rather than for specific values of -1 and +1.
		***
		*** @param[in]	other	The data to compare this object to.
		**/
		int			compareTo(const SWIntegerArray &other) const;

		/**
		*** @brief	Test for equality
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is equal to the other object, false otherwise.
		**/
		bool	operator==(const SWIntegerArray &other) const	{ return (compareTo(other) == 0); }

		/**
		*** @brief	Test for inequality
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is not equal to the other object, false otherwise.
		**/
		bool	operator!=(const SWIntegerArray &other) const	{ return (compareTo(other) != 0); }

		/**
		*** @brief	Test for less than.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is less than the other object, false otherwise.
		**/
		bool	operator< (const SWIntegerArray &other) const	{ return (compareTo(other) <  0); }

		/**
		*** @brief	Test for greater than.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is greater than the other object, false otherwise.
		**/
		bool	operator> (const SWIntegerArray &other) const	{ return (compareTo(other) >  0); }

		/**
		*** @brief	Test for less than or equal to.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is less than or equal to the other object, false otherwise.
		**/
		bool	operator<=(const SWIntegerArray &other) const	{ return (compareTo(other) <= 0); }

		/**
		*** @brief	Test for greater than or equal to.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is greater than or equal to the other object, false otherwise.
		**/
		bool	operator>=(const SWIntegerArray &other) const	{ return (compareTo(other) >= 0); }


public: // virtual overrides

		/// Override for SWObject writeToStream
		virtual sw_status_t		writeToStream(SWOutputStream &stream) const;

		/// Override for SWObject readFromStream
		virtual sw_status_t		readFromStream(SWInputStream &stream);
		};





#endif
