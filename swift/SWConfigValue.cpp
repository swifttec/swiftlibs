/****************************************************************************
**	SWConfigValue.cpp	A class to hold a generic config value (includes a name)
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <swift/SWConfigValue.h>
#include <swift/SWConfigKey.h>



// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif

SWConfigValue::SWConfigValue(SWConfigKey *pOwner, const SWString &name) :
    SWNamedValue(name),
    m_pOwner(pOwner)
		{
		}


SWConfigValue::~SWConfigValue()
		{
		}


/**
*** Set the changed flag.
*** We must also set the changed flag of our owner
**/
void
SWConfigValue::setChangedFlag(bool v)
		{
		SWNamedValue::setChangedFlag(v);
		if (m_pOwner != NULL) m_pOwner->setChangedFlag(SWConfigKey::ValueUpdated, this);
		}

