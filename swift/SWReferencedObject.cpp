/**
*** @file		SWReferencedObject.h
*** @brief		A basic object which allows reference counting.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWReferencedObject class.
**/

/*********************************************************************************
Copyright (c) 2006-2017 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"

#include <swift/SWGuard.h>
#include <swift/SWReferencedObject.h>

SWReferencedObject::SWReferencedObject(bool syncflag) :
	m_refcount(0)
		{
		if (syncflag)
			{
			// Set this up as a synchronised object
			synchronize();
			}
		}


SWReferencedObject::~SWReferencedObject()
		{
		}


int
SWReferencedObject::incRefCount()
		{
		SWGuard	guard(this);

		return ++m_refcount;
		}
		

int
SWReferencedObject::decRefCount()
		{
		SWGuard	guard(this);

		if (m_refcount > 0) m_refcount--;

		return m_refcount;
		}


int
SWReferencedObject::getRefCount()
		{
		SWGuard	guard(this);

		return m_refcount;
		}
