/****************************************************************************
**	SWConfigKey.h	A class to that represents a single level of configuration.
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWConfigKey_h__
#define __SWConfigKey_h__

#include <swift/SWObject.h>
#include <swift/SWString.h>
#include <swift/SWVector.h>
#include <swift/SWConfigValue.h>

// Forward declarations
class SWConfigKey;
class SWConfigValue;


/**
*** This class represents a single configuration key. Each key can contain
*** any number of values and any number of sub-keys.
***
*** @brief	A class to that represents a single level of configuration.
**/
class SWIFT_DLL_EXPORT SWConfigKey : public SWObject
		{
friend class SWConfigValue;

public:
		// Change event codes are sent to the owning key on an event
		// with a pointer to the object in question. The codes refer
		// to the internal state rather than any on-disk state. These
		// codes can be used by deriving classes to do on-the-fly updates
		// to disk, or set flags to indicate that a write to disk is required.
		//
		// Note that the ValueCreated, ValueDeleted, KeyCreated and KeyDeleted
		// codes imply that the underlying key has been changed. A separate
		// KeyUpdated will NOT be sent.
		enum ChangeEventCode
			{
			ValueCreated,	// The attached value has just been created (POST-OP)
			ValueDeleted,	// The attached value is about to be deleted (PRE-OP)
			ValueUpdated,	// The attached value has just been updated (POST-OP)
			KeyCreated,	// The attached key has just been created (POST-OP)
			KeyDeleted,	// The attached key is about to be deleted (PRE-OP)
			KeyUpdated	// The attached key has just been updated (POST-OP)
			};

protected:
		/// Default constructor (protected to prevent direct instantiation)
		SWConfigKey(SWConfigKey *owner, const SWString &name, bool ignoreCase);

public:
		virtual ~SWConfigKey();

		/// Return the name of this key
		const SWString &		name() const						{ return _name; }

		// Return the owner of this key (if any)
		SWConfigKey *			owner() const						{ return _owner; }

		/**************************** SubKey Functions ****************************/

		/// Returns the number of sub-keys in this key
		int						keyCount() const					{ return (int)_keyVec.size(); }

		/// Returns the index of the named key, or -1 if no such key exists
		int						keyIndex(const SWString &name) const;

		/// Returns true if the named key exists, false otherwise
		bool					keyExists(const SWString &name) const		{ return (keyIndex(name) >= 0); }

		/// Returns true if the nth key exists, false otherwise
		bool					keyExists(int n) const					{ return (n >=0 && n < (int)_keyVec.size()); }

		/// Returns the name of the nth key
		SWString				keyName(int n) const;

		/// Returns the named key
		SWConfigKey *			findKey(const SWString &key) const;

		/// Returns the nth key
		SWConfigKey *			getKey(int n) const;

		// Returns the named key, creates it if not found
		virtual SWConfigKey *	findOrCreateKey(const SWString &name);

		// Remove the named key
		void					removeKey(const SWString &name);

		// Remove the nth key
		void					removeKey(int n);


		/**************************** Value Functions ****************************/

		/// Returns the number of values contained by the key
		int						valueCount() const						{ return (int)_valueVec.size(); }

		// Returns the index of the named value, or -1 or no such value exists.
		int						valueIndex(const SWString &name) const;

		/// Returns true if the named value exists, false otherwise.
		bool					valueExists(const SWString &name) const	{ return (valueIndex(name) >= 0); }

		/// Returns true if the nth value exists, false otherwise
		bool					valueExists(int n) const					{ return (n >=0 && n < (int)_valueVec.size()); }
		
		// Return the nth value
		SWConfigValue *			getValue(int i) const;

		// Return the named value
		SWConfigValue *			value(const SWString &name) const;

		// Return the named value
		SWConfigValue *			findValue(const SWString &name) const		{ return value(name); }

		// Returns the named value, creates it if not found
		virtual SWConfigValue *	findOrCreateValue(const SWString &name);

		// Returns the name of the nth value
		SWString				valueName(int i) const;

		// Removes the named value (if it exists).
		void					removeValue(const SWString &name);

		// Removes the nth value (if it exists)
		void					removeValue(int n);
		
		/*
		DWORD					getValueType(int i);
		SWString				getStringValue(const SWString &name);

		RegistryKeyValue		getKeyValue(int i);
		RegistryKeyValue		getKeyValue(const SWString &name);
		*/

		/// Returns the "changed" status of this entry.
		bool					changed()					{ return _changed; }

		void					setIgnoreCase(bool v)	{ _ignoreCase = v; }
		bool					getIgnoreCase() const		{ return _ignoreCase; }

protected:

		// Set the changed flag for this object and any owner
		virtual void			setChangedFlag(ChangeEventCode code, SWObject *obj);

		// Clear the changed flag for this object any children
		void					clearChangedFlag();

		// Clear the internal sructures
		void					clearKeysAndValues();

		// Sets the name of this key
		void					setName(const SWString &v)	{ _name = v; }

protected:
		SWConfigKey					*_owner;		// The owning key (can be null)
		SWString					_name;			// The name of this key
		SWVector<SWConfigKey *>		_keyVec;		// An array of key objects
		SWVector<SWConfigValue *>	_valueVec;		// An array of value objects
		bool						_changed;		// A flag to indicate if this value has changed
		bool						_ignoreCase;	// A flag to indicate if we should ignore the case of value and key names
		};

#endif
