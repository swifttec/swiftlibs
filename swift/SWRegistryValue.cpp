/**
*** @file		SWRegistryValue.cpp
*** @brief		A generic value object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWRegistryValue class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <swift/SWRegistryValue.h>
#include <swift/SWString.h>


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif

SWRegistryValue::SWRegistryValue() :
	m_regtype(RtNone)
		{
		}


SWRegistryValue::SWRegistryValue(const SWString &name) :
	SWNamedValue(name),
	m_regtype(RtNone)
		{
		}



SWRegistryValue::SWRegistryValue(const SWString &vname, RegValueType regtype, const SWValue &value) :
	m_regtype(regtype)
		{
		name(vname);
		SWValue::operator=(value);
		clearChangedFlag();
		}


SWRegistryValue::~SWRegistryValue()
		{
		}


SWRegistryValue::SWRegistryValue(const SWRegistryValue &other) :
	SWNamedValue(other),
	m_regtype(RtNone)
		{
		*this = other;
		}


SWRegistryValue &
SWRegistryValue::operator=(const SWRegistryValue &other)
		{
		SWNamedValue::operator=(other);
		m_regtype = other.m_regtype;

		return *this;
		}


SWRegistryValue::SWRegistryValue(const SWString &vname, int v, ValueType type) : m_regtype(RtInt32)			{ name(vname); setValue(v, type); clearChangedFlag(); }
SWRegistryValue::SWRegistryValue(const SWString &vname, bool v) : m_regtype(RtInt32)						{ name(vname); setValue(v); clearChangedFlag(); }
SWRegistryValue::SWRegistryValue(const SWString &vname, sw_int8_t v) : m_regtype(RtInt32)					{ name(vname); setValue(v); clearChangedFlag(); }
SWRegistryValue::SWRegistryValue(const SWString &vname, sw_int16_t v) : m_regtype(RtInt32)					{ name(vname); setValue(v); clearChangedFlag(); }
SWRegistryValue::SWRegistryValue(const SWString &vname, sw_int32_t v) : m_regtype(RtInt32)					{ name(vname); setValue(v); clearChangedFlag(); }
SWRegistryValue::SWRegistryValue(const SWString &vname, sw_int64_t v) : m_regtype(RtInt64)					{ name(vname); setValue(v); clearChangedFlag(); }
SWRegistryValue::SWRegistryValue(const SWString &vname, sw_uint8_t v) : m_regtype(RtInt32)					{ name(vname); setValue(v); clearChangedFlag(); }
SWRegistryValue::SWRegistryValue(const SWString &vname, sw_uint16_t v) : m_regtype(RtInt32)					{ name(vname); setValue(v); clearChangedFlag(); }
SWRegistryValue::SWRegistryValue(const SWString &vname, sw_uint32_t v) : m_regtype(RtInt32)					{ name(vname); setValue(v); clearChangedFlag(); }
SWRegistryValue::SWRegistryValue(const SWString &vname, sw_uint64_t v) : m_regtype(RtInt64)					{ name(vname); setValue(v); clearChangedFlag(); }
SWRegistryValue::SWRegistryValue(const SWString &vname, float v) : m_regtype(RtBinary)						{ name(vname); setValue(v); clearChangedFlag(); }
SWRegistryValue::SWRegistryValue(const SWString &vname, double v) : m_regtype(RtBinary)						{ name(vname); setValue(v); clearChangedFlag(); }
SWRegistryValue::SWRegistryValue(const SWString &vname, const char *v) : m_regtype(RtString)				{ name(vname); setValue(v); clearChangedFlag(); }
SWRegistryValue::SWRegistryValue(const SWString &vname, const wchar_t *v) : m_regtype(RtString)				{ name(vname); setValue(v); clearChangedFlag(); }
SWRegistryValue::SWRegistryValue(const SWString &vname, const void *p, size_t len) : m_regtype(RtBinary)	{ name(vname); setValue(p, len); clearChangedFlag(); }
SWRegistryValue::SWRegistryValue(const SWString &vname, SWBuffer &v) : m_regtype(RtBinary)					{ name(vname); setValue(v); clearChangedFlag(); }


SWRegistryValue &
SWRegistryValue::operator=(const SWNamedValue &v)
		{
		// Call the underlying class assignment operator to do the value assignment
		SWNamedValue::operator=(v);
		m_regtype = getRegValueTypeFromValueType(type());

		return *this;
		}



SWRegistryValue &
SWRegistryValue::operator=(const SWValue &v)
		{
		// Call the underlying class assignment operator to do the value assignment
		SWValue::operator=(v);
		m_regtype = getRegValueTypeFromValueType(type());

		return *this;
		}


SWRegistryValue::RegValueType
SWRegistryValue::getRegValueTypeFromValueType(ValueType vt)
		{
		RegValueType	rt=RtNone;

		switch (vt)
			{
			case VtNone:	rt = RtNone;	break;
			case VtBoolean:	rt = RtInt32;	break;

			case VtInt8:	rt = RtInt32;	break;
			case VtInt16:	rt = RtInt32;	break;
			case VtInt32:	rt = RtInt32;	break;
			case VtInt64:	rt = RtInt64;	break;

			case VtUInt8:	rt = RtInt32;	break;
			case VtUInt16:	rt = RtInt32;	break;
			case VtUInt32:	rt = RtInt32;	break;
			case VtUInt64:	rt = RtInt64;	break;

			case VtFloat:	rt = RtBinary;	break;
			case VtDouble:	rt = RtBinary;	break;

			case VtCChar:	rt = RtInt32;	break;
			case VtWChar:	rt = RtInt32;	break;

			case VtCString:	rt = RtString;	break;
			case VtWString:	rt = RtString;	break;

			case VtDate:	rt = RtInt32;	break;
			case VtTime:	rt = RtBinary;	break;

			case VtPointer:	rt = RtNone;	break;
			case VtData:	rt = RtBinary;	break;
			}

		return rt;
		}


#if defined(SW_PLATFORM_WINDOWS)
inline void
SWAP(char &c1, char &c2)	
	{
	char	tmp;	
	
	tmp = c1;
	c1 = c2;
	c2 = tmp;
	}


void
SWRegistryValue::setFromWindowsRegistryData(const SWString &vname, DWORD type, const char *data, int datalen)
		{
		clear();
		name(vname);

		switch (type)
			{
			case REG_NONE:
				// No value type
				m_regtype = RtNone;
				break;

			case REG_SZ:				
				// nul terminated string - BUT NOT ALWAYS!
				{
				sw_tchar_t	*tmp=new sw_tchar_t[datalen+2];

				memset(tmp, 0, (datalen+2) * sizeof(sw_tchar_t));
				memcpy(tmp, data, datalen);
				m_regtype = RtString;
				SWValue::setValue(tmp);
				delete [] tmp;
				}
				break;

			case REG_EXPAND_SZ:			
				// nul terminated string (with environment variable references)
				m_regtype = RtExpandableString;
				SWValue::setValue((const sw_tchar_t *)data);
				break;

			case REG_BINARY:			
				// Free form binary
				m_regtype = RtBinary;
				SWValue::setValue(data, datalen);
				break;

			case REG_DWORD_LITTLE_ENDIAN:
				// 32-bit number (same as REG_DWORD)
				{
				sw_int32_t	v;

				m_regtype = RtInt32;
				memcpy(&v, data, datalen);
				SWValue::setValue(v, VtInt32);
				}
				break;

			case REG_DWORD_BIG_ENDIAN:		
				// 32-bit number
				{
				union
					{
					char		ca[4];
					sw_int32_t	i32;
					} u;

				m_regtype = RtInt32;
				memcpy(&u.i32, data, datalen);
				SWAP(u.ca[0], u.ca[3]);
				SWAP(u.ca[1], u.ca[2]);
				SWValue::setValue(u.i32, VtInt32);
				}
				break;

			case REG_LINK:			
				// Symbolic Link (unicode)
				m_regtype = RtSymbolicLink;
				SWValue::setValue(data, datalen);
				break;

			case REG_MULTI_SZ:			
				// Multiple strings
				m_regtype = RtMultiString;
				SWValue::setValue(data, datalen);
				break;

			case REG_RESOURCE_LIST:		
				// Resource list in the resource map
				m_regtype = RtUnsupported;
				SWValue::setValue(data, datalen);
				break;

			case REG_FULL_RESOURCE_DESCRIPTOR:	
				// Resource list in the hardware description
				m_regtype = RtUnsupported;
				SWValue::setValue(data, datalen);
				break;

			case REG_RESOURCE_REQUIREMENTS_LIST:
				m_regtype = RtUnsupported;
				SWValue::setValue(data, datalen);
				break;

			case REG_QWORD_LITTLE_ENDIAN:
				// 64-bit number
				{
				sw_int64_t	v;

				m_regtype = RtInt64;
				memcpy(&v, data, datalen);
				SWValue::setValue(v);
				}
				break;
			}
		}
#endif // defined(SW_PLATFORM_WINDOWS)


float
SWRegistryValue::toFloat() const
		{
		float	res=0;

		if (type() == VtData && length() == sizeof(float))
			{
			memcpy(&res, data(), sizeof(float));
			}
		else if (type() == VtData && length() == sizeof(double))
			{
			double	v;

			memcpy(&v, data(), sizeof(double));
			res = (float)v;
			}
		else
			{
			res = SWValue::toFloat();
			}

		return res;
		}


double
SWRegistryValue::toDouble() const
		{
		double	res=0;

		if (type() == VtData && length() == sizeof(double))
			{
			memcpy(&res, data(), sizeof(double));
			}
		else if (type() == VtData && length() == sizeof(float))
			{
			float	v;

			memcpy(&v, data(), sizeof(float));
			res = v;
			}
		else
			{
			res = SWValue::toDouble();
			}

		return res;
		}

