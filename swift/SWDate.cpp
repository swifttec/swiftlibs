/**
*** @file		SWDate.cpp
*** @brief		A representation of date based on Julian day numbers
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWDate class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <stdio.h>
#include <time.h>
#include <swift/SWDate.h>
#include <swift/SWLocalTime.h>
#include <swift/SWInputStream.h>
#include <swift/SWOutputStream.h>


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





/*
Mathematicians and programmers have naturally interested themselves in mathematical and computational
algorithms to convert between Julian day numbers and Gregorian dates. The following conversion algorithm
is due to Henry F. Fliegel and Thomas C. Van Flandern: 

Days are integer values in the range 1-31, months are integers in the range 1-12, and years are positive or
negative integers. Division is to be understood as in integer arithmetic, with remainders discarded.

This algorithm is valid only in the Gregorian Calendar and the proleptic Gregorian Calendar (for non-negative
Julian day numbers). It does not correctly convert dates in the Julian Calendar.
*/


// The Julian day (jd) is computed from Gregorian day, month and year (d, m, y) as follows:
void
SWDate::dayMonthYearToJulian(int d, int m, int y, sw_int32_t &jd)
		{
		jd = ( 1461 * ( y + 4800 + ( m - 14 ) / 12 ) ) / 4 +
			( 367 * ( m - 2 - 12 * ( ( m - 14 ) / 12 ) ) ) / 12 -
			( 3 * ( ( y + 4900 + ( m - 14 ) / 12 ) / 100 ) ) / 4 +
			d - 32075;
		}

// Converting from the Julian day number to the Gregorian date is performed thus:
void
SWDate::julianToDayMonthYear(sw_int32_t jd, int &d, int &m, int &y)
		{
		int	l, n, j, i;

		l = jd + 68569;
		n = ( 4 * l ) / 146097;
		l = l - ( 146097 * n + 3 ) / 4;
		i = ( 4000 * ( l + 1 ) ) / 1461001;
		l = l - ( 1461 * i ) / 4 + 31;
		j = ( 80 * l ) / 2447;
		d = l - ( 2447 * j ) / 80;
		l = j / 11;
		m = j + 2 - ( 12 * l );
		y = 100 * ( n - 49 ) + i + l;
		}

extern struct tm *localtime_r(const time_t *, struct tm *);

SWDate::~SWDate()
		{
		}


// Construct with today's date
SWDate::SWDate() :
	m_valid(false),
	m_julian(0),
	m_julianYearStart(0),
	m_day(0),
	m_month(0),
	m_year(0)
		{
		// if (setToCurrentDate) update();
		}


SWDate::SWDate(int day, int month, int year, bool throwOnInvalidDMY) :
	m_valid(false),
	m_julian(0),
	m_julianYearStart(0),
	m_day(0),
	m_month(0),
	m_year(0)
		{
		set(day, month, year, throwOnInvalidDMY);
		}


SWDate::SWDate(const SWString &value, const SWString &format, bool throwOnInvalidDMY) :
	m_valid(false),
	m_julian(0),
	m_julianYearStart(0),
	m_day(0),
	m_month(0),
	m_year(0)
		{
		set(value, format, throwOnInvalidDMY);
		}


SWDate::SWDate(sw_int32_t t, bool isJulian) :
	m_valid(false),
	m_julian(0),
	m_julianYearStart(0),
	m_day(0),
	m_month(0),
	m_year(0)
		{
		if (isJulian) m_julian = t;
		else m_julian = SWTime((time_t)t).toJulian();

		calculateFields();
		}


SWDate::SWDate(const SWTime &ut) :
	m_valid(false),
	m_julian(0),
	m_julianYearStart(0),
	m_day(0),
	m_month(0),
	m_year(0)
		{
		m_julian = ut.toJulian();
		calculateFields();
		}


SWDate::SWDate(const SWDate &other) :
	SWObject()
		{
		*this = other;
		}


// Assignment from a Time
SWDate &
SWDate::operator=(const SWTime &ut)
		{
		m_julian = ut.toJulian();
		calculateFields();

		return *this;
		}


// Assignment from another SWDate
SWDate &
SWDate::operator=(const SWDate &other)
		{
#define COPY(x) x = other.x
		COPY(m_valid);
		COPY(m_julian);
		COPY(m_julianYearStart);
		COPY(m_day);
		COPY(m_month);
		COPY(m_year);
#undef COPY

		return *this;
		}


static const char *
getnum(const char *sp, int ndigits, int &value)
		{
		int	v = 0;

		while (*sp && !(*sp >= '0' && *sp <= '9'))
			{
			sp++;
			}

		while (ndigits > 0 && *sp)
			{
			if (*sp >= '0' && *sp <= '9')
				{
				v = v * 10 + (*sp - '0');
				sp++;
				--ndigits;
				}
			else break;
			}

		value = v;

		return sp;
		}



SWString
SWDate::toString(const SWString &format) const
		{
		SWString	tmpf(format), s;
		SWString	res;

		if (!tmpf.isEmpty())
			{
			// Supported formats
			// YYYY 4-digit year
			// YY 2-digit year (00-99)
			// MM 2-digit month (01-12)
			// DD 2-digit day (01-31)

			const char	*fp=tmpf;

			while (*fp)
				{
				if (strncmp(fp, "YYYY", 4) == 0 || strncmp(fp, "yyyy", 4) == 0)
					{
					s.format("%04d", year());
					res += s;
					fp += 4;
					}
				else if (strncmp(fp, "YY", 2) == 0 || strncmp(fp, "yy", 2) == 0)
					{
					s.format("%02d", year() % 100);
					res += s;
					fp += 2;
					}
				else if (strncmp(fp, "Y", 1) == 0 || strncmp(fp, "y", 1) == 0)
					{
					s.format("%d", year() % 10);
					res += s;
					fp += 1;
					}
				else if (strncmp(fp, "MM", 2) == 0)
					{
					s.format("%02d", month());
					res += s;
					fp += 2;
					}
				else if (strncmp(fp, "M", 1) == 0)
					{
					s.format("%d", month());
					res += s;
					fp += 1;
					}
				else if (strncmp(fp, "DD", 2) == 0 || strncmp(fp, "dd", 2) == 0)
					{
					s.format("%02d", day());
					res += s;
					fp += 2;
					}
				else if (strncmp(fp, "D", 1) == 0 || strncmp(fp, "d", 1) == 0)
					{
					s.format("%d", day());
					res += s;
					fp += 1;
					}
				else
					{
					res += *fp++;
					}
				}
			}

		return res;
		}
		

bool
SWDate::set(const SWString &value, const SWString &format, bool throwOnInvalidDMY)
		{
		SWString	tmpf(format);
		SWString	tmpv(value);
		bool		res=false;

		if (!tmpf.isEmpty())
			{
			// Supported formats
			// YYYY 4-digit year
			// YY 2-digit year (00-99)
			// MM 2-digit month (01-12)
			// DD 2-digit day (01-31)

			const char	*fp=tmpf;
			const char	*vp=tmpv;
			int			year=0, month=0, day=0; // , hour=0, minute=0, second=0, nanosecond=0;

			vp = tmpv;
			while (vp != NULL && *fp && *vp)
				{
				if (strncmp(fp, "YYYY", 4) == 0 || strncmp(fp, "yyyy", 4) == 0)
					{
					vp = getnum(vp, 4, year);
					if ((year / 100) == 0)
						{
						SWDate	today;
						int		y;

						today.update();
						y = today.year() % 100;
						year += (today.year() - y) + (y >= 50?100:0) - (year >=50?100:0);
						}
					fp += 4;
					}
				else if (strncmp(fp, "YY", 2) == 0 || strncmp(fp, "yy", 2) == 0)
					{
					vp = getnum(vp, 2, year);
					if (vp != NULL)
						{
						SWDate	today;
						int		y;

						today.update();
						y = today.year() % 100;
						year += (today.year() - y) + (y >= 50?100:0) - (year >=50?100:0);
						}
					fp += 2;
					}
				else if (strncmp(fp, "Y", 1) == 0 || strncmp(fp, "y", 1) == 0)
					{
					vp = getnum(vp, 1, year);
					if (vp != NULL)
						{
						SWDate	today;
						int		y;

						today.update();
						y = today.year() % 10;
						year += (today.year() - y) + (y >= 5?10:0) - (year >=5?10:0);
						}
					fp += 1;
					}
				else if (strncmp(fp, "MM", 2) == 0)
					{
					vp = getnum(vp, 2, month);
					fp += 2;
					}
				else if (strncmp(fp, "M", 1) == 0)
					{
					vp = getnum(vp, 2, month);
					fp += 1;
					}
				else if (strncmp(fp, "DD", 2) == 0 || strncmp(fp, "dd", 2) == 0)
					{
					vp = getnum(vp, 2, day);
					fp += 2;
					}
				else if (strncmp(fp, "D", 1) == 0 || strncmp(fp, "d", 1) == 0)
					{
					vp = getnum(vp, 2, day);
					fp += 1;
					}
				else if (*fp == *vp)
					{
					fp++;
					vp++;
					}
				else
					{
					// Invalid string - does not match pattern
					vp = NULL;
					}
				}

			if (vp != NULL)
				{
				res = set(day, month, year, throwOnInvalidDMY);
				}
			}

		return res;
		}
		

bool
SWDate::set(int day, int month, int year, bool throwOnInvalidDMY)
		{
		sw_int32_t	j;
		int			d, m, y;

		// First convert to a julian
		SWDate::dayMonthYearToJulian(day, month, year, j);

		// And then back again
		SWDate::julianToDayMonthYear(j, d, m, y);

		if (d != day || m != month || y != year)
			{
			// We got back a different result so the date was invalid
			// (or I supposed our calculations went wrong, but of course that's impossible right,
			// after all this is bug-free software!)

			// The caller wanted an exception so throw one.
			if (throwOnInvalidDMY) throw SW_EXCEPTION(SWInvalidDateException);

			// The caller wanted a bool
			return false;
			}

		m_julian = j;
		m_day = (sw_uint8_t)d;
		m_month = (sw_uint8_t)m;
		m_year = (sw_int16_t)y;

		// Calculate the julian day number of the year start
		SWDate::dayMonthYearToJulian(1, 1, y, m_julianYearStart);

		// Finally
		m_valid = true;

		return true;
		}


// PRIVATE
// calculate internal fields from _julian
void
SWDate::calculateFields()
		{
		int		d, m, y;

		SWDate::julianToDayMonthYear(m_julian, d, m, y);
		m_day = (sw_uint8_t)d;
		m_month = (sw_uint8_t)m;
		m_year = (sw_int16_t)y;

		// Calculate the julian day number of the year start
		SWDate::dayMonthYearToJulian(1, 1, y, m_julianYearStart);

		// Finally
		m_valid = true;
		}


// Calculate the week number
// if firstSunday is true the first Sunday in the year is the start of week 1
// otherwise the first Monday in the year is the start of week 1
int
SWDate::weekOfYear(DayNumber firstDayOfWeek1) const
		{
		// Today's day number (zero-based)
		int	yday = dayOfYear()-1;

		// The day number for the 1st of the year
		int	day1 = (m_julianYearStart+1)%7;

		int	week = yday + day1 - firstDayOfWeek1;
		if (day1 <= firstDayOfWeek1) week += 7;
		week /= 7;

		return week;
		}


// Check to see if the given year is a leap year
bool
SWDate::isLeapYear(int year)
		{
		return ((year % 4) == 0 && ((year % 100) != 0 || (year % 400) == 0));
		}



int
SWDate::compareTo(const SWDate &other) const
		{
		return m_julian - other.m_julian;
		}



SWIFT_DLL_EXPORT SWDate
operator+(const SWDate &a, const SWDate &b)
		{
		return SWDate(a.m_julian + b.m_julian, true);
		}


SWIFT_DLL_EXPORT SWDate
operator-(const SWDate &a, const SWDate &b)
		{
		return SWDate(a.m_julian - b.m_julian, true);
		}


// Prefix increment operator.
const SWDate &
SWDate::operator++()
		{
		m_julian++;
		calculateFields();

		return *this;
		}


// Postfix increment operator.
SWDate
SWDate::operator++(int)
		{
		SWDate tmp(*this);

		++m_julian;
		calculateFields();

		return tmp;
		}


// Prefix decrement operator.
const SWDate &
SWDate::operator--()
		{
		m_julian--;
		calculateFields();

		return *this;
		}


// Postfix decrement operator.
SWDate
SWDate::operator--(int)
		{
		SWDate tmp(*this);

		--m_julian;
		calculateFields();

		return tmp;
		}

// += operator (int)
SWDate &
SWDate::operator+=(int v)
		{
		m_julian += v;
		calculateFields();

		return *this;
		}


// -= operator (int)
SWDate &
SWDate::operator-=(int v)
		{
		m_julian -= v;
		calculateFields();

		return *this;
		}


void
SWDate::update()
		{
		SWLocalTime	ut;

		ut.update();
		*this = ut;
		}


// Return the current date.
SWDate
SWDate::today()
		{
		SWDate	d;

		d.update();

		return d;
		}


sw_status_t
SWDate::writeToStream(SWOutputStream &stream) const
		{
		return stream.write(m_julian);
		}


sw_status_t
SWDate::readFromStream(SWInputStream &stream)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		sw_uint32_t	j=0;

		res = stream.read(j);
		if (res == SW_STATUS_SUCCESS)
			{
			m_julian = j;
			calculateFields();
			}

		return res;
		}


void
SWDate::adjust(int amount, int unit)
		{
		if (amount != 0)
			{
			switch (unit)
				{
				case Days:
					m_julian += amount;
					break;
					
				case Weeks:
					m_julian += (amount * 7);
					break;

				case Months:
				case Years:
					{
					int	d, m, y;
					
					d = day();
					m = month();
					y = year();
					
					if (unit == Months)
						{
						m += amount;

						while (m < 1)
							{
							y--;
							m += 12;
							}

						while (m > 12)
							{
							y++;
							m -= 12;
							}
						}
					else
						{
						y += amount;
						}
						
					// Make sure day is valid for selected month
					int		j, d2, m2, y2;
					
					// First convert to a julian
					SWDate::dayMonthYearToJulian(d, m, y, j);

					do
						{
						// And then back again
						SWDate::julianToDayMonthYear(j, d2, m2, y2);
						j--;
						}
					while (m2 != m);
					
					set(d2, m2, y2);
					}
					break;
				}

			calculateFields();
			}
		}


int	
SWDate::compareDates(const SWDate &dt1, const SWDate &dt2, int &years, int &months, int &days)
		{
		int		res=0;
		int		ty, tm, td, by, bm, bd;

		if (dt1.julian() == dt2.julian())
			{
			res = years = months = days = 0;
			}
		else
			{
			if (dt1.julian() < dt2.julian())
				{
				res = -1;
				ty = dt2.year();
				tm = dt2.month();
				td = dt2.day();
				by = dt1.year();
				bm = dt1.month();
				bd = dt1.day();
				}
			else
				{
				res = 1;
				ty = dt1.year();
				tm = dt1.month();
				td = dt1.day();
				by = dt2.year();
				bm = dt2.month();
				bd = dt2.day();
				}

			int		dy=ty-by;
			int		dm=tm-bm;
			int		dd=td-bd;

			if (dd < 0)
				{
				// correct number of days and months
				int	daysInMonth=31;
				
				if (bm == 4 || bm == 6 || bm == 9 || bm == 11)
					{
					daysInMonth = 30;
					}
				else if (bm == 2)
					{
					daysInMonth = 28;
					if ((by % 400) == 0 || ((by % 4) == 0 && (by % 100) != 0)) daysInMonth = 29;
					}
				else
					{
					daysInMonth = 31;
					}
				
				// dd = daysInMonth-abs(dd);
				dd += daysInMonth;
				dm--;
				}

			if (dm < 0)
				{
				// correct number of months and years
				dm = 12-abs(dm);
				dy--;
				}			

			years = dy;
			months = dm;
			days = dd;
			}

		return res;
		}




