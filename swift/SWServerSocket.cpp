/**
*** @file		SWServerSocket.cpp
*** @brief		A generic socket implementation
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWServerSocket class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWServerSocket.h>
#include <swift/SWTcpSocket.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




/// Default constructor
SWServerSocket::SWServerSocket() :
	_pSocket(NULL),
	_delFlag(false)
		{
		}


SWServerSocket::SWServerSocket(SWSocket *pSocket, bool delflag) :
	_pSocket(pSocket),
	_delFlag(delflag)
		{
		}


/// Virtual destructor
SWServerSocket::~SWServerSocket()
		{
		close();
		if (_delFlag)
			{
			delete _pSocket;
			_pSocket = 0;
			}
		}


/// Create a socket connection to a listening socket
sw_status_t
SWServerSocket::listen(const SWSocketAddress &addr)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (_pSocket == NULL)
			{
			// Pick an appropriate socket for the address family.
			switch (addr.type())
				{
				case SWSocketAddress::IP4:
					_pSocket = new SWTcpSocket();
					_delFlag = true;
					break;

				default:
					// Do nothing
					break;
				}
			}

		if (_pSocket != NULL)
			{
			res = _pSocket->open();
			_pSocket->setOption(SWSocket::SW_SOCKOPT_REUSEADDR, true);

			if (_pSocket->address().family() == SWSocketAddress::Unknown || _pSocket->address().family() == addr.family())
				{
				res = _pSocket->listen(addr, 5);
				}
			else
				{
				// The socket and address familes do not match
				res = SW_STATUS_INVALID_PARAMETER;
				}
			}
		else
			{
			// An invalid socket address was passed
			res = SW_STATUS_INVALID_PARAMETER;
			}

		return res;
		}


sw_status_t
SWServerSocket::close()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (_pSocket != NULL) res = _pSocket->close();

		return res;
		}


sw_status_t
SWServerSocket::accept(SWClientSocket &sock, sw_int64_t timeout)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (!isListening())
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}
		else
			{
			SWSocket	*pSocket=NULL;

			res = _pSocket->accept(pSocket, timeout);
			if (res == SW_STATUS_SUCCESS)
				{
				sock.setSocket(pSocket, true);
				}
			}

		return res;
		}


bool
SWServerSocket::isListening() const
		{
		bool	res=false;

		if (_pSocket != NULL) res = _pSocket->isListening();

		return res;
		}




