/**
*** @file		SWHashCode.h
*** @brief		A class to represent a hash code.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWHashCode class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWHashCode_h__
#define __SWHashCode_h__

#include <swift/swift.h>
#include <swift/SWString.h>


// Forward declarations

/**
*** @brief A class to represent a hash code.
***
*** @note
*** This class does not have a DLL EXPORT as it is only in the header
*** and no code is the shared library for this object.
***
*** @author		Simon Sparkes
**/
class SWHashCode
		{
public:
		/// Construct from an 8-bit signed integer
		SWHashCode(sw_int8_t v)			{ _hashcode = v; }

		/// Construct from a 16-bit signed integer
		SWHashCode(sw_int16_t v)		{ _hashcode = v; }

		/// Construct from a 32-bit signed integer
		SWHashCode(sw_int32_t v)		{ _hashcode = v; }

		/// Construct from a 64-bit signed integer
		SWHashCode(sw_int64_t v)		{ _hashcode = (sw_uint32_t)(v & 0xffffffff); }

		/// Construct from an 8-bit unsigned integer
		SWHashCode(sw_uint8_t v)		{ _hashcode = v; }

		/// Construct from a 16-bit unsigned integer
		SWHashCode(sw_uint16_t v)		{ _hashcode = v; }

		/// Construct from a 32-bit unsigned integer
		SWHashCode(sw_uint32_t v)		{ _hashcode = v; }

		/// Construct from a 64-bit unsigned integer
		SWHashCode(sw_uint64_t v)		{ _hashcode = (sw_uint32_t)(v & 0xffffffff); }

		/// Construct from a SWObject derived object
		SWHashCode(const SWObject &obj)	{ _hashcode = obj.hash(); }

		/// Construct from a SWObject derived object
		SWHashCode(const SWString &obj)	{ _hashcode = obj.hash(); }

		/// Return the calculated hashcode.
		sw_uint32_t	hashcode() const	{ return _hashcode; }

private:
		sw_uint32_t	_hashcode;	///< The hashcode
		};

#endif
