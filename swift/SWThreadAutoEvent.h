/**
*** @file		SWThreadAutoEvent.h
*** @brief		An auto-signal event class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWThreadAutoEvent class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWThreadAutoEvent_h__
#define __SWThreadAutoEvent_h__

#include <swift/SWThreadEvent.h>




/**
*** @brief	A thread-level auto-event synchronisation object.
***
*** SWThreadAutoEvent objects are based around the Windows concept of events for synchonisation.
*** They will be emulated on platforms where native support is not available.
***
*** Auto events are events that will
*** 
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Safe</td>
*** </tr>
*** </table>
***
***
*** @author		Simon Sparkes
*** @see		SWManualThreadAutoEvent, SWAutoThreadAutoEvent
**/
class SWIFT_DLL_EXPORT SWThreadAutoEvent : public SWThreadEvent
		{
public:
		/// Construct a auto-reset event with it's initial state
		SWThreadAutoEvent(bool initialstate=false);
		};





#endif
