/**
*** @file		SWSocketOutputStream.h
*** @brief		A generic socket implementation
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWSocketOutputStream class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWSocketOutputStream_h__
#define __SWSocketOutputStream_h__

#include <swift/swift.h>
#include <swift/SWSocket.h>

#include <swift/SWOutputStream.h>




/**
*** @brief	A socket output stream
***
*** This class writes data being sent to the stream and writes it to the socket.

*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWSocketOutputStream : public SWOutputStream
		{
public:
		/// Default constructor
		SWSocketOutputStream();

		/// Constructor with specific socket implementation
		SWSocketOutputStream(SWSocket *pSocket, bool delflag);

		/// Virtual destructor
		virtual ~SWSocketOutputStream();

		/// Returns a pointer to the socket implementation
		SWSocket *			getSocket()	{ return _pSocket; }

		/// Set the socket implementation
		void				setSocket(SWSocket *pSocket, bool delflag);

protected:
		virtual sw_status_t		writeData(const void *pData, sw_size_t len);	///< Write binary data to the output stream.
		
protected:
		SWSocket	*_pSocket;	///< A pointer to the actual socket implementation
		bool		_delFlag;	///< A flag to indicate if the socket implementator should be delete on destruction
		};




#endif
