/**
*** @file		SWLogMessage.cpp
*** @brief		A class to represent a single log message.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWLogMessage class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWLogMessage.h>


#include <swift/SWLocalTime.h>
#include <swift/sw_printf.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




SWLogMessage::SWLogMessage(
	const sw_timespec_t &ts,	// the log time
	sw_pid_t pid,				// the pid (C++ compatibilty only)
	sw_thread_id_t tid,			// the thread id (C++ compatibilty only)
	sw_uint32_t flags,			// the message flags
	sw_uint16_t category,		// the message category
	const SWString &hostname,	// the host name
	const SWString &appname,	// the application name
	const SWString &filename,	// the filename (where generated)
	sw_int32_t lineno,			// the line number of the file
	const SWString &str,		// the actual message
	sw_uint32_t id,				// the message id (default: 0)
	sw_uint64_t feature,		// the message feature code
	const void	*pData,
	sw_size_t datalen) :
	m_pData(NULL),
	m_datalen(0)
		{
		m_time = ts;
		m_pid = pid;
		m_tid = tid;
		m_flags = (flags & 0xffff) | (((sw_uint32_t)category) << 16);
		m_hostname = hostname;
		m_appname = appname;
		m_filename = filename;
		m_lineno = lineno;
		m_text = str;
		m_msgid = id;
		m_feature = feature;

		if (pData == NULL || datalen == 0)
			{
			m_pData = NULL;
			m_datalen = 0;
			}
		else
			{
			m_pData = malloc(datalen);
			if (m_pData != NULL)
				{
				m_datalen = datalen;
				memcpy(m_pData, pData, m_datalen);
				}
			else
				{
				m_datalen = 0;
				}
			}
		}


SWLogMessage::~SWLogMessage()
		{
		if (m_pData != NULL)
			{
			free(m_pData);
			m_pData = NULL;
			}
		}


SWLogMessage::SWLogMessage(const SWLogMessage &other) :
	m_pData(NULL),
	m_datalen(0)
		{
		*this = other;
		}


SWLogMessage &
SWLogMessage::operator=(const SWLogMessage &other)
		{
		m_time = other.m_time;
		m_pid = other.m_pid;
		m_tid = other.m_tid;
		m_flags = other.m_flags;
		m_hostname = other.m_hostname;
		m_appname = other.m_appname;
		m_filename = other.m_filename;
		m_lineno = other.m_lineno;
		m_text = other.m_text;
		m_msgid = other.m_msgid;
		m_feature = other.m_feature;

		// Free up existing data (if any)
		if (m_pData != NULL)
			{
			free(m_pData);
			m_pData = NULL;
			}

		if (other.m_pData == NULL || other.m_datalen == 0)
			{
			m_pData = NULL;
			m_datalen = 0;
			}
		else
			{
			m_pData = malloc(other.m_datalen);
			if (m_pData != NULL)
				{
				m_datalen = other.m_datalen;
				memcpy(m_pData, other.m_pData, other.m_datalen);
				}
			else
				{
				m_datalen = 0;
				}
			}

		return *this;
		}


SWString
SWLogMessage::format(const SWString &fmt) const
		{
		SWString	tmp;

		format(tmp, fmt);

		return tmp;
		}


#define MAKEFMT(fmt, left, zeropad, width, type) sw_snprintf(fmt, sizeof(fmt), "%%%s%s%d%s", left?"-":"", zeropad?"0":"", width, type)


SWString &
SWLogMessage::format(SWString &outstr, const SWString &fmt) const
		{
		outstr.clear();

		SWString	str;
		SWString	param;
		char		ctmp[16];
		int			p, q, r, len;

		p = 0;
		len = (int)fmt.length();
		while (p < len)
			{
			q = fmt.indexOf("$", p);
			if (q >= p)
				{
				int		width=-1;
				bool	leftalign=false;
				bool	zeropad=false;

				outstr += fmt.substring(p, q-1);
				
				// Now do the substitution on the %
				p = q+1;
				q = fmt.indexOf("}", p+1);
				r = fmt.indexOf(":", p+1);
				if (r < 0 || r > q)
					{
					// No width component
					r = q;
					width = 0;
					}
				else
					{
					// We have a width component
					SWString	wstr = fmt.substring(r+1, q-1);
					width = atoi(wstr);

					if (width < 0)
						{
						width = -width;
						leftalign = 1;
						if (wstr.length() > 1 && wstr[1] == '0') zeropad = true;
						}
					else
						{
						if (wstr.length() > 0 && wstr[0] == '0') zeropad = true;
						}
					}

				if (q-p > 1 && fmt.getCharAt(p) == '{')
					{
					// We have at least %{}, where p is the index of the {
					// and q is the index of the }
					param=fmt.substring(p+1, r-1);

					if (param == "text" || param == "msg")
						{
						outstr += m_text;
						}
					else if (param == "type" || param == "shorttype")
						{
						if (width == 0) width = 3;
						str = getTypeString(type(), true);

						MAKEFMT(ctmp, leftalign, false, width, "s");
						outstr += SWString().format(ctmp, str.c_str());
						}
					else if (param == "longtype")
						{
						if (width == 0) width = 11;
						if (r == q) leftalign = true;


						MAKEFMT(ctmp, leftalign, false, width, "s");
						str = getTypeString(type(), false);
						outstr += SWString().format(ctmp, str.c_str());
						}
					else if (param == "line")
						{
						if (width == 0) width = 4;
						if (m_lineno < 0)
							{
							MAKEFMT(ctmp, leftalign, false, width, "s");
							outstr += SWString().format(ctmp, "");
							}
						else
							{
							MAKEFMT(ctmp, leftalign, zeropad, width, "d");
							outstr += SWString().format(ctmp, m_lineno);
							}
						}
					else if (param == "level")
						{
						if (width == 0) width = 2;

						MAKEFMT(ctmp, leftalign, zeropad, width, "d");
						outstr += SWString().format(ctmp, level());
						}
					else if (param == "category")
						{
						if (width == 0) width = 5;

						MAKEFMT(ctmp, leftalign, zeropad, width, "u");
						outstr += SWString().format(ctmp, category());
						}
					else if (param == "pid" || param == "process")
						{
						if (width == 0) width = 5;

						MAKEFMT(ctmp, leftalign, zeropad, width, "d");
						outstr += SWString().format(ctmp, m_pid);
						}
					else if (param == "tid" || param == "thread")
						{
						if (width == 0) width = 5;

						MAKEFMT(ctmp, leftalign, zeropad, width, "u");
						outstr += SWString().format(ctmp, m_tid);
						}
					else if (param == "id")
						{
						if (width == 0) width = 8;

						MAKEFMT(ctmp, leftalign, zeropad, width, "x");
						outstr += SWString().format(ctmp, m_msgid);
						}
					else if (param == "feature")
						{
						if (width == 0) width = 16;
						if (r == q) zeropad = true;

						MAKEFMT(ctmp, leftalign, zeropad, width, "llx");
						outstr += SWString().format(ctmp, m_feature);
						}
					else if (param == "ms")
						{
						if (width == 0) width = 3;

						MAKEFMT(ctmp, leftalign, zeropad, width, "d");
						outstr += SWString().format(ctmp, m_time.tv_nsec/1000000);
						}
					else if (param == "us")
						{
						if (width == 0) width = 6;

						MAKEFMT(ctmp, leftalign, zeropad, width, "d");
						outstr += SWString().format(ctmp, m_time.tv_nsec/1000);
						}
					else if (param == "ns")
						{
						if (width == 0) width = 9;

						MAKEFMT(ctmp, leftalign, zeropad, width, "d");
						outstr += SWString().format(ctmp, m_time.tv_nsec);
						}
					else if (param == "file")
						{
						if (width == 0) width = 16;
						if (r == q) leftalign = true;

						MAKEFMT(ctmp, leftalign, false, width, "s");
						outstr += SWString().format(ctmp, m_filename.c_str());
						}
					else if (param == "app" || param == "application")
						{
						if (width == 0) width = 16;
						if (r == q) leftalign = true;

						MAKEFMT(ctmp, leftalign, false, width, "s");
						outstr += SWString().format(ctmp, m_appname.c_str());
						}
					else if (param == "host" || param == "hostname")
						{
						if (width == 0) width = 16;
						if (r == q) leftalign = true;

						MAKEFMT(ctmp, leftalign, false, width, "s");
						outstr += SWString().format(ctmp, m_hostname.c_str());
						}
					else if (param == "date")
						{
						if (width == 0) width = 10;

						MAKEFMT(ctmp, leftalign, false, width, "s");
						str = SWLocalTime(m_time).format("%d/%m/%Y");
						outstr += SWString().format(ctmp, str.c_str());
						}
					else if (param == "day")
						{
						if (width == 0) width = 2;

						MAKEFMT(ctmp, leftalign, false, width, "s");
						str = SWLocalTime(m_time).format("%d");
						outstr += SWString().format(ctmp, str.c_str());
						}
					else if (param == "month")
						{
						if (width == 0) width = 2;

						MAKEFMT(ctmp, leftalign, false, width, "s");
						str = SWLocalTime(m_time).format("%m");
						outstr += SWString().format(ctmp, str.c_str());
						}
					else if (param == "year")
						{
						if (width == 0) width = 4;

						MAKEFMT(ctmp, leftalign, false, width, "s");
						str = SWLocalTime(m_time).format("%Y");
						outstr += SWString().format(ctmp, str.c_str());
						}
					else if (param == "time")
						{
						if (width == 0) width = 8;

						MAKEFMT(ctmp, leftalign, false, width, "s");
						str = SWLocalTime(m_time).format("%H:%M:%S.");
						str += SWString().format("%03d", m_time.tv_nsec/1000000);
						outstr += SWString().format(ctmp, str.c_str());
						}
					else if (param == "hour")
						{
						if (width == 0) width = 2;

						MAKEFMT(ctmp, leftalign, false, width, "s");
						str = SWLocalTime(m_time).format("%H");
						outstr += SWString().format(ctmp, str.c_str());
						}
					else if (param == "minute")
						{
						if (width == 0) width = 2;

						MAKEFMT(ctmp, leftalign, false, width, "s");
						str = SWLocalTime(m_time).format("%M");
						outstr += SWString().format(ctmp, str.c_str());
						}
					else if (param == "second")
						{
						if (width == 0) width = 2;

						MAKEFMT(ctmp, leftalign, false, width, "s");
						str = SWLocalTime(m_time).format("%S");
						outstr += SWString().format(ctmp, str.c_str());
						}
					else if (param == "datalen")
						{
						if (width == 0) width = 5;

						MAKEFMT(ctmp, leftalign, zeropad, width, "u");
						outstr += SWString().format(ctmp, datalen());
						}
					else if (param == "data")
						{
						if (m_pData != NULL)
							{
							sw_size_t		pos;
							unsigned char	*pData=(unsigned char *)m_pData;

							for (pos=0;pos<m_datalen;pos++,pData++)
								{
								sw_snprintf(ctmp, sizeof(ctmp), "%s%02X", pos?" ":"", (*pData) & 0xff);
								outstr += ctmp;
								}
							}
						}
					
					p = q+1;
					}
				else
					{
					outstr += '$';
					}
				}
			else
				{
				// Copy the rest of the string and then stop
				outstr += fmt.mid(p);
				p = len;
				}
			}

		// Return a reference to the outstr parameter
		return outstr;
		}


// Return the given log type as a string
const sw_tchar_t *
SWLogMessage::getTypeString(sw_uint32_t type, bool shortform)
		{
		const sw_tchar_t *s = _T("");

		if ((type & SW_LOG_TYPE_SUCCESS) != 0)			s = (shortform ?	_T("SUC")	:	_T("Success")		);
		else if ((type & SW_LOG_TYPE_FATAL) != 0)		s = (shortform ?	_T("FTL")	:	_T("Fatal")			);
		else if ((type & SW_LOG_TYPE_ERROR) != 0)		s = (shortform ?	_T("ERR")	:	_T("Error")			);
		else if ((type & SW_LOG_TYPE_WARNING) != 0)		s = (shortform ?	_T("WRN")	:	_T("Warning")		);
		else if ((type & SW_LOG_TYPE_INFO) != 0)		s = (shortform ?	_T("INF")	:	_T("Information")	);
		else if ((type & SW_LOG_TYPE_DEBUG) != 0)		s = (shortform ?	_T("DBG")	:	_T("Debug")			);
		else if ((type & SW_LOG_TYPE_STDERR) != 0)		s = (shortform ?	_T("STE")	:	_T("stderr")		);
		else if ((type & SW_LOG_TYPE_STDOUT) != 0)		s = (shortform ?	_T("OUT")	:	_T("stdout")		);
		else if ((type & SW_LOG_TYPE_NOTICE) != 0)		s = (shortform ?	_T("NOT")	:	_T("Notice")		);
		else if ((type & SW_LOG_TYPE_CRITICAL) != 0)	s = (shortform ?	_T("CRT")	:	_T("Critical")		);
		else if ((type & SW_LOG_TYPE_EMERGENCY) != 0)	s = (shortform ?	_T("EMG")	:	_T("Emergency")		);
		else if ((type & SW_LOG_TYPE_ALERT) != 0)		s = (shortform ?	_T("ALT")	:	_T("Alert")			);
		else											s = (shortform ?	_T("???")	:	_T("Unknown")		);

		return s;
		}


// end the namespace

