/**
*** @file		SWDataObject.cpp
*** @brief		The generic base object from which all library objects are defined
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWDataObject class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <swift/SWDataObject.h>
#include <swift/SWInputStream.h>
#include <swift/SWOutputStream.h>




SWDataObject::SWDataObject() :
	m_changed(false),
	m_keysAreCaseInsensitive(false)
		{
		}


SWDataObject::~SWDataObject()
		{
		}


SWDataObject::SWDataObject(const SWDataObject &other)
		{
		*this = other;
		}

void
SWDataObject::copyValues(const SWDataObject &other)
		{
		SWIterator< SWHashMapEntry<SWString, SWValue> >		it;
		SWHashMapEntry<SWString, SWValue>					*pEntry;
		
		it = other.m_param.iterator();
		while (it.hasNext())
			{
			pEntry = it.next();
			set(pEntry->key(), pEntry->value());
			}
		}


SWDataObject &
SWDataObject::operator=(const SWDataObject &other)
		{
		clear();

#define COPY(x) x = other.x
		COPY(m_changed);
		COPY(m_keysAreCaseInsensitive);
#undef COPY

		m_param.clear();

		SWIterator< SWHashMapEntry<SWString, SWValue> >		it;
		SWHashMapEntry<SWString, SWValue>					*pEntry;
		
		it = other.m_param.iterator();
		while (it.hasNext())
			{
			pEntry = it.next();
			m_param.put(pEntry->key(), pEntry->value());
			}

		return *this;
		}
		
		
void
SWDataObject::clear()
		{
		m_param.clear();
		m_changed = false;
		}


SWString
SWDataObject::getKeyName(const SWString &key) const
		{
		SWString	res=key;

		if (m_keysAreCaseInsensitive) res.MakeLower();

		return res;
		}


bool
SWDataObject::set(const SWString &key, const SWValue &value)
		{
		SWValue		v;
		SWString	keyname=getKeyName(key);

		if (!m_param.get(keyname, v) || v != value)
			{
			m_changed = true;
			m_param.put(keyname, value);
			}
		
		return true;
		}
		

void
SWDataObject::remove(const SWString &key)
		{
		m_param.remove(getKeyName(key));
		}
		

bool
SWDataObject::contains(const SWString &key) const
		{
		return m_param.containsKey(getKeyName(key));
		}

	
bool
SWDataObject::get(const SWString &key, SWValue &value) const
		{
		value.clear();

		return m_param.get(getKeyName(key), value);
		}


SWString
SWDataObject::getString(const SWString &key) const
		{
		return getString(key, _T(""));
		}


SWString
SWDataObject::getString(const SWString &key, const SWString &defval) const
		{
		SWString	s;
		SWValue		v;
		
		if (get(key, v))
			{
			s = v.toString();
			}
		else
			{
			s = defval;
			}
		
		return s;
		}


SWMoney
SWDataObject::getMoney(const SWString &key) const
		{
		SWMoney	m;
		SWValue	v;
		
		if (get(key, v))
			{
			m = v.toMoney();
			}
		
		return m;
		}



SWDouble
SWDataObject::getDouble(const SWString &key, double defval) const
		{
		SWDouble	d;
		SWValue		v;
		
		if (get(key, v))
			{
			d = v.toDouble();
			}
		else
			{
			d = defval;
			}
		
		return d;
		}


bool
SWDataObject::getBoolean(const SWString &key, bool defval) const
		{
		bool		res=false;
		SWValue		v;
		
		if (get(key, v))
			{
			res = v.toBoolean();
			}
		else
			{
			res = defval;
			}
		
		return res;
		}



sw_int16_t
SWDataObject::getInt16(const SWString &key, sw_int16_t defval) const
		{
		sw_int16_t	s;
		SWValue		v;
		
		if (get(key, v))
			{
			s = v.toInt16();
			}
		else
			{
			s = defval;
			}
		
		return s;
		}


sw_uint16_t
SWDataObject::getUInt16(const SWString &key, sw_uint16_t defval) const
		{
		sw_uint16_t	s;
		SWValue		v;
		
		if (get(key, v))
			{
			s = v.toUInt16();
			}
		else
			{
			s = defval;
			}
		
		return s;
		}


sw_int32_t
SWDataObject::getInt32(const SWString &key, sw_int32_t defval) const
		{
		SWInt32	s;
		SWValue		v;
		
		if (get(key, v))
			{
			s = v.toInt32();
			}
		else
			{
			s = defval;
			}
		
		return s;
		}


sw_uint32_t
SWDataObject::getUInt32(const SWString &key, sw_uint32_t defval) const
		{
		SWUInt32	s;
		SWValue		v;
		
		if (get(key, v))
			{
			s = v.toUInt32();
			}
		else
			{
			s = defval;
			}
		
		return s;
		}


sw_int64_t
SWDataObject::getInt64(const SWString &key, sw_int64_t defval) const
		{
		sw_int64_t	s;
		SWValue		v;
		
		if (get(key, v))
			{
			s = v.toInt64();
			}
		else
			{
			s = defval;
			}
		
		return s;
		}


sw_uint64_t
SWDataObject::getUInt64(const SWString &key, sw_uint64_t defval) const
		{
		sw_uint64_t	s;
		SWValue		v;
		
		if (get(key, v))
			{
			s = v.toUInt64();
			}
		else
			{
			s = defval;
			}
		
		return s;
		}

SWValue
SWDataObject::getValue(const SWString &key) const
		{
		SWValue		v;
		
		get(key, v);
		
		return v;
		}


SWDate
SWDataObject::getDate(const SWString &key) const
		{
		SWDate		dt;
		SWValue		v;
		
		if (get(key, v))
			{
			if (v.type() == SWValue::VtTime)
				{
				// When a time - is in localtime format
				if (v.toInt32() != 0)
					{
					dt = SWLocalTime(v.toTime());
					}
				}
			else
				{
				dt = v.toDate();
				}
			}
		
		return dt;
		}


SWLocalTime
SWDataObject::getTime(const SWString &key) const
		{
		SWLocalTime	t;
		SWValue		v;
		
		if (get(key, v))
			{
			t = SWLocalTime(v.toTime());
			}
		
		return t;
		}


void
SWDataObject::getValueNames(SWStringArray &sa) const
		{
		sa.clear();

		SWIterator<SWString>	it=m_param.keys();

		while (it.hasNext())
			{
			sa.add(*it.next());
			}
		}


bool
SWDataObject::setFlag(const SWString &key, int id, bool v)
		{
		bool		res=true;
		SWValue		value;
		
		get(key, value);
		if (id >=0 && id < 32)
			{
			sw_uint32_t	flags;
			sw_uint32_t	bits;

			flags = value.toUInt32();
			bits = (1 << id);
				
			if (v)
				{
				flags |= bits;
				}
			else
				{
				flags &= ~bits;
				}
				
			set(key, flags);
			}
		else
			{
			res = false;
			}
		
		return res;
		}


bool
SWDataObject::isFlagSet(const SWString &key, int id) const
		{
		bool	v=false;
		
		getFlag(key, id, v);
		
		return v;
		}


bool
SWDataObject::getFlag(const SWString &key, int id, bool &v) const
		{
		bool		res=true;
		SWValue		value;
		
		if (id >=0 && id < 32 && get(key, value))
			{
			sw_uint32_t	flags;
		
			get(key, value);
		
			flags = value.toUInt32();

			sw_uint32_t	bits;
			
			bits = (1 << id);
			
			v = ((flags & bits) != 0);
			}
		else
			{
			v = false;
			res = false;
			}
		
		return res;
		}






sw_status_t
SWDataObject::readFromStream(SWInputStream &stream)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		clear();

		sw_uint32_t	nentries=0;

		if (res == SW_STATUS_SUCCESS) res = stream.read(nentries);
		if (res == SW_STATUS_SUCCESS) res = stream.read(m_keysAreCaseInsensitive);

		SWValue	value;
		SWString		param;

		for (sw_uint32_t i=0;res==SW_STATUS_SUCCESS&&i<nentries;i++)
			{
			if (res == SW_STATUS_SUCCESS) res = stream.read(param);
			if (res == SW_STATUS_SUCCESS) res = stream.read(value);

			set(param, value);
			}

		return res;
		}


sw_status_t
SWDataObject::writeToStream(SWOutputStream &stream) const
		{
		sw_status_t			res=SW_STATUS_SUCCESS;
		SWStringArray		keys;
		SWIterator<SWString>	it=m_param.keys();

		while (it.hasNext())
			{
			keys.add(*it.next());
			}

		sw_uint32_t	nentries=(sw_uint32_t)keys.size();
		SWValue	v;

		if (res == SW_STATUS_SUCCESS) res = stream.write(nentries);
		if (res == SW_STATUS_SUCCESS) res = stream.write(m_keysAreCaseInsensitive);

		for (sw_uint32_t i=0;res==SW_STATUS_SUCCESS&&i<nentries;i++)
			{
			get(keys[i],  v);

			if (res == SW_STATUS_SUCCESS) res = stream.write(keys[i]);
			if (res == SW_STATUS_SUCCESS) res = stream.write(v);
			}

		return res;
		}

