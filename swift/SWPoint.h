/**
*** @file		SWPoint.h
*** @brief		A class to represent a point or x/y coordinate
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWPoint class.
**/

/*********************************************************************************
Copyright (c) 2006-2016 SwiftTec Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWPoint_h__
#define __SWPoint_h__

#include <swift/SWObject.h>
#include <swift/SWOutputStream.h>
#include <swift/SWInputStream.h>

// Forward declarations

/**
*** @brief	A class to represent a point or x/y coordinate
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Unsafe</td>
*** </tr>
*** </table>
***
*** @note
*** This class does not have a DLL EXPORT as it is only in the header
*** and no code is the shared library for this object.
***
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWPoint : public SWObject
		{
public:
		/// Constructor
		SWPoint(sw_int32_t x=0, sw_int32_t y=0);

		/// Copy constructor
		SWPoint(const SWPoint &other);

		/// Assignment operator
		SWPoint &	operator=(const SWPoint &other);

		/// Set the X coordinate
		void		X(sw_int32_t v)					{ m_xpos = v; }

		/// Return the X coordinate
		sw_int32_t	X() const						{ return m_xpos; }

		/// Set the Y coordinate
		void		Y(sw_int32_t v)					{ m_ypos = v; }

		/// Return the Y coordinate
		sw_int32_t	Y() const						{ return m_ypos; }

		/// Return the X coordinate
		sw_int32_t	getX() const					{ return m_xpos; }

		/// Return the Y coordinate
		sw_int32_t	getY() const					{ return m_ypos; }

		/// Set the X and Y coordinates
		void		set(sw_int32_t x, sw_int32_t y)	{ m_xpos = x; m_ypos = y; }


#if defined(SW_PLATFORM_WINDOWS)
		/// Convert to a COORD - win32 only
		operator COORD() const;
#endif

		/// Check for equality
		bool	operator==(const SWPoint &other) const	{ return (m_xpos == other.m_xpos && m_ypos == other.m_ypos); }

		/// Check for inequality
		bool	operator!=(const SWPoint &other) const	{ return (m_xpos != other.m_xpos || m_ypos != other.m_ypos); }

public: // virtual overrides

		/// Override for SWObject writeToStream
		virtual sw_status_t	writeToStream(SWOutputStream &stream) const;

		/// Override for SWObject readFromStream
		virtual sw_status_t	readFromStream(SWInputStream &stream);

private:
		sw_int32_t	m_xpos;	///< The X coordinate
		sw_int32_t	m_ypos;	///< The Y coordinate
		};

#endif
