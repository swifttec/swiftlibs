/**
*** @file		SWLogFile.cpp
*** @brief		A class to represent a single log message.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWLogFile class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"


#include <swift/SWLogFile.h>


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif


SWLogFile::SWLogFileThread::SWLogFileThread(SWLogFile *pLogFile) :
	m_pLogFile(pLogFile)
		{
		}


int
SWLogFile::SWLogFileThread::run()
		{
		SWString	line;

		while (runnable())
			{
			while (m_pLogFile->getNextLine(line))
				{
				m_pLogFile->writeLineToFile(line);
				}

			SWThread::sleep(100);
			}

		return 0;
		}


SWLogFile::SWLogFile(int flags, const SWString &filename, sw_size_t maxsize, int backupcount) :
	m_bgthread(this),
	m_file(NULL),
	m_flags(flags),
	m_backupFileCount(0),
	m_maxFileSize(0),
	m_flush(false)
		{
		synchronize();
		open(filename, maxsize, backupcount);
		m_bgthread.start();
		}


SWLogFile::~SWLogFile()
		{
		m_bgthread.stop(true);
		close();
		}


sw_status_t
SWLogFile::writeLineToFile(const SWString &line)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		
		if (m_file == NULL && !m_filename.isEmpty())
			{
			// The file is not open, try to open/create it
			m_file = fopen(m_filename, "a");
			}

		// We can do nothing if the print stream is NULL
		if (m_file != NULL)
			{
			// Have a valid file stream. If we have size limitations check
			// them now and deal with them accoring to parameters
			if (!m_filename.isEmpty() && m_maxFileSize != 0 && (sw_size_t)ftell(m_file) >= m_maxFileSize)
				{
				// We have exceeded the maximum requested file size
				fclose(m_file);
				m_file = NULL;
				
				if (m_backupFileCount < 2)
					{
					// Rename file from xxx to xxx.old
					SWString	newfilename(m_filename);

					newfilename += ".old";
					unlink(newfilename);
					rename(m_filename, newfilename);
					}
				else
					{
					// Rename xxx to xxx.1
					//        xxx.1 to xxx.2
					// and delete xxx.n
					SWString	bakfile, bakfile2;
					char		tmpstr[12];

					sprintf(tmpstr, "%d", m_backupFileCount);
					bakfile = m_filename;
					bakfile += ".";
					bakfile += tmpstr;
					unlink(bakfile);
					for (int i=m_backupFileCount;i>1;i--)
						{
						sprintf(tmpstr, "%d", i);
						bakfile2 = m_filename;
						bakfile2 += ".";
						bakfile2 += tmpstr;

						sprintf(tmpstr, "%d", i-1);
						bakfile = m_filename;
						bakfile += ".";
						bakfile += tmpstr;

						rename(bakfile, bakfile2);
						}

					rename(m_filename, bakfile);
					}
				}
			}

		// We may need to re-open the if we closed it as part of the above
		// max size code.
		if (m_file == NULL && !m_filename.isEmpty())
			{
			// The file is not open, try to open/create it
			m_file = fopen(m_filename, "a");
			}

		if (m_file != NULL)
			{
			fputs(line, m_file);
			fputc('\n', m_file);

			if (!keepOpen())
				{
				res = closeFile();
				}
			else if (autoFlush() || m_flush)
				{
				res = flushFile();
				}

			m_flush = false;
			}
			
		return res;
		}


sw_status_t
SWLogFile::writeLine(const SWString &text)
		{
		SWGuard	guard(this);

		m_data.addTail(text);

		return SW_STATUS_SUCCESS;
		}


bool
SWLogFile::getNextLine(SWString &line)
		{
		SWGuard	guard(this);
		bool	res=false;

		if (m_data.size() != 0)
			{
			res = m_data.removeHead(&line);
			}

		return res;
		}


sw_status_t
SWLogFile::open(const SWString &filename)
		{
		close();

		m_filename = filename;

		return SW_STATUS_SUCCESS;
		}


sw_status_t
SWLogFile::open(const SWString &filename, sw_size_t maxsize, int backupcount)
		{
		close();

		m_filename = filename;
		m_maxFileSize = maxsize;
		m_backupFileCount = backupcount;

		return SW_STATUS_SUCCESS;
		}


sw_status_t
SWLogFile::close()
		{
		lock();
		while (m_data.size() != 0)
			{
			unlock();
			SWThread::yield();
			lock();
			}

		if (m_file != NULL)
			{
			fclose(m_file);
			m_file = NULL;
			}

		unlock();

		return SW_STATUS_SUCCESS;
		}


sw_status_t
SWLogFile::closeFile()
		{
		if (m_file != NULL)
			{
			fclose(m_file);
			m_file = NULL;
			}

		return SW_STATUS_SUCCESS;
		}


sw_status_t
SWLogFile::flush()
		{
		if (m_file != NULL)
			{
			fflush(m_file);
			}

		return SW_STATUS_SUCCESS;
		}


sw_status_t
SWLogFile::flushFile()
		{
		if (m_file != NULL)
			{
			fflush(m_file);
			}

		return SW_STATUS_SUCCESS;
		}




