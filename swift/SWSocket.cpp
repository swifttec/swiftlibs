/**
*** @file		SWSocket.cpp
*** @brief		A generic socket implementation
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWSocket class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/sw_net.h>
#include <swift/SWSocket.h>
#include <swift/SWGuard.h>


#if !defined(SW_PLATFORM_WINDOWS)
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>
	#include <netinet/tcp.h>

	#if defined(SW_PLATFORM_AIX)
		#include <strings.h>
	#endif

	#if defined(SW_PLATFORM_MACH)
		// Used in linux to prevent SIGPIPE errors on socket writes, but not available under Mac
		#define MSG_NOSIGNAL	0
	#endif
#else
	#include <errno.h>
	#if defined(SW_BUILD_MFC)
		#include <winsock2.h>
	#endif

	// Used in linux to prevent SIGPIPE errors on socket writes, but not available under Windows
	#define MSG_NOSIGNAL	0
#endif


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif



SWSocket::SWSocket() :
	_fd(SW_NET_INVALID_SOCKET)
		{
		}


SWSocket::~SWSocket()
		{
		if (_fd != SW_NET_INVALID_SOCKET)
			{
			SWSocket::close(_fd);
			}
		}



sw_status_t
SWSocket::waitForInput(sw_uint64_t waitms)
		{
		return setLastError(SWSocket::waitForInput(_fd, waitms));
		}


sw_status_t
SWSocket::bind(const SWSocketAddress &addr)
		{
		sw_status_t	res;

		res = SWSocket::bind(_fd, addr);
		if (res == SW_STATUS_SUCCESS) _flags.setBit(SW_SOCKFLAG_BOUND);

		return setLastError(res);
		}


sw_status_t
SWSocket::connect(const SWSocketAddress &addr)
		{
		SWGuard		guard(this);
		sw_status_t	res=SW_STATUS_SUCCESS;

		// Check to make sure we are not connected already
		if (isConnected() || isListening())
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}
		else if (addr.type() != SWSocketAddress::IP4)
			{
			res = SW_STATUS_INVALID_PARAMETER;
			}

		if (res == SW_STATUS_SUCCESS && !isOpen())
			{
			// Auto-open the socket
			res = open();
			}

		if (res == SW_STATUS_SUCCESS)
			{
			res = SWSocket::connect(_fd, addr);
			if (res != SW_STATUS_SUCCESS)
				{
				SWSocket::close(_fd);
				_fd = SW_NET_INVALID_SOCKET;
				}
			else
				{
				_flags.setBit(SW_SOCKFLAG_CONNECTED);
				_addr = addr;
				}
			}

		return setLastError(res);
		}


sw_status_t		
SWSocket::read(void *pBuffer, sw_size_t bytesToRead, sw_size_t *pBytesRead)
		{
		SWGuard		guard(this);
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (!isStream() && !isConnected() && !isBound())
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}
		else
			{
			res = SWSocket::recv(_fd, pBuffer, bytesToRead, pBytesRead, false, isStream());
			}

		return setLastError(res);
		}


sw_status_t		
SWSocket::receive(void *pBuffer, sw_size_t bytesToRead, sw_size_t *pBytesRead)
		{
		SWGuard		guard(this);
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (!isConnected() && !isBound())
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}
		else
			{
			res = SWSocket::recv(_fd, pBuffer, bytesToRead, pBytesRead, false, false);
			}

		return setLastError(res);
		}


sw_status_t		
SWSocket::write(const void *pBuffer, sw_size_t bytesToWrite, sw_size_t *pBytesWritten)
		{
		SWGuard		guard(this);
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (!isConnected())
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}
		else
			{
			res = SWSocket::send(_fd, pBuffer, bytesToWrite, pBytesWritten);
			}

		return setLastError(res);
		}


sw_status_t
SWSocket::open(int domain, int type, int protocol, sw_sockhandle_t &sock)
		{
		if (!sw_net_initialised()) sw_net_init();

		sw_sockhandle_t	fd;
		sw_status_t		res=SW_STATUS_SUCCESS;

		fd = (sw_sockhandle_t)::socket(domain, type, protocol);
#if defined(SW_PLATFORM_WINDOWS)
		if (fd == INVALID_SOCKET) 
#else
		if (fd < 0)
#endif
			{
			res = sw_net_getLastError();
			sock = SW_NET_INVALID_SOCKET;
			}
		else sock = fd;

		return res;
		}


sw_status_t
SWSocket::shutdown(sw_sockhandle_t sock, int how)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (::shutdown(sock, how) < 0) res = sw_net_getLastError();

		return res;
		}



sw_status_t
SWSocket::close(sw_sockhandle_t sock)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

#if defined(WIN32) || defined(_WIN32)
		if (::closesocket(sock) != 0)
#else
		if (::close(sock) != 0)
#endif
			{
			res = sw_net_getLastError();
			}

		return res;
		}


sw_status_t
SWSocket::connect(sw_sockhandle_t sock, const SWSocketAddress &addr)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (::connect(sock, (const struct sockaddr *)addr.data(), (int)addr.length()) < 0) res = sw_net_getLastError();

		return res;
		}


sw_status_t
SWSocket::bind(sw_sockhandle_t sock, const SWSocketAddress &addr)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		
		if (::bind(sock, (const struct sockaddr *)addr.data(), (int)addr.length()) < 0) res = sw_net_getLastError();

		return res;
		}


sw_status_t
SWSocket::listen(sw_sockhandle_t sock, int maxPending)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (::listen(sock, maxPending) < 0) res = sw_net_getLastError();

		return res;
		}


sw_status_t
SWSocket::send(sw_sockhandle_t sock, const void *pBuffer, sw_size_t bytesToSend, sw_size_t *pBytesSent)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		sw_size_t	count=0;
		int			r=0;

		while (bytesToSend > 0)
			{
			int	tries=0;

			while ((r = ::send(sock, (char *)pBuffer + count, (int)bytesToSend, MSG_NOSIGNAL)) < 0 && (errno == EINTR || errno == EAGAIN) && tries < 5)
				{
				tries++;
				}

			if (r > 0)
				{
				count += r;
				bytesToSend -= r;
				}
			else
				{
				res = sw_net_getLastError();
				break;
				}
			}

		if (pBytesSent != NULL) 
			{
			*pBytesSent = count;
			}

		return res;
		}


sw_status_t
SWSocket::sendto(sw_sockhandle_t sock, const SWSocketAddress &addr, const void *pBuffer, sw_size_t bytesToSend, sw_size_t *pBytesSent)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		sw_size_t	count=0;
		int			r=0;

		while (bytesToSend > 0)
			{
			int	tries=0;

			while ((r = ::sendto(sock, (char *)pBuffer + count, (int)bytesToSend, 0, (const struct sockaddr *)addr.data(), (int)addr.length())) < 0 && (errno == EINTR || errno == EAGAIN) && tries < 5)
				{
				tries++;
				}

			if (r > 0)
				{
				count += r;
				bytesToSend = 0;
				}
			else
				{
				res = sw_net_getLastError();
				break;
				}
			}

		if (pBytesSent != NULL) 
			{
			*pBytesSent = count;
			}

		return res;
		}


sw_status_t
SWSocket::recv(sw_sockhandle_t sock, void *pBuffer, sw_size_t bytesToReceive, sw_size_t *pBytesReceived, bool peek, bool waitForAllData)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		sw_size_t	count=0;
		int			r=0;
		int			flags=0;

		if (peek) flags = MSG_PEEK;

		while (bytesToReceive > 0)
			{
			int	tries=0;

			while ((r = ::recv(sock, (char *)pBuffer + count, (int)bytesToReceive, flags)) < 0 && (errno == EINTR || errno == EAGAIN) && tries < 5)
				{
				tries++;
				}

			if (r > 0)
				{
				count += r;
				bytesToReceive -= r;
				if (!waitForAllData)
					{
					break;
					}
				}
			else
				{
				res = sw_net_getLastError();
				if (res == 0 && count == 0)
					{
					// This is not an error, but a socket closed situation
					res = SW_STATUS_NO_MORE_DATA;
					}
#if defined(SW_PLATFORM_WINDOWS)
				else if (res == WSAETIMEDOUT)
#else
				else if (res == EAGAIN || res == EWOULDBLOCK)
#endif
					{
					res = SW_STATUS_TIMEOUT;
					}

				break;
				}
			}

		if (pBytesReceived != NULL) 
			{
			*pBytesReceived = count;
			}

		return res;
		}


sw_status_t
SWSocket::recvfrom(sw_sockhandle_t sock, SWSocketAddress &addr, void *pBuffer, sw_size_t bytesToReceive, sw_size_t *pBytesReceived, bool peek)
		{
		sw_status_t		res=SW_STATUS_SUCCESS;
		sw_size_t		count=0;
		int				r=0;
		int				flags=0;
		sw_sockaddr_t	fromaddr;
#if defined(SW_PLATFORM_UNIX)
		socklen_t		fromaddrlen=sizeof(fromaddr);
#else
		int				fromaddrlen=sizeof(fromaddr);
#endif

		if (peek) flags = MSG_PEEK;

		while (bytesToReceive > 0)
			{
			int	tries=0;

			while ((r = ::recvfrom(sock, (char *)pBuffer + count, (int)bytesToReceive, flags, (struct sockaddr *)&fromaddr, &fromaddrlen)) < 0 && (errno == EINTR || errno == EAGAIN) && tries < 5)
				{
				tries++;
				}

			if (r > 0)
				{
				count += r;
				bytesToReceive = 0;
				addr.set((const struct sockaddr *)&fromaddr, fromaddrlen);
				}
			else
				{
				res = sw_net_getLastError();
				if (res == 0 && count == 0)
					{
					// This is not an error, but a socket closed situation
					res = SW_STATUS_NO_MORE_DATA;
					}
				break;
				}
			}

		if (pBytesReceived != NULL) 
			{
			*pBytesReceived = count;
			}

		return res;
		}


sw_status_t
SWSocket::accept(sw_sockhandle_t sock, sw_sockhandle_t &insock, SWSocketAddress &inaddr)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		inaddr.clear();
		insock = SW_NET_INVALID_SOCKET;

		struct sockaddr_in	sin_in;
#if defined(WIN32) || defined(_WIN32)
		SOCKET				in_sock;
		int					in_len;
#else
		int					in_sock;
		socklen_t			in_len;
#endif

		in_len = sizeof(sin_in);
		int	tries=0;
		while ((in_sock = ::accept(sock, (struct sockaddr *)&sin_in, &in_len)) < 0 && (errno == EINTR || errno == EAGAIN) && tries < 5)
			{
			tries++;
			}

#if defined(SW_PLATFORM_WINDOWS)
		if (in_sock == INVALID_SOCKET) 
#else
		if (in_sock < 0)
#endif
			{
			res = sw_net_getLastError();
			}
		else
			{
			inaddr.set((struct sockaddr *)&sin_in, in_len);
			insock = (sw_sockhandle_t)in_sock;
			}

		return res;
		}




sw_status_t
SWSocket::waitForInput(sw_sockhandle_t sock, sw_uint64_t waitms)
		{
		fd_set	rfds;
		int		r;

		FD_ZERO(&rfds);
#if defined(SW_PLATFORM_WINDOWS)
	#if defined(_MSC_VER)
		#pragma warning(disable: 4127) // conditional expression is constant
	#endif
		FD_SET((sw_uint_t)sock, &rfds);
	#if defined(_MSC_VER)
		#pragma warning(default: 4127) // conditional expression is constant
	#endif
#else
		FD_SET(sock, &rfds);
#endif
		struct timeval	tv, *tvp;

		if (waitms == SW_TIMEOUT_INFINITE) tvp = NULL;
		else
			{
			tvp = &tv;
			tv.tv_sec = (long)(waitms / 1000);
			tv.tv_usec = (long)((waitms % 1000) * 1000);
			}

		int	tries=0;
		while ((r = select(sock+1, &rfds, 0, 0, tvp)) < 0 && (errno == EINTR || errno == EAGAIN) && tries < 5)
			{
			tries++;
			};

		sw_status_t	res=SW_STATUS_SUCCESS;

		if (r < 0) res = sw_net_getLastError();
		else if (r == 0) res = SW_STATUS_TIMEOUT;
			
		return res;
		}


sw_status_t
SWSocket::setOption(sw_sockhandle_t sock, int level, int option, int value)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (::setsockopt(sock, level, option, (char *)&value, sizeof(value)) < 0) res = sw_net_getLastError();

		return res;
		}


sw_status_t
SWSocket::getOption(sw_sockhandle_t sock, int level, int option, int &value)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
#if defined(SW_PLATFORM_UNIX)
		socklen_t	len=sizeof(value);
#else
		int			len=sizeof(value);
#endif

		if (::getsockopt(sock, level, option, (char *)&value, &len) < 0) res = sw_net_getLastError();

		return res;
		}


sw_status_t
SWSocket::setOption(SocketOption option, const SWValue &optval)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		int			level=0;
		int			opt=0;
		int			iv=0;

		switch (option)
			{
			// General socket options
			case SW_SOCKOPT_DEBUG:
				iv = optval.toBoolean();
				level = SOL_SOCKET;
				opt = SO_DEBUG;
				break;

			case SW_SOCKOPT_REUSEADDR:
				iv = optval.toBoolean();
				level = SOL_SOCKET;
				opt = SO_REUSEADDR;
				break;

			case SW_SOCKOPT_KEEPALIVE:
				iv = optval.toBoolean();
				level = SOL_SOCKET;
				opt = SO_KEEPALIVE;
				break;

			case SW_SOCKOPT_DONTROUTE:
				iv = optval.toBoolean();
				level = SOL_SOCKET;
				opt = SO_DONTROUTE;
				break;

			case SW_SOCKOPT_LINGER:
				iv = optval.toBoolean();
				level = SOL_SOCKET;
				opt = SO_LINGER;
				break;

			case SW_SOCKOPT_BROADCAST:
				iv = optval.toBoolean();
				level = SOL_SOCKET;
				opt = SO_BROADCAST;
				break;

			case SW_SOCKOPT_OOBINLINE:
				iv = optval.toBoolean();
				level = SOL_SOCKET;
				opt = SO_OOBINLINE;
				break;

			case SW_SOCKOPT_DGRAM_ERRIND:
#if defined(SO_DGRAM_ERRIND)
				iv = optval.toBoolean();
				level = SOL_SOCKET;
				opt = SO_DGRAM_ERRIND;
#else
				res = SW_STATUS_UNSUPPORTED_OPERATION;
#endif
				break;


			case SW_SOCKOPT_SEND_BUFFER_SIZE:
				iv = optval.toInt32();
				level = SOL_SOCKET;
				opt = SO_SNDBUF;
				break;

			case SW_SOCKOPT_RECEIVE_BUFFER_SIZE:
				iv = optval.toInt32();
				level = SOL_SOCKET;
				opt = SO_RCVBUF;
				break;

			case SW_SOCKOPT_SEND_TIMEOUT:
				iv = optval.toInt32();
				level = SOL_SOCKET;
				opt = SO_SNDTIMEO;
				break;

			case SW_SOCKOPT_RECEIVE_TIMEOUT:
				iv = optval.toInt32();
				level = SOL_SOCKET;
				opt = SO_RCVTIMEO;
				break;



			// TCP-specific socket options
			case SW_SOCKOPT_TCP_NODELAY:
				iv = optval.toBoolean();
				level = IPPROTO_TCP;
				opt = TCP_NODELAY;
				break;

			default:
				// This is an unrecognised option
				res = SW_STATUS_INVALID_PARAMETER;
				break;
			}

		if (res == SW_STATUS_SUCCESS)
			{
			res = setOption(_fd, level, opt, iv);
			}

		return setLastError(res);
		}


sw_status_t
SWSocket::getOption(SocketOption option, SWValue &optval)
		{
		sw_status_t			res=SW_STATUS_SUCCESS;
		int					level=0;
		int					opt=0;
		int					iv=0;
		SWValue::ValueType	vt=SWValue::VtNone;

		switch (option)
			{
			// General socket options
			case SW_SOCKOPT_DEBUG:
				vt = SWValue::VtBoolean;
				level = SOL_SOCKET;
				opt = SO_DEBUG;
				break;

			case SW_SOCKOPT_REUSEADDR:
				vt = SWValue::VtBoolean;
				level = SOL_SOCKET;
				opt = SO_REUSEADDR;
				break;

			case SW_SOCKOPT_KEEPALIVE:
				vt = SWValue::VtBoolean;
				level = SOL_SOCKET;
				opt = SO_KEEPALIVE;
				break;

			case SW_SOCKOPT_DONTROUTE:
				vt = SWValue::VtBoolean;
				level = SOL_SOCKET;
				opt = SO_DONTROUTE;
				break;

			case SW_SOCKOPT_LINGER:
				vt = SWValue::VtBoolean;
				level = SOL_SOCKET;
				opt = SO_LINGER;
				break;

			case SW_SOCKOPT_BROADCAST:
				vt = SWValue::VtBoolean;
				level = SOL_SOCKET;
				opt = SO_BROADCAST;
				break;

			case SW_SOCKOPT_OOBINLINE:
				vt = SWValue::VtBoolean;
				level = SOL_SOCKET;
				opt = SO_OOBINLINE;
				break;

			case SW_SOCKOPT_DGRAM_ERRIND:
#if defined(SO_DGRAM_ERRIND)
				vt = SWValue::VtBoolean;
				level = SOL_SOCKET;
				opt = SO_DGRAM_ERRIND;
#else
				res = SW_STATUS_UNSUPPORTED_OPERATION;
#endif
				break;


			case SW_SOCKOPT_SEND_BUFFER_SIZE:
				vt = SWValue::VtInt32;
				level = SOL_SOCKET;
				opt = SO_SNDBUF;
				break;

			case SW_SOCKOPT_RECEIVE_BUFFER_SIZE:
				vt = SWValue::VtInt32;
				level = SOL_SOCKET;
				opt = SO_RCVBUF;
				break;

			case SW_SOCKOPT_SEND_TIMEOUT:
				vt = SWValue::VtInt32;
				level = SOL_SOCKET;
				opt = SO_SNDTIMEO;
				break;

			case SW_SOCKOPT_RECEIVE_TIMEOUT:
				vt = SWValue::VtInt32;
				level = SOL_SOCKET;
				opt = SO_RCVTIMEO;
				break;




			// TCP-specific socket options
			case SW_SOCKOPT_TCP_NODELAY:
				vt = SWValue::VtBoolean;
				level = IPPROTO_TCP;
				opt = TCP_NODELAY;
				break;

			default:
				// This is an unrecognised option
				res = SW_STATUS_INVALID_PARAMETER;
				break;
			}

		if (res == SW_STATUS_SUCCESS)
			{
			res = getOption(_fd, level, opt, iv);
			}

		if (res == SW_STATUS_SUCCESS)
			{
			if (vt == SWValue::VtBoolean)
				{
				optval.setValue(iv!=0);
				}
			else
				{
				optval.setValue(iv);
				}
			}

		return setLastError(res);
		}


sw_status_t		
SWSocket::accept(SWSocket *&/*pSocket*/, sw_uint64_t /*waitms*/)
		{
		return setLastError(SW_STATUS_ILLEGAL_OPERATION);
		}



sw_status_t		
SWSocket::listen(const SWSocketAddress &/*addr*/, int /*maxPending*/)
		{
		return setLastError(SW_STATUS_ILLEGAL_OPERATION);
		}


sw_status_t
SWSocket::sendTo(const SWSocketAddress & /*addr*/, const void * /*pBuffer*/, sw_size_t /*bytesToWrite*/, sw_size_t * /*pBytesWritten=NULL*/)
		{
		return setLastError(SW_STATUS_ILLEGAL_OPERATION);
		}


sw_status_t
SWSocket::receiveFrom(SWSocketAddress &/*addr*/, void * /*pBuffer*/, sw_size_t /*bytesToRead*/, sw_size_t * /*pBytesRead=NULL*/)
		{
		return setLastError(SW_STATUS_ILLEGAL_OPERATION);
		}



bool			
SWSocket::isOpen() const
		{
		return (_fd >= 0);
		}


bool			
SWSocket::isConnected() const
		{
		return (isOpen() && _flags.getBit(SW_SOCKFLAG_CONNECTED));
		}


bool			
SWSocket::isListening() const
		{
		return (isOpen() && _flags.getBit(SW_SOCKFLAG_LISTENING));
		}




bool			
SWSocket::isBound() const
		{
		return (isOpen() && _flags.getBit(SW_SOCKFLAG_BOUND));
		}


bool			
SWSocket::isStream() const
		{
		return (isOpen() && _flags.getBit(SW_SOCKFLAG_STREAM));
		}



