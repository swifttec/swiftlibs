/**
*** @file		SWStringPtr.cpp
*** @brief		A comprehensive string class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWString class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWStringPtr.h>
#include <swift/SWException.h>
#include <swift/SWString.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




SWStringPtr::SWStringPtr(const void *p, int cs) :
	m_ptr(reinterpret_cast<const char *>(p)),
	m_charsize(cs)
		{
		if (cs != sizeof(char) && cs != sizeof(wchar_t))
			{
			m_ptr = NULL;
			m_charsize = 0;
			throw SW_EXCEPTION(SWException);
			}
		}


SWStringPtr::SWStringPtr(const char *s) :
	m_ptr(s),
	m_charsize(sizeof(*s))
		{
		}


SWStringPtr::SWStringPtr(const wchar_t *s) :
	m_ptr(reinterpret_cast<const char *>(s)),
	m_charsize(sizeof(*s))
		{
		}


SWStringPtr::SWStringPtr(const SWString &s) :
	m_ptr(NULL),
	m_charsize((int)s.charSize())
		{
		switch (m_charsize)
			{
			case 0:
				m_ptr = "";
				m_charsize = sizeof(char);
				break;

			case sizeof(wchar_t):
				m_ptr = reinterpret_cast<const char *>(s.w_str());
				break;

			default:
				m_ptr = s.c_str();
				break;
			}
		}


// Prefix increment operator.
const SWStringPtr &
SWStringPtr::operator++()
		{
		m_ptr += m_charsize;

		return *this;
		}


// Postfix increment operator.
SWStringPtr
SWStringPtr::operator++(int)
		{
		SWStringPtr tmp(*this);

		m_ptr += m_charsize;

		return tmp;
		}


// Prefix decrement operator.
const SWStringPtr &
SWStringPtr::operator--()
		{
		m_ptr -= m_charsize;

		return *this;
		}


// Postfix decrement operator.
SWStringPtr
SWStringPtr::operator--(int)
		{
		SWStringPtr tmp(*this);

		m_ptr -= m_charsize;

		return tmp;
		}

// += operator (int)
SWStringPtr &
SWStringPtr::operator+=(int v)
		{
		m_ptr += m_charsize * v;

		return *this;
		}


// -= operator (int)
SWStringPtr &
SWStringPtr::operator-=(int v)
		{
		m_ptr -= m_charsize * v;

		return *this;
		}


int
SWStringPtr::operator[](int idx) const
		{
		int	c;

		if (m_charsize == sizeof(wchar_t))
			{
			const wchar_t	*s = reinterpret_cast<const wchar_t *>(m_ptr);
			c = s[idx];
			}
		else
			{
			c = m_ptr[idx];
			}

		return c;
		}


int
SWStringPtr::operator*() const
		{
		int	c;

		if (m_charsize == sizeof(wchar_t))
			{
			const wchar_t	*s = reinterpret_cast<const wchar_t *>(m_ptr);
			c = *s;
			}
		else
			{
			c = *m_ptr;
			}

		return c;
		}



