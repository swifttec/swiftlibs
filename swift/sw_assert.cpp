/**
*** @file		sw_assert.cpp
*** @brief		A collection of useful assert functions
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains a collection of useful assert functions.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




#include "stdafx.h"

#include <swift/sw_assert.h>
#include <swift/SWString.h>


#if defined(WIN32) || defined(_WIN32)
	#include <crtdbg.h>
#endif

#include <stdio.h>





SWIFT_DLL_EXPORT void
sw_assert_report(const char *expression, const char *file, int line, sw_uint32_t flags)
		{
		SWString	text;

		text.format("Assertion failed: %s at line %d of %s", expression, line, file);

#if defined(SW_PLATFORM_WINDOWS) && defined(SW_BUILD_DEBUG)
		if ((flags & SW_ASSERT_FLAG_THROW_EXCEPTION) == 0)
			{
			TCHAR	module[1024];

			GetModuleFileName(NULL, module, 1024);
			_CrtDbgReport(_CRT_ASSERT, file, line, SWString(module), text);
			}
#endif

		fprintf(stderr, "%s\n", text.c_str());

		if (flags & SW_ASSERT_FLAG_THROW_EXCEPTION)
			{
			throw SWAssertException(file, line, errno, text);
			}
		else if (flags & SW_ASSERT_FLAG_ABORT)
			{
			abort();
			}
		}





