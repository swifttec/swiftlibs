/**
*** @file		SWTextFile.h
*** @brief		A class to handle the reading and writing of a text file.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWTextFile class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWTextFile_h__
#define __SWTextFile_h__

#include <swift/swift.h>
#include <swift/SWString.h>
#include <swift/SWFile.h>
#include <swift/SWDataBuffer.h>

// Forward declarations
class SWTextFile;
class SWTextFileHandler;


// filemode
#define TF_MODE_READ			0x0000                  // existing file
#define TF_MODE_WRITE			0x0001                  // new file
// #define TF_MODE_APPEND			0x0002                  // append to existing file
#define TF_MODE_MASK			0x0003

#define TF_ENCODING_ANSI		0x0000
#define TF_ENCODING_UTF8		0x0010
#define TF_ENCODING_UTF16_LE	0x0020
#define TF_ENCODING_UTF16_BE	0x0030
#define TF_ENCODING_UNICODE		TF_ENCODING_UTF16_LE
#define TF_ENCODING_UTF16		TF_ENCODING_UTF16_LE
#define TF_ENCODING_MASK		0x00f0

#define TF_LINEEND_LF			0x0000
#define TF_LINEEND_CRLF			0x0100
#define TF_LINEEND_CR			0x0200
#define TF_LINEEND_MASK			0x0300

#define TF_FLAGS_NO_READ_BOM	0x1000
#define TF_FLAGS_NO_WRITE_BOM	0x2000
#define TF_FLAGS_MASK			0xf000




/// Internal class for handling text files
class SWIFT_DLL_EXPORT SWTextFileHandler
		{
public:
		SWTextFileHandler(SWTextFile *pTextFile);
		virtual ~SWTextFileHandler();

		virtual sw_status_t			readChar(int &c)=0;
		virtual sw_status_t			writeBOM()=0;
		virtual sw_status_t			writeChar(int c)=0;

protected:
		SWTextFile	*m_pTextFile;
		};


/**
*** @brief	A class to handle the reading and writing of text files in a consistent
***			manner, whether they be ANSI, UTF-8 or UNICODE. Will also handle the
***			three variants of line ending (Unix=LF, Windows/Dos=CRLF, Mac=CR)
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWTextFile
		{
public:
		/// Default constructor (specifiying number of bits and optional fixed size)
		SWTextFile();

		/// Destructor
		virtual ~SWTextFile();

		/**
		*** @brief	Opens the specified file.
		***
		*** The open method opens the specified file in the manner described by the arguments.
		***
		*** @param	filename	The name of the file to be opened.
		*** @param	mode		The mode in which the file is opened. This is a combination
		***						of the flags specified above.
		*** @param	pfa			A pointer to a file attribute object which is used when a file
		***						is created. If not supplied default file attributes are used.
		***
		*** @return SW_STATUS_SUCCESS if successful or the error code.
		**/
		sw_status_t			open(const SWString &filename, int mode, SWFileAttributes *pfa=0);

		/**
		*** @brief	Closes the file
		***
		*** @return SW_STATUS_SUCCESS if successful or the error code.
		**/
		sw_status_t			close();

		/**
		*** @brief	Flushes buffered output to the file
		***
		*** @return SW_STATUS_SUCCESS if successful or the error code.
		**/
		sw_status_t			flush();

		/**
		*** @brief	Reads a line from the file
		***
		*** @return SW_STATUS_SUCCESS if successful or the error code.
		**/
		sw_status_t			readLine(SWString &line, bool stripnl=true);

		/**
		*** @brief	Reads a character from the file
		***
		*** @return SW_STATUS_SUCCESS if successful or the error code.
		**/
		sw_status_t			readChar(int &c);

		/**
		*** @brief	Writes a string to the file and appends a newline in the correct format
		***
		*** @return SW_STATUS_SUCCESS if successful or the error code.
		**/
		sw_status_t			writeLine(const SWString &line);

		/**
		*** @brief	Writes a string to the file
		***
		*** @return SW_STATUS_SUCCESS if successful or the error code.
		**/
		sw_status_t			writeString(const SWString &s);

		/**
		*** @brief	Writes a character to the file
		***
		*** @return SW_STATUS_SUCCESS if successful or the error code.
		**/
		sw_status_t			writeChar(int c);


		bool				isOpen() const		{ return m_file.isOpen(); }
		bool				isReadMode() const	{ return ((m_flags & TF_MODE_MASK) == TF_MODE_READ); }
		bool				isWriteMode() const	{ return ((m_flags & TF_MODE_MASK) == TF_MODE_WRITE); }
		int					encoding() const	{ return (m_flags & TF_ENCODING_MASK); }

public: // Internal Methods
		int					readByte();
		sw_status_t			readByte(sw_byte_t &b);
		sw_status_t			writeBytes(sw_byte_t *pb, int nbytes);


protected:
		SWFile				m_file;				/// The actual file
		int					m_flags;			/// The file mode (read, write, append)
		SWTextFileHandler	*m_pFileHandler;	/// The file handler - depends on encoding
		SWDataBuffer		m_data;				/// The file data
		sw_size_t			m_datapos;			/// The position in the data buffer to read from
		int					m_savedChar;		/// A single saved char
		bool				m_savedCharFlag;
		SWDataBuffer		m_linedata;			///< A buffer to hold a single line of data
		};




#endif
