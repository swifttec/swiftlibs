/**
*** @file		SWMemoryManager.h
*** @brief		An abstract memory allocator.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWMemoryManager class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWMemoryManager_h__
#define __SWMemoryManager_h__

#include <swift/swift.h>
#include <swift/SWException.h>
#include <swift/SWObject.h>


/**
*** @brief An abstract memory allocator.
***
*** An abstract memory allocator.
***
*** @author		Simon Sparkes
*/
class SWIFT_DLL_EXPORT SWMemoryManager : public SWObject
		{
public:
		/// Default constructor
		SWMemoryManager();

		/// Virtual destructor
		virtual ~SWMemoryManager();

		/**
		*** @brief Allocate a memory block (malloc).
		***
		*** Allocates the specified number of bytes of memory from the memory pool
		*** used by the allocator. If the number of bytes requested is zero or no
		*** more memory is available for allocation this function will return a NULL
		*** pointer.
		***
		*** @param[in]	nbytes	The number of bytes required.
		*** @return				A pointer to the memory buffer or NULL if no more memory
		***						can be allocated by the allocator.
		**/
		virtual void *	malloc(sw_size_t nbytes)=0;

		/**
		*** @brief Resize an allocated memory block (realloc).
		***
		*** Changes the size of the data block pointed to by the specified pointer
		*** to be able to store the specified amount of data. If the data block
		*** is a NULL pointer this method allocates a new buffer of the size specified
		*** as if the malloc method had been called.
		***
		*** If the number of bytes requested is zero and the old data pointer is not NULL
		*** the existing data block will be freed (as if the free method had been called)
		*** and a NULL pointer returned.
		***
		*** If no memory is available for allocation this function will return a NULL
		*** pointer but will leave the existing data block intact.
		***
		*** <b>Notes:</b>
		*** - The memory pointer returned may not be the same as the one passed in.
		*** - If the old data passed in was not allocated by this allocator then an exception
		***   will (should) be thrown.
		***
		*** @param[in]	pOldData	The pointer the data block to be resized.
		*** @param[in]	nbytes		The number of bytes required.
		*** @return				A pointer to the memory buffer or NULL if no more memory
		***						can be allocated by the allocator.
		**/
		virtual void *	realloc(void *pOldData, sw_size_t nbytes)=0;

		/**
		*** @brief	Release memory back to the allocator (free).
		***
		*** This method releases the memory block specified back to the allocator.
		*** Once released the memory block should not be re-used.
		***
		*** <b>Notes:</b>
		*** - If the old data passed in was not allocated by this allocator then an exception will (should) be thrown.
		*** - Unlike the standard C function free it is legal to free a NULL pointer. This will do nothing.
		***
		*** @param[in]	pData	The pointer the data block to be released.
		**/
		virtual void	free(void *pData)=0;

		/**
		*** @brief	Allocate and intialise a memory block.
		***
		*** This convienience method allocates a memory block using malloc and
		*** then uses memset or an equivalent function to initialise the memory
		*** block to contain zeroes.
		***
		*** <code>
		*** void	*p;
		***
		*** p = zalloc(100);
		*** </code>
		***
		*** would be the same as writing:
		***
		*** <code>
		*** void	*p;
		***
		*** p = malloc(100);
		*** memset(p, 0, 100);
		*** </code>
		***
		*** @param[in]	nbytes	The number of bytes of memory to be allocate.
		**/
		virtual void *	zalloc(sw_size_t nbytes);


		/**
		*** @brief	Allocate a memory block.
		***
		*** This convienience method allocates a memory block using zalloc which
		*** is large enough to contain the specified number of elements (nelem) of
		*** the specified size. The memory is intialised to zero.
		***
		*** <code>
		*** int	*p;
		***
		*** p = (int *)calloc(100, sizeof(int));
		*** </code>
		***
		*** would be the same as writing:
		***
		*** <code>
		*** int	*p;
		***
		*** p = (int *)zalloc(100 * sizeof(int));
		*** </code>
		***
		*** @param[in]	nelem		The number of elements of the specified size to allocate.
		*** @param[in]	elemsize	The size of each element to be allocated.
		**/
		virtual void *	calloc(sw_size_t nelem, sw_size_t elemsize);


public: // static methods

		/// Return the default memory manager
		static SWMemoryManager *	getDefaultMemoryManager();
		};

/// A memory manager general exception
SW_DEFINE_EXCEPTION(SWMemoryManagerException, SWMemoryException);

#endif
