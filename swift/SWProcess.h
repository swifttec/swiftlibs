/**
*** @file		SWProcess.h
*** @brief		A class to encapsulate a process.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWProcess class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWProcess_h__
#define __SWProcess_h__


#include <swift/SWObject.h>
#include <swift/SWException.h>
#include <swift/SWStringArray.h>
#include <swift/SWAppConfig.h>


/**
*** @brief	Generic process object.
***
*** This object facilitates the handling of processes in 2 ways. Firstly it provides
*** a number of static methods for handling process creation, etc.
***
*** Secondly it supports the concept of an "attached" process. An attached process is
*** one that an instance of a SWProcess object is responsible for. When a SWProcess
*** object is attached to a process and the destructor is called the SWProcess object
*** will wait until the attached process has terminated before existing.
***
*** <b>Note that the concept of fork and exec is not currently supported as it is not
*** portable accross all platforms.</b>
***
*** @author Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWProcess : public SWObject
		{
public:
		/// Default constructor
		SWProcess();

		/// Destructor
		virtual ~SWProcess();

		/**
		*** Create a procress based on the command line specified.
		***
		*** @param	cmdline		The command line to parse to create the executable name and parameters
		***						to pass to it.
		***
		*** @return						A status code.
		*** @retval	SW_STATUS_SUCCESS	If the process was successfully created.
		*** @retval	"An error code"		If the process creation failed.
		**/
		sw_status_t			create(const SWString &cmdline);

		/**
		*** Create a procress based on the program name and arguments specified.
		***
		*** @param	program		The program name to execute. If the program cannot be found
		***						directly it is searched for in the path.
		*** @param	args		An array of command line arguments to pass to the program.
		***
		*** @return	SW_STATUS_SUCCESS if successful or the error code otherwise.
		**/
		sw_status_t			create(const SWString &program, const SWStringArray &args);

		/// Kill the process by sending it the specified signal
		sw_status_t			kill(int sig);

		/// Forcefully terminate the process (may bypass cleanup and therefore leave resources open)
		sw_status_t			terminate();

		/// Get the exit status of the process.
		int					getExitCode() const		{ return m_exitcode; }

		/// Get the process ID for this process.
		sw_pid_t			getProcessId() const	{ return m_pid; }

		/// Test to see if the process is running.
		bool				isRunning() const;

		/// Wait for this process to stop executing
		sw_status_t			waitUntilStopped(sw_uint64_t waitms=SW_TIMEOUT_INFINITE);

		/**
		*** @brief	Attach the process with the specified PID to this object.
		***
		*** This method associates the process with the specified pid to this object.
		***
		*** @param[in]	pid		The ID of the process to associate with the object.
		***
		*** @return	SW_STATUS_SUCCESS if successful or the error code otherwise.
		**/
		sw_status_t			attach(sw_pid_t pid);

		/**
		*** @brief	Detach this process object from the running process.
		***
		*** This method disassociates the process from this object. Any internal
		*** information about the currently represented process (ID, handle, etc)
		*** is discarded.
		***
		*** @return	SW_STATUS_SUCCESS if successful or the error code otherwise.
		**/
		sw_status_t			detach();

public: // Static methods

		/// Return the ID of the currently running process.
		static sw_pid_t			self();

		/**
		*** Create a procress based on the command line specified.
		***
		*** @param	cmdline		The command line to parse to create the executable name and parameters
		***						to pass to it.
		*** @param	pid			An sw_pid_t value to receive the ID of the process started.
		***
		*** @return	SW_STATUS_SUCCESS if successful or the error code otherwise.
		**/
		static sw_status_t		create(const SWString &cmdline, sw_pid_t &pid);

		/**
		*** Create a procress based on the program name and arguments specified.
		***
		*** @param	program		The program name to execute. If the program cannot be found
		***						directly it is searched for in the path.
		*** @param	args		An array of command line arguments to pass to the program.
		*** @param	pid			Received the ID of the created process.
		***
		*** @return	SW_STATUS_SUCCESS if successful or the error code otherwise.
		**/
		static sw_status_t		create(const SWString &program, const SWStringArray &args, sw_pid_t &pid);

		/// Check to see if the specified process is running.
		static bool				isRunning(sw_pid_t pid);

		/// Check to see if the specified process is running.
		static SWString			getName(sw_pid_t pid);

		/// Terminate the process with the specified process ID.
		static sw_status_t		terminate(sw_pid_t pid);

		/// Kill the process with the specified process ID.
		static sw_status_t		kill(sw_pid_t pid, int sig);

		/**
		*** Wait for the process with the specified process ID to exit.
		***
		*** @param	pid			The ID of the process to wait for. If this has a value of
		***						zero then the first process to exit is captured and it's
		***						exitcode is returned.
		*** @param	waitms		Specifies the time period (in milliseconds) to wait for a
		***						a process to die. Can be specified as SW_TIMEOUT_INFINITE.
		*** @param	exitcode	Received the exit code of the exiting process.
		**/
		static sw_status_t		waitForProcess(sw_pid_t pid, sw_uint64_t waitms, int &exitcode);

		static SWString			getProgramPath(const SWString &program);
		static SWString			getProgramPath(const SWString &program, const SWAppConfig &appconfig);
		static SWString			getProgramPath(const SWString &program, const SWStringArray &pathlist, bool searchPathListFirst=true);

private:
		sw_handle_t		m_hProcess;		///< The handle to the process.
		sw_pid_t		m_pid;			///< The process ID of the process.
		int				m_exitcode;		///< The exit code of the process.
		};


// Exceptions
SW_DEFINE_EXCEPTION(SWProcessException, SWException);					///< A generic process exception




#endif
