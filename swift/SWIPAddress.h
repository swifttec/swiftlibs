/**
*** @file		SWIPAddress.h
*** @brief		A class to represent an IP address.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWIPAddress class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWIPAddress_h__
#define __SWIPAddress_h__

#include <swift/swift.h>
#include <swift/SWString.h>
#include <swift/sw_net.h>

#if defined(SW_PLATFORM_WINDOWS)
	#include <inaddr.h>
#else
	#include <netinet/in.h>
#endif



// Forward declarations

/**
*** @brief	A class to represent an IP address
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Safe when synchronized</td>
*** </tr>
*** </table>
***
***
*** @author		Simon Sparkes
*** @todo		Investigate deriving this from a SWSocketAddress object
**/
class SWIFT_DLL_EXPORT SWIPAddress : public SWObject
		{
friend class SWHostname;

public:
		/// A enumeration of the possible address types
		enum IPAddressType
			{
			Unknown=0,
			IP4,
			IP6
			};

public:
		// Default constructor
		SWIPAddress();

		// Convert from a string
		SWIPAddress(const SWString &addr);

		// Convert from an in_addr_t
		SWIPAddress(sw_uint32_t addr);

		// Convert from an struct in_addr
		//SWIPAddress(const struct in_addr &addr);

		// Copy constructor
		SWIPAddress(const SWIPAddress &other);

		// Assignment operators
		SWIPAddress &	operator=(sw_uint32_t addr);
		//SWIPAddress &	operator=(const struct in_addr &addr);
		SWIPAddress &	operator=(const SWIPAddress &other);
		SWIPAddress &	operator=(const SWString &other);

		// Convert to a string
		virtual SWString	toString() const;

		// Convert to a host int
		virtual sw_uint32_t	toHostInteger() const;

		/// @brief	Equality operator
		bool			operator==(const SWIPAddress &other);

		/// @brief	 Cast to in_addr_t
		operator sw_uint32_t() const		{ return *(sw_uint32_t *)_data; }
		operator struct in_addr &() const	{ return *(struct in_addr *)_data; }

		virtual void	clear();

		/**
		*** @brief	Return the length of the IP address
		**/
		int				length() const		{ return _len; }

		/**
		*** @brief	Return the family associated with this IP address.
		**/
		int				family() const;

		/**
		*** @brief	Return a pointer to the internal data for passing into network functions
		**/
		const char *	data() const		{ return _data; }

public: // Static methods
		static bool		isIPAddress(const SWString &s);

protected:
		SWIPAddress(const void *data, int len, int family);

private:
		/// Internal method to set the address data
		void	set(IPAddressType type, int len, const void *data);

private:
		IPAddressType	_type;		///< The address type
		int				_len;		///< The length of the data
		char			_data[16];	///< The address data
		};



#endif
