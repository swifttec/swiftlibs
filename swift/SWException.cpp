/**
*** @file		SWException.cpp
*** @brief		The standard exception class for all Exceptions
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWException class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWCString.h>
#include <swift/SWException.h>




/*
** Common initialisation
*/
void
SWException::init(const char *name, const char *file, int line, sw_status_t status, const char *text)
		{
		m_line = line;
		m_status = status;
		m_outstr = 0;

		if (name) m_name = dupstr(name);
		else m_name = 0;

		if (file) m_file = dupstr(file);
		else m_file = 0;

		if (text) m_text = dupstr(text);
		else m_text = 0;
		}


/*
** Duplicate a string using new
*/
char *
SWException::dupstr(const char *str)
		{
		sw_size_t	len=strlen(str)+1;
		char		*sp;

		sp = new char[len];
		if (sp != NULL)
			{
			memcpy(sp, str, len);
			}

		return sp;
		}


SWException::~SWException()
		{
		delete [] m_name;
		delete [] m_file;
		delete [] m_outstr;
		}


SWException::SWException(const char *name, const char *file, int line, sw_status_t status, const char *text)
		{
		init(name, file, line, status, text);
		}



SWException::SWException(const char *file, int line, sw_status_t status, const char *text)
		{
		init("General Exception", file, line, status, text);
		}


SWException::SWException(const SWException &e)
		{
		init(0, 0, 0, 0, 0);
		*this = e;
		}


SWException &	
SWException::operator=(const SWException &e)
		{
		delete [] m_name;
		delete [] m_file;
		delete [] m_outstr;

		m_name = dupstr(e.m_name);
		m_file = dupstr(e.m_file);
		m_line = e.m_line;
		m_status = e.m_status;
		m_text = dupstr(e.m_text);
		m_outstr = dupstr(e.m_outstr);

		return *this;
		}


/**
*** Convert the exception to a formatted string
***
*** Format codes are:
*** %n	exception name
*** %f	filename where exception generated (if known)
*** %l	lineno in file where exception generated (if known)
*** %e	errno at time of exception
**/
const char *
SWException::toFormattedString(const char *fmt)
		{
		SWCString	outstr;
		SWCString	formatString(fmt);
		int			c, p=0, q;

		while ((q = formatString.indexOf('%', p)) >= 0)
			{
			// If we have any chars copy them into outstr
			if (q > p) outstr += formatString.substring(p, q-1);

			// Now do the substitution on the %
			q++;
			if (q >= (int)formatString.length()) break;

			c = formatString.getCharAt(q++);
			switch (c)
				{
				case '%':	// same as %
					outstr += "%";
					break;

				case 'n':	// Name
					outstr += m_name;
					break;

				case 'f':	// File
					outstr += m_file;
					break;

				case 'l':	// Line
					outstr += SWCString::valueOf(m_line);
					break;

				case 'e':	// Errno
					outstr += SWCString::valueOf((sw_uint32_t)m_status);
					break;

				case 't':	// text
					outstr += m_text;
					break;

				case 'T':	// optional text preceeded by " - "
					if (*m_text)
						{
						outstr += " - ";
						outstr += m_text;
						}
					break;

				default:
					// Don't know so keep it!
					outstr += "%";
					outstr += c;
					break;
				}

			p = q;
			}

		// Add any remaining chars
		outstr += formatString.substring(p);

		delete [] m_outstr;
		m_outstr = strnew((const char *)outstr);

		return m_outstr;
		}


const char *
SWException::toString() const
		{
		if (m_outstr == NULL)
			{
			sw_size_t	buflen;

			// Calculate the buffer size needed allow 8 bytes for each integer
			buflen = strlen(m_name) + strlen(m_file) + strlen(m_text) + 64;
			m_outstr = new char[buflen];
#if defined(SW_PLATFORM_WINDOWS)
			sprintf_s(m_outstr, buflen, "%s thrown at line %d of %s (status=%d)", m_name, m_line, m_file, m_status);
#else
			sprintf(m_outstr, "%s thrown at line %d of %s (status=%d)", m_name, m_line, m_file, m_status);
#endif
			if (*m_text)
				{
				strcat(m_outstr, " - ");
				strcat(m_outstr, m_text);
				}
			}

		return m_outstr;
		}



