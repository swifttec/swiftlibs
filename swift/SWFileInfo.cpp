/**
*** @file		SWFileInfo.cpp
*** @brief		A class representing a single directory entry
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWFileInfo class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <swift/SWFilename.h>
#include <swift/SWFileInfo.h>

#if defined(SW_PLATFORM_WINDOWS)
	#include <winioctl.h>
	#include <io.h>
#else
	#include <unistd.h>

	#if defined(SW_PLATFORM_LINUX) || defined (SW_PLATFORM_MACH)
		#include <utime.h>
		#include <sys/time.h>
	#endif
#endif

#include <sys/stat.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


SWFileInfo::SWFileInfo(const SWString &filename)
		{
		reset();
		if (!filename.isEmpty()) getInfoForFile(filename);
		}


// Copy constructor
SWFileInfo::SWFileInfo(const SWFileInfo &v) :
	SWObject()
		{
		*this = v;
		}


// Assignment operator
const SWFileInfo &
SWFileInfo::operator=(const SWFileInfo &v)
		{
#define COPY(x)	x = v.x
		COPY(_name);
		COPY(_dosname);
		COPY(_directory);

		COPY(_attributes);
		COPY(_size);
		COPY(_inode);
		COPY(_devno);
		COPY(_dirdevno);
		COPY(_linkcount);
		COPY(_createTime);
		COPY(_lastAccessTime);
		COPY(_lastModificationTime);
		COPY(_lastStatusChangeTime);
		COPY(_preferredIoBlockSize);
		COPY(_blocks);
		COPY(_sizeOnDisk);

		COPY(_validData);
#undef COPY

		return *this;
		}

// PRIVATE:
// Reset all attributes
void
SWFileInfo::reset()
		{
#define ZERO(x) x = 0

		_name = _T("");
		_dosname = _T("");
		_directory = _T("");
		_attributes.clear();

		ZERO(_size);
		ZERO(_inode);
		ZERO(_devno);
		ZERO(_dirdevno);
		ZERO(_linkcount);
		ZERO(_preferredIoBlockSize);
		ZERO(_blocks);
		ZERO(_validData);
		ZERO(_sizeOnDisk);

		_createTime = 0.0;
		_lastAccessTime = 0.0;
		_lastModificationTime = 0.0;
		_lastStatusChangeTime = 0.0;
#undef ZERO
		}


/**
*** Ensure the file info is valid.
***
*** This method is not required for the SWFileInfo class, but provided for 
*** derived classes like SWDirectoryEntry which postpone reading file attributes
*** to improve performance.
**/
void
SWFileInfo::validate(ValidateType type)
		{
		if ((_validData & (int)type) == 0)
			{
			// We don't yet have the data we need - so get it
			if ((type & FileAttributes) != 0 && ((_validData & FileAttributes) == 0)) getAttributeInfo(getFullPathname());
			if ((type & FileTime) != 0 && ((_validData & FileTime) == 0)) getTimeInfo(getFullPathname());
			if ((type & FileSize) != 0 && ((_validData & FileSize) == 0)) getSizeInfo(getFullPathname());
			if ((type & FileName) != 0 && ((_validData & FileName) == 0)) getNameInfo(getFullPathname());
			if ((type & FileBlocks) != 0 && ((_validData & FileBlocks) == 0)) getBlockInfo(getFullPathname());
			}
		}



// Refresh the info for this object
void
SWFileInfo::refresh()
		{
		_validData = 0;
		validate(FileAll);
		}


/*
** Get the mode as an ls-like string
** This is a 10 or 11 character string
**
** First char
** d     the entry is a directory;
** D     the entry is a door;
** l     the entry is a symbolic link;
** b     the entry is a block special file;
** c     the entry is a character special file;
** p     the entry is a fifo (or "named pipe") special file;
** s     the entry is an AF_UNIX address family socket;
** -     the entry is an ordinary file;
**
** Next 3 groups of 3 are Read/Write/Execute permissions for
** Owner, Group, and World respectivly
**
** Finally a plus character is appended if there is an acl
** associated with the entry
*/
SWString
SWFileInfo::getUnixModeString()
		{
		SWString	s;

		validate(FileAttributes);
		if (isValid()) s = _attributes.getUnixModeString();

		return s;
		}


SWString
SWFileInfo::getAttributesString()
		{
		SWString	s;

		validate(FileAttributes);
		if (isValid()) s = _attributes.getAttributesString();
		
		return s;
		}


/*
** Read the value of the symbolic link (if we are one)
*/
SWString
SWFileInfo::getLinkValue()
		{
		SWString	result;

		if (isSymbolicLink())
			{
#if defined(SW_PLATFORM_WINDOWS)
			/*******************************************************************************************
			HANDLE	hFile=0; 
			
			hFile = CreateFile(getFullPathname(), GENERIC_READ, FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_FLAG_OPEN_REPARSE_POINT, NULL);
			if (hFile != INVALID_HANDLE_VALUE)
				{
				char						buf[MAXIMUM_REPARSE_DATA_BUFFER_SIZE];
				DWORD						sizebuf=sizeof(buf);

				if (DeviceIoControl(hFile, FSCTL_GET_REPARSE_POINT, NULL, 0, buf, &sizebuf, NULL))
					{
					REPARSE_GUID_DATA_BUFFER 	*rp=(REPARSE_GUID_DATA_BUFFER  *)buf;

					if (rp->ReparseTag == IO_REPARSE_TAG_MOUNT_POINT)
						{
						}
					}

				CloseHandle(hFile);
				}
			*******************************************************************************************/		
#else
			char	tmp[1024];
			int		nbytes;


			if ((nbytes = ::readlink(getFullPathname(), tmp, sizeof(tmp))) > 0) 
				{
				tmp[nbytes] = 0;
				result = tmp;
				}
#endif
			}
		
		return result;
		}


SWString
SWFileInfo::getFullPathname() const
		{
		SWString	path;

		if (_name == _T("/") || (_name.length() == 3 && _name[1] == ':' && (_name[2] == '/' || _name[2] == '\\')))
			{
			path = _name;
			}
		else
			{
			path = SWFilename::concatPaths(_directory, _name);
			}

		return path;
		}


SWString
SWFileInfo::getDosFullPathname() const
		{
		SWString	path;

		if (_dosname == _T("/") || (_dosname.length() == 3 && _dosname[1] == ':' && (_dosname[2] == '/' || _dosname[2] == '\\')))
			{
			path = _dosname;
			}
		else
			{
			path = SWFilename::concatPaths(_dosdir, _dosname);
			}

		return path;
		}


#if defined(SW_USE_UNIX_FILES)
	#if defined(SW_USE_UNIX_64BIT_FILES)
		// These O/S have the stat64 and lstat64 interface
		typedef struct stat64 sw_stat_t;
		#define sw_stat		::stat64
		#define sw_lstat	::lstat64
	#else
		// These O/S have the stat and lstat4 interface
		typedef struct stat sw_stat_t;
		#define sw_stat		::stat
		#define sw_lstat	::lstat
	#endif
#elif defined(WIN32) || defined(_WIN32)
	// These O/S have the stat64 and lstat64 interface
	typedef struct _stati64 sw_stat_t;
	#define sw_stat		::_tstati64
	#define sw_lstat	::_tstati64
#endif


bool
SWFileInfo::getAttributeInfo(const SWString &filename)
		{
		bool		ok=false;
		sw_uint32_t	unixmode=0, winattrs=0, symlink=0;
		sw_uint32_t	uid=0, gid=0;
		sw_stat_t	st;

		// First get the info on the file
		if (sw_lstat(SWString(filename), &st) >= 0)
			{
#ifdef S_IFLNK
			if ((st.st_mode & S_IFMT) == S_IFLNK)
				{
				sw_stat_t	st2;

				if (sw_stat(filename, &st2) >= 0)
					{
					// This is a symbolic link, BUT we want the info on the file it points to!
					// as we regard a symlink as an attribute not a file type
					symlink = 1;
					st = st2;
					}
				else
					{
					// this is a symbolic link, BUT it points to an invalid file
					// so we return the information on the link itself.
					symlink = 0;
					}
				}
#endif
			unixmode = st.st_mode;

			uid = st.st_uid;
			gid = st.st_gid;

			_size = st.st_size;
			_inode = st.st_ino;
			_devno = st.st_rdev;
			_dirdevno = st.st_dev;
			_linkcount = st.st_nlink;
			_lastAccessTime = st.st_atime;
			_lastModificationTime = st.st_mtime;

			// The mtime and ctime values have subtle differences on windows
#if defined(_WIN32) || defined(WIN32)
			_lastStatusChangeTime = st.st_mtime;
			_createTime = st.st_ctime;
#else
			_lastStatusChangeTime = st.st_ctime;
			_createTime = st.st_mtime;
#endif


#if !defined(_WIN32) && !defined(WIN32)
			_preferredIoBlockSize = st.st_blksize;
			_blocks = st.st_blocks;
			_sizeOnDisk = _blocks * 512;
			_validData |= (int)(FileBlocks);
#endif

			ok = true;
			}

		if (ok) _validData |= (int)(FileSize | FileTime);

#if defined(_WIN32) || defined(WIN32)
		if (ok)
			{
			winattrs = ::GetFileAttributes(filename);
			ok = (winattrs != 0);
			}
#endif

		if (ok)
			{
			_attributes.set(filename, unixmode, winattrs, symlink);
			_validData |= FileAttributes;
			}

		return ok;
		}


bool
SWFileInfo::getTimeInfo(const SWString &filename)
		{
		bool	ok=false;

#if defined(_WIN32) || defined(WIN32)
		// use getTimes for improved accuracy over stat.
		getFileTimes(filename, &_lastModificationTime, &_createTime, &_lastAccessTime); 
		_lastStatusChangeTime = _lastModificationTime;
		ok = true;
#else
		SW_UNREFERENCED_PARAMETER(filename);
		ok = true;
#endif

		if (ok) _validData |= FileTime;

		return ok;
		}


bool
SWFileInfo::getSizeInfo(const SWString &filename)
		{
		bool	ok=false;

		// Currently we take the file size from the attribute info.
		if ((_validData & FileAttributes) == 0) ok = getAttributeInfo(filename);
		else ok = true;

		if (ok) _validData |= (int)FileSize;

		return ok;
		}



bool
SWFileInfo::getBlockInfo(const SWString &filename)
		{
		bool	ok=false;

		// Currently we take the file size from the attribute info.
		if ((_validData & FileBlocks) == 0)
			{
#if defined(_WIN32) || defined(WIN32)
			// Unix params simulated for dos/windows
			DWORD   l, h;

			_preferredIoBlockSize = 8192;
			l = ::GetCompressedFileSize(filename, &h);
			_sizeOnDisk = ((sw_uint64_t)h << 32) | (sw_uint64_t)l;
			_blocks = (int)((_sizeOnDisk + 511) / 512);
#else
			if ((_validData & FileAttributes) == 0) ok = getAttributeInfo(filename);
			else ok = true;

#endif
			}
		else ok = true;

		if (ok) _validData |= (int)FileBlocks;

		return ok;
		}



bool
SWFileInfo::getNameInfo(const SWString &filename)
		{
		bool	ok=false;

#if defined(_WIN32) || defined(WIN32)
		SWString	tmpname;
		DWORD		r, n;

		n = (DWORD)filename.length()+1;
		for (;;)
			{
			r = GetShortPathName(filename, (sw_tchar_t *)tmpname.lockBuffer(n, sizeof(sw_tchar_t)), n);
			tmpname.unlockBuffer();
			if (r == 0)
				{
				_dosname = SWFilename::basename(filename);
				_dosdir = SWFilename::dirname(filename);
				break;
				}
			else if (r <= n)
				{
				_dosname = SWFilename::basename(tmpname);
				_dosdir = SWFilename::dirname(tmpname);
				break;
				}
			else
				{
				n = r;
				}
			}
		
		n = (DWORD)filename.length()+1;
		for (;;)
			{
			r = GetLongPathName(filename, (sw_tchar_t *)tmpname.lockBuffer(n, sizeof(sw_tchar_t)), n);
			tmpname.unlockBuffer();
			if (r == 0)
				{
				_name = SWFilename::basename(filename);
				_directory = SWFilename::dirname(filename);
				break;
				}
			else if (r <= n)
				{
				_name = SWFilename::basename(tmpname);
				_directory = SWFilename::dirname(tmpname);
				break;
				}
			else
				{
				n = r;
				}
			}
		
		ok = true;
#else
		_dosname = _name = SWFilename::basename(filename);
		_dosdir = _directory = SWFilename::dirname(filename);
		ok = true;
#endif

		if (ok) _validData |= (int)FileName;

		return ok;
		}


sw_status_t
SWFileInfo::getInfoForFile(const SWString &filename)
		{
		bool		ok=true;

		reset();

		if (ok && (_validData & FileAttributes) == 0) ok = getAttributeInfo(filename);
		if (ok && (_validData & FileTime) == 0) ok = getTimeInfo(filename);
		if (ok && (_validData & FileSize) == 0) ok = getSizeInfo(filename);
		if (ok && (_validData & FileName) == 0) ok = getNameInfo(filename);
		if (ok && (_validData & FileBlocks) == 0) ok = getBlockInfo(filename);

#if defined(WIN32) || defined(_WIN32)
		return (ok && isValid()?SW_STATUS_SUCCESS:GetLastError());
#else
		return (ok && isValid()?SW_STATUS_SUCCESS:errno);
#endif
		}




// The following file types we take from the attributes in preference to the mode.
// because under unix we derive the attributes from the mode and windows gives us
// the attributes directly. Therefore we are less likely to make a system call
// to get the information



bool
SWFileInfo::hasAttributes(sw_fileattr_t wanted, bool wantAny)
		{
		validate(FileAttributes);
		return (isValid() && _attributes.hasAttributes(wanted, wantAny));
		}


/*
bool
SWFileInfo::isFile()
		{
		return (isValid() && !hasAttributes(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_DEVICE|FILE_ATTRIBUTE_REPARSE_POINT));
		}
*/



// Some useful shortcuts
bool
SWFileInfo::isValid()
		{
		return ((_validData & FileAttributes) != 0);
		}




/**
*** Returns a list of allocated ranges in the file.
***
*** If the file is empty no ranges will be returned. If the file is not empty and not sparse
*** or alloacted ranges are not supported by the underlying O/S a single range for the entire file
*** will be returned. Otherwise a number of ranges are returned.
***
*** @return If the file cannot be opened this method will be return false othewise it will be return true
sw_status_t
SWFileInfo::getAllocatedRanges(SWObjectArray &oa)
		{
		sw_status_t		res=SW_STATUS_SUCCESS;
		sw_uint64_t		len=getSize();

		if (len > 0)
			{
#if defined(WIN32) || defined(_WIN32)
	#if _WIN32_WINNT >= 0x0500
			HANDLE	fh;

			DWORD	dwDesiredAccess=GENERIC_READ;
			DWORD	dwShareMode=FILE_SHARE_READ;
			DWORD	dwCreationDisposition=OPEN_EXISTING;
			DWORD	dwFlagsAndAttributes=FILE_ATTRIBUTE_NORMAL;

			fh = CreateFile(getFullPathname(), dwDesiredAccess, dwShareMode, NULL, dwCreationDisposition, dwFlagsAndAttributes, NULL);
			if (fh != INVALID_HANDLE_VALUE)
				{
				FILE_ALLOCATED_RANGE_BUFFER		*pRange, inbuf;
				FILE_ALLOCATED_RANGE_BUFFER		outbuf[32];
				DWORD							dw;
				BOOL							b;

				inbuf.FileOffset.QuadPart = 0;
				inbuf.Length.QuadPart = len;

				for (;;)
					{
					dw = 0;
					b = DeviceIoControl(fh, FSCTL_QUERY_ALLOCATED_RANGES, &inbuf, sizeof(inbuf), outbuf, sizeof(outbuf), &dw, NULL);
					if (b || GetLastError() == ERROR_MORE_DATA)
						{
						pRange = outbuf;
						while (dw > 0)
							{
							oa.add(new SWFileRange(pRange->FileOffset.QuadPart, pRange->Length.QuadPart));
							inbuf.FileOffset.QuadPart = pRange->FileOffset.QuadPart + pRange->Length.QuadPart;
							pRange++;
							dw -= sizeof(FILE_ALLOCATED_RANGE_BUFFER);
							}

						inbuf.Length.QuadPart = len - inbuf.FileOffset.QuadPart;

						if (b)
							{
							res = true;
							break;
							}
						}
					else break;
					}

				CloseHandle(fh);
				res = SW_STATUS_SUCCESS;
				}
			else res = ::GetLastError();
	#else // _WIN32_WINNT >= 0x0500
			// Not supported in earlier versions but this is a runtime error
			// not a compile-time error.
			res = ERROR_NOT_SUPPORTED;
			SetLastError(res);
	#endif // _WIN32_WINNT >= 0x0500
#else
			// For now assume only one range
			oa.add(new SWFileRange(0, len));
			res = SW_STATUS_SUCCESS;
#endif
			}

		return res;
		}
**/


/// Returns true if the specified filename is a file.
bool
SWFileInfo::isFile(const SWString &filename)
		{
		return SWFileInfo(filename).isFile();
		}


/// Returns true if the specified filename is a folder/directory.
bool
SWFileInfo::isDirectory(const SWString &filename)
		{
		return SWFileInfo(filename).isDirectory();
		}


/// Returns true if the specified filename is a folder/directory.
bool
SWFileInfo::isFolder(const SWString &filename)
		{
		return SWFileInfo(filename).isDirectory();
		}


/// Returns true if the specified filename is a symbolic link.
bool
SWFileInfo::isSymbolicLink(const SWString &filename)
		{
		return SWFileInfo(filename).isSymbolicLink();
		}


#if defined(SW_PLATFORM_WINDOWS)
	#define access	_taccess
#endif

/// Returns true if the specified filename is readable.
bool
SWFileInfo::isReadable(const SWString &filename)
		{
		return (access(SWString(filename), 4)==0);
		}


/// Returns true if the specified filename is writeable.
bool
SWFileInfo::isWriteable(const SWString &filename)
		{
		return (access(SWString(filename), 2)==0);
		}


/// Returns true if the specified filename is executeable.
bool
SWFileInfo::isExecutable(const SWString &filename)	
		{
		bool	res;

#if defined(SW_PLATFORM_WINDOWS)
		res = (isFile(filename) && access(SWString(filename), 4)==0 && filename.endsWith(".exe", SWString::ignoreCase));
#else
		res = (isFile(filename) && access(SWString(filename), 1)==0);
#endif

		return res;
		}


sw_filesize_t
SWFileInfo::size(const SWString &filename)
		{
		return SWFileInfo(filename).getSize();
		}



sw_status_t
SWFileInfo::getFileTimes(const SWString &path, SWTime *lastModTime, SWTime *createTime, SWTime *lastAccessTime)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

	#if defined(WIN32) || defined(_WIN32)
		HANDLE		fh = CreateFile(path, GENERIC_READ, FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		FILETIME	*ct=NULL, *at=NULL, *wt=NULL;

		if (lastModTime != NULL) wt = new FILETIME;
		if (createTime != NULL) ct = new FILETIME;
		if (lastAccessTime != NULL) at = new FILETIME;

		if (fh)
			{
			if (!GetFileTime(fh, ct, at, wt)) res = GetLastError();
			CloseHandle(fh);
			}
		else res = GetLastError();

		if (res == SW_STATUS_SUCCESS)
			{
			if (lastModTime != NULL) *lastModTime = *wt;
			if (createTime != NULL) *createTime = *ct;
			if (lastAccessTime != NULL) *lastAccessTime = *at;
			}

		delete at;
		delete wt;
		delete ct;
	#else
		// Use stat to get the file times
		sw_stat_t sb;
		if (sw_stat(path, &sb) == 0)
			{
			if (lastModTime != NULL) *lastModTime = sb.st_mtime;
			if (createTime != NULL) *createTime = sb.st_mtime;	    // There is no create time in unix so pretend
			if (lastAccessTime != NULL) *lastAccessTime = sb.st_atime;
			}
		else res = errno;
	#endif

		return res;
		}


// Sets the various file times for the specified file.
sw_status_t
SWFileInfo::setFileTimes(const SWString &path, SWTime *lastModTime, SWTime *createTime, SWTime *lastAccessTime)
		{
		sw_status_t	    res=SW_STATUS_SUCCESS;

	#if defined(WIN32) || defined(_WIN32)
		HANDLE		fh;
		FILETIME	*ct=NULL, *at=NULL, *wt=NULL;

		fh = CreateFile(path, GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

		if (fh == INVALID_HANDLE_VALUE && isDirectory(path))

			{
			fh = CreateFile(path, GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL|FILE_FLAG_BACKUP_SEMANTICS, NULL);

			}

		if (fh == INVALID_HANDLE_VALUE) res =  GetLastError();
		else
			{
			if (lastModTime != NULL)
				{
				wt = new FILETIME;
				lastModTime->toFILETIME(*wt);
				}

			if (createTime != NULL)
				{
				ct = new FILETIME;
				createTime->toFILETIME(*ct);
				}

			if (lastAccessTime != NULL)
				{
				at = new FILETIME;
				lastAccessTime->toFILETIME(*at);
				}

			if (!SetFileTime(fh, ct, at, wt)) res = GetLastError();
			CloseHandle(fh);
			}

		delete at;
		delete wt;
		delete ct;
	#else
		// Use stat to get the file times
		sw_timeval_t	catv[2]; // 0=last access, 1=last mod
		sw_stat_t		sb;

		if (sw_stat(path, &sb) == 0)
			{
			// There is no create time in unix so pretend
			// Do it before lastModTime so that lastModTime takes precedence
			if (createTime != NULL) createTime->toTimeVal(catv[1]);
			else
				{
				catv[1].tv_sec = sb.st_mtime;
				catv[1].tv_usec = 0;
				}

			if (lastModTime != NULL) lastModTime->toTimeVal(catv[1]);
			else
				{
				catv[1].tv_sec = sb.st_mtime;
				catv[1].tv_usec = 0;
				}
			
			if (lastAccessTime != NULL) lastAccessTime->toTimeVal(catv[0]);
				{
				catv[0].tv_sec = sb.st_atime;
				catv[0].tv_usec = 0;
				}
			
			// Convert from sw_timeval_t to platform-specific struct timeval
			struct timeval	tv[2];

			tv[0].tv_sec = (time_t)catv[0].tv_sec;
			tv[0].tv_usec = catv[0].tv_usec;
			tv[1].tv_sec = (time_t)catv[1].tv_sec;
			tv[1].tv_usec = catv[1].tv_usec;

			if (utimes(SWString(path), tv) != 0) res = errno;
			}
		else res = errno;
	#endif

		return res;
		}






