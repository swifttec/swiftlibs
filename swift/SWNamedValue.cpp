/**
*** @file		SWNamedValue.cpp
*** @brief		A SWValue object with an associated name.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWNamedValue class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWNamedValue.h>
#include <swift/SWOutputStream.h>
#include <swift/SWInputStream.h>


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif



SWNamedValue::SWNamedValue(const SWString &name) :
    _name(name)
		{
		}


SWNamedValue::SWNamedValue(const SWString &name, const SWValue &v) :
    _name(name)
		{
		*this = v;
		}


SWNamedValue::SWNamedValue(const SWNamedValue &v) :
	SWValue()
		{
		*this = v;
		}


SWNamedValue::~SWNamedValue()
		{
		}


SWNamedValue::SWNamedValue(const SWString &name, int v, ValueType type) : _name(name)		{ setValue(v, type); }
SWNamedValue::SWNamedValue(const SWString &name, bool v) : _name(name)						{ setValue(v); }
SWNamedValue::SWNamedValue(const SWString &name, sw_int8_t v) : _name(name)					{ setValue(v); }
SWNamedValue::SWNamedValue(const SWString &name, sw_int16_t v) : _name(name)				{ setValue(v); }
SWNamedValue::SWNamedValue(const SWString &name, sw_int32_t v) : _name(name)				{ setValue(v); }
SWNamedValue::SWNamedValue(const SWString &name, sw_int64_t v) : _name(name)				{ setValue(v); }
SWNamedValue::SWNamedValue(const SWString &name, sw_uint8_t v) : _name(name)				{ setValue(v); }
SWNamedValue::SWNamedValue(const SWString &name, sw_uint16_t v) : _name(name)				{ setValue(v); }
SWNamedValue::SWNamedValue(const SWString &name, sw_uint32_t v) : _name(name)				{ setValue(v); }
SWNamedValue::SWNamedValue(const SWString &name, sw_uint64_t v) : _name(name)				{ setValue(v); }
SWNamedValue::SWNamedValue(const SWString &name, float v) : _name(name)						{ setValue(v); }
SWNamedValue::SWNamedValue(const SWString &name, double v) : _name(name)					{ setValue(v); }
SWNamedValue::SWNamedValue(const SWString &name, const char *v) : _name(name)				{ setValue(v); }
SWNamedValue::SWNamedValue(const SWString &name, const wchar_t *v) : _name(name)			{ setValue(v); }
SWNamedValue::SWNamedValue(const SWString &name, const void *p, size_t len) : _name(name)	{ setValue(p, len); }
SWNamedValue::SWNamedValue(const SWString &name, SWBuffer &v) : _name(name)					{ setValue(v); }
SWNamedValue::SWNamedValue(const SWString &name, const SWString &v) : _name(name)			{ setValue(v); }

SWNamedValue::SWNamedValue(const SWString &name, const SWDate &v) : _name(name)			{ setValue(v); }
SWNamedValue::SWNamedValue(const SWString &name, const SWTime &v) : _name(name)			{ setValue(v); }
SWNamedValue::SWNamedValue(const SWString &name, const SWMoney &v) : _name(name)			{ setValue(v); }

#if defined(SW_BUILD_MFC)
SWNamedValue::SWNamedValue(const SWString &name, const COleVariant &v) : _name(name)			{ setValue(v); }
SWNamedValue::SWNamedValue(const SWString &name, const CString &v) : _name(name)			{ setValue(v); }
#endif

SWNamedValue &
SWNamedValue::operator=(const SWNamedValue &v)
	{
	_name = v._name;

	// Call the underlying class assignment operator to do the value assignment
	SWValue::operator=(v);

	return *this;
	}



SWNamedValue &
SWNamedValue::operator=(const SWValue &v)
		{
		// Call the underlying class assignment operator to do the value assignment
		SWValue::operator=(v);

		return *this;
		}





sw_status_t
SWNamedValue::writeToStream(SWOutputStream &stream) const
		{
		sw_status_t		res=SW_STATUS_SUCCESS;

		if (res == SW_STATUS_SUCCESS) res = SWValue::writeToStream(stream);
		if (res == SW_STATUS_SUCCESS) res = stream.write(_name);

		return res;
		}


sw_status_t
SWNamedValue::readFromStream(SWInputStream &stream)
		{
		sw_status_t		res=SW_STATUS_SUCCESS;

		clear();
		if (res == SW_STATUS_SUCCESS) res = SWValue::readFromStream(stream);
		if (res == SW_STATUS_SUCCESS) res = stream.read(_name);

		return res;
		}
