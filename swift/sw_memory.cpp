/**
*** A collection of internal functions
**/

/*********************************************************************************
Copyright (c) 2006-2016 SwiftTec Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <swift/sw_types.h>
#include <stdio.h>

#ifdef _DEBUG

#define	MAGIC1	0xabcdef01
#define	MAGIC2	0xabcdef02

#define SW_MEMORY_TRACE_NEW				0x0001
#define SW_MEMORY_TRACE_DELETE			0x0002
#define SW_MEMORY_CHECKBLOCKS_ON_NEW	0x0004
#define SW_MEMORY_CHECKBLOCKS_ON_DELETE	0x0008

// A simple mutex class which doesn't use the Swift based classes
class MemoryMutex
		{
public:
		MemoryMutex();
		~MemoryMutex();

		void *	operator new(size_t size)	{ return ::malloc(size); }
		void	operator delete(void *p)	{ ::free(p); }

		void	acquire();
		void	release();

private:
		HANDLE	m_hMutex;
		};


MemoryMutex::MemoryMutex() :
	m_hMutex(NULL)
		{
		m_hMutex = ::CreateMutex(NULL, FALSE, NULL);
		}


MemoryMutex::~MemoryMutex()
		{
		::CloseHandle(m_hMutex);
		}


// Acquire the mutex
void
MemoryMutex::acquire()
		{
		WaitForSingleObject(m_hMutex, INFINITE);
		}


// Release the mutex
void
MemoryMutex::release()
		{
		::ReleaseMutex(m_hMutex);
		}



class Guard
		{
public:
		/// Destructor
		~Guard()
				{
				pMutex->release();
				}

		/// Guard for an Mutex (pointer form)
		Guard(MemoryMutex *obj)
				{
				pMutex = obj;
				pMutex->acquire();
				}

		/// Guard for an Mutex (reference form)
		Guard(MemoryMutex &obj)
				{
				pMutex = &obj;
				pMutex->acquire();
				}

private:
		MemoryMutex	*pMutex;
		};





class MemoryBlock
		{
public:
		void		*pAddress;
		size_t		blocksize;
		size_t		size;
		char		*file;
		int			line;
		int			type;
		MemoryBlock	*pNext;
		MemoryBlock	*pPrev;

public:
		MemoryBlock();
		~MemoryBlock();
		void *	operator new(size_t size)	{ return ::malloc(size); }
		void	operator delete(void *p)	{ ::free(p); }
		};
	  
MemoryBlock::MemoryBlock() :
	file(NULL)
		{
		}

MemoryBlock::~MemoryBlock()
		{
		if (file != NULL)
			{
			::free(file);
			file = NULL;
			}
		}


static MemoryBlock			*pListHead=NULL, *pListTail=NULL;
static MemoryMutex			*pMutex=NULL;
static int					tflags=0;

#define CHECK_MUTEX()	if (pMutex == NULL) pMutex = new MemoryMutex()
#define GUARD_MUTEX()	CHECK_MUTEX(); Guard	guard(pMutex)
#define ACQUIRE_MUTEX()	CHECK_MUTEX(); pMutex->acquire()
#define RELEASE_MUTEX()	CHECK_MUTEX(); pMutex->release()

static void
sw_memory_trace(const TCHAR *fmt, ...)
		{
		va_list	ap;
		TCHAR	buf[1024];

		va_start(ap, fmt);
		t_vsprintf(buf, fmt, ap);
		va_end(ap);

		TRACE(buf);
		printf("%s", buf);
		}


static void
sw_memory_checkMemoryBlock(MemoryBlock *pBlock)
		{
		if (pBlock != NULL)
			{
			char		*pData=(char *)pBlock->pAddress;
			unsigned	*pMagic1=(unsigned *)(pData - 4);
			unsigned	*pMagic2=(unsigned *)(pData + pBlock->blocksize - 8);

			if (*pMagic1 != MAGIC1)
				{
				sw_memory_trace(_T("Memory block at 0x%08x corrupted before buffer (underrun)\n"), pData);
				}
			if (*pMagic2 != MAGIC2)
				{
				sw_memory_trace(_T("Memory block at 0x%08x corrupted after buffer (overrun)\n"), pData);
				}
			}
		}


SWIFT_DLL_EXPORT void
sw_memory_setTraceFlags(int flags)
		{
		tflags = flags;
		}


SWIFT_DLL_EXPORT void
sw_memory_showBlockList()
		{
		GUARD_MUTEX();

		sw_memory_trace(_T("--------------- BEGIN MemoryBlock List ----------------\n"));
		MemoryBlock	*pBlock;

		pBlock = pListHead;
		while (pBlock != NULL)
			{
			sw_memory_trace(_T("Block 0x%08x  size=%d  file=%hs  line=%d\n"), pBlock->pAddress, pBlock->size, pBlock->file, pBlock->line);
			sw_memory_checkMemoryBlock(pBlock);
			pBlock = pBlock->pNext;
			}
		sw_memory_trace(_T("---------------- END MemoryBlock List -----------------\n"));
		}


SWIFT_DLL_EXPORT void
sw_memory_checkBlockList()
		{
		GUARD_MUTEX();
		MemoryBlock	*pBlock;

		pBlock = pListHead;
		while (pBlock != NULL)
			{
			sw_memory_checkMemoryBlock(pBlock);
			pBlock = pBlock->pNext;
			}
		}


SWIFT_DLL_EXPORT void *
sw_memory_addBlock(size_t size, int type, const char *fname, int line)
		{
		GUARD_MUTEX();
		size_t		blocksize=size+8;

		if ((blocksize % 4) != 0)
			{
			blocksize += (4 - (blocksize % 4));
			}

		char		*ptr=(char *)malloc(blocksize);

		if (ptr == NULL)
			{
			sw_memory_trace(_T("NEW %d bytes memory at line %d of %hs - FAILED\n"), size, line, fname);
			}
		else
			{
			unsigned	*pMagic1=(unsigned *)(ptr);
			unsigned	*pMagic2=(unsigned *)(ptr + blocksize - 4);

			*pMagic1 = MAGIC1;
			*pMagic2 = MAGIC2;

			ptr += 4;

			MemoryBlock	*pBlock;
			
			if ((tflags & SW_MEMORY_TRACE_NEW) != 0) sw_memory_trace(_T("NEW %d bytes memory at line %d of %hs - 0x%08x\n"), size, line, fname, ptr);

			pBlock = new MemoryBlock();
			pBlock->pAddress = ptr;
			pBlock->size = size;
			pBlock->blocksize = blocksize;
			pBlock->line = line;
			pBlock->type = type;
			pBlock->file = _strdup(fname);
			pBlock->pNext = pBlock->pPrev = NULL;

			if (pListTail == NULL)
				{
				pListHead = pListTail = pBlock;
				}
			else
				{
				// Add it to the end
				pBlock->pPrev = pListTail;
				pListTail->pNext = pBlock;
				pListTail = pBlock;
				}

			if ((tflags & SW_MEMORY_CHECKBLOCKS_ON_NEW) != 0) sw_memory_checkBlockList();
			}

		return ptr;
		}


static MemoryBlock *
sw_memory_findBlock(void *ptr)
		{
		MemoryBlock	*pBlock, *pFoundBlock=NULL;

		pBlock = pListHead;
		while (pBlock != NULL)
			{
			if (pBlock->pAddress == ptr)
				{
				pFoundBlock = pBlock;
				break;
				}
			pBlock = pBlock->pNext;
			}

		return pFoundBlock;
		}


SWIFT_DLL_EXPORT void
sw_memory_removeBlock(void *ptr, int type, const char *file, int line)
		{
		GUARD_MUTEX();

		if (ptr != NULL)
			{
			MemoryBlock	*pBlock;

			pBlock = sw_memory_findBlock(ptr);
			if (pBlock != NULL)
				{
				sw_memory_checkMemoryBlock(pBlock);

				if (pBlock->type != type)
					{
					sw_memory_trace(_T("DELETE memory at 0x%08x with wrong type (was %d, expected %d) - line %d of %hs\n"), ptr, pBlock->type, type, line, file);
					}

				if (pBlock->pPrev != NULL)
					{
					pBlock->pPrev->pNext = pBlock->pNext;
					}
				if (pBlock->pNext != NULL)
					{
					pBlock->pNext->pPrev = pBlock->pPrev;
					}
				if (pListHead == pBlock)
					{
					pListHead = pBlock->pNext;
					}
				if (pListTail == pBlock)
					{
					pListTail = pBlock->pPrev;
					}

				free(((char *)pBlock->pAddress) - 4);
				delete pBlock;

				if ((tflags & SW_MEMORY_TRACE_DELETE) != 0) sw_memory_trace(_T("DELETE memory at 0x%08x - line %d of %hs\n"), ptr, line, file);
				
				if ((tflags & SW_MEMORY_CHECKBLOCKS_ON_DELETE) != 0) sw_memory_checkBlockList();
				}
			else
				{
				// Attempt to throw exception
				sw_memory_trace(_T("DELETE memory at 0x%08x - NOT FOUND - line %d of %hs\n"), ptr, line, file);
				free(ptr);
				}
			}
		}


SWIFT_DLL_EXPORT void *
sw_memory_resizeBlock(void *ptr, size_t size, int type, const char *file, int line)
		{
		void	*p;

		if (ptr == NULL)
			{
			p = sw_memory_addBlock(size, type, file, line);
			}
		else
			{
			MemoryBlock	*pBlock;

			ACQUIRE_MUTEX();
			pBlock = sw_memory_findBlock(ptr);
			if (pBlock != NULL)
				{
				// Check it beforehand
				sw_memory_checkMemoryBlock(pBlock);

				if (size <= pBlock->size)
					{
					// Do nothing
					p = ptr;
					RELEASE_MUTEX();
					}
				else
					{
					RELEASE_MUTEX();

					// Simulate realloc function by allocating a new block
					// copying contents and freeing the old block
					p = sw_memory_addBlock(size, type, file, line);
					memcpy(p, ptr, pBlock->size);
					sw_memory_removeBlock(ptr, type, file, line);
					}
				}
			else
				{
				// Attempt to throw exception
				sw_memory_trace(_T("RESIZE memory at 0x%08x - NOT FOUND - line %d of %hs\n"), ptr, line, file);
				p = realloc(ptr, size);
				}
			}

		return p;
		}


SWIFT_DLL_EXPORT void
sw_memory_checkBlock(void *ptr)
		{
		if (ptr != NULL)
			{
			MemoryBlock	*pBlock;

			pBlock = sw_memory_findBlock(ptr);
			if (pBlock != NULL)
				{
				sw_memory_checkMemoryBlock(pBlock);
				}
			}
		}


#endif // _DEBUG
