/**
*** @file		SWUdpSocket.h
*** @brief		A TCP socket
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWUdpSocket class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWUdpSocket_h__
#define __SWUdpSocket_h__

#include <swift/SWSocket.h>




// Forward declarations

/**
*** @brief	A TCP socket implementation
***
*** The SWUdpSocket is a TCP specific socket implementation which
*** supports both client and server sockets.
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Safe when synchronized</td>
*** </tr>
*** </table>
***
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWUdpSocket : public SWSocket
		{
public:
		/// Default Constructor
		SWUdpSocket();

		/// Destructor
		virtual ~SWUdpSocket();

public: // SWSocket Overrides

		/**
		*** Opens/creates the socket without making a connection.
		**/
		virtual sw_status_t			open();

		/**
		*** Closes the socket.
		**/
		virtual sw_status_t			close();

		/**
		*** @brief	Send data to an unconnected receiver.
		***
		*** This method writes data to the specified socket address. The socket does not
		*** need to be in a connected state in order to use this method.
		***
		*** @param[in]	addr			The socket address to send the data to.
		*** @param[in]	pBuffer			A pointer to the data to be written.
		*** @param[in]	bytesToWrite	The number of bytes of data to write.
		*** @param[out]	pBytesWritten	A pointer which if non-NULL points to a
		***								sw_size_t variable which receives a count
		***								of the bytes actually written down the socket.
		***
		*** @return		SW_STATUS_SUCCESS if the operation was successful, or a
		***				status code otherwise.
		**/
		virtual sw_status_t			sendTo(const SWSocketAddress &addr, const void *pBuffer, sw_size_t bytesToWrite, sw_size_t *pBytesWritten=NULL);

		/**
		*** @brief	Receive data fron an unconnected receiver.
		***
		*** This method reads data from the socket address. The specified
		*** addr parameter receives the address that the data was received from.
		***
		*** @param[out]	addr		Receives the socket address the data was sent from.
		*** @param[in]	pBuffer		A pointer to a buffer that will receive the data.
		*** @param[in]	bytesToRead	The number of bytes to attempt to read from the socket.
		*** @param[out]	pBytesRead	A pointer which if non-NULL points to a
		***							sw_size_t variable which receives a count
		***							of the bytes actually read from the socket.
		***
		*** @return		SW_STATUS_SUCCESS if the operation was successful, or a
		***				status code otherwise.
		***
		*** @note
		*** Due to the nature of UDP sockets (being packet oriented in nature) the amount of data
		*** actually read from the socket is normally less than that of the bytesToRead parameter.
		*** If there is more data in the packet than could be read into the buffer, the additional
		*** data would be lost.
		***
		**/
		virtual sw_status_t			receiveFrom(SWSocketAddress &addr, void *pBuffer, sw_size_t bytesToRead, sw_size_t *pBytesRead=NULL);
		};




#endif
