/****************************************************************************
**	SWPagedFile.cpp	A random access block based read-write file cache class
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




#include "stdafx.h"

#include <swift/SWPagedFile.h>
#include <swift/SWGuard.h>
#include <swift/SWException.h>
#include <swift/SWMemoryInputStream.h>
#include <swift/SWMemoryOutputStream.h>

#include <swift/sw_file.h>
#include <xplatform/sw_math.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




SWPagedFile::SWPagedFile(const SWString &filename, bool readonly, int pagesize, int maxpages) :
    m_pagesize(0),
    m_maxpages(0)
		{
		// We must have locking turned on for this as we will have a background thread
		// that will write the pages back to disk for us.
		synchronize();

		open(filename, readonly, pagesize, maxpages);
		}


SWPagedFile::~SWPagedFile()
		{
		close();
		}
	

sw_status_t
SWPagedFile::open(const SWString &filename, bool readonly, int pagesize, int maxpages)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (m_file.isOpen())
			{
			res = SW_STATUS_ALREADY_OPEN;
			}
		else
			{
			int			mode;

			if (readonly)
				{
				mode = SWFile::ModeReadOnly;
				}
			else
				{
				mode = SWFile::ModeCreate | SWFile::ModeReadWrite;
				};

			mode |= (SWFile::ModeBinary);
			res = m_file.open(filename, mode);
			m_pagesize = pagesize;
			m_maxpages = maxpages;
			}

		return res;
		}


sw_status_t
SWPagedFile::close()
		{
		sw_status_t	res=SW_STATUS_SUCCESS, r;

		if (m_file.isOpen())
			{
			if (!m_file.isReadOnly())
				{
				flush();
				}

			SWPagedFilePage	*pp=NULL;

			while (m_pagelist.size() > 0)
				{
				m_pagelist.remove(SWList<SWPagedFilePage *>::ListHead, &pp);
				if (pp->isDirty())
					{
					r = writePageToDisk(pp);
					if (r != SW_STATUS_SUCCESS && res == SW_STATUS_SUCCESS)
						{
						res = r;
						}
					}
				delete pp;
				}

			r = m_file.close();
			if (r != SW_STATUS_SUCCESS && res == SW_STATUS_SUCCESS)
				{
				res = r;
				}

			m_pagesize = 0;
			m_maxpages = 0;
			}

		return res;
		}


/**
*** Flushes dirty pages to disk
**/
sw_status_t
SWPagedFile::flush()
		{
		SWGuard							guard(this);
		SWPagedFilePage					*pp;
		SWIterator<SWPagedFilePage *>	it;
		sw_status_t						res=SW_STATUS_SUCCESS, r;

		it = m_pagelist.iterator();
		while (it.hasNext())
			{
			pp = *it.next();
			if (pp->isDirty())
				{
				r = writePageToDisk(pp);
				if (r != SW_STATUS_SUCCESS && res == SW_STATUS_SUCCESS)
					{
					res = r;
					}
				}
			}

		return res;
		}


/**
*** Writes a dirty page to disk and marks it as clean
**/
sw_status_t
SWPagedFile::writePageToDisk(SWPagedFilePage *pp)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		// Only bother if the page is marked as dirty
		if (pp->isDirty())
			{
		//	sw_log_trace1(_T("**** SWPagedFile::writePageToDisk - page %d"), pp->pageNo());
			res = m_file.lseek(pp->pageNo() * m_pagesize, SWFile::SeekSet);
			if (res == SW_STATUS_SUCCESS)
				{
				res = m_file.write(pp->data(), m_pagesize);
				}

			if (res == SW_STATUS_SUCCESS)
				{
				pp->m_dirty = false;
				}
			}

		return res;
		}



/**
*** Reads a page from disk and marks it as clean
**/
sw_status_t
SWPagedFile::readPageFromDisk(SWPagedFilePage *pp)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

	//	sw_log_trace1(_T("**** SWPagedFile::readPageFromDisk - page %d"), pp->pageNo());
		memset(pp->data(), 0, m_pagesize);
		res = m_file.lseek(pp->pageNo() * m_pagesize, SWFile::SeekSet);
		if (res == SW_STATUS_SUCCESS)
			{
			res = m_file.read(pp->data(), m_pagesize);
			}

		pp->m_dirty = false;

		return res;
		}


/**
*** Gets and returns the page for the given page number.
*** The page will be read into memory if required
*** flushing another page if necessary
**/
SWPagedFilePage *
SWPagedFile::getPage(int page)
		{
		SWIterator<SWPagedFilePage *>	it;
		SWPagedFilePage	*pp;

		if (m_pagemap.getBit(page))
			{
			// The page is available in memory so find it
			it = m_pagelist.iterator();
			while (it.hasNext())
				{
				pp = *it.next();
				if (pp->pageNo() == page)
					{
					time(&(pp->m_lastAccessTime));
					return pp;
					}
				}

			// We should not get here, but catch it in case we do whilst under development
			throw SW_EXCEPTION_TEXT(SWIllegalStateException, "Error in SWPagedFile page list - expected page not found");
			}

		// The page is not available in memory if we get here
		// so we must create one

		// If we are at the page limit we must drop a page so we don't
		// use too much memory
		if ((int)m_pagelist.size() >= m_maxpages) dropLruPage();

		// Now we can create a page
		pp = new SWPagedFilePage(page, m_pagesize);

		// TODO: Put in the list in an ordered fashion for more efficient
		//       caching
		readPageFromDisk(pp);
		m_pagelist.add(pp);
		time(&(pp->m_lastAccessTime));
		m_pagemap.setBit(pp->pageNo());

		//sw_log_trace1(_T("**** SWPagedFile::getPage - added page %d"), pp->pageNo());

		return pp;
		}


/**
*** Drops the LRU page from the cache to make room 
*** for a new page. A clean page is dropped in preference to
*** a dirty page is the preferClean flag is true
**/
void
SWPagedFile::dropLruPage(bool preferClean)
		{
		SWPagedFilePage	*oldest=NULL;
		SWPagedFilePage	*oldestClean=NULL;
		SWPagedFilePage	*pp;
		SWIterator<SWPagedFilePage *>	it = m_pagelist.iterator();

		while (it.hasNext())
			{
			pp = *it.next();

			// Check for the oldest page
			if (oldest == NULL || pp->m_lastAccessTime < oldest->m_lastAccessTime) oldest = pp;

			// Check for the oldest clean page
			if (pp->isClean() && (oldestClean == NULL || pp->m_lastAccessTime < oldestClean->m_lastAccessTime)) oldestClean = pp;
			}

		pp = oldest;
		if (preferClean && oldestClean != NULL) pp = oldestClean;

		if (pp != NULL)
			{
	//	    sw_log_trace1(_T("**** SWPagedFile::dropLruPage - dropping page %d"), pp->pageNo());
			if (pp->isDirty()) writePageToDisk(pp);

			m_pagemap.clearBit(pp->pageNo());
			m_pagelist.remove(pp);
			delete pp;
			}
		}


/**
*** writes data to the file
**/
sw_status_t
SWPagedFile::write(off_t off, const void *pData, sw_size_t nbytes, sw_size_t *pBytesWritten)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		sw_size_t	nwritten=0;

		if (nbytes != 0)
			{
			SWGuard			guard(this);
			SWPagedFilePage	*pp;
			int				pageno, n, p;
			const char		*bp = (const char *)pData;

			// Calculate first write size
			while (nbytes > 0)
				{
				p = (off % m_pagesize);
				n = sw_math_min(m_pagesize - p, (int)nbytes);
				pageno = pageNumber(off);
				pp = getPage(pageno);
				memcpy(pp->data() + p, bp, n);
				pp->setDirty();
				bp += n;
				off += n;
				nbytes -= n;
				pageno++;
				nwritten += n;
				}
			}

		if (pBytesWritten != NULL)
			{
			*pBytesWritten = nwritten;
			}

		return res;
		}


/**
*** reads data from the file
**/
sw_status_t
SWPagedFile::read(off_t off, void *pData, sw_size_t nbytes, sw_size_t *pBytesRead)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		sw_size_t	nread=0;

		if (nbytes != 0)
			{
			SWGuard			guard(this);
			SWPagedFilePage	*pp;
			int				pageno, n, p;
			char			*bp = (char *)pData;

			// Calculate first write size
			while (nbytes > 0)
				{
				p = (off % m_pagesize);
				n = sw_math_min(m_pagesize - p, (int)nbytes);
				pageno = pageNumber(off);
				pp = getPage(pageno);
				memcpy(bp, pp->data() + p, n);
				bp += n;
				off += n;
				nbytes -= n;
				nread += n;
				pageno++;
				}
			}

		if (pBytesRead != NULL)
			{
			*pBytesRead = nread;
			}

		return res;
		}





/**
*** Dump to stdout
**/
void
SWPagedFile::dump()
		{
		//SWObject::dump();
		
		printf("Page Size      = %d\n", pagesize());
		printf("Page Count     = %d\n", pagecount());
		printf("Max Page Count = %d\n", maxpages());

		SWPagedFilePage	*pp;
		SWIterator<SWPagedFilePage *>	it = m_pagelist.iterator();

		while (it.hasNext())
			{
			pp = *it.next();

			printf("  Page %d: %s\n", pp->pageNo(), pp->isDirty()?"DIRTY":"CLEAN");
			}
		}




sw_status_t
SWPagedFile::read(off_t off, sw_int8_t &v)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		char		buf[sizeof(v)];

		res = read(off, buf, sizeof(v));

		if (res == SW_STATUS_SUCCESS)
			{
			SWMemoryInputStream	mis(buf, sizeof(buf));

			res = mis.read(v);
			}

		return res;
		}


sw_status_t
SWPagedFile::read(off_t off, sw_int16_t &v)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		char		buf[sizeof(v)];

		res = read(off, buf, sizeof(v));

		if (res == SW_STATUS_SUCCESS)
			{
			SWMemoryInputStream	mis(buf, sizeof(buf));

			res = mis.read(v);
			}

		return res;
		}


sw_status_t
SWPagedFile::read(off_t off, sw_int32_t &v)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		char		buf[sizeof(v)];

		res = read(off, buf, sizeof(v));

		if (res == SW_STATUS_SUCCESS)
			{
			SWMemoryInputStream	mis(buf, sizeof(buf));

			res = mis.read(v);
			}

		return res;
		}


sw_status_t
SWPagedFile::read(off_t off, sw_int64_t &v)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		char		buf[sizeof(v)];

		res = read(off, buf, sizeof(v));

		if (res == SW_STATUS_SUCCESS)
			{
			SWMemoryInputStream	mis(buf, sizeof(buf));

			res = mis.read(v);
			}

		return res;
		}


sw_status_t
SWPagedFile::read(off_t off, sw_uint8_t &v)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		char		buf[sizeof(v)];

		res = read(off, buf, sizeof(v));

		if (res == SW_STATUS_SUCCESS)
			{
			SWMemoryInputStream	mis(buf, sizeof(buf));

			res = mis.read(v);
			}

		return res;
		}


sw_status_t
SWPagedFile::read(off_t off, sw_uint16_t &v)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		char		buf[sizeof(v)];

		res = read(off, buf, sizeof(v));

		if (res == SW_STATUS_SUCCESS)
			{
			SWMemoryInputStream	mis(buf, sizeof(buf));

			res = mis.read(v);
			}

		return res;
		}


sw_status_t
SWPagedFile::read(off_t off, sw_uint32_t &v)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		char		buf[sizeof(v)];

		res = read(off, buf, sizeof(v));

		if (res == SW_STATUS_SUCCESS)
			{
			SWMemoryInputStream	mis(buf, sizeof(buf));

			res = mis.read(v);
			}

		return res;
		}


sw_status_t
SWPagedFile::read(off_t off, sw_uint64_t &v)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		char		buf[sizeof(v)];

		res = read(off, buf, sizeof(v));

		if (res == SW_STATUS_SUCCESS)
			{
			SWMemoryInputStream	mis(buf, sizeof(buf));

			res = mis.read(v);
			}

		return res;
		}


sw_status_t
SWPagedFile::read(off_t off, float &v)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		char		buf[sizeof(v)];

		res = read(off, buf, sizeof(v));

		if (res == SW_STATUS_SUCCESS)
			{
			SWMemoryInputStream	mis(buf, sizeof(buf));

			res = mis.read(v);
			}

		return res;
		}


sw_status_t
SWPagedFile::read(off_t off, double &v)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		char		buf[sizeof(v)];

		res = read(off, buf, sizeof(v));

		if (res == SW_STATUS_SUCCESS)
			{
			SWMemoryInputStream	mis(buf, sizeof(buf));

			res = mis.read(v);
			}

		return res;
		}




sw_status_t
SWPagedFile::read(off_t off, bool &v)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		char		buf[sizeof(v)];

		res = read(off, buf, sizeof(v));

		if (res == SW_STATUS_SUCCESS)
			{
			SWMemoryInputStream	mis(buf, sizeof(buf));

			res = mis.read(v);
			}

		return res;
		}



sw_status_t
SWPagedFile::write(off_t off, bool v)
		{
		SWMemoryOutputStream	mos;
		sw_status_t				res=SW_STATUS_SUCCESS;
		
		res = mos.write(v);
		if (res == SW_STATUS_SUCCESS)
			{
			res = write(off, mos.data(), sizeof(v));
			}

		return res;
		}


sw_status_t
SWPagedFile::write(off_t off, sw_int8_t v)
		{
		SWMemoryOutputStream	mos;
		sw_status_t				res=SW_STATUS_SUCCESS;
		
		res = mos.write(v);
		if (res == SW_STATUS_SUCCESS)
			{
			res = write(off, mos.data(), sizeof(v));
			}

		return res;
		}


sw_status_t
SWPagedFile::write(off_t off, sw_int16_t v)
		{
		SWMemoryOutputStream	mos;
		sw_status_t				res=SW_STATUS_SUCCESS;
		
		res = mos.write(v);
		if (res == SW_STATUS_SUCCESS)
			{
			res = write(off, mos.data(), sizeof(v));
			}

		return res;
		}


sw_status_t
SWPagedFile::write(off_t off, sw_int32_t v)
		{
		SWMemoryOutputStream	mos;
		sw_status_t				res=SW_STATUS_SUCCESS;
		
		res = mos.write(v);
		if (res == SW_STATUS_SUCCESS)
			{
			res = write(off, mos.data(), sizeof(v));
			}

		return res;
		}


sw_status_t
SWPagedFile::write(off_t off, sw_int64_t v)
		{
		SWMemoryOutputStream	mos;
		sw_status_t				res=SW_STATUS_SUCCESS;
		
		res = mos.write(v);
		if (res == SW_STATUS_SUCCESS)
			{
			res = write(off, mos.data(), sizeof(v));
			}

		return res;
		}


sw_status_t
SWPagedFile::write(off_t off, sw_uint8_t v)
		{
		SWMemoryOutputStream	mos;
		sw_status_t				res=SW_STATUS_SUCCESS;
		
		res = mos.write(v);
		if (res == SW_STATUS_SUCCESS)
			{
			res = write(off, mos.data(), sizeof(v));
			}

		return res;
		}


sw_status_t
SWPagedFile::write(off_t off, sw_uint16_t v)
		{
		SWMemoryOutputStream	mos;
		sw_status_t				res=SW_STATUS_SUCCESS;
		
		res = mos.write(v);
		if (res == SW_STATUS_SUCCESS)
			{
			res = write(off, mos.data(), sizeof(v));
			}

		return res;
		}


sw_status_t
SWPagedFile::write(off_t off, sw_uint32_t v)
		{
		SWMemoryOutputStream	mos;
		sw_status_t				res=SW_STATUS_SUCCESS;
		
		res = mos.write(v);
		if (res == SW_STATUS_SUCCESS)
			{
			res = write(off, mos.data(), sizeof(v));
			}

		return res;
		}


sw_status_t
SWPagedFile::write(off_t off, sw_uint64_t v)
		{
		SWMemoryOutputStream	mos;
		sw_status_t				res=SW_STATUS_SUCCESS;
		
		res = mos.write(v);
		if (res == SW_STATUS_SUCCESS)
			{
			res = write(off, mos.data(), sizeof(v));
			}

		return res;
		}


sw_status_t
SWPagedFile::write(off_t off, float v)
		{
		SWMemoryOutputStream	mos;
		sw_status_t				res=SW_STATUS_SUCCESS;
		
		res = mos.write(v);
		if (res == SW_STATUS_SUCCESS)
			{
			res = write(off, mos.data(), sizeof(v));
			}

		return res;
		}


sw_status_t
SWPagedFile::write(off_t off, double v)
		{
		SWMemoryOutputStream	mos;
		sw_status_t				res=SW_STATUS_SUCCESS;
		
		res = mos.write(v);
		if (res == SW_STATUS_SUCCESS)
			{
			res = write(off, mos.data(), sizeof(v));
			}

		return res;
		}




SWPagedFilePage::SWPagedFilePage(int pageno, int size) :
    m_pageno(pageno),
    m_dirty(false)
		{
		m_pData = new char[size];
		memset(m_pData, 0, size);
		}


SWPagedFilePage::~SWPagedFilePage()
		{
		delete [] m_pData;
		m_pData = 0;
		}




