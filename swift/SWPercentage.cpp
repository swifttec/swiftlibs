/****************************************************************************
**	SWIniFile.C	A class for reading and writing .ini files
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2016 SwiftTec Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <swift/SWPercentage.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif

// Begin the namespace
SWIFT_BEGIN_NAMESPACE


#define DP			4
#define INT_DIVISOR	10000
#define DBL_DIVISOR	((double)INT_DIVISOR * 100.0)

SWPercentage::SWPercentage(int v) :
	m_percentage(v)
		{
		}

SWPercentage::~SWPercentage(void)
		{
		}

SWPercentage::SWPercentage(const SWPercentage &other) :
	m_percentage(other.m_percentage)
		{
		}


SWPercentage::SWPercentage(const SWString &s)
		{
		*this = s;
		}



SWPercentage &
SWPercentage::operator=(const SWPercentage &other)
		{
		m_percentage = other.m_percentage;

		return *this;
		}



SWPercentage &
SWPercentage::operator=(int v)
		{
		m_percentage = v;

		return *this;
		}



SWPercentage &
SWPercentage::operator=(const SWString &s)
		{
		int			p;
		int			w, f;
		SWString	t;
		
		p = s.first('.');
		if (p < 0)
			{
			t = s;
			t.replaceAll(",", "");
			w = t.toInt32(10);
			f = 0;
			}
		else
			{
			t = s.left(p);
			t.replaceAll(",", "");
			w = t.toInt32(10);

			t = s.mid(p+1, DP);
			if ((p = t.first('%')) >= 0) t.remove(p, t.length()-p);
			while (t.length() < DP) t += "0";
			
			f = t.toInt32(10);
			}

		m_percentage = (w * INT_DIVISOR) + f;

		return *this;
		}


SWPercentage::operator double() const
		{
		return (double)m_percentage / DBL_DIVISOR;
		}


SWPercentage::operator int() const
		{
		return m_percentage;
		}


SWString
SWPercentage::toString(bool addPercentSign) const
		{
		SWString	s;
		int			w, f;
		
		w = m_percentage / INT_DIVISOR;
		f = m_percentage % INT_DIVISOR;
		
		s.format("%d", w);
		if (f != 0)
			{
			char	ns[32], *np;

			sprintf(ns, ".%0*d", DP, f);
			np = &ns[DP];
			while (np >= ns && *np == '0') *np-- = 0;

			s += ns;
			}
		
		if (addPercentSign)
			{
			s += "%";
			}

		return s;
		}


int
SWPercentage::compareTo(const SWPercentage &other) const
		{
		return m_percentage - other.m_percentage;
		}

// end the namespace
SWIFT_END_NAMESPACE
