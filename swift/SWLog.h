/**
*** @file		SWLog.h
*** @brief		A class to handle logging operations.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWLog class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWLog_h__
#define __SWLog_h__


#include <swift/swift.h>
#include <swift/SWLogMessage.h>
#include <swift/SWLogMessageHandler.h>

#include <swift/SWString.h>
#include <swift/SWList.h>
#include <swift/SWQueue.h>


// Forward declarations
class SWLog;


/**
*** @internal
*** @brief		A class used to store log message handler entries in the SWLog object.
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Unsafe</td>
*** </tr>
*** </table>
***
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWLogMessageHandlerEntry
		{
public:
		/// Constructor
		SWLogMessageHandlerEntry(SWLogMessageHandler *pHandler=NULL, bool delflag=false) :
			m_pHandler(pHandler),
			m_delflag(delflag)
				{
				}

		/// Copy constructor
		SWLogMessageHandlerEntry(const SWLogMessageHandlerEntry &other)
				{
				*this = other;
				}

		/// Assignment operator
		SWLogMessageHandlerEntry &
		operator=(const SWLogMessageHandlerEntry &other)
				{
				m_pHandler = other.m_pHandler;
				m_delflag = other.m_delflag;

				return *this;
				}

		/// Returns the delete flag which is used by SWLog to determine if
		/// the handler stored should be deleted or not.
		bool
		deleteFlag() const
				{
				return m_delflag;
				}


		/// Returns a pointer to the stored message handler
		SWLogMessageHandler	*
		handler() const
				{
				return m_pHandler;
				}
		
		/// Comparison operator (required for SWList template)
		bool
		operator==(const SWLogMessageHandlerEntry &other) const
				{
				return (m_pHandler == other.m_pHandler);
				}

private:
		SWLogMessageHandler	*m_pHandler;	///< A pointer to the log message handler
		bool				m_delflag;		///< A flag that determines if the SWLog object is responsible for deleting the attached handler.
		};



/**
*** @internal
*** @brief		A thread class used when the SWLog object is running in "background mode".
***
*** This class implements the run function of the SWThread class. The thread waits for messages
*** to added to the queue and the processes all queued messages by passing the messages to the
*** registered handlers and then deleting the messages once handling is complete.
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Safe</td>
*** </tr>
*** </table>
***
***
*** @author		Simon Sparkes
*** @see		SWLog for more details
**/
class SWIFT_DLL_EXPORT SWLogThread : public SWThread
		{
friend class SWLog;

public:
		/// Constructor
		SWLogThread(SWLog *pOwner);
		virtual ~SWLogThread();

		virtual int	run();

protected:
		void		add(const SWLogMessage &lm);	///< Called by SWLog to add a message to the queue
		void		flush();						///< Called by SWLog to flush any queued log messages
		void		shutdown();						///< Called by SWLog to shutdown the queue and exit the thread

private:
		SWLog			*m_pOwner;		///< The owning SWLog object
		SWQueue			m_logQ;			///< The queue of pending log messages
		SWThreadCondVar	m_cond;			///< Conditional var to syncronise flushing
		bool			m_flushFlag;	///< A flag to indicate that the queue must be flushed
		};


/**
*** @brief		A logging object that can handle a variety of logging options.
***
*** The SWLog class is a extensible and comprehensive logging facility. It uses
*** a SWLogMessage object to represent a single log message. The SWLogMessage object
*** holds all the attributes required to represent a log message on any system.
***
*** - The a high resolution time which represents the time the log message was created
*** - The id of the process that created the message
*** - The id of the thread within the process that created the message
*** - The log level of this message (fatal, error, warning, info, etc)
*** - The message feature code which can be used to filter messages.
*** - The name of the application that created the message
*** - The filename and line number where the message was generated.
*** - The message text
*** - The id of the message
***
***
*** @par Creating log messages
***
*** Log messages from an application can passed into the SWLog object via
*** 4 possible routes:
***
*** - The simple log methods debug, trace, etc.
*** - The report method which allows the specifying of most of the SWLogMessage contents
*** - Via an application generated SWLogMessage object
*** - From a another SWLog object
***
*** These are described in more detail below.
***
*** Each log message has a number of attributes which can be used to create complex
*** filtering (see below). To make the SWLog class easy to use a number of quick
*** methods and helpful macros have been added to make life easy for those wanting
*** to use the log class.
***
***
*** @par Using the "simple" methods
***
*** The simple methods allow a quick use of the SWLog class to create basic log entries.
*** The methods error, fatal, alert, etc take a printf like format string and create
*** log messages with a generic feature value, and appropriate log level and flags.
*** The file and line number information is set to default values.
***
*** There are a number of macros of the for SW_LOG_XXXXN defined in SWLogMessage.h which can 
*** be used to fill in the file and line information automatically. For example one 
*** could write:
***
*** 	SW_LOG_DEBUG5(log, "Starting XXX(%d, %d, %d, %d, %d)", a, b, c, d, e);
***
*** Note that the number specifies the number of parameters expected.
***
***
*** @par Using the "complex" methods
***
*** The report method allows the caller to define most of the log message parameters,
*** with the exception of system information like thread ID, process ID and create time.
*** When using the report method the caller passes all of the log message information
*** along with a format string and any parameters required. The message flags field is
*** constructed using a combination of a message type and a message level (see SWLogMessage.h).
*** Alternative there are a number of pre-defined combinations in SWLogMessage.h which should
*** be adequate for most applications.
***
*** If total control of message details is required (for example to re-construct messages
*** received from another process, perhaps from another machine, the caller should create a 
*** SWLogMessage object and pass this into the report method for processing.
***
***
*** @par Filtering log messages
***
*** SWLog supports 3 basic mechanisms for filtering log messages:
*** - log type mask
*** - log level
*** - log feature mask
***
*** Each log message has associated a message type which determines the type or
*** class of the message. The pre-defined types are:
***
*** - SW_LOG_TYPE_STDERR	- A message sent to stderr
*** - SW_LOG_TYPE_STDOUT	- A message sent to stdout
*** - SW_LOG_TYPE_ALWAYS	- A message to always be output, regardless of current log level
*** - SW_LOG_TYPE_FATAL		- A fatal message
*** - SW_LOG_TYPE_ERROR		- An error message
*** - SW_LOG_TYPE_WARNING	- A warning message
*** - SW_LOG_TYPE_INFO		- An informational message
*** - SW_LOG_TYPE_SUCCESS	- A message about a successful event
*** - SW_LOG_TYPE_NOTICE	- A message that indicates a non-error condition, but that may need special handling.
*** - SW_LOG_TYPE_CRITICAL	- A critical condition message (e.g. hard devices errors)
*** - SW_LOG_TYPE_EMERGENCY	- An emergency/panic condition message
*** - SW_LOG_TYPE_ALERT		- A message to indicate a situation that should corrected immediately
***
*** The application can set the SWLog object to filter out messages based on the message type
*** this is done by setting the log type mask (see SWLog::typeMask). All messages whose type
*** matches a bit in the SWLog type mask will be sent to handlers for processing.
***
*** In addition to the message type all messages have a log level, which is a numeric value
*** from 0-15. All messages with the log level that numerically less than
*** or equal to the current log level n the SWLog object will be reported. So if the log level is set to
*** SW_LOG_LEVEL_DETAIL, log messages with a log level of SW_LOG_LEVEL_DETAIL will be
*** sent to the handlers for processing. The file SWLogMessage.h contains definitions of log level names
*** (SW_LOG_LEVEL_XXXX) which give a pseudo name for the log level and its expected usage. Users are
*** not required to stick to these definitions, but they are provided to be useful for the majority of
*** applications.
***
*** Each log message also has a bit field (currently held in a 64-bit integer) which can be used
*** by the application to selectively filter messages. The filtering is performed by setting the
*** feature mask in the SWLog object. Only messages that successfully have a non-zero result when
*** and'd with the feature mask are sent to the message handlers. Messages with a zero feature code
*** are handled specially and always reported as long as the message type filtering does not remove them.
***
*** In order to always have a message sent to the log message handlers regardless of feature mask and
*** log level, an application should specify a message type of SW_LOG_TYPE_ALWAYS and a feature code of zero.
***
*** <b>Important:</b>
*** - It is important to note that log messages will only be sent to log handlers if all 3 conditions
***   (log level, log type mask and log feature mask) are matched.
*** - A log message with a type field of zero will bypass the type mask checks and will always be sent
***   to the log message handlers.
*** - A log message with a feature code of zero will bypass the feature mask checks and will always be sent
***   to the log message handlers.
***
***
*** @par Log message handling
***
*** Once the log message objects have been created they are handled by the attached 
*** message handlers. This happens in one of 2 modes. If the SWLog object is operating
*** in normal mode (bgMode() returns false) then one receipt of a log message, the message
*** is passed immediately to all the registered log message handlers. Control is then
*** to the caller when all handlers have processed the message.
***
*** Regardless of the logging mode being used the messages are passed to all of the registered
*** message handlers via the process method (see SWLogMessageHandler).
***
*** A SWLog object is derived from SWLogMessageHandler which means that a hierarchy of SWLog objects
*** can easily be created, each one potentially reporting different levels of log messages or selecting
*** different features and reporting them to their specified targets.
***
*** @author		Simon Sparkes
***
**/
class SWIFT_DLL_EXPORT SWLog : public SWLogMessageHandler
		{
		friend class SWLogThread;

public:
		/**
		*** @brief Constructor
		***
		*** The constructor will construct a SWLog object and then call the setHandler
		*** method with the specified handler string. If used in the default form (i.e.
		*** with a blank string) this will create a SWLog object with no attached log
		*** message handlers.
		***
		*** @param	handler	The parameter to pass to the setHandler method.
		**/
		SWLog(const SWString &handler="");

		/// Virtual destructor
		virtual ~SWLog();

		/**
		*** @brief Set the handlers
		***
		*** This method removes all the current handlers by calling the removeAllHandlers
		*** method and then parses the handler parameter, calling the addHandler method
		*** for each handler found.
		***
		*** @param	handler		A comma separated list of handlers to register.
		**/
		sw_status_t				setHandler(const SWString &handler);

		/**
		*** @brief Set the specified log message handler to be the only handler for this log object
		***
		*** This method removes all the current handlers by calling the removeAllHandlers
		*** method and then adds the handler specified by calling the second form of the addHandler
		*** method.
		***
		*** @param	pHandler	A pointer to the handler to register for log messages.
		*** @param	deleteFlag	A flag which when true indicates that this SWLog object is responsible
		***						for deleting the handler on removal.
		**/
		sw_status_t				setHandler(SWLogMessageHandler *pHandler, bool deleteFlag);

		/**
		*** @brief Add the specified log message handler to this log object.
		***
		*** This method adds the specified handler string as a registered handler
		*** for this log object. The handler parameter is a URL in the form scheme:parameters.
		*** The handler URL can take one of the following forms:
		***
		*** - stdout: -				Specifies a handler that writes it's output to stdout.
		*** - stderr: -				Specifies a handler that writes it's output to stderr.
		*** - syslog: -				Specifies a handler that writes it's output to the
		***							system log facility. (e.g. windows event viewer).
		*** @if use_log4cplus
		*** - log4cplus:cfg;tgt -	Specifies a handler that sends log messages to log4cplus.
		***							<i>cfg</i> specifies the log4cplus config file, <i>tgt</i>
		***							specifies the log4cplus target name.
		*** @endif
		*** - file:filename;maxsize;backupcount -
		***							Specifies a handler that writes its output to a file.
		***							<i>filename</i> specifies the filename to be opened.
		***							<i>maxsize</i> and <i>backupcount</i> are optional parameters
		***							which specify the maximum log file size and the number of backup
		***							copies to keep when the maximum size is reached. 
		***							<i>maxsize</i> and <i>backupcount</i> both default to zero (no
		***							maximum size, no backup copies).
		***
		*** @param	handler		A URL that specifies the handler to create.
		**/
		sw_status_t				addHandler(const SWString &handler);

		/**
		*** @brief Add the specified log message handler to this log object.
		***
		*** This method adds a handler that is derived from the SWLogMessageHandler class
		*** as a message handler for this SWLog object.
		***
		*** @param	pHandler	A pointer to the handler to register for log messages.
		*** @param	deleteFlag	A flag which when true indicates that this SWLog object is responsible
		***						for deleting the handler on removal.
		**/
		sw_status_t				addHandler(SWLogMessageHandler *pHandler, bool deleteFlag);

		/**
		*** @brief Remove the specified log message handler from this log object.
		***
		*** This method removes a handler that was previously added via the addHandler method.
		*** Any handlers that were marked to be deleted on removal are deleted.
		***
		*** @param	pHandler	A pointer to the handler to deregister for log messages.
		**/
		void				removeHandler(SWLogMessageHandler *pHandler);

		/**
		*** @brief Clear all the handlers from this logger
		***
		*** This method removes all the registered handlers from this SWLog object.
		*** Any handlers that were marked to be deleted on removal are deleted.
		**/
		void				removeAllHandlers();

		/**
		*** @brief Set the application name associated with this log
		**/
		void				application(const SWString &v)	{ flush(); m_appName = v; }

		/**
		*** @brief Get the application name associated with this log
		**/
		const SWString &	application() const				{ return m_appName; }

		
		/**
		*** @brief Set the mode of this logger
		***
		*** If the background mode is false (the default) and log messages are immediately
		*** passed to all the registered log message handlers. If the background mode is
		*** set to true the log messages are placed in a queue and are processed by a 
		*** separate thread. This will free up the main application thread to run it's code
		*** with logging having minimal impact.
		**/
		void				bgMode(bool v);

		/**
		*** @brief Get the mode of this logger
		**/
		bool				bgMode() const					{ return (m_pLogThread != NULL); }


		/**
		*** @brief Set the autoflush mode of this logger (not in background mode).
		***
		*** If the autoflush is set to true and background mode is false (the default)
		*** the flush method will be called after every message is processed. If background
		*** mode is true autoflush is ignored.
		**/
		void				autoflush(bool v)				{ m_autoflush = v; }

		/**
		*** @brief Get the mode of this logger
		**/
		bool				autoflush() const				{ return m_autoflush; }

/**
*** @name General Log Functions
***
*** The following methods are convienience functions to allow the caller 
*** to quickly log information using the SWLog class. The SWLogMessage
*** generated contains no file and line and has a feature code with all
*** bits set.
***
*** For better results use one of the SW_LOG_XXXX macros from SWLog.h 
*** which automatically fill in the file and line number information, or
*** use one of the other logging methods (log or process) which provide 
*** facilities for creating messages with all the SWLogMessage fields 
*** defined.
***/
//@{
		void		alert(const char *str, ...);			///< Log an alert message using the default log level for this message type.
		void		alert(const wchar_t *str, ...);		///< Log an alert message using the default log level for this message type.
		void		notice(const char *str, ...);			///< Log a notice message using the default log level for this message type.
		void		notice(const wchar_t *str, ...);		///< Log a notice message using the default log level for this message type.
		void		emergency(const char *str, ...);		///< Log a emergency message using the default log level for this message type.
		void		emergency(const wchar_t *str, ...);	///< Log a emergency message using the default log level for this message type.
		void		critical(const char *str, ...);			///< Log a critical message using the default log level for this message type.
		void		critical(const wchar_t *str, ...);	///< Log a critical message using the default log level for this message type.
		void		success(const char *str, ...);			///< Log a general success message using the default log level for this message type.
		void		success(const wchar_t *str, ...);	///< Log a general success message using the default log level for this message type.
		void		fatal(const char *str, ...);			///< Log a general fatal message using the default log level for this message type.
		void		fatal(const wchar_t *str, ...);		///< Log a general fatal message using the default log level for this message type.
		void		error(const char *str, ...);			///< Log a general error message using the default log level for this message type.
		void		error(const wchar_t *str, ...);		///< Log a general error message using the default log level for this message type.
		void		warning(const char *str, ...);			///< Log a general warning message using the default log level for this message type.
		void		warning(const wchar_t *str, ...);	///< Log a general warning message using the default log level for this message type.
		void		info(const char *str, ...);				///< Log a general info message using the default log level for this message type.
		void		info(const wchar_t *str, ...);		///< Log a general info message using the default log level for this message type.
		void		detail(const char *str, ...);			///< Log a general detail message using the default log level for this message type.
		void		detail(const wchar_t *str, ...);		///< Log a general detail message using the default log level for this message type.
		void		debug(const char *str, ...);			///< Log a general debug message using the default log level for this message type.
		void		debug(const wchar_t *str, ...);		///< Log a general debug message using the default log level for this message type.
		void		trace(const char *str, ...);			///< Log a general trace message using the default log level for this message type.
		void		trace(const wchar_t *str, ...);		///< Log a general trace message using the default log level for this message type.

		void		alert(sw_uint32_t level, const char *str, ...);			///< Log an alert message using the specified log level for this message type.
		void		alert(sw_uint32_t level, const wchar_t *str, ...);		///< Log an alert message using the specified log level for this message type.
		void		notice(sw_uint32_t level, const char *str, ...);			///< Log a notice message using the specified log level for this message type.
		void		notice(sw_uint32_t level, const wchar_t *str, ...);		///< Log a notice message using the specified log level for this message type.
		void		emergency(sw_uint32_t level, const char *str, ...);		///< Log a emergency message using the specified log level for this message type.
		void		emergency(sw_uint32_t level, const wchar_t *str, ...);	///< Log a emergency message using the specified log level for this message type.
		void		critical(sw_uint32_t level, const char *str, ...);			///< Log a critical message using the specified log level for this message type.
		void		critical(sw_uint32_t level, const wchar_t *str, ...);	///< Log a critical message using the specified log level for this message type.
		void		success(sw_uint32_t level, const char *str, ...);			///< Log a general success message using the specified log level for this message type.
		void		success(sw_uint32_t level, const wchar_t *str, ...);	///< Log a general success message using the specified log level for this message type.
		void		fatal(sw_uint32_t level, const char *str, ...);			///< Log a general fatal message using the specified log level for this message type.
		void		fatal(sw_uint32_t level, const wchar_t *str, ...);		///< Log a general fatal message using the specified log level for this message type.
		void		error(sw_uint32_t level, const char *str, ...);			///< Log a general error message using the specified log level for this message type.
		void		error(sw_uint32_t level, const wchar_t *str, ...);		///< Log a general error message using the specified log level for this message type.
		void		warning(sw_uint32_t level, const char *str, ...);			///< Log a general warning message using the specified log level for this message type.
		void		warning(sw_uint32_t level, const wchar_t *str, ...);	///< Log a general warning message using the specified log level for this message type.
		void		info(sw_uint32_t level, const char *str, ...);				///< Log a general info message using the specified log level for this message type.
		void		info(sw_uint32_t level, const wchar_t *str, ...);		///< Log a general info message using the specified log level for this message type.
		void		debug(sw_uint32_t level, const char *str, ...);			///< Log a general debug message using the specified log level for this message type.
		void		debug(sw_uint32_t level, const wchar_t *str, ...);		///< Log a general debug message using the specified log level for this message type.
//@}

		/**
		*** @brief	Log a message via a SWLogMessage object
		**/
		void		report(const SWLogMessage &msg);

		/**
		*** @brief	Log a message via parameters
		***
		*** This method creates a log message and passes it to the message handlers.
		*** Processing of the message is dependent on the message type, log level
		*** and feature codes matching the filters in the SWLog object.
		***
		*** @param[in]	msgflags	The message flags (normally a combination of a message type and a log level)
		***							as defiend in SWLogMessage.h
		*** @param[in]	category	A number representing the message category.
		*** @param[in]	file		The file name where the message originated.
		*** @param[in]	lineno		The line number where the message originated
		*** @param[in]	id			A numeric message ID - for language independent messages
		*** @param[in]	feature		A 64-bit feature code for masking groups of messages, e.g. by module.
		*** @param[in]	fmt			The printf-like format string.
		**/
		void		report(sw_uint32_t msgflags, sw_uint16_t category, const SWString &file, sw_int32_t lineno, sw_uint32_t id, sw_uint64_t feature, const char *fmt, ...);

		/**
		*** @brief	Log a message via parameters
		*** @see report(sw_uint32_t msgflags, sw_uint16_t category, const SWString &file, sw_int32_t lineno, sw_uint32_t id, sw_uint64_t feature, const char *fmt, ...)
		**/
		void		report(sw_uint32_t msgflags, sw_uint16_t category, const SWString &file, sw_int32_t lineno, sw_uint32_t id, sw_uint64_t feature, const wchar_t *fmt, ...);

public: // Virtual class overrides
		/**
		*** @brief Causes any stored messages to be flushed.
		***
		*** @note
		*** Called by a SWLog object when any messages need to be flushed to disk, etc
		*** when this object is a registered handler of another SWLog object
		**/
		virtual sw_status_t	flush();

protected: // Virtual class overrides

		// Called by a SWLog object when the handler has just been added
		virtual sw_status_t	open();
	
		// Called by a SWLog object when a message needs to be processed.
		virtual sw_status_t	process(const SWLogMessage &lm);

		// Called by a SWLog object when the handler is about to be removed from the SWLog object
		virtual sw_status_t	close();
	
protected: // METHODS
		void			sendMessageToHandlers(const SWLogMessage &msg);

protected:
		SWLogThread							*m_pLogThread;	///< The background log handler thread (only used in bg mode),
		SWList<SWLogMessageHandlerEntry>	m_handlers;		///< A list of log message handlers
		SWThreadMutex						m_mutex;		///< A mutex for thread-safeness
		SWString							m_appName;		///< The application name associated with this log object
		SWString							m_hostname;		///< The name of this host.
		sw_pid_t							m_pid;			///< The process ID
		bool								m_autoflush;	///< If true flush is called after every message is processed.
		};


#ifndef SW_LOG_CURRENT_FEATURE
/**
*** @def	SW_LOG_FEATURE
*** @brief	The default log feature for use with the SW_LOG_XXXX macros
***
*** The SW_LOG_FEATURE #define is used in conjunction with the SW_LOG_XXXX macros and
*** defines a number which is the feature code to be used for the generated log message.
*** A caller can define this for a module or section of code by first undefining it and
*** then re-defining it to the desired value.
***
*** The default value of SW_LOG_FEATURE is 0 which means that the message is not maskable
*** by feature code mask and will always be reported if the log level and type mask matches
*** succeed.
**/
	#define SW_LOG_FEATURE	0
#endif



/**
*** @name	General Log report macros
**/
//@{
#define SW_LOG_REPORT0(logobj, type, level)										logobj.report(type|level, 0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, "%s", msg)
#define SW_LOG_REPORT1(logobj, type, level, p1)									logobj.report(type|level, 0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1)
#define SW_LOG_REPORT2(logobj, type, level, p1, p2)								logobj.report(type|level, 0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2)
#define SW_LOG_REPORT3(logobj, type, level, p1, p2, p3)							logobj.report(type|level, 0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3)
#define SW_LOG_REPORT4(logobj, type, level, p1, p2, p3, p4)						logobj.report(type|level, 0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4)
#define SW_LOG_REPORT5(logobj, type, level, p1, p2, p3, p4, p5)					logobj.report(type|level, 0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5)
#define SW_LOG_REPORT6(logobj, type, level, p1, p2, p3, p4, p5, p6)				logobj.report(type|level, 0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6)
#define SW_LOG_REPORT7(logobj, type, level, p1, p2, p3, p4, p5, p6, p7)			logobj.report(type|level, 0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7)
#define SW_LOG_REPORT8(logobj, type, level, p1, p2, p3, p4, p5, p6, p7, p8)		logobj.report(type|level, 0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7, p8)
#define SW_LOG_REPORT9(logobj, type, level, p1, p2, p3, p4, p5, p6, p7, p8, p9)	logobj.report(type|level, 0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7, p8, p9)
//@}


/**
*** @name	General Log Functions (MACROS - 0 params)
**/
//@{
#define SW_LOG_SUCCESS0(logobj, msg)										logobj.report(SW_LOG_MSG_SUCCESS,  0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, "%s", msg)
#define SW_LOG_FATAL0(logobj, msg)											logobj.report(SW_LOG_MSG_FATAL,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, "%s", msg)
#define SW_LOG_ERROR0(logobj, msg)											logobj.report(SW_LOG_MSG_ERROR,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, "%s", msg)
#define SW_LOG_WARNING0(logobj, msg)										logobj.report(SW_LOG_MSG_WARNING, 0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, "%s", msg)
#define SW_LOG_INFO0(logobj, msg)											logobj.report(SW_LOG_MSG_INFO,    0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, "%s", msg)
#define SW_LOG_DETAIL0(logobj, msg)											logobj.report(SW_LOG_MSG_DETAIL,  0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, "%s", msg)
#define SW_LOG_DEBUG0(logobj, msg)											logobj.report(SW_LOG_MSG_DEBUG,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, "%s", msg)
#define SW_LOG_TRACE0(logobj, msg)											logobj.report(SW_LOG_MSG_TRACE,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, "%s", msg)
//@}

/**
*** @name	General Log Functions (MACROS - 1 params)
**/
//@{
#define SW_LOG_SUCCESS1(logobj, msg, p1)									logobj.report(SW_LOG_MSG_SUCCESS,  0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1)
#define SW_LOG_FATAL1(logobj, msg, p1)										logobj.report(SW_LOG_MSG_FATAL,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1)
#define SW_LOG_ERROR1(logobj, msg, p1)										logobj.report(SW_LOG_MSG_ERROR,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1)
#define SW_LOG_WARNING1(logobj, msg, p1)									logobj.report(SW_LOG_MSG_WARNING, 0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1)
#define SW_LOG_INFO1(logobj, msg, p1)										logobj.report(SW_LOG_MSG_INFO,    0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1)
#define SW_LOG_DETAIL1(logobj, msg, p1)										logobj.report(SW_LOG_MSG_DETAIL,  0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1)
#define SW_LOG_DEBUG1(logobj, msg, p1)										logobj.report(SW_LOG_MSG_DEBUG,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1)
#define SW_LOG_TRACE1(logobj, msg, p1)										logobj.report(SW_LOG_MSG_TRACE,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1)
//@}

/**
*** @name	General Log Functions (MACROS - 2 params)
**/
//@{
#define SW_LOG_SUCCESS2(logobj, msg, p1, p2)								logobj.report(SW_LOG_MSG_SUCCESS,  0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2)
#define SW_LOG_FATAL2(logobj, msg, p1, p2)									logobj.report(SW_LOG_MSG_FATAL,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2)
#define SW_LOG_ERROR2(logobj, msg, p1, p2)									logobj.report(SW_LOG_MSG_ERROR,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2)
#define SW_LOG_WARNING2(logobj, msg, p1, p2)								logobj.report(SW_LOG_MSG_WARNING, 0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2)
#define SW_LOG_INFO2(logobj, msg, p1, p2)									logobj.report(SW_LOG_MSG_INFO,    0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2)
#define SW_LOG_DETAIL2(logobj, msg, p1, p2)									logobj.report(SW_LOG_MSG_DETAIL,  0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2)
#define SW_LOG_DEBUG2(logobj, msg, p1, p2)									logobj.report(SW_LOG_MSG_DEBUG,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2)
#define SW_LOG_TRACE2(logobj, msg, p1, p2)									logobj.report(SW_LOG_MSG_TRACE,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2)
//@}

/**
*** @name	General Log Functions (MACROS - 3 params)
**/
//@{
#define SW_LOG_SUCCESS3(logobj, msg, p1, p2, p3)							logobj.report(SW_LOG_MSG_SUCCESS,  0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3)
#define SW_LOG_FATAL3(logobj, msg, p1, p2, p3)								logobj.report(SW_LOG_MSG_FATAL,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3)
#define SW_LOG_ERROR3(logobj, msg, p1, p2, p3)								logobj.report(SW_LOG_MSG_ERROR,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3)
#define SW_LOG_WARNING3(logobj, msg, p1, p2, p3)							logobj.report(SW_LOG_MSG_WARNING, 0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3)
#define SW_LOG_INFO3(logobj, msg, p1, p2, p3)								logobj.report(SW_LOG_MSG_INFO,    0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3)
#define SW_LOG_DETAIL3(logobj, msg, p1, p2, p3)								logobj.report(SW_LOG_MSG_DETAIL,  0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3)
#define SW_LOG_DEBUG3(logobj, msg, p1, p2, p3)								logobj.report(SW_LOG_MSG_DEBUG,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3)
#define SW_LOG_TRACE3(logobj, msg, p1, p2, p3)								logobj.report(SW_LOG_MSG_TRACE,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3)
//@}

/**
*** @name	General Log Functions (MACROS - 4 params)
**/
//@{
#define SW_LOG_SUCCESS4(logobj, msg, p1, p2, p3, p4)						logobj.report(SW_LOG_MSG_SUCCESS,  0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4)
#define SW_LOG_FATAL4(logobj, msg, p1, p2, p3, p4)							logobj.report(SW_LOG_MSG_FATAL,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4)
#define SW_LOG_ERROR4(logobj, msg, p1, p2, p3, p4)							logobj.report(SW_LOG_MSG_ERROR,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4)
#define SW_LOG_WARNING4(logobj, msg, p1, p2, p3, p4)						logobj.report(SW_LOG_MSG_WARNING, 0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4)
#define SW_LOG_INFO4(logobj, msg, p1, p2, p3, p4)							logobj.report(SW_LOG_MSG_INFO,    0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4)
#define SW_LOG_DETAIL4(logobj, msg, p1, p2, p3, p4)							logobj.report(SW_LOG_MSG_DETAIL,  0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4)
#define SW_LOG_DEBUG4(logobj, msg, p1, p2, p3, p4)							logobj.report(SW_LOG_MSG_DEBUG,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4)
#define SW_LOG_TRACE4(logobj, msg, p1, p2, p3, p4)							logobj.report(SW_LOG_MSG_TRACE,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4)
//@}

/**
*** @name	General Log Functions (MACROS - 5 params)
**/
//@{
#define SW_LOG_SUCCESS5(logobj, msg, p1, p2, p3, p4, p5)					logobj.report(SW_LOG_MSG_SUCCESS,  0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5)
#define SW_LOG_FATAL5(logobj, msg, p1, p2, p3, p4, p5)						logobj.report(SW_LOG_MSG_FATAL,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5)
#define SW_LOG_ERROR5(logobj, msg, p1, p2, p3, p4, p5)						logobj.report(SW_LOG_MSG_ERROR,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5)
#define SW_LOG_WARNING5(logobj, msg, p1, p2, p3, p4, p5)					logobj.report(SW_LOG_MSG_WARNING, 0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5)
#define SW_LOG_INFO5(logobj, msg, p1, p2, p3, p4, p5)						logobj.report(SW_LOG_MSG_INFO,    0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5)
#define SW_LOG_DETAIL5(logobj, msg, p1, p2, p3, p4, p5)						logobj.report(SW_LOG_MSG_DETAIL,  0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5)
#define SW_LOG_DEBUG5(logobj, msg, p1, p2, p3, p4, p5)						logobj.report(SW_LOG_MSG_DEBUG,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5)
#define SW_LOG_TRACE5(logobj, msg, p1, p2, p3, p4, p5)						logobj.report(SW_LOG_MSG_TRACE,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5)
//@}

/**
*** @name	General Log Functions (MACROS - 6 params)
**/
//@{
#define SW_LOG_SUCCESS6(logobj, msg, p1, p2, p3, p4, p5, p6)				logobj.report(SW_LOG_MSG_SUCCESS,  0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6)
#define SW_LOG_FATAL6(logobj, msg, p1, p2, p3, p4, p5, p6)					logobj.report(SW_LOG_MSG_FATAL,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6)
#define SW_LOG_ERROR6(logobj, msg, p1, p2, p3, p4, p5, p6)					logobj.report(SW_LOG_MSG_ERROR,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6)
#define SW_LOG_WARNING6(logobj, msg, p1, p2, p3, p4, p5, p6)				logobj.report(SW_LOG_MSG_WARNING, 0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6)
#define SW_LOG_INFO6(logobj, msg, p1, p2, p3, p4, p5, p6)					logobj.report(SW_LOG_MSG_INFO,    0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6)
#define SW_LOG_DETAIL6(logobj, msg, p1, p2, p3, p4, p5, p6)					logobj.report(SW_LOG_MSG_DETAIL,  0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6)
#define SW_LOG_DEBUG6(logobj, msg, p1, p2, p3, p4, p5, p6)					logobj.report(SW_LOG_MSG_DEBUG,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6)
#define SW_LOG_TRACE6(logobj, msg, p1, p2, p3, p4, p5, p6)					logobj.report(SW_LOG_MSG_TRACE,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6)
//@}

/**
*** @name	General Log Functions (MACROS - 7 params)
**/
//@{
#define SW_LOG_SUCCESS7(logobj, msg, p1, p2, p3, p4, p5, p6, p7)			logobj.report(SW_LOG_MSG_SUCCESS,  0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7)
#define SW_LOG_FATAL7(logobj, msg, p1, p2, p3, p4, p5, p6, p7)				logobj.report(SW_LOG_MSG_FATAL,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7)
#define SW_LOG_ERROR7(logobj, msg, p1, p2, p3, p4, p5, p6, p7)				logobj.report(SW_LOG_MSG_ERROR,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7)
#define SW_LOG_WARNING7(logobj, msg, p1, p2, p3, p4, p5, p6, p7)			logobj.report(SW_LOG_MSG_WARNING, 0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7)
#define SW_LOG_INFO7(logobj, msg, p1, p2, p3, p4, p5, p6, p7)				logobj.report(SW_LOG_MSG_INFO,    0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7)
#define SW_LOG_DETAIL7(logobj, msg, p1, p2, p3, p4, p5, p6, p7)				logobj.report(SW_LOG_MSG_DETAIL,  0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7)
#define SW_LOG_DEBUG7(logobj, msg, p1, p2, p3, p4, p5, p6, p7)				logobj.report(SW_LOG_MSG_DEBUG,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7)
#define SW_LOG_TRACE7(logobj, msg, p1, p2, p3, p4, p5, p6, p7)				logobj.report(SW_LOG_MSG_TRACE,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7)
//@}

/**
*** @name	General Log Functions (MACROS - 8 params)
**/
//@{
#define SW_LOG_SUCCESS8(logobj, msg, p1, p2, p3, p4, p5, p6, p7, p8)		logobj.report(SW_LOG_MSG_SUCCESS,  0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7, p8)
#define SW_LOG_FATAL8(logobj, msg, p1, p2, p3, p4, p5, p6, p7, p8)			logobj.report(SW_LOG_MSG_FATAL,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7, p8)
#define SW_LOG_ERROR8(logobj, msg, p1, p2, p3, p4, p5, p6, p7, p8)			logobj.report(SW_LOG_MSG_ERROR,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7, p8)
#define SW_LOG_WARNING8(logobj, msg, p1, p2, p3, p4, p5, p6, p7, p8)		logobj.report(SW_LOG_MSG_WARNING, 0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7, p8)
#define SW_LOG_INFO8(logobj, msg, p1, p2, p3, p4, p5, p6, p7, p8)			logobj.report(SW_LOG_MSG_INFO,    0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7, p8)
#define SW_LOG_DETAIL8(logobj, msg, p1, p2, p3, p4, p5, p6, p7, p8)			logobj.report(SW_LOG_MSG_DETAIL,  0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7, p8)
#define SW_LOG_DEBUG8(logobj, msg, p1, p2, p3, p4, p5, p6, p7, p8)			logobj.report(SW_LOG_MSG_DEBUG,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7, p8)
#define SW_LOG_TRACE8(logobj, msg, p1, p2, p3, p4, p5, p6, p7, p8)			logobj.report(SW_LOG_MSG_TRACE,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7, p8)
//@}

/**
*** @name	General Log Functions (MACROS - 9 params)
**/
//@{
#define SW_LOG_SUCCESS9(logobj, msg, p1, p2, p3, p4, p5, p6, p7, p8, p9)	logobj.report(SW_LOG_MSG_SUCCESS,  0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7, p8, p9)
#define SW_LOG_FATAL9(logobj, msg, p1, p2, p3, p4, p5, p6, p7, p8, p9)		logobj.report(SW_LOG_MSG_FATAL,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7, p8, p9)
#define SW_LOG_ERROR9(logobj, msg, p1, p2, p3, p4, p5, p6, p7, p8, p9)		logobj.report(SW_LOG_MSG_ERROR,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7, p8, p9)
#define SW_LOG_WARNING9(logobj, msg, p1, p2, p3, p4, p5, p6, p7, p8, p9)	logobj.report(SW_LOG_MSG_WARNING, 0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7, p8, p9)
#define SW_LOG_INFO9(logobj, msg, p1, p2, p3, p4, p5, p6, p7, p8, p9)		logobj.report(SW_LOG_MSG_INFO,    0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7, p8, p9)
#define SW_LOG_DETAIL9(logobj, msg, p1, p2, p3, p4, p5, p6, p7, p8, p9)		logobj.report(SW_LOG_MSG_DETAIL,  0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7, p8, p9)
#define SW_LOG_DEBUG9(logobj, msg, p1, p2, p3, p4, p5, p6, p7, p8, p9)		logobj.report(SW_LOG_MSG_DEBUG,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7, p8, p9)
#define SW_LOG_TRACE9(logobj, msg, p1, p2, p3, p4, p5, p6, p7, p8, p9)		logobj.report(SW_LOG_MSG_TRACE,   0, __FILE__, __LINE__, 0, SW_LOG_FEATURE, msg, p1, p2, p3, p4, p5, p6, p7, p8, p9)
//@}


/**
*** @example log_simple.cpp
***
*** An example of using the SWLog interface in the simplest manner.
**/

/**
*** @example log_macro.cpp
***
*** An example of using the SWLog interface using the macros to fill in the
*** file and line information.
**/

/**
*** @example log_report.cpp
***
*** An example of using the SWLog interface using the report method to allow
*** all log message parameters to be specified precisely.
**/

/**
*** @example log_multi.cpp
***
*** An example of using the SWLog interface with multiple log handlers (targets)
*** each one using filtering to get a different set of messages.
**/




#endif
