/**
*** @file		SWOrderedList.h
*** @brief		A templated, ordered, double-linked list.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWOrderedList class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWOrderedList_h__
#define __SWOrderedList_h__

#include <swift/SWGuard.h>
#include <swift/SWIterator.h>
#include <swift/SWList.h>



// Forward Declarations
class SWMemoryManager;
template<class T> class SWOrderedList;
template<class T> class SWOrderedListIterator;


// Forward Declarations

/**
*** @brief	Templated form of the SWOrderedList class.
***
*** SWOrderedList is a templated version of the SWOrderedList class.
*** The primary difference between a SWOrderedList and a SWOrderedList object is that the
*** templated SWOrderedList will correctly handle the SWCollection delete flag.
***
*** @par General Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Safe when synchronized</td>
*** </tr>
*** </table>
***
*** @par Collection Attributes
*** <table class="collection">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>Internal Structure</td>
***		<td>Double-linked list</td>
*** </tr>
*** <tr>
***		<td>Stored Data Type</td>
***		<td>Templated Data</td>
*** </tr>
*** <tr>
***		<td>Allows duplicate values?</td>
***		<td>Yes</td>
*** </tr>
*** <tr>
***		<td>Ordered?</td>
***		<td>Yes</td>
*** </tr>
*** <tr>
***		<td>Sortable?</td>
***		<td>No</td>
*** </tr>
*** <tr>
***		<td>Requirements for stored type</td>
***		<td>
***			<ul>
***			<li>Copy constructor
***			<li>operator=
***			<li>operator==
***			<li>operator<
***			</ul>
***		</td>
*** </tr>
*** </table>
***
***
*** @see		SWOrderedList, SWCollection
*** @author		Simon Sparkes
**/
template<class T>
class SWOrderedList : public SWCollection
		{
friend class SWOrderedListIterator<T>;

public:
		/// Position enumeration
		enum ListPosition
			{
			ListHead,
			ListTail
			};


public:
		/// Default constructor - empty list
		SWOrderedList();

		/// Virtual destructor
		virtual ~SWOrderedList();


		/**
		*** Clears the list
		**/
		virtual void	clear();


		/**
		*** @brief Adds a copy of the given data to the list.
		***
		*** The copy of the specified element is made using the copy constructor
		*** of the element. The insert will only succeed if the delete flag is set
		*** to true. If the delete flag is false this method will do nothing and
		*** return false;
		***
		*** @param	data	The data element to copy and add.
		***
		*** @return	true if successful, false otherwise.
		**/
		void			add(const T &data);
		
		
		/**
		*** @brief	Returns a pointer to the data at the head of the list
		**/
		bool			get(ListPosition pos, T &data) const;

		/// Get list head (alternate name)
		bool			getHead(T &data) const;

		/// Get list tail (alternate name)
		bool			getTail(T &data) const;


		/**
		*** @brief	Removes and returns the first element from this list.
		**/
		bool			remove(ListPosition pos, T *pData=NULL);

		/// Remove list head (alternate name)
		bool			removeHead(T *pData=NULL);

		/// Remove list tail (alternate name)
		bool			removeTail(T *pData=NULL);


		/**
		*** @brief	Return true if the list contains the data specified (tested using operator==).
		**/
		bool			contains(const T &data) const;


		/**
		*** @brief	Find and return the data that matches the search criteria
		**/
		bool			find(const T &search, T &data) const;


		/**
		*** @brief	Removes the first occurrence of the specified data (tested using operator==).
		**/
		bool			remove(const T &search, T *pData=NULL);


		/**
		*** @brief	Returns an iterator over the elements of this collection.
		***
		*** @param	iterateFromEnd	If false (default) the iterator starts
		***							at the beginning of the vector. If true
		***							the iterator starts at the end of the
		***							vector.
		**/
		SWIterator<T>		iterator(bool iterateFromEnd=false) const;



protected:
		/// Get an iterator initialised to start at the beginning or end as specified.
		virtual SWCollectionIterator *	getIterator(bool iterateFromEnd=false) const;

		/// Compare the specified data elements
		virtual int						compareData(const void *pData1, const void *pData2) const;

		/// Remove the specified element
		void							removeElement(SWListElement<T> *pElement);

		/// Add the specified element
		void							addElement(SWListElement<T> *pElement, bool addToHead, sw_collection_compareDataFunction *pCompareFunc);

protected:
		SWMemoryManager		*m_pMemoryManager;	///< The memory manager
		SWListElement<T>	*m_head;			///< A pointer to the start if the list
		SWListElement<T>	*m_tail;			///< Pointer to the end of the list
		sw_size_t			m_elemsize;			///< The size of each element to be stored
		};



// Include the implementation
#if defined(SW_TEMPLATES_REQUIRE_SOURCE)
	#include <swift/SWOrderedList.cpp>
#elif defined(SW_TEMPLATES_REQUIRE_PRAGMA)
	#pragma implementation("swift/SWOrderedList.cpp")
#else
	#error No implementation
#endif

#endif
