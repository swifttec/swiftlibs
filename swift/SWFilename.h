/**
*** @file		SWFilename.h
*** @brief		A filename manipulation class.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWFilename class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




#ifndef __SWFilename_h__
#define __SWFilename_h__

#include <swift/swift.h>
#include <swift/SWObject.h>
#include <swift/SWException.h>
#include <swift/SWString.h>
#include <swift/SWStringArray.h>


#if defined(SW_PLATFORM_WINDOWS)
	#define SW_PATH_SEPARATOR			'\\'
	#define SW_PATH_SEPARATOR_STRING	"\\"
#else
	#define SW_PATH_SEPARATOR			'/'
	#define SW_PATH_SEPARATOR_STRING	"/"
#endif


/**
*** @brief	An object and methods to represent and manipulate file name.
***
*** This class provides a simple and portable mechanism for handling file names.
*** Methods are provided for concatenating and splitting paths which understand
*** the various platform specific paths. This class also provides a mechanism 
*** for converting to and from various path types (current Unix and Windows).
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWFilename : public SWObject
		{
public:
		/// An enumeration of basic file handle types
		enum PathType
			{
			PathTypeNoChange=0,	///< Path separator should not be changed
			PathTypePlatform=1,	///< Platform specific path
			PathTypeUnix=2,		///< Path using unix separators - i.e. forward slash
			PathTypeWindows=3	///< Path using windows separators - i.e. backslash
			};

public:
		/// Default constructor
		SWFilename(const SWString &path="", PathType pt=PathTypeNoChange);

		/// Virtual destructor
		virtual ~SWFilename();

		/// Copy constructor.
		SWFilename(const SWFilename &other);

		/// Assignment operator - SWFilename
		SWFilename &	operator=(const SWFilename &other);

		/// Assignment operator - SWString
		SWFilename &	operator=(const SWString &other);


		/// Returns true if the path is a relative path. Blank paths always return false.
		bool				isRelativePath() const;

		/// Returns true if the path is a absolute path (e.g. starts with /). Blank paths always return false.
		bool				isAbsolutePath() const;

		/// Returns the file extension (e.g. x.cpp) will return cpp
		SWString			extension() const;

		/// Returns the directory part of a filename (e.g. /a/b/c -> /a/b)
		SWString			dirname() const;

		/// Returns the base part of a filename (e.g. /a/b/c -> c)
		SWString			basename() const;

		/// Returns the drive part of a filename (e.g. x:/a/b/c -> x)
		SWString			drivename() const;

		/// Returns the share part of a filename.
		SWString			sharename() const;

		/// Returnd the base and extension of the filename.
		void				split(SWString &base, SWString &extension) const;

		/// Returns the type of this path
		PathType			type() const	{ return m_type; }

		/// Returns the path of this filename
		const SWString &	path() const	{ return m_path; }


public: // Comparison operators

		/**
		*** @brief	Compare this object with another.
		***
		*** This method compares this object with the one specified and returns a
		*** result indicating if the other object is less than, equal to, or greater
		*** than this object.
		***
		*** - If this object is less than the specified object then a negative result is returned.
		*** - If this object is greater than the specified object then a positive result is returned.
		*** - If this object is equal to the specified object then a zero result is returned.
		***
		*** @note
		*** The compareTo method is not required to return -1 and +1 for the negative and positive
		*** values although this is typically the value returned. Callers should test for \< 0 and \> 0
		*** rather than for specific values of -1 and +1.
		**/
		int			compareTo(const SWFilename &other) const;

		/**
		*** @brief	Test for equality
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is equal to the other object, false otherwise.
		**/
		bool	operator==(const SWFilename &other) const	{ return (compareTo(other) == 0); }

		/**
		*** @brief	Test for inequality
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is not equal to the other object, false otherwise.
		**/
		bool	operator!=(const SWFilename &other) const	{ return (compareTo(other) != 0); }

		/**
		*** @brief	Test for less than.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is less than the other object, false otherwise.
		**/
		bool	operator< (const SWFilename &other) const	{ return (compareTo(other) <  0); }

		/**
		*** @brief	Test for greater than.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is greater than the other object, false otherwise.
		**/
		bool	operator> (const SWFilename &other) const	{ return (compareTo(other) >  0); }

		/**
		*** @brief	Test for less than or equal to.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is less than or equal to the other object, false otherwise.
		**/
		bool	operator<=(const SWFilename &other) const	{ return (compareTo(other) <= 0); }

		/**
		*** @brief	Test for greater than or equal to.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is greater than or equal to the other object, false otherwise.
		**/
		bool	operator>=(const SWFilename &other) const	{ return (compareTo(other) >= 0); }


public: // Virtual overrides

		/// Override for SWObject writeToStream
		virtual sw_status_t		writeToStream(SWOutputStream &stream) const;

		/// Override for SWObject readFromStream
		virtual sw_status_t		readFromStream(SWInputStream &stream);


public: // STATIC Methods

		/**
		*** Fixes a filename to ensure that it is valid and contains no extra components
		*** and all the separators are correct.
		***
		*** For the windows platform:
		***	C:    		-> C:\\<br>
		***	C:\\xxx\\	-> C:\\xxx
		***
		*** For the Unix platforms
		***	/a/	-> /a<br>
		***	/a//b	-> /a/b
		***
		*** If the pathtype is anything other than PathTypeNoChange all path
		*** separators are changed to the type specified. If the pathtype is 
		*** PathTypeNoChange (the default) the separators in the filename are
		*** left unchanged.
		***
		*** @param	path	The pathname to fix.
		*** @param	type	The type of path separator to use.
		**/
		static SWString		fix(const SWString &path, PathType type=PathTypeNoChange);

		/**
		*** Concatenate 2 paths.
		*** Assumes that 2nd path is relative, if not results are unpredicatable!
		***
		*** This function will handle both dos and unix paths. Specify a type
		*** if a specific concatenation is required, otherwise the default
		*** is to use the platform default. (Unix=/, Dos=\)
		***
		*** @param path1	The first path
		*** @param path2	The path to append
		*** @param ptype	The path type
		**/
		static SWString		concatPaths(const SWString &path1, const SWString &path2, PathType ptype=PathTypePlatform);

		/// Returns true if the path is a relative path. Blank paths always return false.
		static bool			isRelativePath(const SWString &path);

		/// Returns true if the path is a absolute path (e.g. starts with /). Blank paths always return false.
		static bool			isAbsolutePath(const SWString &path);

		/**
		*** Return the extension of the given file name.
		*** 
		*** The extension is any characters after (but not including) the last period
		*** of the filename.
		***
		*** e.g.	fred.c		-> extension=c
		***			fred		-> extension=
		***
		*** If the filename includes a directory part this will be ignored.
		***
		*** e.g.	a/fred.c	-> extension=c
		**/
		static SWString		extension(const SWString &file);

		/**
		*** Returns the directory component of a path.
		***
		*** The dirname method takes a pointer to a character string
		*** that  contains a pathname, and returns a pointer to a string
		*** that is a pathname of the parent directory of that file.
		*** Trailing '/' characters in the path are not counted as part
		*** of the path.
		*** 
		*** If path does not contain a '/',  then  dirname()  returns  a
		*** pointer  to  the  string "." .  If path is a null pointer or
		*** points to an empty string, dirname() returns  a  pointer  to
		*** the string "." .
		***
		*** <b>Addition to Dos/Windows version</b><br>
		*** If the path is a DOS drive root path (e.g x:\) this is returned
		**/
		static SWString		dirname(const SWString &file);

		/**
		*** Returns the last component of a path (normally filename)
		*** like the standard basename, except that we return a SWString
		*** 
		*** Note that this function will parse both DOS and Unix format
		*** filenames.
		**/
		static SWString		basename(const SWString &file, bool stripextension=false);
		
		/**
		*** This function returns the drive component of a path.
		*** This only makes sense in windows/dos, but left for compatibiliy reasons
		*** and will return an empty string under Unix.
		**/
		static SWString		drivename(const SWString &file);

		/**
		*** This function returns the share component of a path
		*** This only makes sense in windows/dos, but left for compatibiliy reasons
		*** and returns a blank in a non-dos environment.
		**/
		static SWString		sharename(const SWString &file);

		/**
		*** @brief	Compare 2 paths
		**/
		static int			comparePaths(const SWString &path1, const SWString &path2, bool ignoreCase);

		/**
		*** Split a path into it's component parts
		**/
		static sw_size_t	splitPath(const SWString &path, SWStringArray &sa);

		/**
		*** This function splits the given file name into its base and extension.
		*** 
		*** The extension is any characters after (but not including) the last period
		*** of the filename. The base consists of all characters before
		*** (but not including) the last period. If not period appears in the base will
		*** be the same as the filename, and the extension will be blank.
		***
		*** e.g.	fred.c		-> base=fred	extension=c
		***			fred		-> base=fred	extension=
		***
		*** If the filename includes a directory part this will be ignored.
		***
		*** e.g.	a/fred.c	-> base=fred	extension=c
		**/
		static void			split(const SWString &file, SWString &base, SWString &extension);


		/**
		*** This function splits the given file name into its base and extension.
		*** 
		*** The extension is any characters after (but not including) the last period
		*** of the filename. The base consists of all characters before
		*** (but not including) the last period. If not period appears in the base will
		*** be the same as the filename, and the extension will be blank.
		***
		*** e.g.	fred.c		-> base=fred	extension=c
		***			fred		-> base=fred	extension=
		***
		*** If the filename includes a directory part this will be ignored.
		***
		*** e.g.	a/fred.c	-> base=fred	extension=c
		**/
		static void			split(const SWString &file, SWString &directory, SWString &base, SWString &extension);


		/**
		*** Optimize a path by reducing duplicate path separators
		*** . and .. where possible
		**/
		static SWString		optimizePath(const SWString &path, PathType type);

		/**
		*** Joins path components to make a path
		**/
		static SWString		joinPath(SWStringArray &sa, PathType type);

		/**
		*** Returns the absolute path name for the given path
		*** in relation to the specified directory (default current)
		***
		*** If the neither the directory nor the path passed to this function are absolute
		*** it is impossible to generate an absolute path and the result will be the same
		*** as calling concatPaths.
		***
		*** @param path		The path name
		*** @param dir		The directory name used to generate a relative path
		*** @param type		The path type
		**/
		static SWString		absolutePath(const SWString &path, const SWString &dir, PathType type);


		/**
		*** Returns the relative path name for the given path
		*** in relation to the specified directory (default .)
		***
		*** There are certain conditions when it is impossible to calculate a relative
		*** path. These are described below:
		***
		*** When the path is absolute and the directory is relative it is impossible
		*** to determine the relative path between them. Under these circumstances the
		*** orginal path is returned.
		***
		*** When the path and the directory are on different physical drives (e.g. c: and d:)
		*** there is no relative path between them. Under these circumstances the
		*** orginal path is returned.
		***
		*** @param p		The path name (can be absolute)
		*** @param d		The directory name used to generate an absolute path
		*** @param type		The path type
		**/
		static SWString		relativePath(const SWString &p, const SWString &d, PathType type);

private:	 // Members
		PathType	m_type;		///< The type of path
		SWString	m_path;		///< The path string
		};





#endif
