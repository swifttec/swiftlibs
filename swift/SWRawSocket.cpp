/**
*** @file		SWRawSocket.cpp
*** @brief		A raw socket
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWRawSocket class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




// Needed for precompiled header support
#include "stdafx.h"


#include <swift/SWGuard.h>

#include <swift/SWRawSocket.h>
#include <swift/sw_net.h>


#if !defined(SW_PLATFORM_WINDOWS)
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <arpa/inet.h>
	#if defined(SW_PLATFORM_MACH)
		// These options are not supported, but allows compilation (copied from linux)
		#define AF_PACKET	17
		#define ETH_P_ALL	3
	#else
		#include <netpacket/packet.h>
		#include <linux/if_ether.h>
	#endif
#else
	#if defined(SW_BUILD_MFC)
		#include <winsock2.h>
	#endif

	// These options are not supported, but allows compilation (copied from linux)
	#define AF_PACKET	17
	#define ETH_P_ALL	3
#endif


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





SWRawSocket::SWRawSocket() :
    m_family(AF_PACKET),
	m_protocol(htons(ETH_P_ALL))
		{
		}


SWRawSocket::SWRawSocket(int family, int protocol) :
    m_family(family),
	m_protocol(protocol)
		{
		}

SWRawSocket::~SWRawSocket()
		{
		close();
		}


sw_status_t
SWRawSocket::close()
		{
		SWGuard		guard(this);
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (_fd != SW_NET_INVALID_SOCKET)
			{
			res = SWSocket::close(_fd);
			_fd = SW_NET_INVALID_SOCKET;
			_flags.clear();
			}
		
		return setLastError(res);
		}




sw_status_t
SWRawSocket::open()
		{
		SWGuard		guard(this);

		// Attempt to create the socket
		return setLastError(SWSocket::open(m_family, SOCK_RAW, m_protocol, _fd));
		}




sw_status_t
SWRawSocket::receiveFrom(SWSocketAddress &addr, void *pBuffer, sw_size_t bytesToRead, sw_size_t *pBytesRead)
		{
		SWGuard		guard(this);
		sw_status_t	res=SW_STATUS_SUCCESS;

		res = SWSocket::recvfrom(_fd, addr, pBuffer, bytesToRead, pBytesRead);

		return setLastError(res);
		}


sw_status_t
SWRawSocket::receive(void *pBuffer, sw_size_t bytesToRead, sw_size_t *pBytesRead)
		{
		return read(pBuffer, bytesToRead, pBytesRead);
		}


sw_status_t
SWRawSocket::read(void *pBuffer, sw_size_t bytesToRead, sw_size_t *pBytesRead)
		{
		SWGuard		guard(this);
		sw_status_t	res=SW_STATUS_SUCCESS;

#if defined(SW_PLATFORM_WINDOWS) || defined(SW_PLATFORM_MACH)
		res = SW_STATUS_UNSUPPORTED_OPERATION;
#else
		if (pBytesRead != NULL) *pBytesRead = 0;

		int		r;

		r = ::read(_fd, pBuffer, bytesToRead);
		if (r > 0)
			{
			if (pBytesRead != NULL) *pBytesRead = r;
			}
		else if (r < 0)
			{
			res = sw_net_getLastError();
			}
#endif

		return setLastError(res);
		}


sw_status_t
SWRawSocket::addMembership(int index)
		{
		sw_status_t		res=SW_STATUS_SUCCESS;

#if defined(SW_PLATFORM_WINDOWS) || defined(SW_PLATFORM_MACH)
		res = SW_STATUS_UNSUPPORTED_OPERATION;
#else
		struct packet_mreq	mr;

		memset(&mr, 0, sizeof(mr));
		mr.mr_ifindex = index;
		mr.mr_type = PACKET_MR_PROMISC;
		setsockopt(_fd, SOL_PACKET, PACKET_DROP_MEMBERSHIP, &mr, sizeof(mr));

		if (setsockopt(_fd, SOL_PACKET, PACKET_ADD_MEMBERSHIP, &mr, sizeof(mr)) < 0) res = errno;
#endif		
		
		return res;
		}


sw_status_t
SWRawSocket::dropMembership(int index)
		{
		sw_status_t		res=SW_STATUS_SUCCESS;

#if defined(SW_PLATFORM_WINDOWS) || defined(SW_PLATFORM_MACH)
		res = SW_STATUS_UNSUPPORTED_OPERATION;
#else
		struct packet_mreq	mr;

		memset(&mr, 0, sizeof(mr));
		mr.mr_ifindex = index;
		mr.mr_type = PACKET_MR_PROMISC;

		if (setsockopt(_fd, SOL_PACKET, PACKET_DROP_MEMBERSHIP, &mr, sizeof(mr)) < 0) res = errno;
#endif		
		
		return res;
		}




