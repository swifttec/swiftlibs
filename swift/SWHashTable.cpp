/**
*** @file		SWHashTable.cpp
*** @brief		A simple hashtable template class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWHashTable class.
**/

/*********************************************************************************
Copyright (c) 2006-2016 SwiftTec Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __base_SWHashTable_cpp__
#define __base_SWHashTable_cpp__

#include <swift/SWHashTable.h>
#include <swift/SWLocalMemoryManager.h>
#include <swift/SWFixedSizeMemoryManager.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif

SWIFT_NAMESPACE_BEGIN

template<class T>
SWHashTable<T>::SWHashTable(int initialCapacity, float loadFactor, bool autoRehash) :
	m_pBucketMemoryManager(0),
	m_pElementMemoryManager(0),
	m_elemsize(0),
	m_autoRehash(autoRehash),
    m_loadFactor(loadFactor),
    m_bucketArray(0),
	m_bucketCount(initialCapacity)
		{
		// Try to ensure good alignment internally
		m_elemsize = sizeof(SWListElement<T>);
		m_elemsize = ((m_elemsize + sizeof(void *) - 1) / sizeof(void *)) * sizeof(void *);

		m_pElementMemoryManager = new SWFixedSizeMemoryManager(m_elemsize);

		m_pBucketMemoryManager = SWLocalMemoryManager::getInstance();
		m_pBucketMemoryManager->addRef();

		m_bucketArray = (Bucket *)m_pBucketMemoryManager->malloc(m_bucketCount * sizeof(Bucket));

		int		i;
		Bucket	*pBucket;

		for (i=0,pBucket=m_bucketArray;i<m_bucketCount;i++,pBucket++)
			{
			pBucket->head = pBucket->tail = NULL;
			}
		}


template<class T>
SWHashTable<T>::~SWHashTable()
		{
		clear();
	
		m_pBucketMemoryManager->free(m_bucketArray);
		m_pBucketMemoryManager->dropRef();

		delete m_pElementMemoryManager;
		}


template<class T> void
SWHashTable<T>::clear()
		{
		SWWriteGuard	guard(this);	// Lock the object for the duration
		int				i;
		Bucket			*pBucket;

		// Clear out the objects from each list
		for (i=0,pBucket=m_bucketArray;i<m_bucketCount;i++,pBucket++)
			{
			if (pBucket->head != NULL)
				{
				SWListElement<T>	*pElement=pBucket->head, *pNextElement;

				while (pElement != NULL)
					{
					pNextElement = pElement->next;

					delete pElement;
					m_pElementMemoryManager->free(pElement);

					pElement = pNextElement;
					}
				}

			pBucket->head = pBucket->tail = NULL;
			}

		// Inform the collection that the collection has changed
		SWCollection::setSize(0);
		}


template<class T> void
SWHashTable<T>::rehash()
		{
		SWWriteGuard	guard(this);
		Bucket			*pOldBucketArray=m_bucketArray;
		int				oldBucketCount=m_bucketCount;
		int				i;
		Bucket			*pBucket;

		// Update the size and calculate the new threshold
		m_bucketCount = m_bucketCount * 2 + 1;
		m_threshold = (int)(m_loadFactor * (float)m_bucketCount);

		// Create and initialise a new bucket array
		m_bucketArray = (Bucket *)m_pBucketMemoryManager->malloc(m_bucketCount * sizeof(Bucket));

		for (i=0,pBucket=m_bucketArray;i<m_bucketCount;i++,pBucket++)
			{
			pBucket->head = pBucket->tail = NULL;
			}

		// Run through the old buckets and re-insert all the elements
		for (i=0,pBucket=pOldBucketArray;i<oldBucketCount;i++,pBucket++)
			{
			if (pBucket->head != NULL)
				{
				SWListElement<T>	*pElement=pBucket->head, *pNextElement;

				while (pElement != NULL)
					{
					pNextElement = pElement->next;
					addElement(pElement);
					pElement = pNextElement;
					}
				}
			}
		
		m_pBucketMemoryManager->free(pOldBucketArray);
		}


template<class T> void
SWHashTable<T>::addElement(SWListElement<T> *pElement)
		{
		Bucket	*pBucket=&m_bucketArray[bucket(pElement->data())];

		if (pBucket->tail == NULL)
			{
			pBucket->head = pBucket->tail = pElement;
			pElement->next = pElement->prev = NULL;
			}
		else
			{
			pBucket->tail->next = pElement;
			pElement->prev = pBucket->tail;
			pElement->next = NULL;
			pBucket->tail = pElement;
			}
		}


template<class T> bool
SWHashTable<T>::add(const T &data)
		{
		SWListElement<T>	*pElement;
		void				*p;

		p = m_pElementMemoryManager->malloc(m_elemsize);
		if (p == NULL) throw SW_EXCEPTION(SWMemoryException);
		pElement = new (p) SWListElement<T>(data);

		SWWriteGuard	guard(this);	// Lock the object for the duration

		addElement(pElement);

		// Inform the collection that the collection has changed
		incSize();

		if (m_autoRehash && size() > m_threshold)
			{
			rehash();
			}

		return true;
		}



template<class T> bool
SWHashTable<T>::get(const T &search, T &data) const
		{
		return contains(search, &data);
		}



template<class T> bool
SWHashTable<T>::contains(const T &data, T *pFoundData) const
		{
		SWReadGuard		guard(this);	// Lock the object for the duration
		Bucket			*pBucket=&m_bucketArray[bucket(data)];
		bool			found=false;

		if (pBucket->head != NULL)
			{
			SWListElement<T>	*pElement=pBucket->head;

			while (pElement != NULL)
				{
				if (pElement->data() == data)
					{
					if (pFoundData != NULL) *pFoundData = pElement->data();
					found = true;
					break;
					}

				pElement = pElement->next;
				}
			}

		return found;
		}


template<class T> void
SWHashTable<T>::removeElement(Bucket *pBucket, SWListElement<T> *pElement)
		{
		SWListElement<T>	*n, *p;

		n = pElement->next;
		p = pElement->prev;
		if (pBucket->head == pElement) pBucket->head = pElement->next;
		if (pBucket->tail == pElement) pBucket->tail = pElement->prev;
		if (n) n->prev = pElement->prev;
		if (p) p->next = pElement->next;

		delete pElement;
		m_pElementMemoryManager->free(pElement);

		SWCollection::decSize();
		}


template<class T> bool
SWHashTable<T>::remove(const T &data, T *pFoundData)
		{
		SWWriteGuard	guard(this);	// Lock the object for the duration
		Bucket			*pBucket=&m_bucketArray[bucket(data)];
		bool			found=false;

		if (pBucket->head != NULL)
			{
			SWListElement<T>	*pElement=pBucket->head;

			while (pElement != NULL)
				{
				if (pElement->data() == data)
					{
					if (pFoundData != NULL) *pFoundData = pElement->data();
					removeElement(pBucket, pElement);
					found = true;
					break;
					}

				pElement = pElement->next;
				}
			}

		return found;
		}

template<class T> SWIterator<T>
SWHashTable<T>::iterator(bool iterateFromEnd) const
		{
		SWIterator<T>		res;

		initIterator(res, getIterator(iterateFromEnd));

		return res;
		}


template<class T> int
SWHashTable<T>::bucket(const T &data) const
		{
		return (SWHashCode(data).hashcode() % m_bucketCount);
		}


/*******************************************************************************
**	SWHashTableIterator Implementaion (internal class)
*******************************************************************************/


/**
*** Actual iterator for a hash table
**/
template<class T>
class SWHashTableIterator : public SWCollectionIterator
		{
friend class SWHashTable<T>;

public:
		SWHashTableIterator(SWHashTable<T> *ht, bool iterateFromEnd);

public:

		virtual bool		hasCurrent();
		virtual bool		hasNext();
		virtual bool		hasPrevious();

		virtual void *		current();
		virtual void *		next();
		virtual void *		previous();

		virtual bool		remove(void *pOldData);
		virtual bool		add(void *data, SWAbstractIterator::AddLocation where);
		virtual bool		set(void *data, void *pOldData);

		SWHashTable<T> *	collection()	{ return (SWHashTable<T> *)m_pCollection; }

private:
		void				findNextElement(SWListElement<T> *pElement, int bucket, SWListElement<T> *&pNextElement, int &nextbucket);
		void				findPrevElement(SWListElement<T> *pElement, int bucket, SWListElement<T> *&pPrevElement, int &prevbucket);
private:
		int					m_currBucketNo;	// The bucket number / chain number
		int					m_nextBucketNo;	// The bucket number / chain number
		int					m_prevBucketNo;	// The bucket number / chain number
		SWListElement<T>	*m_curr;	// The "current" element
		SWListElement<T>	*m_next;	// The next element
		SWListElement<T>	*m_prev;	// The previous element
		};



template<class T> SWCollectionIterator *
SWHashTable<T>::getIterator(bool iterateFromEnd) const
		{
		return new SWHashTableIterator<T>((SWHashTable<T> *)this, iterateFromEnd);
		}




template<class T>
SWHashTableIterator<T>::SWHashTableIterator(SWHashTable<T> *ht, bool iterateFromEnd) :
    SWCollectionIterator(ht),
    m_curr(NULL),
    m_next(NULL),
    m_prev(NULL),
	m_currBucketNo(-1),
	m_nextBucketNo(-1),
	m_prevBucketNo(-1)
		{
		SWReadGuard					guard(m_pCollection);
		typename SWHashTable<T>::Bucket	*pBucket;
		int							i;


		m_modcount = collection()->getModCount();

		if (iterateFromEnd)
			{
			// Look for the last non-empty bucket
			i = ht->m_bucketCount - 1;
			pBucket = &ht->m_bucketArray[i];
			while (i >= 0)
				{
				if (pBucket->tail != NULL)
					{
					m_prev = pBucket->tail;
					m_prevBucketNo = i;
					break;
					}

				i--;
				pBucket--;
				}
			}
		else
			{
			// Look for the first non-empty bucket
			for (i=0,pBucket=ht->m_bucketArray;i<ht->m_bucketCount;i++,pBucket++)
				{
				if (pBucket->head != NULL)
					{
					m_next = pBucket->head;
					m_nextBucketNo = i;
					break;
					}
				}
			}
		}


template<class T> bool
SWHashTableIterator<T>::hasCurrent()
		{
		SWReadGuard	guard(m_pCollection);

		checkForComodification();

		return (m_curr != NULL);
		}


template<class T> bool		
SWHashTableIterator<T>::hasNext()
		{
		SWReadGuard	guard(m_pCollection);

		checkForComodification();

		return (m_next != NULL);
		}


template<class T> bool		
SWHashTableIterator<T>::hasPrevious()
		{
		SWReadGuard	guard(m_pCollection);

		checkForComodification();

		return (m_prev != NULL);
		}


template<class T> void *
SWHashTableIterator<T>::current()
		{
		SWReadGuard	guard(m_pCollection);

		checkForComodification();

		if (m_curr == NULL) throw SW_EXCEPTION(SWNoSuchElementException);

		return (void *)&(m_curr->data());
		}


template<class T> void
SWHashTableIterator<T>::findNextElement(SWListElement<T> *pElement, int bucket, SWListElement<T> *&pNextElement, int &nextbucket)
		{
		if (pElement->next != NULL)
			{
			pNextElement = pElement->next;
			nextbucket = bucket;
			}
		else
			{
			// Look for the next non-empty bucket
			typename SWHashTable<T>::Bucket	*pBucket;
			int							i;
			bool						found=false;

			i = bucket+1;
			pBucket = &collection()->m_bucketArray[i];
			while (i < collection()->m_bucketCount)
				{
				if (pBucket->head != NULL)
					{
					pNextElement = pBucket->head;
					nextbucket = i;
					found = true;
					break;
					}

				i++;
				pBucket++;
				}

			if (!found)
				{
				pNextElement = NULL;
				nextbucket = -1;
				}
			}
		}


template<class T> void
SWHashTableIterator<T>::findPrevElement(SWListElement<T> *pElement, int bucket, SWListElement<T> *&pPrevElement, int &prevbucket)
		{
		if (pElement->prev != NULL)
			{
			pPrevElement = pElement->prev;
			prevbucket = bucket;
			}
		else
			{
			// Look for the next non-empty bucket
			typename SWHashTable<T>::Bucket	*pBucket;
			int							i;
			bool						found=false;

			i = bucket+1;
			pBucket = &collection()->m_bucketArray[i];
			while (i >= 0)
				{
				if (pBucket->tail != NULL)
					{
					pPrevElement = pBucket->tail;
					prevbucket = i;
					found = true;
					break;
					}

				i--;
				pBucket--;
				}

			if (!found)
				{
				pPrevElement = NULL;
				prevbucket = -1;
				}
			}
		}


template<class T> void *	
SWHashTableIterator<T>::next()
		{
		SWReadGuard	guard(m_pCollection);

		checkForComodification();

		if (m_next == NULL) throw SW_EXCEPTION(SWNoSuchElementException);

		m_curr = m_next;
		m_currBucketNo = m_nextBucketNo;

		findNextElement(m_curr, m_currBucketNo, m_next, m_nextBucketNo);
		findPrevElement(m_curr, m_currBucketNo, m_prev, m_prevBucketNo);

		return (void *)&(m_curr->data());
		}


template<class T> void *	
SWHashTableIterator<T>::previous()
		{
		SWReadGuard	guard(m_pCollection);

		checkForComodification();

		if (m_prev == NULL) throw SW_EXCEPTION(SWNoSuchElementException);

		m_curr = m_prev;
		m_currBucketNo = m_prevBucketNo;

		findNextElement(m_curr, m_currBucketNo, m_next, m_nextBucketNo);
		findPrevElement(m_curr, m_currBucketNo, m_prev, m_prevBucketNo);

		return (void *)&(m_curr->data());
		}

template<class T> bool
SWHashTableIterator<T>::remove(void *pOldData)
		{
		SWWriteGuard	guard(m_pCollection);

		checkForComodification();

		if (m_curr == NULL) throw SW_EXCEPTION(SWNoSuchElementException);

		if (pOldData != NULL) *(T *)pOldData = m_curr->data();

		collection()->removeElement(&(collection()->m_bucketArray[m_currBucketNo]), m_curr);
		m_curr = NULL;
		m_modcount = collection()->getModCount();

		return true;
		}


template<class T> bool
SWHashTableIterator<T>::add(void * /*data*/, SWAbstractIterator::AddLocation /*where*/)
		{
		bool	res=false;

		if (!res) throw SW_EXCEPTION(SWUnsupportedOperationException);

		return res;
		}


template<class T> bool
SWHashTableIterator<T>::set(void * /*data*/, void * /*ppOldData*/)
		{
		bool	res=false;

		if (!res) throw SW_EXCEPTION(SWUnsupportedOperationException);

		return res;
		}


SWIFT_NAMESPACE_END

#endif
