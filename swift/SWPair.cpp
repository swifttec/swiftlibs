/**
*** @file		SWPair.cpp
*** @brief		Templated vector implementation.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWPair class.
**/

/*********************************************************************************
Copyright (c) 2006-2016 SwiftTec Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __base_SWPair_cpp__
#define __base_SWPair_cpp__

#include <swift/SWPair.h>

SWIFT_NAMESPACE_BEGIN

template<class T1, class T2> 
SWPair<T1, T2>::SWPair()
		{
		}


template<class T1, class T2> 
SWPair<T1, T2>::SWPair(const T1 &v1, const T2 &v2) :
	_value1(v1),
	_value2(v2)
		{
		}


template<class T1, class T2> 
SWPair<T1, T2>::SWPair(const SWPair<T1, T2> &other) :
	_value1(other._value1),
	_value2(other._value2)
		{
		}


template<class T1, class T2> SWPair<T1, T2> &
SWPair<T1, T2>::operator=(const SWPair<T1, T2> &other)
		{
		_value1 = other._value1;
		_value2 = other._value2;

		return *this;
		}


template<class T1, class T2> bool
SWPair<T1, T2>::operator==(const SWPair<T1, T2> &other) const
		{
		return (_value1 == other._value1 && _value2 == other._value2);
		}


template<class T1, class T2> bool
SWPair<T1, T2>::operator!=(const SWPair<T1, T2> &other) const
		{
		return (!(_value1 == other._value1) || !(_value2 == other._value2));
		}


SWIFT_NAMESPACE_END

#endif
