/**
*** @file		SWLogMessage.h
*** @brief		A class to represent a single log entry.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWLogMessage class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWLogMessage_h__
#define __SWLogMessage_h__

#include <swift/swift.h>

#include <swift/SWThread.h>
#include <swift/SWString.h>





/**
*** @name	Log message types
***
*** The log message types define the type of the message. Examples of message types are:
***
*** - FATAL - an error message indicating that a fatal event has occurred
*** - ERROR - an error message indicating that an error has occurred.
*** - WARNING - a warning message.
*** - INFO - an informational message
***
*** Each message type is defined as a single bit, which means that log message handlers
*** can select the message types they are interesting by using a type mask.
*** Normally log messages should only have one type bit set, but this not enforced in
*** case situation arises whereby a dual-natured message should be needed.
***
*** If the message type is zero then the log message is deemed to have no type.
**/
//@{
#define SW_LOG_TYPE_SUCCESS		0x00008000	///< A message about a successful event
#define SW_LOG_TYPE_FATAL		0x00004000	///< A fatal message
#define SW_LOG_TYPE_ERROR		0x00002000	///< An error message
#define SW_LOG_TYPE_WARNING		0x00001000	///< A warning message
#define SW_LOG_TYPE_INFO		0x00000800	///< An informational message
#define SW_LOG_TYPE_DEBUG		0x00000400	///< A message to aid in the debugging of a program/module
#define SW_LOG_TYPE_STDERR		0x00000200	///< A message that has been captured from the stderr stream
#define SW_LOG_TYPE_STDOUT		0x00000100	///< A message that has been captured from the stdout stream
#define SW_LOG_TYPE_NOTICE		0x00000080	///< A message that indicates a non-error condition, but that may need special handling.
#define SW_LOG_TYPE_CRITICAL	0x00000040	///< A critical condition message (e.g. hard devices errors)
#define SW_LOG_TYPE_EMERGENCY	0x00000020	///< An emergency/panic condition message
#define SW_LOG_TYPE_ALERT		0x00000010	///< A message to indicate a situation that should corrected immediately

#define SW_LOG_TYPE_MASK		0x0000fff0	///< A bit mask used to mask out the message type bits
//@}


/**
*** @name	Log message levels
***
*** The log message level is a 4-bit number that is combined with the log type.
*** It intended to give a level of verbosity within the logging system. The
*** higher the number, the greater the level of verbosity within the logs.
***
*** Users of the SWLog class can set the level of reported messages by calling the
*** xxxx method. Only messages that at or below the verbosity level set in the SWLog
*** object are sent to the log handlers.
***
*** A log level of zero (SW_LOG_LEVEL_ALWAYS) has a special meaning in that it cannot
*** be filtered out.
***
*** The following SW_LOG_LEVEL_XXXX defines attempt to give meaning to the possible
*** log level values.
**/
//@{
#define SW_LOG_LEVEL_ALWAYS				0x0		///< A message to always be output, regardless of current log level
#define SW_LOG_LEVEL_VERY_BRIEF			0x1		///< Messages containing very brief information (use very sparingly, should be very few of these).
#define SW_LOG_LEVEL_BRIEF				0x2		///< Messages containing brief information (use sparingly, should be few of these).
#define SW_LOG_LEVEL_SHORT_SUMMARY		0x3		///< Messages containing brief summary information.
#define SW_LOG_LEVEL_SUMMARY			0x4		///< Messages containing summary information.
#define SW_LOG_LEVEL_EXTENDED_SUMMARY	0x5		///< Messages containing extended summary information.
#define SW_LOG_LEVEL_DETAIL				0x6		///< Detail messages
#define SW_LOG_LEVEL_EXTENDED_DETAIL	0x7		///< Messages containing extended detail
#define SW_LOG_LEVEL_VERBOSE			0x8		///< Messages required for a more verbose logs
#define SW_LOG_LEVEL_EXTENDED_VERBOSE	0x9		///< Messages required for a greater level of verbosity
#define SW_LOG_LEVEL_DEBUG				0xa		///< Messages required for application debugging
#define SW_LOG_LEVEL_EXTENDED_DEBUG		0xc		///< Messages required for extended application debugging
#define SW_LOG_LEVEL_TRACE				0xd		///< Messages required for application tracing
#define SW_LOG_LEVEL_EXTENDED_TRACE		0xe		///< Messages required for extended application tracing
#define SW_LOG_LEVEL_MAXIMUM			0xf		///< Messages required only at the maximum level of tracing

#define SW_LOG_LEVEL_MASK				0xf		///< Only trace - fatal messages reported
//@}


/**
*** @name	Basic log message types
***
*** The following combine a log message type and a log message level in an attempt
*** to provide a set of easily used quick constants for logging, whilst still
*** enabling more complex logging facilities for applications that require it.
**/
//@{
#define SW_LOG_MSG_ALERT		(SW_LOG_TYPE_ALERT | SW_LOG_LEVEL_ALWAYS)		///< An alert message, always reported
#define SW_LOG_MSG_EMERGENCY	(SW_LOG_TYPE_EMERGENCY | SW_LOG_LEVEL_ALWAYS)	///< A emergency error message, always reported
#define SW_LOG_MSG_CRITICAL		(SW_LOG_TYPE_CRITICAL | SW_LOG_LEVEL_ALWAYS)	///< A critical error message, always reported
#define SW_LOG_MSG_NOTICE		(SW_LOG_TYPE_NOTICE | SW_LOG_LEVEL_ALWAYS)		///< A notice, always reported
#define SW_LOG_MSG_FATAL		(SW_LOG_TYPE_FATAL | SW_LOG_LEVEL_ALWAYS)		///< A fatal error message, always reported
#define SW_LOG_MSG_ERROR		(SW_LOG_TYPE_ERROR | SW_LOG_LEVEL_SUMMARY)		///< An error message, reported at summary level
#define SW_LOG_MSG_WARNING		(SW_LOG_TYPE_WARNING | SW_LOG_LEVEL_SUMMARY)	///< A warning message, reported at summary level
#define SW_LOG_MSG_SUCCESS		(SW_LOG_TYPE_SUCCESS | SW_LOG_LEVEL_SUMMARY)	///< A success message, reported at summary level
#define SW_LOG_MSG_INFO			(SW_LOG_TYPE_INFO | SW_LOG_LEVEL_SUMMARY)		///< An information message, reported at summary level.
#define SW_LOG_MSG_DETAIL		(SW_LOG_TYPE_INFO | SW_LOG_LEVEL_DETAIL)		///< An information message reported at detail level.
#define SW_LOG_MSG_DEBUG		(SW_LOG_TYPE_DEBUG | SW_LOG_LEVEL_DEBUG)		///< A debug message reported at debug level.
#define SW_LOG_MSG_TRACE		(SW_LOG_TYPE_DEBUG | SW_LOG_LEVEL_TRACE)		///< A debug message reported at trace level.
//@}


#define	SW_LOG_SWTEGORY_SHIFT	16			///< The amount to shift the flags to get to the category
#define	SW_LOG_SWTEGORY_MASK	0xffff		///< The mask for category


/**
*** @name	Pre-Define log format strings
**/
//@{

/// Default log format
#define SW_LOG_FORMAT_DEFAULT	"${date} ${time} ${tid} ${type} ${text}"

/// Detailed log format
#define SW_LOG_FORMAT_DETAIL	"${date} ${time} ${app} ${pid} ${tid} ${type}:${level} ${text}"

/// Full log format
#define SW_LOG_FORMAT_FULL		"${date} ${time} ${app} ${file} ${line} ${pid} ${tid} ${type}:${level} ${id} ${text}"

/// Log format containing only the text message
#define SW_LOG_FORMAT_MSGONLY	"${text}"

//@}





/**
*** @brief		A class to represent a single log message.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWLogMessage
		{
public:
		/**
		*** @brief	The main log message constructor
		*** @param	ts			The time of the message
		*** @param	pid			The ID of the process that created the message
		*** @param	tid			The ID of the thread that created the message
		*** @param	flags		The message flags (type, level, etc)
		*** @param	category	The message category
		*** @param	hostname	The name of the host where the message was created
		*** @param	appname		The application name
		*** @param	filename	The filename (where generated)
		*** @param	lineno		The line number of the file
		*** @param	str			The actual message
		*** @param	id			The message id (default: 0)
		*** @param	feature		The message feature code
		*** @param	pData		A pointer to a data block to associate with this message
		*** @param	datalen		The size of data block to associate with this message
		**/
		SWLogMessage(
			const sw_timespec_t &ts,	// the log time
			sw_pid_t pid,				// the pid (C++ compatibilty only)
			sw_thread_id_t tid,			// the thread id (C++ compatibilty only)
			sw_uint32_t flags,			// the message flags (type, level, etc)
			sw_uint16_t category,		// the message category
			const SWString &hostname,	// the application name
			const SWString &appname,	// the application name
			const SWString &filename,	// the filename (where generated)
			sw_int32_t lineno,			// the line number of the file
			const SWString &str,		// the actual message
			sw_uint32_t id=0,			// the message id (default: 0)
			sw_uint64_t feature=0,		// the message feature code
			const void	*pData=0,
			sw_size_t datalen=0);

		/// Copy constructor
		SWLogMessage(const SWLogMessage &e);

		/// Assignment operator
		SWLogMessage &	operator=(const SWLogMessage &e);

		/// Virtual destructor
		virtual ~SWLogMessage();


		/**
		*** @brief	Returns the log message formatted into a string
		**/
		SWString				format(const SWString &fmt) const;

		/**
		*** @brief	Convert the log message to a string and store it in the specified output string.
		***
		*** This method takes the format string (fmt) and uses it to format the contents
		*** of this log message object into the specified output string.
		***
		*** The format string consists of text and field identifiers which are specified as
		*** <b>${id}</b> or <b>${id:width}</b> . The field identifiers are as follows:
		***
		*** - ${text} - The message text
		*** - ${msg} - The same as ${text}
		*** - ${shorttype} - The message type as a 3 letter acronym
		*** - ${longtype} - The message type as a string
		*** - ${type} - The same as ${shorttype}
		*** - ${file} - The file name where the message was generated
		*** - ${line} - The line number of the generated message
		*** - ${level} - The numeric log level (0-15).
		*** - ${category} - The numeric category value.
		*** - ${process} - The ID of the process that generated the message.
		*** - ${pid} - The same as ${process}
		*** - ${thread} - The ID of the thread that generated the message.
		*** - ${tid} - The same as ${thread}
		*** - ${id} = The numeric message ID
		*** - ${feature} - The numeric feature value as a hexadecimal number
		*** - ${date} - The date/time value date component as DD/MM/YYYY
		*** - ${time} - The date/time value time component as HH:MM:SS
		*** - ${day} - The date/time value day component (1-31)
		*** - ${month} - The date/time value month component (1-12)
		*** - ${year} - The date/time value year component (1-31)
		*** - ${hour} - The date/time value hour component (0-23)
		*** - ${minute} - The date/time value minute component (0-59)
		*** - ${second} - The date/time value second component (0-61)
		*** - ${ms} - The date/time value fraction in milliseconds 
		*** - ${us} - The date/time value fraction in microseconds 
		*** - ${ds} - The date/time value fraction in nanoseconds 
		*** - ${application} - The application name
		*** - ${app} - The same as ${application}
		*** - ${host} - The host name
		*** - ${datalen} - The length of the associated data
		*** - ${data} - A hex dump of the data - use with caution as strings could get very long
		***
		*** If the field identifier is specified in the second form <b>${id:width}</b> 
		*** the width component specifies the width the field is padded to with spaces.
		***
		*** @param[out]	outstr	Receives the formatted output.
		*** @param[in]	fmt		The format string.
		***
		*** @return		A reference to outstr.
		**/
		SWString &				format(SWString &outstr, const SWString &fmt) const;

		/**
		*** @brief Returns the time component of the log message
		**/
		const sw_timespec_t &	time() const		{ return m_time; }

		/**
		*** @brief Returns the process ID component of the log message
		**/
		sw_pid_t				pid() const			{ return m_pid; }

		/**
		*** @brief Returns the thread ID component of the log message
		**/
		sw_thread_id_t				tid() const			{ return m_tid; }

		/**
		*** @brief Returns the message flags.
		**/
		sw_uint32_t				flags() const		{ return m_flags; }

		/**
		*** @brief Returns the type of this message as defined in SWLogMessage.h
		**/
		sw_uint32_t				type() const		{ return (m_flags & SW_LOG_TYPE_MASK); }

		/**
		*** @brief Returns the level of this message as defined in SWLogMessage.h
		**/
		sw_uint32_t				level() const		{ return (m_flags & SW_LOG_LEVEL_MASK); }

		/**
		*** @brief Returns the numeric category of this message.
		**/
		sw_uint16_t				category() const	{ return (sw_uint16_t)((m_flags >> SW_LOG_SWTEGORY_SHIFT) & SW_LOG_SWTEGORY_MASK); }

		/**
		*** @brief Returns the message ID component of the log message
		**/
		sw_uint32_t				id() const			{ return m_msgid; }

		/**
		*** @brief Returns the feature code component of the log message
		**/
		sw_uint64_t				feature() const		{ return m_feature; }

		/**
		*** @brief Returns the application name component of the log message
		***
		*** @note
		*** If there is no valid file name component this method will return
		*** an empty string.
		**/
		const SWString &		application() const	{ return m_appname; }

		/**
		*** @brief Returns the file name component of the log message
		***
		*** @note
		*** If there is no valid file name component this method will return
		*** an empty string.
		**/
		const SWString &		file() const		{ return m_filename; }

		/**
		*** @brief Returns the line number component of the log message
		***
		*** @note
		*** If there is no valid line number component this method will return
		*** a negative value.
		**/
		sw_int32_t				line() const		{ return m_lineno; }

		/**
		*** @brief Returns the message text component of the log message
		**/
		const SWString &		text() const		{ return m_text; }

		/**
		*** @brief Returns the name of the host that generated this message
		**/
		const SWString &		hostname() const	{ return m_hostname; }

		/**
		*** @brief	Returns a pointer to the data associated with this message (may be NULL)
		**/
		const void *			data() const		{ return m_pData; }

		/**
		*** @brief	Returns the length of the data associated with this message (may be 0)
		**/
		sw_size_t				datalen() const		{ return m_datalen; }


public: // STATIC methods

		/**
		*** @brief	return the specified log message type as a string
		**/
		static const sw_tchar_t *		getTypeString(sw_uint32_t type, bool shortform);


private:
		sw_timespec_t 	m_time;			///< the time the message was generated
		sw_pid_t		m_pid;			///< the id of the process that generated the message
		sw_thread_id_t		m_tid;			///< the id of the thread that generated the message
		sw_uint32_t		m_flags;		///< the message flags (type, level, etc)
		SWString		m_appname;		///< the application name
		SWString		m_filename;		///< the filename where the message was generated
		sw_int32_t		m_lineno;		///< the line number of the file where the message was generated
		SWString		m_text;			///< the message text
		sw_uint32_t		m_msgid;		///< the message id - used to identify the message
		sw_uint64_t		m_feature;		///< the message featurecode - used to mask out a specific class of message
		SWString		m_hostname;		///< the name of the host that generated this log message
		void			*m_pData;		///< a block of binary data associated with this message (may be null)
		sw_size_t		m_datalen;		///< the size of the binary data block
		};




#endif
