/****************************************************************************
**	SWPagedFile.h	A class to access a file in a paged manner
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWPagedFile_h__
#define __SWPagedFile_h__

#include <swift/SWLockableObject.h>
#include <swift/SWFlags.h>
#include <swift/SWList.h>
#include <swift/SWString.h>
#include <swift/SWFile.h>


/**
*** The internal object used to represent a page of data
**/
class SWIFT_DLL_EXPORT SWPagedFilePage : public SWObject
		{
friend class SWPagedFile;

public:
		SWPagedFilePage(int pageno, int size);

		virtual ~SWPagedFilePage();

		// Returns true if the page is dirty
		bool		isDirty() const				{ return m_dirty; }

		// Returns true if the page is clean (not dirty)
		bool		isClean() const				{ return !m_dirty; }

		// Returns the page number
		int			pageNo() const				{ return m_pageno; }

		// Returns the data pointer for this page
		char *		data() const				{ return m_pData; }

		// Mark the page as dirty
		void		setDirty(bool v=true)		{ m_dirty = v; }

protected:
		int		m_pageno;			///< The page number
		char	*m_pData;			///< The data
		bool	m_dirty;			///< true if the page is dirty
		time_t	m_lastAccessTime;	///< The time of last access
		};


/**
*** The object represents a file that is expected to accessed in a random
*** access mode. The data is held in a number of pages which are paged into
*** memory as required.
***
*** @brief	A class to access a file in a paged manner
**/
class SWIFT_DLL_EXPORT SWPagedFile : public SWLockableObject
		{
public:
		/// Default constructor (read-write, 8k blocksize, max 32 pages in memory)
		SWPagedFile(const SWString &filename, bool readonly=false, int pagesize=8192, int maxpages=32);

		virtual ~SWPagedFile();

		/// Open the specified file
		sw_status_t			open(const SWString &filename, bool readonly=false, int pagesize=8192, int maxpages=32);

		/// Open the specified file
		sw_status_t			close();

		/// Return the filename
		SWString			filename() const	{ return m_file.getFullPath(); }

		/// Return the page size used
		int					pagesize() const	{ return m_pagesize; }

		/// Return the maximum number of pages
		int					maxpages() const	{ return m_maxpages; }

		/// Return the number of pages currently in memory
		int					pagecount() const	{ return (int)m_pagelist.size(); }

		// Flush all the dirty pages to disk
		sw_status_t			flush();

		// Read data from the file at the given offset
		sw_status_t			read(off_t off, void *pData, sw_size_t nbytes, sw_size_t *pBytesRead=NULL);

		// Write data to the file at the given offset
		sw_status_t			write(off_t off, const void *pData, sw_size_t nbytes, sw_size_t *pBytesWritten=NULL);

		// Debugging Aid
		virtual void		dump();




		/// Read a signed byte (1 bytes / 8 bits) from the given offset
		sw_status_t			read(off_t off, bool &v);

		/// Read a signed byte (1 bytes / 8 bits) from the given offset
		sw_status_t			read(off_t off, sw_int8_t &v);

		/// Read a signed short integer (2 bytes / 16 bits) from the given offset
		sw_status_t			read(off_t off, sw_int16_t &v);

		/// Read an signed integer (4 bytes / 32 bits) from the given offset
		sw_status_t			read(off_t off, sw_int32_t &v);

		/// Read a signed long integer 8 bytes / 64) bits from the given offset
		sw_status_t			read(off_t off, sw_int64_t &v);

		/// Read a signed byte (1 bytes / 8 bits) from the given offset
		sw_status_t			read(off_t off, sw_uint8_t &v);

		/// Read a signed short integer (2 bytes / 16 bits) from the given offset
		sw_status_t			read(off_t off, sw_uint16_t &v);

		/// Read an signed integer (4 bytes / 32 bits) from the given offset
		sw_status_t			read(off_t off, sw_uint32_t &v);

		/// Read a signed long integer 8 bytes / 64) bits from the given offset
		sw_status_t			read(off_t off, sw_uint64_t &v);

		// Read a float (32 bits) from the given offset
		sw_status_t			read(off_t off, float &v);

		// Read a double (64 bits) from the given offset
		sw_status_t			read(off_t off, double &v);



		// Write a single byte of data to the given offset
		sw_status_t			write(off_t off, bool ba);

		/// Write a signed byte (1 bytes / 8 bits0 to the given offset
		sw_status_t			write(off_t off, sw_int8_t v);

		/// Write a signed short (2 bytes / 16 bits) to the given offset
		sw_status_t			write(off_t off, sw_int16_t v);

		/// Write a signed integer (4 bytes / 32 bits) to the given offset
		sw_status_t			write(off_t off, sw_int32_t v);

		/// Write a signed long integer (8 bytes / 64 bits) to the given offset
		sw_status_t			write(off_t off, sw_int64_t v);

		/// Write a signed byte (1 bytes / 8 bits0 to the given offset
		sw_status_t			write(off_t off, sw_uint8_t v);

		/// Write a signed short (2 bytes / 16 bits) to the given offset
		sw_status_t			write(off_t off, sw_uint16_t v);

		/// Write a signed integer (4 bytes / 32 bits) to the given offset
		sw_status_t			write(off_t off, sw_uint32_t v);

		/// Write a signed long integer (8 bytes / 64 bits) to the given offset
		sw_status_t			write(off_t off, sw_uint64_t v);

		/// Write a float (32 bits) to the given offset
		sw_status_t			write(off_t off, float v);

		/// Write a double (64 bits) to the given offset
		sw_status_t			write(off_t off, double v);


private:
		// Reads a page from disk
		sw_status_t			readPageFromDisk(SWPagedFilePage *pp);

		// Writes a dirty page back to disk
		sw_status_t			writePageToDisk(SWPagedFilePage *pp);

		// Returns the page number for a given offset
		int			pageNumber(off_t off) const	{ return off / m_pagesize; }

		// Gets a page from the file
		SWPagedFilePage *	getPage(int pageno);

		// Drops the LRU page from the cache
		void			dropLruPage(bool preferClean=false);

private:
		SWFile						m_file;		// The file descriptor
		int							m_pagesize;	// The page size
		int							m_maxpages;	// The maximum number of pages to be held in memory
		SWFlags						m_pagemap;	// A bitset representing which pages of the file are in memory
		SWList<SWPagedFilePage *>	m_pagelist;	// A list of all the pages
		};






#endif
