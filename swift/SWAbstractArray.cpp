/**
*** @file		SWAbstractArray.cpp
*** @brief		A generic array object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWAbstractArray class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <string.h>

#include <swift/SWAbstractArray.h>
#include <swift/SWException.h>
#include <swift/SWGuard.h>

#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif






SWAbstractArray::SWAbstractArray(SWMemoryManager *pMemoryManager, sw_size_t elemsize) :
	m_pMemoryManager(pMemoryManager),
	m_pData(NULL),
	m_capacity(0),
    m_nelems(0),
    m_elemsize(elemsize)
		{
		if (m_pMemoryManager == NULL)
			{
			m_pMemoryManager = SWMemoryManager::getDefaultMemoryManager();
			}
		}


SWAbstractArray::~SWAbstractArray()
		{
		clear();
		if (m_pData != NULL)
			{
			m_pMemoryManager->free(m_pData);
			m_pData = NULL;
			}
		}


// Make sure our _data array has enough space!
void
SWAbstractArray::ensureCapacity(sw_size_t nelems)
		{
		sw_size_t		wanted=nelems * m_elemsize;

		if (wanted > m_capacity)
			{
			SWWriteGuard	guard(this);	// Guard this object

			if (m_capacity == 0) m_capacity = 128;

			while (m_capacity < wanted)
				{
				m_capacity <<= 1;
				}

			char	*pOldData = m_pData;

			m_pData = (char *)m_pMemoryManager->malloc(m_capacity);
			if (m_pData == NULL)
				{
				m_pData = pOldData;
				throw SW_EXCEPTION(SWMemoryException);
				}

			if (pOldData != NULL)
				{
				memcpy(m_pData, pOldData, m_nelems * m_elemsize);
				memset(m_pData + (m_nelems * m_elemsize), 0, (nelems - m_nelems) * m_elemsize);
				m_pMemoryManager->free(pOldData);
				}
			}
		}


// Init back to zero
void
SWAbstractArray::clear()
		{
		if (m_nelems != 0)
			{
			SWWriteGuard	guard(this);	// Guard this object

			m_nelems = 0;
			memset(m_pData, 0, m_capacity);
			}
		}


/**
*** Removes count elements from the position specified
**/
void
SWAbstractArray::removeElements(sw_index_t pos, sw_size_t count)
		{
		if (pos < 0 || pos > (sw_index_t)m_nelems) throw SW_EXCEPTION(SWIndexOutOfBoundsException);

		// Make sure we don't remove too much
		if ((pos + count) > m_nelems) count = m_nelems - pos;
		if (count > 0)
			{
			SWWriteGuard	guard(this);

			if (pos + count != m_nelems)
				{
				// We must shift the data down to fill the gap.
				sw_index_t	from, to, amount;

				to = (sw_index_t)(pos * m_elemsize);
				from = to + (sw_index_t)(count * m_elemsize);
				amount = (sw_index_t)((m_nelems - count - pos) * m_elemsize);

				memmove((char *)m_pData + to, (char *)m_pData + from, amount);
				}

			// Adjust the element count and reflect this
			// in the underlying buffer too so that when it re-grows
			// it gets zeroed out again.
			m_nelems -= count;
			//m_data.data(_nelems * _elemsize);
			}
		}


/**
*** Inserts count elements at the position specified
**/
void
SWAbstractArray::insertElements(sw_index_t pos, sw_size_t count)
		{
		if (pos < 0) throw SW_EXCEPTION(SWIndexOutOfBoundsException);

		if (count > 0)
			{
			SWWriteGuard	guard(this);

			// Make sure we have the capacity for the new elements
			ensureCapacity(m_nelems + count);

			if (pos < (sw_index_t)m_nelems)
				{
				// We must shift the data down to fill the gap.
				int	from, to, amount;

				from = (int)(pos * m_elemsize);
				to = from + (int)(count * m_elemsize);
				amount = (int)((m_nelems - pos) * m_elemsize);

				memmove((char *)m_pData + to, (char *)m_pData + from, amount);

				// Zero out the "new" data
				memset((char *)m_pData + from, 0, count * m_elemsize);
				}

			// Adjust the element count and reflect this
			// in the underlying buffer too so that when it re-grows
			// it gets zeroed out again.
			m_nelems += count;
			//m_data.data(m_nelems * m_elemsize);
			}
		}


bool
SWAbstractArray::defined(int pos) const
		{
		bool	res=false;

		if (pos >= 0 && pos < (sw_index_t)size())
			{
			res = true;
			}

		return res;
		}


sw_size_t
SWAbstractArray::size() const
		{
		return m_nelems;
		}


sw_size_t
SWAbstractArray::count() const
		{
		return m_nelems;
		}


void
SWAbstractArray::remove(sw_index_t pos, sw_size_t count)
		{
		removeElements(pos, count);
		}



