/****************************************************************************
**	SWConfigFile.h	Generic config file implementation
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWConfigFile_h__
#define __SWConfigFile_h__

#include <swift/SWException.h>
#include <swift/SWConfigKey.h>


/**
*** Generic config file implementation
**/
class SWIFT_DLL_EXPORT SWConfigFile : public SWConfigKey
		{
public:
		/// Default constructor
		SWConfigFile(const sw_tchar_t *filename, bool readonly=false, bool writeOnChange=false, bool ignoreCase=false);

		/// Destructor
		virtual ~SWConfigFile();

		/// Find out if we are writeable or not
		bool			isReadOnly() const;

		/// Find out if we are writeable or not
		bool			isWriteOnChange() const;

		/// Find out if we are writeable or not
		bool			isLoaded() const;

		/// Loads the internal structures from the file
		virtual bool	readFromFile(const SWString &filename);

		/// Saves the internal structures to the file
		virtual bool	writeToFile(const SWString &filename);

protected: // Methods

		// Flushes any changes back to the file
		virtual void	flush();

		// Sets the change flag
		virtual void	setChangedFlag(ChangeEventCode code, SWObject *obj);

protected:
		bool		m_readOnlyFlag;			//< Flag to show if we can write changes
		bool		m_writeOnChangeFlag;	//< Flag to automatically rewrite the file on an update
		bool		m_loading;				//< Flag to show that we are currently in a load situation
		bool		m_loaded;
		};

// Exceptions
SW_DEFINE_EXCEPTION(SWConfigFileException, SWException);

#endif
