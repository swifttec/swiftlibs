/**
*** @file		SWSize.h
*** @brief		A class to represent a dimension or x/y size
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWSize class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWSize_h__
#define __SWSize_h__


#include <swift/SWObject.h>
#include <swift/SWOutputStream.h>
#include <swift/SWInputStream.h>

/**
*** @brief	A class to represent a dimension or x/y size
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWSize : public SWObject
		{
public:

		/**
		*** @brief	Default constructor with optional height and width specifier.
		***
		*** @param[in]	w	The width component of the size.
		*** @param[in]	h	The height component of the size.
		**/
		SWSize(sw_uint32_t w=0, sw_uint32_t h=0);


		/// Copy constructor
		SWSize(const SWSize &other);

		/// Set the width component
		void		width(sw_int32_t v)					{ m_width = v; }

		/// Return the width component
		sw_int32_t	width() const						{ return m_width; }

		/// Set the height component
		void		height(sw_int32_t v)				{ m_height = v; }

		/// Return the height component
		sw_int32_t	height() const						{ return m_height; }

		/// Assignment operator
		SWSize &	operator=(const SWSize &other);

		/// Check for equality
		bool	operator==(const SWSize &other) const	{ return (m_width == other.m_width && m_height == other.m_height); }

		/// Check for inequality
		bool	operator!=(const SWSize &other) const	{ return (m_width != other.m_width || m_height != other.m_height); }

public: // virtual overrides

		/// Override for SWObject writeToStream
		virtual sw_status_t	writeToStream(SWOutputStream &stream) const;

		/// Override for SWObject readFromStream
		virtual sw_status_t readFromStream(SWInputStream &stream);

private: // Members
		sw_uint32_t	m_width;	///< The width component of the size
		sw_uint32_t	m_height;	///< The height component of the size
		};

#endif
