/**
*** @file		SWFilteredFolder.cpp
*** @brief		A class to represent a handle to a directory.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWFilteredFolder class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




// Needed for precompiled header support
#include "stdafx.h"

#if defined(SW_PLATFORM_UNIX)
	#include <dirent.h>
	#include <sys/stat.h>
#endif

#include <swift/swift.h>
#include <swift/SWFilteredFolder.h>
#include <swift/SWFile.h>
#include <swift/SWFileInfo.h>
#include <swift/SWFilename.h>


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





#undef USES_REGEXP

/*
** Default constructor.
*/
SWFilteredFolder::SWFilteredFolder(const SWString &path, const SWString &filter) :
	SWFolder(path, filter),
#if defined(SW_PLATFORM_WINDOWS)
	m_caseSensitive(false),
#else
	m_caseSensitive(true),
#endif
	m_pRegExpr(NULL)
		{
		setFilter(filter);
		open(path);
		}




/*
** Copy constructor
*/
SWFilteredFolder::SWFilteredFolder(const SWFilteredFolder &other) :
	SWFolder(other),
#if defined(SW_PLATFORM_WINDOWS)
	m_caseSensitive(false),
#else
	m_caseSensitive(true),
#endif
	m_pRegExpr(NULL)
		{
		*this = other;
		}


/*
** Destructor
*/
SWFilteredFolder::~SWFilteredFolder()
		{
		close();
		delete m_pRegExpr;
		m_pRegExpr = NULL;
		}


/*
** Assignment operator
*/
SWFilteredFolder &
SWFilteredFolder::operator=(const SWFilteredFolder &other)
		{
		close();
		m_caseSensitive = other.m_caseSensitive;
		setFilter(other.m_filter);
		open(other.m_path);

		return *this;
		}




/*
** Check to see if the given file name would be included or excluded
** by the current file filter.
*/
bool
SWFilteredFolder::includeFile(const SWString &file)
		{
		return !excludeFile(file);
		}


/*
** Check to see if the given file name would be included or excluded
** by the current file filter.
*/
bool
SWFilteredFolder::excludeFile(const SWString &file)
		{
		return (m_pRegExpr != NULL && !m_pRegExpr->matches(file));
		}



/**
*** Creates a SWRegularExpression object for the given filter.
***
*** The filter string is a simple wildcard filter.
*** ?		matches any single char
*** *		matches zero or more chars
*** [..]	matches any single char in the given range
***
***
*** This method creates a SWRegularExpression object if one is required
*** that the caller is responsible for deleting. If no SWRegularExpression
*** object is required because the filter is blank or * a NULL pointer is
*** returned.
***
*** @param filter	A simple filter string.
*** @return		A pointer to a newly created SWRegularExpression object
***			or NULL if no filtering is required.
**/
SWRegularExpression *
SWFilteredFolder::createFilterRE(const SWString &filter, bool caseSensitive)
		{
		SWRegularExpression	*re=0;

		if (!filter.isEmpty() && filter != _T("*"))
			{
			SWString	s(filter);

			// Prepare a RE filter string from the given simple filter
			s.replaceAll(_T("."), _T("\\."));
			s.replaceAll(_T("+"), _T("\\+"));
			s.replaceAll(_T("*"), _T(".*"));
			s.replaceAll(_T("?"), _T(".{1}"));
			s.replaceAll(_T("]"), _T("]{1}"));
			if (s[0] != '^') s.insert(0, "^");
			s += _T("$");
			re = new SWRegularExpression(s, true, caseSensitive);
			}

		return re;
		}


/**
*** Sets the filter for the directory handle.
***
*** If a filter is set it affects which directory entries
*** are returned by the read method.
***
*** Setting a filter takes effect immediately and is applied
*** to the next call to read.
***
*** The filter string is a simple wildcard filter.
*** ?		matches any single char
*** *		matches zero or more chars
*** [..]	matches any single char in the given range
***
*** @param filter	A simple filter string.
*** @see		createFilterRE
**/
void
SWFilteredFolder::setFilter(const SWString &filter)
		{
		m_filter = filter;
		if (m_filter.isEmpty()) m_filter = _T("*");

		delete m_pRegExpr;
		m_pRegExpr = createFilterRE(filter, m_caseSensitive);
		}







