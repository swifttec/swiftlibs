/**
*** @file		SWGuard.h
*** @brief		A generic object guard.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWGuard, SWReadGuard and SWWriteGuard classes.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWGuard_h__
#define __SWGuard_h__

#include <swift/SWObject.h>
#include <swift/SWMutex.h>

// Begin the namespace


/**
***	@brief	A generic object guard.
***	
*** This class locks the specified synchronisation object on construction
*** by calling the synchronisation object's acquire/lock method. On
*** destruction the synchronisation object's release/unlock method is
*** called.
***
***	This object is useful for ensuring that any acquired synchronisation object
*** is release before exiting a function/method even if this is due to an execption
*** or multiple calls to return.
***
*** @author		Simon Sparkes
**/
class SWGuard
		{
public:
		/// Destructor
		~SWGuard()
				{
				pObject->unlock();
				}

		/// Guard for an Object (pointer form)
		SWGuard(const SWObject *obj)
				{
				pObject = obj;
				pObject->lock();
				}

		/// Guard for an Object (reference form)
		SWGuard(const SWObject &obj)
				{
				pObject = &obj;
				pObject->lock();
				}

private:
		const SWObject	*pObject;	///< The object to be guarded
		};





/**
***	@brief A generic object read guard.
***	
*** This class locks the specified synchronisation object on construction
*** by calling the synchronisation object's acquireReadLock/lockForRead method. On
*** destruction the synchronisation object's release/unlock method is
*** called.
***
***	This object is useful for ensuring that any acquired synchronisation object
*** is release before exiting a function/method even if this is due to an execption
*** or multiple calls to return.
***
*** @author		Simon Sparkes
**/
class SWReadGuard
		{
public:
		/// Destructor
		~SWReadGuard()
				{
				pObject->unlock();
				}

		/// Guard for an Object (pointer form)
		SWReadGuard(const SWObject *obj)
				{
				pObject = obj;
				pObject->lockForReading();
				}

		/// Guard for an Object (reference form)
		SWReadGuard(const SWObject &obj)
				{
				pObject = &obj;
				pObject->lockForReading();
				}

private:
		const SWObject	*pObject;	///< The object to be guarded
		};


/**
***	@brief A generic object write guard.
***	
*** This class locks the specified synchronisation object on construction
*** by calling the synchronisation object's acquireWriteLock/lockForWrite method. On
*** destruction the synchronisation object's release/unlock method is
*** called.
***
***	This object is useful for ensuring that any acquired synchronisation object
*** is release before exiting a function/method even if this is due to an execption
*** or multiple calls to return.
***
*** @author		Simon Sparkes
**/
class SWWriteGuard
		{
public:
		/// Destructor
		~SWWriteGuard()
				{
				pObject->unlock();
				}

		/// Guard for an Object (pointer form)
		SWWriteGuard(const SWObject *obj)
				{
				pObject = obj;
				pObject->lockForWriting();
				}

		/// Guard for an Object (reference form)
		SWWriteGuard(const SWObject &obj)
				{
				pObject = &obj;
				pObject->lockForWriting();
				}
private:
		const SWObject	*pObject;	///< The object to be guarded
		};


/**
***	@brief	A generic object guard.
***	
*** This class locks the specified synchronisation object on construction
*** by calling the synchronisation object's acquire/lock method. On
*** destruction the synchronisation object's release/unlock method is
*** called.
***
***	This object is useful for ensuring that any acquired synchronisation object
*** is release before exiting a function/method even if this is due to an execption
*** or multiple calls to return.
***
*** @author		Simon Sparkes
**/
class SWMutexGuard
		{
public:
		/// Destructor
		~SWMutexGuard()
				{
				pMutex->release();
				}

		/// Guard for an Mutex (pointer form)
		SWMutexGuard(SWMutex *obj)
				{
				pMutex = obj;
				pMutex->acquire();
				}

		/// Guard for an Mutex (reference form)
		SWMutexGuard(SWMutex &obj)
				{
				pMutex = &obj;
				pMutex->acquire();
				}

private:
		SWMutex	*pMutex;	///< The object to be guarded
		};





/**
***	@brief A generic object read guard.
***	
*** This class locks the specified synchronisation object on construction
*** by calling the synchronisation object's acquireReadLock/lockForRead method. On
*** destruction the synchronisation object's release/unlock method is
*** called.
***
***	This object is useful for ensuring that any acquired synchronisation object
*** is release before exiting a function/method even if this is due to an execption
*** or multiple calls to return.
***
*** @author		Simon Sparkes
**/
class SWMutexReadGuard
		{
public:
		/// Destructor
		~SWMutexReadGuard()
				{
				pMutex->release();
				}

		/// Guard for an Mutex (pointer form)
		SWMutexReadGuard(SWMutex *obj)
				{
				pMutex = obj;
				pMutex->acquireReadLock();
				}

		/// Guard for an Mutex (reference form)
		SWMutexReadGuard(SWMutex &obj)
				{
				pMutex = &obj;
				pMutex->acquireReadLock();
				}

private:
		SWMutex	*pMutex;	///< The object to be guarded
		};


/**
***	@brief A generic object write guard.
***	
*** This class locks the specified synchronisation object on construction
*** by calling the synchronisation object's acquireWriteLock/lockForWrite method. On
*** destruction the synchronisation object's release/unlock method is
*** called.
***
***	This object is useful for ensuring that any acquired synchronisation object
*** is release before exiting a function/method even if this is due to an execption
*** or multiple calls to return.
***
*** @author		Simon Sparkes
**/
class SWMutexWriteGuard
		{
public:
		/// Destructor
		~SWMutexWriteGuard()
				{
				pMutex->release();
				}

		/// Guard for an Mutex (pointer form)
		SWMutexWriteGuard(SWMutex *obj)
				{
				pMutex = obj;
				pMutex->acquireWriteLock();
				}

		/// Guard for an Mutex (reference form)
		SWMutexWriteGuard(SWMutex &obj)
				{
				pMutex = &obj;
				pMutex->acquireWriteLock();
				}
private:
		SWMutex	*pMutex;	///< The object to be guarded
		};

#endif
