/**
*** @file		SWDataBuffer.cpp
*** @brief		A general purpose self-contained memory buffer
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWDataBuffer class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWDataBuffer.h>
#include <swift/SWGuard.h>
#include <swift/SWInputStream.h>
#include <swift/SWOutputStream.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





SWDataBuffer::SWDataBuffer(SWMemoryManager *pMemoryManager) :
	SWBuffer(pMemoryManager),
	m_size(0)
		{
		}


SWDataBuffer::SWDataBuffer(sw_size_t bufsize, SWMemoryManager *pMemoryManager) :
	SWBuffer(bufsize, pMemoryManager),
	m_size(0)
		{
		}



SWDataBuffer::SWDataBuffer(void *pBuf, sw_size_t bufsize) :
	SWBuffer(pBuf, bufsize),
	m_size(bufsize)
		{
		}


SWDataBuffer::SWDataBuffer(const void *pBuf, sw_size_t bufsize) :
	SWBuffer(pBuf, bufsize),
	m_size(bufsize)
		{
		}


SWDataBuffer::~SWDataBuffer()
		{
		}


SWDataBuffer::SWDataBuffer(const SWDataBuffer &other, SWMemoryManager *pMemoryManager) :
	SWBuffer(pMemoryManager),
	m_size(0)
		{
		*this = other;
		}


SWDataBuffer &
SWDataBuffer::operator=(const SWDataBuffer &other)
		{
		if (other.data() == NULL || other.size() == 0)
			{
			clear();
			}
		else
			{
			memcpy(data(other.size()), other, other.size());
			}

		m_size = other.size();

		return *this;
		}


void
SWDataBuffer::clear()
		{
		SWBuffer::clear();
		m_size = 0;
		}


sw_size_t
SWDataBuffer::size() const
		{
		return m_size;
		}


void
SWDataBuffer::size(sw_size_t size, bool preserveOldData, bool clearNewData, bool freeExtraMemory)
		{
		SWBuffer::size(size, preserveOldData, clearNewData, freeExtraMemory);
		m_size = size;
		}





