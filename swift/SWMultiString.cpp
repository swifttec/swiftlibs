/**
*** @file		SWMultiString.cpp
*** @brief		A Microsoft-like multi-string
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWMultiString class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWGuard.h>
#include <swift/SWException.h>
#include <swift/SWInputStream.h>
#include <swift/SWOutputStream.h>
#include <swift/SWTokenizer.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif

#include <swift/SWMultiString.h>


SWMultiString::SWMultiString()
		{
		clear();
		}


SWMultiString::~SWMultiString()
		{
		clear();
		}


SWMultiString::SWMultiString(const char *sp)
		{
		*this = sp;
		}


SWMultiString::SWMultiString(const wchar_t *sp)
		{
		*this = sp;
		}


SWMultiString::SWMultiString(const SWStringArray &sa)
		{
		*this = sa;
		}


SWMultiString::SWMultiString(const SWRegistryValue &rv)
		{
		*this = rv;
		}


SWMultiString::SWMultiString(const SWMultiString &other) :
	m_charsize(0),
	m_nelems(0)
		{
		// Copy the data via the assigment operator
		*this = other;
		}


SWMultiString &
SWMultiString::operator=(const SWMultiString &other)
		{
		sw_size_t	n = other.m_data.size();

		m_data.data(other.m_data.size());
		memcpy(m_data, other.m_data, n);
		m_charsize = other.m_charsize;
		m_nelems = other.m_nelems;

		return *this;
		}


SWMultiString &
SWMultiString::operator=(const char *sp)
		{
		sw_size_t	n;
		const char	*q;

		m_charsize = sizeof(*sp);
		m_nelems = 0;

		q = sp;
		while (*q)
			{
			n = strlen(q);
			q += (n + 1);
			m_nelems++;
			}

		n = (q - sp) + 1;

		m_data.size(m_charsize * n);
		memcpy(m_data, sp, n);

		return *this;
		}


SWMultiString &
SWMultiString::operator=(const wchar_t *sp)
		{
		sw_size_t		n;
		const wchar_t	*q;

		m_charsize = sizeof(*sp);
		m_nelems = 0;

		q = sp;
		while (*q)
			{
			n = wcslen(q);
			q += (n + 1);
			m_nelems++;
			}

		n = (q - sp) + 1;

		m_data.size(m_charsize * n);
		memcpy(m_data, sp, n);

		return *this;
		}


SWMultiString &
SWMultiString::operator=(const SWRegistryValue &rv)
		{
		sw_size_t		n=rv.length();

		m_nelems = 0;
		m_charsize = sizeof(sw_tchar_t);
		m_data.size(n + m_charsize);
		memcpy(m_data, rv.data(), n);
		memset((char *)m_data + n, 0, m_charsize);

		if (m_charsize == sizeof(char))
			{
			const char	*q;

			q = m_data;
			while (*q)
				{
				n = strlen(q);
				q += (n + 1);
				m_nelems++;
				}
			}
		else if (m_charsize == sizeof(wchar_t))
			{
			const wchar_t	*q;

			q = m_data;
			while (*q)
				{
				n = wcslen(q);
				q += (n + 1);
				m_nelems++;
				}
			}

		return *this;
		}



SWMultiString &
SWMultiString::operator=(const SWStringArray &sa)
		{
		clear();
		for (int i=0;i<(int)sa.size();i++) add(sa.get(i));

		return *this;
		}


void
SWMultiString::load(const SWStringArray &sa, int cs)
		{
		clear(cs);
		for (int i=0;i<(int)sa.size();i++) add(sa.get(i));
		}


void *
SWMultiString::getElement(sw_index_t pos) const
		{
		void		*pData=NULL;
		sw_size_t	n;

		if (m_charsize == sizeof(char))
			{
			const char	*q;

			q = m_data;
			while (pos > 0 && *q)
				{
				n = strlen(q);
				q += (n + 1);
				pos--;
				}

			pData = (void *)q;
			}
		else if (m_charsize == sizeof(wchar_t))
			{
			const wchar_t	*q;

			q = m_data;
			while (pos > 0 && *q)
				{
				n = wcslen(q);
				q += (n + 1);
				pos--;
				}

			pData = (void *)q;
			}

		return pData;
		}


SWString
SWMultiString::operator[](sw_index_t pos) const
		{
		return get(pos);
		}


SWString
SWMultiString::get(sw_index_t pos) const
		{
		if (pos < 0 || pos >= (sw_index_t)m_nelems) throw SW_EXCEPTION(SWIndexOutOfBoundsException);

		SWString	s;

		if (m_charsize != 0)
			{
			void		*pElement;

			pElement = getElement(pos);
			if (pElement != NULL)
				{
				if (m_charsize == sizeof(char))
					{
					s = (const char *)pElement;
					}
				else if (m_charsize == sizeof(wchar_t))
					{
					s = (const wchar_t *)pElement;
					}
				}
			}

		return s;
		}


void
SWMultiString::clear()
		{
		clear(0);
		}


void
SWMultiString::clear(int cs)
		{
		if (cs == 0) cs = sizeof(sw_tchar_t);
		m_charsize = cs;
		m_data.size(0);
		m_data.data(m_charsize * 2);
		m_nelems = 0;
		memset(m_data, 0, m_charsize * 2);
		}


/*
void
SWMultiString::remove(sw_index_t pos, sw_size_t count)
		{
		if (count > 0)
			{
			SWWriteGuard	guard(this);

			if (pos >= 0 && pos < (sw_index_t)m_nelems)
				{
				if (pos + count > m_nelems) count = m_nelems - pos;

				if (count > 0)
					{
					sw_index_t	startpos=pos, endpos=pos+(sw_index_t)count;
					char		*p = m_pData + (startpos * m_elemsize);
					char		*q = m_pData + (endpos * m_elemsize);
					sw_size_t	oldn = m_nelems;

					// Reduce the number of elems and shift all the elems down
					for (pos=endpos;pos<(sw_index_t)size();pos++)
						{
						((SWMultiStringElement *)p)->data() = ((SWMultiStringElement *)q)->data();
						p += m_elemsize;
						q += m_elemsize;
						}

					 // Blank out the rest
					pos = (sw_index_t)(m_nelems - count);
					p = m_pData + (pos * m_elemsize);
					while (pos < (sw_index_t)oldn)
						{
						delete (SWMultiStringElement *)p;
						p += m_elemsize;
						pos++;
						}

					m_nelems -= count;
					}
				}
			else throw SW_EXCEPTION(SWIndexOutOfBoundsException);
			}
		}
*/


void
SWMultiString::add(const SWString &v)
		{
		if (!v.isEmpty())
			{
			if (m_charsize == 0)
				{
				m_charsize = v.charSize();
				}

			if (m_charsize == sizeof(char))
				{
				const char	*sp=v;
				char		*dp;
				sw_size_t	l=strlen(sp);
				sw_size_t	n=(l+1) * sizeof(*sp);

				dp = (char *)m_data.data(m_data.size() + n) + m_data.size();
				m_data.size(m_data.size() + n);
				memcpy(dp, sp, n);
				dp += l;
				*dp = 0;
				}
			else if (m_charsize == sizeof(wchar_t))
				{
				const wchar_t	*sp=v;
				wchar_t			*dp;
				sw_size_t		l=wcslen(sp);
				sw_size_t		n=(l+1) * sizeof(*sp);

				dp = (wchar_t *)((char *)m_data.data(m_data.size() + n) + m_data.size());
				m_data.size(m_data.size() + n);
				memcpy(dp, sp, n);
				dp += l;
				*dp = 0;
				}
			}
		}


/*
void
SWMultiString::insert(const SWString &v, sw_index_t pos)
		{
		SWWriteGuard	guard(this);

		if (pos >= 0 && pos <= (sw_index_t)m_nelems)
			{
			ensureCapacity(m_nelems+1);

			sw_index_t	ipos = (sw_index_t)m_nelems;
			char		*p = m_pData + (ipos * m_elemsize);
			char		*q = p - m_elemsize;
			bool		exists=false;

			// Increase the number of elems by one and shift all the elems up by 1
			// not forgetting to make sure we have enough space first!
			if (ipos > pos)
				{
				new (p) SWMultiStringElement((((SWMultiStringElement *)q)->data()));
				p -= m_elemsize;
				q -= m_elemsize;
				ipos--;
				exists = true;
				}
			
			while (ipos > pos)
				{
				(((SWMultiStringElement *)p)->data()) = (((SWMultiStringElement *)q)->data());
				p -= m_elemsize;
				q -= m_elemsize;
				ipos--;
				}
				
			if (exists) ((SWMultiStringElement *)p)->data() = v;
			else new (p) SWMultiStringElement(v);

			m_nelems++;
			}
		else throw SW_EXCEPTION(SWIndexOutOfBoundsException);
		}
*/


int
SWMultiString::compareTo(const SWMultiString &other) const
		{
		SWReadGuard	guard(this);
		int			nelems1=(int)size();
		int			nelems2=(int)other.size();
		int			pos=0;
		int			res=0;

		while (res == 0 && pos < nelems1 && pos < nelems2)
			{
			res = get(pos).compareTo(other.get(pos));
			pos++;
			}

		if (res == 0)
			{
			if (nelems1 > nelems2) res = 1;
			else if (nelems1 < nelems2) res = -1;
			}

		return res;
		}



sw_status_t
SWMultiString::writeToStream(SWOutputStream &stream) const
		{
		SWReadGuard	guard(this);
		sw_status_t	res;

		res = stream.write((sw_uint32_t)size());
		for (int i=0;res==SW_STATUS_SUCCESS&&i<(int)size();i++)
			{
			res = get(i).writeToStream(stream);
			}

		return res;
		}


sw_status_t
SWMultiString::readFromStream(SWInputStream &stream)
		{
		SWWriteGuard	guard(this);
		sw_status_t		res;
		sw_uint32_t		i, nelems;
		SWString		s;

		clear();
		res = stream.read(nelems);
		for (i=0;res==SW_STATUS_SUCCESS&&i<nelems;i++)
			{
			res = s.readFromStream(stream);
			if (res == SW_STATUS_SUCCESS) add(s);
			}

		return res;
		}


/*
** Turn the the string array into a string with the elements separated 
** with the supplied separator character
**
** @param sep		The separator char to be inserted between the elements
** @param squote	The char to use to prefix each element
** @param equote	The char to use to postfix each element
** @param meta		The char to prefix any quote string with
**
** @return The assembled string
*/
SWString
SWMultiString::convertToString(int sep, int squote, int equote, int meta)
		{
		SWString		str, tmp;
		SWString		rsquote, requote, rmeta, rsep;	// The replacement strings
		const char		*cp=m_data;
		const wchar_t	*wp=m_data;
		bool			first=true;

		if (sep) rsep.format(_T("%c%c"), meta, sep);
		if (squote) rsquote.format(_T("%c%c"), meta, squote);
		if (equote) requote.format(_T("%c%c"), meta, equote);
		if (meta) rmeta.format(_T("%c%c"), meta, meta);

		for (;;)
			{
			if (m_charsize == sizeof(char))
				{
				if (!*cp) break;
				tmp = cp;
				cp += strlen(cp) + 1;
				}
			else if (m_charsize == sizeof(wchar_t))
				{
				if (!*wp) break;
				tmp = wp;
				wp += wcslen(wp) + 1;
				}
			else break;

			if (!first) str += sep;
			first = false;

			// First if we have a quote string replace all instances of itself
			// with a double version of itself.
			if (meta) tmp.replaceAll(SWString(meta), rmeta);

			// if we have a separator string and don't have quote strings
			// then replace all the separator chars in the strings
			if (sep && meta && !squote && !equote) tmp.replaceAll(SWString(sep), rsep);

			// Finally replace all the contained quotes
			if (squote && meta) tmp.replaceAll(SWString(squote), rsquote);
			if (equote && meta && squote != equote) tmp.replaceAll(SWString(equote), requote);

			// Now we can append to the result string
			if (squote) str += squote;
			str += tmp;
			if (equote) str += equote;
			}
		
		return str;
		}


void
SWMultiString::toStringArray(SWStringArray &sa)
		{
		sa.clear();

		if (m_charsize == sizeof(char))
			{
			const char	*sp=m_data;

			while (*sp)
				{
				sa.add(sp);
				sp += strlen(sp)+1;
				}
			}
		else if (m_charsize == sizeof(wchar_t))
			{
			const wchar_t	*sp=m_data;

			while (*sp)
				{
				sa.add(sp);
				sp += wcslen(sp)+1;
				}
			}
		}


/*
** Take the string given and load the array from it by successive
** calls to add.
**
** @param	s	The string to be split.
** @param	sep	The separator character (default ',')
** @param	squote	The start-of-quoted string character (default '"')
** @param	equote	The end-of-quoted string character (default '"')
** @param	meta	The meta character which can be used to prefix separator and quote chars. (default '\')
** @param	append	A flag to indicate if the strings should be appended to the array (true) or if the array 
**			should be cleared before starting the split (false). The default action is to clear the
**			array.
*/
void
SWMultiString::convertFromString(const SWString &s, int sep, int squote, int equote, int meta, bool append)
		{
		SWString	tmp(s);
		SWString	sepChars;

		sepChars = sep;

		SWTokenizer tok(tmp, sepChars, false, false);

		tok.addQuotes(squote, equote);
		tok.setMetaChar(meta);

		if (!append) clear();
		while (tok.hasMoreTokens()) add(tok.nextToken());
		}





