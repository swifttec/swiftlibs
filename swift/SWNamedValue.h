/**
*** @file		SWNamedValue.h
*** @brief		A SWValue object with an associated name.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWNamedValue class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWNamedValue_h__
#define __SWNamedValue_h__

#include <swift/SWValue.h>



// Forward declarations

/**
*** @brief	A SWValue object (variant) with an associated name.
***
*** This class stores a name/value pair. The object value is implemented
*** via the SWValue class which can take any data format.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWNamedValue : public SWValue
		{
public:
		/// Default constructor
		SWNamedValue(const SWString &name=_T(""));

		/// Virtual destructor
		virtual ~SWNamedValue();

		SWNamedValue(const SWString &name, const SWValue &v);

		/// @brief	Constructs a named value given the integer value and related integer type.
		SWNamedValue(const SWString &name, int v, ValueType type);

		SWNamedValue(const SWString &name, bool v);						///< Constructs a named value with the type VtNone
		SWNamedValue(const SWString &name, sw_int8_t v);				///< Constructs a named value with the type VtInt8
		SWNamedValue(const SWString &name, sw_int16_t v);				///< Constructs a named value with the type VtInt16
		SWNamedValue(const SWString &name, sw_int32_t v);				///< Constructs a named value with the type VtInt16
		SWNamedValue(const SWString &name, sw_int64_t v);				///< Constructs a named value with the type VtInt16
		SWNamedValue(const SWString &name, sw_uint8_t v);				///< Constructs a named value with the type VtUInt8
		SWNamedValue(const SWString &name, sw_uint16_t v);				///< Constructs a named value with the type VtUInt8
		SWNamedValue(const SWString &name, sw_uint32_t v);				///< Constructs a named value with the type VtUInt8
		SWNamedValue(const SWString &name, sw_uint64_t v);				///< Constructs a named value with the type VtUInt8
		SWNamedValue(const SWString &name, float v);					///< Constructs a named value with the type VtFloat
		SWNamedValue(const SWString &name, double v);					///< Constructs a named value with the type VtDouble
		SWNamedValue(const SWString &name, const char *v);				///< Constructs a named value with the type VtString
		SWNamedValue(const SWString &name, const wchar_t *v);		///< Constructs a named value with the type VtDString
		SWNamedValue(const SWString &name, const void *, size_t len);	///< Constructs a named value with the type VtData from the data pointer and size
		SWNamedValue(const SWString &name, SWBuffer &v);				///< Constructs a named value with the type VtData from SWBuffer
		SWNamedValue(const SWString &name, const SWString &v);			///< Constructs a named value with a string type of VtString, VtDString or VtQString depending on the SWString

		SWNamedValue(const SWString &name, const SWDate &v);		///< Constructs a value with the type VtDate
		SWNamedValue(const SWString &name, const SWTime &v);		///< Constructs a value with the type VtTime
		SWNamedValue(const SWString &name, const SWMoney &v);		///< Constructs a value with the type VtMoney

#if defined(SW_BUILD_MFC)
		SWNamedValue(const SWString &name, const COleVariant &v);
		SWNamedValue(const SWString &name, const CString &v);
#endif

		/// Copy constructor
		SWNamedValue(const SWNamedValue &v);

		/// Assignment Operator (copies both name and value)
		SWNamedValue &		operator=(const SWNamedValue &v);

		/// Assignment from SWValue object (no name change - value only!)
		SWNamedValue &		operator=(const SWValue &v);

		/// Returns the name of this value
		const SWString &	name() const			{ return _name; }

		/// Sets the name of this value
		void				name(const SWString &v)	{ _name = v; }

public: // Virtual overrides

		/// Override for SWObject writeToStream
		virtual sw_status_t		writeToStream(SWOutputStream &stream) const;

		/// Override for SWObject readFromStream
		virtual sw_status_t		readFromStream(SWInputStream &stream);

protected:
		SWString	_name;	///< The name of this value
		};



#endif
