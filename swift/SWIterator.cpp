/**
*** @file		SWIterator.cpp
*** @brief		Templated iterator for templated collections.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWIterator class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWIterator_cpp__
#define __SWIterator_cpp__

#include <swift/SWIterator.h>
#include <swift/SWCollection.h>
#include <swift/SWCollectionIterator.h>
#include <swift/SWException.h>



template<class T>
SWIterator<T>::SWIterator()
		{
		}


template<class T>
SWIterator<T>::SWIterator(const SWIterator<T> &other) :
	SWAbstractIterator(other)
		{
		}


template<class T> SWIterator<T> &
SWIterator<T>::operator=(const SWIterator<T> &other)
		{
		SWAbstractIterator::operator=(other);

		return *this;
		}


template<class T> T *
SWIterator<T>::current()
		{
		if (!m_pIterator) throw SW_EXCEPTION(SWNoSuchElementException);
		return (T *)m_pIterator->current();
		}


template<class T> T *
SWIterator<T>::next()
		{
		if (!m_pIterator) throw SW_EXCEPTION(SWNoSuchElementException);
		return (T *)m_pIterator->next();
		}


template<class T> T *
SWIterator<T>::previous()
		{
		if (!m_pIterator) throw SW_EXCEPTION(SWNoSuchElementException);
		return (T *)m_pIterator->previous();
		}


template<class T> bool
SWIterator<T>::add(const T &data, SWAbstractIterator::AddLocation where)
		{
		if (!m_pIterator) throw SW_EXCEPTION(SWIllegalStateException);

		return m_pIterator->add((void *)&data, where);
		}


template<class T> bool
SWIterator<T>::set(const T &data, T *pOldData)
		{
		if (!m_pIterator) throw SW_EXCEPTION(SWIllegalStateException);

		return m_pIterator->set(&data, pOldData);
		}


template<class T> bool
SWIterator<T>::remove(T *pOldData)
		{
		if (!m_pIterator) throw SW_EXCEPTION(SWIllegalStateException);

		return m_pIterator->remove(pOldData);
		}




#endif
