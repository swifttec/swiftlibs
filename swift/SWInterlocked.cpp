/**
*** @file		SWInterlocked.cpp
*** @brief		An interlocked variable.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWInterlocked class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/





#include "stdafx.h"

#include <swift/SWInterlocked.h>
#include <swift/SWException.h>
#include <swift/SWThreadMutex.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif



SWMutex	*SWInterlocked::__mutex=NULL;
int		SWInterlocked::__refcount=0;


// Default constructor
SWInterlocked::SWInterlocked()
		{
		if (__mutex == NULL)
			{
			__mutex = new SWThreadMutex(true);
			__refcount = 1;
			}
		else
			{
			__mutex->acquire();
			__refcount++;
			__mutex->release();
			}
		}


SWInterlocked::~SWInterlocked()
		{
		__mutex->acquire();
		--__refcount;

		if (__refcount == 0)
			{
			// That was the last reference
			__mutex->release();

			/* This may be risky so we won't do it for now.
			delete __mutex;
			__mutex = NULL;
			*/
			}
		else __mutex->release();
		}


// Lock the object (if synchronised).
sw_status_t
SWInterlocked::lock(sw_uint64_t waitms) const
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (__mutex != NULL)
			{
			res = __mutex->acquire(waitms);
			}

		return res;
		}


// Lock the object for reading (if synchronised).
sw_status_t
SWInterlocked::lockForReading(sw_uint64_t waitms) const
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (__mutex != NULL)
			{
			res = __mutex->acquireReadLock(waitms);
			}

		return res;
		}


// Lock the object for writing (if synchronised).
sw_status_t
SWInterlocked::lockForWriting(sw_uint64_t waitms) const
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (__mutex != NULL)
			{
			res = __mutex->acquireWriteLock(waitms);
			}

		return res;
		}


// Unlock the object (if synchronised).
sw_status_t
SWInterlocked::unlock() const
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (__mutex != NULL)
			{
			res = __mutex->release();
			}

		return res;
		}



