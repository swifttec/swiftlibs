/**
*** @file		SWSimpleWString.cpp
*** @brief		Implements a class for a simple single-byte char string.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWSimpleWString class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWSimpleWString.h>
#include <swift/sw_misc.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




SWSimpleWString::SWSimpleWString() :
	m_sp(0)
		{
		m_sp = wcsnew("");
		}


SWSimpleWString::SWSimpleWString(const char *sp) :
	m_sp(0)
		{
		m_sp = wcsnew(sp);
		}


SWSimpleWString::SWSimpleWString(const wchar_t *sp) :
	m_sp(0)
		{
		m_sp = wcsnew(sp);
		}


SWSimpleWString::SWSimpleWString(const SWSimpleWString &s) :
	m_sp(0)
		{
		m_sp = wcsnew(s.m_sp);
		}


SWSimpleWString::~SWSimpleWString()
		{
		delete [] m_sp;
		m_sp = NULL;
		}


SWSimpleWString &
SWSimpleWString::operator=(const char *sp)
		{
		delete [] m_sp;
		m_sp = wcsnew(sp);
		return *this;
		}


SWSimpleWString &
SWSimpleWString::operator=(const wchar_t *sp)
		{
		delete [] m_sp;
		m_sp = wcsnew(sp);
		return *this;
		}


SWSimpleWString &
SWSimpleWString::operator=(const SWSimpleWString &s)
		{
		delete [] m_sp;
		m_sp = wcsnew(s.m_sp);
		return *this;
		}


sw_size_t
SWSimpleWString::length() const
		{
		return wcslen(m_sp);
		}





