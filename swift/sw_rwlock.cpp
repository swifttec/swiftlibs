/**
*** @file		sw_rwlock.cpp
*** @brief		Implement various functions which are missing from a given platform.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the various functions missing from certain platforms.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/sw_rwlock.h>
#include <swift/sw_shrobj.h>
#include <swift/SWSharedMemoryManager.h>
#include <swift/SWThreadMutex.h>
#include <swift/SWThread.h>
#include <swift/SWProcess.h>
#include <swift/SWTime.h>


#if defined(SW_USE_SOLARIS_MUTEXES)
	#include <thread.h>
	#include <synch.h>

	typedef mutex_t				sw_shrobj_mutex_t;
#elif defined(SW_USE_POSIX_MUTEXES)
	#include <pthread.h>

	typedef pthread_mutex_t		sw_shrobj_mutex_t;
#endif


// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif





#if defined(SW_USE_EMULATED_RWLOCKS)
static sw_uint32_t
sw_rwlock_getCallerId(sw_rwlock_t *lock)
		{
		sw_uint32_t	id=0;

		if (lock->type == SW_RWLOCK_THREAD)
			{
			id = (sw_uint32_t)SWThread::self();
			}
		else
			{
			id = (sw_uint32_t)SWProcess::self();
			}

		return id;
		}
#endif


SWIFT_DLL_EXPORT sw_status_t
sw_rwlock_create(sw_rwlock_t *&lock, int type, const sw_tchar_t *name)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		lock = NULL;
		if (type != SW_RWLOCK_THREAD && type != SW_RWLOCK_PROCESS) res = SW_STATUS_INVALID_PARAMETER;

		if (res == SW_STATUS_SUCCESS)
			{
			lock = new sw_rwlock_t;
			if (lock != NULL)
				{
				memset(lock, 0, sizeof(*lock));
				lock->type = type;
				}
			else res = SW_STATUS_OUT_OF_MEMORY;
			}

		if (res == SW_STATUS_SUCCESS)
			{
			if (type == SW_RWLOCK_THREAD)
				{
				lock->pData = new sw_rwlock_data_t;
				if (lock->pData == NULL)
					{
					res = SW_STATUS_OUT_OF_MEMORY;
					}
				else
					{
					memset(lock->pData, 0, sizeof(sw_rwlock_data_t));
					}

				if (res == SW_STATUS_SUCCESS)
					{
#if defined(SW_USE_SOLARIS_RWLOCKS)
					if (rwlock_init(lock->pData, USYNC_THREAD, 0) != 0) res = errno;
#elif defined(SW_USE_POSIX_RWLOCKS)
					pthread_rwlockattr_t	mattr;

					pthread_rwlockattr_init(&mattr);
					pthread_rwlockattr_setpshared(&mattr, PTHREAD_PROCESS_PRIVATE);

					if (pthread_rwlock_init(lock->pData, &mattr) != 0) res = errno;
					pthread_rwlockattr_destroy(&mattr);
#elif defined(SW_USE_EMULATED_RWLOCKS)
	#if defined(SW_PLATFORM_WINDOWS)
					// The mutex, event, and data are all local to this process.
					// The name is ignored
					lock->hMutex = CreateMutex(NULL, FALSE, NULL);
					lock->hEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
	#else // defined(SW_PLATFORM_WINDOWS)
					pthread_mutexattr_t	mattr;
					pthread_condattr_t	cattr;

					pthread_mutexattr_init(&mattr);
					pthread_condattr_init(&cattr);

					pthread_mutexattr_setpshared(&mattr, PTHREAD_PROCESS_PRIVATE);
					pthread_condattr_setpshared(&cattr, PTHREAD_PROCESS_PRIVATE);

					pthread_mutex_init(&lock->pData->mutex, &mattr);
					pthread_cond_init(&lock->pData->cond, &cattr);

					pthread_mutexattr_destroy(&mattr);
					pthread_condattr_destroy(&cattr);
	#endif // defined(SW_PLATFORM_WINDOWS)
#else
					res = SW_STATUS_UNSUPPORTED_OPERATION;
#endif

					if (res != SW_STATUS_SUCCESS)
						{
						delete lock->pData;
						}
					}
				}
			else
				{
				// The mutex, event, are local to this process, but reference a named resource
				// The data is a named reference to a shared object (via sw_shrobj interface)
				res = sw_shrobj_attach(name, SW_SHROBJ_TYPE_PROCESS_RWLOCK, lock->hData);
				if (res != SW_STATUS_SUCCESS)
					{
					res = sw_shrobj_create(name, SW_SHROBJ_TYPE_PROCESS_RWLOCK, sizeof(sw_rwlock_data_t), lock->hData);
					if (res == SW_STATUS_SUCCESS)
						{
						// We have just created the data, we must initialise it
						lock->pData = (sw_rwlock_data_t *)sw_shrobj_data(lock->hData);
						memset(lock->pData, 0, sizeof(sw_rwlock_data_t));

#if defined(SW_USE_SOLARIS_RWLOCKS)
						if (rwlock_init(lock->pData, USYNC_PROCESS, 0) != 0) res = errno;
#elif defined(SW_USE_POSIX_RWLOCKS)
						pthread_rwlockattr_t	mattr;

						pthread_rwlockattr_init(&mattr);
						pthread_rwlockattr_setpshared(&mattr, PTHREAD_PROCESS_SHARED);

						if (pthread_rwlock_init(lock->pData, &mattr) != 0) res = errno;
						pthread_rwlockattr_destroy(&mattr);
#elif defined(SW_USE_EMULATED_RWLOCKS)
	#if !defined(SW_PLATFORM_WINDOWS)
						pthread_mutexattr_t	mattr;
						pthread_condattr_t	cattr;

						pthread_mutexattr_init(&mattr);
						pthread_condattr_init(&cattr);

						pthread_mutexattr_setpshared(&mattr, PTHREAD_PROCESS_SHARED);
						pthread_condattr_setpshared(&cattr, PTHREAD_PROCESS_SHARED);

						pthread_mutex_init(&lock->pData->mutex, &mattr);
						pthread_cond_init(&lock->pData->cond, &cattr);

						pthread_mutexattr_destroy(&mattr);
						pthread_condattr_destroy(&cattr);
	#endif // !defined(SW_PLATFORM_WINDOWS)
#else
						res = SW_STATUS_UNSUPPORTED_OPERATION;
#endif

						if (res != SW_STATUS_SUCCESS)
							{
							sw_shrobj_detach(lock->hData);
							lock->hData = 0;
							}
						}
					}
				else
					{
					// Get the address of the data
					lock->pData = (sw_rwlock_data_t *)sw_shrobj_data(lock->hData);
					}

#if defined(SW_USE_EMULATED_RWLOCKS) && defined(SW_PLATFORM_WINDOWS)
				if (res == SW_STATUS_SUCCESS)
					{
					SWString	tmp;

					tmp = name;
					tmp += "_rwlock_mutex";
					lock->hMutex = CreateMutex(NULL, FALSE, tmp);

					tmp = name;
					tmp += "_rwlock_event";
					lock->hEvent = CreateEvent(NULL, TRUE, FALSE, tmp);
					}
#endif
				}


	#if defined(SW_PLATFORM_WINDOWS)
			#define ACQUIRE_LOCK(pLock)	WaitForSingleObject(pLock->hMutex, INFINITE);
			#define RELEASE_LOCK(pLock)	ReleaseMutex(pLock->hMutex);
	#else // defined(SW_PLATFORM_WINDOWS)
			#define ACQUIRE_LOCK(pLock)	pthread_mutex_lock(&pLock->pData->mutex);
			#define RELEASE_LOCK(pLock)	pthread_mutex_unlock(&pLock->pData->mutex);
	#endif // defined(SW_PLATFORM_WINDOWS)
			}

		if (res != SW_STATUS_SUCCESS && lock != NULL)
			{
			delete lock;
			lock = NULL;
			}

		return res;
		}


SWIFT_DLL_EXPORT sw_status_t
sw_rwlock_destroy(sw_rwlock_t *lock)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (lock->type == SW_RWLOCK_THREAD)
			{
			// Destroy the associated storage
#if defined(SW_USE_EMULATED_RWLOCKS) && !defined(SW_PLATFORM_WINDOWS)
			pthread_cond_destroy(&lock->pData->cond);
			pthread_mutex_destroy(&lock->pData->mutex);
#elif defined(SW_USE_SOLARIS_RWLOCKS)
			if (rwlock_destroy(lock->pData) != 0) res = errno;
#elif defined(SW_USE_POSIX_RWLOCKS)
			if (pthread_rwlock_destroy(lock->pData) != 0) res = errno;
#endif

#if defined(SW_USE_SOLARIS_RWLOCKS) || defined(SW_USE_POSIX_RWLOCKS)
			delete lock->pData;
#endif
			}
		else
			{
			// Don't destroy the mutex here in case another process is using it.
			sw_shrobj_detach(lock->hData);
			}

#if defined(SW_USE_EMULATED_RWLOCKS) && defined(SW_PLATFORM_WINDOWS)
		// For windows we simply close the handles
		CloseHandle(lock->hMutex);
		CloseHandle(lock->hEvent);
#endif

		lock->pData = NULL;
		lock->hData = NULL;
		delete lock;

		return res;
		}



/*
** The code for sw_rwlock_acquire was getting complicated to read with a large
** number of ifdef sections. It was split to make it easier to understand and follow.
** This requires more discipline if the code is chanegd, but should be easier for
** support.
*/


#if defined(SW_USE_EMULATED_RWLOCKS)
/*
** Emulated RW locks
*/

static sw_status_t
sw_rwlock_internal_acquire_read(sw_rwlock_t *lock, sw_uint64_t waitms)
		{
		sw_status_t			res=SW_STATUS_SUCCESS;
		sw_rwlock_data_t	*pData=lock->pData;

		ACQUIRE_LOCK(lock);

		while (res != SW_STATUS_TIMEOUT && (pData->writerid != 0 || (pData->wwaiting > 0 && pData->wtaken == 0)))
			{
			pData->rwaiting++;

#if defined(SW_PLATFORM_WINDOWS)
			DWORD	t, r;

			t = (waitms == SW_TIMEOUT_INFINITE)?INFINITE:(DWORD)waitms;
			RELEASE_LOCK(lock);
			SWThread::yield();
			r = WaitForSingleObject(lock->hEvent, t);
			ACQUIRE_LOCK(lock);

			if (r == WAIT_TIMEOUT) res = SW_STATUS_TIMEOUT;
			else if (r == WAIT_OBJECT_0) res = SW_STATUS_SUCCESS;
			else res = GetLastError();
#else // defined(SW_PLATFORM_WINDOWS)
			pthread_cond_wait(&pData->cond, &pData->mutex);
#endif // defined(SW_PLATFORM_WINDOWS)

			pData->rwaiting--;

#if defined(SW_PLATFORM_WINDOWS)
			if (pData->rwaiting == 0 && pData->wwaiting == 0)
				{
				ResetEvent(lock->hEvent);
				}
#endif // defined(SW_PLATFORM_WINDOWS)
			}

		if (res == SW_STATUS_SUCCESS)
			{
			pData->nreaders++;
			if (pData->rwaiting == 0) pData->wtaken = 0;
			}

		RELEASE_LOCK(lock);

		return res;
		}


static sw_status_t
sw_rwlock_internal_acquire_write(sw_rwlock_t *lock, sw_uint64_t waitms)
		{
		sw_status_t			res=SW_STATUS_SUCCESS;
		sw_rwlock_data_t	*pData=lock->pData;

		ACQUIRE_LOCK(lock);

		while (res != SW_STATUS_TIMEOUT && (pData->writerid > 0 || pData->nreaders > 0))
			{
			pData->wwaiting++;
#if defined(SW_PLATFORM_WINDOWS)
			DWORD	t, r;

			t = (waitms == SW_TIMEOUT_INFINITE)?INFINITE:(DWORD)waitms;
			RELEASE_LOCK(lock);
			SWThread::yield();
			r = WaitForSingleObject(lock->hEvent, t);
			ACQUIRE_LOCK(lock);

			if (r == WAIT_TIMEOUT) res = SW_STATUS_TIMEOUT;
			else if (r == WAIT_OBJECT_0) res = SW_STATUS_SUCCESS;
			else res = GetLastError();
#else // defined(SW_PLATFORM_WINDOWS)
			pthread_cond_wait(&pData->cond, &pData->mutex);
#endif // defined(SW_PLATFORM_WINDOWS)
			pData->wwaiting--;

#if defined(SW_PLATFORM_WINDOWS)
			if (pData->rwaiting == 0 && pData->wwaiting == 0)
				{
				ResetEvent(lock->hEvent);
				}
#endif // defined(SW_PLATFORM_WINDOWS)
			}

#if defined(SW_PLATFORM_WINDOWS)
		// Reset the event to stop other threads spinning
		ResetEvent(lock->hEvent);
#endif // defined(SW_PLATFORM_WINDOWS)

		if (res == SW_STATUS_SUCCESS)
			{
			pData->writerid = sw_rwlock_getCallerId(lock);
			pData->wtaken++;
			}

		RELEASE_LOCK(lock);

		return res;
		}


static sw_status_t
sw_rwlock_internal_acquire_write_upgrade(sw_rwlock_t *lock, sw_uint64_t /*waitms*/)
		{
		sw_status_t			res=SW_STATUS_SUCCESS;
		sw_rwlock_data_t	*pData=lock->pData;

		ACQUIRE_LOCK(lock);

		if (pData->writerid == 0 && pData->nreaders == 1)
			{
#if defined(SW_PLATFORM_WINDOWS)
			// Reset the event to stop other threads spinning
			ResetEvent(lock->hEvent);
#endif // defined(SW_PLATFORM_WINDOWS)

			pData->writerid = sw_rwlock_getCallerId(lock);
			pData->wtaken++;
			pData->nreaders--;
			}
		else
			{
			res = SW_STATUS_CANNOT_UPGRADE;
			}

		RELEASE_LOCK(lock);

		return res;
		}


static sw_status_t
sw_rwlock_internal_acquire_read_downgrade(sw_rwlock_t *lock, sw_uint64_t /*waitms*/)
		{
		sw_rwlock_data_t	*pData=lock->pData;

		ACQUIRE_LOCK(lock);

		pData->writerid = 0;
		pData->nreaders++;

		RELEASE_LOCK(lock);

		return SW_STATUS_SUCCESS;
		}


#elif defined(SW_USE_SOLARIS_RWLOCKS) || defined(SW_USE_POSIX_RWLOCKS)
/*
** Solaris and POSIX RW locks
**
** The are very similar so we put them together
*/

static sw_status_t
sw_rwlock_internal_acquire_read(sw_rwlock_t *lock, sw_uint64_t waitms)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (waitms == SW_TIMEOUT_INFINITE)
			{
#if defined(SW_USE_SOLARIS_RWLOCKS)
			if (rw_rdlock(lock->pData) != 0) res = errno;
#else
			if (pthread_rwlock_rdlock(lock->pData) != 0) res = errno;
#endif
			}
		else
			{
			SWTime	stime, etime;

			stime.update();
			for (;;)
				{
#if defined(SW_USE_SOLARIS_RWLOCKS)
				if (rw_tryrdlock(lock->pData) != 0)
#else
				if (pthread_rwlock_tryrdlock(lock->pData) != 0)
#endif
					{
					// According to the documentation we should only get certain errno
					// values but in practice it is not true. Seems we can get errno 0,
					// 2 and 13 in addition to EBUSY.
					etime.update();

					sw_uint64_t	elapsed = (sw_uint64_t)(((double)etime - (double)stime) * 1000.0);
					if (elapsed >= waitms)
						{
						res = SW_STATUS_TIMEOUT;
						break;
						}
					else
						{
						// Sleep for 1ms and try again
						SWThread::sleep(1);
						}
					}
				else
					{
					// We have the lock!
					break;
					}
				}
			}

		return res;
		}


static sw_status_t
sw_rwlock_internal_acquire_write(sw_rwlock_t *lock, sw_uint64_t waitms)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (waitms == SW_TIMEOUT_INFINITE)
			{
#if defined(SW_USE_SOLARIS_RWLOCKS)
			if (rw_wrlock(lock->pData) != 0) res = errno;
#else
			if (pthread_rwlock_wrlock(lock->pData) != 0) res = errno;
#endif
			}
		else
			{
			SWTime	stime, etime;

			stime.update();
			for (;;)
				{
#if defined(SW_USE_SOLARIS_RWLOCKS)
				if (rw_trywrlock(lock->pData) != 0)
#else
				if (pthread_rwlock_trywrlock(lock->pData) != 0)
#endif
					{
					// According to the documentation we should only get certain errno
					// values but in practice it is not true. Seems we can get errno 0,
					// 2 and 13 in addition to EBUSY.
					etime.update();

					sw_uint64_t	elapsed = (sw_uint64_t)(((double)etime - (double)stime) * 1000.0);
					if (elapsed >= waitms)
						{
						res = SW_STATUS_TIMEOUT;
						break;
						}
					else
						{
						// Sleep for 1ms and try again
						SWThread::sleep(1);
						}
					}
				else
					{
					// We have the lock!
					break;
					}
				}
			}
		return res;
		}


static sw_status_t
sw_rwlock_internal_acquire_write_upgrade(sw_rwlock_t *lock, sw_uint64_t waitms)
		{
		return SW_STATUS_UNSUPPORTED_OPERATION;
		}


static sw_status_t
sw_rwlock_internal_acquire_read_downgrade(sw_rwlock_t *lock, sw_uint64_t waitms)
		{
		return SW_STATUS_UNSUPPORTED_OPERATION;
		}

#else
	#error Unsupported platform - please update sw_rwlock.cpp
#endif






/*
SWIFT_DLL_EXPORT sw_status_t
sw_rwlock_acquire(sw_rwlock_t *lock, int locktype, sw_uint64_t waitms)
		{
		sw_status_t			res=SW_STATUS_SUCCESS;

#if defined(SW_USE_EMULATED_RWLOCKS)
		sw_rwlock_data_t	*pData=lock->pData;
#endif // defined(SW_USE_EMULATED_RWLOCKS)

		switch (locktype)
			{
			case SW_RWLOCK_READ:
#if defined(SW_USE_EMULATED_RWLOCKS)
				ACQUIRE_LOCK(lock);

				while (pData->writerid != 0 || (pData->wwaiting > 0 && pData->wtaken == 0))
					{
					pData->rwaiting++;

	#if defined(SW_PLATFORM_WINDOWS)
					RELEASE_LOCK(lock);
					SWThread::yield();
					WaitForSingleObject(lock->hEvent, INFINITE);
					ACQUIRE_LOCK(lock);
	#else // defined(SW_PLATFORM_WINDOWS)
					pthread_cond_wait(&pData->cond, &pData->mutex);
	#endif // defined(SW_PLATFORM_WINDOWS)

					pData->rwaiting--;

	#if defined(SW_PLATFORM_WINDOWS)
					if (pData->rwaiting == 0 && pData->wwaiting == 0)
						{
						ResetEvent(lock->hEvent);
						}
	#endif // defined(SW_PLATFORM_WINDOWS)
					}

				pData->nreaders++;
				if (pData->rwaiting == 0) pData->wtaken = 0;

				RELEASE_LOCK(lock);
#elif defined(SW_USE_SOLARIS_RWLOCKS)
				if (rw_rdlock(lock->pData) != 0) res = errno;
#elif defined(SW_USE_POSIX_RWLOCKS)
				if (pthread_rwlock_rdlock(lock->pData) != 0) res = errno;
#else
	#error Unsupported platform - please update sw_rwlock.cpp
#endif
				break;

			case SW_RWLOCK_READ_NBLOCK:
#if defined(SW_USE_EMULATED_RWLOCKS)
				ACQUIRE_LOCK(lock);

				if (pData->writerid == 0 && pData->wwaiting == 0)
					{
					// There are no writers, and no writers waiting
					pData->nreaders++;
					pData->wtaken = 0;
					}
				else
					{
					res = SW_STATUS_ALREADY_EXISTS;
					}

				RELEASE_LOCK(lock);
#elif defined(SW_USE_SOLARIS_RWLOCKS)
				if (rw_tryrdlock(lock->pData) != 0) res = errno;
#elif defined(SW_USE_POSIX_RWLOCKS)
				if (pthread_rwlock_tryrdlock(lock->pData) != 0) res = errno;
#else
	#error Unsupported platform - please update sw_rwlock.cpp
#endif
				break;

			case SW_RWLOCK_WRITE:
#if defined(SW_USE_EMULATED_RWLOCKS)
				ACQUIRE_LOCK(lock);

				while (pData->writerid > 0 || pData->nreaders > 0)
					{
					pData->wwaiting++;
	#if defined(SW_PLATFORM_WINDOWS)
					RELEASE_LOCK(lock);
					SWThread::yield();
					WaitForSingleObject(lock->hEvent, INFINITE);
					ACQUIRE_LOCK(lock);
	#else // defined(SW_PLATFORM_WINDOWS)
					pthread_cond_wait(&pData->cond, &pData->mutex);
	#endif // defined(SW_PLATFORM_WINDOWS)
					pData->wwaiting--;

	#if defined(SW_PLATFORM_WINDOWS)
					if (pData->rwaiting == 0 && pData->wwaiting == 0)
						{
						ResetEvent(lock->hEvent);
						}
	#endif // defined(SW_PLATFORM_WINDOWS)
					}

	#if defined(SW_PLATFORM_WINDOWS)
				// Reset the event to stop other threads spinning
				ResetEvent(lock->hEvent);
	#endif // defined(SW_PLATFORM_WINDOWS)

				pData->writerid = sw_rwlock_getCallerId(lock);
				pData->wtaken++;

				RELEASE_LOCK(lock);
#elif defined(SW_USE_SOLARIS_RWLOCKS)
				if (rw_wrlock(lock->pData) != 0) res = errno;
#elif defined(SW_USE_POSIX_RWLOCKS)
				if (pthread_rwlock_wrlock(lock->pData) != 0) res = errno;
#else
	#error Unsupported platform - please update sw_rwlock.cpp
#endif
				break;

			case SW_RWLOCK_WRITE_NBLOCK:
#if defined(SW_USE_EMULATED_RWLOCKS)
				ACQUIRE_LOCK(lock);

				if (pData->writerid == 0 && pData->nreaders == 0)
					{
	#if defined(SW_PLATFORM_WINDOWS)
				// Reset the event to stop other threads spinning
				ResetEvent(lock->hEvent);
	#endif // defined(SW_PLATFORM_WINDOWS)

					pData->writerid = sw_rwlock_getCallerId(lock);
					pData->wtaken++;
					}
				else
					{
					res = SW_STATUS_ALREADY_EXISTS;
					}

				RELEASE_LOCK(lock);
#elif defined(SW_USE_SOLARIS_RWLOCKS)
				if (rw_trywrlock(lock->pData) != 0) res = errno;
#elif defined(SW_USE_POSIX_RWLOCKS)
				if (pthread_rwlock_trywrlock(lock->pData) != 0) res = errno;
#else
	#error Unsupported platform - please update sw_rwlock.cpp
#endif
				break;

			case SW_RWLOCK_WRITE_UPGRADE:
#if defined(SW_USE_EMULATED_RWLOCKS)
				ACQUIRE_LOCK(lock);

				if (pData->writerid == 0 && pData->nreaders == 1)
					{
	#if defined(SW_PLATFORM_WINDOWS)
				// Reset the event to stop other threads spinning
				ResetEvent(lock->hEvent);
	#endif // defined(SW_PLATFORM_WINDOWS)

					pData->writerid = sw_rwlock_getCallerId(lock);
					pData->wtaken++;
					pData->nreaders--;
					}
				else
					{
					res = SW_STATUS_SWNNOT_UPGRADE;
					}

				RELEASE_LOCK(lock);
#elif defined(SW_USE_SOLARIS_RWLOCKS) || defined(SW_USE_POSIX_RWLOCKS)
				res = SW_STATUS_UNSUPPORTED_OPERATION;
#else
	#error Unsupported platform - please update sw_rwlock.cpp
#endif
				break;

			case SW_RWLOCK_READ_DOWNGRADE:
#if defined(SW_USE_EMULATED_RWLOCKS)
				ACQUIRE_LOCK(lock);

				pData->writerid = 0;
				pData->nreaders++;

				RELEASE_LOCK(lock);
#elif defined(SW_USE_SOLARIS_RWLOCKS) || defined(SW_USE_POSIX_RWLOCKS)
				res = SW_STATUS_UNSUPPORTED_OPERATION;
#else
	#error Unsupported platform - please update sw_rwlock.cpp
#endif
				break;

			default:
				res = SW_STATUS_INVALID_PARAMETER;
				break;
			}

		return res;
		}
*/

SWIFT_DLL_EXPORT sw_status_t
sw_rwlock_acquire(sw_rwlock_t *lock, int locktype, sw_uint64_t waitms)
		{
		sw_status_t			res=SW_STATUS_SUCCESS;

		switch (locktype)
			{
			case SW_RWLOCK_READ:
				res = sw_rwlock_internal_acquire_read(lock, waitms);
				break;

			case SW_RWLOCK_READ_NBLOCK:
				res = sw_rwlock_internal_acquire_read(lock, 0);
				break;

			case SW_RWLOCK_WRITE:
				res = sw_rwlock_internal_acquire_write(lock, waitms);
				break;

			case SW_RWLOCK_WRITE_NBLOCK:
				res = sw_rwlock_internal_acquire_write(lock, 0);
				break;

			case SW_RWLOCK_WRITE_UPGRADE:
				res = sw_rwlock_internal_acquire_write_upgrade(lock, waitms);
				break;

			case SW_RWLOCK_READ_DOWNGRADE:
				res = sw_rwlock_internal_acquire_read_downgrade(lock, waitms);
				break;

			default:
				res = SW_STATUS_INVALID_PARAMETER;
				break;
			}

		return res;
		}


SWIFT_DLL_EXPORT sw_status_t
sw_rwlock_release(sw_rwlock_t *lock)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

#if defined(SW_USE_EMULATED_RWLOCKS)
		sw_rwlock_data_t	*pData=lock->pData;

		ACQUIRE_LOCK(lock);

		if (pData->writerid != 0)
			{
			if (pData->writerid == sw_rwlock_getCallerId(lock))
				{
				pData->writerid = 0;
				}
			else
				{
				// We don't own the write lock, so we can't do a release
				res = SW_STATUS_NOT_OWNER;
				}
			}
		else
			{
			pData->nreaders--;
			}

		if (pData->writerid == 0 && pData->nreaders == 0 && (pData->wwaiting > 0 || pData->rwaiting > 0))
			{
	#if defined(SW_PLATFORM_WINDOWS)
			SetEvent(lock->hEvent);
	#else // defined(SW_PLATFORM_WINDOWS)
			pthread_cond_broadcast(&pData->cond);
	#endif // defined(SW_PLATFORM_WINDOWS)
			}

		RELEASE_LOCK(lock);
#elif defined(SW_USE_SOLARIS_RWLOCKS)
		if (rw_unlock(lock->pData) != 0) res = errno;
#elif defined(SW_USE_POSIX_RWLOCKS)
		if (pthread_rwlock_unlock(lock->pData) != 0) res = errno;
#else
	#error Unsupported platform - please update sw_rwlock.cpp
#endif

		return res;
		}





