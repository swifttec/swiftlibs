/**
*** @file		SWValueArray.cpp
*** @brief		An array of Values
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWValueArray class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWValueArray.h>
#include <swift/SWException.h>
#include <swift/SWInputStream.h>
#include <swift/SWOutputStream.h>
#include <swift/SWGuard.h>






class SWValueArrayElement
		{
public:
		/// Default Constructor
		SWValueArrayElement()										{ }

		/// Data copy constructor
		SWValueArrayElement(const SWValue &data) : m_data(data)	{ }

		/// Custom new operator
		void *			operator new(size_t nbytes, sw_size_t &res)	{ res = nbytes; return malloc(nbytes); }

		/// Custom new operator
		void *			operator new(size_t /*nbytes*/, void *p)	{ return p; }

		/// Custom delete operator
		void			operator delete(void *, sw_size_t &)		{ }

		/// Custom delete operator
		void			operator delete(void *, void *)				{ }

		/// Custom delete operator
		void			operator delete(void *)						{ }

		/// Get data reference
		SWValue &				data()										{ return m_data; }

private:
		SWValue	m_data;		///< The actual data
		};


SWValueArray::SWValueArray(sw_size_t initialSize) :
    SWAbstractArray(NULL, sizeof(SWValueArrayElement))
		{
		// Make sure we have space for some (8) elements
		ensureCapacity(initialSize);
		}


SWValueArray::SWValueArray(const SWValueArray &other) :
    SWAbstractArray(NULL, sizeof(SWValueArrayElement))
		{
		*this = other;
		}


SWValueArray::~SWValueArray()
		{
		clear();
		}


SWValueArray &
SWValueArray::operator=(const SWValueArray &other)
		{
		sw_size_t	n=other.size();

		clear();

		// Do it backwards to prevent ensureCapacity from
		// doing multiple reallocs
		while (n > 0)
			{
			--n;
			(*this)[(int)n] = other.get((sw_index_t)n);
			}

		return *this;
		}




void
SWValueArray::ensureCapacity(sw_size_t nelems)
		{
		sw_size_t		wanted=nelems * m_elemsize;

		if (wanted > m_capacity)
			{
			SWWriteGuard	guard(this);	// Guard this object

			if (m_capacity == 0) m_capacity = 128;

			while (m_capacity < wanted)
				{
				m_capacity <<= 1;
				}

			char	*pOldData = m_pData;

			m_pData = (char *)m_pMemoryManager->malloc(m_capacity);
			if (m_pData == NULL)
				{
				m_pData = pOldData;
				throw SW_EXCEPTION(SWMemoryException);
				}

			if (pOldData != NULL)
				{
				// We must "new" all the existing data elements
				char		*pTo=(char *)m_pData;
				char		*pFrom=(char *)pOldData;
				sw_size_t	i, n=size();

				for (i=0;i<n;i++)
					{
					new (pTo) SWValueArrayElement(((SWValueArrayElement *)pFrom)->data());
					delete (SWValueArrayElement *)pFrom;
					pTo += m_elemsize;
					pFrom += m_elemsize;
					}

				m_pMemoryManager->free(pOldData);
				}
			}
		}


/*
** Clear needs special handling here since we
** must delete the object in question.
*/
void
SWValueArray::clear()
		{
		SWWriteGuard		guard(this);
		SWValueArrayElement	*pElement;
		sw_size_t			pos=m_nelems;

		while (pos > 0)
			{
			--pos;
			pElement = ((SWValueArrayElement *)(m_pData + (pos * m_elemsize)));
			delete pElement;
			}

		m_nelems = 0;
		}


/*
** Remove needs special handling here since we
** must delete the object in question.
*/
void
SWValueArray::remove(sw_index_t pos, sw_size_t count)
		{
		if (count > 0)
			{
			SWWriteGuard	guard(this);

			if (pos >= 0 && pos < (sw_index_t)m_nelems)
				{
				if (pos + count > m_nelems) count = m_nelems - pos;

				if (count > 0)
					{
					sw_index_t	startpos=pos, endpos=pos+(sw_index_t)count;
					char		*p = m_pData + (startpos * m_elemsize);
					char		*q = m_pData + (endpos * m_elemsize);
					sw_size_t	oldn = m_nelems;

					// Reduce the number of elems and shift all the elems down
					for (pos=endpos;pos<(sw_index_t)size();pos++)
						{
						((SWValueArrayElement *)p)->data() = ((SWValueArrayElement *)q)->data();
						p += m_elemsize;
						q += m_elemsize;
						}

					 // Blank out the rest
					pos = (sw_index_t)(m_nelems - count);
					p = m_pData + (pos * m_elemsize);
					while (pos < (sw_index_t)oldn)
						{
						delete (SWValueArrayElement *)p;
						p += m_elemsize;
						pos++;
						}

					m_nelems -= count;
					}
				}
			else throw SW_EXCEPTION(SWIndexOutOfBoundsException);
			}
		}


SWValue &	
SWValueArray::operator[](sw_index_t pos)
		{
		if (pos < 0) throw SW_EXCEPTION(SWArrayIndexOutOfBoundsException);

		SWWriteGuard	guard(this);

		ensureCapacity((sw_size_t)pos + 1);

		while (m_nelems <= (sw_size_t)pos)
			{
			// Make sure all of the interim elements are valid
			new (m_pData + (m_nelems * m_elemsize)) SWValueArrayElement;
			m_nelems++;
			}

		return ((SWValueArrayElement *)(m_pData + (pos * m_elemsize)))->data();
		}


SWValue &	
SWValueArray::get(sw_index_t pos) const
		{
		if (pos < 0 || pos >= (sw_index_t)m_nelems) throw SW_EXCEPTION(SWIndexOutOfBoundsException);

		SWReadGuard	guard(this);

		return ((SWValueArrayElement *)(m_pData + (pos * m_elemsize)))->data();
		}


void
SWValueArray::push(const SWValue &v)
		{
		add(v);
		}


SWValue
SWValueArray::pop()
		{
		if (m_nelems < 1) throw SW_EXCEPTION(SWIndexOutOfBoundsException);

		SWWriteGuard	guard(this);

		SWValue	v=get((sw_index_t)m_nelems-1);

		remove((sw_index_t)m_nelems-1, 1);

		return v;
		}



sw_status_t
SWValueArray::writeToStream(SWOutputStream &stream) const
		{
		SWReadGuard	guard(this);
		sw_status_t	res;

		res = stream.write((sw_uint32_t)size());
		for (int i=0;res==SW_STATUS_SUCCESS&&i<(int)size();i++)
			{
			res = get(i).writeToStream(stream);
			}

		return res;
		}


sw_status_t
SWValueArray::readFromStream(SWInputStream &stream)
		{
		SWWriteGuard	guard(this);
		sw_status_t		res;
		sw_uint32_t		i, nelems;
		SWValue		s;

		clear();
		res = stream.read(nelems);
		for (i=0;res==SW_STATUS_SUCCESS&&i<nelems;i++)
			{
			res = s.readFromStream(stream);
			if (res == SW_STATUS_SUCCESS) add(s);
			}

		return res;
		}


int
SWValueArray::compareTo(const SWValueArray &other) const
		{
		SWReadGuard	guard(this);
		int			nelems1=(int)size();
		int			nelems2=(int)other.size();
		int			pos=0;
		int			res=0;

		while (res == 0 && pos < nelems1 && pos < nelems2)
			{
			res = get(pos).compareTo(other.get(pos));
			pos++;
			}

		if (res == 0)
			{
			if (nelems1 > nelems2) res = 1;
			else if (nelems1 < nelems2) res = -1;
			}

		return res;
		}



void
SWValueArray::add(const SWValue &v)
		{
		insert(v, (sw_index_t)m_nelems);
		}


void
SWValueArray::insert(const SWValue &v, sw_index_t pos)
		{
		SWWriteGuard	guard(this);

		if (pos >= 0 && pos <= (sw_index_t)m_nelems)
			{
			ensureCapacity(m_nelems+1);

			sw_index_t	ipos = (sw_index_t)m_nelems;
			char		*p = m_pData + (ipos * m_elemsize);
			char		*q = p - m_elemsize;
			bool		exists=false;

			// Increase the number of elems by one and shift all the elems up by 1
			// not forgetting to make sure we have enough space first!
			if (ipos > pos)
				{
				new (p) SWValueArrayElement((((SWValueArrayElement *)q)->data()));
				p -= m_elemsize;
				q -= m_elemsize;
				ipos--;
				exists = true;
				}
			
			while (ipos > pos)
				{
				(((SWValueArrayElement *)p)->data()) = (((SWValueArrayElement *)q)->data());
				p -= m_elemsize;
				q -= m_elemsize;
				ipos--;
				}
				
			if (exists) ((SWValueArrayElement *)p)->data() = v;
			else new (p) SWValueArrayElement(v);

			m_nelems++;
			}
		else throw SW_EXCEPTION(SWIndexOutOfBoundsException);
		}

/*
bool
SWValueArray::defined(int pos) const
		{
		bool	res=false;

		if (pos >= 0 && pos < _nelems)
			{
			SWValue **us = (SWValue **)((char *)_data + (pos * sizeof(SWValue *)));
			if (*us != NULL) res = true;
			}

		return res;
		}
*/



