/**
*** @file		SWTime.h
*** @brief		A class to represent a time in seconds and nanoseconds since 1st Jan 1900
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWTime class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __swift_SWTime_h__
#define __swift_SWTime_h__


#include <swift/swift.h>
#include <swift/SWString.h>

#ifndef __SWDate_h__
#include <swift/SWDate.h>
#endif




// Forward declarations
class SWDate;


// Special dates as Julian day numbers
#define JULIAN_DAY_1ST_JAN_1601		2305814		///< The julian number for 1st Jan 1601
#define JULIAN_DAY_31ST_DEC_1899	2415020		///< The julian number for 31st Dec 1899
#define JULIAN_DAY_1ST_JAN_1900		2415021		///< The julian number for 1st Jan 1900
#define JULIAN_DAY_1ST_JAN_1970		2440588		///< The julian number for 1st Jan 1970


/// The number of seconds between zero in SWTime and zero in Unix time
#define SECONDS_SWTIME_UNTIL_UNIXTIME	 SW_CONST_ULONG(2209075200)

/// The number of seconds between zero in windows FILETIME and zero in SWTime
#define SECONDS_FILETIME_UNTIL_SWTIME	 SW_CONST_ULONG(9435398400)

#define SECONDS_IN_DAY		86400				///< The numebr of seconds in a day
#define SECONDS_IN_HOUR		3600				///< The numebr of seconds in an hour

/**
*** @brief A class to represent a time in seconds and nanoseconds.
***
***	The SWTime class is a portable representation of time. It has 2 components
*** - The time in seconds since midnight on 31st December 1899 (held as a 64-bit integer).
*** - The time fraction in nanoseconds (held as a 32-bit unsigned integer)
***
*** The SWTime makes no adjustments for timezone and works in GMT time.
***
*** @par Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Unsafe</td>
*** </tr>
*** </table>
***
***
*** @author Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWTime : public SWObject
		{
public:
		/**
		*** An enumeration used to determine the time type when performing a
		*** conversion from another type.
		**/
		enum TimeType
			{
			InternalTime,			///< The internal time format (time in seconds since 00:00:00 on 31 Dec 1899)
			UnixTime,				///< A standard unix time_t time (time in seconds since 00:00:00 on 1 Jan 1970).
			WindowsFileTime			///< A windows FILETIME as a 64-bit number (the number of 100-nanosecond intervals since 00:00:00 on January 1, 1601).
			};

public:
		/**
		*** Default constructor
		***
		*** The default constructor creates a time object with the time elements set to
		*** zero, i.e. midnight 31st Decemeber 1899. If the setToCurrentTime parameter
		*** is set to true then the constructor calls the update method to set the current
		*** time.
		***
		*** @param	setToCurrentTime	A boolean to indicate if the time should be set
		***								to the current time or not.
		**/
		SWTime(bool setToCurrentTime=false);

		/**
		*** @brief		Construct from a internal time structure
		*** @param[in]	v	The value with which to construct this time object.
		**/
		SWTime(const sw_internal_time_t &v);

		/**
		*** @brief		Construct from a unix timespec
		*** @param[in]	v	The value with which to construct this time object.
		**/
		SWTime(const sw_timespec_t &v);

		/**
		*** @brief		Construct from a unix timeval
		*** @param[in]	v	The value with which to construct this time object.
		**/
		SWTime(const sw_timeval_t &v);

		/**
		*** @brief		Construct from unix time
		*** @param[in]	v	The value with which to construct this time object.
		**/
		SWTime(time_t v);

		/**
		*** @brief		Construct from a windows FILETIME
		*** @param[in]	v	The value with which to construct this time object.
		**/
		SWTime(const FILETIME &v);

		/**
		*** @brief		Construct from a windows SYSTEMTIME
		*** @param[in]	v	The value with which to construct this time object.
		**/
		SWTime(const SYSTEMTIME &v);

		/**
		*** @brief		Construct from a SWDate - time will be set to midnight (00:00) on the date specified by the SWDate object.
		*** @param[in]	v	The value with which to construct this time object.
		**/
		SWTime(const SWDate &v);

		/**
		*** @brief		Construct from a struct tm (unix)
		*** @param[in]	v	The value with which to construct this time object.
		**/
		SWTime(const sw_tm_t &v);


		/**
		*** Construct from a double (either a SWTime double or a windows variant time)
		***
		*** @param	clock			The time value. This is handled according to the isVariantTime parameter.
		*** @param	isVariantTime	If false (the default) the double is treated as a SWTime double where
		***							the whole part of the double is the number of seconds since midnight on
		***							31/12/1899 and the fractional part represents the fraction of the time.
		***
		***							If isVariantTime is true then the double is treated as a Windows variant
		***							time where the whole part of the double represents the number of days
		***							since 31/12/1899 and the fraction part represents the fractional part
		***							of the day since midnight (e.g. 0.25=6am, 0.5=12pm, etc)
		***							When converting from a variant time the fractional part of seconds is ignored
		***							and always set to zero.
		**/
		SWTime(double clock, bool isVariantTime=false);


		/**
		*** Construct from a sw_int64_t and sw_uint32_t.
		***
		*** @param	secs			The seconds value. This is handled according to the type parameter.
		*** @param	nsecs			The nanoseconds value.
		*** @param	tt				Specifies the type of time being assigned. This can have any of the values
		***							from the TimeType enumeration.
		**/
		SWTime(sw_int64_t secs, sw_uint32_t nsecs, TimeType tt=InternalTime);


		/**
		*** Construct from a string
		***
		*** @param	value			The time value string. This value is interpreted according to the format parameter.
		*** @param	format			The format string which specifies how the string value is interpreted. The format is
		***							a sequence of characters built up from the following:
		***							- YYYY - A 4-digit year
		***							- YY - A 2-digit year (00-99)
		***							- MM - A 2-digit month (01-12)
		***							- DD - A 2-digit day number (01-31)
		***							- hh - A 2-digit hour (00-23)
		***							- mm - A 2-digit minute (00-59)
		***							- ss - A 2-digit second (00-59)
		***							- ms - A 3-digit millisecond value (000-999)
		***							- us - A 6-digit millisecond value (000000-999999)
		***							- ns - A 9-digit millisecond value (000000000-999999999)
		**/
		SWTime(const SWString &value, const SWString &format="YYYYMMDDhhmmss");

/**
*** @name Assignment operators
**/
//@{
		/// Assignmemt from a internal time
		SWTime &	operator=(const sw_internal_time_t &tv);

		/// Assignmemt from a unix timespec
		SWTime &	operator=(const sw_timespec_t &tv);

		/// Assignmemt from a unix timeval
		SWTime &	operator=(const sw_timeval_t &tv);

		/// Assignment from a unix time
		SWTime &	operator=(time_t clock);

		/// Assignment from a double (SWTime format only)
		SWTime &	operator=(double v);

		/// Assignment from a windows FILETIME
		SWTime &	operator=(const FILETIME &v);

		/// Assignment from a windows SYSTEMTIME
		SWTime &	operator=(const SYSTEMTIME &v);

		/// Assignment from a SWDate
		SWTime &	operator=(const SWDate &v);

		/// Assignment from a struct tm (unix)
		SWTime &	operator=(const sw_tm_t &v);

		/**
		*** Adjust the time by the number of seconds and nanoseconds given
		***
		*** @param	secs			The seconds value.
		*** @param	nsecs			The nanoseconds value.
		**/
		virtual bool	adjust(sw_int64_t secs, sw_int32_t nsecs);

		/**
		*** Adjust the time by the number of seconds given
		***
		*** @param	v			The seconds value.
		**/
		virtual bool	adjust(double v);

		/**
		*** Set the time from the sw_int64_t and sw_uint32_t given
		***
		*** @param	secs			The seconds value. This is handled according to the type parameter.
		*** @param	nsecs			The nanoseconds value.
		*** @param	tt				Specifies the type of time being assigned. This can have any of the values
		***							from the TimeType enumeration.
		**/
		virtual bool	set(sw_int64_t secs, sw_uint32_t nsecs, TimeType tt);

		/**
		*** Set the time value from a string value and format.
		***
		*** The string value is parsed using the format string as a guide and the time value is set accordingly.
		*** The time value is assumed to be in GMT format and no offset is applied.
		***
		*** @param	value			The time value string. This value is interpreted according to the format parameter.
		*** @param	format			The format string which specifies how the string value is interpreted. The format is
		***							a sequence of characters built up from the following:
		***
		***							- YYYY - A 4-digit year
		***							- YY - A 2-digit year (00-99)
		***							- MM - A 2-digit month (01-12)
		***							- DD - A 2-digit day number (01-31)
		***							- hh - A 2-digit hour (00-23)
		***							- mm - A 2-digit minute (00-59)
		***							- ss - A 2-digit second (00-59)
		***							- ms - A 3-digit millisecond value (000-999)
		***							- us - A 6-digit millisecond value (000000-999999)
		***							- ns - A 9-digit millisecond value (000000000-999999999)
		***
		***							Other characters in the format string are matched exactly, so a format of "hh:mm:ss" will
		***							only match a number sequence with colons (i.e. 10:20:32) and not (102032).
		***
		*** @return					true if the conversion was successful, false otherwise.
		**/
		virtual bool	set(const SWString &value, const SWString &format="YYYYMMDDhhmmss");

		/**
		*** Set from a double (either a SWTime double or a windows variant time)
		***
		*** @param	clock			The time value. This is handled according to the isVariantTime parameter.
		*** @param	isVariantTime	If false (the default) the double is treated as a SWTime double where
		***							the whole part of the double is the number of seconds since midnight on
		***							31/12/1899 and the fractional part represents the fraction of the time.
		***
		***							If isVariantTime is true then the double is treated as a Windows variant
		***							time where the whole part of the double represents the number of days
		***							since 31/12/1899 and the fraction part represents the fractional part
		***							of the day since midnight (e.g. 0.25=6am, 0.5=12pm, etc)
		***							When converting from a variant time the fractional part of seconds is ignored
		***							and always set to zero.
		**/
		virtual void	set(double clock, bool isVariantTime=false);

		/**
		*** Set from the given date/time parameters
		**/
		virtual bool	set(int nYear, int nMonth, int nDay, int nHour, int nMinute, int nSeconds, int nNanoseconds);

//@}


/**
*** @name Member access
**/
//@{

		/// Return the hour (0-23)
		virtual int		hour() const;

		/// Return the minute (0-59)
		virtual int		minute() const;

		/// Return the second (0-59)
		virtual int		second() const;

		/// Return the fractional portion of the time in milliseconds
		virtual int		millisecond() const;

		/// Return the fractional portion of the time in microseconds
		virtual int		microsecond() const;

		/// Return the fractional portion of the time in nanoseconds
		virtual int		nanosecond() const;

		/// Return the day (1-31)
		virtual int		day() const;

		/// Return the day (1-12)
		virtual int		month() const;

		/// Return the year
		virtual int		year() const;

		/// Return the dayOfWeek (0=Sunday, 1=Monday, ....)
		virtual int		dayOfWeek() const;

		/// Return the day of year (1-366 if dayOneIsZero is false) or (0-365 if dayOneIsZero is true)
		virtual int		dayOfYear(bool dayOneIsZero=false) const;

		/// Return the julian day number
		virtual int		julian() const;

//@}


/**
*** @name Time Conversions
**/
//@{
		/// Return the time as a double where the whole part is the number of seconds
		/// since midnight 31st Dec 1899 and the fraction part is the fraction of the second
		virtual double		toDouble() const;

		/**
		*** @brief	Return the time as a 64-bit integer where the representing the number of seconds
		***			since midnight 31st Dec 1899.
		***
		*** @param[in]	tt	Specify the time type to convert to. This can take one of the 
		***					following values:
		***					- InternalTime -	returns the current seconds component of this time object.
		***					- UnixTime -		returns the current time value of this time object after
		***										adjusting it to correctly represent a unix time_t value.
		***										Note that it is possible to get a value that will be out
		***										of the range of a 32-bit time_t variable.
		***					- WindowsFileTime	returns the current time value as a windows FILETIME value.
		***
		*** @return			A 64-bit integer represent the time of this object as specified
		***					by the tt parameter.
		**/
		virtual sw_int64_t	toInt64(TimeType tt=InternalTime) const;

		/**
		*** @brief	Return the Julian Day Number for this time
		**/
		virtual int			toJulian() const;
		
		/**
		*** Return the time as a unix time_t (seconds since midnight 1st Jan 1970).
		***
		*** @throws	SWTimeConversionException	If the result will not fit within a time_t variable.
		**/
		time_t			toUnixTime() const;

		/**
		*** Get the time as a unix time_t (seconds since midnight 1st Jan 1970).
		***
		*** This form of the toUnixTime method returns a bool to indicate if the conversion
		*** has failed and will not throw an exception.
		***
		*** @param[out]	t	Receives the converted value if the conversion is successful.
		***
		*** @return			true if the conversion succeeds, false if it fails.
		**/
		bool			toUnixTime(time_t &t) const;

		/**
		*** Return the time as a internal time
		**/
		sw_internal_time_t	toInternalTime() const;

		/**
		*** Get the time as a internal time
		***
		*** This form of the toTimeSpec method returns a bool to indicate if the conversion
		*** has failed and will not throw an exception.
		***
		*** @param[out]	tv	Receives the converted value if the conversion is successful.
		***
		*** @return			true if the conversion succeeds, false if it fails.
		**/
		bool			toInternalTime(sw_internal_time_t &tv) const;

		/**
		*** Return the time as a unix struct timespec (seconds since midnight 1st Jan 1970).
		***
		*** @throws	SWTimeConversionException	If the result will not fit within a struct timespec variable.
		**/
		sw_timespec_t	toTimeSpec() const;

		/**
		*** Get the time as a unix struct timespec (seconds since midnight 1st Jan 1970).
		***
		*** This form of the toTimeSpec method returns a bool to indicate if the conversion
		*** has failed and will not throw an exception.
		***
		*** @param[out]	tv	Receives the converted value if the conversion is successful.
		***
		*** @return			true if the conversion succeeds, false if it fails.
		**/
		bool			toTimeSpec(sw_timespec_t &tv) const;

		/**
		*** Return the time as a unix struct timeval (seconds since midnight 1st Jan 1970).
		***
		*** @throws	SWTimeConversionException	If the result will not fit within a struct timeval variable.
		**/
		sw_timeval_t	toTimeVal() const;

		/**
		*** Get the time as a unix struct timeval (seconds since midnight 1st Jan 1970).
		***
		*** This form of the toTimeVal method returns a bool to indicate if the conversion
		*** has failed and will not throw an exception.
		***
		*** @param[out]	tv	Receives the converted value if the conversion is successful.
		***
		*** @return			true if the conversion succeeds, false if it fails.
		**/
		bool			toTimeVal(sw_timeval_t &tv) const;

		/**
		*** Return the time as a windows FILETIME (the number of 100-nanosecond intervals since January 1, 1601)
		***
		*** @throws	SWTimeConversionException	If the result will not fit within a FILETIME variable.
		**/
		FILETIME		toFILETIME() const;

		/**
		*** Get the time as a windows FILETIME (the number of 100-nanosecond intervals since January 1, 1601)
		***
		*** This form of the toFILETIME method returns a bool to indicate if the conversion
		*** has failed and will not throw an exception.
		***
		*** @param[out]	v	Receives the converted value if the conversion is successful.
		***
		*** @return			true if the conversion succeeds, false if it fails.
		**/
		bool		    toFILETIME(FILETIME &v) const;


		/**
		*** Return the time as a windows SYSTEMTIME
		***
		*** @throws	SWTimeConversionException	If the result will not fit within a SYSTEMTIME variable.
		**/
		SYSTEMTIME		toSYSTEMTIME() const;

		/**
		*** Get the time as a windows SYSTEMTIME
		***
		*** This form of the toSYSTEMTIME method returns a bool to indicate if the conversion
		*** has failed and will not throw an exception.
		***
		*** @param[out]	v	Receives the converted value if the conversion is successful.
		***
		*** @return			true if the conversion succeeds, false if it fails.
		**/
		bool		    toSYSTEMTIME(SYSTEMTIME &v) const;


		/**
		*** Return the time as a struct tm (unix)
		***
		*** @throws	SWTimeConversionException	If the result will not fit within a struct tm variable.
		**/
		sw_tm_t			toTM() const;

		/**
		*** Get the time as a struct tm (unix)
		***
		*** This form of the toTM method returns a bool to indicate if the conversion
		*** has failed and will not throw an exception.
		***
		*** @param[out]	v	Receives the converted value if the conversion is successful.
		***
		*** @return			true if the conversion succeeds, false if it fails.
		**/
		bool		    toTM(sw_tm_t &v) const;


		/**
		*** Return the time as a SWDate
		***
		*** @throws	SWTimeConversionException	If the result will not fit within a SWDate variable.
		**/
		SWDate			toDate() const;

		/**
		*** Get the time as a struct tm (unix)
		***
		*** This form of the toTM method returns a bool to indicate if the conversion
		*** has failed and will not throw an exception.
		***
		*** @param[out]	v	Receives the converted value if the conversion is successful.
		***
		*** @return			true if the conversion succeeds, false if it fails.
		**/
		bool		    toDate(SWDate &v) const;


		/// Cast the time to a 64-bit integer.
		/// @see toInt64
		operator sw_int64_t() const			{ return toInt64(); }

		/// Cast the time to a double
		/// @see toDouble
		operator double() const				{ return toDouble(); }

		/// Cast the time to a unix timeval
		/// @see toTimeVal
		operator sw_timeval_t() const		{ return toTimeVal(); }

		/// Cast the time to a internal time
		/// @see toTimeVal
		operator sw_internal_time_t() const	{ return toInternalTime(); }

		/// Cast the time to a unix timespec
		/// @see toTimeVal
		operator sw_timespec_t() const		{ return toTimeSpec(); }

		/// Cast the time to a windows FILETIME
		/// @see toFILETIME
		operator FILETIME() const			{ return toFILETIME(); }

		/// Cast the time to a windows SYSTEMTIME
		/// @see toSYSTEMTIME
		operator SYSTEMTIME() const			{ return toSYSTEMTIME(); }

		/// Cast the time to a struct tm
		/// @see toTM
		operator sw_tm_t() const			{ return toTM(); }

		/// Cast the time to a SWDate
		/// @see toDate
		operator SWDate() const				{ return toDate(); }

//@}


		/// Update this object to hold the current time.
		void				update();

		/// Return true if the time is AM
		bool				isAM() const		{ return (hour() < 12); }

		/// Return true if the time is PM
		bool				isPM() const		{ return (hour() >= 12); }

		/**
		*** Check to see if the time is in DST (Daylight Savings Time)
		***
		*** @return	0 if DST is not in effect, 1 if DST is in effect or
		***			-1 if DST information is unavailable
		**/
		virtual int			dst() const;

		/// Returns true is DST is in effect
		bool				isDST() const		{ return (dst() > 0); }

		/// Return the name of the timezone
		virtual SWString	tzName(int dst) const;


		/**
		*** @brief	Format the time into a string.
		***
		*** This method takes the current time object and using the specified
		*** format string fmt generates a string representation of the time.
		*** The format string is based on the unix cftime call. Complete details
		*** of the format string can be found in the document
		*** <a href="../userguide/SWTimeFormatting.html">SWTime formatting</a>.
		***
		*** @param	fmt		The format string (see <a href="../userguide/SWTimeFormatting.html">Time Formatting</a> for more details).
		***
		*** @return			A SWString object containing the formatted time string.
		**/
		SWString			format(const SWString &fmt) const;


		SWString			toString(const SWString &fmt="%Y%m%d%H%M%S") const	{ return format(fmt); }


public: // Comparison operators

		/**
		*** @brief	Compare this object with another.
		***
		*** This method compares this object with the one specified and returns a
		*** result indicating if the other object is less than, equal to, or greater
		*** than this object.
		***
		*** - If this object is less than the specified object then a negative result is returned.
		*** - If this object is greater than the specified object then a positive result is returned.
		*** - If this object is equal to the specified object then a zero result is returned.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				The result of the comparision:
		***						-  0 - This object is equal to the other object.
		***						- <0 - This object is less than the other object.
		***						- >0 - This object is greater than the other object.
		***
		*** @note
		*** The compareTo method is not required to return -1 and +1 for the negative and positive
		*** values although this is typically the value returned. Callers should test for \< 0 and \> 0
		*** rather than for specific values of -1 and +1.
		**/
		int			compareTo(const SWTime &other) const;

		/**
		*** @brief	Test for equality
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is equal to the other object, false otherwise.
		**/
		bool	operator==(const SWTime &other) const	{ return (compareTo(other) == 0); }

		/**
		*** @brief	Test for inequality
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is not equal to the other object, false otherwise.
		**/
		bool	operator!=(const SWTime &other) const	{ return (compareTo(other) != 0); }

		/**
		*** @brief	Test for less than.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is less than the other object, false otherwise.
		**/
		bool	operator< (const SWTime &other) const	{ return (compareTo(other) <  0); }

		/**
		*** @brief	Test for greater than.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is greater than the other object, false otherwise.
		**/
		bool	operator> (const SWTime &other) const	{ return (compareTo(other) >  0); }

		/**
		*** @brief	Test for less than or equal to.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is less than or equal to the other object, false otherwise.
		**/
		bool	operator<=(const SWTime &other) const	{ return (compareTo(other) <= 0); }

		/**
		*** @brief	Test for greater than or equal to.
		***
		*** @param[in]	other	The object to compare this object with.
		*** @return				true if this object is greater than or equal to the other object, false otherwise.
		**/
		bool	operator>=(const SWTime &other) const	{ return (compareTo(other) >= 0); }



public: // Other operators

		/// Prefix increment operator.
		const SWTime &	operator++();

		/// Postfix increment operator.
		SWTime			operator++(int);

		/// Prefix decrement operator.
		const SWTime &	operator--();

		/// Postfix decrement operator.
		SWTime			operator--(int);

		/// += operator (int)
		SWTime &		operator+=(int v);

		/// -= operator (int)
		SWTime &		operator-=(int v);

		/// += operator (double)
		SWTime &		operator+=(double v);

		/// -= operator (double)
		SWTime &		operator-=(double v);


public: // Friend operators and methods

		/**
		*** @brief	Return the sum of 2 times
		***
		*** Adds the 2 specified times together and returns a new time object
		*** containing that time.
		***
		*** @param[in]	a	The first time to add together.
		*** @param[in]	b	The second time to add together.
		***
		*** @return			The sum of a + b.
		**/
		friend SWIFT_DLL_EXPORT SWTime	operator+(const SWTime &a, const SWTime &b);

		/**
		*** @brief	Return the difference of 2 times
		***
		*** Subtracts the second specified time from the first and returns a new time object
		*** containing that time.
		***
		*** @param[in]	a	The time to subtract from.
		*** @param[in]	b	The time to subtract from a.
		***
		*** @return			The sum of a - b.
		**/
		friend SWIFT_DLL_EXPORT SWTime	operator-(const SWTime &a, const SWTime &b);


public: // Virtual overrides

		/// Override for SWObject writeToStream
		virtual sw_status_t		writeToStream(SWOutputStream &stream) const;

		/// Override for SWObject readFromStream
		virtual sw_status_t		readFromStream(SWInputStream &stream);


public:
		/// Return the current time.
		static SWTime		now();
		static SWTime		getCurrentTime()	{ return now(); }

		/// Get the current time in a sw_timeval_t
		static sw_status_t	gettimeofday(sw_timeval_t &tv);

		/// Set the system time to the one given
		static sw_status_t	setSystemTime(const SWTime &t);

protected: // Methods
		/**
		*** @brief	Determine if the current time is in DST or not.
		***
		*** This method is not use directly by SWTime, but is implemented in the
		*** derived class SWAdjustedTime to adjust the time for daylight savings.
		**/
		virtual void		calculateDST();

protected: // Members
		sw_int64_t		m_seconds;		///< Time in seconds since 00:00:00 Jan 1 1900 GMT
		sw_uint32_t		m_nanoseconds;	///< Time fraction in nanoseconds
		};


/// A general time exception
SW_DEFINE_EXCEPTION(SWTimeException, SWException);

/// Failure to perform a time conversion.
SW_DEFINE_EXCEPTION(SWTimeConversionException, SWTimeException);


#endif // __swift_SWTime_h__
