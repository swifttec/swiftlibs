/**
*** @file		SWObject.h
*** @brief		Base object for all non-primary objects in the swift library.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWObject class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __swift_SWObject_h__
#define __swift_SWObject_h__

#include <swift/swift.h>

// Forward declarations
class SWInputStream;
class SWOutputStream;

/**
*** @brief	Forms the base of all swift non-primary objects.
**/
class SWIFT_DLL_EXPORT SWObject
		{
public:
		/// Default Constructor for all objects
		SWObject();

		/// Virtual Destructor
		virtual ~SWObject();

		/**
		*** @brief	Return a hashcode for this object
		***
		*** The hashCode method returns a value that can be used for hashing this object
		*** into a hash table. If this method is not overridden the hash code returned is
		*** based on the value of the this pointer. Some objects (e.g. String) override
		*** this method to produce a hash code based on their data content.
		***
		*** The hashCode is used by collections such as the HashTable.
		**/
		virtual sw_uint32_t		hash() const;

		virtual sw_status_t		lock(sw_uint64_t waitms=SW_TIMEOUT_INFINITE) const;
		virtual sw_status_t		lockForReading(sw_uint64_t waitms=SW_TIMEOUT_INFINITE) const;
		virtual sw_status_t		lockForWriting(sw_uint64_t waitms=SW_TIMEOUT_INFINITE) const;
		virtual sw_status_t		unlock() const;

		/**
		*** @brief clear this object
		**/
		virtual void			clear();

		/**
		*** @brief	Write this object to the given output stream.
		***
		*** This method will write the object data to the output stream in a way
		*** that can be read by a corresponding input stream.
		***
		*** @note
		*** Object developers should override these methods as the default handling
		*** is to return an error status.
		***
		*** @param[in]	stream	The stream to write to.
		***
		*** @return				The write status.
		**/
		virtual sw_status_t		writeToStream(SWOutputStream &stream) const;


		/**
		*** @brief	Read this object from the given input stream.
		***
		*** This method will read the object data from the input stream. The input
		*** stream data is expected to have been written by a corresponding output
		*** stream.
		***
		*** @note
		*** Object developers should override these methods as the default handling
		*** is to return an error status.
		***
		*** @param[in]	stream	The stream to write to.
		***
		*** @return				The write status.
		**/
		virtual sw_status_t		readFromStream(SWInputStream &stream);

#if defined(SW_BUILD_DEBUG) && defined(SW_DEBUG_MEMORY)
		void *	operator new(size_t size);
		void *	operator new(size_t size, const char *filename, int line);
		void *	operator new[](size_t size);
		void *	operator new[](size_t size, const char *filename, int line);

		void	operator delete(void *);
		void	operator delete(void *, const char *filename, int line);
		void	operator delete[](void *);
		void	operator delete[](void *, const char *filename, int line);
#endif // defined(SW_BUILD_DEBUG)
		};


#endif // __swift_SWObject_h__
