/**
*** @file		SWTempFile.h
*** @brief		A class for handling temporary files.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWTempFile class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




#ifndef __SWTempFile_h__
#define __SWTempFile_h__

#include <swift/swift.h>
#include <swift/SWFile.h>







/**
*** @brief		A class for handling temporary files.
***
*** A class for handling temporary files.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWTempFile : public SWFile
		{
public:
		/**
		*** @brief	Construct an open temporary file in an optionally specified directory.
		***
		*** @param	dir			The directory name in which to create the temporary file.
		***						If this parameter is blank the file is created in the directory
		***						returned by calling getTempDirectory with an empty string.
		*** @param	persistent	If true the temporary file is persistent (i.e. will remain
		***						on disk after the file is closed and the object destructed.
		***						If false the file is created with the Temporary and DeleteOnClose
		***						flags to ensure that the file is deleted when the file is closed.
		*** @param	prefix		Specifies the prefix that is given to the temporary file name generated (default tmp).
		*** @param	ext			Specifies the extension that is given to the temporary file name generated (default .tmp).
		**/
		SWTempFile(const SWString &dir="", bool persistent=false, const SWString &prefix="tmp", const SWString &ext=".tmp");

		/// Virtual destructor
		virtual ~SWTempFile();

public:
		/**
		*** @brief	Returns the name of a directory in which temporary files can be created.
		***
		*** @param dir	The preferred directory in which to store the file. If specified
		***				and is a real directory this value will be returned. If an empty
		***				is specified the method tries to work out an appropriate value
		***				according to the platform defaults.
		**/
		static SWString		getTempDirectory(const SWString &dir="");

		/**
		*** @brief	Generate and return a unique temporary file name.
		***
		*** @param prefix	The prefix for the file part of the temporary filename.
		*** @param ext		The filename extension to use.
		*** @param dir		The directory name in which to store the file. If NULL the system
		***					tries to work out an appropriate value
		**/
		static SWString		getTempFileName(const SWString &prefix=_T("tmp"), const SWString &ext=_T(".tmp"), const SWString &dir="");
		};




#endif
