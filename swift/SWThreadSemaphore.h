/**
*** @file		SWThreadSemaphore.h
*** @brief		A thread semaphore
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWThreadSemaphore class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWThreadSemaphore_h__
#define __SWThreadSemaphore_h__

#include <swift/SWSemaphore.h>
#include <swift/SWThread.h>





/**
*** @brief Thread-specific version of a Dijkstra style general semaphore.
***
*** Thread-specific version of a Dijkstra style general semaphore.
***
*** @see		Semaphore
*** @author		Simon Sparkes
*/
class SWIFT_DLL_EXPORT SWThreadSemaphore : public SWSemaphore
		{
public:
		/// Constructor
		SWThreadSemaphore(int count=1, int maxcount=0x7fffffff);

		/// Virtual destructor
		virtual ~SWThreadSemaphore();

		/// Acquire this semaphore. Blocks the thread until the semaphore count becomes greater than 0, then decrements it.
		virtual sw_status_t	acquire(sw_uint64_t waitms=SW_TIMEOUT_INFINITE);

		/// Increment the semaphore by 1, potentially unblocking a waiting thread. 
		virtual sw_status_t	release();

private:
		sw_handle_t		m_hSemaphore;	///< The handle to the actual mutex
		};




#endif
