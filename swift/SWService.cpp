/****************************************************************************
**	SWService.cpp	A class for testing and controlling services (primarily Win32)
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


// Needed for precompiled header support
#include "stdafx.h"

#include <swift/SWService.h>
#include <swift/SWMultiString.h>
#include <swift/SWRegistryKey.h>
#include <swift/sw_file.h>

#if defined(SW_PLATFORM_WINDOWS)
SWThreadMutex	SWService::m_mutex(true);		
SC_HANDLE		SWService::m_mgrHandle=NULL;
int				SWService::m_mgrRefCount=0;
int				SWService::m_mgrType=0;


bool
SWService::openManager()
		{
		SWMutexGuard	guard(m_mutex);
		bool			res=true;

		if (++m_mgrRefCount == 1)
			{
			int		type=1;

			m_mgrHandle = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
			if (!m_mgrHandle)
				{
				type = 2;
				m_mgrHandle = OpenSCManager(NULL, NULL, SC_MANAGER_ENUMERATE_SERVICE);
				}
			
			if (!m_mgrHandle)
				{
				m_mgrRefCount = 0;
				res = false;
				}
			else
				{
				m_mgrType = type;
				}
			}

		return res;
		}


bool
SWService::closeManager()
		{
		SWMutexGuard	guard(m_mutex);

		if (m_mgrRefCount > 0)
			{
			if (--m_mgrRefCount == 0)
				{
				CloseServiceHandle(m_mgrHandle);
				m_mgrHandle = NULL;
				m_mgrType = 0;
				}
			}

		return true;
		}


bool
SWService::canControlServices()
		{
		return (m_mgrType == 1);
		}

#endif // defined(SW_PLATFORM_WINDOWS)


// Constructor
SWService::SWService(const SWString &name, bool lookForName) :
    m_name(name)
#if defined(SW_PLATFORM_WINDOWS)
    , m_svcHandle(NULL)
#endif // defined(SW_PLATFORM_WINDOWS)
		{
		if (!name.isEmpty()) open(name, lookForName);
		}


// Destructor
SWService::~SWService()
		{
		close();
		}



bool
SWService::isOpen() const
		{
#if defined(SW_PLATFORM_WINDOWS)
		return (m_svcHandle != NULL);
#else
		return false;
#endif
		}


/**
*** Searches the registry for the given service name (display or actual)
*** and returns the actual service name that can be passed to the OpenService
*** function.
**/
SWString
SWService::actualServiceName(const SWString &name)
		{
		SWString	res = name;

#if defined(SW_PLATFORM_WINDOWS)
		if (!name.isEmpty())
			{
			// Check we've got the right name before starting
			SWRegistryKey	rrk(SW_REGISTRY_LOCAL_MACHINE, _T("SYSTEM\\CurrentControlSet\\Services"));

			// If the key exists it's a valid service name already so we return it unchanged
			// otherwise we must search all the display names and look for a match
			if (!rrk.keyExists(name))
				{
				SWArray<SWString>	sa;

				// We've probably been given the display name so look for it amongst all the services
				rrk.keyNames(sa);
				for (int i=0;i<(int)sa.size();i++)
					{
					SWRegistryKey   subkey;
					
					if (rrk.openKey(sa[i], subkey) == SW_STATUS_SUCCESS)
						{
						if (subkey.valueExists(_T("DisplayName")))
							{
							SWRegistryValue	rv;

							subkey.getValue(_T("DisplayName"), rv);
							if (name.compareNoCase(rv.toString()) == 0)
								{
								res = sw_file_basename(subkey.name());
								break;
								}
							}
						}
					}
				}
			}
#endif // defined(SW_PLATFORM_WINDOWS)

		return res;
		}



/**
*** Opens a connection to the service if possible
***
*** @param lookForName	If true and the initial open fails the service name
***			will be searched for in the registry for a display name match
**/
sw_status_t
SWService::open(const SWString &name, bool lookForName)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (isOpen())
			{
			res = SW_STATUS_ALREADY_OPEN;
			}
		else
			{
#if defined(SW_PLATFORM_WINDOWS)
			memset(m_configBuf, 0, sizeof(m_configBuf));
			m_config = (QUERY_SERVICE_CONFIG *)m_configBuf;

			if (!name.isEmpty())
				{
				openManager();
				if (m_mgrHandle)
					{
					SWString	tmpname;

					tmpname = name;
					m_svcHandle = OpenService(m_mgrHandle, tmpname, SERVICE_ALL_ACCESS);
					if (m_svcHandle == NULL && lookForName)
						{
						tmpname = actualServiceName(name);
						if (tmpname != name)
							{
							// We've got a match that we can open
							m_svcHandle = OpenService(m_mgrHandle, tmpname, SERVICE_ALL_ACCESS);
							}
						}

					if (m_svcHandle != NULL)
						{
						DWORD   tmp;

						QueryServiceConfig(m_svcHandle, m_config, sizeof(m_configBuf), &tmp);
						m_name = tmpname;
						}
					else
						{
						DWORD   tmp;

						tmp = GetLastError();
						res = SW_STATUS_NOT_FOUND;
						}
					}
				else
					{
					res = SW_STATUS_ILLEGAL_OPERATION;
					}
				}
			else
				{
				res = SW_STATUS_INVALID_PARAMETER;
				}

#endif // defined(SW_PLATFORM_WINDOWS)
			}

		return res;
		}


// Close connections
sw_status_t
SWService::close()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

#if defined(SW_PLATFORM_WINDOWS)
		// If previously open - close up previous connections.
		if (m_svcHandle)
			{
			CloseServiceHandle(m_svcHandle);
			m_svcHandle = NULL;
			}

		closeManager();
#endif // defined(SW_PLATFORM_WINDOWS)

		return res;
		}


// Copy constructor
SWService::SWService(const SWService &other)
		{
		*this = other;
		}


// Assignment operator
SWService &
SWService::operator=(const SWService &other)
		{
		close();
		open(other.name());

		return *this;
		}



// Check to see if the service is running
SWServiceState
SWService::state() const
		{
		SWServiceState	res=SvcStateUnknown;

#if defined(SW_PLATFORM_WINDOWS)
		SERVICE_STATUS	svcStatus;

		memset(&svcStatus, 0, sizeof(svcStatus));

		if (isOpen())
			{
			if (QueryServiceStatus(m_svcHandle, &svcStatus) != FALSE)
				{
				switch (svcStatus.dwCurrentState)
					{
					case SERVICE_STOPPED:
						res = SvcStateStopped;
						break;

					case SERVICE_START_PENDING:
						res = SvcStateStartPending;
						break;

					case SERVICE_STOP_PENDING:
						res = SvcStateStopPending;
						break;

					case SERVICE_RUNNING:
						res = SvcStateRunning;
						break;

					case SERVICE_CONTINUE_PENDING:
						res = SvcStateContinuePending;
						break;

					case SERVICE_PAUSE_PENDING:
						res = SvcStatePausePending;
						break;

					case SERVICE_PAUSED:
						res = SvcStatePaused;
						break;
					}
				}
			}
#endif // defined(SW_PLATFORM_WINDOWS)

		return res;
		}



// Set the status to the requested state (wanted) and
// optionally wait for it to reach that state
sw_status_t
SWService::control(SWServiceState wanted, sw_uint64_t waitms=SW_TIMEOUT_INFINITE)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

#if defined(SW_PLATFORM_WINDOWS)
		SWServiceState	ss;
		DWORD			command=0;

		// If it's already in the required state do nothing
		ss = state();
		if (ss == SvcStateUnknown) return SW_STATUS_SYSTEM_ERROR;
		if (ss == wanted) return SW_STATUS_SUCCESS;

		switch (wanted)
			{
			case SvcStateStopped:
				if (ss != SvcStateStopPending) command = SERVICE_CONTROL_STOP;
				break;

			case SvcStateRunning:
				if (ss != SvcStateStartPending && ss != SvcStateContinuePending)
					{
					// There is no command for start so use 0xffffffff instead
					command = ss==SvcStateStopped?0xffffffff:SERVICE_CONTROL_CONTINUE;
					}
				break;

			case SvcStatePaused:
				// Check to make sure the process is not being stopped.
				if (ss == SvcStateStopped || ss == SvcStateStopPending)
					{
					res = SW_STATUS_ILLEGAL_OPERATION;
					}
				if (res == SW_STATUS_SUCCESS && ss != SvcStatePausePending) command = SERVICE_CONTROL_PAUSE;
				break;

			default:
				// Unsupported
				res = SW_STATUS_INVALID_PARAMETER;
				break;
			}

		if (res == SW_STATUS_SUCCESS && command != 0)
			{
			if (command == 0xffffffff)
				{
				// Do a service start
				if (StartService(m_svcHandle, 0, NULL) == 0)
					{
					// It failed, get the error and quit
					res = SW_STATUS_ILLEGAL_OPERATION;
					}
				}
			else
				{
				SERVICE_STATUS	tmp;

				if (ControlService(m_svcHandle, command, &tmp) == 0)
					{
					// It failed, get the error and quit
					res = SW_STATUS_ILLEGAL_OPERATION;
					}
				}
			}
				
		if (res == SW_STATUS_SUCCESS && waitms > 0)
			{
			SWTime  now;
			double	start, end, ms;

			now.update();
			start = now.toDouble();
			ms = (double)waitms / 1000.0;
			while (state() != wanted)
				{
				now.update();
				end = now.toDouble();
				if ((end - start) > ms)
					{
					res = SW_STATUS_TIMEOUT;
					}

				// Wait 1/4 a second and try again
				::Sleep(250);
				}
			}
		
#else // defined(SW_PLATFORM_WINDOWS)
		res = SW_STATUS_UNSUPPORTED_OPERATION;
#endif // defined(SW_PLATFORM_WINDOWS)

		return res;
		}



// Service control
bool
SWService::stop(sw_uint64_t waitms)
		{
#if defined(SW_PLATFORM_WINDOWS)
		return (control(SvcStateStopped, waitms) == SW_STATUS_SUCCESS);
#else // defined(SW_PLATFORM_WINDOWS)
		return false;
#endif // defined(SW_PLATFORM_WINDOWS)
		}

bool
SWService::start(sw_uint64_t waitms)
		{
#if defined(SW_PLATFORM_WINDOWS)
		return (control(SvcStateRunning, waitms) == SW_STATUS_SUCCESS);
#else // defined(SW_PLATFORM_WINDOWS)
		return false;
#endif // defined(SW_PLATFORM_WINDOWS)
		}

bool
SWService::pause(sw_uint64_t waitms)
		{
#if defined(SW_PLATFORM_WINDOWS)
		return (control(SvcStatePaused, waitms) == SW_STATUS_SUCCESS);
#else // defined(SW_PLATFORM_WINDOWS)
		return false;
#endif // defined(SW_PLATFORM_WINDOWS)
		}


// Get Info
int
SWService::getType() const
		{
#if defined(SW_PLATFORM_WINDOWS)
		return m_config->dwServiceType;
#else // defined(SW_PLATFORM_WINDOWS)
		return ~0;
#endif // defined(SW_PLATFORM_WINDOWS)
		}


int
SWService::getStartType() const
		{
		int		res=0;

#if defined(SW_PLATFORM_WINDOWS)
		res = m_config->dwStartType;
#else // defined(SW_PLATFORM_WINDOWS)
		return ~0;
#endif // defined(SW_PLATFORM_WINDOWS)

		return res;
		}


int	
SWService::getErrorControl() const
		{
#if defined(SW_PLATFORM_WINDOWS)
		return m_config->dwErrorControl;
#else // defined(SW_PLATFORM_WINDOWS)
		return ~0;
#endif // defined(SW_PLATFORM_WINDOWS)
		}


SWString
SWService::getProgramName() const
		{
		SWString	v;

#if defined(SW_PLATFORM_WINDOWS)
		v = m_config->lpBinaryPathName;
#else // defined(SW_PLATFORM_WINDOWS)
		v = m_name;
#endif // defined(SW_PLATFORM_WINDOWS)

		return v;
		}


SWString
SWService::getLoadOrderGroup() const
		{
		SWString	v;

#if defined(SW_PLATFORM_WINDOWS)
		v = m_config->lpLoadOrderGroup;
#endif // defined(SW_PLATFORM_WINDOWS)

		return v;
		}


int	
SWService::getTagId() const
		{
#if defined(SW_PLATFORM_WINDOWS)
		return m_config->dwTagId;
#else // defined(SW_PLATFORM_WINDOWS)
		return 0;
#endif // defined(SW_PLATFORM_WINDOWS)
		}


/**
*** Returns a list of services this service depends upon.
**/
void
SWService::getDependencies(SWStringArray &sa) const
		{
		sa.clear();

#if defined(SW_PLATFORM_WINDOWS)
		SWMultiString	ms(m_config->lpDependencies);

		ms.toStringArray(sa);
#endif // defined(SW_PLATFORM_WINDOWS)
		}


/**
*** Returns a list of services that depend upon this service
**/
void
SWService::getDependents(SWStringArray &sa) const
		{
		sa.clear();

#if defined(SW_PLATFORM_WINDOWS)
		ENUM_SERVICE_STATUS	*pDependents=0;
		int					nDependents=8;
		bool				ok=false;

		pDependents = new ENUM_SERVICE_STATUS[nDependents];
		while (!ok)
			{
			DWORD	needed=0, count=0;

			if (!EnumDependentServices(m_svcHandle, SERVICE_STATE_ALL, pDependents, nDependents * sizeof(ENUM_SERVICE_STATUS), &needed, &count))
				{
				// printf("EnumDependentServices failed\n");
				if (GetLastError() == ERROR_MORE_DATA)
					{
					delete [] pDependents;

					//printf("Not enough space (was %d needed %d)\n", nDependents * sizeof(ENUM_SERVICE_STATUS), needed);

					// The needed value returned is not always an exact multiple of
					// of the size - so we adjust it here.
					nDependents = (needed / sizeof(ENUM_SERVICE_STATUS)) + 2;
					pDependents = new ENUM_SERVICE_STATUS[nDependents];
					}
				else
					{
					// We got an error so break out.
					// printf("Other error\n");
					break;
					}
				}
			else
				{
				// printf("Got %d names\n", count);
				for (int j=0;j<(int)count;j++)
					{
					sa.add(pDependents[j].lpServiceName);
					}

				ok = true;
				}
			}

		delete [] pDependents;
#endif // defined(SW_PLATFORM_WINDOWS)
		}


SWString
SWService::getServiceStartName() const
		{
		SWString	v;

#if defined(SW_PLATFORM_WINDOWS)
		v = m_config->lpServiceStartName;
#else // defined(SW_PLATFORM_WINDOWS)
		v = m_name;
#endif // defined(SW_PLATFORM_WINDOWS)

		return v;
		}


SWString
SWService::getDisplayName() const
		{
		SWString	v;

#if defined(SW_PLATFORM_WINDOWS)
		v = m_config->lpDisplayName;
#else // defined(SW_PLATFORM_WINDOWS)
		v = m_name;
#endif // defined(SW_PLATFORM_WINDOWS)

		return v;
		}


void
SWService::getServiceList(SWArray<SWServiceEntry> &list, int type)
		{
		list.clear();

#if defined(SW_PLATFORM_WINDOWS)
		openManager();

		if (m_mgrHandle)
			{
			sw_byte_t	*bp=new sw_byte_t[256 * 1024];
			DWORD		needed=0, count=0, handle=0, r;
			BOOL		b;

			for (;;)
				{
				b = EnumServicesStatus(m_mgrHandle, SERVICE_WIN32, SERVICE_STATE_ALL, (ENUM_SERVICE_STATUS *)bp, 256 * 1024, &needed, &count, &handle);
				r = GetLastError();
				if (b || r == ERROR_MORE_DATA)
					{
					ENUM_SERVICE_STATUS	*sp=(ENUM_SERVICE_STATUS *)bp;

					for (int i=0;i<(int)count;i++,sp++)
						{
						SWServiceEntry	&rec=list[(int)list.size()];

						rec.name = sp->lpServiceName;
						rec.title = sp->lpDisplayName;

						switch (sp->ServiceStatus.dwCurrentState)
							{
							case SERVICE_STOPPED:			rec.state = SvcStateStopped;			break;
							case SERVICE_START_PENDING:		rec.state = SvcStateStartPending;		break;
							case SERVICE_STOP_PENDING:		rec.state = SvcStateStopPending;		break;
							case SERVICE_RUNNING:			rec.state = SvcStateRunning;			break;
							case SERVICE_CONTINUE_PENDING:	rec.state = SvcStateContinuePending;	break;
							case SERVICE_PAUSE_PENDING:		rec.state = SvcStatePausePending;		break;
							case SERVICE_PAUSED:			rec.state = SvcStatePaused;				break;
							default:						rec.state = SvcStateUnknown;			break;
							}
						}
					}

				if (r == ERROR_MORE_DATA) continue;
				break;
				}

			safe_delete_array(bp);
			}
		
		closeManager();
#endif // defined(SW_PLATFORM_WINDOWS)
		}

