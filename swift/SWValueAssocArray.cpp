/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"

#include <swift/SWValueAssocArray.h>
#include <swift/SWGuard.h>



SWValueAssocArray::SWValueAssocArray(bool ignoreCase) :
	m_ignoreCase(ignoreCase)
		{
		}


SWValue &	
SWValueAssocArray::operator[](const SWString &key)
		{
		SWGuard								guard(this);
		SWHashMapEntry<SWString,SWValue>	*pEntry;

		pEntry = m_data.getMapEntry(key);
		if (pEntry == NULL)
			{
			void				*p;

			p = m_data.m_pElementMemoryManager->malloc(m_data.m_elemsize);
			if (p == NULL) throw SW_EXCEPTION(SWMemoryException);
			pEntry = new (p) SWHashMapEntry<SWString,SWValue>(key, "");
			m_data.addEntry(pEntry);
			m_data.incSize();

			//printf("New SWString at %x\n", vp);
			}

		return pEntry->m_value;
		}



// Copy constructor
SWValueAssocArray::SWValueAssocArray(SWValueAssocArray &other)
		{
		// Do copy via assignment operator
		*this = other;
		}



SWValueAssocArray &
SWValueAssocArray::operator=(const SWValueAssocArray &other)
		{
		SWGuard		guard(this);

		clear();
		m_ignoreCase = other.m_ignoreCase;

		SWIterator< SWHashMapEntry<SWString,SWValue> >	it = other.iterator();
		SWHashMapEntry<SWString,SWValue>				*me;

		while (it.hasNext())
			{
			me = it.next();
			
			(*this)[me->key()] = me->value();
			}

		return *this;
		}


// Returns an array of the keys contained in this map.
SWStringArray
SWValueAssocArray::keysArray() const
		{
		SWGuard					guard(this);
		SWStringArray			sa;
		SWIterator<SWString>	it;
		bool					done=false;
		
		while (!done)
			{
			try
				{
				it = keys();

				while (it.hasNext())
					{
					sa.add(*(SWString *)it.next());
					}

				done = true;
				}
			catch (SWConcurrentModificationException &)
				{
				sa.clear();
				done = false;
				}
			}

		return sa;
		}


// Returns an array of the values contained in this map.
SWValueArray
SWValueAssocArray::valuesArray() const
		{
		SWGuard					guard(this);
		SWValueArray			sa;
		SWIterator<SWValue>		it;
		bool					done=false;
		
		while (!done)
			{
			try
				{
				it = values();

				while (it.hasNext())
					{
					sa.add(*(SWValue *)it.next());
					}
				
				done = true;
				}
			catch (SWConcurrentModificationException &)
				{
				sa.clear();
				done = false;
				}
			}

		return sa;
		}


bool
SWValueAssocArray::defined(const SWString &key) const
		{
		SWString	kstr(key);

		if (m_ignoreCase) kstr.toLower();

		return m_data.containsKey(key);
		}


bool
SWValueAssocArray::remove(const SWString &key)
		{
		SWString	kstr(key);

		if (m_ignoreCase) kstr.toLower();

		return m_data.remove(key);
		}


void
SWValueAssocArray::clear()
		{
		m_data.clear();
		}



// Returns an iterator of MapEntry objects
SWIterator< SWHashMapEntry<SWString,SWValue> >
SWValueAssocArray::iterator() const
		{
		return m_data.iterator();
		}


// Returns an array of values
SWIterator<SWValue>
SWValueAssocArray::values() const
		{
		return m_data.values();
		}


// Returns an array of keys
SWIterator<SWString>
SWValueAssocArray::keys() const
		{
		return m_data.keys();
		}


bool
SWValueAssocArray::lookup(const SWString &key, SWValue &value) const
		{
		SWGuard								guard(this);
		SWHashMapEntry<SWString,SWValue>	*pEntry;
		bool	res;

		pEntry = m_data.getMapEntry(key);
		if (pEntry != NULL)
			{
			value = pEntry->value();
			res = true;
			}
		else
			{
			res = false;
			}

		return res;
		}


void
SWValueAssocArray::set(const SWString &key, const SWValue &value)
		{
		(*this)[key] = value;
		}


sw_size_t
SWValueAssocArray::size() const
		{
		return m_data.size();
		}



