/**
*** A collection of internal functions
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <swift/swift.h>
#include <swift/SWException.h>

#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif

#if defined(linux)
#include <wctype.h>
#endif



SWIFT_DLL_EXPORT char *
strnew(const char *s)
		{
		char		*sp;
		sw_size_t	nchars;

		nchars = strlen(s) + 1;
		sp = new char[nchars];
		if (sp == NULL) throw SW_EXCEPTION(SWMemoryException);

		strcpy(sp, s);

		return sp;
		}


SWIFT_DLL_EXPORT char *
strnew(const wchar_t *s, int codepage)
		{
		sw_size_t	slen, nlen;
		char		*sp;
		
		slen = wcslen(s);
		nlen = sw_wcstombs(NULL, s, slen+1, codepage);

		if (nlen != (sw_size_t)-1)
			{
			sp = new char[nlen+1];
			if (sp == NULL) throw SW_EXCEPTION(SWMemoryException);
			sw_wcstombs(sp, s, slen+1, codepage);
			sp[nlen] = 0;
			}
		else
			{
			sp = new char[1];
			*sp = 0;
			}

		return sp;
		}


SWIFT_DLL_EXPORT wchar_t *
wcsnew(const char *s, int codepage)
		{
		wchar_t		*sp;
		sw_size_t	nchars;

		nchars = strlen(s) + 1;
		sp = new wchar_t[nchars];
		if (sp == NULL) throw SW_EXCEPTION(SWMemoryException);

		int	nc;

		nc = (int)sw_mbstowcs(sp, s, nchars, codepage);
		if (nc < 0) nc = 0;
		sp[nc] = 0;

		return sp;
		}


SWIFT_DLL_EXPORT wchar_t *
wcsnew(const wchar_t *s)
		{
		wchar_t		*sp;
		sw_size_t	nchars;

		nchars = wcslen(s) + 1;
		sp = new wchar_t[nchars];
		if (sp == NULL) throw SW_EXCEPTION(SWMemoryException);

		wcscpy(sp, s);

		return sp;
		}



