/**
*** @file		SWDataObject.h
*** @brief		A generic data object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWDataObject class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWDataObject_h__
#define __SWDataObject_h__

#include <swift/swift.h>

#include <swift/SWString.h>
#include <swift/SWTime.h>
#include <swift/SWIPAddress.h>
#include <swift/SWValueAssocArray.h>
#include <swift/SWUInt32.h>
#include <swift/SWDate.h>
#include <swift/SWLocalTime.h>
#include <swift/SWValue.h>
#include <swift/SWString.h>
#include <swift/SWHashMap.h>
#include <swift/SWInt32.h>
#include <swift/SWDouble.h>

#include <swift/SWConfigKey.h>



class SWIFT_DLL_EXPORT SWDataObject : public SWObject
		{
public:
		SWDataObject();
		SWDataObject(const SWDataObject &other);
		virtual ~SWDataObject();

		SWDataObject &	operator=(const SWDataObject &other);
		// bool			operator==(const SWDataObject &other);

		/// Copies values from the other object, but does not clear the existing ones
		void			copyValues(const SWDataObject &other);

		void					changed(bool v)		{ m_changed = v; }
		bool					changed() const		{ return m_changed; }

		virtual void			clear();
		
		virtual bool			contains(const SWString &key) const;
		virtual void			remove(const SWString &key);
		virtual bool			get(const SWString &key, SWValue &value) const;
		virtual bool			set(const SWString &key, const SWValue &value);

		virtual SWString		getString(const SWString &key) const;
		virtual SWString		getString(const SWString &key, const SWString &defval) const;
		virtual sw_int16_t		getInt16(const SWString &key, sw_int16_t defval=0) const;
		virtual sw_uint16_t		getUInt16(const SWString &key, sw_uint16_t defval=0) const;
		virtual sw_int32_t		getInt32(const SWString &key, sw_int32_t defval=0) const;
		virtual sw_uint32_t		getUInt32(const SWString &key, sw_uint32_t defval=0) const;
		virtual sw_int64_t		getInt64(const SWString &key, sw_int64_t defval=0) const;
		virtual sw_uint64_t		getUInt64(const SWString &key, sw_uint64_t defval=0) const;
		virtual SWDouble		getDouble(const SWString &key, double defval=0.0) const;
		virtual SWMoney			getMoney(const SWString &key) const;

		virtual SWValue			getValue(const SWString &key) const;
		virtual SWDate			getDate(const SWString &key) const;
		virtual SWLocalTime		getTime(const SWString &key) const;
		virtual bool			getBoolean(const SWString &key, bool defval=false) const;

		virtual bool			getFlag(const SWString &key, int id, bool &v) const;
		virtual bool			setFlag(const SWString &key, int id, bool v);
		virtual bool			isFlagSet(const SWString &key, int id) const;

		SWIterator<SWString>	keys(bool iterateFromEnd=false) const	{ return m_param.keys(iterateFromEnd); }
		void					getValueNames(SWStringArray &sa) const;

		/// Read the message from the given input stream
		virtual sw_status_t		readFromStream(SWInputStream &s);

		/// Write the message to the given output stream
		virtual sw_status_t		writeToStream(SWOutputStream &s) const;
		
protected:
		SWString				getKeyName(const SWString &key) const;

protected:
		SWHashMap<SWString, SWValue>	m_param;
		bool							m_changed;
		bool							m_keysAreCaseInsensitive;
		};

#endif
