/**
*** @file		SWHashMap.h
*** @brief		A template map class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWHashMap class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWHashMap_h__
#define __SWHashMap_h__

#include <swift/SWList.h>
#include <swift/SWGuard.h>
#include <swift/SWHashMap.h>
#include <swift/SWHashCode.h>
#include <swift/SWIterator.h>



// Forward Declarations
template<class K, class V> class SWHashMap;
template<class K, class V> class SWHashMapIterator;


/**
*** @internal
*** @brief	An internal object used to encapsuate the key-value data.
**/
template<class K, class V>
class SWHashMapEntry
		{
friend class SWHashMap<K,V>;
friend class SWHashMapIterator<K,V>;
friend class SWStringAssocArray;
friend class SWValueAssocArray;

public:

		/// Return a reference to the key data
		const K &		key() const									{ return m_key; }

		/// Return a reference to the value data
		V &				value()										{ return m_value; }

protected:

		/// Default constructor
		SWHashMapEntry()											{ }

		/// Key/Value constructor
		SWHashMapEntry(const K &key, const V &value) : next(0), prev(0), m_key(key), m_value(value)				{ }

		/// Custom new operator
		void *			operator new(size_t /*nbytes*/, void *p)	{ return p; }

		/// Custom delete operator
		void			operator delete(void *, void *)				{ }

		/// Custom delete operator
		void			operator delete(void *)						{ }

		/// Assignment operator
		SWHashMapEntry<K,V> &	operator=(const SWHashMapEntry<K,V> &other)	{ m_key = other.m_key; m_value = other.m_value; return *this; }

protected:
		SWHashMapEntry<K,V>	*next;		///< The next element in the chain
		SWHashMapEntry<K,V>	*prev;		///< The previous element in the chain
		K					m_key;		///< The key
		V					m_value;	///< The value
		};


/**
*** @brief	A key-value map template.
***
*** SWHashMap is a templated class that implements a hashed key-value map.
*** Unlike several other collections only the templated form of hash map
*** is available due to the need to be able to calculate hash values on the 
*** elements inserted.
***
*** The hash map does not allows duplicate keys to be inserted, but duplicate values
*** can be insrted.
***
*** Developers need to provide a method of calculating the hash value of the
*** inserted data. This can be done in one of several ways:
*** - Numeric cast			- If a numeric cast is already available then it will be used.
*** - Derive from SWObject	- The data type is an object that is dervied from SWObject
***							  then the hashCode method can be overridden to determine the hashcode.
*** - SWHashCode cast		- Providing a SWHashCode cast will allow this template to compile.
***
*** The data type used must also have a == and assignment operator.
***
*** @par General Attributes
*** <table class="attributes">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>MT-Level</td>
***		<td>Safe when synchronized</td>
*** </tr>
*** </table>
***
*** @par Collection Attributes
*** <table class="collection">
*** <tr>
***		<td><b>Attribute</b></td>
***		<td><b>Value</b></td>
*** </tr>
*** <tr>
***		<td>Internal Structure</td>
***		<td>Hash table of SWMapEntry objects</td>
*** </tr>
*** <tr>
***		<td>Stored Data Type</td>
***		<td>Pairs of templated data (key and value)</td>
*** </tr>
*** <tr>
***		<td>Allows duplicate values?</td>
***		<td>
***			<ul>
***			<li>Duplicate keys - No
***			<li>Duplicate values - Yes
***			</ul>
***		</td>
*** </tr>
*** <tr>
***		<td>Ordered?</td>
***		<td>No</td>
*** </tr>
*** <tr>
***		<td>Sortable?</td>
***		<td>No</td>
*** </tr>
*** <tr>
***		<td>Requirements for stored type</td>
***		<td>
***			<ul>
***			<li>Copy constructor
***			<li>operator=
***			<li>operator==
***			</ul>
***		</td>
*** </tr>
*** </table>
***
***
*** @see		SWHashMap, SWCollection
*** @author		Simon Sparkes
**/
template<class K, class V>
class SWHashMap : public SWCollection
		{
friend class SWHashMapIterator<K,V>;
friend class SWStringAssocArray;
friend class SWValueAssocArray;

public:
		/// A enumerator used internally to specify the iterator type
		enum SWHashMapIteratorType
			{
			ITER_ENTRIES=0,		///< An iterator or key/value pairs
			ITER_KEYS=1,		///< An iterator of keys
			ITER_VALUES=2		///< An iterator of values
			};

public:
		/**
		*** @brief	Constructor
		***
		*** @param[in]	initialCapacity		Specifies the inital number of buckets used in the map.
		*** @param[in]	loadFactor			Specifies the load factor used to trigger auto rehashing.
		*** @param[in]	autoRehash			Specifies if auto-rehashing is enabled or not.
		**/
		SWHashMap(int initialCapacity=23, float loadFactor=0.75, bool autoRehash=false);


		/// Virtual destructor
		virtual ~SWHashMap();


		/**
		*** Copy constructor
		**/
		SWHashMap(const SWHashMap<K, V> &other);


		/**
		*** Assignment operator
		**/
		SWHashMap<K, V> &	operator=(const SWHashMap<K, V> &other);


		/**
		*** @brief	Clears the list of all entries
		**/
		virtual void				clear();


		/**
		*** @brief	Put the value at the location specified by the key
		***
		*** The specified value is stored at the location specified by the key.
		*** If an entry matching the key does not already exist within the map
		*** it will be created and the value inserted. If an entry for the specified 
		*** key does exist the existing value will be replaced the new one. If the 
		*** pOldValue parameter is supplied it will receive a copy of the previous value
		*** if any.
		***
		*** If a new entry is created to store the key this method will return true. If
		*** the entry already existed and the existing value was replaced then this
		*** method will return false.
		***
		*** @param[in]	key			The key used to "locate" the value
		*** @param[in]	value		The value to store at the location specified by the key.
		*** @param[out]	pOldValue	If specified this variable receives a copy of any data
		***							that was replaced when an entry for the specified key
		***							already exists.
		***
		*** @return					true if this resulted in a new entry, false if an existing
		***							entry was updated.
		**/
		bool						put(const K &key, const V &value, V *pOldValue=NULL);
		
		
		
		/**
		*** @brief	Get the value that matches the specified key.
		***
		*** This method looks for the specified key in the map and if found
		*** it returns the data associated with it. If the key was found this method
		*** returns true and fills in the value parameter with the discovered data.
		*** If the key was not found this method returns false and the value parameter
		*** is unchanged.
		***
		*** @param[in]	key		Specifies the key to search for in the collection.
		*** @param[out]	value	If the specified key if found this receives a copy of the data.
		***
		*** @return				true if the data was found, false otherwise.
		**/
		bool						get(const K &key, V &value) const;

		
		/**
		*** @brief	Remove the specified key (and any associated value) from the map.
		***
		*** This method searches for the specified key and if found the entry
		*** is removed. If an entry is found the value being removed is returned to the 
		*** caller in the pValue paramater (if supplied).
		***
		*** @param[in]	key		Specifies key to delete from the map.
		*** @param[out]	pValue	If specified and is not NULL this receives a copy of the
		***						value that was removed.
		***
		*** @return				true if the key specified was found and removed, false
		***						otherwise.
		**/
		bool						remove(const K &key, V *pValue=NULL);


		/**
		*** @brief	Checks to see if the collection contains the specified key.
		***
		*** @param[in]	key		Specifies key to search for.
		***
		*** @return				true if found, false otherwise.
		**/
		bool						containsKey(const K &key) const;


		/**
		*** @brief	Checks to see if the collection contains the specified value.
		***
		*** <b>Note:</b>In order to find a value the entire collection must be walked
		*** until the value is found. This will be much slower than the containsKey method
		*** which uses the hash value to find the key more quickly.
		***
		*** @param[in]	value	Specifies value to search for.
		***
		*** @return				true if found, false otherwise.
		**/
		bool						containsValue(const V &value) const;


		/**
		*** @brief	Returns an iterator over the entries in this collection.
		***
		*** This method returns an iterator that returns pointers to 
		*** SWMapEntry objects. These are used internally to store the key
		*** value pairs.
		***
		*** @param	iterateFromEnd	If false (default) the iterator starts
		***							at the beginning of the vector. If true
		***							the iterator starts at the end of the
		***							vector.
		**/
		SWIterator< SWHashMapEntry<K,V> >	iterator(bool iterateFromEnd=false) const;


		/**
		*** @brief	Returns an iterator over the keys in this collection.
		***
		*** This method returns an iterator that returns pointers to 
		*** key data elements.
		***
		*** @param	iterateFromEnd	If false (default) the iterator starts
		***							at the beginning of the vector. If true
		***							the iterator starts at the end of the
		***							vector.
		**/
		SWIterator<K>				keys(bool iterateFromEnd=false) const;


		/**
		*** @brief	Returns an iterator over the values in this collection.
		***
		*** This method returns an iterator that returns pointers to 
		*** value data elements.
		***
		*** @param	iterateFromEnd	If false (default) the iterator starts
		***							at the beginning of the vector. If true
		***							the iterator starts at the end of the
		***							vector.
		**/
		SWIterator<V>				values(bool iterateFromEnd=false) const;


		/**
		*** @brief	Rehashes the hash table by doubling the number of buckets.
		**/
		virtual void				rehash();

protected:
		typedef struct hmbucket
			{
			public:
				SWHashMapEntry<K,V>	*head;
				SWHashMapEntry<K,V>	*tail;
			} Bucket;

protected:
		int								bucket(const K &key) const;
		void							addEntry(SWHashMapEntry<K,V> *pNewEntry);
		void							removeEntry(Bucket *pBucket, SWHashMapEntry<K,V> *pEntry);
		SWHashMapEntry<K,V> *			getMapEntry(const K &key) const;

		virtual SWCollectionIterator *	getIterator(bool iterateFromEnd=false) const;
		SWCollectionIterator *			getIterator(SWHashMapIteratorType type, bool iterateFromEnd) const;

protected:
		SWMemoryManager		*m_pBucketMemoryManager;	///< The memory manager for the buckets
		SWMemoryManager		*m_pElementMemoryManager;	///< The memory manager for the elements
		sw_size_t			m_elemsize;					///< The size of each element to be stored
		float				m_loadFactor;				///< The load factor
		int					m_threshold;				///< The number of elements threshold which when exceeded causes a rehash takes place.
		int					m_bucketCount;				///< The capacity (number of buckets)
		Bucket				*m_bucketArray;				///< An array of buckets
		bool				m_autoRehash;				///< A flag to indicate if auto-rehashing is enabled
		};



// Include the implementation
#include <swift/SWHashMap.cpp>

#endif
