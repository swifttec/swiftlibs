/**
*** @file		SWSharedMemoryManager.h
*** @brief		An memory allocator that sub-allocates from	a shared memory segment.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWSharedMemoryManager class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWSharedMemoryManager_h__
#define __SWSharedMemoryManager_h__

#include <swift/SWMemoryManager.h>
#include <swift/SWMutex.h>
#include <swift/SWSharedMemory.h>






/**
*** @brief A memory allocator that sub-allocates from a shared memory segment.
***
*** This is a memory allocator that takes a shared memory segment and sub-allocates
*** it. The structures in held shared memory use offsets rather than addresses which
*** allows multiple processes to access the segment concurrently as long as suitable
*** locking is observed.
***
*** @see		SWAbstractMemoryManager
*** @author		Simon Sparkes
*/
class SWIFT_DLL_EXPORT SWSharedMemoryManager : public SWMemoryManager
		{
public:
		/**
		*** Default constructor
		**/
		SWSharedMemoryManager();

		/**
		*** @brief Construct an allocator connected to the shared memory segment with the specified ID.
		**/
		SWSharedMemoryManager(sw_uint32_t id, sw_uint32_t size, SWMutex *pMutex=NULL, sw_uint32_t reserved=0);
		virtual ~SWSharedMemoryManager();


		/**
		*** @brief Attach to an existing shared memory segment
		***
		*** Attaches to the shared memory segment with the specified ID and verifies
		*** that is useable by this class by checking for some magic numbers in key
		*** locations.
		***
		*** If a mutex is specified then this is stored and used for synchronising
		*** access to the shared memory segment. If supplied this should be a mutex
		*** type that can synchronise at the process level, e.g. SWProcessMutex or
		*** SWProcessRWMutex. <b>Care should be taken to use the same mutex in all processes
		*** that use this segment.</b>
		***
		*** If a mutex is not specified then a SWProcessMutex is created with the same ID
		*** as specified for the shared memory segment. <b>This is the preferred method
		*** of synchronisation for this object</b>
		***
		*** @param[in]	id			The ID of the shared memory segment to attach to.
		*** @param[in]	pMutex		The mutex to use for syncronisation on the segment.
		***							If this parameter is NULL a SWProcessMutex is created
		*** @param[in]	reserved	The number of bytes to reserve a the beginning of the
		***							shared memory segment.
		***
		*** @return	SW_STATUS_SUCCESS if successful, or an error code otherwise.
		**/
		sw_status_t		attach(sw_uint32_t id, SWMutex *pMutex=NULL, sw_uint32_t reserved=0);

		/**
		*** @brief Create a new shared memory segment with the specified size.
		***
		*** Creates a shared memory segment with the specified ID and initialises it
		*** for use by this class. This will fail if the segment already exists.
		***
		*** If a mutex is specified then this is stored and used for synchronising
		*** access to the shared memory segment. If supplied this should be a mutex
		*** type that can synchronise at the process level, e.g. SWProcessMutex or
		*** SWProcessRWMutex. <b>Care should be taken to use the same mutex in all processes
		*** that use this segment.</b>
		***
		*** If a mutex is not specified then a SWProcessMutex is created with the same ID
		*** as specified for the shared memory segment. <b>This is the preferred method
		*** of synchronisation for this object</b>
		***
		*** @param[in]	id			The ID of the shared memory segment to create.
		*** @param[in]	size		The size of the shared memory segment to create.
		*** @param[in]	pMutex		The mutex to use for syncronisation on the segment.
		***							If this parameter is NULL a SWProcessMutex is created
		*** @param[in]	reserved	The number of bytes to reserve a the beginning of the
		***							shared memory segment.
		***
		*** @return	SW_STATUS_SUCCESS if successful, or an error code otherwise.
		**/
		sw_status_t		create(sw_uint32_t id, sw_uint32_t size, SWMutex *pMutex=NULL, sw_uint32_t reserved=0);

		/**
		*** @brief	Detaches from the shared memory segment.
		***
		*** @return	SW_STATUS_SUCCESS if successful, or an error code otherwise.
		**/
		sw_status_t		detach();

		/**
		*** @brief	Determine if we are attached to a segment or not.
		**/
		bool			isAttached() const	{ return _shmem.isAttached(); }

		/**
		*** @brief	Get the ID of the currently attached segment
		**/
		sw_uint32_t		id() const			{ return _shmem.id(); }

		/**
		*** @brief	Get the size of the currently attached segment
		**/
		sw_uint32_t		size() const		{ return _shmem.size(); }

		/**
		*** @internal
		*** @brief			Dump the memory structures to the specified file handle (default=stdout) for debugging purposes.
		*** @param[in]	f	The FILE handle to write the output to.
		**/
		void			dump(FILE *f=stdout);


public: // SWMemoryManager virtual overrides

		/**
		*** @brief Allocate a memory block (malloc).
		***
		*** Allocates the specified number of bytes of memory from the memory pool
		*** used by the allocator. If the number of bytes requested is zero or no
		*** more memory is available for allocation this function will return a NULL
		*** pointer.
		***
		*** @param	nbytes		The number of bytes required.
		*** @return				A pointer to the memory buffer or NULL if no more memory
		***						can be allocated by the allocator.
		**/
		virtual void *	malloc(sw_size_t nbytes);

		/**
		*** @brief Resize an allocated memory block (realloc).
		***
		*** Changes the size of the data block pointed to by the specified pointer
		*** to be able to store the specified amount of data. If the data block
		*** is a NULL pointer this method allocates a new buffer of the size specified
		*** as if the malloc method had been called.
		***
		*** If the number of bytes requested is zero and the old data pointer is not NULL
		*** the existing data block will be freed (as if the free method had been called)
		*** and a NULL pointer returned.
		***
		*** If no memory is available for allocation this function will return a NULL
		*** pointer but will leave the existing data block intact.
		***
		*** <b>Notes:</b>
		*** - The memory pointer returned may not be the same as the one passed in.
		*** - If the old data passed in was not allocated by this allocator then an exception
		***   will (should) be thrown.
		***
		*** @param	pOldData	The pointer the data block to be resized.
		*** @param	nbytes		The number of bytes required.
		*** @return				A pointer to the memory buffer or NULL if no more memory
		***						can be allocated by the allocator.
		***
		*** @throws	SWMemoryException
		**/
		virtual void *	realloc(void *pOldData, sw_size_t nbytes);

		/**
		*** @brief	Release memory back to the allocator (free).
		***
		*** This method releases the memory block specified back to the allocator.
		*** Once released the memory block should not be re-used.
		***
		*** <b>Note:</b>
		*** If the old data passed in was not allocated by this allocator then an exception
		*** will (should) be thrown.
		***
		*** @param	pData	The pointer the data block to be released.
		***
		*** @throws	SWMemoryException
		**/
		virtual void	free(void *pData);


		/**
		*** @brief	Translate an offset into an actual address
		***
		*** This method is mostly for internal purposes but can be used to translate
		*** a segment offset into an allocated address when received from another
		*** process. The other process can convert an address to a segment offset
		*** by using the offset() method.
		***
		*** @param[in]	offset	The offset to convert to an address.
		***
		*** @return				The address correspnong to the given offset.
		**/
		void *			address(sw_uint32_t offset) const		{ return (reinterpret_cast<char *>(_shmem.address()) + offset); }


		/**
		*** @brief	Translate an address into a offset
		***
		*** This method is mostly for internal purposes but can be used to translate
		*** an allocated address into a segment offset which can be passed to another
		*** process. The other process can convert the received offset back into a valid
		*** address by using the address() method.
		***
		*** @param[in]	addr	The address to convert to an offset.
		***
		*** @return				The offset correspnong to the given address.
		**/
		sw_uint32_t		offset(const void *addr) const				{ return (sw_uint32_t)(reinterpret_cast<const char *>(addr) - reinterpret_cast<const char *>(_shmem.address())); }


protected:
		/**
		*** @internal
		***
		*** The header from a shared memory block (allocated or in free list)
		**/
		typedef struct shm_memory_header
				{
				sw_uint32_t		magic;		///< A sanity check magic number
				sw_uint32_t		next;		///< The offset of the next block in the chain
				sw_uint32_t		prev;		///< The offset of the previous block in the chain
				sw_size_t		actualSize;	///< The actual size of the block
				sw_uint32_t		refcount;	///< The number of references to this block
				} ShmMemoryHeader;


		/**
		*** @internal
		***
		*** The header from a shared memory block (allocated or in free list)
		**/
		typedef struct shm_memory_list
				{
				sw_uint32_t		magic;		///< A sanity check magic number
				sw_uint32_t		head;		///< The offset of the first element in the list
				sw_uint32_t		tail;		///< The offset of the last element in the list
				sw_uint32_t		nelems;		///< The number of elements in the list
				sw_size_t		size;		///< The size (in bytes) of the list
				sw_uint32_t		ordered;	///< non-zero if list is ordered
				} ShmMemoryList;


protected:
		/// Get a pointer to the memory list at the specified offset.
		ShmMemoryList *		getListAtOffset(sw_uint32_t offset)		{ return reinterpret_cast<ShmMemoryList *>(address(offset)); }

		/// Get a pointer to the memory block header at the specified offset.
		ShmMemoryHeader *	getHeaderAtOffset(sw_uint32_t offset)	{ return reinterpret_cast<ShmMemoryHeader *>(address(offset)); }

		/// Get a pointer to the memory free list
		ShmMemoryList *		getFreeList()							{ return getListAtOffset(_reserved); }

		/// Get a pointer to the memory in-use list
		ShmMemoryList *		getInUseList()							{ return getListAtOffset(_reserved + sizeof(ShmMemoryList)); }

		/// Add the specified memory block to the specified list
		void				addToList(ShmMemoryList *pList, ShmMemoryHeader *pHdr);

		/// Remove the specified memory block from the specified list
		void				removeFromList(ShmMemoryList *list, ShmMemoryHeader *block);

		/// Add the specified memory block to the free list
		void				insertIntoFreeList(ShmMemoryHeader *hdr);

protected:
		SWSharedMemory	_shmem;		///< The shared memory segment.
		SWMutex			*_pMutex;	///< A pointer to the mutex that protects the shared memory segment.
		bool			_ownMutex;	///< A flag to indicate if we created the mutex
		sw_uint32_t		_reserved;	///< The number of reserved bytes at the start of the memory segment
		};





#endif
