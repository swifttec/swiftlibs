/**
*** @file		SWFixedSizeMemoryManager.h
*** @brief		An memory allocator that sub-allocates from local memory.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWFixedSizeMemoryManager class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWFixedSizeMemoryManager_h__
#define __SWFixedSizeMemoryManager_h__

#include <swift/SWMemoryManager.h>






/**
*** @brief A memory manager that sub-allocates fixed-size blocks from local memory.
***
*** This memory manager is designed to be more efficient than the system memory management
*** functions (::malloc, ::realloc and ::free) when allocating fixed-size blocks. It works
*** by requesting a large memory block (known internally as a super-block) from the O/S
*** and sub-allocting it to the caller. Allocated blocks that are freed are placed on
*** an internal free list for rapid re-use.
***
*** @note
*** The memory manager uses the interface derived from the SWMemoryManager class, but on the
*** malloc and realloc calls the parameter specifying the number of bytes is ignored and the
*** returned blocks are always of the size specified on the constructor.
***
*** @see		SWMemoryManager
*** @author		Simon Sparkes
*/
class SWIFT_DLL_EXPORT SWFixedSizeMemoryManager : public SWMemoryManager
		{
public:
		/**
		*** @brief	Construct a memory manager that allocates fixed-size memory blocks
		***
		*** This method constructs a memory manager that sub-allocates fixed-size blocks
		*** when the malloc method is called. The memory is sub-allocated from large blocks
		*** (super-blocks) which are in turn allocated from a memory manager. By default
		*** an instance of the SWLocalMemoryManager class is used to allocate large blocks
		*** but the caller can specify an alternate manager if sub allocation is required
		*** from another memory manager.
		***
		*** @param[in]	blocksize		The size of block to allocate. If zero then JIT sizing is used.
		*** @param[in]	minblocks		The minimum number of blocks held in each super-block.
		*** @param[in]	maxblocks		The maximum number of blocks held in each super-block.
		*** @param[in]	pMemoryManager	If not NULL a pointer to the memory manager to allocate
		***								super-blocks from.
		**/
		SWFixedSizeMemoryManager(sw_size_t blocksize, sw_size_t minblocks=128, sw_size_t maxblocks=1024, SWMemoryManager *pMemoryManager=NULL);

		/// Virtual destructor
		virtual ~SWFixedSizeMemoryManager();

		virtual void *	malloc(sw_size_t nbytes);
		virtual void *	realloc(void *pOldData, sw_size_t nbytes);
		virtual void	free(void *pData);

private:
		typedef struct memblock
			{
			struct memblock	*next;
			} MemoryBlock;

		typedef struct memsuperblock
			{
			struct memsuperblock	*next;		///< A pointer to the next memory superblock
			sw_size_t				nelements;	///< The size (in elements) of the superblock
			sw_size_t				nextfree;	///< The index of the next free element
			} MemorySuperBlock;

		sw_size_t			m_blocksize;			///< The size of each block to be allocated by this manager
		sw_size_t			m_blocksPerSuper;		///< The number of blocks in each superblock
		sw_size_t			m_maxBlocksPerSuper;	///< The maximum number of blocks in each superblock
		MemorySuperBlock	*m_superlist;			///< The list of memory superblocks
		MemoryBlock			*m_freelist;			///< The list of free memory blocks
		SWMemoryManager		*m_pMemoryManager;		///< The memory manager to allocate superblocks from
		};





#endif
