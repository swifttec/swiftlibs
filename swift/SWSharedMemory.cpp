/**
*** @file		SWSharedMemory.cpp
*** @brief		An object for handling shared memory segments.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWSharedMemory class.
***
*** <b>Special note for windows implementation:</b>
*** Share memory is not available as for Unix so we use a file mapping technique
*** as suggested by Microsoft. However using a page file is not persistent so we
*** must use temp files.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <config/config.h>

#include <swift/SWSharedMemory.h>
#include <swift/SWString.h>
#include <swift/SWFilename.h>
#include <swift/SWFile.h>
#include <swift/SWGuard.h>
#include <swift/SWFolder.h>
#include <swift/SWFileInfo.h>

#if !defined(_WIN32) && !defined(WIN32)
	#include <unistd.h>
	#include <sys/ipc.h>
	#include <sys/shm.h>
#else
	#include <Shlobj.h>
#endif

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif



#if defined(SW_PLATFORM_WINDOWS)
#include <Accctrl.h>
#include <Aclapi.h>
static void
grantEverbodyAccessToFile(const SWString &FileName)
		{
		PSID pEveryoneSID = NULL;
		PACL pACL = NULL;
		EXPLICIT_ACCESS ea[1];
		SID_IDENTIFIER_AUTHORITY SIDAuthWorld = SECURITY_WORLD_SID_AUTHORITY;

		// Create a well-known SID for the Everyone group.
		AllocateAndInitializeSid(&SIDAuthWorld, 1,
							SECURITY_WORLD_RID,
							0, 0, 0, 0, 0, 0, 0,
							&pEveryoneSID);

		// Initialize an EXPLICIT_ACCESS structure for an ACE.
		// The ACE will allow Everyone read access to the key.
		ZeroMemory(&ea, 1 * sizeof(EXPLICIT_ACCESS));
		ea[0].grfAccessPermissions = 0xFFFFFFFF;
		ea[0].grfAccessMode = GRANT_ACCESS;
		ea[0].grfInheritance= NO_INHERITANCE;
		ea[0].Trustee.TrusteeForm = TRUSTEE_IS_SID;
		ea[0].Trustee.TrusteeType = TRUSTEE_IS_WELL_KNOWN_GROUP;
		ea[0].Trustee.ptstrName  = (LPTSTR) pEveryoneSID;

		// Create a new ACL that contains the new ACEs.
		SetEntriesInAcl(1, ea, NULL, &pACL);

		// Initialize a security descriptor.  
		PSECURITY_DESCRIPTOR pSD = (PSECURITY_DESCRIPTOR) LocalAlloc(LPTR, 
									SECURITY_DESCRIPTOR_MIN_LENGTH); 

		InitializeSecurityDescriptor(pSD,SECURITY_DESCRIPTOR_REVISION);

		// Add the ACL to the security descriptor. 
		SetSecurityDescriptorDacl(pSD, 
				TRUE,     // bDaclPresent flag   
				pACL, 
				FALSE);   // not a default DACL 


		//Change the security attributes
		SetFileSecurity(FileName, DACL_SECURITY_INFORMATION, pSD);

		if (pEveryoneSID) 
			FreeSid(pEveryoneSID);
		if (pACL) 
			LocalFree(pACL);
		if (pSD) 
			LocalFree(pSD);
		}

static SWString
getSharedMemoryFilename(sw_uint32_t id, SWString &name)
		{
		SWString		filename, foldername;
		WCHAR		tmpdir[1024];

		SHGetFolderPath(0, CSIDL_COMMON_APPDATA, 0, 0, tmpdir);
		foldername = SWFilename::concatPaths(tmpdir, "SharedMemory");
		if (!SWFileInfo::isFolder(foldername))
			{
			SWFolder::create(foldername);
			}

#ifndef SW_CONFIG_WINDOWS_SHMFILE_PREFIX
	#define SW_CONFIG_WINDOWS_SHMFILE_PREFIX	"SWIFTbase_SWSharedMemory"
#endif

		name.format(_T(SW_CONFIG_WINDOWS_SHMFILE_PREFIX "_%u"), id);
		filename = SWFilename::concatPaths(foldername, name);
		
		return filename;
		}
#endif




SWSharedMemory::SWSharedMemory() :
#if defined(WIN32) || defined(_WIN32)
	_fileHandle(0),
	_memHandle(0),
#else
	_handle(0),
#endif
	_addr(0),
	_id(0),
	_size(0)
		{
		}


SWSharedMemory::SWSharedMemory(sw_uint32_t id) :
#if defined(WIN32) || defined(_WIN32)
	_fileHandle(0),
	_memHandle(0),
#else
	_handle(0),
#endif
	_addr(0),
	_id(0),
	_size(0)
		{
		if (attach(id) != SW_STATUS_SUCCESS) throw SW_EXCEPTION(SWSharedMemoryException);
		}


SWSharedMemory::SWSharedMemory(sw_uint32_t id, sw_uint32_t size) :
#if defined(WIN32) || defined(_WIN32)
	_fileHandle(0),
	_memHandle(0),
#else
	_handle(0),
#endif
	_addr(0),
	_id(0),
	_size(0)
		{
		if (create(id, size) != SW_STATUS_SUCCESS) throw SW_EXCEPTION(SWSharedMemoryException);
		}


SWSharedMemory::~SWSharedMemory()
		{
		// Detach but ignore any errors.
		detach();
		}


sw_status_t
SWSharedMemory::attach(sw_uint32_t id)
		{
		SWGuard		guard(this);
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (!isAttached())
			{
#if defined(_WIN32) || defined(_WIN32)
			SWString	filename, name;
			DWORD		size, sizehigh;

			filename = getSharedMemoryFilename(id, name);

			_fileHandle = CreateFile(filename, GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
			if (_fileHandle == INVALID_HANDLE_VALUE)
				{
				res = GetLastError();
				}
			else
				{
				size = GetFileSize(_fileHandle, &sizehigh);
				_memHandle = CreateFileMapping(_fileHandle, NULL, PAGE_READWRITE, 0, size, name);
				if (_memHandle == NULL)
					{
					res = GetLastError();
					CloseHandle(_fileHandle);
					_fileHandle = INVALID_HANDLE_VALUE;
					}
				else
					{
					_addr = MapViewOfFile(_memHandle, FILE_MAP_ALL_ACCESS, 0, 0, size);
					if (_addr == NULL)
						{
						res = GetLastError();
						CloseHandle(_memHandle);
						CloseHandle(_fileHandle);
						_fileHandle = INVALID_HANDLE_VALUE;
						}
					else
						{
						_size = size;
						}
					}
				}
#else // defined(_WIN32) || defined(_WIN32)
			_handle = -1;
			if ((_handle = shmget((int)id, 0, 0666)) >= 0) 
				{
				_addr = shmat(_handle, 0, 0666);
				if (_addr == (void *)-1)
					{
					// The attach failed.
					res = errno;
					}
				else
					{
					// The attach succeeded - get the size for verification purposes
					struct shmid_ds	info;

					// Get the size of the segment
					if (shmctl(_handle, IPC_STAT, &info) != 0)
						{
						res = errno;
						shmdt((char *)_addr);
						_addr = 0;
						}
					else
						{
						_size = info.shm_segsz;
						}
					}
				}
			else 
				{
				res = errno;
				}
#endif // defined(_WIN32) || defined(_WIN32)
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}


		if (res == SW_STATUS_SUCCESS) _id = id;

		return res;
		}


sw_status_t
SWSharedMemory::create(sw_uint32_t id, sw_uint32_t size)
		{
		SWGuard		guard(this);
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (!isAttached())
			{
#if defined(_WIN32) || defined(_WIN32)
			SWString	filename, name;

			filename = getSharedMemoryFilename(id, name);

			_fileHandle = CreateFile(filename, GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);
			if (_fileHandle == INVALID_HANDLE_VALUE)
				{
				res = GetLastError();
				}
			else
				{
				grantEverbodyAccessToFile(filename);
				_memHandle = CreateFileMapping(_fileHandle, NULL, PAGE_READWRITE, 0, size, name);
				if (_memHandle == NULL)
					{
					res = GetLastError();
					CloseHandle(_fileHandle);
					_fileHandle = INVALID_HANDLE_VALUE;
					}
				else
					{
					_addr = MapViewOfFile(_memHandle, FILE_MAP_ALL_ACCESS, 0, 0, size);
					if (_addr == NULL)
						{
						res = GetLastError();
						CloseHandle(_memHandle);
						CloseHandle(_fileHandle);
						_fileHandle = INVALID_HANDLE_VALUE;
						}
					else
						{
						_size = size;
						}
					}
				}
#else // defined(_WIN32) || defined(_WIN32)
			_handle = -1;
			if ((_handle = shmget(id, (unsigned)size, IPC_CREAT|IPC_EXCL|0666)) >= 0) 
				{
				_addr = shmat(_handle, 0, 0666);
				if (_addr == (void *)-1)
					{
					// The attach failed.
					res = errno;
					}
				else
					{
					// The attach succeeded - get the size for verification purposes
					struct shmid_ds	info;

					// Get the size of the segment
					if (shmctl(_handle, IPC_STAT, &info) != 0)
						{
						res = errno;
						shmdt((char *)_addr);
						_addr = 0;
						}
					else
						{
						_size = info.shm_segsz;
						}
					}
				}
			else 
				{
				res = errno;
				}
#endif // defined(_WIN32) || defined(_WIN32)
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}

		if (res == SW_STATUS_SUCCESS) _id = id;

		return res;
		}



sw_status_t
SWSharedMemory::detach(bool delflag)
		{
		SWGuard		guard(this);
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (isAttached())
			{
#if defined(_WIN32) || defined(WIN32)
			FlushViewOfFile(_addr, 0);
			UnmapViewOfFile(_addr);
			CloseHandle(_memHandle);
			CloseHandle(_fileHandle);
#else // defined(_WIN32) || defined(WIN32)
			// Need a cast here despite the fact the manual page
			// says it's a void *
			if (_handle >= 0 && _addr != (void *)-1) shmdt((char *)_addr);
			_handle = 0;
#endif // defined(_WIN32) || defined(WIN32)
			}
		else
			{
			res = SW_STATUS_ILLEGAL_OPERATION;
			}

		if (res == SW_STATUS_SUCCESS)
			{
			if (delflag)
				{
				// Remove the segment
				remove(_id);
				}

			_id = 0;
			_addr = 0;
			_size = 0;
			}

		return res;
		}


bool
SWSharedMemory::isAttached() const
		{
		return (_addr != NULL);
		}



//------------------------------------------------------------
// Static Methods
//------------------------------------------------------------

// Remove an unwanted segment
sw_status_t
SWSharedMemory::remove(sw_uint32_t id)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

#if defined(_WIN32) || defined(WIN32)
		SWString	filename, name;
		
		filename = getSharedMemoryFilename(id, name);

		res = SWFile::remove(filename);
#else // defined(_WIN32) || defined(WIN32)
		int	sm;

		if ((sm = shmget(id, 0, 0444)) < 0 || shmctl(sm, IPC_RMID, NULL) < 0)
			{
			res = errno;
			}
#endif // defined(_WIN32) || defined(WIN32)

		return res;
		}

// End namespace

