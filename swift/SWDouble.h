/**
*** @file		SWDouble.h
*** @brief		A class to represent a date
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWDouble class.
***
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __SWDouble_h__
#define __SWDouble_h__

#include <swift/swift.h>
#include <swift/SWObject.h>
#include <swift/SWException.h>
#include <swift/SWString.h>




// Forward declarations
class SWTime;

/**
*** @brief	A generic date class based on Julian Day Numbers.
***
***	This is a generic date class that represents dates as Julian Day Numbers.
*** This gives a enormous range of dates that are stored in a 32-bit integer value.
***
*** The class stores date information only and has no time component. However it
*** can easily be converted both to and from a SWTime value. Where both date and
*** time information are required developers should use the SWTime class.
***
*** @author		Simon Sparkes
*/
class SWIFT_DLL_EXPORT SWDouble
		{
public:
		SWDouble(double amount=0.0);
		SWDouble(int amount);
		SWDouble(const char *s);
		SWDouble(const wchar_t *s);
		SWDouble(const SWString &s);

		SWDouble &	operator=(const char *s);
		SWDouble &	operator=(const wchar_t *s);
		SWDouble &	operator=(const SWString &s);
		SWDouble &	operator=(int amount)		{ m_amount = (double)amount; return *this; }
		SWDouble &	operator=(double amount)	{ m_amount = amount; return *this; }

		SWString		toString(const SWString &fmt="%f") const;

		operator double() const			{ return m_amount; }
		operator double &()				{ return m_amount; }

private:
		double	m_amount;
		};





#endif
