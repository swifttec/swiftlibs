/**
*** @file		SWMutex.cpp
*** @brief		An abstract class defining the interface for all  mutex objects
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the SWMutex class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




#include "stdafx.h"

#include <swift/SWMutex.h>
#include <swift/SWException.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif




// Protected Constructor
SWMutex::SWMutex(sw_uint32_t flags) :
	m_flags(flags)
		{
		}


SWMutex::~SWMutex()
		{
		}


void
SWMutex::fullRelease(int &readLockCount, int &writeLockCount)
		{
		readLockCount = writeLockCount = 0;

		// Release any write locks first as these take precedence
		while (hasWriteLock())
			{
			release();
			writeLockCount++;
			}

		// Release any read locks second
		while (hasReadLock())
			{
			release();
			readLockCount++;
			}
		}


void
SWMutex::fullAcquire(int readLockCount, int writeLockCount)
		{
		while (readLockCount-- > 0) acquireReadLock();
		while (writeLockCount-- > 0) acquireWriteLock();
		}


// default behaviour is to return 0
void
SWMutex::getOwnerAndLockCounts(sw_thread_id_t &tid, int &readLockCount, int &writeLockCount)
		{
		tid = 0;
		readLockCount = writeLockCount = 0;
		}


// default behaviour is to return 0
void
SWMutex::setOwnerAndLockCounts(sw_thread_id_t /*tid*/, int /*readLockCount*/, int /*writeLockCount*/)
		{
		}


sw_status_t
SWMutex::acquireReadLock(sw_uint64_t waitms)
		{
		return acquire(waitms);
		}


sw_status_t
SWMutex::acquireWriteLock(sw_uint64_t waitms)
		{
		return acquire(waitms);
		}



