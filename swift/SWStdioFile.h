/**
*** @file		SWStdioFile.h
*** @brief		Basic file handling class
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the SWStdioFile class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/




#ifndef __SWStdioFile_h__
#define __SWStdioFile_h__

#include <swift/swift.h>

#include <swift/SWObject.h>
#include <swift/SWString.h>






/**
*** @brief	Basic file handling class.
***
*** SWStdioFile is a basic file handling class which hides the 32/64 bit implementation
*** of the underlying operating system by providing a 64-bit interface.
*** SWStdioFile does not do any data caching or buffering an provides an interface
*** that is as close to the underlying O/S as possible.
***
*** @author		Simon Sparkes
**/
class SWIFT_DLL_EXPORT SWStdioFile : public SWObject
		{
public:
		/// Construct a file object ready to be opened.
		SWStdioFile();

		/// Virtual destructor
		virtual ~SWStdioFile();

		/**
		*** @brief	Opens the file
		***
		*** @return SW_STATUS_SUCCESS if successful or the error code.
		**/
		sw_status_t			open(const SWString &filename, const SWString &mode);

		/**
		*** @brief	Closes the file
		***
		*** @return SW_STATUS_SUCCESS if successful or the error code.
		**/
		sw_status_t			close();

		/// Return the open status of the file.
		bool				isOpen() const;

		/// Returns the current size of the file.
		sw_filesize_t		size();

		/// Cast to a FILE
		operator FILE *()	{ return (FILE *)m_hFile; }

		/// Flush output if possible
		void				flush();

private: // Members
		FILE		*m_hFile;			///< A handle to the file
		};




#endif
