#!perl

sub fix
		{
		my ($filename) = @_;

		if (-f $filename)
			{
			my $oldfilename="$filename.bak";
			my $newfilename=$filename;
			my $overwrite=0;

			print "Fixing $filename\n";

			my $oldfilename="$filename.bak";
			my $newfilename=$filename;

			if (-f "$filename.bak") 
				{
				unlink("$filename.bak");
				}

			rename($filename, "$filename.bak");

			if (-f $newfilename)
				{
				print "$newfilename already exists\n";
				return;
				}

			open(IN, "$oldfilename");
			open(OUT, ">$newfilename");

			while ($line = <IN>)
				{
				$line =~ s/CBASE/XPLATFORM/g;
				$line =~ s/cbase/xplatform/g;

				print OUT $line;
				}

			close(OUT);
			close(IN);
			}
		else
			{
			print "$file is not a file\n";
			}
		}


foreach $file (glob "@ARGV")
	{
	fix($file);
	}
