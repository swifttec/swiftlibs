/**
*** @file		AMAddress.cpp
*** @brief		Basic database access object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Implements the AMAddress class
***
*** <b>Copyright 2008 SwiftTec. All rights reserved.</b>
**/
/****************************************************************************
** $Date: 2008/12/18 15:08:01 $
** $Author: simon $
** $Revision: 1.1 $
*****************************************************************************
** $Log: AMAddress.cpp,v $
** Revision 1.1  2008/12/18 15:08:01  simon
** Initial version
**
***************************************************************************/


#include "stdafx.h"

#include <addrmgmt/AMAddress.h>


AMAddress::AMAddress()
		{
		}


AMAddress::AMAddress(const AMAddress &other)
		{
		*this = other;
		}


AMAddress::~AMAddress()
		{
		}


AMAddress &
AMAddress::operator=(const AMAddress &other)
		{
		AMDataObject::operator=(other);
			
		return *this;
		}


SWString
AMAddress::toString(const SWString &separator) const
		{
		SWString	s, v;

		s = property();

		v = street();
		if (!v.isEmpty())
			{
			if (!s.isEmpty())
				{
				s += separator;
				}
			s += v;
			}

		v = locality();
		if (!v.isEmpty())
			{
			if (!s.isEmpty()) s += separator;
			s += v;
			}

		v = town();
		if (!v.isEmpty())
			{
			if (!s.isEmpty()) s += separator;
			s += v;
			}

		v = county();
		if (!v.isEmpty())
			{
			if (!s.isEmpty()) s += separator;
			s += v;
			}

		v = postcode();
		if (!v.isEmpty())
			{
			if (!s.isEmpty()) s += separator;
			s += v;
			}

		return s;
		}
