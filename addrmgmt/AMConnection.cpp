/**
*** @file		AMConnection.cpp
*** @brief		Basic database access object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Implements the AMConnection class
***
*** <b>Copyright 2008 SwiftTec. All rights reserved.</b>
**/
/****************************************************************************
** $Date: 2008/12/18 15:08:01 $
** $Author: simon $
** $Revision: 1.1 $
*****************************************************************************
** $Log: AMConnection.cpp,v $
** Revision 1.1  2008/12/18 15:08:01  simon
** Initial version
**
****************************************************************************/


#include "stdafx.h"

#include <swift/SWTokenizer.h>
#include <swift/sw_crypt.h>

#include <addrmgmt/AMConnection.h>

AMConnection::AMConnection()
		{
		}


AMConnection::~AMConnection()
		{
		}


sw_status_t
AMConnection::open(const SWString &params)
		{
		// We have a connection, now attempt to open the database using the parameters given
		SWArray<SWNamedValue>	va;
		SWTokenizer				tok(params, "&");
		SWString				s, n, v;
		int						p;
		
		while (tok.hasMoreTokens())
			{
			s = tok.nextToken();
			p = s.first('=');
			if (p < 0)
				{
				n = s;
				v.empty();
				}
			else
				{
				n = s.left(p);
				v = s.mid(p+1);
				}

			va.add(SWNamedValue(n, v));
			}

		m_params = params;

		return open(va);
		}


SWString
AMConnection::getConnectionString() const
		{
		return m_params;
		}
