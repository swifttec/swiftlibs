#include "stdafx.h"

#include <swift/SWFlags32.h>
#include <swift/SWFilename.h>
#include <swift/SWFileInfo.h>
#include <swift/SWFile.h>
#include <swift/SWFolder.h>
#include <swift/SWLocalTime.h>
#include <swift/sw_crypt.h>

#include <addrmgmt/AMDataObject.h>

AMDataObject::AMDataObject() :
	m_changed(false),
	m_loaded(false)
		{
		}


AMDataObject::~AMDataObject()
		{
		}


AMDataObject::AMDataObject(const AMDataObject &other)
		{
		*this = other;
		}


AMDataObject &
AMDataObject::operator=(const AMDataObject &other)
		{
#define	COPY(x)	x = other.x
		COPY(m_changed);
		COPY(m_loaded);
#undef COPY

		m_param.clear();
		SWIterator< SWHashMapEntry<SWString, SWValue> >		it;
		SWHashMapEntry<SWString, SWValue>					*pEntry;
		
		it = other.m_param.iterator();
		while (it.hasNext())
			{
			pEntry = it.next();
			m_param.put(pEntry->key(), pEntry->value());
			}
			
		return *this;
		}




void
AMDataObject::clear()
		{
		m_param.clear();
		m_changed = false;
		m_loaded = false;
		}


bool
AMDataObject::set(const SWString &key, const SWValue &value)
		{
		SWValue	v;
		
		if (!m_param.get(key, v) || v != value)
			{
			m_changed = true;
			m_param.put(key, value);
			}
		
		return true;
		}
		
		
bool
AMDataObject::get(const SWString &key, SWValue &value) const
		{
		value.clear();

		return m_param.get(key, value);
		}


SWString
AMDataObject::getString(const SWString &key) const
		{
		SWString	s;
		SWValue		v;
		
		if (get(key, v))
			{
			s = v.toString();
			}
		
		return s;
		}


bool
AMDataObject::getBoolean(const SWString &key) const
		{
		return (getInt32(key) != 0);
		}



SWInt32
AMDataObject::getInt32(const SWString &key) const
		{
		SWInt32	s;
		SWValue		v;
		
		if (get(key, v))
			{
			s = v.toInt32();
			}
		
		return s;
		}


SWValue
AMDataObject::getValue(const SWString &key) const
		{
		SWValue		v;
		
		get(key, v);
		
		return v;
		}


SWDate
AMDataObject::getDate(const SWString &key) const
		{
		SWDate		dt;
		SWValue		v;
		
		if (get(key, v))
			{
			if (v.type() == SWValue::VtTime)
				{
				// When a time - is in localtime format
				if (v.toInt32() != 0)
					{
					dt = SWLocalTime(v.toTime());
					}
				}
			else
				{
				dt = v.toDate();
				}
			}
		
		return dt;
		}


SWLocalTime
AMDataObject::getTime(const SWString &key) const
		{
		SWLocalTime	t;
		SWValue		v;
		
		if (get(key, v))
			{
			t = SWLocalTime(v.toTime());
			}
		
		return t;
		}

