/**
*** @file		AMAddress.h
*** @brief		Basic database access object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Defines the AMAddress class
***
*** <b>Copyright 2008 SwiftTec. All rights reserved.</b>
**/
/****************************************************************************
** $Date: 2008/12/18 15:08:01 $
** $Author: simon $
** $Revision: 1.1 $
*****************************************************************************
** $Log: AMAddress.h,v $
** Revision 1.1  2008/12/18 15:08:01  simon
** Initial version
**
****************************************************************************/


#ifndef __addrmgmt_AMAddress_h__
#define __addrmgmt_AMAddress_h__

#include <addrmgmt/AMtypes.h>
#include <addrmgmt/AMDataObject.h>

#if defined(SW_PLATFORM_WINDOWS)
	#pragma warning(disable: 4251)
#endif


class ADDRMGMT_DLL_EXPORT AMAddress : public AMDataObject
		{
public:
		AMAddress();
		AMAddress(const AMAddress &other);
		virtual ~AMAddress();
		
		AMAddress &			operator=(const AMAddress &other);

		SWString		property() const				{ return getString("property"); }
		void			property(const SWString &v)		{ set("property", v); }

		SWString		street() const					{ return getString("street"); }
		void			street(const SWString &v)		{ set("street", v); }

		SWString		locality() const				{ return getString("locality"); }
		void			locality(const SWString &v)		{ set("locality", v); }

		SWString		town() const					{ return getString("town"); }
		void			town(const SWString &v)			{ set("town", v); }

		SWString		county() const					{ return getString("county"); }
		void			county(const SWString &v)		{ set("county", v); }

		SWString		postcode() const				{ return getString("postcode"); }
		void			postcode(const SWString &v)		{ set("postcode", v); }

		SWString		toString(const SWString &separator="\n") const;
		};


#endif // __addrmgmt_AMAddress_h__
