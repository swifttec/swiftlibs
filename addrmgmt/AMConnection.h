/**
*** @file		AMConnection.h
*** @brief		Basic database access object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Defines the AMConnection class
***
*** <b>Copyright 2008 SwiftTec. All rights reserved.</b>
**/
/****************************************************************************
** $Date: 2008/12/18 15:08:01 $
** $Author: simon $
** $Revision: 1.1 $
*****************************************************************************
** $Log: AMConnection.h,v $
** Revision 1.1  2008/12/18 15:08:01  simon
** Initial version
**
****************************************************************************/


#ifndef __addrmgmt_AMConnection_h__
#define __addrmgmt_AMConnection_h__

#include <addrmgmt/AMtypes.h>
#include <addrmgmt/AMAddress.h>

#include <swift/SWString.h>
#include <swift/SWArray.h>
#include <swift/SWNamedValue.h>


class ADDRMGMT_DLL_EXPORT AMConnection
		{
protected:
		AMConnection();

public:
		virtual ~AMConnection();

		/// Open/connect to the database
		virtual sw_status_t		open(const SWString &params);

		/// Open/connect to the database
		virtual sw_status_t		open(const SWArray<SWNamedValue> &va)=0;

		/// Close the database
		virtual sw_status_t		close()=0;

		/// Execute a string lookup
		virtual sw_status_t		lookup(int type, const SWString &value, SWArray<AMAddress> &results)=0;

		/// Get the connection details
		virtual SWString		getConnectionString() const;

protected:
		SWString	m_params;	//< The opening parameters
		};

#endif // __addrmgmt_AMConnection_h__
