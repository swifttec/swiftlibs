/**
*** @file		AMDataObject.h
*** @brief		Object to represent a single database record (row)
*** @version	1.0
*** @author		Simon Sparkes
***
*** Object to represent a single database record (row)
***
*** <b>Copyright 2008 SwiftTec. All rights reserved.</b>
**/
/****************************************************************************
** $Date: 2008/12/18 15:08:01 $
** $Author: simon $
** $Revision: 1.1 $
*****************************************************************************
** $Log: AMDataObject.h,v $
** Revision 1.1  2008/12/18 15:08:01  simon
** Initial version
**
****************************************************************************/


#ifndef __addrmgmt_AMDataObject_h__
#define __addrmgmt_AMDataObject_h__

#include <addrmgmt/AMtypes.h>

#include <swift/SWUint32.h>
#include <swift/SWDate.h>
#include <swift/SWLocalTime.h>
#include <swift/SWValue.h>
#include <swift/SWString.h>
#include <swift/SWHashMap.h>
#include <swift/SWInt32.h>
#include <swift/SWConfigKey.h>

#if defined(SW_PLATFORM_WINDOWS)
	#pragma warning(disable: 4251)
#endif

class ADDRMGMT_DLL_EXPORT AMDataObject
		{
friend class AMDatabase;

public:
		AMDataObject();
		AMDataObject(const AMDataObject &other);
		virtual ~AMDataObject();
		
		AMDataObject &		operator=(const AMDataObject &other);

		virtual void		clear();
		bool				changed() const		{ return m_changed; }
		bool				loaded() const		{ return m_loaded; }
		bool				isNewRecord() const	{ return !m_loaded; }

		void				changed(bool v)		{ m_changed = v; }
		
		virtual bool		get(const SWString &key, SWValue &value) const;
		virtual bool		set(const SWString &key, const SWValue &value);

		virtual SWString	getString(const SWString &key) const;
		virtual SWInt32		getInt32(const SWString &key) const;
		virtual SWValue		getValue(const SWString &key) const;
		virtual SWDate		getDate(const SWString &key) const;
		virtual SWLocalTime	getTime(const SWString &key) const;
		virtual bool		getBoolean(const SWString &key) const;

protected:
		bool							m_changed;
		bool							m_loaded;
		SWHashMap<SWString, SWValue>	m_param;
		};

#endif // __addrmgmt_AMRecord_h__
