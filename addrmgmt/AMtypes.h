/**
*** @file		types.h
*** @brief		DB-specific types
*** @version	1.0
*** @author		Simon Sparkes
***
*** Defines the types used by the DB library.
***
*** <b>Copyright 2007 SwiftTec. All rights reserved.</b>
**/
/****************************************************************************
** $Date: 2008/12/18 15:08:02 $
** $Author: simon $
** $Revision: 1.1 $
*****************************************************************************
** $Log: types.h,v $
** Revision 1.1  2008/12/18 15:08:02  simon
** Initial version
**
** Revision 1.2  2008/04/18 07:25:07  simon
** Added service advertising support for SongManager and other suites
**
** Revision 1.1  2007/04/24 13:37:27  simon
** Initial Version
**
** Revision 1.1  2007/03/12 11:46:07  simon
** Work in progress
**
****************************************************************************/


#ifndef __addrmgmt_types_h__
#define __addrmgmt_types_h__

#include <addrmgmt/addrmgmt.h>

enum AMType
	{
	AM_TYPE_NONE=0,
	AM_TYPE_AFD,

	// Always the final type in the list
	AM_TYPE_COUNT
	};

enum AMLookup
	{
	AM_LOOKUP_NONE=0,
	AM_LOOKUP_GENERAL,
	AM_LOOKUP_POSTCODE,

	// Always the final type in the list
	AM_LOOKUP_COUNT
	};

#endif // __addrmgmt_types_h__
