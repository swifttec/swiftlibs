#include "stdafx.h"

// DB includes
#include <addrmgmt/AMAfdApiConnection.h>
#include <addrmgmt/AMAddress.h>

// Disable warning about deprecated objects
#pragma warning(disable: 4995)
#pragma warning(disable: 4251)

SWLoadableModule	AMAfdApiConnection::m_dll;
SWInterlockedInt32	AMAfdApiConnection::m_initCount(0);


// Structure for passing fields to/from the DLL
struct afdAddressData {
  char Lookup[256];
  char Name[121];
  char Organisation[121];
  char Property[121];
  char Street[121];
  char Locality[71];
  char Town[31];
  char PostalCounty[31];
  char AbbreviatedPostalCounty[31];
  char OptionalCounty[31];
  char AbbreviatedOptionalCounty[31];
  char TraditionalCounty[31];
  char AdministrativeCounty[31];
  char Postcode[11];
  char DPS[3];
  char PostcodeFrom[9];
  char PostcodeType[7];
  char MailsortCode[6];
  char Phone[21];
  char GridE[11];
  char GridN[11];
  char Miles[7];
  char Km[7];
  char Latitude[11];
  char Longitude[11];
  char JustBuilt[11];
  char UrbanRuralCode[3];
  char UrbanRuralName[61];
  char WardCode[7];
  char WardName[31];
  char Constituency[51];
  char EERCode[3];
  char EERName[41];
  char AuthorityCode[5];
  char Authority[51];
  char LEACode[4];
  char LEAName[51];
  char TVRegion[31];
  char Occupancy[7];
  char OccupancyDescription[31];
  char AddressType[7];
  char AddressTypeDescription[56];
  char UDPRN[9];
  char NHSCode[7];
  char NHSName[51];
  char NHSRegionCode[7];
  char NHSRegionName[41];
  char PCTCode[7];
  char PCTName[51];
  char CensationCode[11];
  char Affluence[31];
  char Lifestage[101];
  char AdditionalCensusInfo[201];
  char SOALower[11];
  char SOAMiddle[11];
  char Residency[7];
  char HouseholdComposition[107];
  char Business[101];
  char Size[7];
  char SICCode[11];
  char OnEditedRoll[7];
  char Gender[7];
  char Forename[31];
  char MiddleInitial[7];
  char Surname[31];
  char DateOfBirth[11];
  char PhoneMatchType[7];
  char DataSet[17];
  char CouncilTaxBand[7];
  char Product[41];
  char Key[256];
  char List[513];
  char Country[31];
  char CountryISO[4];
  afdAddressData(){     // constructor - zero the contents
    clear();
  }
  void clear(){
    memset(this,'\0',sizeof(*this));
  }
};

// Function to lookup Address data
long __stdcall AFDData(char* dataName, long operation, char* tData);
typedef long(__stdcall *AFDDATA)(char* dataName, long operation, char* tData);

// String to let the DLL know the fields required
static char afdFieldSpec[2048] = "Address@LX{::}@Lookup:256@Name:121@Organisation:121@Property:121@Street:121@Locality:71@Town:31@PostalCounty:31@AbbreviatedPostalCounty:31@OptionalCounty:31@AbbreviatedOptionalCounty:31@TraditionalCounty:31@AdministrativeCounty:31@Postcode:11@DPS:3@PostcodeFrom:9@PostcodeType:7@MailsortCode:6@Phone:21@GridE:11@GridN:11@Miles:7@Km:7@Latitude:11@Longitude:11@JustBuilt:11@UrbanRuralCode:3@UrbanRuralName:61@WardCode:7@WardName:31@Constituency:51@EERCode:3@EERName:41@AuthorityCode:5@Authority:51@LEA Code:4@LEA Name:51@TVRegion:31@Occupancy:7@OccupancyDescription:31@AddressType:7@AddressTypeDescription:56@UDPRN:9@NHSCode:7@NHSName:51@NHSRegionCode:7@NHSRegionName:41@PCTCode:7@PCTName:51@CensationCode:11@Affluence:31@Lifestage:101@AdditionalCensusInfo:201@SOALower:11@SOAMiddle:11@Residency:7@HouseholdComposition:107@Business:101@Size:7@SICCode:11@OnEditedRoll:7@Gender:7@Forename:31@MiddleInitia\
l:7@Surname:31@DateOfBirth:11@PhoneMatchType:7@DataSet:17@CouncilTaxBand:7@Product:41@Key:256@List:513@Country:31@CountryISO:4";

// Function Type Constants
#define AFD_POSTCODE_LOOKUP 0
#define AFD_POSTCODE_PROPERTY_LOOKUP 1
#define AFD_FASTFIND_LOOKUP 2
#define AFD_SEARCH 3
#define AFD_RETRIEVE_RECORD 4
#define AFD_CLEAN 7
#define AFD_GET_NEXT 32
#define AFD_LIST_BOX 64
#define AFD_SHOW_ERROR 128

// Sector Skip Constants
#define AFD_NO_SKIP 0
#define AFD_ADDRESS_SKIP 512
#define AFD_POSTCODE_SKIP 1024
#define AFD_SECTOR_SKIP 1536
#define AFD_OUTCODE_SKIP 2048
#define AFD_POST_TOWN_SKIP 2560
#define AFD_POSTCODE_AREA_SKIP 3072

// Success Code Constants
#define AFD_RECORD_BREAK 0
#define AFD_SUCCESS 1

// Error Code Constants
#define AFD_ERROR_INVALID_FIELDSPEC -1
#define AFD_ERROR_NO_RESULTS_FOUND -2
#define AFD_ERROR_INVALID_RECORD_NUMBER -3
#define AFD_ERROR_OPENING_FILES -4
#define AFD_ERROR_FILE_READ -5
#define AFD_ERROR_END_OF_SEARCH -6
#define AFD_ERROR_DATA_LICENCE_ERROR -7
#define AFD_ERROR_CONFLICTING_SEARCH_PARAMETERS -8
#define AFD_USER_CANCELLED -99

// Refiner Cleaning Code Constants
#define AFD_REFINER_PAF_MATCH 100
#define AFD_REFINER_POSTCODE_MATCH 200
#define AFD_REFINER_CHANGED_POSTCODE 201
#define AFD_REFINER_ASSUME_POSTCODE_CORRECT 202
#define AFD_REFINER_ASSUME_CHANGED_POSTCODE_CORRECT 203
#define AFD_REFINER_ASSUME_POSTCODE_ADDED_PROPERTY 204
#define AFD_REFINER_ASSUME_CHANGED_POSTCODE_ADDED_PROPERTY 205
#define AFD_REFINER_FULL_DPS_MATCH 300
#define AFD_REFINER_FULL_DPS_MATCH_NO_ORG 301
#define AFD_REFINER_FULL_DPS_MATCH_LIMITED 302
#define AFD_REFINER_STREET_MATCH 400
#define AFD_REFINER_NO_MATCH_FOUND -101
#define AFD_REFINER_AMBIGUOUS_POSTCODE -102
#define AFD_REFINER_SUGGEST_RECORD -103
#define AFD_REFINER_AMBIGUOUS_MATCH -104
#define AFD_REFINER_INTERNATIONAL_ADDRESS -105
#define AFD_REFINER_NO_RECORD_DATA -106

// Declarations for String Utilities (not required for the core API)
struct afdStringData {
  char Lookup[256];
  char Outcode[5];
  char Incode[4];
  char Search[256];
  char Replace[256];
  afdStringData(){     // constructor - zero the contents
    clear();
  }
  void clear(){
    memset(this,'\0',sizeof(*this));
  }
};

static char afdStringFieldSpec[2048] = "String@LX@Lookup:256@Outcode:5@Incode:4@Search:256@Replace:256";
#define AFD_STRING_SEARCH_REPLACE 0
#define AFD_STRING_SEARCH_REPLACE_CASE 1
#define AFD_STRING_CAPITALISE 2
#define AFD_STRING_CLEAN_LINE 3
#define AFD_STRING_CHECK_POSTCODE 4
#define AFD_STRING_CLEAN_POSTCODE 5
#define AFD_STRING_ABBREVIATE_COUNTY 6

// Declarations for Grid Utilities (not required for the core API)
struct afdGridData {
  char Lookup[256];
  char GBGridE[11];
  char GBGridN[11];
  char NIGridE[11];
  char NIGridN[11];
  char Latitude[11];
  char Longitude[11];
  char TextualLatitude[16];
  char TextualLongitude[16];
  char Km[7];
  char Miles[7];
  char GBGridEFrom[11];
  char GBGridNFrom[11];
  char NIGridEFrom[11];
  char NIGridNFrom[11];
  char LatitudeFrom[11];
  char LongitudeFrom[11];
  char TextualLatitudeFrom[16];
  char TextualLongitudeFrom[16];
  afdGridData(){     // constructor - zero the contents
    clear();
  }
  void clear(){
    memset(this,'\0',sizeof(*this));
  }
};

static char afdGridFieldSpec[2048] = "Grid@LX@Lookup:256@GBGridE:11@GBGridN:11@NIGridE:11@NIGridN:11@Latitude:11@Longitude:11@TextualLatitude:16@TextualLongitude:16@Km:7@Miles:7@GBGridEFrom:11@GBGridNFrom:11@NIGridEFrom:11@NIGridNFrom:11@LatitudeFrom:11@LongitudeFrom:11@TextualLatitudeFrom:16@TextualLongitudeFrom:16";
#define AFD_GRID_CONVERT 0
#define AFD_GRID_LOOKUP_LOCATION 1
#define AFD_GRID_DISTANCE 2

// Declarations for Map Utilities (not required for the core API)
struct afdMapData {
  char GBGridE[11];
  char GBGridN[11];
  char NIGridE[11];
  char NIGridN[11];
  char Latitude[11];
  char Longitude[11];
  char TextualLatitude[16];
  char TextualLongitude[16];
  char ImageWidth[5];
  char ImageHeight[5];
  char ImageScale[2];
  char OutputType[2];
  char Filename[256];
  afdMapData(){     // constructor - zero the contents
    clear();
  }
  void clear(){
    memset(this,'\0',sizeof(*this));
  }
};

static char afdMapFieldSpec[2048] = "Map@LX@GBGridE:10@GBGridN:10@NIGridE:10@NIGridN:10@Latitude:10@Longitude:10@TextualLatitude:15@TextualLongitude:15@ImageWidth:4@ImageHeight:4@ImageScale:1@OutputType:1@Filename:255";
#define AFD_MAP_BMP "0"
#define AFD_MAP_PNG "1"
#define AFD_MAP_MINI 0
#define AFD_MAP_FULL 1

// // Procedure to return text corresponding to the error code supplied, e.g. for display in a Message Box
static void AFDErrorText(long errorCode, char* errorTxt) {

  switch (errorCode) {
  case AFD_ERROR_INVALID_FIELDSPEC:
    strcpy(errorTxt, "Application Internal Error");  // The fieldSpec string passed to the AFD API was invalid
    break;
  case AFD_ERROR_NO_RESULTS_FOUND:
    strcpy(errorTxt, "No Results Found");
    break;
  case AFD_ERROR_INVALID_RECORD_NUMBER:
    strcpy(errorTxt, "Invalid Record Number"); // Only possible if an invalid key is passed when re-looking up a result
    break;
  case AFD_ERROR_OPENING_FILES:
    strcpy(errorTxt, "Error Opening AFD Data Files");
    break;
  case AFD_ERROR_FILE_READ:
    strcpy(errorTxt, "AFD Data File Read Error");
    break;
  case AFD_ERROR_END_OF_SEARCH:
    strcpy(errorTxt, "End Of Search"); // Shouldn't normally be displayed to the user;
    break;
  case AFD_ERROR_DATA_LICENCE_ERROR:
    strcpy(errorTxt, "Data Licence Error - Check software is registered");
    break;
  case AFD_ERROR_CONFLICTING_SEARCH_PARAMETERS:
    strcpy(errorTxt, "Conflicting Search Parameters"); // The Name and Organisation fields cannot be searched at the same time
                                                         // The UDPRN field must be searched independently of any other fields
    break;
  case AFD_USER_CANCELLED:
    strcpy(errorTxt, "User Cancelled"); // User Cancelled DLL Internal ListBox
    break;
  default:
    if (errorCode >= 0)
      strcpy(errorTxt, ""); // Success - Shouldn't call this function in this case!
    else {
      strcpy(errorTxt, "Unknown Error ");      
	  _ltoa(errorCode, errorTxt + strlen(errorTxt), 10);
    }
  }
}

// Procedure to return text corresponding to the Refiner Cleaning Result
static void
AFDRefinerCleaningText(long returnCode, char* statusTxt)
		{
		switch (returnCode)
			{
			case AFD_REFINER_PAF_MATCH:
				strcpy(statusTxt, "Record identically matches PAF Record");
				break;
			case AFD_REFINER_POSTCODE_MATCH:
				strcpy(statusTxt, "Postcode Match");
				break;
			case AFD_REFINER_CHANGED_POSTCODE:
				strcpy(statusTxt, "Postcode Match with Changed Postcode");
				break;
			case AFD_REFINER_ASSUME_POSTCODE_CORRECT:
				strcpy(statusTxt, "Postcode, Property Match Assuming The Postcode Is Correct");
				break;
			case AFD_REFINER_ASSUME_CHANGED_POSTCODE_CORRECT:
				strcpy(statusTxt, "Postcode, Property Match with Changed Postcode Assuming The Postcode Is Correct");
				break;
			case AFD_REFINER_ASSUME_POSTCODE_ADDED_PROPERTY:
				strcpy(statusTxt, "Postcode, Property Match with Non-Matched Property Added In");
				break;
			case AFD_REFINER_ASSUME_CHANGED_POSTCODE_ADDED_PROPERTY:
				strcpy(statusTxt, "Postcode, Property Match with Changed Postcode and Non-Matched Property Added In");
				break;
			case AFD_REFINER_FULL_DPS_MATCH:
				strcpy(statusTxt, "Full DPS Match");
				break;
			case AFD_REFINER_FULL_DPS_MATCH_NO_ORG:
				strcpy(statusTxt, "Full DPS Match (Ambiguous Organisation Not Retained)");
				break;
			case AFD_REFINER_FULL_DPS_MATCH_LIMITED:
				strcpy(statusTxt, "Limited DPS Match (Locality or Town information not present)");
				break;
			case AFD_REFINER_STREET_MATCH:
				strcpy(statusTxt, "Street (Non DPS) Match");
				break;
			case AFD_REFINER_NO_MATCH_FOUND:
				strcpy(statusTxt, "No Match Found");
				break;
			case AFD_REFINER_AMBIGUOUS_POSTCODE:
				strcpy(statusTxt, "Ambiguous Postcode (Non DPS Match)");
				break;
			case AFD_REFINER_SUGGEST_RECORD:
				strcpy(statusTxt, "Suggested Record");
				break;
			case AFD_REFINER_AMBIGUOUS_MATCH:
				strcpy(statusTxt, "Ambiguous Match");
				break;
			case AFD_REFINER_INTERNATIONAL_ADDRESS:
				strcpy(statusTxt, "International Address");
				break;
			case AFD_REFINER_NO_RECORD_DATA:
				strcpy(statusTxt, "No Record Data Supplied");
				break;
			default:
			AFDErrorText(returnCode, statusTxt);
			}
		}







// Loaded functions
#define MYAPI_FUNCTION_LIST \
		MYAPIFUNC(AFDData);


typedef long(__stdcall *func_AFDData)(char* dataName, long operation, char* tData);


#define MYAPIFUNC(x) func_##x				fn_##x
MYAPI_FUNCTION_LIST
#undef MYAPIFUNC



AMAfdApiConnection::AMAfdApiConnection()
		{
		SWGuard	guard(m_initCount);

		if (m_initCount == 0)
			{
			startApi();
			}

		m_initCount++;
		}


AMAfdApiConnection::~AMAfdApiConnection()
		{
		// Make sure the database is actually closed
		close();

		SWGuard	guard(m_initCount);

		if (--m_initCount == 0)
			{
			stopApi();
			}
		}


bool
AMAfdApiConnection::startApi()
		{
		bool	res=true;

		if (m_dll.open("afddata") != SW_STATUS_SUCCESS)
			{
			res = false;
			// printf("Can't load MySQL DLL\n");
			// exit(1);
			}

#define MYAPIFUNC(x) fn_##x			= (func_##x)m_dll.symbol(#x); \
		if (fn_##x == NULL) { res = false; } 
		// printf("Failed to load %s from DLL\n", #x); exit(1); }

MYAPI_FUNCTION_LIST
#undef MYAPIFUNC

		return res;
		}



bool
AMAfdApiConnection::stopApi()
		{
		// Do nothing
		return true;
		}



sw_status_t
AMAfdApiConnection::open(const SWArray<SWNamedValue> &)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (fn_AFDData == NULL)
			{
			res =  SW_STATUS_NOT_FOUND;
			}
		
		return res;
		}
		
		
sw_status_t
AMAfdApiConnection::close()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		
		return res;
		}



sw_status_t
AMAfdApiConnection::lookup(int type, const SWString &value, SWArray<AMAddress> &results)
		{
		sw_status_t		res=SW_STATUS_SUCCESS;
		afdAddressData	details;		
		long			r;
		int				lookup=0;

		switch (type)
			{
			case AM_LOOKUP_GENERAL:		lookup = AFD_FASTFIND_LOOKUP;		break;
			case AM_LOOKUP_POSTCODE:	lookup = AFD_POSTCODE_LOOKUP;		break;
			default:					res = SW_STATUS_INVALID_PARAMETER;	break;
			}

		results.clear();

		if (res == SW_STATUS_SUCCESS)
			{
			strcpy(details.Lookup, value.left(sizeof(details)-1)); // Change this to your lookup entry textbox value variable

			r = fn_AFDData(afdFieldSpec, lookup, (char*)&details);
			while (r >= 0)
				{
				if (r != AFD_RECORD_BREAK)
					{
					AMAddress	&rec=results[results.size()];

					rec.property(details.Property);
					rec.street(details.Street);
					rec.locality(details.Locality);
					rec.town(details.Town);
					rec.county(details.PostalCounty);
					rec.postcode(details.Postcode);
					}

				r = fn_AFDData(afdFieldSpec, AFD_GET_NEXT + lookup, (char*)&details);
				}
			}

		return res;
		}

