#include "stdafx.h"

#include <swift/SWTokenizer.h>
#include <swift/sw_crypt.h>


#include <addrmgmt/AMDatabase.h>
#include <addrmgmt/AMAfdApiConnection.h>


AMDatabase::AMDatabase() :
	m_pAM(NULL),
	m_type(AM_TYPE_NONE)
		{
		}


AMDatabase::~AMDatabase()
		{
		// Make sure the database is actually closed
		close();
		}


sw_status_t
AMDatabase::open(AMType type, const SWString &params)
		{
		sw_status_t		res=SW_STATUS_SUCCESS;
		AMConnection	*pAM=NULL;

		if (m_pAM != NULL)
			{
			res = SW_STATUS_ALREADY_OPEN;
			}
		else
			{
			switch (type)
				{
				case AM_TYPE_AFD:
					pAM = new AMAfdApiConnection();
					break;

				}
			}

		if (res == SW_STATUS_SUCCESS)
			{
			res = pAM->open(params);
			}

		if (res == SW_STATUS_SUCCESS)
			{
			// Record the type if successful
			m_type = type;
			m_pAM = pAM;
			}
		else if (pAM != NULL)
			{
			pAM->close();
			delete pAM;
			pAM = NULL;
			m_type = AM_TYPE_NONE;
			}

		return res;
		}


sw_status_t
AMDatabase::close()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (m_pAM != NULL)
			{
			m_pAM->close();
			delete m_pAM;
			m_pAM = NULL;
			m_type = AM_TYPE_NONE;
			}

		return res;
		}


bool
AMDatabase::isOpen() const
		{
		return (m_pAM != NULL);
		}


sw_status_t
AMDatabase::lookup(int type, const SWString &value, SWArray<AMAddress> &results)
		{
		sw_status_t	res=false;

		if (m_pAM != NULL)
			{
			res = m_pAM->lookup(type, value, results);
			}

		return res;
		}


AMType
AMDatabase::type() const
		{
		return m_type;
		}


SWString
AMDatabase::params() const
		{
		SWString	res;

		if (m_pAM != NULL)
			{
			res = m_pAM->getConnectionString();
			}

		return res;
		}
