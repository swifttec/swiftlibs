/**
*** @file		AMDatabase.h
*** @brief		Basic database access object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Defines the AMDatabase class
***
*** <b>Copyright 2008 SwiftTec. All rights reserved.</b>
**/
/****************************************************************************
** $Date: 2008/12/18 15:08:01 $
** $Author: simon $
** $Revision: 1.1 $
*****************************************************************************
** $Log: AMDatabase.h,v $
** Revision 1.1  2008/12/18 15:08:01  simon
** Initial version
**
****************************************************************************/


#ifndef __addrmgmt_AMDatabase_h__
#define __addrmgmt_AMDatabase_h__

#include <addrmgmt/AMtypes.h>
#include <addrmgmt/AMDataObject.h>
#include <addrmgmt/AMConnection.h>

#include <swift/SWString.h>
#include <swift/SWLocalTime.h>
#include <swift/SWIterator.h>
#include <swift/SWException.h>

class AMConnection;

class ADDRMGMT_DLL_EXPORT AMDatabase
		{
public:
		AMDatabase();
		virtual ~AMDatabase();

		/// Open/connect to the database
		virtual sw_status_t		open(AMType type, const SWString &params);

		/// Close the database
		virtual sw_status_t		close();

		/// Verify the database open status
		bool			isOpen() const;

		/// Return the database type
		AMType			type() const;

		/// Return the connection parameters
		SWString		params() const;

		/// Execute a string lookup
		sw_status_t		lookup(int type, const SWString &value, SWArray<AMAddress> &results);

private:
		AMConnection	*m_pAM;
		AMType			m_type;
		};


SW_DEFINE_EXCEPTION(AMException, SWException);

#endif // __addrmgmt_AMDatabase_h__
