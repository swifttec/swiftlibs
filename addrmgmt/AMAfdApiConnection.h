/**
*** @file		AMAfdApiConnection.h
*** @brief		Basic database access object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Defines the AMAfdApiConnection class
***
*** <b>Copyright 2008 SwiftTec. All rights reserved.</b>
**/
/****************************************************************************
** $Date: 2008/12/18 15:08:01 $
** $Author: simon $
** $Revision: 1.1 $
*****************************************************************************
** $Log: AMAfdApiConnection.h,v $
** Revision 1.1  2008/12/18 15:08:01  simon
** Initial version
**
****************************************************************************/


#ifndef __addrmgmt_AMAfdApiConnection_h__
#define __addrmgmt_AMAfdApiConnection_h__

#include <addrmgmt/AMConnection.h>

#include <swift/SWString.h>
#include <swift/SWInterlockedInt32.h>
#include <swift/SWLoadableModule.h>

// Disable warning about deprecated objects
#pragma warning(disable: 4995)
#pragma warning(disable: 4251)


class ADDRMGMT_DLL_EXPORT AMAfdApiConnection : public AMConnection
		{
public:
		AMAfdApiConnection();
		virtual ~AMAfdApiConnection();

		/// Open the databaseImportedVideo
		virtual sw_status_t		open(const SWArray<SWNamedValue> &va);

		/// Close the database
		virtual sw_status_t		close();

		/// Execute a string lookup
		virtual sw_status_t		lookup(int type, const SWString &value, SWArray<AMAddress> &results);

private:
		bool					startApi();
		bool					stopApi();

private:
		static SWLoadableModule		m_dll;
		static SWInterlockedInt32	m_initCount;
		};


#endif // __addrmgmt_AMAfdApiConnection_h__
