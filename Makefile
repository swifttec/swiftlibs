PROJROOT        = .

include $(PROJROOT)/Makefile.proj

# Generate the Makefile.defs every time
dummy := $(shell ../build/scripts/mkdefs `basename $(CURDIR)`.sln > Makefile.defs)

# Now include the generated file
include Makefile.defs

all: $(MAKEIN)
	$(SILENT)$(MAKEIN) -m $(MAKE) -f Makefile $(DIRS)

clean: $(MAKEIN)
	$(SILENT)$(MAKEIN) -m $(MAKE) -f Makefile -t clean $(DIRS)

cleandebug:
	@$(MAKE) BUILDTYPE=debug clean

debug:
	@$(MAKE) BUILDTYPE=debug
