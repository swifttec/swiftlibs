/**
*** A collection of internal functions
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"

#include <xplatform/sw_osfunc.h>

#if defined(linux)
#include <wctype.h>
#endif


#if defined(SW_PLATFORM_WINDOWS)
	#pragma warning(disable: 4996)
#endif


XPLATFORM_DLL_EXPORT size_t		
sw_mbstowcs(wchar_t *wcstr, const char *mbstr, size_t count, int codepage)
		{
		size_t	res;

#if defined(SW_PLATFORM_WINDOWS)
		int		r;

		if (*mbstr)
			{
			r = (int)MultiByteToWideChar(
				codepage,				// UINT CodePage,            // code page
				0,						// DWORD dwFlags,            // performance and mapping flags
				mbstr,					// LPSTR lpMultiByteStr,     // buffer for new string
				(int)strlen(mbstr),			// int cchWideChar,          // number of chars in string.
				wcstr,					// LPCWSTR lpWideCharStr,    // wide-character string
				wcstr==NULL?0:(int)count	// int cbMultiByte,          // size of buffer
				);

			if (r == 0) res = (size_t)-1;
			else res = (size_t)r;
			}
		else
			{
			r = 0;
			res = 0;
			}

		if (wcstr != NULL && r < (int)count)
			{
			// Termniate the string.
			wcstr[r] = 0;
			}
#else
		res = mbstowcs(wcstr, mbstr, count);
#endif
		
		return res;
		}


XPLATFORM_DLL_EXPORT size_t
sw_wcstombs(char *mbstr, const wchar_t *wcstr, size_t count, int codepage)
		{
#if defined(SW_PLATFORM_WINDOWS)
		size_t	res;
		int		r;

		if (*wcstr)
			{
			r = (int)WideCharToMultiByte(
				codepage,				// UINT CodePage,            // code page
				0,						// DWORD dwFlags,            // performance and mapping flags
				wcstr,					// LPCWSTR lpWideCharStr,    // wide-character string
				(int)wcslen(wcstr),			// int cchWideChar,          // number of chars in string.
				mbstr,					// LPSTR lpMultiByteStr,     // buffer for new string
				mbstr==NULL?0:(int)count,	// int cbMultiByte,          // size of buffer
				NULL,					// LPCSTR lpDefaultChar,     // default for unmappable chars
				NULL					// LPBOOL lpUsedDefaultChar  // set when default char used
				);

			if (r == 0) res = (size_t)-1;
			else res = (size_t)r;
			}
		else
			{
			r = 0;
			res = 0;
			}

		return res;
#else
		return wcstombs(mbstr, wcstr, count);
#endif
		}


#if defined(SW_PLATFORM_WINDOWS)
// Thread safe version of localtime
XPLATFORM_DLL_EXPORT struct tm *
localtime_r(time_t *clock, struct tm *tp)
		{
		struct tm   *tmp;

		tmp = localtime(clock);
		memcpy(tp, tmp, sizeof(*tmp));

		return tp;
		}


// Thread safe version of gmtime
XPLATFORM_DLL_EXPORT struct tm *
gmtime_r(time_t *clock, struct tm *tp)
		{
		struct tm   *tmp;

		tmp = gmtime(clock);
		memcpy(tp, tmp, sizeof(*tmp));

		return tp;
		}


/*
XPLATFORM_DLL_EXPORT sw_pid_t
getpid()
		{
		return (sw_pid_t)GetCurrentProcessId();
		}
*/
#endif



#if defined(WIN32) || defined(_WIN32) || defined(__hpux) || defined(linux) || defined(__MACH__)
// For those platforms where it is missing
XPLATFORM_DLL_EXPORT double
watof(const wchar_t *ws)
		{
		double	res=0.0;
		char	*cs=strCopyWString(ws, 0);

		if (cs != NULL)
			{
			res = atof(cs);
			free(cs);
			}

		return res;
		}
#endif


#if defined(__hpux) || defined(linux) || defined(__MACH__)
// For those platforms where it is missing
XPLATFORM_DLL_EXPORT int
watoi(const wchar_t *ws)
		{
		return (int)sw_string_convertWStringToULong(ws, 10, false);
		}
#endif


#if defined(linux) || defined(__MACH__)
// For those platforms where it is missing
XPLATFORM_DLL_EXPORT long
watol(const wchar_t *ws)
		{
		return (long)sw_string_convertWStringToULong(ws, 10, false);
		}
#endif


#if defined(__hpux)
// For those platforms where it is missing
XPLATFORM_DLL_EXPORT sw_int64_t
atoll(const char *s)
		{
		return (sw_int64_t)sw_string_convertStringToUInt64(ws, 10, false);
		}
#endif


#if defined(__hpux) || defined(linux) || defined(__MACH__)
// For those platforms where it is missing
XPLATFORM_DLL_EXPORT sw_int64_t
watoll(const wchar_t *ws)
		{
		return (sw_int64_t)sw_string_convertWStringToUInt64(ws, 10, false);
		}
#endif


#if !defined(WIN32) && !defined(_WIN32) && !defined (__SunOS_5_9)
// For those platforms where it is missing
XPLATFORM_DLL_EXPORT char *
strupr(char *s)
		{
		char	*p=s;

		while (*p)
			{
			*p = toupper(*p);
			p++;
			}

		return s;
		}
#endif


#if !defined(WIN32) && !defined(_WIN32) && !defined (__SunOS_5_9)
// For those platforms where it is missing
XPLATFORM_DLL_EXPORT char *
strlwr(char *s)
		{
		char	*p=s;

		while (*p)
			{
			*p = tolower(*p);
			p++;
			}

		return s;
		}
#endif

#if !defined(WIN32) && !defined(_WIN32) && !defined (__SunOS_5_9)
// For those platforms where it is missing
XPLATFORM_DLL_EXPORT wchar_t *
wcsupr(wchar_t *s)
		{
		wchar_t	*p=s;

		while (*p)
			{
			*p = towupper(*p);
			p++;
			}

		return s;
		}
#endif


#if !defined(WIN32) && !defined(_WIN32) && !defined (__SunOS_5_9)
// For those platforms where it is missing
XPLATFORM_DLL_EXPORT wchar_t *
wcslwr(wchar_t *s)
		{
		wchar_t	*p=s;

		while (*p)
			{
			*p = towlower(*p);
			p++;
			}

		return s;
		}
#endif

#if !defined(WIN32) && !defined(_WIN32)
// For those platforms where it is missing
XPLATFORM_DLL_EXPORT int
wcscasecmp(const wchar_t *s1, const wchar_t *s2)
		{
		wint_t	d;

		while (*s1 || *s2)
			{
			d = towlower(*s1) - towlower(*s2);
			if (d != 0) return d;
			s1++;
			s2++;
			}

		return 0;
		}
#endif

#if !defined(WIN32) && !defined(_WIN32)
// For those platforms where it is missing
XPLATFORM_DLL_EXPORT int
wcsncasecmp(const wchar_t *s1, const wchar_t *s2, size_t n)
		{
		wint_t	d;

		while (n-- > 0 && (*s1 || *s2))
			{
			d = towlower(*s1) - towlower(*s2);
			if (d != 0) return d;
			s1++;
			s2++;
			}

		return 0;
		}
#endif

#if !defined(WIN32) && !defined(_WIN32)
// For those platforms where it is missing
XPLATFORM_DLL_EXPORT wchar_t *
wcsdup(const wchar_t *s)
		{
		wchar_t	*wp;

		wp = (wchar_t *)malloc((wcslen(s)+1) * sizeof(wchar_t));
		if (wp != NULL) wcscpy(wp, s);

		return wp;
		}
#endif



#if defined(WIN32) || defined(_WIN32)

// Number of days from 1/1/1601 to 1/1/1970 = 134774
// Number of seconds from 1/1/1601 to 1/1/1970 = 11644473600

// Take from knowledge base sample
XPLATFORM_DLL_EXPORT void
UnixTimeToFileTime(time_t t, LPFILETIME pft)
		{
		// Note that LONGLONG is a 64-bit value
		LONGLONG ll;

		ll = Int32x32To64(t, 10000000) + 116444736000000000;
		pft->dwLowDateTime = (DWORD)ll;
		pft->dwHighDateTime = (DWORD)(ll >> 32);
		}


XPLATFORM_DLL_EXPORT void
FileTimeToUnixTime(FILETIME *pft, time_t *ut)
		{
		// Note that LONGLONG is a 64-bit value
		sw_int64_t ll;

		ll = ((sw_int64_t)pft->dwHighDateTime << 32) | (sw_int64_t)pft->dwLowDateTime;
		ll -= 116444736000000000;
		ll /= 10000000;
		*ut = (time_t)ll;
		}

#endif




XPLATFORM_DLL_EXPORT char *
strCopyString(const char *s)
		{
		char		*sp;
		sw_size_t	nchars;

		nchars = strlen(s) + 1;
		sp = (char *)malloc(nchars * sizeof(char));
		if (sp != NULL) strcpy(sp, s);

		return sp;
		}


XPLATFORM_DLL_EXPORT char *
strCopyWString(const wchar_t *s, int codepage)
		{
		sw_size_t	slen, nlen;
		char		*sp;
		
		slen = wcslen(s);
		nlen = sw_wcstombs(NULL, s, slen+1, codepage);

		if (nlen != (sw_size_t)-1)
			{
			sp = (char *)malloc((nlen+1) * sizeof(char));
			if (sp != NULL)
				{
				sw_wcstombs(sp, s, slen+1, codepage);
				sp[nlen] = 0;
				}
			}
		else
			{
			sp = (char *)malloc(sizeof(char));
			if (sp != NULL) *sp = 0;
			}

		return sp;
		}


XPLATFORM_DLL_EXPORT wchar_t *
wcsCopyString(const char *s, int codepage)
		{
		wchar_t		*sp;
		sw_size_t	nchars;

		nchars = strlen(s) + 1;
		sp = (wchar_t *)malloc(nchars * sizeof(wchar_t));
		if (sp != NULL)
			{
			int	nc;

			nc = (int)sw_mbstowcs(sp, s, nchars, codepage);
			if (nc < 0) nc = 0;
			sp[nc] = 0;
			}

		return sp;
		}


XPLATFORM_DLL_EXPORT wchar_t *
wcsCopyWString(const wchar_t *s)
		{
		wchar_t		*sp;
		sw_size_t	nchars;

		nchars = wcslen(s) + 1;
		sp = (wchar_t *)malloc(nchars * sizeof(wchar_t));
		if (sp != NULL) wcscpy(sp, s);

		return sp;
		}


#if defined(SW_PLATFORM_WINDOWS)
XPLATFORM_DLL_EXPORT unsigned int
sleep(unsigned int seconds)
		{
		Sleep(seconds * 1000);
		return 0;
		}

/*
XPLATFORM_DLL_EXPORT int
gettimeofday(struct timeval *tv, struct timezone *tz)
		{
		if (tv != NULL)
			{
			SYSTEMTIME	st;
			FILETIME	ft;
			sw_int64_t	t;

			GetSystemTime(&st);
			SystemTimeToFileTime(&st, &ft);

			t = (((sw_int64_t)ft.dwHighDateTime << 32) | (sw_int64_t)ft.dwLowDateTime) - 116444736000000000;
			tv->tv_sec = (int)(t / 10000000);
			tv->tv_usec = (int)((t % 10000000) / 10);
			}

		if (tz != NULL)
			{
			static int tzflag=0;

			if (!tzflag)
				{
				_tzset();
				tzflag++;
				}

			tz->tz_minuteswest = _timezone / 60;
			tz->tz_dsttime = _daylight;
			}

		return 0;
		}
*/
#endif


#if !defined(SW_PLATFORM_WINDOWS)
XPLATFORM_DLL_EXPORT char *
gets_s(char *buf, sw_size_t nchars)
		{
		char	*pBuf;

		pBuf = fgets(buf, nchars, stdin);

		if (pBuf != NULL)
			{
			int	slen=strlen(buf);

			if (slen > 0 && buf[slen-1] == '\n') buf[slen-1] = 0;
			}

		return pBuf;
		}
#endif
