/****************************************************************************
**	sw_process.h
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __xplatform_sw_process_h__
#define __xplatform_sw_process_h__

#include <xplatform/xplatform.h>

#ifdef __cplusplus
extern "C" {
#endif

XPLATFORM_DLL_EXPORT	sw_pid_t	sw_process_self();
XPLATFORM_DLL_EXPORT	sw_bool_t	sw_process_isrunning(sw_pid_t pid);
XPLATFORM_DLL_EXPORT	sw_bool_t	sw_process_isConsoleApp();
XPLATFORM_DLL_EXPORT	sw_bool_t	sw_process_getname(sw_pid_t pid, char *buf, int bufsize);

#ifdef __cplusplus
}
#endif

#endif // __xplatform_sw_process_h__
