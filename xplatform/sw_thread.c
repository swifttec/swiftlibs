/****************************************************************************
**	sw_thread.c		Define platform independent handles as used by this library
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"


#include "sw_internal_thread.h"

#include <xplatform/sw_status.h>

#if defined(SW_PLATFORM_WINDOWS)
	#pragma warning(disable: 4142)
#endif

#undef ENABLE_DEBUG

/**
***	 Return the ID of the currently running thread.
**/
XPLATFORM_DLL_EXPORT	sw_thread_id_t
sw_thread_self()
		{
#if defined(sun)
        return thr_self();
#elif defined(WIN32) || defined(_WIN32)
        return (sw_thread_id_t)GetCurrentThreadId();
#else
        return pthread_self();
#endif
		}


/**
***	Suspend the currently running thread for the specified number
*** of milliseconds.
**/
XPLATFORM_DLL_EXPORT	void
sw_thread_sleep(int ms)
		{
#if defined(ENABLE_DEBUG)
	printf("sw_thread_sleep(%d ms)\n", ms);
#endif

#ifdef sun
		// Solaris
		poll(0, 0, ms);
#elif defined(WIN32) || defined(_WIN32)
		// Windows
		Sleep(ms);
#else
		// Other Unix
		struct timeval	tv;

		tv.tv_sec = ms / 1000;
		tv.tv_usec = (ms % 1000) * 1000;

		select(0, 0, 0, 0, &tv);
#endif

#if defined(ENABLE_DEBUG)
	printf("sw_thread_sleep(%d ms) - ended\n", ms);
#endif
		}


static
#if defined(WIN32) || defined(_WIN32)
// Win32 threads
unsigned int


#if defined(_AFX_H_)
// MFC threads
__cdecl
#else // !defined(_AFXDLL) || defined(AFXAPI)
// Non-MFC threads
__stdcall
#endif // !defined(_AFXDLL) || defined(AFXAPI)

#else // defined(WIN32) || defined(_WIN32)
// Solaris and POSIX threads
void *
#endif // defined(WIN32) || defined(_WIN32)
sw_thread_internal_threadproc(void *param)
		{
#if defined(ENABLE_DEBUG)
	printf("thread %x started\n", param);
#endif

		SWInternalThread	*pThread=(SWInternalThread *)param;

		pThread->stopped = false;
		pThread->started = true;

#if defined(ENABLE_DEBUG)
	printf("thread %x tproc = %x\n", pThread, pThread->tproc);
#endif

		pThread->exitcode = pThread->tproc(pThread->tparam);
		pThread->stopped = true;

#if defined(ENABLE_DEBUG)
	printf("thread stopping%x\n", param);
#endif

#if defined(WIN32) || defined(_WIN32)
		return pThread->exitcode;
#else
		return (void *)(sw_intptr_t)(pThread->exitcode);
#endif
		}



XPLATFORM_DLL_EXPORT sw_status_t
sw_thread_start(sw_thread_t hThread)
		{
#if defined(ENABLE_DEBUG)
	printf("Starting thread %x\n", hThread);
#endif

		SWInternalThread	*pThread=(SWInternalThread *)hThread;
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (pThread != NULL && pThread->hdr.type == SW_HANDLE_TYPE_THREAD)
			{
			if (pThread->started && !pThread->stopped)
				{
				// The thread is already running - can't start it again
				// until is has stopped.
				return sw_status_setLastError(SW_STATUS_THREAD_ALREADY_RUNNING);
				}

			pThread->joined = false;

#if defined(WIN32) || defined(_WIN32)
	#if defined(_AFX_H_)

			#error MFC Mode not supported

			/*
			// The code required for MFC apps
			CWinThread  *tp;

			tp = AfxBeginThread(sw_threadproc_t, this);
			_tid = tp->m_nThreadID;
			_handle = tp->m_hThread;
			*/
	#else
			{
			// The code required for non-MFC apps
			unsigned int	id=0;

			if (pThread->handle != 0)
				{
				CloseHandle(pThread->handle);
				pThread->handle = 0;
				}

			pThread->handle = (HANDLE)_beginthreadex(NULL, 0, sw_thread_internal_threadproc, pThread, 0, &id);
			pThread->tid = id;
			}
	#endif
			if (pThread->handle == 0) res = sw_status_getSystemErrorStatus();
#elif defined(sun)
			{
			// Solaris Threads
			int	tflags=0;

			if ((pThread->flags & SW_THREAD_TYPE_DETACHED) != 0) tflags |= THR_DETACHED;
			if ((pThread->flags & SW_THREAD_TYPE_DAEMON) != 0) tflags |= THR_DAEMON;
			if ((pThread->flags & SW_THREAD_TYPE_BOUND) != 0) tflags |= THR_BOUND;
			if (thr_create(NULL, pThread->stacksize, sw_thread_internal_threadproc, pThread, tflags, &pThread->tid) < 0)
				{
				res = sw_status_getSystemErrorStatus();
				}
			}
#else
			{
			// POSIX Threads
			pthread_attr_init(&pThread->attr);
			if ((pThread->flags & (SW_THREAD_TYPE_DETACHED|SW_THREAD_TYPE_DAEMON)) != 0)
				{
				pthread_attr_setdetachstate(&pThread->attr, PTHREAD_CREATE_DETACHED);
				}
			else
				{
				pthread_attr_setdetachstate(&pThread->attr, PTHREAD_CREATE_JOINABLE);
				}

			pthread_attr_setscope(&pThread->attr, ((pThread->flags & SW_THREAD_TYPE_BOUND) != 0)?PTHREAD_SCOPE_SYSTEM:PTHREAD_SCOPE_PROCESS);

			int	r=pthread_create(&pThread->tid, &pThread->attr, sw_thread_internal_threadproc, pThread);

			if (r != 0)
				{
				res = sw_status_getSystemErrorStatus();
				}
			}
#endif
			}
		else res = SW_STATUS_INVALID_HANDLE;

#if defined(ENABLE_DEBUG)
	printf("sw_thread_start(%x) returning %d\n", hThread, res);
#endif
		return sw_status_setLastError(res);
		}


/**
***	Create a thread.
***
*** This function allows the create of a thread and provides the ability to set
*** various thread paramaters used in creating the thread.
***
*** @param	tproc		Specifies the function to be called when the thread starts.
*** @param	tparam		Specifies a parameter to be passed to the tproc function.
*** @param	flags		Specifies the flags to be used when creating the thread.
*** @param	stacksize	Specifies the the size of stack to use for the thread.
***						If the stacksize is zero the default stacksize for the system
***						is used.
***
*** @return				If successful this function returns a handle to a thread. 
***						If not sucessful a SW_INVALID_HANDLE is returned.
**/
XPLATFORM_DLL_EXPORT sw_thread_t
sw_thread_create_ex(sw_threadproc_t tproc, void *tparam, int flags, int stacksize)
		{
		SWInternalThread	*pThread;

#if defined(ENABLE_DEBUG)
	printf("sizeof(SWInternalThread) = %d\n", sizeof(SWInternalThread));
	printf("sizeof(sw_thread_t) = %d\n", sizeof(sw_thread_t));
	printf("tproc = %x\n", tproc);
	printf("tparam = %x\n", tparam);
	printf("flags = %x\n", flags);
	printf("stacksize = %d\n", stacksize);
#endif

		pThread = (SWInternalThread *)malloc(sizeof(SWInternalThread));
		if (pThread != NULL)
			{
			memset(pThread, 0, sizeof(SWInternalThread));
			pThread->hdr.type = SW_HANDLE_TYPE_THREAD;
			pThread->hdr.size = sizeof(SWInternalThread);
			pThread->flags = flags;
			pThread->stacksize = stacksize;
			pThread->tproc = tproc;
			pThread->tparam = tparam;

			// Start the thread unless told not to.
			if ((flags & SW_THREAD_TYPE_NOSTART) == 0)
				{
				if (sw_thread_start(pThread) != SW_STATUS_SUCCESS)
					{
					free(pThread);
					pThread = NULL;
					}
				}
			}
		else sw_status_setLastError(SW_STATUS_OUT_OF_MEMORY);

#if defined(ENABLE_DEBUG)
	printf("sw_thread_create_ex returning %x\n", pThread);
#endif

		return pThread;
		}


/**
*** Simple version of sw_thread_create where all the
*** type, stack and other info are done by default.
**/
XPLATFORM_DLL_EXPORT sw_thread_t
sw_thread_create(sw_threadproc_t tproc, void *param)
		{
		return sw_thread_create_ex(tproc, param, 0, 0);
		}


XPLATFORM_DLL_EXPORT	sw_status_t
sw_thread_destroy(sw_thread_t hThread)
		{
		SWInternalThread	*pThread=(SWInternalThread *)hThread;
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (pThread != NULL && pThread->hdr.type == SW_HANDLE_TYPE_THREAD)
			{
			res = sw_thread_waitUntilStopped(pThread);

#ifdef sun
			// Solaris Threads
#elif defined(WIN32) || defined(_WIN32)
			// Windows Threads
			CloseHandle(pThread->handle);
#else
			// POSIX Threads
			pthread_attr_destroy(&pThread->attr);
#endif

			memset(pThread, 0, sizeof(SWInternalThread));
			free(pThread);
			res = SW_STATUS_SUCCESS;
			}
		else res = SW_STATUS_INVALID_HANDLE;

		return res;
		}


XPLATFORM_DLL_EXPORT	sw_status_t
sw_thread_waitUntilStopped(sw_thread_t hThread)
		{
		SWInternalThread	*pThread=(SWInternalThread *)hThread;
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (pThread != NULL && pThread->hdr.type == SW_HANDLE_TYPE_THREAD)
			{
			// We must not leave here until the thread is stopped
			if (pThread->tid > 0 && (!pThread->stopped || !pThread->joined))
				{
				int		r;

				if ((pThread->flags & (SW_THREAD_TYPE_DETACHED|SW_THREAD_TYPE_DAEMON)) != 0)
					{
					// We cannot do a join of detached threads,
					// so we must wait until they die
					while (pThread->tid > 0 && !pThread->stopped)
						{
						sw_thread_yield();
						}
					}
				else
					{
#ifdef sun
					// Solaris Threads
					void	*exitstatus;

					while ((r = thr_join(pThread->tid, 0, &exitstatus)) != 0 && r == EDEADLK)
#elif defined(WIN32) || defined(_WIN32)
					// Windows Threads
					DWORD	exitstatus;

					while ((r = WaitForSingleObject(pThread->handle, INFINITE)) != 0 && r != WAIT_ABANDONED)
#else
					// POSIX Threads
					void	*exitstatus;

					while ((r = pthread_join(pThread->tid, &exitstatus)) != 0 && r == EDEADLK)
#endif
						{
						sw_thread_yield();
						}

#if defined(WIN32) || defined(_WIN32)
					GetExitCodeThread(pThread->handle, &exitstatus);
#endif
					// Save the exitstatus
					pThread->exitcode = (int)(sw_intptr_t)exitstatus;

#if defined(ENABLE_DEBUG)
					printf("exitcode = %d\n", pThread->exitcode);
#endif
					}

				res = SW_STATUS_SUCCESS;
				pThread->joined = true;
				}
			else res = SW_STATUS_SUCCESS;
			}
		else res = SW_STATUS_INVALID_HANDLE;

		return sw_status_setLastError(res);
		}


XPLATFORM_DLL_EXPORT	sw_status_t
sw_thread_yield()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

#ifdef sun
		// Solaris
		thr_yield();
#elif defined(WIN32) || defined(_WIN32)
		// Windows
		BOOL	b=FALSE;

	#if _WIN32_WINNT >= 0x0500
		b = SwitchToThread();
	#endif
		// if (!res) res = SW_STATUS_THREAD_CANNOT_YIELD;
		if (!b) Sleep(1);

#else
		// Other Unix
		struct timeval	tv;

		tv.tv_sec = 0;
		tv.tv_usec = 0;
		select(0, 0, 0, 0, &tv);
#endif

		return sw_status_setLastError(res);
		}


XPLATFORM_DLL_EXPORT sw_status_t
sw_thread_waitUntilStarted(sw_thread_t hThread)
		{
		SWInternalThread	*pThread=(SWInternalThread *)hThread;
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (pThread != NULL && pThread->hdr.type == SW_HANDLE_TYPE_THREAD)
			{
			// We must not leave here until the thread is started
			while (pThread->tid > 0 && !pThread->started)
				{
				sw_thread_yield();
				}

			if (!pThread->started) res = SW_STATUS_THREAD_NOT_STARTED;
			}
		else res = SW_STATUS_INVALID_HANDLE;

		return sw_status_setLastError(res);
		}


XPLATFORM_DLL_EXPORT	sw_status_t
sw_thread_getExitCode(sw_thread_t hThread, int *pExitCode)
		{
		SWInternalThread	*pThread=(SWInternalThread *)hThread;
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (pExitCode == NULL) res = SW_STATUS_NULL_POINTER;
		else if (pThread != NULL && pThread->hdr.type == SW_HANDLE_TYPE_THREAD)
			{
			if (pThread->stopped)
				{
				*pExitCode = pThread->exitcode;
				}
			else res = SW_STATUS_THREAD_NOT_STOPPED;
			}
		else res = SW_STATUS_INVALID_HANDLE;

		return sw_status_setLastError(res);
		}



XPLATFORM_DLL_EXPORT	sw_status_t
sw_thread_getThreadId(sw_thread_t hThread, sw_thread_id_t *pThreadId)
		{
		SWInternalThread	*pThread=(SWInternalThread *)hThread;
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (pThreadId == NULL) res = SW_STATUS_NULL_POINTER;
		else if (pThread != NULL && pThread->hdr.type == SW_HANDLE_TYPE_THREAD)
			{
			*pThreadId = pThread->tid;
			}
		else res = SW_STATUS_INVALID_HANDLE;

		return sw_status_setLastError(res);
		}



XPLATFORM_DLL_EXPORT	sw_status_t
sw_thread_suspend(sw_thread_t hThread)
		{
		SWInternalThread	*pThread=(SWInternalThread *)hThread;
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (pThread != NULL && pThread->hdr.type == SW_HANDLE_TYPE_THREAD)
			{
			if (pThread->tid > 0 && pThread->started)
				{
				if (pThread->suspendcount > 0)
					{
					pThread->suspendcount++;
					res = SW_STATUS_SUCCESS;
					}
				else
					{
#if defined(WIN32) || defined(_WIN32)
					if (SuspendThread(pThread->handle) >= 0) res = SW_STATUS_SUCCESS;
					else res = sw_status_getSystemErrorStatus();
#else
	#ifdef sun
					thr_suspend(pThread->tid);
					res = SW_STATUS_SUCCESS;
	#else
					// Not available - can we use pthread_kill?
					res = SW_STATUS_UNSUPPORTED_OPERATION;
	#endif
#endif

					if (res == SW_STATUS_SUCCESS)
						{
						pThread->suspendcount++;
						}
					}
				}
			}

		return sw_status_setLastError(res);
		}


XPLATFORM_DLL_EXPORT	sw_status_t
sw_thread_resume(sw_thread_t hThread)
		{
		SWInternalThread	*pThread=(SWInternalThread *)hThread;
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (pThread != NULL && pThread->hdr.type == SW_HANDLE_TYPE_THREAD)
			{
			if (pThread->tid > 0 && pThread->started)
				{
				if (pThread->suspendcount > 1)
					{
					pThread->suspendcount--;
					res = SW_STATUS_SUCCESS;
					}
				else if (pThread->suspendcount == 1)
					{
#if defined(WIN32) || defined(_WIN32)
					if (ResumeThread(pThread->handle) == 1) res = SW_STATUS_SUCCESS;
					else res = sw_status_getSystemErrorStatus();
#else
	#ifdef sun
					thr_continue(pThread->tid);
					res = SW_STATUS_SUCCESS;
	#else
					// Not available - can we use pthread_kill?
					res = SW_STATUS_UNSUPPORTED_OPERATION;
	#endif
#endif

					if (res == SW_STATUS_SUCCESS)
						{
						pThread->suspendcount--;
						}
					}
				}
			}
		else res = SW_STATUS_INVALID_HANDLE;

		return sw_status_setLastError(res);
		}


/**
***	Test to see if the thread has started
**/
XPLATFORM_DLL_EXPORT bool
sw_thread_started(sw_thread_t hThread)
		{
		SWInternalThread	*pThread=(SWInternalThread *)hThread;
		bool	res=false;

		if (pThread != NULL && pThread->hdr.type == SW_HANDLE_TYPE_THREAD)
			{
			res = pThread->started;
			}

		return res;
		}


/**
***	Test to see if the thread has stopped
**/
XPLATFORM_DLL_EXPORT bool
sw_thread_stopped(sw_thread_t hThread)
		{
		SWInternalThread	*pThread=(SWInternalThread *)hThread;
		bool	res=false;

		if (pThread != NULL && pThread->hdr.type == SW_HANDLE_TYPE_THREAD)
			{
			res = pThread->stopped;
			}

		return res;
		}


/**
***	Test to see if the thread is running
**/
XPLATFORM_DLL_EXPORT	bool
sw_thread_running(sw_thread_t hThread)
		{
		SWInternalThread	*pThread=(SWInternalThread *)hThread;
		bool	res=false;

		if (pThread != NULL && pThread->hdr.type == SW_HANDLE_TYPE_THREAD)
			{
			res = (pThread->tid > 0 && !(pThread->stopped));
			}

		return res;
		}


/**
*** Sets the relative priority for this thread.
***
*** The thread priority is set as an integer value from SW_THREAD_PRIORITY_MIN
*** to SW_THREAD_PRIORITY_MAX. Threads are created with a priority of
*** SW_THREAD_PRIORITY_NORMAL. Negative values indicate a lower than normal
*** priority and postive values indicate a higher than normal priority. Zero
*** is "normal" priority.
***
*** Since each O/S attaches a different meaning to thread priority values the
*** thread priority is set on each platform in the best manner possible.
**/
XPLATFORM_DLL_EXPORT	sw_status_t
sw_thread_setPriority(sw_thread_t hThread, int priority)
		{
		SWInternalThread	*pThread=(SWInternalThread *)hThread;
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (priority < SW_THREAD_PRIORITY_MIN) priority = SW_THREAD_PRIORITY_MIN;
		if (priority > SW_THREAD_PRIORITY_MAX) priority = SW_THREAD_PRIORITY_MAX;

		if (pThread != NULL && pThread->hdr.type == SW_HANDLE_TYPE_THREAD)
			{
#if !defined(WIN32) && !defined(_WIN32)
	#ifdef sun
			{ // Solaris threads
			int		p;

			// Solaris threads run from 0-127, and it seems that 0 is the default value,
			p = priority;
			if (p < 0) p = 0;

			thr_setprio(pThread->tid, p);
			} // Solaris threads
	#else
			{ // POSIX threads
			// Currently not supported.
			} // POSIX threads
	#endif
#else
			{ // Windows threads
			int		p=THREAD_PRIORITY_NORMAL;
			int		nLimit=SW_THREAD_PRIORITY_MAX/3;
			int		aLimit=nLimit*2;

			if (priority < -aLimit) p = THREAD_PRIORITY_LOWEST;
			else if (priority <= -nLimit && priority > -aLimit) p = THREAD_PRIORITY_BELOW_NORMAL;
			else if (priority > aLimit) p = THREAD_PRIORITY_HIGHEST;
			else if (priority >= nLimit && priority < aLimit) p = THREAD_PRIORITY_ABOVE_NORMAL;
			else p = THREAD_PRIORITY_NORMAL;

			if (SetThreadPriority(pThread->handle, p))
				{
				res = sw_status_getSystemErrorStatus();
				}
			} // Windows threads
#endif
			}
		else res = SW_STATUS_INVALID_HANDLE;

		return sw_status_setLastError(res);
		}


XPLATFORM_DLL_EXPORT	sw_status_t
sw_thread_getPriority(sw_thread_t hThread, int *priority)
		{
		SWInternalThread	*pThread=(SWInternalThread *)hThread;
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (priority == NULL) res = SW_STATUS_NULL_POINTER;
		else if (pThread != NULL && pThread->hdr.type == SW_HANDLE_TYPE_THREAD)
			{
#if !defined(WIN32) && !defined(_WIN32)
	#ifdef sun
			// Solaris threads
			thr_getprio(pThread->tid, priority);
	#else
			// POSIX threads
			*priority = 0;
	#endif
#else
			// Windows threads
			{ // Windows threads
			int		p=THREAD_PRIORITY_NORMAL;
			int		nLimit=SW_THREAD_PRIORITY_MAX/3;
			int		aLimit=nLimit*2;

			if ((p = GetThreadPriority(pThread->handle)) == THREAD_PRIORITY_ERROR_RETURN)
				{
				res = sw_status_getSystemErrorStatus();
				}
			else
				{
				switch (p)
					{
					case THREAD_PRIORITY_LOWEST:		*priority = -nLimit * 2;	break;
					case THREAD_PRIORITY_BELOW_NORMAL:	*priority = -nLimit;		break;
					case THREAD_PRIORITY_ABOVE_NORMAL:	*priority = nLimit;			break;
					case THREAD_PRIORITY_HIGHEST:		*priority = nLimit * 2;		break;

					case THREAD_PRIORITY_NORMAL:
					default:
						*priority = 0;
						break;
					}
				}
			} // Windows threads
#endif
			}
		else res = SW_STATUS_INVALID_HANDLE;

		return sw_status_setLastError(res);
		}



XPLATFORM_DLL_EXPORT	void
sw_thread_setConcurrency(int level)
		{
#if !defined(WIN32) && !defined(_WIN32)
	#ifdef sun
		// Solaris threads
		thr_setconcurrency(level);
	#else
		// POSIX threads
		pthread_setconcurrency(level);
	#endif
#else
		// Not supported in WIN32
#endif
		}


XPLATFORM_DLL_EXPORT	int
sw_thread_getConcurrency()
		{
		int		level=0;

#if !defined(WIN32) && !defined(_WIN32)
	#ifdef sun
		// Solaris threads
		level = thr_getconcurrency();
	#else
		// POSIX threads
		level = pthread_getconcurrency();
	#endif
#else
		// Not supported in WIN32
#endif

		return level;
		}
