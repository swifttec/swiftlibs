/**
*** @file		sw_math.h
*** @brief		A collection of math-related functions.
*** @version	1.0
*** @author		Simon Sparkes
***
*** A collection of math-related functions.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#ifndef __xplatform_sw_math_h__
#define __xplatform_sw_math_h__

#include <xplatform/xplatform.h>
#include <math.h>

#ifndef PI
	#define PI	3.14159265358979323846
#endif

#ifdef __cplusplus
extern "C" {
#endif

/// Return the maximum of the 2 given integer values
XPLATFORM_DLL_EXPORT int			sw_math_max(int a, int b);

/// Return the minimum of the 2 given integer values
XPLATFORM_DLL_EXPORT int			sw_math_min(int a, int b);


/// Return the maximum of the 2 given integer values
XPLATFORM_DLL_EXPORT sw_int64_t		sw_math_maxInt64(sw_int64_t a, sw_int64_t b);

/// Return the minimum of the 2 given integer values
XPLATFORM_DLL_EXPORT sw_int64_t		sw_math_minInt64(sw_int64_t a, sw_int64_t b);


/// Returns the sine of the given angle in degrees
XPLATFORM_DLL_EXPORT double			sw_math_sin(double angle);

/// Returns the cosine of the given angle in degrees
XPLATFORM_DLL_EXPORT double			sw_math_cos(double angle);

/// Returns the tangent of the given angle in degrees
XPLATFORM_DLL_EXPORT double			sw_math_tan(double angle);

/// Returns the arc-tangent angle in degrees of the given value
XPLATFORM_DLL_EXPORT double			sw_math_atan(double v);

/// Returns the arc-tangent angle in degrees of the given value
XPLATFORM_DLL_EXPORT double			sw_math_atan2(double y, double x);

/// Returns the square root of the given number
XPLATFORM_DLL_EXPORT double			sw_math_sqrt(double v);

/**
*** @brief	Produce a signed 8-bit random number
**/
XPLATFORM_DLL_EXPORT sw_int8_t		sw_math_randomInt8();


/**
*** @brief	Produce a unsigned 8-bit random number
**/
XPLATFORM_DLL_EXPORT sw_uint8_t		sw_math_randomUInt8();


/**
*** @brief	Produce a signed 16-bit random number
**/
XPLATFORM_DLL_EXPORT sw_int16_t		sw_math_randomInt16();


/**
*** @brief	Produce a unsigned 16-bit random number
**/
XPLATFORM_DLL_EXPORT sw_uint16_t	sw_math_randomUInt16();


/**
*** @brief	Produce a signed 32-bit random number
**/
XPLATFORM_DLL_EXPORT sw_int32_t		sw_math_randomInt32();


/**
*** @brief	Produce a unsigned 32-bit random number
**/
XPLATFORM_DLL_EXPORT sw_uint32_t	sw_math_randomUInt32();


/**
*** @brief	Produce a signed 64-bit random number
**/
XPLATFORM_DLL_EXPORT sw_int64_t		sw_math_randomInt64();


/**
*** @brief	Produce a unsigned 64-bit random number
**/
XPLATFORM_DLL_EXPORT sw_uint64_t	sw_math_randomUInt64();


/**
*** @brief	Produce a random number in the range given
**/
XPLATFORM_DLL_EXPORT double			sw_math_randomDoubleInRange(double lowest, double highest);


/**
*** @brief	Produce a random number in the range given
**/
XPLATFORM_DLL_EXPORT int			sw_math_randomIntegerInRange(int lowest, int highest);


XPLATFORM_DLL_EXPORT sw_int8_t		sw_math_randomInt8InRange(sw_int8_t lowest, sw_int8_t highest);
XPLATFORM_DLL_EXPORT sw_uint8_t		sw_math_randomUInt8InRange(sw_uint8_t lowest, sw_uint8_t highest);

XPLATFORM_DLL_EXPORT sw_int32_t		sw_math_randomInt32InRange(sw_int32_t lowest, sw_int32_t highest);
XPLATFORM_DLL_EXPORT sw_uint32_t	sw_math_randomUInt32InRange(sw_uint32_t lowest, sw_uint32_t highest);

XPLATFORM_DLL_EXPORT sw_int16_t		sw_math_randomInt16InRange(sw_int16_t lowest, sw_int16_t highest);
XPLATFORM_DLL_EXPORT sw_uint16_t	sw_math_randomUInt16InRange(sw_uint16_t lowest, sw_uint16_t highest);

XPLATFORM_DLL_EXPORT sw_int64_t		sw_math_randomInt64InRange(sw_int64_t lowest, sw_int64_t highest);
XPLATFORM_DLL_EXPORT sw_uint64_t	sw_math_randomUInt64InRange(sw_uint64_t lowest, sw_uint64_t highest);


XPLATFORM_DLL_EXPORT sw_uint64_t	sw_math_bitsToUnsignedInteger(sw_uint64_t v, int firstbit, int nbits);
XPLATFORM_DLL_EXPORT sw_int64_t		sw_math_bitsToSignedInteger(sw_uint64_t v, int firstbit, int nbits);

#ifndef max
	#define max(a,b)	(((a) > (b)) ? (a) : (b))
#endif

#ifndef min
	#define min(a,b)	(((a) < (b)) ? (a) : (b))
#endif

#ifdef __cplusplus
}
#endif

#endif
