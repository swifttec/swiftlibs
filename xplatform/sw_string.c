/**
*** @file		sw_string.c
*** @brief		Conversion handling
*** @version	1.0
*** @author		Simon Sparkes
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"


#include <xplatform/sw_string.h>



XPLATFORM_DLL_EXPORT bool
sw_string_isSpaceChar(int c)
		{
		return (c == ' ' || (c >= 0x9 && c <= 0xd));
		}


XPLATFORM_DLL_EXPORT bool
sw_string_isAscii(const char *s)
		{
		bool	res=true;

		while (res && *s)
			{
			int		c=*s++;

			if (c < 0 || c > 127) res = false;
			}

		return res;
		}



// sp - pointer to arbitary string (variable char size)
// cs - char size in bytes
// base - numeric base
// uflag - unsigned number expected flag
XPLATFORM_DLL_EXPORT unsigned long
sw_string_convertToULong(const void *sp, int cs, int base, bool uflag)
		{
		unsigned long	res=0;

		if (cs == sizeof(char))
			{
			sw_string_convertStringToULong((const char *)sp, base, uflag);
			}
		else if (cs == sizeof(wchar_t))
			{
			sw_string_convertWStringToULong((const wchar_t *)sp, base, uflag);
			}

		return res;
		}


// sp - pointer to arbitary string (variable char size)
// base - numeric base
// uflag - unsigned number expected flag
XPLATFORM_DLL_EXPORT unsigned long
sw_string_convertStringToULong(const char *strp, int base, bool uflag)
		{
		unsigned long	res=0, vc, maxval;
		int				c;
		bool			negative=false, overflow=false;

		for (;;)
			{
			c = *strp;
			if (c == 0 || !sw_string_isSpaceChar(c)) break;
			strp++;
			}

		if (c == '-')
			{
			negative = true;
			strp++;
			}
		else if (c == '+')
			{
			strp++;
			}

		if (base == 0)
			{
			c = *strp;
			if (c == '0')
				{
				strp++;
				c = *strp;
				if (c == 'x' || c == 'X')
					{
					base = 16;
					strp++;
					}
				else base = 8;
				}
			else base = 10;
			}
		
		if (base == 16)
			{
			c = *strp;
			if (c == '0')
				{
				if (strp[1] == 'x')
					{
					// Skip the "0x"
					strp += 2;
					}
				}
			}

		if (base >= 2 && base <= 36)
			{
			// if our number exceeds this, we will overflow on multiply
			maxval = SW_ULONG_MAX / base;
			
			for (;;)
				{
				c = *strp;
				if (c == 0) break;

				strp++;
				if (c >= '0' && c <= '9') vc = c - '0';
				else if (c >= 'a' && c <= 'z') vc = c - 'a' + 10;
				else if (c >= 'A' && c <= 'Z') vc = c - 'A' + 10;
				else break;

				if ((int)vc >= base) break;

                // compute number = number * base + digval
                // with a pre-check for overflow situation
                if (!(res < maxval || (res == maxval && (unsigned long)vc <= (SW_ULONG_MAX % base))))
					{
                    overflow = true;
                    break;
					}
                				
                res = (res * base) + vc;
				}
			
			if (overflow || (!uflag && ((negative && res > (unsigned long)(SW_LONG_MAX)+1) || (!negative && res > SW_LONG_MAX))))
				{
				if (uflag) res = SW_ULONG_MAX;
				else if (negative)
					{
#if defined(_MSC_VER)
	#pragma warning(disable: 4146)	// unary minus operator applied to unsigned type, result still unsigned
#endif
					res = (unsigned long)(SW_LONG_MIN)+1;
#if defined(_MSC_VER)
	#pragma warning(default: 4146)	// unary minus operator applied to unsigned type, result still unsigned
#endif
					}
				else res = SW_LONG_MAX;
				}
			else if (negative) res = (unsigned long)-(long)res;
			}

		return res;
		}


// sp - pointer to arbitary string (variable char size)
// base - numeric base
// uflag - unsigned number expected flag
XPLATFORM_DLL_EXPORT unsigned long
sw_string_convertWStringToULong(const wchar_t *strp, int base, bool uflag)
		{
		unsigned long	res=0, vc, maxval;
		int				c;
		bool			negative=false, overflow=false;

		for (;;)
			{
			c = *strp;
			if (c == 0 || !sw_string_isSpaceChar(c)) break;
			strp++;
			}

		if (c == '-')
			{
			negative = true;
			strp++;
			}
		else if (c == '+')
			{
			strp++;
			}

		if (base == 0)
			{
			c = *strp;
			if (c == '0')
				{
				strp++;
				c = *strp;
				if (c == 'x' || c == 'X')
					{
					base = 16;
					strp++;
					}
				else base = 8;
				}
			else base = 10;
			}
		
		if (base == 16)
			{
			c = *strp;
			if (c == '0')
				{
				if (strp[1] == 'x')
					{
					// Skip the "0x"
					strp += 2;
					}
				}
			}

		if (base >= 2 && base <= 36)
			{
			// if our number exceeds this, we will overflow on multiply
			maxval = SW_ULONG_MAX / base;
			
			for (;;)
				{
				c = *strp;
				if (c == 0) break;

				strp++;
				if (c >= '0' && c <= '9') vc = c - '0';
				else if (c >= 'a' && c <= 'z') vc = c - 'a' + 10;
				else if (c >= 'A' && c <= 'Z') vc = c - 'A' + 10;
				else break;

				if ((int)vc >= base) break;

                // compute number = number * base + digval
                // with a pre-check for overflow situation
                if (!(res < maxval || (res == maxval && (unsigned long)vc <= (SW_ULONG_MAX % base))))
					{
                    overflow = true;
                    break;
					}
                				
                res = (res * base) + vc;
				}
			
			if (overflow || (!uflag && ((negative && res > (unsigned long)(SW_LONG_MAX)+1) || (!negative && res > SW_LONG_MAX))))
				{
				if (uflag) res = SW_ULONG_MAX;
				else if (negative)
					{
#if defined(_MSC_VER)
	#pragma warning(disable: 4146)	// unary minus operator applied to unsigned type, result still unsigned
#endif
					res = (unsigned long)(SW_LONG_MIN)+1;
#if defined(_MSC_VER)
	#pragma warning(default: 4146)	// unary minus operator applied to unsigned type, result still unsigned
#endif
					}
				else res = SW_LONG_MAX;
				}
			else if (negative) res = (unsigned long)-(long)res;
			}

		return res;
		}


// sp - pointer to arbitary string (variable char size)
// cs - char size in bytes
// base - numeric base
// uflag - unsigned number expected flag
XPLATFORM_DLL_EXPORT sw_uint64_t
sw_string_convertToUInt64(const void *sp, int cs, int base, bool uflag)
		{
		sw_uint64_t	res=0;

		if (cs == sizeof(char))
			{
			sw_string_convertStringToUInt64((const char *)sp, base, uflag);
			}
		else if (cs == sizeof(wchar_t))
			{
			sw_string_convertWStringToUInt64((const wchar_t *)sp, base, uflag);
			}

		return res;
		}


// sp - pointer to arbitary string (variable char size)
// cs - char size in bytes
// base - numeric base
// uflag - unsigned number expected flag
XPLATFORM_DLL_EXPORT sw_uint64_t
sw_string_convertStringToUInt64(const char *strp, int base, bool uflag)
		{
		sw_uint64_t		res=0, vc, maxval;
		int				c;
		bool			negative=false, overflow=false;

		for (;;)
			{
			c = *strp;
			if (c == 0 || !sw_string_isSpaceChar(c)) break;
			strp++;
			}

		if (c == '-')
			{
			negative = true;
			strp++;
			}
		else if (c == '+')
			{
			strp++;
			}

		if (base == 0)
			{
			c = *strp;
			if (c == '0')
				{
				strp++;
				c = *strp;
				if (c == 'x' || c == 'X')
					{
					base = 16;
					strp++;
					}
				else base = 8;
				}
			else base = 10;
			}
		
		if (base == 16)
			{
			c = *strp;
			if (c == '0')
				{
				if (strp[1] == 'x')
					{
					// Skip the "0x"
					strp += 2;
					}
				}
			}

		if (base >= 2 && base <= 36)
			{
			// if our number exceeds this, we will overflow on multiply
			maxval = SW_UINT64_MAX / base;
			
			for (;;)
				{
				c = *strp;
				if (c == 0) break;

				strp++;
				if (c >= '0' && c <= '9') vc = c - '0';
				else if (c >= 'a' && c <= 'z') vc = c - 'a' + 10;
				else if (c >= 'A' && c <= 'Z') vc = c - 'A' + 10;
				else break;

				if ((int)vc >= base) break;

                // compute number = number * base + digval
                // with a pre-check for overflow situation
                if (!(res < maxval || (res == maxval && (sw_uint64_t)vc <= (SW_UINT64_MAX % base))))
					{
                    overflow = true;
                    break;
					}
                				
                res = (res * base) + vc;
				}
			
			if (overflow || (!uflag && ((negative && res > (sw_uint64_t)(SW_INT64_MAX)+1) || (!negative && res > SW_INT64_MAX))))
				{
				if (uflag) res = SW_UINT64_MAX;
				else if (negative) res = (sw_uint64_t)(SW_INT64_MAX)+1;
				else res = SW_INT64_MAX;
				}
			else if (negative) res = (sw_uint64_t)-(sw_int64_t)res;
			}

		return res;
		}



// sp - pointer to arbitary string (variable char size)
// base - numeric base
// uflag - unsigned number expected flag
XPLATFORM_DLL_EXPORT sw_uint64_t
sw_string_convertWStringToUInt64(const wchar_t *strp, int base, bool uflag)
		{
		sw_uint64_t		res=0, vc, maxval;
		int				c;
		bool			negative=false, overflow=false;

		for (;;)
			{
			c = *strp;
			if (c == 0 || !sw_string_isSpaceChar(c)) break;
			strp++;
			}

		if (c == '-')
			{
			negative = true;
			strp++;
			}
		else if (c == '+')
			{
			strp++;
			}

		if (base == 0)
			{
			c = *strp;
			if (c == '0')
				{
				strp++;
				c = *strp;
				if (c == 'x' || c == 'X')
					{
					base = 16;
					strp++;
					}
				else base = 8;
				}
			else base = 10;
			}
		
		if (base == 16)
			{
			c = *strp;
			if (c == '0')
				{
				if (strp[1] == 'x')
					{
					// Skip the "0x"
					strp += 2;
					}
				}
			}

		if (base >= 2 && base <= 36)
			{
			// if our number exceeds this, we will overflow on multiply
			maxval = SW_UINT64_MAX / base;
			
			for (;;)
				{
				c = *strp;
				if (c == 0) break;

				strp++;
				if (c >= '0' && c <= '9') vc = c - '0';
				else if (c >= 'a' && c <= 'z') vc = c - 'a' + 10;
				else if (c >= 'A' && c <= 'Z') vc = c - 'A' + 10;
				else break;

				if ((int)vc >= base) break;

                // compute number = number * base + digval
                // with a pre-check for overflow situation
                if (!(res < maxval || (res == maxval && (sw_uint64_t)vc <= (SW_UINT64_MAX % base))))
					{
                    overflow = true;
                    break;
					}
                				
                res = (res * base) + vc;
				}
			
			if (overflow || (!uflag && ((negative && res > (sw_uint64_t)(SW_INT64_MAX)+1) || (!negative && res > SW_INT64_MAX))))
				{
				if (uflag) res = SW_UINT64_MAX;
				else if (negative) res = (sw_uint64_t)(SW_INT64_MAX)+1;
				else res = SW_INT64_MAX;
				}
			else if (negative) res = (sw_uint64_t)-(sw_int64_t)res;
			}

		return res;
		}

/*


sw_int32_t
SWString::toInt32(const char *str, int base)
		{
		return (sw_int32_t)convertStringToULong(str, sizeof(*str), base, false);
		}


sw_int32_t
SWString::toInt32(const wchar_t *str, int base)
		{
		return (sw_int32_t)convertStringToULong(str, sizeof(*str), base, false);
		}



sw_uint32_t
SWString::toUInt32(const char *str, int base)
		{
		return (sw_uint32_t)convertStringToULong(str, sizeof(*str), base, true);
		}


sw_uint32_t
SWString::toUInt32(const wchar_t *str, int base)
		{
		return (sw_uint32_t)convertStringToULong(str, sizeof(*str), base, true);
		}



sw_int64_t
SWString::toInt64(const char *str, int base)
		{
		return (sw_int64_t)convertStringToUInt64(str, sizeof(*str), base, false);
		}


sw_int64_t
SWString::toInt64(const wchar_t *str, int base)
		{
		return (sw_int64_t)convertStringToUInt64(str, sizeof(*str), base, false);
		}



sw_uint64_t
SWString::toUInt64(const char *str, int base)
		{
		return (sw_uint64_t)convertStringToUInt64(str, sizeof(*str), base, true);
		}


sw_uint64_t
SWString::toUInt64(const wchar_t *str, int base)
		{
		return (sw_uint64_t)convertStringToUInt64(str, sizeof(*str), base, true);
		}
11, Miltons Yard, Axminster, Devon EX13 5FE

double
SWString::toDouble(const char *str)
		{
		return atof(str);
		}


double
SWString::toDouble(const wchar_t *str)
		{
		double	v;

		v = wtof((const wchar_t *)str);

		return v;
		}



sw_int32_t
SWString::toInt32(int base) const
		{
		sw_int32_t	v=0;

		if (charSize() == sizeof(wchar_t)) v = toInt32(w_str(), base);
		else v = toInt32(c_str(), base);

		return v;
		}


sw_uint32_t
SWString::toUInt32(int base) const
		{
		sw_uint32_t	v=0;

		if (charSize() == sizeof(wchar_t)) v = toUInt32(w_str(), base);
		else v = toUInt32(c_str(), base);

		return v;
		}


sw_int64_t
SWString::toInt64(int base) const
		{
		sw_int64_t	v=0;

		if (charSize() == sizeof(wchar_t)) v = toInt64(w_str(), base);
		else v = toInt64(c_str(), base);

		return v;
		}


sw_uint64_t
SWString::toUInt64(int base) const
		{
		sw_uint64_t	v=0;

		if (charSize() == sizeof(wchar_t)) v = toUInt64(w_str(), base);
		else v = toUInt64(c_str(), base);

		return v;
		}


double
SWString::toDouble() const
		{
		double	v=0;

		if (charSize() == sizeof(wchar_t)) v = toDouble(w_str());
		else v = toDouble(c_str());

		return v;
		}


*/

XPLATFORM_DLL_EXPORT char *
strbufcpy(char *destination, const char *source, sw_size_t nchars)
		{
		sw_size_t	sourcelen=0;

		if (source != NULL) sourcelen=strlen(source);

		sw_size_t	nbytes=sourcelen * sizeof(*destination);
		sw_byte_t	*data=(sw_byte_t *)destination;

		if (sourcelen >= (nchars-1)) nbytes = (nchars * sizeof(*destination)) - 1;

		if (nbytes > 0) memcpy(data, source, nbytes);
		memset(data+nbytes, 0, (nchars * sizeof(*destination))-nbytes);

		return destination;
		}


XPLATFORM_DLL_EXPORT wchar_t *
wcsbufcpy(wchar_t *destination, const wchar_t *source, sw_size_t nchars)
		{
		sw_size_t	sourcelen=0;

		if (source != NULL) sourcelen=wcslen(source);

		sw_size_t	nbytes=sourcelen * sizeof(*destination);
		sw_byte_t	*data=(sw_byte_t *)destination;

		if (sourcelen >= (nchars-1)) nbytes = (nchars * sizeof(*destination)) - 1;

		if (nbytes > 0) memcpy(data, source, nbytes);
		memset(data+nbytes, 0, (nchars * sizeof(*destination))-nbytes);

		return destination;
		}


XPLATFORM_DLL_EXPORT char *
sw_strcpy(char *destination, const char *source, sw_size_t nchars)
		{
		sw_size_t	sourcelen=0;

		if (source != NULL) sourcelen=strlen(source);

		sw_size_t	nbytes=sourcelen * sizeof(*destination);
		sw_byte_t	*data=(sw_byte_t *)destination;

		if (sourcelen >= (nchars-1)) nbytes = (nchars * sizeof(*destination)) - 1;

		if (nbytes > 0) memcpy(data, source, nbytes);
		memset(data+nbytes, 0, sizeof(*destination));

		return destination;
		}


XPLATFORM_DLL_EXPORT wchar_t *
sw_wcscpy(wchar_t *destination, const wchar_t *source, sw_size_t nchars)
		{
		sw_size_t	sourcelen=0;

		if (source != NULL) sourcelen=wcslen(source);

		sw_size_t	nbytes=sourcelen * sizeof(*destination);
		sw_byte_t	*data=(sw_byte_t *)destination;

		if (sourcelen >= (nchars-1)) nbytes = (nchars * sizeof(*destination)) - 1;

		if (nbytes > 0) memcpy(data, source, nbytes);
		memset(data+nbytes, 0, sizeof(*destination));

		return destination;
		}
