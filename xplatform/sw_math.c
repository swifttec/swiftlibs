/**
*** @file
*** @author		Simon Sparkes
***
*** A collection of math-related functions.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"

#include <xplatform/sw_math.h>
#include <xplatform/sw_time.h>
#include <xplatform/sw_thread.h>
#include <xplatform/sw_clock.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif


#if defined(SW_PLATFORM_WINDOWS)
__declspec(thread) static
#else 
static __thread
#endif
bool	seeded = false;		///< A flag used to indicate if the random numbers have been seeded.



// Return the maximum of the 2 given integer values
XPLATFORM_DLL_EXPORT int
sw_math_max(int a, int b)
		{
		return a>b?a:b;
		}



// Return the minimum of the 2 given integer values
XPLATFORM_DLL_EXPORT int
sw_math_min(int a, int b)
		{
		return a<b?a:b;
		}


// Return the maximum of the 2 given integer values
XPLATFORM_DLL_EXPORT sw_int64_t
sw_math_maxInt64(sw_int64_t a, sw_int64_t b)
		{
		return a>b?a:b;
		}



// Return the minimum of the 2 given integer values
XPLATFORM_DLL_EXPORT sw_int64_t
sw_math_minInt64(sw_int64_t a, sw_int64_t b)
		{
		return a<b?a:b;
		}

// Returns the sine of the given angle in degrees
XPLATFORM_DLL_EXPORT double
sw_math_sin(double angle)
		{
		return sin(angle * PI / 180.0);
		}


// Returns the cosine of the given angle in degrees
XPLATFORM_DLL_EXPORT double
sw_math_cos(double angle)
		{
		return cos(angle * PI / 180.0);
		}


// Returns the tangent of the given angle in degrees
XPLATFORM_DLL_EXPORT double
sw_math_tan(double angle)
		{
		return tan(angle * PI / 180.0);
		}


// Returns the arc-tangent angle in degrees of the given value
XPLATFORM_DLL_EXPORT double
sw_math_atan(double v)
		{
		return (atan(v) * 180.0 / PI);
		}


// Returns the arc-tangent angle in degrees of the given value
XPLATFORM_DLL_EXPORT double
sw_math_atan2(double y, double x)
		{
		return (atan2(y, x) * 180.0 / PI);
		}

// Returns the square root of the given number
XPLATFORM_DLL_EXPORT double
sw_math_sqrt(double v)
		{
		return sqrt(v);
		}

#if defined(SW_PLATFORM_WINDOWS)

#define RAND48_SEED_0   (0x330e)
#define RAND48_SEED_1 (0xabcd)
#define RAND48_SEED_2 (0x1234)
#define RAND48_MULT_0 (0xe66d)
#define RAND48_MULT_1 (0xdeec)
#define RAND48_MULT_2 (0x0005)
#define RAND48_ADD (0x000b)

unsigned short _rand48_seed[3] =
		{
		RAND48_SEED_0,
		RAND48_SEED_1,
		RAND48_SEED_2
		};

unsigned short _rand48_mult[3] =
		{
		RAND48_MULT_0,
		RAND48_MULT_1,
		RAND48_MULT_2
		};

unsigned short _rand48_add = RAND48_ADD;

void
_dorand48(unsigned short xseed[3])
		{
		unsigned long accu;
		unsigned short temp[2];
	
		accu = (unsigned long)_rand48_mult[0] * (unsigned long)xseed[0] +
		(unsigned long)_rand48_add;
		temp[0] = (unsigned short)accu;        /* lower 16 bits */
		accu >>= sizeof(unsigned short)* 8;
		accu += (unsigned long)_rand48_mult[0] * (unsigned long)xseed[1] +
		(unsigned long)_rand48_mult[1] * (unsigned long)xseed[0];
		temp[1] = (unsigned short)accu;        /* middle 16 bits */
		accu >>= sizeof(unsigned short)* 8;
		accu += _rand48_mult[0] * xseed[2] + _rand48_mult[1] * xseed[1] + _rand48_mult[2] * xseed[0];
		xseed[0] = temp[0];
		xseed[1] = temp[1];
		xseed[2] = (unsigned short)accu;
		}


double
erand48(unsigned short xseed[3])
		{
		_dorand48(xseed);
		return ldexp((double) xseed[0], -48) +
			ldexp((double) xseed[1], -32) +
			ldexp((double) xseed[2], -16);
		}

double
drand48()
		{
		return erand48(_rand48_seed);
		}

void
srand48(long seed)
		{
		_rand48_seed[0] = RAND48_SEED_0;
		_rand48_seed[1] = (unsigned short)seed;
		_rand48_seed[2] = (unsigned short)(seed >> 16);
		_rand48_mult[0] = RAND48_MULT_0;
		_rand48_mult[1] = RAND48_MULT_1;
		_rand48_mult[2] = RAND48_MULT_2;
		_rand48_add = RAND48_ADD;
		}

#endif



/**
*** @internal
*** @brief		Internal static method used to seed the random number generator(s).
**/
static void
sw_math_seedRandomNumbers()
		{
		// Seed the random number generator before continuing
		sw_timeval_t		tv;

		sw_gettimeofday(&tv);

		srand((unsigned int)(sw_int64_t)sw_clock_getCurrentTimeMS() + sw_thread_self());
		srand48((long)(sw_int64_t)sw_clock_getCurrentTimeMS() + sw_thread_self());

		seeded = true;
		}


/**
*** @brief	Produce a random number in the range given
**/
XPLATFORM_DLL_EXPORT double
sw_math_randomDoubleInRange(double lowest, double highest)
		{
		if (!seeded) sw_math_seedRandomNumbers();

		double		res;
		
		if (highest < lowest)
			{
			double	tmp;

			tmp = lowest;
			lowest = highest;
			highest = tmp;
			}

		double		range=highest-lowest;

		res = lowest + (drand48() * range);

		return res;
		}




#if defined(SW_PLATFORM_WINDOWS)
	#pragma warning(disable: 4244)
#endif

XPLATFORM_DLL_EXPORT sw_int64_t
sw_math_randomInt64InRange(sw_int64_t lowest, sw_int64_t highest)
		{
		double	r=sw_math_randomDoubleInRange(lowest, highest);

		if (r < lowest) r = lowest;
		if (r > highest) r = highest;

		return (sw_int64_t)r;
		}


XPLATFORM_DLL_EXPORT sw_uint64_t
sw_math_randomUInt64InRange(sw_uint64_t lowest, sw_uint64_t highest)
		{
		double	r=sw_math_randomDoubleInRange(lowest, highest);

		if (r < lowest) r = lowest;
		if (r > highest) r = highest;

		return (sw_uint64_t)r;
		}

#if defined(SW_PLATFORM_WINDOWS)
	#pragma warning(default: 4244)
#endif


XPLATFORM_DLL_EXPORT sw_int32_t
sw_math_randomInt32InRange(sw_int32_t lowest, sw_int32_t highest)
		{
		double	r=sw_math_randomDoubleInRange((double)lowest-1.0, (double)highest+1.0);

		if (r < lowest) r = lowest;
		if (r > highest) r = highest;

		return (sw_int32_t)r;
		}

XPLATFORM_DLL_EXPORT sw_int16_t
sw_math_randomInt16InRange(sw_int16_t lowest, sw_int16_t highest)
		{
		double	r=sw_math_randomDoubleInRange((double)lowest-1.0, (double)highest+1.0);

		if (r < lowest) r = lowest;
		if (r > highest) r = highest;

		return (sw_int16_t)r;
		}

XPLATFORM_DLL_EXPORT sw_int8_t
sw_math_randomInt8InRange(sw_int8_t lowest, sw_int8_t highest)
		{
		double	r=sw_math_randomDoubleInRange((double)lowest-1.0, (double)highest+1.0);

		if (r < lowest) r = lowest;
		if (r > highest) r = highest;

		return (sw_int8_t)r;
		}


XPLATFORM_DLL_EXPORT sw_uint32_t
sw_math_randomUInt32InRange(sw_uint32_t lowest, sw_uint32_t highest)
		{
		double	r=sw_math_randomDoubleInRange((double)lowest-1.0, (double)highest+1.0);

		if (r < lowest) r = lowest;
		if (r > highest) r = highest;

		return (sw_uint32_t)r;
		}

XPLATFORM_DLL_EXPORT sw_uint16_t
sw_math_randomUInt16InRange(sw_uint16_t lowest, sw_uint16_t highest)
		{
		double	r=sw_math_randomDoubleInRange((double)lowest-1.0, (double)highest+1.0);

		if (r < lowest) r = lowest;
		if (r > highest) r = highest;

		return (sw_uint16_t)r;
		}

XPLATFORM_DLL_EXPORT sw_uint8_t
sw_math_randomUInt8InRange(sw_uint8_t lowest, sw_uint8_t highest)
		{
		double	r=sw_math_randomDoubleInRange((double)lowest-1.0, (double)highest+1.0);

		if (r < lowest) r = lowest;
		if (r > highest) r = highest;

		return (sw_uint8_t)r;
		}


XPLATFORM_DLL_EXPORT int
sw_math_randomIntegerInRange(int lowest, int highest)
		{
		double	r=sw_math_randomDoubleInRange((double)lowest-1.0, (double)highest+1.0);

		if (r < lowest) r = lowest;
		if (r > highest) r = highest;

		return (int)r;
		}





// Produce a 8 bit random number
XPLATFORM_DLL_EXPORT sw_int8_t	
sw_math_randomInt8()
		{
		return sw_math_randomInt8InRange(SW_INT8_MIN, SW_INT8_MAX);
		}


// Produce a 8 bit random number
XPLATFORM_DLL_EXPORT sw_uint8_t	
sw_math_randomUInt8()
		{
		return sw_math_randomUInt8InRange(0, SW_UINT8_MAX);
		}


// Produce a 16 bit random number
XPLATFORM_DLL_EXPORT sw_int16_t	
sw_math_randomInt16()
		{
		return sw_math_randomInt16InRange(SW_INT16_MIN, SW_INT16_MAX);
		}


// Produce a 16 bit random number
XPLATFORM_DLL_EXPORT sw_uint16_t	
sw_math_randomUInt16()
		{
		return sw_math_randomUInt16InRange(0, SW_UINT16_MAX);
		}


// Produce a 32 bit random number
XPLATFORM_DLL_EXPORT sw_int32_t	
sw_math_randomInt32()
		{
		return sw_math_randomInt32InRange(SW_INT32_MIN, SW_INT32_MAX);
		}


// Produce a 32 bit random number
XPLATFORM_DLL_EXPORT sw_uint32_t	
sw_math_randomUInt32()
		{
		return sw_math_randomUInt32InRange(0, SW_UINT32_MAX);
		}


// Produce a 64 bit random number
XPLATFORM_DLL_EXPORT sw_int64_t	
sw_math_randomInt64()
		{
		return sw_math_randomInt64InRange(SW_INT64_MIN, SW_INT64_MAX);
		}


// Produce a 64 bit random number
XPLATFORM_DLL_EXPORT sw_uint64_t	
sw_math_randomUInt64()
		{
		return sw_math_randomUInt64InRange(0, SW_UINT64_MAX);
		}



XPLATFORM_DLL_EXPORT sw_uint64_t
sw_math_bitsToUnsignedInteger(sw_uint64_t v, int firstbit, int nbits)
		{
		sw_uint64_t	mask=~((sw_uint64_t)0xffffffffffffffff << nbits);

		v = (v >> firstbit) & mask;

		return v;
		}


XPLATFORM_DLL_EXPORT sw_int64_t
sw_math_bitsToSignedInteger(sw_uint64_t v, int firstbit, int nbits)
		{
		sw_uint64_t	valuemask=~((sw_uint64_t)0xffffffffffffffff << (nbits-1));
		sw_uint64_t	signmask=((sw_uint64_t)1 << (nbits-1));
		sw_int64_t	res=0;

		v = sw_math_bitsToUnsignedInteger(v, firstbit, nbits);

		res = (sw_int64_t)(v & valuemask);
		if ((v & signmask) != 0)
			{
			res = -res - 1;
			}

		return res;
		}
