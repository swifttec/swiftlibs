/****************************************************************************
**	sw_mutex.cpp	An abstract class defining the interface for all mutex objects
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"


#include <xplatform/sw_mutex.h>
#include <xplatform/sw_thread.h>
#include <xplatform/sw_status.h>

#if defined(SW_PLATFORM_WINDOWS)
	#pragma warning(disable: 4996)
#endif

typedef struct sw_mutex_internal
		{
		sw_handle_header_t	hdr;		///< The handle common header
		int					type;		///< The type of mutex
		char				*name;		///< The name of the mutex (process mutex only)
#if defined(WIN32) || defined(_WIN32)
		HANDLE				mutex;		///< A windows handle for the real mutex
#else
	#ifdef sun
		mutex_t				mutex;		///< The Solaris mutex
	#else
		pthread_mutex_t	 mutex;		///< The POSIX mutex
	#endif
#endif
		volatile sw_thread_id_t	owner;		///< The id of the current thread that is holding the lock
		volatile int			lockcount;	///< A count of the number of times this lock has been locked recursively
		}  SWInternalMutex;


#define IS_PROCESS_MUTEX(pMutex)	((pMutex->type & SW_MUTEX_TYPE_PROCESS) != 0)
#define IS_RECURSIVE_MUTEX(pMutex)	((pMutex->type & SW_MUTEX_TYPE_RECURSIVE) != 0)


XPLATFORM_DLL_EXPORT sw_mutex_t
sw_mutex_create(int type, const char *name)
		{
		SWInternalMutex	*pMutex;

		pMutex = (SWInternalMutex *)malloc(sizeof(SWInternalMutex));
		if (pMutex != NULL)
			{
			memset(pMutex, 0, sizeof(SWInternalMutex));
			pMutex->hdr.type = SW_HANDLE_TYPE_MUTEX;
			pMutex->hdr.size = sizeof(SWInternalMutex);
			pMutex->type = type;

			if (IS_PROCESS_MUTEX(pMutex))
				{
				if (name != NULL) pMutex->name = strdup(name);
				else
					{
					free(pMutex);
					pMutex = NULL;
					sw_status_setLastError(SW_STATUS_INVALID_PARAMETER);
					return NULL;
					}
				}
			else
				{
				pMutex->name = NULL;
				}

#if defined(WIN32) || defined(_WIN32)
			pMutex->mutex = CreateMutexA(NULL, FALSE, pMutex->name);
			if (pMutex->mutex == NULL)
				{
				free(pMutex);
				pMutex = NULL;
				sw_status_setLastError(GetLastError());
				}
#else
			int	r=0;

	#ifdef sun
			r = mutex_init(&pMutex->mutex, USYNC_THREAD, NULL); 
	#else // sun
			r = pthread_mutex_init(&pMutex->mutex, NULL); 
	#endif // sun
			if (r != 0)
				{
				free(pMutex);
				pMutex = NULL;
				sw_status_setLastError(sw_status_getSystemErrorStatusForCode(r));
				}
#endif // WIN32
			}
		else sw_status_setLastError(SW_STATUS_OUT_OF_MEMORY);

		return pMutex;
		}

XPLATFORM_DLL_EXPORT sw_status_t
sw_mutex_lock(sw_mutex_t hMutex)
		{
		SWInternalMutex		*pMutex=(SWInternalMutex *)hMutex;
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (pMutex != NULL && pMutex->hdr.type == SW_HANDLE_TYPE_MUTEX)
			{
			int	r;

			if (pMutex->owner == sw_thread_self())
				{
				if (IS_RECURSIVE_MUTEX(pMutex))
					{
					pMutex->lockcount++;
					
					return sw_status_setLastError(SW_STATUS_SUCCESS);
					}
				else
					{
					// We have recursively called a non-recursive mutex
					//sw_error_setLastError()
					
					return sw_status_setLastError(SW_STATUS_ALREADY_LOCKED);
					}
				}

#ifdef sun
			r = mutex_lock(&pMutex->mutex);
#elif defined(WIN32) || defined(_WIN32)
			r = WaitForSingleObject(pMutex->mutex, INFINITE);
			if (r == WAIT_ABANDONED) r = 0;
#else
			r = pthread_mutex_lock(&pMutex->mutex);
#endif

			if (r == 0)
				{
				pMutex->owner = sw_thread_self();
				pMutex->lockcount = 1;
				res = SW_STATUS_SUCCESS;
				}
			else
				{
				res = sw_status_getSystemErrorStatus();
				}
			}
		else res = SW_STATUS_INVALID_HANDLE;

		return sw_status_setLastError(res);
		}

XPLATFORM_DLL_EXPORT sw_status_t
sw_mutex_tryLock(sw_mutex_t hMutex)
		{
		SWInternalMutex		*pMutex=(SWInternalMutex *)hMutex;
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (pMutex != NULL && pMutex->hdr.type == SW_HANDLE_TYPE_MUTEX)
			{
			int	r;

			if (pMutex->owner == sw_thread_self())
				{
				if (IS_RECURSIVE_MUTEX(pMutex))
					{
					pMutex->lockcount++;
					
					return sw_status_setLastError(SW_STATUS_SUCCESS);
					}
				else
					{
					// We have recursively called a non-recursive mutex
					//sw_error_setLastError()
					
					return sw_status_setLastError(SW_STATUS_ALREADY_LOCKED);
					}
				}

#ifdef sun
			#error "Code incomplete"
			r = mutex_lock(&pMutex->mutex);
#elif defined(WIN32) || defined(_WIN32)
			r = WaitForSingleObject(pMutex->mutex, 0);
			if (r == WAIT_ABANDONED) r = 0;
#else
			r = pthread_mutex_trylock(&pMutex->mutex);
#endif

			if (r == 0)
				{
				pMutex->owner = sw_thread_self();
				pMutex->lockcount = 1;
				res = SW_STATUS_SUCCESS;
				}
			else
				{
#ifdef sun
				#error "Code incomplete"
#elif defined(WIN32) || defined(_WIN32)
				if (r == WAIT_TIMEOUT) res = SW_STATUS_MUTEX_NOT_ACQUIRED;
				else res = sw_status_getSystemErrorStatus();
#else
				if (r != 0) res = SW_STATUS_MUTEX_NOT_ACQUIRED;
#endif

				}
			}
		else res = SW_STATUS_INVALID_HANDLE;

		return sw_status_setLastError(res);
		}

#if defined(SW_USE_LOCAL_POSIX_TIMEDLOCK)
int
pthread_mutex_timedlock(pthread_mutex_t *mutex, struct timespec *timeout)
		{
		struct timeval	timenow;
		struct timespec	sleepytime;
		int				retcode;

		/* This is just to avoid a completely busy wait */
		sleepytime.tv_sec = 0;
		sleepytime.tv_nsec = 10000000; /* 10ms */

		while ((retcode = pthread_mutex_trylock(mutex)) == EBUSY)
			{
			gettimeofday(&timenow, NULL);

			if (timenow.tv_sec > timeout->tv_sec || (timenow.tv_sec == timeout->tv_sec && (timenow.tv_usec * 1000) >= timeout->tv_nsec))
				{
				return ETIMEDOUT;
				}

			nanosleep(&sleepytime, NULL);
			}

		return retcode;
		}
#endif


XPLATFORM_DLL_EXPORT sw_status_t
sw_mutex_timedLock(sw_mutex_t hMutex, sw_uint64_t waitms)
		{
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (waitms == 0)
			{
			res = sw_mutex_tryLock(hMutex);
			if (res == SW_STATUS_MUTEX_NOT_ACQUIRED) res = SW_STATUS_TIMEOUT;
			}
		else if (waitms == SW_TIMEOUT_INFINITE)
			{
			res = sw_mutex_lock(hMutex);
			}
		else
			{
			SWInternalMutex		*pMutex=(SWInternalMutex *)hMutex;

			if (pMutex != NULL && pMutex->hdr.type == SW_HANDLE_TYPE_MUTEX)
				{
				int		r;
#if defined(WIN32) || defined(_WIN32)
				DWORD	t;

				t = (waitms == SW_TIMEOUT_INFINITE)?INFINITE:(DWORD)waitms;
#endif

				if (pMutex->owner == sw_thread_self())
					{
					if (IS_RECURSIVE_MUTEX(pMutex))
						{
						pMutex->lockcount++;
						
						return sw_status_setLastError(SW_STATUS_SUCCESS);
						}
					else
						{
						// We have recursively called a non-recursive mutex
						//sw_error_setLastError()
						
						return sw_status_setLastError(SW_STATUS_ALREADY_LOCKED);
						}
					}

#ifdef sun
				#error "Code incomplete"
				r = mutex_lock(&pMutex->mutex);
#elif defined(WIN32) || defined(_WIN32)
				r = WaitForSingleObject(pMutex->mutex, t);
				if (r == WAIT_ABANDONED) r = 0;
#else
				struct timespec	ts;
				struct timeval	timenow;

				gettimeofday(&timenow, NULL);

				sw_uint64_t		adjsec=waitms / 1000;
				sw_uint64_t		adjms=waitms % 1000;

				ts.tv_sec = timenow.tv_sec + adjsec;
				ts.tv_nsec = (timenow.tv_usec * 1000) + (adjms * 1000000);
				if (ts.tv_nsec >= 1000000000)
					{
					ts.tv_nsec -= 1000000000;
					ts.tv_sec++;
					}

				r = pthread_mutex_timedlock(&pMutex->mutex, &ts);
#endif

				if (r == 0)
					{
					pMutex->owner = sw_thread_self();
					pMutex->lockcount = 1;
					res = SW_STATUS_SUCCESS;
					}
				else
					{
#ifdef sun
					#error "Code incomplete"
#elif defined(WIN32) || defined(_WIN32)
					if (r == WAIT_TIMEOUT) res = SW_STATUS_TIMEOUT;
					else res = sw_status_getSystemErrorStatus();
#else
					if (r == ETIMEDOUT) res = SW_STATUS_TIMEOUT;
					else res = sw_status_getSystemErrorStatus();
#endif

					}
				}
			else res = SW_STATUS_INVALID_HANDLE;
			}

		return sw_status_setLastError(res);
		}


XPLATFORM_DLL_EXPORT sw_bool_t
sw_mutex_hasLock(sw_mutex_t hMutex)
		{
		SWInternalMutex	*pMutex=(SWInternalMutex *)hMutex;
		sw_bool_t			res=false;

		if (pMutex != NULL && pMutex->hdr.type == SW_HANDLE_TYPE_MUTEX)
			{
			res = (pMutex->owner == sw_thread_self());
			}

		return res;
		}


XPLATFORM_DLL_EXPORT sw_bool_t
sw_mutex_isLocked(sw_mutex_t hMutex)
		{
		SWInternalMutex	*pMutex=(SWInternalMutex *)hMutex;
		sw_bool_t			res=false;

		if (pMutex != NULL && pMutex->hdr.type == SW_HANDLE_TYPE_MUTEX)
			{
			res = (pMutex->owner != 0);
			}

		return res;
		}


XPLATFORM_DLL_EXPORT sw_status_t
sw_mutex_unlock(sw_mutex_t hMutex)
		{
		SWInternalMutex		*pMutex=(SWInternalMutex *)hMutex;
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (pMutex != NULL && pMutex->hdr.type == SW_HANDLE_TYPE_MUTEX)
			{
			int	r;

			if (pMutex->lockcount <= 0 || pMutex->owner != sw_thread_self())
				{
				// We have called release, but we don't have the lock
				// so throw an exception
				//SW_THROW_TEXT(SWThreadMutexException, _T("release/unlock called when lock not held"));
				return sw_status_setLastError(SW_STATUS_NOT_OWNER);
				}

			// Decrement the lock count and simply return if we are not the last!
			if (--pMutex->lockcount > 0) return sw_status_setLastError(SW_STATUS_SUCCESS);

			// If we get here and something is wrong!
			// assert(pMutex->lockcount == 0);

			pMutex->owner = 0;
#if defined(WIN32) || defined(_WIN32)
			r = !ReleaseMutex(pMutex->mutex);
#else
	#ifdef sun
			r = mutex_unlock(&pMutex->mutex);
	#else
			// POSIX
			r = pthread_mutex_unlock(&pMutex->mutex);
	#endif
			if (r == 0) res = SW_STATUS_SUCCESS;
			else res = sw_status_getSystemErrorStatus();
#endif
			}
		else res = SW_STATUS_INVALID_HANDLE;

		return sw_status_setLastError(res);
		}

XPLATFORM_DLL_EXPORT sw_status_t
sw_mutex_destroy(sw_mutex_t hMutex)
		{
		SWInternalMutex		*pMutex=(SWInternalMutex *)hMutex;
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (pMutex != NULL && pMutex->hdr.type == SW_HANDLE_TYPE_MUTEX)
			{
#ifdef sun
			mutex_destroy(&pMutex->mutex); 
#elif defined(WIN32) || defined(_WIN32)
			CloseHandle(pMutex->mutex);
#else
			pthread_mutex_destroy(&pMutex->mutex); 
#endif
			if (pMutex->name != NULL)
				{
				free(pMutex->name);
				}

			memset(pMutex, 0, sizeof(SWInternalMutex));
			free(pMutex);
			res = SW_STATUS_SUCCESS;
			}
		else res = SW_STATUS_INVALID_HANDLE;

		return sw_status_setLastError(res);
		}


XPLATFORM_DLL_EXPORT	int
sw_mutex_forceUnlock(sw_mutex_t hMutex)
		{
		SWInternalMutex	*pMutex=(SWInternalMutex *)hMutex;
		int				res=-1;

		if (pMutex != NULL && pMutex->hdr.type == SW_HANDLE_TYPE_MUTEX)
			{
			if (pMutex->owner == sw_thread_self())
				{
				res = pMutex->lockcount;
				if (res > 0)
					{
					pMutex->lockcount = 1;
					sw_mutex_unlock(hMutex);
					}
				}
			}

		return res;
		}


XPLATFORM_DLL_EXPORT	sw_status_t
sw_mutex_restoreLock(sw_mutex_t hMutex, int count)
		{
		SWInternalMutex		*pMutex=(SWInternalMutex *)hMutex;
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (count > 0 && (res = sw_mutex_lock(hMutex)) == SW_STATUS_SUCCESS)
			{
			pMutex->lockcount = count;
			res = SW_STATUS_SUCCESS;
			}

		return res;
		}


/**
*** Internal function used by sw_condvar.c to get the mutex owner and
*** lockcount fields.
**/
void
sw_mutex_internal_getOwnerAndLockCount(sw_mutex_t hMutex, sw_thread_id_t *pOwner, int *pLockCount)
		{
		SWInternalMutex	*pMutex=(SWInternalMutex *)hMutex;

		if (pMutex != NULL && pMutex->hdr.type == SW_HANDLE_TYPE_MUTEX)
			{
			if (pOwner) *pOwner = pMutex->owner;
			if (pLockCount) *pLockCount = pMutex->lockcount;
			}
		}



/**
*** Internal function used by sw_condvar.c to set the mutex owner and
*** lockcount fields.
**/
void
sw_mutex_internal_setOwnerAndLockCount(sw_mutex_t hMutex, sw_thread_id_t owner, int lockcount)
		{
		SWInternalMutex	*pMutex=(SWInternalMutex *)hMutex;

		if (pMutex != NULL && pMutex->hdr.type == SW_HANDLE_TYPE_MUTEX)
			{
			pMutex->lockcount = lockcount;
			pMutex->owner = owner;
			}
		}


#if !defined(WIN32) && !defined(_WIN32)
/**
*** Internal function to get the mutex (required by sw_condvar.c).
**/
#ifdef sun
mutex_t *
#else
pthread_mutex_t *
#endif
sw_mutex_internal_getMutexPtr(sw_mutex_t hMutex)
		{
		SWInternalMutex	*pMutex=(SWInternalMutex *)hMutex;

		if (pMutex != NULL && pMutex->hdr.type == SW_HANDLE_TYPE_MUTEX)
			{
			return &pMutex->mutex;
			}

		return NULL;
		}

#endif
