/**
*** @file		sw_types.h
*** @brief		Defines data types
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definitions of the various data types used by the swift 
*** library and any dervied software.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __xplatform_sw_types_h__
#define __xplatform_sw_types_h__


#ifndef __xplatform_sw_platform_h__
#include <xplatform/sw_platform.h>
#endif

#ifndef __xplatform_sw_build_h__
#include <xplatform/sw_build.h>
#endif

/**************************************************************** 
** Define the "standard" types
****************************************************************/
#if defined(SW_PLATFORM_WINDOWS)
	#include <config/targetver.h>

	#if defined(SW_BUILD_MFC) && defined(__cplusplus)
		// We are building the MFC version
		#include <afxwin.h>
		#include <afxdisp.h>
	#endif

	#define _CRT_RAND_S
	#define WIN32_LEAN_AND_MEAN
	#define _CRT_SECURE_NO_WARNINGS
	#define _WINSOCK_DEPRECATED_NO_WARNINGS

	#include <windows.h>
	#include <winnt.h>
	#include <tchar.h>

	// Disable common warnings
	#pragma warning(disable: 4251)
	#pragma warning(disable: 4996)
	#pragma warning(disable: 4353)
#endif // defined(SW_PLATFORM_WINDOWS)


#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include <time.h>
#include <wchar.h>
#include <limits.h>
#include <errno.h>

#include <xplatform/sw_handle.h>


/**************************************************************** 
** Boolean
****************************************************************/
#if !defined(__cplusplus)
	typedef unsigned char		bool;		///< bool data type for C programs (for C++ this is native)

	// Define true (Java compatibility)
	#ifndef true
		#define true	1					///< bool true value for C programs
	#endif

	// Define false (Java compatibility)
	#ifndef false
		#define false	0					///< bool false value for C programs
	#endif
#endif


// Define TRUE (fairly standard)
#ifndef TRUE
	#define TRUE	true					///< Alternative bool value true for Microsoft compatibility.
#endif

// Define FALSE (fairly standard)
#ifndef FALSE
	#define FALSE	false					///< Alternative bool value false for Microsoft compatibility.
#endif

typedef bool					sw_bool_t;		///< A boolean value


/**************************************************************** 
** Integer types
****************************************************************/

typedef char					sw_int8_t;		///< An 8-bit (1 byte) signed integer
typedef short					sw_int16_t;		///< An 16-bit (2 byte) signed integer
typedef int						sw_int32_t;		///< An 32-bit (4 byte) signed integer
#if defined(SW_PLATFORM_WINDOWS)
	typedef __int64				sw_int64_t;		///< An 64-bit (8 byte) signed integer
#else
	typedef long long			sw_int64_t;		///< An 64-bit (8 byte) signed integer
#endif

typedef unsigned char			sw_uint8_t;		///< An 8-bit (1 byte) unsigned integer
typedef unsigned short			sw_uint16_t;	///< An 16-bit (2 byte) unsigned integer
typedef unsigned int			sw_uint32_t;	///< An 32-bit (4 byte) unsigned integer
#if defined(SW_PLATFORM_WINDOWS)
	typedef unsigned __int64	sw_uint64_t;	///< An 64-bit (8 byte) signed integer
#else
	typedef unsigned long long	sw_uint64_t;	///< An 64-bit (8 byte) signed integer
#endif


typedef signed short			sw_short_t;		///< A signed short integer 
typedef signed int				sw_int_t;		///< A signed integer
typedef signed long				sw_long_t;		///< A signed long integer

typedef unsigned short			sw_ushort_t;	///< An unsigned short integer
typedef unsigned int			sw_uint_t;		///< A unsigned integer
typedef unsigned long			sw_ulong_t;		///< An unsigned long integer


typedef sw_int32_t				sw_index_t;		///< A signed type used for positions
typedef size_t					sw_size_t;		///< An unsigned type used for sizes
typedef sw_uint8_t				sw_byte_t;		///< An integer type for bytes (8-bit, unsigned).
typedef sw_uint32_t				sw_status_t;	///< A type used for all status codes.
typedef sw_int64_t				sw_money_t;		///< Used for general money calculations


/**
*** @brief An integer that specifies the pointer size in bytes for the current platform
***
*** Note that this is a number not a sizeof(void *) because we want to use this with the
*** pre-processor. Unfortunately you cannot write <b>#if sizeof(void *) == 4</b> as the
*** pre-processor does not know what the compiler model is.
**/
#undef SW_SIZEOF_POINTER

#if defined(__LP64__) || defined(_LP64) || defined(__ia64) || defined(_M_IA64) || defined(_M_ALPHA) || defined(__alpha) || defined(_M_AMD64)
	#define SW_SIZEOF_POINTER	8
	typedef sw_int64_t		sw_intptr_t;	///< An integer that is the same size as a pointer
	typedef sw_uint64_t		sw_uintptr_t;	///< An unsigned integer that is the same size as a pointer
#else
	#define SW_SIZEOF_POINTER	4
	typedef sw_int32_t		sw_intptr_t;	///< An integer that is the same size as a pointer
	typedef sw_uint32_t		sw_uintptr_t;	///< An unsigned integer that is the same size as a pointer
#endif

/**
*** @brief An integer that specifies the size of the native type <b>long</b>in bytes for the current platform.
***
*** Note that this is a number not a sizeof(void *) because we want to use this with the
*** pre-processor. Unfortunately you cannot write <b>#if sizeof(void *) == 4</b> as the
*** pre-processor does not know what the compiler model is.
**/
#if defined(__SIZEOF_LONG__)
	#define SW_SIZEOF_LONG	__SIZEOF_LONG__
#elif defined(_M_ALPHA) || defined(__alpha) || defined(__MACH__)
	#define SW_SIZEOF_LONG	8
#else
	#define SW_SIZEOF_LONG	4
#endif


/**************************************************************** 
** Integer Splits
****************************************************************/
typedef union sw_uint64_split
		{
		struct
			{
#if defined(SW_PLATFORM_INT64_IS_HIGH32_LOW32)
			sw_uint32_t	high32;
			sw_uint32_t	low32;
#else
			sw_uint32_t	low32;
			sw_uint32_t	high32;
#endif
			} u;
		sw_uint64_t	ui64;
		} SW_SPLIT_UINT64;


/**************************************************************** 
** Floating point
****************************************************************/
typedef float				sw_float_t;		///< A floating point data type
typedef double				sw_double_t;	///< A double-precision floating point data type


/**************************************************************** 
** Character and string types
****************************************************************/
#if defined(_NATIVE_WCHAR_T_DEFINED)
	#define SW_NATIVE_WCHAR_T_DEFINED
#endif

#if defined(SW_BUILD_UNICODE)
	typedef wchar_t	sw_tchar_t;
#else
	typedef char	sw_tchar_t;
#endif

typedef char			sw_cchar_t;		///< Single-byte character data type.
typedef wchar_t			sw_wchar_t;		///< Platform specific wide character data type.
typedef unsigned char	sw_uchar_t;		///< Unsigned single-byte character data type.


#if defined(WIN32) || defined(_WIN32) || defined(_AIX)
	#define SW_SIZEOF_WCHAR		2		///< Define the size of a wchar_t on this platform
#elif defined(sun) || defined(linux) || defined(__osf__) || defined(__MACH__)
	#define SW_SIZEOF_WCHAR		4		///< Define the size of a wchar_t on this platform
#else
	#error size of wchar_t unknown on this platform. Please update swift/sw_types.h
#endif


/**
*** @brief	A generic character type.
***
*** This type is not intended for declaring strings but a type to be able
*** to hold a single character that can be passed to a single, double or quad
*** byte string. This type is used extensively by the SWString class when
*** operations involving characters are declared.
**/
typedef int		sw_char_t;



/**************************************************************** 
** File
****************************************************************/
typedef sw_uint32_t	sw_dev_t;
typedef sw_uint32_t	sw_inode_t;
typedef sw_uint32_t	sw_fileattr_t;
typedef sw_uint64_t	sw_filesize_t;
typedef sw_int64_t	sw_filepos_t;

#if defined(SW_PLATFORM_WINDOWS)
	typedef HANDLE		sw_filehandle_t;
	typedef sw_uint32_t	sw_mode_t;				///< A type representing the unix mode of a file.
	typedef sw_int32_t	sw_uid_t;				///< A type representing a unix user ID
	typedef sw_int32_t	sw_gid_t;				///< A type representing a unix group ID
#else
	typedef int			sw_filehandle_t;
	typedef mode_t		sw_mode_t;				///< A type representing the unix mode of a file.
	typedef uid_t		sw_uid_t;				///< A type representing a unix user ID
	typedef gid_t		sw_gid_t;				///< A type representing a unix group ID
#endif


/**************************************************************** 
** Time
****************************************************************/

/**
*** A time structure used internally.
**/
typedef struct sw_internal_time
	{
	sw_int64_t	tv_sec;		///< Seconds
	sw_uint32_t	tv_nsec;	///< Nanoseconds
	} sw_internal_time_t;


/**
*** A high resolution time structure holding time in seconds and nanoseconds.
**/
typedef struct sw_timespec
	{
	sw_int64_t	tv_sec;		///< Seconds
	sw_uint32_t	tv_nsec;	///< Nanoseconds
	} sw_timespec_t;


/**
*** A high resolution time structure holding time in seconds and microseconds.
**/
typedef struct sw_timeval
	{
	sw_int64_t	tv_sec;		///< Seconds
	sw_uint32_t	tv_usec;	///< Nanoseconds
	} sw_timeval_t;

/*
#if (defined(WIN32) || defined(_WIN32)) && !defined(ACE_TIME_VALUE_H)
#else
typedef struct timespec	sw_timespec_t;	///< A high resolution time structure holding time in seconds and nanoseconds.
#endif

typedef struct timeval	sw_timeval_t;	///< A high resolution time structure holding time in seconds and microseconds.
*/
typedef struct tm		sw_tm_t;		///< A time/date structure representing the different portions of the time and date.

#if !defined(WIN32) && !defined(_WIN32)
// Used for windows compatibilty and NOT provided by ACE

/**
*** @brief	The SYSTEMTIME structure as defined by Microsoft for windows.
***
*** We use the type sw_uint_t16 instead of WORD here to avoid having to define
*** unnessecary microsoft types in the SWIFT.
**/
typedef struct SW_WINDOWS_SYSTEMTIME
	{
	sw_uint16_t	wYear;				///< The year (1602 - 30806)
	sw_uint16_t	wMonth;				///< The month of the year (January=1 - December=12)
	sw_uint16_t	wDayOfWeek;			///< The day of the week (Sunday=0 - Saturday=6)
	sw_uint16_t	wDay;				///< The day of the month (1-31)
	sw_uint16_t	wHour;				///< The hour (0-23)
	sw_uint16_t	wMinute;			///< The minute (0-59)
	sw_uint16_t	wSecond;			///< The second (0-61)
	sw_uint16_t	wMilliseconds;		///< The milliseconds
	} SYSTEMTIME, *PSYSTEMTIME, *LPSYSTEMTIME;


/**
*** @brief	The FILETIME structure as defined by Microsoft for windows.
***
*** The FILETIME structure is a 64-bit value split in 2 halves (the low
*** and high 32-bit values) which together represent the number of
*** 100-nanosecond intervals since 1st January 1900.
***
*** We use the type sw_uint32_t instead of DWORD here to avoid having to define
*** unnessecary microsoft types in the SWIFT.
***
*** The order of the high and low 32-bit words is reversed depending on which platform
*** we are running on so that we can do a cast to a 64-bit integer directly as we can
*** under windows. This is checked as part of the tests built in to the SWIFT library builds.
**/
typedef struct SW_WINDOWS_FILETIME
		{
#if defined(SW_PLATFORM_INT64_IS_HIGH32_LOW32)
		sw_uint32_t	dwHighDateTime;		///< The high 32 bits of the 64-bit filetime value
		sw_uint32_t	dwLowDateTime;		///< The low 32 bits of the 64-bit filetime value
#else
		sw_uint32_t	dwLowDateTime;		///< The low 32 bits of the 64-bit filetime value
		sw_uint32_t	dwHighDateTime;		///< The high 32 bits of the 64-bit filetime value
#endif
		} FILETIME, *PFILETIME, *LPFILETIME;

#endif


/**
*** @brief	Special timeout value for infinite wait.
***
*** A special timeout value used by operations involving timeouts
*** when an option to never timeout is required.
**/
#ifdef __cplusplus
const sw_uint64_t		SW_TIMEOUT_INFINITE	= SW_CONST_UINT64(0xffffffffffffffff);
#else
#define SW_TIMEOUT_INFINITE	SW_CONST_UINT64(0xffffffffffffffff)
#endif


/**************************************************************** 
** UUID
****************************************************************/

/// UUID
typedef struct sw_uuid
	{
	sw_uint32_t		timeLow;				///< The low field of the timestamp
	sw_uint16_t		timeMid;				///< The middle field of the timestamp
	sw_uint16_t		timeHighAndVersion;		///< The high field of the timestamp multiplexed with the version number
	sw_uint8_t		clockSeqHighAndVariant;	///< The high field of the clock sequence multiplexed with the variant
	sw_uint8_t		clockSeqLow;			///< The low field of the clock sequence
	sw_byte_t		node[6];				///< The spatially unique node identifier
	} sw_uuid_t;


/**************************************************************** 
** Network
****************************************************************/

/// Opaque socket address
typedef struct sw_sockaddr
	{
	sw_uint16_t	family;		///< The address family (16-bits) =- must be the first item
	char		data[30];	///< Padding data - must be enough to cover all types that will overlay it.
	} sw_sockaddr_t;

typedef int		sw_sockhandle_t;	///< A type representing a socket handle


#if defined(SW_PLATFORM_WINDOWS)
	typedef sw_uint32_t		ipaddr_t;
	typedef int				socklen_t;
	typedef unsigned long	in_addr_t;

	#define MAXHOSTNAMELEN			256
#endif

#define SW_NET_INVALID_SOCKET	-1



/**************************************************************** 
** Misc
****************************************************************/
typedef sw_handle_t		sw_hkey_t;			///< Windows HKEY equivalent
typedef sw_uint32_t		sw_pid_t;			///< Type used for process IDs
typedef sw_uint32_t		sw_resourceid_t;	///< The type used for resource IDs


/**************************************************************** 
** Limits
****************************************************************/

#ifdef __cplusplus
const sw_int8_t			SW_INT8_MIN		= SW_CONST_INT8(-128);											///< minimum (signed) int8 value
const sw_int8_t			SW_INT8_MAX		= SW_CONST_INT8(127);											///< maximum (signed) int8 value
const sw_uint8_t		SW_UINT8_MAX	= SW_CONST_UINT8(0xff);											///< maximum unsigned int8 value

const sw_int16_t		SW_INT16_MIN	= SW_CONST_INT16(-32768);										///< minimum (signed) int16 value
const sw_int16_t		SW_INT16_MAX	= SW_CONST_INT16(32767);										///< maximum (signed) int16 value
const sw_uint16_t		SW_UINT16_MAX	= SW_CONST_UINT16(0xffff);										///< maximum unsigned int16 value

const sw_int32_t		SW_INT32_MIN	= SW_CONST_INT32(-2147483647) - SW_CONST_INT32(1);				///< minimum (signed) int32 value
const sw_int32_t		SW_INT32_MAX	= SW_CONST_INT32(2147483647);									///< maximum (signed) int32 value
const sw_uint32_t		SW_UINT32_MAX	= SW_CONST_UINT32(0xffffffff);									///< maximum unsigned int32 value

const sw_int64_t		SW_INT64_MAX	= SW_CONST_INT64(9223372036854775807);							///< maximum signed int64 value
const sw_int64_t		SW_INT64_MIN	= SW_CONST_INT64(-9223372036854775807) - SW_CONST_INT64(1);		///< minimum signed int64 value
const sw_uint64_t		SW_UINT64_MAX	= SW_CONST_UINT64(0xffffffffffffffff);							///< maximum unsigned int64 value

#if SW_SIZEOF_LONG == 4
	const sw_long_t		SW_LONG_MIN		= SW_CONST_LONG(-2147483647) - SW_CONST_LONG(1);				///< minimum (signed) long value
	const sw_long_t		SW_LONG_MAX		= SW_CONST_LONG(2147483647);									///< maximum (signed) long value
	const sw_ulong_t	SW_ULONG_MAX	= SW_CONST_ULONG(0xffffffff);									///< maximum unsigned long value
#elif SW_SIZEOF_LONG == 8
	const sw_long_t		SW_LONG_MIN		= SW_CONST_LONG(-9223372036854775807) - SW_CONST_LONG(1);		///< minimum (signed) long value
	const sw_long_t		SW_LONG_MAX		= SW_CONST_LONG(9223372036854775807);							///< maximum (signed) long value
	const sw_ulong_t	SW_ULONG_MAX	= SW_CONST_ULONG(0xffffffffffffffff);									///< maximum unsigned long value
#else
	#error SW_SIZEOF_LONG undefined - please update sw_types.h
#endif

#else

#define	SW_INT8_MIN		SW_CONST_INT8(-128)											///< minimum (signed) int8 value
#define	SW_INT8_MAX		SW_CONST_INT8(127)											///< maximum (signed) int8 value
#define	SW_UINT8_MAX	SW_CONST_UINT8(0xff)											///< maximum unsigned int8 value

#define	SW_INT16_MIN	SW_CONST_INT16(-32768)										///< minimum (signed) int16 value
#define	SW_INT16_MAX	SW_CONST_INT16(32767)										///< maximum (signed) int16 value
#define	SW_UINT16_MAX	SW_CONST_UINT16(0xffff)										///< maximum unsigned int16 value

#define	SW_INT32_MIN	(SW_CONST_INT32(-2147483647) - SW_CONST_INT32(1))				///< minimum (signed) int32 value
#define	SW_INT32_MAX	SW_CONST_INT32(2147483647)									///< maximum (signed) int32 value
#define	SW_UINT32_MAX	SW_CONST_UINT32(0xffffffff)									///< maximum unsigned int32 value

#define	SW_INT64_MAX	SW_CONST_INT64(9223372036854775807)							///< maximum signed int64 value
#define	SW_INT64_MIN	(SW_CONST_INT64(-9223372036854775807) - SW_CONST_INT64(1))		///< minimum signed int64 value
#define	SW_UINT64_MAX	SW_CONST_UINT64(0xffffffffffffffff)							///< maximum unsigned int64 value

#if SW_SIZEOF_LONG == 4
	#define	SW_LONG_MIN		SW_CONST_LONG(-2147483647) - SW_CONST_LONG(1)				///< minimum (signed) long value
	#define	SW_LONG_MAX		SW_CONST_LONG(2147483647)									///< maximum (signed) long value
	#define	SW_ULONG_MAX	SW_CONST_ULONG(0xffffffff)									///< maximum unsigned long value
#elif SW_SIZEOF_LONG == 8
	#define	SW_LONG_MIN		(SW_CONST_LONG(-9223372036854775807) - SW_CONST_LONG(1))		///< minimum (signed) long value
	#define	SW_LONG_MAX		SW_CONST_LONG(9223372036854775807)							///< maximum (signed) long value
	#define	SW_ULONG_MAX	SW_CONST_ULONG(0xffffffffffffffff)									///< maximum unsigned long value
#else
	#error SW_SIZEOF_LONG undefined - please update sw_types.h
#endif

#endif


#endif // __xplatform_sw_types_h__
