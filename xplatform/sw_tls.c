/****************************************************************************
**	sw_tls.c		Define platform independent thread local storage
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"


#include <xplatform/sw_tls.h>
#include <xplatform/sw_status.h>
#include <errno.h>

typedef struct sw_tls_internal
		{
		sw_handle_header_t	hdr;			///< The handle common header
#if defined(WIN32) || defined(_WIN32)
		// Windows Thread Specific Members
		DWORD			index;		///< The thread handle
#else
	// Unix/Linux
	#ifdef sun
		// Solaris Threads Specific Members
		thread_key_t	key;		///< The TLS key
	#else // sun
		// POSIX Threads Specific Members
		pthread_key_t	key;		///< The TLS key
	#endif // sun
#endif // WIN32
		}  SWInternalTLS;


XPLATFORM_DLL_EXPORT	sw_tls_t
sw_tls_create()
		{
		SWInternalTLS	*pTLS;

		pTLS = (SWInternalTLS *)malloc(sizeof(SWInternalTLS));
		if (pTLS != NULL)
			{
			memset(pTLS, 0, sizeof(SWInternalTLS));
			pTLS->hdr.type = SW_HANDLE_TYPE_TLS;
			pTLS->hdr.size = sizeof(SWInternalTLS);

#if defined(WIN32) || defined(_WIN32)
			pTLS->index = TlsAlloc();
			if (pTLS->index == TLS_OUT_OF_INDEXES)
				{
				// sw_status_setLastError(sw_status_getSystemErrorStatus());
				}
#else
			// Unix/Linux
			int	r=0;

	#ifdef sun
			r = thr_keycreate(&pTLS->key, 0);
	#else // sun
			r = pthread_key_create(&pTLS->key, 0);
	#endif // sun

			if (r != 0)
				{
				// sw_status_setLastError(sw_status_getSystemErrorStatusForCode(r));
				}
#endif // WIN32
			}
		else
			{
			// sw_status_setLastError(SW_STATUS_OUT_OF_MEMORY);
			}

		return pTLS;
		}




XPLATFORM_DLL_EXPORT	sw_status_t
sw_tls_setValue(sw_tls_t hTls, void *data)
		{
		SWInternalTLS	*pTLS=(SWInternalTLS *)hTls;
		sw_status_t		res=SW_STATUS_SUCCESS;

		if (pTLS != NULL && pTLS->hdr.type == SW_HANDLE_TYPE_TLS)
			{
#if defined(WIN32) || defined(_WIN32)
			if (!TlsSetValue(pTLS->index, data)) res = sw_status_getSystemErrorStatus();
#else
	// Unix/Linux
			int	r;

	#ifdef sun
			r = thr_setspecific(pTLS->key, data);
	#else // sun
			r = pthread_setspecific(pTLS->key, data);
	#endif // sun

			if (r != 0) res = sw_status_getSystemErrorStatusForCode(r);
#endif // WIN32
			}
		else res = SW_STATUS_INVALID_HANDLE;

		return res; // sw_status_setLastError(res);
		}


XPLATFORM_DLL_EXPORT	sw_status_t
sw_tls_getValue(sw_tls_t hTls, void **data)
		{
		SWInternalTLS	*pTLS=(SWInternalTLS *)hTls;
		sw_status_t		res=SW_STATUS_SUCCESS;

		if (pTLS != NULL && pTLS->hdr.type == SW_HANDLE_TYPE_TLS)
			{
#if defined(WIN32) || defined(_WIN32)
			*data = TlsGetValue(pTLS->index);
			if (*data == 0 && GetLastError() != 0) res = sw_status_getSystemErrorStatus();
#else
	// Unix/Linux
			int		r;

	#ifdef sun
			r = thr_getspecific(pTLS->key, data);
	#else // sun
			*data = pthread_getspecific(pTLS->key);
			if (*data == NULL) r = errno;
			else r = 0;
	#endif // sun

			if (r != 0) res = sw_status_getSystemErrorStatusForCode(r);
#endif // WIN32
			}
		else res = SW_STATUS_INVALID_HANDLE;

		return res; // sw_status_setLastError(res);
		}


XPLATFORM_DLL_EXPORT	sw_status_t
sw_tls_destroy(sw_tls_t hTls)
		{
		SWInternalTLS	*pTLS=(SWInternalTLS *)hTls;
		sw_status_t		res=SW_STATUS_SUCCESS;

		if (pTLS != NULL && pTLS->hdr.type == SW_HANDLE_TYPE_TLS)
			{
#if defined(WIN32) || defined(_WIN32)
			if (!TlsFree(pTLS->index)) res = sw_status_getSystemErrorStatus();
#else
	// Unix/Linux
			int		r;
	#ifdef sun
			// Nothing to do here
			r = 0;
	#else // sun
			r = pthread_key_delete(pTLS->key);
	#endif // sun

			if (r != 0) res = sw_status_getSystemErrorStatusForCode(r);
#endif // WIN32

			memset(pTLS, 0, sizeof(SWInternalTLS));
			free(pTLS);
			res = SW_STATUS_SUCCESS;
			}
		else res = SW_STATUS_INVALID_HANDLE;

		return res; // sw_status_setLastError(res);
		}
