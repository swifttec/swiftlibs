/**
*** @file		sw_string.h
*** @brief		String handling
*** @version	1.0
*** @author		Simon Sparkes
***
*** This file is the general header used by all of the components of the
*** swift library.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#ifndef __xplatform_sw_string_h__
#define __xplatform_sw_string_h__

#include <xplatform/xplatform.h>

#ifdef __cplusplus
extern "C" {
#endif

XPLATFORM_DLL_EXPORT unsigned long	sw_string_convertToULong(const void *sp, int cs, int base, bool uflag);
XPLATFORM_DLL_EXPORT unsigned long	sw_string_convertStringToULong(const char *sp, int base, bool uflag);
XPLATFORM_DLL_EXPORT unsigned long	sw_string_convertWStringToULong(const wchar_t *sp, int base, bool uflag);

XPLATFORM_DLL_EXPORT sw_uint64_t		sw_string_convertToUInt64(const void *sp, int cs, int base, bool uflag);
XPLATFORM_DLL_EXPORT sw_uint64_t		sw_string_convertStringToUInt64(const char *sp, int base, bool uflag);
XPLATFORM_DLL_EXPORT sw_uint64_t		sw_string_convertWStringToUInt64(const wchar_t *sp, int base, bool uflag);


/// Copy a string to a limited buffer, guaranteed to zero terminated
/// bufsize is in chars not bytes
XPLATFORM_DLL_EXPORT char *			strbufcpy(char *destination, const char *source, sw_size_t bufsize);

/// Copy a wide string to a limited buffer, guaranteed to zero terminated
/// bufsize is in chars not bytes
XPLATFORM_DLL_EXPORT wchar_t *		wcsbufcpy(wchar_t *destination, const wchar_t *source, sw_size_t bufsize);

/// Copy a string to a limited buffer, guaranteed to zero terminated
/// bufsize is in chars not bytes
XPLATFORM_DLL_EXPORT char *			sw_strcpy(char *destination, const char *source, sw_size_t bufsize);

/// Copy a wide string to a limited buffer, guaranteed to zero terminated
/// bufsize is in chars not bytes
XPLATFORM_DLL_EXPORT wchar_t *		sw_wcscpy(wchar_t *destination, const wchar_t *source, sw_size_t bufsize);


#ifdef __cplusplus
}
#endif

#endif // __xplatform_sw_string_h__
