/****************************************************************************
**	sw_mutex.h	An abstract mutex class
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __xplatform_sw_mutex_h__
#define __xplatform_sw_mutex_h__

#include <xplatform/xplatform.h>


#ifdef __cplusplus
extern "C" {
#endif

#define SW_MUTEX_TYPE_UNKNOWN	0x0
#define SW_MUTEX_TYPE_THREAD	0x1
#define SW_MUTEX_TYPE_PROCESS	0x2
#define SW_MUTEX_TYPE_RECURSIVE	0x4

#define SW_MUTEX_TYPE_RECURSIVE_THREAD	(SW_MUTEX_TYPE_THREAD|SW_MUTEX_TYPE_RECURSIVE)
#define SW_MUTEX_TYPE_RECURSIVE_PROCESS	(SW_MUTEX_TYPE_PROCESS|SW_MUTEX_TYPE_RECURSIVE)

typedef sw_handle_t	sw_mutex_t;

// Create a mutex (name is ignored for thread mutexes and should be NULL)
XPLATFORM_DLL_EXPORT	sw_mutex_t	sw_mutex_create(int type, const char *name);

XPLATFORM_DLL_EXPORT	sw_status_t	sw_mutex_lock(sw_mutex_t hMutex);
XPLATFORM_DLL_EXPORT	int			sw_mutex_forceUnlock(sw_mutex_t hMutex);
XPLATFORM_DLL_EXPORT	sw_status_t	sw_mutex_restoreLock(sw_mutex_t hMutex, int count);
XPLATFORM_DLL_EXPORT	sw_status_t	sw_mutex_timedLock(sw_mutex_t hMutex, sw_uint64_t waitms);
XPLATFORM_DLL_EXPORT	sw_status_t	sw_mutex_tryLock(sw_mutex_t hMutex);
XPLATFORM_DLL_EXPORT	sw_bool_t	sw_mutex_hasLock(sw_mutex_t hMutex);
XPLATFORM_DLL_EXPORT	sw_status_t	sw_mutex_unlock(sw_mutex_t hMutex);
XPLATFORM_DLL_EXPORT	sw_status_t	sw_mutex_destroy(sw_mutex_t hMutex);
XPLATFORM_DLL_EXPORT	sw_bool_t	sw_mutex_isLocked(sw_mutex_t hMutex);

// Alternate names
#define sw_mutex_acquire(x)	sw_mutex_lock(x)
#define sw_mutex_release(x)	sw_mutex_unlock(x)

#ifdef __cplusplus
}
#endif

#endif
