/**
*** @file		sw_platform.h
*** @brief		Platform header for the xplatform library.
*** @version	1.0
*** @author		Simon Sparkes
***
*** This file is used to define various macros dependent on the current
*** build platform/target.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#ifndef __xplatform_sw_platform_h__
#define __xplatform_sw_platform_h__

#include <xplatform/sw_osid.h>

#undef SW_TEMPLATES_REQUIRE_PRAGMA							///< Defined if the compiler requires an implementation pragma

// Thread interface
#undef SW_USE_WINDOWS_THREADS
#undef SW_USE_SOLARIS_THREADS
#undef SW_USE_POSIX_THREADS
#undef SW_USE_POSIX_THREADS_LACKS_YIELD

// File interface
#undef SW_USE_UNIX_FILES
#undef SW_USE_UNIX_64BIT_FILES
#undef SW_USE_UNIX_TELL
#undef SW_USE_WINDOWS_FILES

// Folders (directories) interface
#undef SW_USE_UNIX_FOLDERS
#undef SW_USE_WINDOWS_FOLDERS

// DLL/Shared library interface
#undef SW_USE_WINDOWS_DLLS
#undef SW_USE_UNIX_DLLS

// Semaphores
#undef SW_USE_WINDOWS_SEMAPHORES
#undef SW_USE_POSIX_SEMAPHORES
#undef SW_USE_SOLARIS_SEMAPHORES
#undef SW_USE_SYSV_SEMAPHORES

// Mutexes
#undef SW_USE_WINDOWS_MUTEXES
#undef SW_USE_SOLARIS_MUTEXES
#undef SW_USE_POSIX_MUTEXES
#undef SW_USE_LOCAL_POSIX_TIMEDLOCK

// Reader/Writer locks
#undef SW_USE_EMULATED_RWLOCKS
#undef SW_USE_POSIX_RWLOCKS
#undef SW_USE_SOLARIS_RWLOCKS

#if defined(WIN32) || defined(_WIN32)

	#define SW_PLATFORM_WINDOWS								///< Defined when building for a windows platform
	#define SW_TEMPLATES_REQUIRE_SOURCE						///< Defined if the compiler requires the source includes

	// Set up the various types used internally
	#define SW_USE_WINDOWS_THREADS
	#define SW_USE_WINDOWS_FILES
	#define SW_USE_WINDOWS_FOLDERS
	#define SW_USE_WINDOWS_DLLS
	#define SW_USE_WINDOWS_SEMAPHORES
	#define SW_USE_EMULATED_RWLOCKS
	#define SW_USE_WINDOWS_MUTEXES

	#if defined(_MSC_VER) && _MSC_VER < 1300
		#define SW_CONST_INT64(n)	(n##L)
		#define SW_CONST_UINT64(n)	(n##UL)
	#endif

	#define SW_ATTRIBUTE_TLS	__declspec(thread)
#elif defined(linux)

	#define SW_PLATFORM_UNIX								///< Defined when building for a unix-derived platform
	#define SW_PLATFORM_LINUX								///< Defined when building for a linux platform
	#define SW_TEMPLATES_REQUIRE_SOURCE						///< Defined if the compiler requires the source includes

	// Set up the various types used internally
	#define SW_USE_POSIX_THREADS
	#define SW_USE_UNIX_FILES
	#define SW_USE_UNIX_64BIT_FILES
	#define SW_USE_UNIX_FOLDERS
	#define SW_USE_UNIX_DLLS
	#define SW_USE_POSIX_SEMAPHORES
	#define SW_USE_POSIX_RWLOCKS
	#define SW_USE_POSIX_MUTEXES

	#define SW_ATTRIBUTE_TLS	__thread
#elif defined(sun)

	#define SW_PLATFORM_UNIX								///< Defined when building for a unix-derived platform
	#define SW_PLATFORM_SUN									///< Defined when building for a sun platform (SunOS/Solaris)
	#define SW_PLATFORM_SOLARIS								///< Defined when building for a Solaris platform (SunOS/Solaris)
	#define SW_TEMPLATES_REQUIRE_SOURCE						///< Defined if the compiler requires the source includes

	// Set up the various types used internally
	#define SW_USE_SOLARIS_THREADS
	#define SW_USE_UNIX_FILES
	#define SW_USE_UNIX_64BIT_FILES
	#define SW_USE_UNIX_TELL
	#define SW_USE_UNIX_FOLDERS
	#define SW_USE_UNIX_DLLS
	#define SW_USE_POSIX_SEMAPHORES
	#define SW_USE_SOLARIS_RWLOCKS
	#define SW_USE_SOLARIS_MUTEXES

	#define SW_ATTRIBUTE_TLS	__thread
#elif defined(__hpux)

	#define SW_PLATFORM_UNIX								///< Defined when building for a unix-derived platform
	#define SW_PLATFORM_HPUX								///< Defined when building for a HPUX platform
	#define SW_TEMPLATES_REQUIRE_SOURCE						///< Defined if the compiler requires the source includes

	// Set up the various types used internally
	#define SW_USE_POSIX_THREADS
	#define SW_USE_UNIX_FILES
	#define SW_USE_UNIX_64BIT_FILES
	#define SW_USE_UNIX_FOLDERS
	#define SW_USE_UNIX_DLLS
	#define SW_USE_POSIX_SEMAPHORES
	#define SW_USE_POSIX_RWLOCKS
	#define SW_USE_POSIX_MUTEXES

	#define SW_ATTRIBUTE_TLS	__thread
#elif defined(__osf__)

	#define SW_PLATFORM_UNIX								///< Defined when building for a unix-derived platform
	#define SW_PLATFORM_OSF1								///< Defined when building for a OSF1 (Tru64) platform
	#define SW_TEMPLATES_REQUIRE_SOURCE						///< Defined if the compiler requires the source includes

	// Set up the various types used internally
	#define SW_USE_POSIX_THREADS
	#define SW_USE_UNIX_FILES
	#define SW_USE_UNIX_64BIT_FILES
	#define SW_USE_UNIX_FOLDERS
	#define SW_USE_UNIX_DLLS
	#define SW_USE_POSIX_SEMAPHORES
	#define SW_USE_POSIX_RWLOCKS
	#define SW_USE_POSIX_MUTEXES

	#define SW_ATTRIBUTE_TLS	__thread
#elif defined(_AIX)

	#define SW_PLATFORM_UNIX								///< Defined when building for a unix-derived platform
	#define SW_PLATFORM_AIX									///< Defined when building for an AIX platform
	#define SW_TEMPLATES_REQUIRE_SOURCE						///< Defined if the compiler requires the source includes

	// Set up the various types used internally
	#define SW_USE_POSIX_THREADS
	#define SW_USE_POSIX_THREADS_LACKS_YIELD
	#define SW_USE_UNIX_FILES
	#define SW_USE_UNIX_64BIT_FILES
	#define SW_USE_UNIX_FOLDERS
	#define SW_USE_UNIX_DLLS
	#define SW_USE_POSIX_SEMAPHORES
	#define SW_USE_POSIX_RWLOCKS
	#define SW_USE_POSIX_MUTEXES

	#define SW_ATTRIBUTE_TLS	__thread
#elif defined(__MACH__)

	#define SW_PLATFORM_UNIX								///< Defined when building for a unix-derived platform
	#define SW_PLATFORM_MACH								///< Defined when building for a Mac platform
	#define SW_TEMPLATES_REQUIRE_SOURCE						///< Defined if the compiler requires the source includes

	// Set up the various types used internally
	#define SW_USE_POSIX_THREADS
	#define SW_USE_UNIX_FILES
	#define SW_USE_UNIX_FOLDERS
	#define SW_USE_UNIX_DLLS
	#define SW_USE_SYSV_SEMAPHORES
	#define SW_USE_POSIX_RWLOCKS
	#define SW_USE_POSIX_MUTEXES
	#define SW_USE_LOCAL_POSIX_TIMEDLOCK

	#define SW_ATTRIBUTE_TLS	thread_local
#else

	#error Unknown platform - please update sw_platform.h

#endif


#if defined(sparc) || defined(_POWER)

	#define	SW_PLATFORM_INT64_IS_HIGH32_LOW32				///< Defined if 64-bit integers on the platform are in the order (high32, low32).
	#define SW_PLATFORM_NATIVE_DOUBLE_IN_IEEE_FORMAT
	#define SW_PLATFORM_NATIVE_FLOAT_IN_IEEE_FORMAT

#elif defined(_M_AMD64)

	#define	SW_PLATFORM_INT64_IS_LOW32_HIGH32				///< Defined if 64-bit integers on the platform are in the order (low32, high32).
	#define SW_PLATFORM_NATIVE_DOUBLE_IN_IEEE_FORMAT
	#define SW_PLATFORM_NATIVE_FLOAT_IN_IEEE_FORMAT

#elif defined(_M_IX86) || defined(i386) || defined(__i386) || defined(__alpha__) || defined(__alpha) || defined(_M_ALPHA) || defined(__MACH__) || defined(__arm__)

	#define	SW_PLATFORM_INT64_IS_LOW32_HIGH32				///< Defined if 64-bit integers on the platform are in the order (low32, high32).
	#define SW_PLATFORM_NATIVE_DOUBLE_IN_IEEE_FORMAT
	#define SW_PLATFORM_NATIVE_FLOAT_IN_IEEE_FORMAT

#elif defined(linux) && defined(__LP64__)
	
	// REDHAT gcc does not give us machine processor so assume AMD64
	#define	SW_PLATFORM_INT64_IS_LOW32_HIGH32				///< Defined if 64-bit integers on the platform are in the order (low32, high32).
	#define SW_PLATFORM_NATIVE_DOUBLE_IN_IEEE_FORMAT
	#define SW_PLATFORM_NATIVE_FLOAT_IN_IEEE_FORMAT

#else

	#error Unknown processor architecture - please update sw_platform.h

#endif


#if !defined(SW_BUILD_DEBUG) && !defined(SW_BUILD_RELEASE)
	#if defined(_DEBUG)
		#define SW_BUILD_DEBUG									///< Defined for a debug build
	#else
		#define SW_BUILD_RELEASE								///< Defined for a release build
	#endif
#endif



/*************************************************************
** CONST macros (defaults)
*************************************************************/
#ifndef SW_CONST_INT8
	#define SW_CONST_INT8(n)	(n)
#endif

#ifndef SW_CONST_UINT8
	#define SW_CONST_UINT8(n)	(n##U)
#endif

#ifndef SW_CONST_INT16
	#define SW_CONST_INT16(n)	(n)
#endif

#ifndef SW_CONST_UINT16
	#define SW_CONST_UINT16(n)	(n##U)
#endif

#ifndef SW_CONST_INT32
	#define SW_CONST_INT32(n)	(n)
#endif

#ifndef SW_CONST_UINT32
	#define SW_CONST_UINT32(n)	(n##U)
#endif

#ifndef SW_CONST_INT64
	#define SW_CONST_INT64(n)	(n##LL)
#endif

#ifndef SW_CONST_UINT64
	#define SW_CONST_UINT64(n)	(n##ULL)
#endif

#ifndef SW_CONST_LONG
	#define SW_CONST_LONG(n)	(n##L)
#endif

#ifndef SW_CONST_ULONG
	#define SW_CONST_ULONG(n)	(n##UL)
#endif


#endif // __xplatform_sw_platform_h__
