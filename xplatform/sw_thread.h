/****************************************************************************
**	sw_thread.h		Define platform independent handles as used by this library
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __xplatform_sw_thread_h__
#define __xplatform_sw_thread_h__

#include <xplatform/xplatform.h>

#ifdef sun
// Solaris 6/7/8
#include <synch.h>
#include <thread.h>
#include <poll.h>

typedef thread_t			sw_thread_id_t;

#elif defined(WIN32) || defined(_WIN32)
#include <process.h>
typedef DWORD				sw_thread_id_t;

#else
// Assume POSIX
#include <pthread.h>

typedef pthread_t			sw_thread_id_t;

#endif

#ifdef __cplusplus
extern "C" {
#endif

typedef sw_handle_t       sw_thread_t;
typedef int (*sw_threadproc_t) (void *);

/// Specify a normal thread
#define SW_THREAD_TYPE_NORMAL		0x00

/// Specify a detached thread
#define SW_THREAD_TYPE_DETACHED		0x01

/// Specify a daemon thread
#define SW_THREAD_TYPE_DAEMON		0x02

/// Thread is unbound to a LWP (may be ignored by O/S)
#define SW_THREAD_TYPE_UNBOUND		0x00

/// Thread is bound to a LWP (may be ignored by O/S)
#define SW_THREAD_TYPE_BOUND		0x04

/// Thread is started when sw_thread_create is called
#define SW_THREAD_TYPE_START		0x00

/// Thread is NOT started when sw_thread_create is called
#define SW_THREAD_TYPE_NOSTART		0x08

/// Thread type mask - can be used to extract the type info from the flags
#define SW_THREAD_TYPE_MASK			0x07


// Thread priorities

/// The maximum thread priority value
#define SW_THREAD_PRIORITY_MAX		127

/// The minimum thread priority value
#define SW_THREAD_PRIORITY_MIN		-SW_THREAD_PRIORITY_MAX

/// The normal (default) thread priority value
#define SW_THREAD_PRIORITY_NORMAL	0


// Internal thread flags
#define SW_THREAD_FLAG_RUN		0x00010000	///< The thread can continue to run
#define SW_THREAD_FLAG_STARTED	0x00020000	///< The thread has started
#define SW_THREAD_FLAG_STOPPED	0x00040000	///< The thread has stopped
#define SW_THREAD_FLAG_FUNCTION	0x00080000	///< Indicates a function-style thread
#define SW_THREAD_FLAG_DERIVED	0x00100000	///< Indicates a dervied-style thread
#define SW_THREAD_FLAG_VALID	0x00200000	///< Indicates a valid thread
#define SW_THREAD_FLAG_JOINED	0x00400000	///< Indicates a a join has been done on this thread



XPLATFORM_DLL_EXPORT	sw_thread_id_t	sw_thread_self();
XPLATFORM_DLL_EXPORT	sw_status_t		sw_thread_yield();
XPLATFORM_DLL_EXPORT	void			sw_thread_sleep(int ms);
XPLATFORM_DLL_EXPORT	sw_thread_t		sw_thread_create(sw_threadproc_t func, void *param);
XPLATFORM_DLL_EXPORT	sw_thread_t		sw_thread_create_ex(sw_threadproc_t tproc, void *tparam, int flags, int stacksize);
XPLATFORM_DLL_EXPORT	sw_status_t		sw_thread_destroy(sw_thread_t hThread);
XPLATFORM_DLL_EXPORT	sw_status_t		sw_thread_suspend(sw_thread_t hThread);
XPLATFORM_DLL_EXPORT	sw_status_t		sw_thread_resume(sw_thread_t hThread);

XPLATFORM_DLL_EXPORT	sw_status_t		sw_thread_waitUntilStopped(sw_thread_t hThread);
XPLATFORM_DLL_EXPORT	sw_status_t		sw_thread_waitUntilStarted(sw_thread_t hThread);
XPLATFORM_DLL_EXPORT	sw_status_t		sw_thread_getExitCode(sw_thread_t hThread, int *pExitCode);

XPLATFORM_DLL_EXPORT	sw_status_t		sw_thread_getThreadId(sw_thread_t hThread, sw_thread_id_t *pThreadId);

XPLATFORM_DLL_EXPORT	sw_bool_t			sw_thread_started(sw_thread_t hThread);
XPLATFORM_DLL_EXPORT	sw_bool_t			sw_thread_stopped(sw_thread_t hThread);
XPLATFORM_DLL_EXPORT	sw_bool_t			sw_thread_running(sw_thread_t hThread);

XPLATFORM_DLL_EXPORT	sw_status_t		sw_thread_setPriority(sw_thread_t hThread, int priority);
XPLATFORM_DLL_EXPORT	sw_status_t		sw_thread_getPriority(sw_thread_t hThread, int *priority);

XPLATFORM_DLL_EXPORT	void			sw_thread_setConcurrency(int level);
XPLATFORM_DLL_EXPORT	int				sw_thread_getConcurrency();

#ifdef __cplusplus
}
#endif

#endif // __xplatform_sw_thread_h__
