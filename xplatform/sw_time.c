/****************************************************************************
**      sw_time.c	A set of time and date functions which are platform
**					independent
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"


#include <xplatform/sw_time.h>

XPLATFORM_DLL_EXPORT sw_status_t
sw_gettimeofday(sw_timeval_t *tv)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (tv != NULL)
			{
#if defined(SW_PLATFORM_WINDOWS)
			FILETIME	ft;
			sw_int64_t	t;

			/*
			SYSTEMTIME	st;
			
			GetSystemTime(&st);
			SystemTimeToFileTime(&st, &ft);
			*/
			GetSystemTimeAsFileTime(&ft);
			
			t = (((sw_int64_t)ft.dwHighDateTime << 32) | (sw_int64_t)ft.dwLowDateTime) - 116444736000000000;
			tv->tv_sec = (int)(t / 10000000);
			tv->tv_usec = (int)((t % 10000000) / 10);
#else
			struct timeval	tmp;
			int				r;
			
			r = gettimeofday(&tmp, NULL);
			if (r == 0)
				{
				tv->tv_sec = tmp.tv_sec;
				tv->tv_usec = tmp.tv_usec;
				}
			else
				{
				res = errno;
				}
#endif
			}
		else
			{
			res = SW_STATUS_INVALID_POINTER;
			}

		return res;
		}




XPLATFORM_DLL_EXPORT double
sw_getDoubleTime()
		{
		sw_timeval_t	tv;

		sw_gettimeofday(&tv);

		return (double)tv.tv_sec + ((double)tv.tv_usec / 1000000.0);
		}


#ifdef NEVER

static int		sw_time_localTimeDiff=0; 
static int		sw_time_dstTimeDiff=3600;
static sw_bool_t	sw_time_initDone=false;


/*************************************************************************************************************************
Mathematicians and programmers have naturally interested themselves in mathematical and computational
algorithms to convert between Julian day numbers and Gregorian dates. The following conversion algorithm
is due to Henry F. Fliegel and Thomas C. Van Flandern: 

Days are integer values in the range 1-31, months are integers in the range 1-12, and years are positive or
negative integers. Division is to be understood as in integer arithmetic, with remainders discarded.

This algorithm is valid only in the Gregorian Calendar and the proleptic Gregorian Calendar (for non-negative
Julian day numbers). It does not correctly convert dates in the Julian Calendar.
*************************************************************************************************************************/

// The Julian day (jd) is computed from Gregorian day, month and year (d, m, y) as follows:
XPLATFORM_DLL_EXPORT int
sw_time_convertDayMonthYearToJulian(int d, int m, int y)
		{
		int jd = ( 1461 * ( y + 4800 + ( m - 14 ) / 12 ) ) / 4 +
			( 367 * ( m - 2 - 12 * ( ( m - 14 ) / 12 ) ) ) / 12 -
			( 3 * ( ( y + 4900 + ( m - 14 ) / 12 ) / 100 ) ) / 4 +
			d - 32075;

		return jd;
		}

// Converting from the Julian day number to the Gregorian date is performed thus:
XPLATFORM_DLL_EXPORT void
sw_time_convertJulianToDayMonthYear(int jd, int *pd, int *pm, int *py)
		{
		int	l, n, j, i, d, m, y;

		l = jd + 68569;
		n = ( 4 * l ) / 146097;
		l = l - ( 146097 * n + 3 ) / 4;
		i = ( 4000 * ( l + 1 ) ) / 1461001;
		l = l - ( 1461 * i ) / 4 + 31;
		j = ( 80 * l ) / 2447;
		d = l - ( 2447 * j ) / 80;
		l = j / 11;
		m = j + 2 - ( 12 * l );
		y = 100 * ( n - 49 ) + i + l;

		*pd = d;
		*pm = m;
		*py = y;
		}


/**
*** Internal function to intialisation values for local time and DST
*** (daylight savings time).
**/
static void
sw_time_init()
		{
		if (!sw_time_initDone)
			{
#if defined(WIN32) || defined(_WIN32)
			TIME_ZONE_INFORMATION	tzi;

			if (GetTimeZoneInformation(&tzi) != TIME_ZONE_ID_UNKNOWN)
				{
				sw_time_localTimeDiff = (tzi.Bias + tzi.StandardBias) * 60;
				sw_time_dstTimeDiff = -tzi.DaylightBias * 60;
				}

#else
			tzset();
			sw_time_localTimeDiff = timezone;
	#ifdef sun
			sw_time_dstTimeDiff = timezone-altzone;
	#else
			// No altzone available, we must work it out
			sw_time_dstTimeDiff = timezone; //-altzone;
	#endif
#endif
			sw_time_initDone = true;
			}
		}


static sw_bool_t
calculateDST(time_t t, int year, int dom, int dow, int mon, int hr, int min)
		{
		sw_bool_t	dst=false;

		if (year >= 1970 && year <= 2038)
			{
			// Use the Unix localtime algorithm
			struct tm	tm, *tp;

			tp = localtime_r(&t, &tm);
			dst = (tp->tm_isdst > 0);
			}
		else
			{
			// Standard not available, used next best guess
			if      (mon >  4  && mon <  10)									dst = true;	// May thru September 
			else if (mon == 4  && dom >  7)										dst = true;	// After first week in April 
			else if (mon == 4  && dom <= 7  && dow == 0 && hr >= 2)				dst = true;	// After 2am on first Sunday ($dow=0) in April 
			else if (mon == 4  && dom <= 7  && dow != 0 && (dom-dow > 0))		dst = true;	// After Sunday of first week in April 
			else if (mon == 10 && dom <  25)									dst = true;	// Before last week of October 
			else if (mon == 10 && dom >= 25 && dow == 0 && hr < 2)				dst = true;	// Before 2am on last Sunday in October 
			else if (mon == 10 && dom >= 25 && dow != 0 && (dom-24-dow < 1) )	dst = true;	// Before Sunday of last week in October 
			}

		return dst;
		}


/**
*** Internal function to adjust the time (in seconds) to compensate
*** for daylight savings.
**/
static uint64
getAdjustedTime(uint64 t, sw_bool_t useLocalTime, sw_bool_t useDST)
		{
		sw_bool_t	inDST;
		int		julian = (int)(t / SECONDS_IN_DAY) + JULIAN_DAY_1ST_JAN_1900;
		time_t	ut = (time_t)(t - SECONDS_1900_UNTIL_1970);
		int		d, m, y;
		int		hour = (int)((t / 3600) % 24);
		int		minute = (int)((t / 60) % 60);
		int		dow = ((julian+1) % 7);

		if (!sw_time_initDone) sw_time_init();

		sw_time_convertJulianToDayMonthYear(julian, &d, &m, &y);

		inDST = calculateDST(ut, y, d, dow, m, hour, minute);

		return t - (useLocalTime?sw_time_localTimeDiff:0) + ((inDST && useDST)?sw_time_dstTimeDiff:0);
		}

/**
*** Initialise the sw_timeval_t structure with the current time.
**/
XPLATFORM_DLL_EXPORT void
sw_time_setFromCurrentTime(sw_timeval_t *pCATV)
		{
		struct timeval	v;

		gettimeofday(&v, NULL);
		sw_time_setFromTimeVal(pCATV, &v);
		}


XPLATFORM_DLL_EXPORT void
sw_time_setFromDayMonthYear(sw_timeval_t *pCATV, int day, int month, int year)
		{
		sw_time_setFromJulian(pCATV, sw_time_convertDayMonthYearToJulian(day, month, year));
		}



/**
*** Set the sw_timeval_t structure by using the given timeval structure.
**/
XPLATFORM_DLL_EXPORT void
sw_time_setFromTimeVal(sw_timeval_t *pCATV, const struct timeval *pTV)
		{
		pCATV->tv_sec = (int64)pTV->tv_sec + SECONDS_1900_UNTIL_1970;
		pCATV->tv_nsec = pTV->tv_usec * 1000;
		}


/**
*** get the sw_timeval_t structure by using the given timeval structure.
**/
XPLATFORM_DLL_EXPORT void
sw_time_getTimeVal(const sw_timeval_t *pCATV, struct timeval *pTV)
		{
		pTV->tv_sec = (uint)(pCATV->tv_sec - SECONDS_1900_UNTIL_1970);
		pTV->tv_usec = pCATV->tv_nsec / 1000;
		}



/**
*** Set the sw_timeval_t structure by using the given unix time value
*** (seconds since 1st Jan 1970)
**/
XPLATFORM_DLL_EXPORT void
sw_time_setFromUnixTime(sw_timeval_t *pCATV, time_t ut)
		{
		pCATV->tv_sec = ut + SECONDS_1900_UNTIL_1970;
		pCATV->tv_nsec = 0;
		}


/**
*** Get the unix time (time_t) from the given sw_timeval_t structure
*** (seconds since 1st Jan 1970)
**/
XPLATFORM_DLL_EXPORT time_t
sw_time_getUnixTime(const sw_timeval_t *pCATV)
		{
		time_t	ut;

		ut = (time_t)(pCATV->tv_sec - SECONDS_1900_UNTIL_1970);

		return ut;
		}


/**
*** Set the sw_timeval_t structure by using the supplied double
**/
XPLATFORM_DLL_EXPORT void
sw_time_setFromDouble(sw_timeval_t *pCATV, double v)
		{
		double	w, f;

		f = modf(v, &w);
		pCATV->tv_sec = (int64)w;
		pCATV->tv_nsec = (uint32)(f * 1000000000.0);
		}


/**
*** Return a double representing the current time. The whole part
*** of the double is the number of seconds, the fraction part is
*** the fractional part of the time from the tv_nsec member.
**/
XPLATFORM_DLL_EXPORT double
sw_time_getAsDouble(const sw_timeval_t *pCATV)
		{
		// The whole part of vt is the number of days since 31 December 1899
		// The fractional part of vt represents the time of day (0.0=midnight, 0.25=6am, 0.5=noon, etc)
		double	v;

		v = (double)pCATV->tv_sec + ((double)pCATV->tv_nsec / 1000000000.0);

		return v;
		}


/**
*** Set the sw_timeval_t structure by using the windows FILETIME value.
**/
XPLATFORM_DLL_EXPORT void
sw_time_setFromWindowsFileTime(sw_timeval_t *pCATV, const FILETIME *ft)
		{
		// t is the number of 100-nanosecond intervals since January 1, 1601
		int64	 t;

		t = (((int64)ft->dwHighDateTime << 32) | (int64)ft->dwLowDateTime) - (SECONDS_1601_UNTIL_1900 * 10000000UL);
		pCATV->tv_sec = t / 10000000UL;
		pCATV->tv_nsec = (uint32)(t % 10000000UL) * 100;
		}



/**
*** Set the sw_timeval_t structure by using the windows FILETIME value.
**/
XPLATFORM_DLL_EXPORT void
sw_time_getWindowsFileTime(const sw_timeval_t *pCATV, FILETIME *ft)
		{
		int64	t;

		t = ((int64)pCATV->tv_sec * 10000000UL) + ((int64)pCATV->tv_nsec / 100UL) + ((uint64)SECONDS_1601_UNTIL_1900 * 10000000UL);

		ft->dwHighDateTime = (u_long)(t >> 32);
		ft->dwLowDateTime = (u_long)(t & 0xffffffff);
		}




/**
*** Set the sw_timeval_t structure by using the windows SYSTEMTIME value.
**/
XPLATFORM_DLL_EXPORT void
sw_time_setFromWindowsSystemTime(sw_timeval_t *pCATV, const SYSTEMTIME *st)
		{
		sw_datetime_t	dt;

		memset(&dt, 0, sizeof(dt));
		dt.dt_year = st->wYear;
		dt.dt_month = st->wMonth;
		dt.dt_mday = st->wDay;
		dt.dt_hour = st->wHour;
		dt.dt_min = st->wMinute;
		dt.dt_sec = st->wSecond;
		dt.dt_nsec = st->wMilliseconds * 1000000;
		dt.dt_wday = st->wDayOfWeek;

		sw_time_setFromDateTime(pCATV, &dt);
		}



/**
*** Set the sw_timeval_t structure by using the windows SYSTEMTIME value.
**/
XPLATFORM_DLL_EXPORT void
sw_time_getWindowsSystemTime(const sw_timeval_t *pCATV, SYSTEMTIME *st)
		{
		sw_datetime_t	dt;

		sw_time_getDateTime(pCATV, &dt);
		st->wYear = dt.dt_year;
		st->wMonth = dt.dt_month;
		st->wDay = dt.dt_mday;
		st->wHour = dt.dt_hour;
		st->wMinute = dt.dt_min;
		st->wSecond = dt.dt_sec;
		st->wMilliseconds = dt.dt_nsec / 1000000;
		st->wDayOfWeek = dt.dt_wday;
		}




/**
*** Set the sw_timeval_t structure by using the windows variant time
**/
XPLATFORM_DLL_EXPORT void
sw_time_setFromWindowsVariantTime(sw_timeval_t *pCATV, double vt)
		{
		// The whole part of vt is the number of days since 31 December 1899
		// The fractional part of vt represents the time of day (0.0=midnight, 0.25=6am, 0.5=noon, etc)
		pCATV->tv_sec = (int64)((vt - 1.0) * SECONDS_IN_DAY);
		pCATV->tv_nsec = 0;
		}


/**
*** Set the sw_timeval_t structure by using the windows variant time
**/
XPLATFORM_DLL_EXPORT double
sw_time_getWindowsVariantTime(const sw_timeval_t *pCATV)
		{
		// The whole part of vt is the number of days since 31 December 1899
		// The fractional part of vt represents the time of day (0.0=midnight, 0.25=6am, 0.5=noon, etc)
		double	vt;

		vt = 1.0 + ((double)pCATV->tv_sec / (double)SECONDS_IN_DAY);

		return vt;
		}


XPLATFORM_DLL_EXPORT void
sw_time_convertWindowsVariantTimeToSystemTime(double vt, SYSTEMTIME *st)
		{
		sw_timeval_t	tv;

		sw_time_setFromWindowsVariantTime(&tv, vt);
		sw_time_getWindowsSystemTime(&tv, st);
		}


XPLATFORM_DLL_EXPORT void
sw_time_convertWindowsSystemTimeToVariantTime(const SYSTEMTIME *st, double *vt)
		{
		sw_timeval_t	tv;

		sw_time_setFromWindowsSystemTime(&tv, st);
		*vt = sw_time_getWindowsVariantTime(&tv);
		}


// Julian Dates
XPLATFORM_DLL_EXPORT void
sw_time_setFromJulian(sw_timeval_t *pCATV, int julian)
		{
		pCATV->tv_sec = (int64)(julian - JULIAN_DAY_1ST_JAN_1900) * SECONDS_IN_DAY;
		pCATV->tv_nsec = 0;
		}


XPLATFORM_DLL_EXPORT int
sw_time_getJulian(const sw_timeval_t *pCATV)
		{
		return (int)(getAdjustedTime(pCATV->tv_sec, true, true) / SECONDS_IN_DAY) + JULIAN_DAY_1ST_JAN_1900;
		}


// Various time components
XPLATFORM_DLL_EXPORT void
sw_time_getHourMinSecDST(const sw_timeval_t *pCATV, int *hour, int *minute, int *second, sw_bool_t *dst)
		{
		uint64	at=getAdjustedTime(pCATV->tv_sec, true, true);

		*hour = (int)((at / 3600) % 24);
		*minute = (int)((at / 60) % 60);
		*second = (int)(at % 60);
		*dst = true;
		}


XPLATFORM_DLL_EXPORT uint32
sw_time_getHour(const sw_timeval_t *pCATV)
		{
		return (uint32)((getAdjustedTime(pCATV->tv_sec, true, true) / 3600) % 24);
		}


XPLATFORM_DLL_EXPORT uint32
sw_time_getMinute(const sw_timeval_t *pCATV)
		{
		return (uint32)((getAdjustedTime(pCATV->tv_sec, true, true) / 60) % 60);
		}


XPLATFORM_DLL_EXPORT uint32
sw_time_getSecond(const sw_timeval_t *pCATV)
		{
		return (uint32)(getAdjustedTime(pCATV->tv_sec, true, true) % 60);
		}


XPLATFORM_DLL_EXPORT uint32
sw_time_getMillisecond(const sw_timeval_t *pCATV)
		{
		return (pCATV->tv_nsec / 1000000);
		}


XPLATFORM_DLL_EXPORT uint32
sw_time_getMicrosecond(const sw_timeval_t *pCATV)
		{
		return (pCATV->tv_nsec / 1000);
		}


XPLATFORM_DLL_EXPORT uint32
sw_time_getNanosecond(const sw_timeval_t *pCATV)
		{
		return (pCATV->tv_nsec);
		}


XPLATFORM_DLL_EXPORT ushort
sw_time_getMonth(const sw_timeval_t *pCATV)
		{
		int		d, m, y;

		sw_time_convertJulianToDayMonthYear(sw_time_getJulian(pCATV), &d, &m, &y);

		return m;
		}



XPLATFORM_DLL_EXPORT ushort
sw_time_getYear(const sw_timeval_t *pCATV)
		{
		int		d, m, y;

		sw_time_convertJulianToDayMonthYear(sw_time_getJulian(pCATV), &d, &m, &y);

		return y;
		}



XPLATFORM_DLL_EXPORT ushort
sw_time_getDayOfWeek(const sw_timeval_t *pCATV)
		{
		return ((sw_time_getJulian(pCATV)+1) % 7);
		}


XPLATFORM_DLL_EXPORT ushort
sw_time_getDayOfMonth(const sw_timeval_t *pCATV)
		{
		int		d, m, y;

		sw_time_convertJulianToDayMonthYear(sw_time_getJulian(pCATV), &d, &m, &y);

		return d;
		}



XPLATFORM_DLL_EXPORT void
sw_time_setFromDateTime(sw_timeval_t *pCATV, const sw_datetime_t *pDT)
		{
		int		julian, adjsec;

		if (pDT->dt_year >= 1970 && pDT->dt_year <= 2038)
			{
			struct tm	tm;
			time_t		t;

			memset(&tm, 0, sizeof(tm));
			tm.tm_sec = pDT->dt_sec;    /* seconds after the minute - [0, 61] */
			tm.tm_min = pDT->dt_min;    /* minutes after the hour - [0, 59] */
			tm.tm_hour = pDT->dt_hour;   /* hour since midnight - [0, 23] */
			tm.tm_mday = pDT->dt_mday;   /* day of the month - [1, 31] */
			tm.tm_mon = pDT->dt_month-1;    /* months since January - [0, 11] */
			tm.tm_year = pDT->dt_year - 1900;   /* years since 1900 */
			tm.tm_isdst = pDT->dt_isdst;

			t = mktime(&tm);
			sw_time_setFromUnixTime(pCATV, t);
			}
		else
			{
			julian = sw_time_convertDayMonthYearToJulian(pDT->dt_mday, pDT->dt_month, pDT->dt_year);
			sw_time_setFromJulian(pCATV, julian);

			
			adjsec = pDT->dt_sec + (pDT->dt_min * 60) + (pDT->dt_hour * 3600);
			pCATV->tv_sec += adjsec;
			pCATV->tv_nsec = pDT->dt_nsec;

			if (calculateDST((time_t)(pCATV->tv_sec - SECONDS_1900_UNTIL_1970), pDT->dt_year, pDT->dt_mday, (julian + 1)%7, pDT->dt_month, pDT->dt_hour, pDT->dt_min))
				{
				pCATV->tv_sec -= sw_time_dstTimeDiff;
			//return t - (useLocalTime?sw_time_localTimeDiff:0) + ((inDST && useDST)?sw_time_dstTimeDiff:0);
				}
			}
		}




XPLATFORM_DLL_EXPORT void
sw_time_getDateTime(const sw_timeval_t *pCATV, sw_datetime_t *pDT)
		{
		int		julian=sw_time_getJulian(pCATV), julianYearStart;
		int		day, month, year;
		int		hour, minute, second;
		sw_bool_t	dst;
		int		yday, dayone, week, firstDayOfWeek1=SW_DAY_SUNDAY;

		sw_time_convertJulianToDayMonthYear(julian, &day, &month, &year);
		julianYearStart = sw_time_convertDayMonthYearToJulian(1, 1, year);

		sw_time_getHourMinSecDST(pCATV, &hour, &minute, &second, &dst);

		// Today's day number (zero-based)
		yday = julian - julianYearStart;

		// The day number for the 1st of the year
		dayone = (julianYearStart+1)%7;

		// Calculate the week number
		// if firstSunday is true the first Sunday in the year is the start of week 1
		// otherwise the first Monday in the year is the start of week 1
		week = yday + dayone - firstDayOfWeek1;
		if (dayone <= firstDayOfWeek1) week += 7;
		week /= 7;

		pDT->dt_year = year;							///< Gregorian Year (e.g 1980)
		pDT->dt_month = month;							///< Month in year (1=Jan - 12=Dec)
		pDT->dt_mday = day;								///< Day of month (1-31)
		pDT->dt_hour = hour;							///< Hour of day (0-23)
		pDT->dt_min = minute;							///< Minute of Hour (0-61)
		pDT->dt_sec = second;							///< Second of Minute (0-61)
		pDT->dt_nsec = pCATV->tv_nsec;					///< Nanoseconds
		pDT->dt_wday = ((julian+1) % 7);				///< Day of week (0=Sunday - 6=Saturday)
		pDT->dt_yday = julian - julianYearStart + 1;	///< Day of year (1-366)
		pDT->dt_week = week;							///< Week of year
		pDT->dt_isdst = calculateDST(sw_time_getUnixTime(pCATV), year, day, pDT->dt_wday, month, hour, minute);
		pDT->dt_local = true;							
		}



/*
From the cftime man page

     %%      same as %
     %a      locale's abbreviated weekday name
     %A      locale's full weekday name
     %b      locale's abbreviated month name
     %B      locale's full month name
     %c      locale's appropriate date and time representation
  Default
     %C      locale's date and time representation as produced by
             date(1)
 Standard-conforming
     %C      century number (the year divided by  100  and  trun-
             cated  to  an  integer  as a decimal number [1,99]);
             single digits are preceded by 0; see standards(5)
     %d      day of month [1,31]; single digits are preceded by 0
     %D      date as %m/%d/%y
     %e      day of month [1,31]; single digits are preceded by a
             space
     %h      locale's abbreviated month name
     %H      hour (24-hour clock) [0,23]; single digits are  pre-
             ceded by 0
     %I      hour (12-hour clock) [1,12]; single digits are  pre-
             ceded by 0
     %j      day number of year [1,366]; single digits  are  pre-
             ceded by 0
     %k      hour (24-hour clock) [0,23]; single digits are  pre-
             ceded by a blank
     %l      hour (12-hour clock) [1,12]; single digits are  pre-
             ceded by a blank
     %m      month number [1,12]; single digits are preceded by 0
     %M      minute [00,59]; leading zero is  permitted  but  not
             required
     %n      insert a newline
     %p      locale's equivalent of either a.m. or p.m.
     %r      appropriate time  representation  in  12-hour  clock
             format with %p
     %R      time as %H:%M
     %S      seconds [00,61]
     %t      insert a tab
     %T      time as %H:%M:%S
     %u      weekday as a decimal number [1,7], with 1 represent-
             ing Sunday
     %U      week number of year as  a  decimal  number  [00,53],
             with Sunday as the first day of week 1
     %V      week number of the year as a decimal number [01,53],
             with  Monday  as  the first day of the week.  If the
             week containing 1 January has four or more  days  in
             the  new  year, then it is considered week 1; other-
             wise, it is week 53 of the previous  year,  and  the
             next week is week 1.
     %w      weekday as a decimal number [0,6], with 0 represent-
             ing Sunday
     %W      week number of year as  a  decimal  number  [00,53],
             with Monday as the first day of week 1
     %x      locale's appropriate date representation
     %X      locale's appropriate time representation
     %y      year within century [00,99]
     %Y      year, including the century (for example 1993)
     %Z      time zone name or abbreviation, or no  bytes  if  no
             time zone information exists


*/


static const wchar_t *ABBR_WEEKDAY_NAME[]	= { L"Sun", L"Mon", L"Tue", L"Wed", L"Thu", L"Fri", L"Sat" };
static const wchar_t *FULL_WEEKDAY_NAME[]	= { L"Sunday", L"Monday", L"Tuesday", L"Wednesday", L"Thursday", L"Friday", L"Saturday" };
static const wchar_t *ABBR_MONTH_NAME[]	= { L"Jan", L"Feb", L"Mar", L"Apr", L"May", L"Jun", L"Jul", L"Aug", L"Sep", L"Oct", L"Nov", L"Dec" };
static const wchar_t *FULL_MONTH_NAME[]	= { L"January", L"February", L"March", L"April", L"May", L"June", L"July", L"August", L"September", L"October", L"November", L"December" };


static void
putstrA(char **buf, int *bufsize, const char *s)
		{
		int		l=strlen(s);

		if (l < *bufsize-1) strcpy(*buf, s);
		else
			{
			l = *bufsize-1;
			if (l > 0) strncpy(*buf, s, l);
			}

		if (l > 0)
			{
			*bufsize -= l;
			*buf += l;
			}
		}


static void
putstrW(wchar_t **buf, int *bufsize, const wchar_t *s)
		{
		int		l=wcslen(s);

		if (l < *bufsize-1) wcscpy(*buf, s);
		else
			{
			l = *bufsize-1;
			if (l > 0) wcsncpy(*buf, s, l);
			}

		if (l > 0)
			{
			*bufsize -= l;
			*buf += l;
			}
		}


XPLATFORM_DLL_EXPORT SW_STATUS
sw_time_formatW(wchar_t *buf, int bufsize, const wchar_t *fmt, const sw_timeval_t *pCATV)
		{
		const wchar_t	*in;
		wchar_t			*out;
		wchar_t			tmp[80];
		sw_datetime_t	dt;

		sw_time_getDateTime(pCATV, &dt);

		in = fmt;
		out = buf;
		while (*in && bufsize > 1)
			{
			if (*in != '%')
				{
				// Simply copy the file.
				*out++ = *in++;
				bufsize--;
				}
			else
				{
				// do the substitution on the %
				in++;

				switch (*in)
					{
					case '%':	// same as %
						putstrW(&out, &bufsize, L"%");
						break;

					case 'a':	// locale's abbreviated weekday name
						putstrW(&out, &bufsize, ABBR_WEEKDAY_NAME[dt.dt_wday]);
						break;

					case 'A':	// locale's full weekday name
						putstrW(&out, &bufsize, FULL_WEEKDAY_NAME[dt.dt_wday]);
						break;

					case 'b':	// locale's abbreviated month name
						putstrW(&out, &bufsize, ABBR_MONTH_NAME[dt.dt_month-1]);
						break;

					case 'B':	// locale's full month name
						putstrW(&out, &bufsize, FULL_MONTH_NAME[dt.dt_month-1]);
						break;

					case 'c':	// locale's appropriate date and time representation
						sw_time_formatW(tmp, sizeof(tmp)/sizeof(tmp[0]), L"%a %b %d %T %Y", pCATV);
						putstrW(&out, &bufsize, tmp);
						break;

					case 'C':	// Default: locale's date and time representation as produced by date(1)
						sw_time_formatW(tmp, sizeof(tmp)/sizeof(tmp[0]), L"%a %b %e %T %Z %Y", pCATV);
						putstrW(&out, &bufsize, tmp);
						break;

					/*
					case 'C':	// Standard-conforming: century number (the year divided by  100  and  trun- cated  to  an  integer  as a decimal number [1,99]); single digits are preceded by 0; see standards(5)
						break;
					*/

					case 'D':	// date as %m/%d/%y
						sw_time_formatW(tmp, sizeof(tmp)/sizeof(tmp[0]), L"%m/%d/%y", pCATV);
						putstrW(&out, &bufsize, tmp);
						break;

					case 'd':	// day of month [1,31]; single digits are preceded by 0
					case 'e':	// day of month [1,31]; single digits are preceded by a space
						//v = calendar.getDayOfMonth();
						sw_swprintf(tmp, *in=='d'?L"%02d":L"%2d", dt.dt_mday);
						putstrW(&out, &bufsize, tmp);
						break;

					case 'g':	// Week-based year within century [00,99].
						sw_swprintf(tmp, L"%02d", dt.dt_year % 100);
						putstrW(&out, &bufsize, tmp);
						break;

					case 'G':	// Week-based year, including the century [0000,9999].
						sw_swprintf(tmp, L"%04d", dt.dt_year % 10000);
						putstrW(&out, &bufsize, tmp);
						break;

					case 'h':	// locale's abbreviated month name
						putstrW(&out, &bufsize, ABBR_MONTH_NAME[dt.dt_month-1]);
						break;

					case 'H':	// hour (24-hour clock) [0,23]; single digits are  preceded by 0
					case 'k':	// hour (24-hour clock) [0,23]; single digits are  preceded by a blank
						//v = getHour();
						sw_swprintf(tmp, *in=='H'?L"%02d":L"%2d", dt.dt_hour);
						putstrW(&out, &bufsize, tmp);
						break;

					case 'I':	// hour (12-hour clock) [1,12]; single digits are  preceded by 0
					case 'l':	// hour (12-hour clock) [1,12]; single digits are  preceded by a blank
						// v = getHour() % 12;
						{
						int	v = dt.dt_hour % 12;

						if (v == 0) v = 12;
						sw_swprintf(tmp, *in=='I'?L"%02d":L"%2d", v);
						putstrW(&out, &bufsize, tmp);
						}
						break;

					case 'j':	// day number of year [1,366]; single digits  are  preceded by 0
						//v = calendar.getDayOfYear();
						sw_swprintf(tmp, L"%03d", dt.dt_yday);
						putstrW(&out, &bufsize, tmp);
						break;

					case 'm':	// month number [1,12]; single digits are preceded by 0
						//v = calendar.getMonth()+1;
						sw_swprintf(tmp, L"%02d", dt.dt_month);
						putstrW(&out, &bufsize, tmp);
						break;

					case 'M':	// minute [00,59]; leading zero is  permitted  but  not required
						// v = getMinute();
						sw_swprintf(tmp, L"%02d", dt.dt_min);
						putstrW(&out, &bufsize, tmp);
						break;

					case 'n':	// insert a newline
						putstrW(&out, &bufsize, L"\n");
						break;

					case 'p':	// locale's equivalent of either a.m. or p.m.
						putstrW(&out, &bufsize, dt.dt_hour<12?L"AM":L"PM");
						break;

					case 'P':	// locale's equivalent of either a.m. or p.m.
						putstrW(&out, &bufsize, dt.dt_hour<12?L"am":L"pm");
						break;

					case 'r':	// appropriate time  representation  in  12-hour  clock format with %p
						sw_time_formatW(tmp, sizeof(tmp)/sizeof(tmp[0]), L"%I:%M:%S %p", pCATV);
						putstrW(&out, &bufsize, tmp);
						break;

					case 'R':	// time as %H:%M
						sw_time_formatW(tmp, sizeof(tmp)/sizeof(tmp[0]), L"%H:%M", pCATV);
						putstrW(&out, &bufsize, tmp);
						break;

					case 'S':	// seconds [00,61]
						// v = getSecond();
						sw_swprintf(tmp, L"%02d", dt.dt_sec);
						putstrW(&out, &bufsize, tmp);
						break;

					case 't':	// insert a tab
						putstrW(&out, &bufsize, L"\t");
						break;

					case 'T':	// time as %H:%M:%S
						sw_time_formatW(tmp, sizeof(tmp)/sizeof(tmp[0]), L"%H:%M:%S", pCATV);
						putstrW(&out, &bufsize, tmp);
						break;

					case 'u':	// weekday as a decimal number [1,7], with 1 represent- ing Sunday
						//v = calendar.getDayOfWeek();
						{
						int	d = dt.dt_wday;

						if (d == 0) d = 7;
						sw_swprintf(tmp, L"%d", d);
						putstrW(&out, &bufsize, tmp);
						}
						break;

					case 'U':	// week number of year as  a  decimal  number  [00,53], with Sunday as the first day of week 1
						//v = calendar.getWeekOfYear();
						sw_swprintf(tmp, L"%02d", dt.dt_week);
						putstrW(&out, &bufsize, tmp);
						break;

					case 'V':	// week number of the year as a decimal number [01,53], with  Monday  as  the first day of the week.  If the week containing 1 January has four or more  days  in the  new  year, then it is considered week 1; other- wise, it is week 53 of the previous  year,  and  the next week is week 1.
						// v = calendar.getWeekOfYear();
						sw_swprintf(tmp, L"%02d", dt.dt_week);
						putstrW(&out, &bufsize, tmp);
						break;

					case 'w':	// weekday as a decimal number [0,6], with 0 represent- ing Sunday
						// v = calendar.getDayOfWeek();
						sw_swprintf(tmp, L"%d", dt.dt_wday);
						putstrW(&out, &bufsize, tmp);
						break;

					case 'W':	// week number of year as  a  decimal  number  [00,53], with Monday as the first day of week 1
						// v = calendar.getWeekOfYear();
						sw_swprintf(tmp, L"%02d", dt.dt_week);
						putstrW(&out, &bufsize, tmp);
						break;

					case 'x':	// locale's appropriate date representation
						sw_time_formatW(tmp, sizeof(tmp)/sizeof(tmp[0]), L"%d/%m/%Y", pCATV);
						putstrW(&out, &bufsize, tmp);
						break;

					case 'X':	// locale's appropriate time representation
						sw_time_formatW(tmp, sizeof(tmp)/sizeof(tmp[0]), L"%H:%M:%S", pCATV);
						putstrW(&out, &bufsize, tmp);
						break;

					case 'y':	// year within century [00,99]
						// v = calendar.getYear() % 100;
						sw_swprintf(tmp, L"%02d", dt.dt_year % 100);
						putstrW(&out, &bufsize, tmp);
						break;

					case 'Y':	// year, including the century (for example 1993)
						sw_swprintf(tmp, L"%d", dt.dt_year);
						putstrW(&out, &bufsize, tmp);
						break;

					case 'Z':	// time zone name or abbreviation, or no  bytes  if  no time zone information exists
						{
						/*
						TimeZone tz = calendar.getTimeZone();

						outstr += tz.getDisplayName(true, TimeZone.SHORT);
						*/
						wchar_t	*tzstr;

						if (dt.dt_isdst >= 0)
							{
							tzstr = sw_string_mallocUnicodeStringA(tzname[dt.dt_isdst>0?1:0]);
							}
						else tzstr = L"GMT";

						putstrW(&out, &bufsize, tzstr);

						if (dt.dt_isdst >= 0) free(tzstr);
						}

						break;

					default:
						// Don't know so keep it!
						tmp[0] = '%';
						tmp[1] = *in;
						tmp[2] = 0;

						putstrW(&out, &bufsize, tmp);
						break;
					}

				if (*in) in++;
				}
			}

		if (bufsize > 0) *out = 0;
		
		return SW_STATUS_SUCCESS;
		}


XPLATFORM_DLL_EXPORT SW_STATUS
sw_time_formatA(char *buf, int bufsize, const char *fmt, const sw_timeval_t *pCATV)
		{
		wchar_t	*wtmp, *wfmt;
		char	*tmp;

		wtmp = (wchar_t *)malloc(bufsize * sizeof(wchar_t));
		wfmt = sw_string_mallocUnicodeStringA(fmt);
		sw_time_formatW(wtmp, bufsize, wfmt, pCATV);

		tmp = sw_string_mallocCharStringW(wtmp);
		putstrA(&buf, &bufsize, tmp);

		free(wfmt);
		free(wtmp);
		free(tmp);

		return SW_STATUS_SUCCESS;
		}
#endif
