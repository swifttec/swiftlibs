/****************************************************************************
**	sw_tls.h		Define platform independent thread local storage
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __xplatform_sw_tls_h__
#define __xplatform_sw_tls_h__


#include <xplatform/sw_thread.h>

typedef sw_handle_t       sw_tls_t;

#ifdef __cplusplus
extern "C" {
#endif

XPLATFORM_DLL_EXPORT	sw_tls_t	sw_tls_create();
XPLATFORM_DLL_EXPORT	sw_status_t	sw_tls_setValue(sw_tls_t hTls, void *data);
XPLATFORM_DLL_EXPORT	sw_status_t	sw_tls_getValue(sw_tls_t hTls, void **data);
XPLATFORM_DLL_EXPORT	sw_status_t	sw_tls_destroy(sw_tls_t hTls);

#ifdef __cplusplus
}
#endif

#endif // __xplatform_sw_tls_h__
