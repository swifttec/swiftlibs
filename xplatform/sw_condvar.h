/**
*** @file
*** @author		Simon Sparkes
***
***  Condional Variables are those on which a thread/process can wait
***  and be woken up (signalled) by another thread/process.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __xplatform_sw_condvar_h__
#define __xplatform_sw_condvar_h__

#include <xplatform/sw_mutex.h>

#ifdef __cplusplus
extern "C" {
#endif

#define SW_CONDVAR_TYPE_UNKNOWN	0x0		///< An unknown condvar type
#define SW_CONDVAR_TYPE_THREAD	0x1		///< A thread condvar type
// #define SW_CONDVAR_TYPE_PROCESS	0x2

typedef sw_handle_t	sw_condvar_t;		///< A handle to a condvar

XPLATFORM_DLL_EXPORT	sw_condvar_t		sw_condvar_create(int type);
XPLATFORM_DLL_EXPORT	sw_condvar_t		sw_condvar_create_ex(int type, sw_mutex_t hMutex);
XPLATFORM_DLL_EXPORT	sw_status_t		sw_condvar_wait(sw_condvar_t hCondVar);
XPLATFORM_DLL_EXPORT	sw_status_t		sw_condvar_timedWait(sw_condvar_t hCondVar, sw_uint64_t waitms);
XPLATFORM_DLL_EXPORT	sw_status_t		sw_condvar_signal(sw_condvar_t hCondVar);
XPLATFORM_DLL_EXPORT	sw_status_t		sw_condvar_broadcast(sw_condvar_t hCondVar);
XPLATFORM_DLL_EXPORT	sw_status_t		sw_condvar_close(sw_condvar_t hCondVar);
XPLATFORM_DLL_EXPORT	sw_status_t		sw_condvar_destroy(sw_condvar_t hCondVar);
XPLATFORM_DLL_EXPORT	sw_bool_t		sw_condvar_isClosed(sw_condvar_t hCondVar);
XPLATFORM_DLL_EXPORT	sw_bool_t		sw_condvar_isValidHandle(sw_condvar_t hCondVar);
XPLATFORM_DLL_EXPORT	sw_mutex_t		sw_condvar_getMutex(sw_condvar_t hCondVar);
XPLATFORM_DLL_EXPORT	sw_status_t		sw_condvar_acquireMutex(sw_condvar_t hCondVar);
XPLATFORM_DLL_EXPORT	sw_status_t		sw_condvar_releaseMutex(sw_condvar_t hCondVar);
XPLATFORM_DLL_EXPORT	sw_bool_t		sw_condvar_mutexHasLock(sw_condvar_t hCondVar);

#ifdef __cplusplus
}
#endif

#endif
