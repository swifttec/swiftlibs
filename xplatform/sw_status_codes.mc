;/**
;*** @file
;*** @brief		Generated status codes
;*** @version	1.0
;*** @author	Simon Sparkes
;***
;*** The file sw_status_codes.h is generated from the message compiler
;*** file sw_status_codes.mc using the utility <b>mc</b>.
;***
;*** <b>Copyright 2006-2018 SwiftTec. All rights reserved.</b>
;**/
;
;#ifndef __sw_status_codes_h__
;#define __sw_status_codes_h__
;

MessageIdTypedef=   sw_status_t

SeverityNames = (	
					Success=0x0			: SW_SEVERITY_SUCCESS
					Info=0x1			: SW_SEVERITY_INFORMATIONAL
					Warning=0x2			: SW_SEVERITY_WARNING
					Error=0x3			: SW_SEVERITY_ERROR
				)

FacilityNames = (	
					System=0x0			: SW_FACILITY_SYSTEM
					SystemError=0xfff	: SW_FACILITY_SYSTEM_ERROR
					General=0x100		: SW_FACILITY_GENERAL
                )

LanguageNames = (
					English=0x0409		: sw_status_ENU
				)




;// General "success" status code - should be zero
MessageId		= 0x0000
Facility		= System
Severity		= Success
SymbolicName	= SW_STATUS_SUCCESS
Language		= English
The operation completed succesfully
.

;// General "error" status code - this should not normally
;// occur except within the libraries. On finding this error
;// the sw_status functions should substitute this with a call
;// to errno, GetLastError, or the platform equivalent.
MessageId		= 0xffff
Facility		= SystemError
Severity		= Error
SymbolicName	= SW_STATUS_SYSTEM_ERROR
Language		= English
The operation reported an error.
.

;// General error codes
MessageId		= 0x1000
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_TIMEOUT
Language		= English
A timeout has occured.
.


MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_INVALID_HANDLE
Language		= English
An invalid handle was used.
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_ALREADY_LOCKED
Language		= English
An attempt was made to lock a resource when
the lock was already held by the calling thread.
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_OUT_OF_MEMORY
Language		= English
A call to allocate memory has failed.
.


MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_THREAD_NOT_STARTED
Language		= English
The specified thread has not started.
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_THREAD_ALREADY_RUNNING
Language		= English
The specified thread is already running.
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_THREAD_NOT_STOPPED
Language		= English
The specified thread has not stopped.
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_NOT_FOUND
Language		= English
The specified object/element was not found
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_BUFFER_TOO_SMALL
Language		= English
The buffer provided was to small to hold all the data available.
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_THREAD_CANNOT_YIELD
Language		= English
The current thread is unable to yield to another thread.
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_NOT_OWNER
Language		= English
The calling thread was not the owner of the element being
operated on.
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_INVALID_POINTER
Language		= English
A pointer passed to the function was invalid.
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_NULL_POINTER
Language		= English
A NULL pointer was passed to the function.
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_NO_MORE_DATA
Language		= English
No more data is available.
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_INVALID_PARAMETER
Language		= English
A parameter passed to the function was invalid.
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_UNSUPPORTED_OPERATION
Language		= English
An unsupported operation was attempted.
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_PROCESS_NOT_RUNNING
Language		= English
An operation was attempted on a non-existent process.
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_ILLEGAL_OPERATION
Language		= English
An operation was attempted on an object that was not permitted
because the object was not in a state that would permit the operation
requested.
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_INVALID_SHARED_MEMORY_SEGMENT
Language		= English
An attempt to attach to a shared memory segment for sub-allocation
failed because the segment existed but was not valid.
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_ALREADY_OPEN
Language		= English
An attempt was made to open a file, folder or other resource
when the resource was already open.
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_INSUFFICIENT_DATA
Language		= English
When performing an operation insufficient data was available to
complete the request.
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_INVALID_DATA
Language		= English
When performing an operation invalid data was encountered.
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_TOO_MUCH_DATA
Language		= English
When performing an operation too much data was available to
complete the request.
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_ALREADY_EXISTS
Language		= English
The specified object/element already exists.
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_CANNOT_UPGRADE
Language		= English
The specified object/element cannot be upgraded.
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_JOB_NOT_EXECUTED
Language		= English
The job has not yet been executed.
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_JOB_RUNNING
Language		= English
The job is currently running.
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_XML_PARSE_ERROR
Language		= English
The XML did not parse correctly
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_CONDVAR_CLOSED
Language		= English
The conditional variable has been closed.
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_MUTEX_NOT_ACQUIRED
Language		= English
The mutex was not acquired
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_NOT_EXECUTABLE
Language		= English
The program/file specified was not executable
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_NO_ACCESS
Language		= English
The program/file specified was not accessible
.

MessageId		= 
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_READONLY
Language		= English
The program/file specified was read-only
.

;// Database error codes
MessageId		= 0x2000
Facility		= General
Severity		= Error
SymbolicName	= SW_STATUS_DB_ERROR
Language		= English
The database action failed
.


MessageId		= 
Facility		= General
Severity		= Success
SymbolicName	= SW_STATUS_DB_TABLE_CREATED
Language		= English
The operation created a database table
.


;#endif // __sw_status_codes_h__
