/**
*** A collection of O/S emulation functions
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#ifndef __xplatform_xplatform_h__
#include <xplatform/xplatform.h>
#endif

#ifndef __xplatform_sw_osfunc_h__
#define __xplatform_sw_osfunc_h__

#ifdef __cplusplus
extern "C" {
#endif

#if defined(SW_PLATFORM_WINDOWS)
	#include <time.h>
	#include <process.h>
	#include <io.h>
	#include <sys/stat.h>

	#define atoi64(x)	_atoi64(x)
	#if (_MSC_VER < 1300)
		#define wtof(x)		watof(x)
	#else
		#define wtof(x)		_wtof(x)
	#endif
	#define wtoi(x)		_wtoi(x)
	#define wtol(x)		_wtol(x)
	#define watoi(x)	_wtoi(x)
	#define watol(x)	_wtol(x)
	#define watoll(x)	_wtoi64(x)
	#define wtoi64(x)	_wtoi64(x)
	#define watoi64(x)	_wtoi64(x)
	#define atoll(x)	_atoi64(x)
	#define wtoll(x)	_wtoi64(x)

	// Has stricmp
	#define strcasecmp	stricmp
	#define strncasecmp	strnicmp
	#define wcscasecmp	wcsicmp
	#define wcsncasecmp	wcsnicmp
#else
	#include <unistd.h>

	#if defined(SW_PLATFORM_SOLARIS)
		#include <widec.h>
	#elif defined(SW_PLATFORM_AIX)
		#include <wstring.h>
	#endif

	#define atoi64(x)	atoll(x)
	#define wtoi(x)		watoi(x)
	#define wtof(x)		watof(x)
	#define wtol(x)		watol(x)
	#define wtoll(x)	watoll(x)
	#define wtoi64(x)	watoll(x)
	#define watoi64(x)	watoll(x)

	// Has strcasecmp
	#define stricmp		strcasecmp
	#define strnicmp	strncasecmp
	#define wcsicmp		wcscasecmp
	#define wcsnicmp	wcsncasecmp

	// No code pages - it's a windwos-only thing, but we need these 2 constants
	#define CP_ACP                    0           // default to ANSI code page
	#define CP_UTF8                   65001       // UTF-8 translation
#endif

#include <stdlib.h>
#include <wchar.h>


XPLATFORM_DLL_EXPORT char *		strCopyString(const char *);
XPLATFORM_DLL_EXPORT char *		strCopyWString(const wchar_t *, int codepage/*=CP_UTF8*/);
XPLATFORM_DLL_EXPORT wchar_t *	wcsCopyString(const char *, int codepage/*=CP_ACP*/);
XPLATFORM_DLL_EXPORT wchar_t *	wcsCopyWString(const wchar_t *);

XPLATFORM_DLL_EXPORT size_t		sw_wcstombs(char *mbstr, const wchar_t *wcstr, size_t count, int codepage/*=CP_UTF8*/);
XPLATFORM_DLL_EXPORT size_t		sw_mbstowcs(wchar_t *wcstr, const char *mbstr, size_t count, int codepage/*=CP_ACP*/);

#if defined(SW_PLATFORM_WINDOWS)
XPLATFORM_DLL_EXPORT struct tm *	localtime_r(time_t *clock, struct tm *tp);
XPLATFORM_DLL_EXPORT struct tm *	gmtime_r(time_t *clock, struct tm *tp);

struct timezone 
	{
	int  tz_minuteswest; /* minutes W of Greenwich */
	int  tz_dsttime;     /* type of dst correction */
	};

XPLATFORM_DLL_EXPORT unsigned int sleep(unsigned int seconds);

#endif

#define SW_UNREFERENCED_PARAMETER(x)	(x)



/*
** The following are an attempt to unify what Microsoft should have done with
** the TCHAR functions. Here all string functions (e.g. strcpy) are prefixed
** with t_ to make life easy, rather than always having to look them up.
*/


#if defined(UNICODE) || defined(_UNICODE)
// -------------------- Start of UNICODE defs -----------------------

#ifndef _T
#define _T(x)	L##x
#endif

#define t_system	_tsystem
#define t_stat		_tstat

#define t_printf	_tprintf
#define t_sprintf	_stprintf
#define t_fprintf	_ftprintf
#define t_vprintf	_vtprintf
#define t_vsprintf	_vstprintf
#define t_vfprintf	_vftprintf

#define t_scanf		_tscanf
#define t_fscanf	_ftscanf
#define t_sscanf	_stscanf

#define t_putchar	_puttchar
#define t_putc		_puttc
#define t_getchar	_gettchar
#define t_getc		_gettc
#define t_fgets		_fgetts
#define t_gets		_getts
#define t_fputs		_fputts

#define t_fopen		_tfopen
#define t_open		_topen
#define t_unlink	_tunlink
#define t_chmod		_tchmod
#define t_access	_taccess
#define t_chdir		_tchdir

#define t_atoi		_ttoi
#define t_atol		_ttol
#define t_atof		_ttof

#define t_getenv	_tgetenv
#define t_mkdir		_tmkdir

#define	t_strcpy	_tcscpy
#define	t_strncpy	_tcsncpy
#define t_strcat	_tcscat
#define t_strncat	_tcsncat
#define	t_strcmp	_tcscmp
#define	t_strncmp	_tcsncmp
#define	t_strlen	_tcslen
#define	t_strspn	_tcsspn
#define	t_strcspn	_tcscspn
#define t_strchr	_tcschr
#define t_strrchr	_tcsrchr
#define t_strcoll	_tcscoll


#define	t_stricmp	_tcsicmp
#define	t_strnicmp	_tcsnicmp
#define	t_strcasecmp	_tcsicmp
#define	t_strncasecmp	_tcsnicmp

#define t_strdup	_tcsdup

#define t_strftime	wcsftime

#define T_EOF		WEOF

#else
// -------------------- Start of non-UNICODE defs -----------------------

#ifndef _T
#define _T(x)	x
#endif

#define t_system	system
#define t_stat		stat

#define t_printf	printf
#define t_sprintf	sprintf
#define t_fprintf	fprintf
#define t_vprintf	vprintf
#define t_vsprintf	vsprintf
#define t_vfprintf	vfprintf

#define t_scanf		scanf
#define t_fscanf	fscanf
#define t_sscanf	sscanf

#define t_putchar	putchar
#define t_putc		putc
#define t_getchar	getchar
#define t_getc		getc
#define t_fgets		fgets
#define t_gets		gets
#define t_fputs		_fputts

#define t_fopen		fopen
#define t_open		open
#define t_unlink	unlink
#define t_chmod		chmod
#define t_access	access
#define t_chdir		chdir

#define t_atoi		atoi
#define t_atol		atol
#define t_atof		atof

#define t_getenv	getenv
#define t_mkdir		mkdir

#define	t_strcpy	strcpy
#define	t_strncpy	strncpy
#define t_strcat	strcat
#define t_strncat	strncat
#define	t_strcmp	strcmp
#define	t_strncmp	strncmp
#define	t_strlen	strlen
#define	t_strspn	strspn
#define	t_strcspn	strcspn
#define t_strchr	strchr
#define t_strrchr	strrchr
#define t_strcoll	strcoll


#define t_strcasecmp	strcasecmp
#define t_strncasecmp	strncasecmp
#define t_stricmp	stricmp
#define t_strnicmp	strnicmp

#define t_strdup	strdup

#define t_strftime	strftime

#define T_EOF		EOF

// -------------------- End of non-UNICODE defs -----------------------
#endif

// Common definitions
#ifndef TEXT
	#define TEXT(x)		_T(x)
#endif

#ifndef _TEXT
	#define _TEXT(x)	_T(x)
#endif



/*
** Other O/S like functions
*/
#if defined(SW_PLATFORM_WINDOWS)
// XPLATFORM_DLL_EXPORT sw_pid_t	getpid();
#endif


// Other useful stuff
#define countof(x)	(sizeof(x) / sizeof(x[0]))

#if defined(WIN32) || defined(_WIN32) || defined(__hpux) || defined(linux) || defined(__MACH__)
	XPLATFORM_DLL_EXPORT double	watof(const wchar_t *s);
#endif

#if defined(linux) || defined(__MACH__)
	XPLATFORM_DLL_EXPORT long		watol(const wchar_t *s);
#endif

#if defined(__hpux) || defined(linux) || defined(__MACH__)
	XPLATFORM_DLL_EXPORT int		watoi(const wchar_t *s);
	XPLATFORM_DLL_EXPORT sw_int64_t	watoll(const wchar_t *ws);
#endif

#if defined(__hpux) || defined(__MACH__)
	XPLATFORM_DLL_EXPORT sw_int64_t	atoll(const char *s);
#endif

#if !defined(WIN32) && !defined(_WIN32)
	#if !defined (linux)
		XPLATFORM_DLL_EXPORT int			wcscasecmp(const wchar_t *s1, const wchar_t *s2);
		XPLATFORM_DLL_EXPORT int			wcsncasecmp(const wchar_t *s1, const wchar_t *s2, size_t n);
		XPLATFORM_DLL_EXPORT wchar_t *	wcsdup(const wchar_t *s);
	#endif
	#if !defined (__SunOS_5_9)
		XPLATFORM_DLL_EXPORT char *		strlwr(char *s);
		XPLATFORM_DLL_EXPORT char *		strupr(char *s);
		XPLATFORM_DLL_EXPORT wchar_t *	wcslwr(wchar_t *s);
		XPLATFORM_DLL_EXPORT wchar_t *	wcsupr(wchar_t *s);
	#endif

	XPLATFORM_DLL_EXPORT char *		gets_s(char *buf, sw_size_t nchars);
#endif


// Time functions
#if defined(WIN32) || defined(_WIN32)

/* Symbolic constants for the "access" routine: */
#ifndef R_OK
	#define R_OK    4       /* Test for Read permission */
#endif

#ifndef W_OK
	#define W_OK    2       /* Test for Write permission */
#endif

#ifndef X_OK
	#define X_OK    1       /* Test for eXecute permission */
#endif

#ifndef F_OK
	#define F_OK    0       /* Test for existence of File */
#endif


#endif

#if defined(WIN32) || defined(_WIN32)
XPLATFORM_DLL_EXPORT struct tm *	localtime_r(time_t *clock, struct tm *tp);
XPLATFORM_DLL_EXPORT struct tm *	gmtime_r(time_t *clock, struct tm *tp);
#endif

#ifdef __cplusplus
}
#endif

#endif // __xplatform_sw_osfunc_h__
