/**
*** @file		sw_status.c
*** @brief		Status code handling
*** @version	1.0
*** @author		Simon Sparkes
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"


#include <xplatform/sw_status.h>
#include <xplatform/sw_tls.h>

// Should be last include
#ifdef SW_DEBUG_NEW
	#define new SW_DEBUG_NEW
#endif

#if defined(SW_PLATFORM_MACH)
	#define USE_SW_TLS
#endif

#if defined(USE_SW_TLS)
static sw_tls_t	hTlsStatus=0;
static sw_tls_t	hTlsErrorString=0;
#else 
SW_ATTRIBUTE_TLS sw_status_t			tls_status=SW_STATUS_SUCCESS;
SW_ATTRIBUTE_TLS char					tls_errorstring[256];
#endif


XPLATFORM_DLL_EXPORT const char *
sw_status_getLastErrorString()
		{
		return sw_status_getErrorStringForStatus(sw_status_getLastError());
		}



XPLATFORM_DLL_EXPORT const char *
sw_status_getErrorStringForStatus(sw_status_t status)
		{
		char	*buf="";
		int		bufsize=256;

#if defined(USE_SW_TLS)
		if (hTlsErrorString == 0)
			{
			hTlsErrorString = sw_tls_create();
			if (hTlsErrorString != 0)
				{
				buf = (char *)malloc(bufsize);
				memset(buf, 0, bufsize);
				sw_tls_setValue(hTlsErrorString, buf);
				}
			else
				{
				// TLS has failed - use a non-thread safe buf as a fallback
				static char	tmpbuf[256];

				buf = tmpbuf;
				}
			}
		else
			{
			void	*data;

			sw_tls_getValue(hTlsErrorString, &data);
			buf = (char *)data;
			}

#else
		buf = tls_errorstring;
		bufsize = sizeof(tls_errorstring);
#endif

		snprintf(buf, bufsize, "%u / 0x%08x", status, status);
		
		return buf;
		}


XPLATFORM_DLL_EXPORT sw_status_t
sw_status_getLastError()
		{
#if defined(USE_SW_TLS)
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (hTlsStatus != 0)
			{
			void	*data;

			sw_tls_getValue(hTlsStatus, &data);
			res = (sw_status_t)data;
			}

		return res;
#else
		return tls_status;
#endif
		}


/// Set and return the last status
XPLATFORM_DLL_EXPORT sw_status_t
sw_status_setLastError(sw_status_t status)
		{
#if defined(USE_SW_TLS)
		if (hTlsStatus == 0)
			{
			hTlsStatus = sw_tls_create();
			}

		if (hTlsStatus != 0)
			{
			sw_tls_setValue(hTlsStatus, (void *)status);
			}

		return status;
#else
		tls_status = status;

		return tls_status;
#endif
		}



XPLATFORM_DLL_EXPORT	sw_status_t
sw_status_getSystemErrorStatus()
		{
#if defined(WIN32) || defined(_WIN32)
		return sw_status_getSystemErrorStatusForCode(GetLastError());
#else
		return sw_status_getSystemErrorStatusForCode(errno);
#endif
		}


XPLATFORM_DLL_EXPORT	sw_status_t
sw_status_getSystemErrorStatusForCode(sw_status_t code)
		{
		// For now return a generic system error
		return code; // SW_STATUS_SYSTEM_ERROR;
		}
