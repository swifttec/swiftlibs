/**
*** @file
*** @author		Simon Sparkes
***
***  Condional Variables are those on which a thread/process can wait
***  and be woken up (signalled) by another thread/process.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"

#include <xplatform/sw_condvar.h>
#include <xplatform/sw_thread.h>
#include <xplatform/sw_status.h>


// Internal function reference to sw_mutex.c
void				sw_mutex_internal_getOwnerAndLockCount(sw_mutex_t hMutex, sw_thread_id_t *pOwner, int *pLockCount);
void				sw_mutex_internal_setOwnerAndLockCount(sw_mutex_t hMutex, sw_thread_id_t owner, int lockcount);
#if !defined(WIN32) && !defined(_WIN32)
#ifdef sun
mutex_t *			sw_mutex_internal_getMutexPtr(sw_mutex_t hMutex);
#else
pthread_mutex_t *	sw_mutex_internal_getMutexPtr(sw_mutex_t hMutex);
#endif
#endif


#include <sys/types.h>
#if !defined(WIN32) && !defined(_WIN32)
#include <unistd.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <errno.h>

typedef struct sw_condvar_internal
		{
		sw_handle_header_t	hdr;		///< The handle common header
		int					type;		///< The type of condvar

		sw_mutex_t			hMutex;		///< A handle to the associated mutex
		sw_bool_t			own_mutex;	///< If true, we provided the mutex and must destroy it.
		sw_bool_t			has_lock;	///< If true, we acquired the mutex and must release it.
		volatile sw_bool_t	closed;		///< A flag to indicate we have been closed
		volatile int		waiting;	///< The number of waiting threads
#if defined(WIN32) || defined(_WIN32)
		// Special stuff for WIN32 which doesn't have condvars
		volatile sw_bool_t	broadcast;	///< A flag to indicate if this wakeup is a broadcast or not
		volatile sw_bool_t	ready;		///< A flag to indicate if we are ready for use
		volatile sw_bool_t	taken;		///< A flag to indicate if the non-broadcast signal has already been taken
		HANDLE				hCondEvent;	///< The event used for condvar simulation
#else
	#ifdef sun
		cond_t				cond;		///< The Solaris condition variable
	#else
		pthread_cond_t		cond;		///< The POSIX condition variable
	#endif
#endif
		}  SWInternalCondVar;



/**
*** Creates a handle to a condvar which can be used in calls to other sw_condvar functions.
*** Currently type must be SW_CONDVAR_TYPE_THREAD
***
*** @brief			Create a condvar
*** @param type		The type of condvar to create
*** @return			If successful a handle to the created condvar, otherwise NULL
*** @see			sw_condvar_create_ex
**/
XPLATFORM_DLL_EXPORT sw_handle_t
sw_condvar_create(int type)
		{
		return sw_condvar_create_ex(type, NULL);
		}

/**
*** Creates a handle to a condvar which can be used in calls to other sw_condvar functions.
*** Currently type must be SW_CONDVAR_TYPE_THREAD
***
*** @param type		The type of condvar to create
*** @param hMutex	A handle to the mutex to associate with this condvar
*** @return			If successful a handle to the created condvar, otherwise NULL
*** @see			sw_condvar_create
**/
XPLATFORM_DLL_EXPORT sw_handle_t
sw_condvar_create_ex(int type, sw_mutex_t hMutex)
		{
		SWInternalCondVar	*pCondVar=NULL;
		sw_bool_t				ownMutex=false;

		if (hMutex == NULL)
			{
			int	mtype = SW_MUTEX_TYPE_THREAD;

#if defined(SW_CONDVAR_TYPE_PROCESS)
			// In case we a process-level condvar support
			if (type == SW_CONDVAR_TYPE_PROCESS) mtype = SW_MUTEX_TYPE_PROCESS;
#endif

			hMutex = sw_mutex_create(mtype, NULL);
			ownMutex = true;
			}

		if (hMutex)
			{
			pCondVar = (SWInternalCondVar *)malloc(sizeof(SWInternalCondVar));
			if (pCondVar != NULL)
				{
				memset(pCondVar, 0, sizeof(SWInternalCondVar));
				pCondVar->hdr.type = SW_HANDLE_TYPE_CONDVAR;
				pCondVar->hdr.size = sizeof(SWInternalCondVar);
				pCondVar->type = type;
				pCondVar->hMutex = hMutex;
				pCondVar->own_mutex = ownMutex;
				pCondVar->has_lock = false;

#if defined(WIN32) || defined(_WIN32)
				// Use a Manual-Reset event object so that PulseEvent
				// wakes up all threads.
				pCondVar->hCondEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
				if (pCondVar->hCondEvent == NULL)
					{
					free(pCondVar);
					pCondVar = NULL;
					//sw_error_setLastError(GetLastError());
					}
#else
				int	r=0;

	#ifdef sun
				r = cond_init(&pCondVar->cond, USYNC_THREAD, NULL);
	#else
				// POSIX
				r = pthread_cond_init(&pCondVar->cond, NULL);
	#endif

				if (r != 0)
					{
					free(pCondVar);
					pCondVar = NULL;
					//sw_error_setLastError(r);
					}
#endif // WIN32
				}

			if (pCondVar != NULL)
				{
				// Complete internal initialisation
#if defined(WIN32) || defined(_WIN32)
				pCondVar->broadcast = false;
				pCondVar->ready = true;
				pCondVar->taken = false;
#endif
				pCondVar->closed = false;
				pCondVar->waiting = 0;
				}
			}

		if (pCondVar == NULL && hMutex != NULL && ownMutex)
			{
			// Destroy the mutex we created.
			sw_mutex_destroy(hMutex);
			}

		return pCondVar;
		}


/**
*** Destroys a condvar.
***
*** @param hMutex	A handle to a condvar previously created using sw_condvar_create or sw_condvar_create_ex
*** @return			If successful a handle to the created condvar, otherwise NULL
*** @see			sw_condvar_create, sw_condvar_create_ex
**/
XPLATFORM_DLL_EXPORT	sw_status_t
sw_condvar_destroy(sw_handle_t hCondVar)
		{
		SWInternalCondVar	*pCondVar=(SWInternalCondVar *)hCondVar;
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (pCondVar != NULL && pCondVar->hdr.type == SW_HANDLE_TYPE_CONDVAR)
			{
			sw_condvar_close(hCondVar);

			// Wait until all threads have exited
			while (pCondVar->waiting > 0)
				{
				sw_thread_yield();
				// printf("waiting = %d\n", pCondVar->waiting);
				}

#if defined(WIN32) || defined(_WIN32)
			CloseHandle(pCondVar->hCondEvent);
			pCondVar->hCondEvent = 0;
#else
	#ifdef sun
			cond_destroy(&pCondVar->cond);
	#else
			pthread_cond_destroy(&pCondVar->cond);
	#endif

			memset(&pCondVar->cond, 0, sizeof(pCondVar->cond));
#endif

			if (pCondVar->own_mutex) sw_mutex_destroy(pCondVar->hMutex);
			else if (sw_mutex_hasLock(pCondVar->hMutex) && pCondVar->has_lock)
				{
				pCondVar->has_lock = false;
				sw_mutex_unlock(pCondVar->hMutex);
				}

			memset(pCondVar, 0, sizeof(SWInternalCondVar));
			free(pCondVar);
			res = SW_STATUS_SUCCESS;
			}
		else res = SW_STATUS_INVALID_HANDLE;

		return sw_status_setLastError(res);
		}


#if defined(_WIN32) || defined(WIN32)
/**
*** Internal function used to wait until all the threads have woken up
*** and the event has been set.
***
*** This is only needed for platforms without condvar support in the O/S
**/
static void
sw_condvar_waitUntilReady(SWInternalCondVar *pCondVar)
		{
		int lockCount;

		// Wait until ready
		while (!pCondVar->ready)
			{
			lockCount = sw_mutex_forceUnlock(pCondVar->hMutex);
			sw_thread_yield();
			sw_mutex_restoreLock(pCondVar->hMutex, lockCount);
			}
		}
#endif


/**
*** Internal function to signal one or all threads waiting on this condition
***
*** NOTE:	No checks on state are done
***       	The mutex must be acquired before entering
***			pCondVar is assumed to be valid.
**/
static void 
sw_condvar_wakeup(SWInternalCondVar *pCondVar, sw_bool_t all)
		{
#if defined(_WIN32) || defined(WIN32)
		sw_condvar_waitUntilReady(pCondVar);
	
		if (pCondVar->waiting)
			{
			pCondVar->ready = false;
			pCondVar->broadcast = all;
			pCondVar->taken = false;
			SetEvent(pCondVar->hCondEvent);
			}
#else
	#ifdef sun
		// Solaris
		if (all) cond_broadcast(&pCondVar->cond);
		else cond_signal(&pCondVar->cond);
	#else
		// POSIX
		if (all) pthread_cond_broadcast(&pCondVar->cond);
		else pthread_cond_signal(&pCondVar->cond);
	#endif
#endif
		}


// Close this var
XPLATFORM_DLL_EXPORT	sw_status_t
sw_condvar_close(sw_handle_t hCondVar)
		{
		SWInternalCondVar	*pCondVar=(SWInternalCondVar *)hCondVar;
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (pCondVar != NULL && pCondVar->hdr.type == SW_HANDLE_TYPE_CONDVAR)
			{
			if (!pCondVar->closed)
				{
				sw_bool_t	hasLock=false;

				hasLock = sw_mutex_hasLock(pCondVar->hMutex);
				if (!hasLock)
					{
					sw_mutex_lock(pCondVar->hMutex);
					pCondVar->has_lock = true;
					}

				pCondVar->closed = true;
				sw_condvar_wakeup(pCondVar, true);

#if defined(WIN32) || defined(_WIN32)
				pCondVar->ready = false;
#endif

				if (!hasLock)
					{
					pCondVar->has_lock = false;
					sw_mutex_unlock(pCondVar->hMutex);
					}

				res = SW_STATUS_SUCCESS;
				}
			}
		else res = SW_STATUS_INVALID_HANDLE;

		return sw_status_setLastError(res);
		}


XPLATFORM_DLL_EXPORT	sw_status_t
sw_condvar_wait(sw_handle_t hCondVar)
		{
		return sw_condvar_timedWait(hCondVar, SW_TIMEOUT_INFINITE);
		}


/**
*** Wait for a condition to occur on our variable
***
*** @param	waitms	The time in millisecons to wait before timing out.
***
*** @return	0=timeout, <0=error, >0 success
**/
XPLATFORM_DLL_EXPORT	sw_status_t
sw_condvar_timedWait(sw_handle_t hCondVar, sw_uint64_t waitms)
		{
		SWInternalCondVar	*pCondVar=(SWInternalCondVar *)hCondVar;
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (pCondVar != NULL && pCondVar->hdr.type == SW_HANDLE_TYPE_CONDVAR)
			{
			sw_bool_t	hasLock=false;
			sw_bool_t		timedWait=false;
			int			lockCount = 0;
			int			r=0;

			// Check to see if we want a timed wait. If < 0 then no
			if (waitms != SW_TIMEOUT_INFINITE) timedWait = true;

			// Make sure we have the mutex
			hasLock = sw_mutex_hasLock(pCondVar->hMutex);
			if (!hasLock)
				{
				sw_mutex_lock(pCondVar->hMutex);
				pCondVar->has_lock = true;
				}

			// Check our state before continuing
			if (pCondVar->closed)
				{
				if (!hasLock)
					{
					pCondVar->has_lock = false;
					sw_mutex_unlock(pCondVar->hMutex);
					}
				
				return SW_STATUS_CONDVAR_CLOSED;
				}

#if defined(WIN32) || defined(_WIN32)
			{
			DWORD	to = INFINITE;

			if (timedWait) to = (DWORD)waitms;

			for (;;)
				{
				sw_condvar_waitUntilReady(pCondVar);

				// Check the closed state again.
				if (pCondVar->closed)
					{
					if (!hasLock)
						{
						pCondVar->has_lock = false;
						sw_mutex_unlock(pCondVar->hMutex);
						}

					return SW_STATUS_CONDVAR_CLOSED;
					}

				pCondVar->waiting++;
				lockCount = sw_mutex_forceUnlock(pCondVar->hMutex);
				//printf("[%d]  waiting with timeout=%d\n", sw_thread_self(), to);
				r = WaitForSingleObject(pCondVar->hCondEvent, to);
				switch (r)
					{
					case WAIT_TIMEOUT:	res = SW_STATUS_TIMEOUT;				break;
					case WAIT_OBJECT_0:	res = SW_STATUS_SUCCESS;				break;
					default:			res = sw_status_getSystemErrorStatus();	break;
					}

				sw_mutex_restoreLock(pCondVar->hMutex, lockCount);
				pCondVar->waiting--;
				//printf("[%d]  closed=%d  waiting=%d\n", sw_thread_self(), pCondVar->closed, pCondVar->waiting);

				if (pCondVar->closed)
					{
					if (!hasLock)
						{
						pCondVar->has_lock = false;
						sw_mutex_unlock(pCondVar->hMutex);
						}

					return SW_STATUS_CONDVAR_CLOSED;
					}

				if (pCondVar->waiting == 0)
					{
					// We are the last one
					pCondVar->ready = true;
					ResetEvent(pCondVar->hCondEvent);
					}
				
				// If this was a broadcast all drop out of the loop
				// otherwise we only drop out if we are the taker of the signal
				// All other thread go back to the top!
				if (pCondVar->broadcast || !pCondVar->taken || res == SW_STATUS_TIMEOUT) break;
				}

			if (res != SW_STATUS_TIMEOUT) pCondVar->taken = true;
			}
#else
			{
			// Unix variations
			//
			// Life is more complicated here as condvar is supported by the 
			// unix O/S. We must make an internal call into the sw_mutex functions
			// to reset the lock information before we call the unic condvar wait
			// functions as they automatically clear the associated mutex.
			// When the wait exits we will have regained the mutex and must restore
			// the information.
			//
			// We have to do this because we have implemented recursive mutexes
			// which are not native to the O/S
	#ifdef sun
			mutex_t			*pMutex;
	#else
			pthread_mutex_t	*pMutex;
	#endif

	#ifndef MILLISEC
		#define MILLISEC        1000
	#endif
	#ifndef MICROSEC
		#define MICROSEC        1000000
	#endif
	#ifndef NANOSEC
		#define NANOSEC         1000000000
	#endif
			pCondVar->waiting++;

			// Save and clear the internal mutex structures
			sw_mutex_internal_getOwnerAndLockCount(pCondVar->hMutex, 0, &lockCount);
			sw_mutex_internal_setOwnerAndLockCount(pCondVar->hMutex, 0, 0);

			// Get a pointer to the real mutex
			pMutex = sw_mutex_internal_getMutexPtr(pCondVar->hMutex);

			if (timedWait)
				{
				// fill timeout structure
		#if defined(_AIX) || defined(linux) || defined(__MACH__)
				struct timespec	to;
		#else
				timestruc_t		to;
		#endif
				struct timeval	tv;

				gettimeofday(&tv, NULL);

				to.tv_sec = tv.tv_sec + waitms / 1000;
				waitms = waitms % 1000;
				tv.tv_usec += waitms * 1000;
				if (tv.tv_usec > MICROSEC)
					{
					to.tv_sec++;
					tv.tv_usec -= MICROSEC;
					}

				to.tv_nsec = tv.tv_usec * 1000;

		#ifdef sun
				// Solaris
				r = cond_timedwait(&pCondVar->cond, pMutex, &to);
		#else
				// POSIX
				r = pthread_cond_timedwait(&pCondVar->cond, pMutex, &to);
		#endif
				}
			else
				{
				// Infinite wait
		#ifdef sun
				r = cond_wait(&pCondVar->cond, pMutex);
		#else
				// POSIX
				r = pthread_cond_wait(&pCondVar->cond, pMutex);
		#endif
				}

			pCondVar->waiting--;

			if (r != 0)
				{
				if (r == ETIME || r == ETIMEDOUT) res = SW_STATUS_TIMEOUT;
				else res = sw_status_getSystemErrorStatus();
				}
			else res = SW_STATUS_SUCCESS;
			}
	#endif

			// Restore the internal mutex structures
			sw_mutex_internal_setOwnerAndLockCount(pCondVar->hMutex, sw_thread_self(), lockCount);

			if (pCondVar->closed)
				{
				if (!hasLock)
					{
					pCondVar->has_lock = false;
					sw_mutex_unlock(pCondVar->hMutex);
					}

				return SW_STATUS_CONDVAR_CLOSED;
				}

			// Release the mutex
			if (!hasLock)
				{
				pCondVar->has_lock = false;
				sw_mutex_unlock(pCondVar->hMutex);
				}
			}
		else res = SW_STATUS_INVALID_HANDLE;

		return sw_status_setLastError(res);
		}



// Signal NEXT thread waiting on this condition
XPLATFORM_DLL_EXPORT	sw_status_t
sw_condvar_signal(sw_handle_t hCondVar)
		{
		SWInternalCondVar	*pCondVar=(SWInternalCondVar *)hCondVar;
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (pCondVar != NULL && pCondVar->hdr.type == SW_HANDLE_TYPE_CONDVAR)
			{
			sw_bool_t	hasLock=false;

			hasLock = sw_mutex_hasLock(pCondVar->hMutex);
			if (!hasLock)
				{
				sw_mutex_lock(pCondVar->hMutex);
				pCondVar->has_lock = true;
				}
			if (pCondVar->closed)
				{
				if (!hasLock)
					{
					pCondVar->has_lock = false;
					sw_mutex_unlock(pCondVar->hMutex);
					}

				return sw_status_setLastError(SW_STATUS_CONDVAR_CLOSED);
				}
			sw_condvar_wakeup(pCondVar, false);
			if (!hasLock)
				{
				pCondVar->has_lock = false;
				sw_mutex_unlock(pCondVar->hMutex);
				}
			res = SW_STATUS_SUCCESS;
			}
		else res = SW_STATUS_INVALID_HANDLE;

		return sw_status_setLastError(res);
		}


// Signal ALL threads waiting on this condition
XPLATFORM_DLL_EXPORT	sw_status_t
sw_condvar_broadcast(sw_handle_t hCondVar)
		{
		SWInternalCondVar	*pCondVar=(SWInternalCondVar *)hCondVar;
		sw_status_t			res=SW_STATUS_SUCCESS;

		if (pCondVar != NULL && pCondVar->hdr.type == SW_HANDLE_TYPE_CONDVAR)
			{
			sw_bool_t	hasLock=false;

			hasLock = sw_mutex_hasLock(pCondVar->hMutex);
			if (!hasLock)
				{
				sw_mutex_lock(pCondVar->hMutex);
				pCondVar->has_lock = true;
				}

			if (pCondVar->closed)
				{
				if (!hasLock)
					{
					pCondVar->has_lock = false;
					sw_mutex_unlock(pCondVar->hMutex);
					}

				return sw_status_setLastError(SW_STATUS_CONDVAR_CLOSED);
				}
			sw_condvar_wakeup(pCondVar, true);
			if (!hasLock)
				{
				pCondVar->has_lock = false;
				sw_mutex_unlock(pCondVar->hMutex);
				}

			res = SW_STATUS_SUCCESS;
			}
		else res = SW_STATUS_INVALID_HANDLE;

		return sw_status_setLastError(res);
		}


XPLATFORM_DLL_EXPORT	sw_bool_t
sw_condvar_isClosed(sw_handle_t hCondVar)
		{
		SWInternalCondVar	*pCondVar=(SWInternalCondVar *)hCondVar;
		sw_bool_t			res=true;

		if (pCondVar != NULL && pCondVar->hdr.type == SW_HANDLE_TYPE_CONDVAR)
			{
			res = pCondVar->closed;
			}

		return res;
		}


XPLATFORM_DLL_EXPORT	sw_bool_t
sw_condvar_isValidHandle(sw_handle_t hCondVar)
		{
		SWInternalCondVar	*pCondVar=(SWInternalCondVar *)hCondVar;
		sw_bool_t			res=false;

		if (pCondVar != NULL && pCondVar->hdr.type == SW_HANDLE_TYPE_CONDVAR)
			{
			res = true;
			}

		return res;
		}


XPLATFORM_DLL_EXPORT	sw_mutex_t
sw_condvar_getMutex(sw_handle_t hCondVar)
		{
		SWInternalCondVar	*pCondVar=(SWInternalCondVar *)hCondVar;
		sw_mutex_t			res=0;

		if (pCondVar != NULL && pCondVar->hdr.type == SW_HANDLE_TYPE_CONDVAR)
			{
			res = pCondVar->hMutex;
			}

		return res;
		}


XPLATFORM_DLL_EXPORT sw_status_t
sw_condvar_acquireMutex(sw_handle_t hCondVar)
		{
		SWInternalCondVar	*pCondVar=(SWInternalCondVar *)hCondVar;
		sw_status_t			res=0;

		if (pCondVar != NULL && pCondVar->hdr.type == SW_HANDLE_TYPE_CONDVAR)
			{
			res = sw_mutex_lock(pCondVar->hMutex);
			pCondVar->has_lock = true;
			}

		return res;
		}


XPLATFORM_DLL_EXPORT sw_status_t
sw_condvar_releaseMutex(sw_handle_t hCondVar)
		{
		SWInternalCondVar	*pCondVar=(SWInternalCondVar *)hCondVar;
		sw_status_t			res=0;

		if (pCondVar != NULL && pCondVar->hdr.type == SW_HANDLE_TYPE_CONDVAR)
			{
			pCondVar->has_lock = false;
			res = sw_mutex_unlock(pCondVar->hMutex);
			}

		return res;
		}


XPLATFORM_DLL_EXPORT sw_bool_t
sw_condvar_mutexHasLock(sw_handle_t hCondVar)
		{
		SWInternalCondVar	*pCondVar=(SWInternalCondVar *)hCondVar;
		sw_bool_t			res=false;

		if (pCondVar != NULL && pCondVar->hdr.type == SW_HANDLE_TYPE_CONDVAR)
			{
			res = sw_mutex_hasLock(pCondVar->hMutex);
			}

		return res;
		}


