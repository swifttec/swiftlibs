/****************************************************************************
** sw_internal_thread.h	Common definitions for thread functions
**
**		FOR INTERNAL BASE LIBRARY USE ONLY
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __xplatform_sw_internal_thread_h__
#define __xplatform_sw_internal_thread_h__

#include <xplatform/sw_types.h>
#include <xplatform/sw_handle.h>
#include <xplatform/sw_thread.h>

#include <errno.h>

typedef struct sw_thread_internal
		{
		sw_handle_header_t	hdr;			///< The handle common header
		int					flags;			///< The thread creation flags
		int					stacksize;		///< The thread stack size
		sw_thread_id_t		tid;			///< The thread ID (only valid when running)
		sw_threadproc_t		tproc;			///< The thread function to call at startup
		void				*tparam;		///< The param to pass to the thread function
		bool				started;		///< True if the thread has started
		bool				stopped;		///< True if the thread has stopped
		bool				joined;			///< True if the thread has been joined
		int					exitcode;		///< The exit code as returned by the thread.
		int					suspendcount;	///< The number of times this thread has been suspended
#if defined(WIN32) || defined(_WIN32)
		// Windows Thread Specific Members
		HANDLE				handle;		///< The thread handle
#else
	// Unix/Linux
	#ifdef sun
		// Solaris Threads Specific Members
	#else // sun
		// POSIX Threads Specific Members
		pthread_attr_t		attr;		///< POSIX thread attributes
	#endif // sun
#endif // WIN32
		}  SWInternalThread;


#endif // __xplatform_sw_internal_thread_h__
