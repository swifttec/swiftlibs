/**
*** @file		sw_status.h
*** @brief		Status code handling
*** @version	1.0
*** @author		Simon Sparkes
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#ifndef __xplatform_sw_status_h__
#define __xplatform_sw_status_h__

#include <xplatform/xplatform.h>

#ifdef __cplusplus
extern "C" {
#endif

/// Get the last error code
XPLATFORM_DLL_EXPORT	sw_status_t		sw_status_getLastError();

/// Set and return the last status
XPLATFORM_DLL_EXPORT	sw_status_t		sw_status_setLastError(sw_status_t status);

/// Get the last system error status
XPLATFORM_DLL_EXPORT	sw_status_t		sw_status_getSystemErrorStatus();

/// Get the last system error status for the given code
XPLATFORM_DLL_EXPORT	sw_status_t		sw_status_getSystemErrorStatusForCode(sw_status_t code);

/// Get the last error code
XPLATFORM_DLL_EXPORT	const char *	sw_status_getLastErrorString();

/// Get the last error code
XPLATFORM_DLL_EXPORT	const char *	sw_status_getErrorStringForStatus(sw_status_t status);

#ifdef __cplusplus
}
#endif

#endif // __xplatform_sw_status_h__
