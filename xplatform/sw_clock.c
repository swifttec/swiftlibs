/****************************************************************************
**      sw_time.c	A set of time and date functions which are platform
**					independent
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"


#include <xplatform/sw_clock.h>

#if defined(SW_PLATFORM_MACH)
	#include <mach/clock.h>
	#include <mach/mach.h>
#endif


XPLATFORM_DLL_EXPORT sw_clockms_t
sw_clock_getCurrentTimeMS()
		{
		sw_clockms_t	res=0;

#if defined(SW_PLATFORM_WINDOWS)
		res = GetTickCount64();
#elif defined(SW_PLATFORM_MACH)
		clock_serv_t	cclock;
		mach_timespec_t	mts;

		host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
		clock_get_time(cclock, &mts);
		mach_port_deallocate(mach_task_self(), cclock);
		res = (mts.tv_sec * 1000) + (mts.tv_nsec / 1000000);
#else
		struct timespec	ts;

		clock_gettime(CLOCK_MONOTONIC, &ts);

		res = (ts.tv_sec * 1000) + (ts.tv_nsec / 1000000);
#endif

		return res;
		}


XPLATFORM_DLL_EXPORT double
sw_clock_getCurrentTimeAsDouble()
		{
		double	res=0.0;

#if defined(SW_PLATFORM_WINDOWS)
		res = (double)GetTickCount64() / 1000.0;
#elif defined(SW_PLATFORM_MACH)
		clock_serv_t	cclock;
		mach_timespec_t	mts;

		host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
		clock_get_time(cclock, &mts);
		mach_port_deallocate(mach_task_self(), cclock);
		res = (double)mts.tv_sec + ((double)mts.tv_nsec / 1000000000.0);
#else
		struct timespec	ts;

		clock_gettime(CLOCK_MONOTONIC, &ts);

		res = (double)ts.tv_sec + ((double)ts.tv_nsec / 1000000000.0);
#endif

		return res;
		}
