/****************************************************************************
**	sw_process.c		Define platform independent handles as used by this library
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"


#include <xplatform/sw_status.h>
#include <xplatform/sw_string.h>

#if defined(SW_PLATFORM_WINDOWS)
	#include <psapi.h>
#endif

#undef ENABLE_DEBUG

/**
***	 Return the ID of the currently running process.
**/
XPLATFORM_DLL_EXPORT sw_pid_t
sw_process_self()
		{
		sw_pid_t	res=0;

#if defined(SW_PLATFORM_WINDOWS)
		res = (sw_pid_t)GetCurrentProcessId();
#else
		res = (sw_pid_t)getpid();
#endif

		return res;
		}


XPLATFORM_DLL_EXPORT sw_bool_t
sw_process_isrunning(sw_pid_t pid)
		{
		sw_bool_t	active=false;

#if defined(SW_PLATFORM_WINDOWS)
		HANDLE hProcess = OpenProcess(SYNCHRONIZE, FALSE, pid);

		if (hProcess != NULL)
			{
			DWORD	ret=WaitForSingleObject(hProcess, 0);
			
			CloseHandle(hProcess);
			active = (ret == WAIT_TIMEOUT);
			}
		else
			{
			DWORD	errcode=GetLastError();

			if (errcode == ERROR_ACCESS_DENIED)
				{
				active = true;
				}
			}
#else
		int		r;

		r = kill(pid, 0);
		active = (r == 0 || errno == EPERM);
#endif

		return active;
		}


XPLATFORM_DLL_EXPORT sw_bool_t
sw_process_getname(sw_pid_t pid, char *buf, int bufsize)
		{
		sw_bool_t	res=false;
		char		tmpbuf[1024];

		*buf = 0;

#if defined(SW_PLATFORM_WINDOWS)
		HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION, FALSE, pid);

		if (hProcess == NULL)
			{
			hProcess = OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, FALSE, pid);
			}

		if (hProcess != NULL)
			{
			if (GetModuleFileNameExA(hProcess, 0, tmpbuf, 1024))
				{
				// At this point, buffer contains the full path to the executable
				res = true;
				strbufcpy(buf, tmpbuf, bufsize);
				}

			CloseHandle(hProcess);
			}
#else
		sprintf(tmpbuf, "/proc/%d/cmdline", pid);
		
		FILE	*f=fopen(tmpbuf, "r");
			
		if (f != NULL)
			{
			size_t size;
			
			size = fread(tmpbuf, sizeof(char), 1024, f);
			if (size > 0)
				{
				res = true;
				if (tmpbuf[size-1] == '\n')
					{
					tmpbuf[size-1] = '\0';
					}
				strbufcpy(buf, tmpbuf, bufsize);
				}
			fclose(f);
			}
#endif

		return res;
		}

XPLATFORM_DLL_EXPORT sw_bool_t
sw_process_isConsoleApp()
		{
		bool	res=true;

		// Non-windows apps are always attached to console or terminal.
		// so only need the test for windows
#if defined(SW_PLATFORM_WINDOWS)
		CONSOLE_SCREEN_BUFFER_INFO csbi;

		if (!GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi))
			{
			res = false;
			}
#endif
		
		return res;
		}

