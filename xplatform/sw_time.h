/****************************************************************************
**      sw_time.h	A set of time and date functions which are platform
**					independent
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#ifndef __xplatform_sw_time_h__
#define __xplatform_sw_time_h__

#include <xplatform/xplatform.h>
#include <xplatform/sw_types.h>

#include <time.h>

#ifdef __cplusplus
extern "C" {
#endif

// Special dates as Julian day numbers
#define JULIAN_DAY_1ST_JAN_1601		2305814		///< The julian number for 1st Jan 1601
#define JULIAN_DAY_31ST_DEC_1899	2415020		///< The julian number for 31st Dec 1899
#define JULIAN_DAY_1ST_JAN_1900		2415021		///< The julian number for 1st Jan 1900
#define JULIAN_DAY_1ST_JAN_1970		2440588		///< The julian number for 1st Jan 1970


/// The number of seconds between zero in SWTime and zero in Unix time
#define SECONDS_SWTIME_UNTIL_UNIXTIME	 SW_CONST_ULONG(2209075200)

/// The number of seconds between zero in windows FILETIME and zero in SWTime
#define SECONDS_FILETIME_UNTIL_SWTIME	 SW_CONST_ULONG(9435398400)

#define SECONDS_IN_DAY		86400				///< The number of seconds in a day
#define SECONDS_IN_HOUR		3600				///< The number of seconds in an hour


// Special time periods in seconds used in calculations between various time formats
// on different platforms
#if (defined(WIN32) || defined(_WIN32)) && (_MSC_VER < 1300)
	#define SECONDS_1900_UNTIL_1970	 2208988800UL
	#define SECONDS_1601_UNTIL_1900	 9435484800UL
	#define SECONDS_1601_UNTIL_1970	11644473600UL
#else
	#define SECONDS_1900_UNTIL_1970	 2208988800ULL
	#define SECONDS_1601_UNTIL_1900	 9435484800ULL
	#define SECONDS_1601_UNTIL_1970	11644473600ULL
#endif

// Just plain useful!
#define SECONDS_IN_DAY		86400
#define SECONDS_IN_HOUR		3600

// Day Numbers
#define SW_DAY_SUNDAY		0
#define SW_DAY_MONDAY		1
#define SW_DAY_TUESDAY		2
#define SW_DAY_WEDNESDAY	3
#define SW_DAY_THURSDAY		4
#define SW_DAY_FRIDAY		5
#define SW_DAY_SATURDAY		6

// Month Numbers
#define SW_MONTH_JANUARY	1
#define SW_MONTH_FEBRUARY	2
#define SW_MONTH_MARCH		3
#define SW_MONTH_APRIL		4
#define SW_MONTH_MAY		5
#define SW_MONTH_JUNE		6
#define SW_MONTH_JULY		7
#define SW_MONTH_AUGUST		8
#define SW_MONTH_SEPTEMBER	9
#define SW_MONTH_OCTOBER	10
#define SW_MONTH_NOVEMBER	11
#define SW_MONTH_DECEMBER	12

typedef struct sw_datetime
		{
		sw_uint16_t	dt_year;		///< Gregorian Year (e.g 1980)
		sw_uint16_t	dt_month;		///< Month in year (1=Jan - 12=Dec)
		sw_uint16_t	dt_mday;		///< Day of month (1-31)
		sw_uint16_t	dt_hour;		///< Hour of day (0-23)
		sw_uint16_t	dt_min;			///< Minute of Hour (0-61)
		sw_uint16_t	dt_sec;			///< Second of Minute (0-61)
		sw_uint32_t	dt_nsec;		///< Nanoseconds
		sw_uint16_t	dt_wday;		///< Day of week (0=Sunday - 6=Saturday)
		sw_uint16_t	dt_yday;		///< Day of year (1-366)
		sw_uint16_t	dt_week;		///< Week of year (0-53) where Sunday is considered the first day of week 1
		int			dt_isdst;		///< Is in Daylight Savings in effect (0=no >0=yes <0 not available)
		sw_bool_t	dt_local;		///< Is local time.
		} SW_DATETIME;

XPLATFORM_DLL_EXPORT sw_status_t	sw_gettimeofday(sw_timeval_t *tv);
XPLATFORM_DLL_EXPORT double			sw_getDoubleTime();

#ifdef __cplusplus
}
#endif

#endif // __xplatform_sw_time_h__
