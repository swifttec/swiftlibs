/****************************************************************************
**	sw_handle.h		Define platform independent handles for use by the 
**					other libraries
****************************************************************************/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#ifndef __xplatform_sw_handle_h__
#define __xplatform_sw_handle_h__


/**
*** A generic type used for all handles. This type may be used directly
*** or cast into an internal struct such as the one below
**/
typedef void *			sw_handle_t;

/**
*** A sw_handle is an obscured type which has a common header
*** as defined below. The idea is that any function which uses a handle
*** can check it is the right type and then cast the sw_handle_t to the
*** structure
**/
typedef struct sw_handle_header
		{
		short	type;		///< An integer specifying the handle type
		short	size;		///< The size of the handle
		} sw_handle_header_t;


// A list of the known handle types
#define SW_HANDLE_TYPE_UNKNOWN				0
#define SW_HANDLE_TYPE_MUTEX				1
#define SW_HANDLE_TYPE_THREAD				2
#define SW_HANDLE_TYPE_CONDVAR				3
#define SW_HANDLE_TYPE_TLS					4
#define SW_HANDLE_TYPE_RESOURCE_MODULE		5

#endif // __xplatform_sw_handle_h__
