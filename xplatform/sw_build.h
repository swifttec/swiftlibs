/**
*** @file
*** @brief		Defines SW_BUILD macros
*** @author		Simon Sparkes
***
*** Contains the definitions of the various SW_BUILD macros used by the 
*** library and any derived software.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#ifndef __xplatform_sw_build_h__
#define __xplatform_sw_build_h__

// Useful for determining which model we are in.
#undef SW_BUILD_MULTITHREADED
#undef SW_BUILD_SINGLETHREADED
#if defined(_MT) || defined(_REENTRANT)
	#define SW_BUILD_MULTITHREADED
#else
	#define SW_BUILD_SINGLETHREADED
#endif

// Character mode - unicode or mcbs
#undef SW_BUILD_MCBS
#undef SW_BUILD_UNICODE
#if !defined(SW_BUILD_MCBS) && !defined(SW_BUILD_UNICODE)
	#if defined(UNICODE) || defined(_UNICODE)
		#define SW_BUILD_UNICODE
	#else
		#define SW_BUILD_MCBS
	#endif
#endif

// Built-in runtime typing
#undef SW_BUILD_RTTI
#if defined(_CPPRTTI) || defined(__GXX_RTTI) || defined(__GNUC__)
	#define SW_BUILD_RTTI
#endif

// Built-in runtime typing
#undef SW_BUILD_NATIVE_WCHAR
#if defined(_NATIVE_WCHAR_T_DEFINED) || defined(__GNUC__)
	#define SW_BUILD_NATIVE_WCHAR
#endif

// Static or shared
#undef SW_BUILD_STATIC
#undef SW_BUILD_SHARED
#if defined(_LIB)
	#define SW_BUILD_STATIC
#else
	#define SW_BUILD_SHARED
#endif

// MFC
#undef SW_BUILD_MFC
#if defined(__AFX_H__) || defined(_AFXDLL) || defined(AFXAPI)
	#define SW_BUILD_MFC
#endif

#if defined(_DEBUG) && !defined(SW_BUILD_DEBUG)
	#define SW_BUILD_DEBUG
#endif


// Build the buildtype suffix
#if defined(SW_BUILD_MULTITHREADED)
	#define SW_BUILDSTRING_THREAD	"t"
	#ifndef SW_BUILDSTRING_UNDERSCORE
		#define SW_BUILDSTRING_UNDERSCORE "_"
	#endif
#else
	#define SW_BUILDSTRING_THREAD	""
#endif

#if defined(SW_BUILD_DEBUG)
	#define SW_BUILDSTRING_DEBUG	"d"
	#ifndef SW_BUILDSTRING_UNDERSCORE
		#define SW_BUILDSTRING_UNDERSCORE "_"
	#endif
#else
	#define SW_BUILDSTRING_DEBUG	""
#endif

#if defined(SW_BUILD_MFC)
	#define SW_BUILDSTRING_MFC	"m"
	#ifndef SW_BUILDSTRING_UNDERSCORE
		#define SW_BUILDSTRING_UNDERSCORE "_"
	#endif
#else
	#define SW_BUILDSTRING_MFC	""
#endif

#if defined(SW_BUILD_UNICODE)
	#define SW_BUILDSTRING_CHARTYPE	"u"
	#ifndef SW_BUILDSTRING_UNDERSCORE
		#define SW_BUILDSTRING_UNDERSCORE "_"
	#endif
#else
	#define SW_BUILDSTRING_CHARTYPE	""
#endif

#if defined(SW_BUILD_STATIC)
	#define SW_BUILDSTRING_STATIC	"s"
	#ifndef SW_BUILDSTRING_UNDERSCORE
		#define SW_BUILDSTRING_UNDERSCORE "_"
	#endif
#else
	#define SW_BUILDSTRING_STATIC	""
#endif

#ifndef SW_BUILDSTRING_UNDERSCORE
	#define SW_BUILDSTRING_UNDERSCORE ""
#endif


/// Define the standard buildtype string
#define SW_BUILDTYPE_STRING SW_BUILDSTRING_UNDERSCORE SW_BUILDSTRING_CHARTYPE SW_BUILDSTRING_MFC SW_BUILDSTRING_THREAD SW_BUILDSTRING_DEBUG SW_BUILDSTRING_STATIC

#endif // __xplatform_sw_build_h__
