
/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"

#include <config/config.h>

#ifdef SW_CONFIG_USE_MYSQL

// Swift includes
#include <swift/SWTokenizer.h>
#include <swift/SWStringArray.h>
#include <swift/SWFileInfo.h>
#include <swift/SWCollectionIterator.h>
#include <swift/SWGuard.h>
#include <swift/SWLocalTime.h>
#include <swift/SWInterlockedInt32.h>
#include <swift/SWFolder.h>
#include <swift/SWFilename.h>



// DB includes
#include <db/DBMySqlDatabase.h>
#include <db/DBRecord.h>
#include <db/DBDatabase.h>

#if defined(SW_PLATFORM_WINDOWS)
	#include <Shlobj.h>
	
	// Copied from winsock.h
	typedef UINT_PTR        SOCKET;
#endif

// For MySQL objects
#include <my_global.h>
#include <mysql.h>

// Disable warning about deprecated objects
#pragma warning(disable: 4995)


static SWInterlockedInt32	mySqlInitCount(0);

SWLoadableModule	DBMySqlDatabase::m_dll;

// Loaded functions
#define MYSQL_FUNCTION_LIST \
		MYSQLFUNC(mysql_init); \
		MYSQLFUNC(mysql_options); \
		MYSQLFUNC(mysql_close); \
		MYSQLFUNC(mysql_real_connect); \
		MYSQLFUNC(mysql_real_escape_string); \
		MYSQLFUNC(mysql_errno); \
		MYSQLFUNC(mysql_query); \
		MYSQLFUNC(mysql_real_query); \
		MYSQLFUNC(mysql_store_result); \
		MYSQLFUNC(mysql_num_fields); \
		MYSQLFUNC(mysql_num_rows); \
		MYSQLFUNC(mysql_fetch_field_direct); \
		MYSQLFUNC(mysql_fetch_lengths); \
		MYSQLFUNC(mysql_fetch_row); \
		MYSQLFUNC(mysql_free_result);

typedef MYSQL * (STDCALL *func_mysql_init) (MYSQL *mysql);
typedef void (STDCALL *func_mysql_close) (MYSQL *mysql);
typedef unsigned int (STDCALL *func_mysql_errno) (MYSQL *mysql);
typedef MYSQL *	(STDCALL *func_mysql_real_connect) (MYSQL *mysql, const char *host,
					   const char *user,
					   const char *passwd,
					   const char *db,
					   unsigned int port,
					   const char *unix_socket,
					   unsigned long clientflag);
typedef int		(STDCALL *func_mysql_query)(MYSQL *mysql, const char *q);
typedef int		(STDCALL *func_mysql_real_query)(MYSQL *mysql, const char *q, unsigned long length);
typedef MYSQL_RES *     (STDCALL *func_mysql_store_result)(MYSQL *mysql);
typedef MYSQL_ROW	(STDCALL *func_mysql_fetch_row)(MYSQL_RES *result);
typedef unsigned long * (STDCALL *func_mysql_fetch_lengths)(MYSQL_RES *result);
typedef MYSQL_FIELD *	(STDCALL *func_mysql_fetch_field)(MYSQL_RES *result);
typedef void		(STDCALL *func_mysql_free_result)(MYSQL_RES *result);
typedef unsigned int (STDCALL *func_mysql_num_fields)(MYSQL_RES *res);
typedef unsigned int (STDCALL *func_mysql_num_rows)(MYSQL_RES *res);
typedef void		(STDCALL *func_mysql_free_result)(MYSQL_RES *result);
typedef MYSQL_FIELD * (STDCALL *func_mysql_fetch_field_direct)(MYSQL_RES *res, unsigned int fieldnr);
typedef unsigned long (STDCALL *func_mysql_real_escape_string)(MYSQL *mysql, char *to,const char *from, unsigned long length);
typedef int		(STDCALL *func_mysql_options)(MYSQL *mysql,enum mysql_option option, const char *arg);



#define MYSQLFUNC(x) func_##x				fn_##x
MYSQL_FUNCTION_LIST
#undef MYSQLFUNC


// Internal (hidden) helper classes

class MySqlRow
		{
friend class MySqlResult;

public:
		MySqlRow();
		~MySqlRow();

		int				getFieldCount() const	{ return m_nFields; }
		const char *	getField(int idx) const;
		
protected:
		void	*m_pData;
		int		m_nFields;
		};


MySqlRow::MySqlRow()
		{
		}


MySqlRow::~MySqlRow()
		{
		}


const char *
MySqlRow::getField(int idx) const
		{
		return ((MYSQL_ROW)m_pData)[idx];
		}


class MySqlResult
		{
friend class MySqlDB;

public:
		MySqlResult();
		~MySqlResult();
		
		bool				isOpen() const	{ return (m_pData != NULL); }
		bool				close();
		bool				nextRow(MySqlRow &row);
		
		int					getFieldCount() const		{ return m_nFields; }
		const SWString &	getFieldName(int idx) const	{ return m_fieldNames.get(idx); }

protected:
		void			*m_pData;
		int				m_nFields;
		SWStringArray	m_fieldNames;
		};

MySqlResult::MySqlResult() :
	m_pData(NULL),
	m_nFields(0)
		{
		}


MySqlResult::~MySqlResult()
		{
		close();
		}


bool
MySqlResult::close()
		{
		SWGuard	guard(mySqlInitCount);
		if (m_pData != NULL)
			{
			fn_mysql_free_result((MYSQL_RES *)m_pData);
			m_pData = NULL;
			m_nFields = 0;
			m_fieldNames.RemoveAll();
			}
		
		return true;
		}
		

bool
MySqlResult::nextRow(MySqlRow &row)
		{		
		SWGuard	guard(mySqlInitCount);
		bool	res=false;
		
		if (m_pData != NULL)
			{
			MYSQL_ROW	r;
			
			r = fn_mysql_fetch_row((MYSQL_RES *)m_pData);
			if (r != NULL)
				{
				res = true;
				row.m_pData = r;
				row.m_nFields = m_nFields;
				}
			else
				{
				row.m_pData = NULL;
				row.m_nFields = 0;
				}
			}
		
		return res;
		}

class MySqlDB
		{
public:
		MySqlDB(DBMySqlDatabase *pOwner, MYSQL *pDB);
		~MySqlDB();

		sw_status_t	execute(const char *sql);
		sw_status_t	execute(const char *sql, MySqlResult &result);
		
		bool	isOpen() const	{ return m_pDB != NULL; }

private:
		MYSQL				*m_pDB;
		DBMySqlDatabase		*m_pOwner;
		};

MySqlDB::MySqlDB(DBMySqlDatabase *pOwner, MYSQL *pDB) :
	m_pOwner(pOwner),
	m_pDB(pDB)
		{
		}


MySqlDB::~MySqlDB()
		{
		}


sw_status_t
MySqlDB::execute(const char *sql, MySqlResult &result)
		{
		SWGuard	guard(mySqlInitCount);
		int	r;
		
		r = fn_mysql_query(m_pDB, sql);
		if (r != 0)
			{
			int		suberr=fn_mysql_errno(m_pDB);

			if (suberr == 2006 || suberr == 2013)
				{
				// Try a reconnect
				if (m_pOwner->reconnect())
					{
					m_pDB = (MYSQL*)(m_pOwner->getMySQLDB());
					r = fn_mysql_query(m_pDB, sql);
					}
				}
			}

		if (r == 0)
			{
			if (result.isOpen()) result.close();
			
			MYSQL_RES	*pResult;

			pResult = fn_mysql_store_result(m_pDB);
			if (pResult != NULL && fn_mysql_num_rows(pResult) > 0)
				{
				result.m_pData = pResult;
				result.m_nFields = fn_mysql_num_fields(pResult);
				result.m_fieldNames.RemoveAll();
				
				for (int i=0;i<result.m_nFields;i++)
					{
					MYSQL_FIELD	*pField=fn_mysql_fetch_field_direct(pResult, i);
					
					result.m_fieldNames.Add(pField->name);
					}
				}
			else
				{
				r = -1;
				}
			}
		
		return r;
		}


sw_status_t
MySqlDB::execute(const char *sql)
		{
		SWGuard	guard(mySqlInitCount);
		int		r;
		
		r = fn_mysql_query(m_pDB, sql);
		if (r != 0)
			{
			int		suberr=fn_mysql_errno(m_pDB);

			if (suberr == 2006 || suberr == 2013)
				{
				// Try a reconnect
				if (m_pOwner->reconnect())
					{
					m_pDB = (MYSQL*)(m_pOwner->getMySQLDB());
					r = fn_mysql_query(m_pDB, sql);
					}
				}
			}

		
		return r;
		}



#define MYPDB	((MYSQL *)m_pDB)

DBMySqlDatabase::DBMySqlDatabase() :
	m_pDB(NULL)
		{
		SWGuard	guard(mySqlInitCount);

		if (mySqlInitCount == 0)
			{
			startMySQL();
			}

		mySqlInitCount++;
		}


DBMySqlDatabase::~DBMySqlDatabase()
		{
		// Make sure the database is actually closed
		close();

		SWGuard	guard(mySqlInitCount);

		if (--mySqlInitCount == 0)
			{
			stopMySQL();
			}
		}


bool
DBMySqlDatabase::startMySQL()
		{
		bool	res=false;

#if defined(SW_PLATFORM_WINDOWS)
		if (!res)
			{
			// Try to locate the dll as it is not in the path
			TCHAR		path[MAX_PATH];
			SWString	foldername, filename;
			SWFolder	folder;
			SWFileInfo	entry;
			
			SHGetFolderPath(NULL, CSIDL_PROGRAM_FILES, NULL, SHGFP_TYPE_CURRENT, path);
			foldername = SWFilename::concatPaths(path, "MySQL");

			if (folder.open(foldername) == SW_STATUS_SUCCESS)
				{
				while (!res && folder.read(entry) == SW_STATUS_SUCCESS)
					{
					SWString	name=entry.getName();
			
					// Always ignore . and ..
					if (name == "." || name == "..") continue;

					if (entry.isFolder())
						{
						filename = SWFilename::concatPaths(entry.getFullPathname(), "bin/libmysql.dll");
						if (SWFileInfo::isFile(filename))
							{
							res = (m_dll.open(filename, SWLoadableModule::SharedLibrary, SWLoadableModule::Normal) == SW_STATUS_SUCCESS);
							}
						}
					}
				}
			}

		if (!res)
			{
			int	clist[] =
				{
				CSIDL_PROGRAM_FILES,
				CSIDL_PROGRAM_FILESX86,
				0
				};

			for (int i=0;clist[i];i++)
				{
				// Try looking in common places
				TCHAR	path[MAX_PATH];
				
				SHGetFolderPath(NULL, clist[i], NULL, SHGFP_TYPE_CURRENT, path);

				SWString	foldername = SWFilename::concatPaths(path, "MySQL");

				if (SWFileInfo::isFolder(foldername))
					{
					SWString	filename;
					SWFolder	folder;
					SWFileInfo	entry;
					
					if (folder.open(foldername) == SW_STATUS_SUCCESS)
						{
						while (!res && folder.read(entry) == SW_STATUS_SUCCESS)
							{
							SWString	name=entry.getName();
					
							// Always ignore . and ..
							if (name == "." || name == "..") continue;

							if (entry.isFolder())
								{
								filename = SWFilename::concatPaths(entry.getFullPathname(), "bin/libmysql.dll");
								if (!res && SWFileInfo::isFile(filename))
									{
									res = (m_dll.open(filename, SWLoadableModule::SharedLibrary, SWLoadableModule::Normal) == SW_STATUS_SUCCESS);
									}

								filename = SWFilename::concatPaths(entry.getFullPathname(), "lib/opt/libmysql.dll");
								if (!res && SWFileInfo::isFile(filename))
									{
									res = (m_dll.open(filename, SWLoadableModule::SharedLibrary, SWLoadableModule::Normal) == SW_STATUS_SUCCESS);
									}
								}
							}
						}
					}
				}
			}

		if (!res)
			{
			res = (m_dll.open("libmysql", SWLoadableModule::SharedLibrary, SWLoadableModule::Normal) == SW_STATUS_SUCCESS);
			}
#endif

#define MYSQLFUNC(x) fn_##x			= (func_##x)m_dll.symbol(#x); \
		if (fn_##x == NULL) { res = false; } 
		// printf("Failed to load %s from DLL\n", #x); exit(1); }

MYSQL_FUNCTION_LIST
#undef MYSQLFUNC

		return res;
		}



bool
DBMySqlDatabase::stopMySQL()
		{
		// Do nothing
		return true;
		}



bool
DBMySqlDatabase::isSupported()
		{
		SWGuard	guard(mySqlInitCount);

		return (fn_mysql_init != NULL);
		}


bool
DBMySqlDatabase::reconnect()
		{
		SWArray<SWNamedValue> va;

		va.add(SWNamedValue("host", m_host));
		va.add(SWNamedValue("username", m_username));
		va.add(SWNamedValue("passwd", m_passwd));
		va.add(SWNamedValue("database", m_database));
		va.add(SWNamedValue("port", m_port));

		close();

		return (opendb(va) == SW_STATUS_SUCCESS);
		}


sw_status_t
DBMySqlDatabase::opendb(const SWArray<SWNamedValue> &va)
		{
		SWGuard	guard(mySqlInitCount);
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (fn_mysql_init != NULL)
			{
			SWString	host, username, passwd, database;
			int			port=3306;
			bool		createflag=true;
			
			for (int i=0;i<(int)va.size();i++)
				{
				if (va.get(i).name() == "host") host = va.get(i).toString();
				else if (va.get(i).name() == "username") username = va.get(i).toString();
				else if (va.get(i).name() == "passwd") passwd = va.get(i).toString();
				else if (va.get(i).name() == "database") database = va.get(i).toString();
				else if (va.get(i).name() == "port") port = va.get(i).toInt32();
				else if (va.get(i).name() == "create") createflag = va.get(i).toBoolean();
				}

			if (MYPDB != NULL)
				{
				res = SW_STATUS_ALREADY_OPEN;
				}
			else
				{
				m_pDB = fn_mysql_init(NULL);
				if (MYPDB == NULL)
					{
					res =  SW_STATUS_DB_ERROR;
					}
				else if (fn_mysql_real_connect(MYPDB, host, username, passwd, NULL, port, NULL, 0) == NULL)
					{
					res =  SW_STATUS_DB_ERROR;
					}
				else
					{
					m_host = host;
					m_username = username;
					m_passwd = passwd;
					m_database = database;
					m_port = port;

					if (!database.isEmpty())
						{
						MySqlDB		mdb(this, MYPDB);
						SWString	sql;

						sql = "use ";
						sql += database;

						if (mdb.execute(sql) != SW_STATUS_SUCCESS && createflag)
							{
							sql = "create database ";
							sql += database;
							sql += " CHARACTER SET utf8 COLLATE utf8_general_ci";
							res = (mdb.execute(sql)==SW_STATUS_SUCCESS)?SW_STATUS_SUCCESS:SW_STATUS_DB_ERROR;
							}
						
						if (res == SW_STATUS_SUCCESS)
							{
							mdb.execute("set names latin1");
							}
						}
					}
				}
			}
		else
			{
			res =  SW_STATUS_DB_ERROR;
			}
		
		return res;
		}
		
		
sw_status_t
DBMySqlDatabase::close()
		{
		SWGuard	guard(mySqlInitCount);
		sw_status_t	res=SW_STATUS_SUCCESS;
		
		if (MYPDB != NULL)
			{
			fn_mysql_close(MYPDB);
			m_pDB = NULL;
			}
		
		m_host.empty();
		m_username.empty();
		m_passwd.empty();
		m_database.empty();
		m_port = 0;

		return res;
		}



sw_status_t
DBMySqlDatabase::checkTable(const SWString &table, const SWArray<DBTableField> &fieldDefs, const SWArray<DBTableIndex> &indexDefs)
		{
		SWGuard	guard(mySqlInitCount);
		sw_status_t			res=SW_STATUS_SUCCESS;
		MySqlDB				mdb(this, MYPDB);
		bool				create=false;
		SWStringArray		fa;		// Field names
		SWStringArray		ta;		// Type strings
		int					r;
		SWString			sql;
		MySqlResult			mr;
		
		sql = "desc ";
		sql += table;

		if (mdb.execute(sql, mr) == SW_STATUS_SUCCESS)
			{
			MySqlRow	row;

			while (mr.nextRow(row))
				{
				fa.add(row.getField(0));	// Name
				ta.add(row.getField(1));	// type
				}
			}
		else
			{
			create = true;
			}
		
		sql.Empty();
		
		SWString	extra;
		bool		autoincr=false;

		for (int i=0;i<(int)fieldDefs.size();i++)
			{
			const DBTableField	&fd=fieldDefs.get(i);
			bool	found=false;
			
			if (!create)
				{
				for (int i=0;!found&&i<(int)fa.size();i++)
					{
					if (fa[i] == fd.name)
						{
						found = true;

						// If a test field make sure that if needed we increase the size to match
						// but never reduce size
						if (fd.type == DBF_TEXT || fd.type == DBF_PASSWORD)
							{
							SWString	ts=ta[i];

							if (ts.startsWith("varchar(", SWString::ignoreCase))
								{
								int	len;

								ts.remove(0, 8);
								len = ts.toInt32(10);

								if (len > 0 && len < fd.size)
									{
									SWString	sqlGrow;

									sqlGrow = "alter table ";
									sqlGrow += table;
									sqlGrow += " modify ";
									sqlGrow += fd.name;
									sqlGrow += " ";

									SWString	s;
						
									if (fd.size < 16384) s.format("VARCHAR(%d)", fd.size);
									else s = "MEDIUMTEXT";

									sqlGrow += s;
									r = mdb.execute(sqlGrow);
									}
								}
							}
						}
					}
				}
			
			if (!found)
				{
				if (sql.IsEmpty())
					{
					if (create) sql = "create table ";
					else sql = "alter table ";
					sql += table;
					
					if (create) sql += " (";
					}
				else
					{
					sql += ", ";
					}
				
				sql += " ";
				if (!create)
					{
					sql += "ADD COLUMN ";
					}
				
				sql += fd.name;
				sql += " ";
				
				switch (fd.type)
					{
					case DBF_BOOLEAN:	sql += "BOOL";		break;		// A boolean value
					case DBF_INT8:		sql += "TINYINT";	break;		// An 8-bit signed integer
					case DBF_INT16:		sql += "SMALLINT";	break;		// A 16-bit signed integer
					case DBF_FLOAT: 	sql += "FLOAT";		break;		// A float
					case DBF_DOUBLE:	sql += "DOUBLE";	break;		// A double
					case DBF_DATE:		sql += "DATE";		break;		// A date/time (SWTime) stored in local time format
					case DBF_DATETIME:	sql += "DATETIME";	break;		// A date/time (SWTime) stored in local time format
					case DBF_TIMESTAMP:	sql += "DATETIME";	break;		// A date/time (SWTime) stored in local time format
					
					case DBF_MONEY:
					case DBF_INT32:	
						sql += "INT";
						break;		// A 32-bit signed integer

					case DBF_INT64:	
						sql += "BIGINT";
						break;		// A 64-bit signed integer

					case DBF_TEXT:
					case DBF_PASSWORD:	// An encrypted text field
						{
						SWString	s;
						
						if (fd.size < 16384) s.format("VARCHAR(%d)", fd.size);
						else s = "MEDIUMTEXT";
						sql += s;
						}
						break;

					case DBF_SMALL_BLOB:	sql += "TINYBLOB";		break;
					case DBF_BLOB:			sql += "BLOB";			break;
					case DBF_MEDIUM_BLOB:	sql += "MEDIUMBLOB";	break;
					case DBF_LARGE_BLOB:	sql += "LONGBLOB";		break;
					default:
						throw 1;
						break;
					}

				if ((fd.attributes & DBA_NOTNULL) != 0) sql += " NOT NULL";
				if ((fd.attributes & DBA_AUTOINC) != 0)
					{
					if (autoincr)
						{
						// We can only have one auto-inc field
						throw SW_EXCEPTION_TEXT(DBException, "Only one auto-increment field allowed per table");
						}

					// Enforces primary key
					sql += " AUTO_INCREMENT";
					extra += ", PRIMARY KEY (";
					extra += fd.name;
					extra += ")";
					}
				}
			}
			
		sql += extra;

		if (!sql.IsEmpty())
			{
			if (create) sql += ")";
			r = mdb.execute(sql);
			
			if (create)
				{
				bool				primary=true;

				// Need to create the indexes
				for (int i=0;i<(int)indexDefs.size();i++)
					{
					const DBTableIndex	&fd=indexDefs.get(i);
					
					sql = "alter table ";
					sql += table;
					sql += " add ";

					
					if (fd.unique)
						{
						if (primary)
							{
							sql += "primary ";
							primary = false;
							}
						else
							{
							sql += "unique ";
							}
						}
						
					sql += "key ";
					sql += "idx_";
					sql += fd.name;
					sql += " (";
					sql += fd.fields;
					sql += " )";
					
					r = mdb.execute(sql);
					}
				}
			}
		
		return res;
		}

//--------------------------------------------------------------------------------
// SWList Iterator
//--------------------------------------------------------------------------------


class MySqlIterator : public SWCollectionIterator
		{
		//friend class SWList;

public:
		MySqlIterator(DBRecordSet *pRecordSet, MYSQL_RES *pResult);
		~MySqlIterator();

		virtual bool		hasCurrent();
		virtual bool		hasNext();
		virtual bool		hasPrevious();

		virtual void *		current();
		virtual void *		next();
		virtual void *		previous();

		virtual bool		remove(void *pOldData);
		virtual bool		add(void *data, SWAbstractIterator::AddLocation where);
		virtual bool		set(void *data, void *pOldData);

private:
		void				filldata(MYSQL_ROW row);

private:
		MYSQL_RES		*m_pResult;
		DBRecordSet		*m_pRecordSet;
		DBRecord		m_data;
		sw_int32_t		m_row;
		sw_int32_t		m_rowCount;
		sw_int32_t		m_fieldCount;
		};


#define FLAG_INIT		0x1
#define FLAG_NEXT		0x2
#define FLAG_PREV		0x4
#define FLAG_CURR		0x8


MySqlIterator::MySqlIterator(DBRecordSet *pRecordSet, MYSQL_RES *pResult) :
	SWCollectionIterator(NULL),
	m_pResult(pResult),
	m_pRecordSet(pRecordSet),
	m_row(-1),
	m_rowCount(0),
	m_fieldCount(0)
		{
		if (m_pResult != NULL)
			{
			SWGuard	guard(mySqlInitCount);

			m_rowCount = fn_mysql_num_rows(m_pResult);
			m_fieldCount = fn_mysql_num_fields(m_pResult);

			m_pRecordSet->clearFieldInfo();
			for (sw_int32_t f=0;f<m_fieldCount;f++)
				{
				MYSQL_FIELD	*pField=fn_mysql_fetch_field_direct(m_pResult, f);
				
				DBFieldInfo	dbfi;

				dbfi.name(pField->name);
				switch (pField->type)
					{
					case MYSQL_TYPE_DECIMAL:		dbfi.type(DBF_DECIMAL);		break;
					case MYSQL_TYPE_NEWDECIMAL:		dbfi.type(DBF_DECIMAL);		break;
					case MYSQL_TYPE_TINY:			dbfi.type(DBF_INT8);		break;
					case MYSQL_TYPE_SHORT:			dbfi.type(DBF_INT16);		break;
					case MYSQL_TYPE_LONG:			dbfi.type(DBF_INT32);		break;
					case MYSQL_TYPE_FLOAT:			dbfi.type(DBF_DOUBLE);		break;
					case MYSQL_TYPE_DOUBLE:			dbfi.type(DBF_DOUBLE);		break;
					case MYSQL_TYPE_NULL:			dbfi.type(DBF_INVALID);		break;
					case MYSQL_TYPE_TIMESTAMP:		dbfi.type(DBF_TIMESTAMP);	break;
					case MYSQL_TYPE_LONGLONG:		dbfi.type(DBF_INT64);		break;
					case MYSQL_TYPE_INT24:			dbfi.type(DBF_INT32);		break;
					case MYSQL_TYPE_DATE:			dbfi.type(DBF_DATE);		break;
					case MYSQL_TYPE_DATETIME:		dbfi.type(DBF_DATETIME);	break;
					case MYSQL_TYPE_YEAR:			dbfi.type(DBF_INT16);		break;

					case MYSQL_TYPE_VARCHAR:
					case MYSQL_TYPE_ENUM:
					case MYSQL_TYPE_SET:
					case MYSQL_TYPE_VAR_STRING:
					case MYSQL_TYPE_STRING:		
						dbfi.type(DBF_TEXT);
						break;

					case MYSQL_TYPE_BIT:
					case MYSQL_TYPE_TINY_BLOB:
					case MYSQL_TYPE_MEDIUM_BLOB:
					case MYSQL_TYPE_LONG_BLOB:
					case MYSQL_TYPE_BLOB:
						if (pField->charsetnr == 0x3f) dbfi.type(DBF_BLOB);
						else dbfi.type(DBF_TEXT);
						break;

					// Unsupported types
					case MYSQL_TYPE_NEWDATE:
					case MYSQL_TYPE_TIME:
					case MYSQL_TYPE_GEOMETRY:
						dbfi.type(DBF_INVALID);
						break;
					}

				dbfi.size(pField->length);
				dbfi.flags(0);

				m_pRecordSet->setFieldInfo(f, dbfi);
				}
			}
		}


MySqlIterator::~MySqlIterator()
		{
		SWGuard	guard(mySqlInitCount);
		if (m_pResult != NULL)
			{
			fn_mysql_free_result(m_pResult);
			m_pResult = NULL;
			m_row = -1;
			m_rowCount = 0;
			m_fieldCount = 0;
			}
		}


void
MySqlIterator::filldata(MYSQL_ROW row)
		{
		SWGuard	guard(mySqlInitCount);
		m_data.clear();

		unsigned long	*flengths;

		flengths = fn_mysql_fetch_lengths(m_pResult);
		for (sw_int32_t f=0;f<m_fieldCount;f++)
			{
			DBFieldInfo	fi;
			SWValue		nv;

			if (row[f] != NULL)
				{
				m_pRecordSet->getFieldInfo(f, fi);
				
				switch (fi.type())
					{
					case DBF_DATE:
						{
						SWDate	dt;

						dt.set(row[f], "YYYY-MM-DD");
						nv = dt;
						}
						break;

					case DBF_DATETIME:
						{
						SWLocalTime	lt;

						lt.set(row[f], "YYYY-MM-DD hh:mm:ss");
						nv = lt;
						}
						break;

					case DBF_INT8:
					case DBF_INT16:
					case DBF_INT32:
						nv = SWString::toInt32(row[f], 10);
						break;

					case DBF_INT64:
						nv = SWString::toInt64(row[f], 10);
						break;

					case DBF_SMALL_BLOB:
					case DBF_BLOB:
					case DBF_MEDIUM_BLOB:
					case DBF_LARGE_BLOB:
						{
						nv.setValue(row[f], flengths[f], true);
						break;
						}

					default:
						if (SWString::isAscii(row[f]))
							{
							nv = row[f];
							}
						else
							{
							// Convert it to unicode before assignment as we are set up to get UTF-8 strings from MySQL
							wchar_t	*ws;

							ws = wcsnew(row[f], CP_UTF8);
							nv = ws;
							delete ws;
							}
						break;
					}
				}

			m_data.setValue(f, nv);
			}
		}


bool
MySqlIterator::hasCurrent()
		{
		return (m_row >= 0 && m_row < m_rowCount);
		}


bool
MySqlIterator::hasNext()
		{
		return ((m_row + 1) < m_rowCount);
		}


bool
MySqlIterator::hasPrevious() 
		{
		// Currently not supported
		return false;
		}


void *
MySqlIterator::current() 
		{
		if (!hasCurrent()) throw SW_EXCEPTION(SWNoSuchElementException);

		return &m_data;
		}

void *
MySqlIterator::next() 
		{
		SWGuard	guard(mySqlInitCount);
		if (!hasNext()) throw SW_EXCEPTION(SWNoSuchElementException);

		MYSQL_ROW	row;
		row = fn_mysql_fetch_row(m_pResult);
		if (row == NULL) throw SW_EXCEPTION(SWNoSuchElementException);

		m_row++;
		filldata(row);

		return &m_data;
		}

void *
MySqlIterator::previous() 
		{
		void	*res=NULL;

		if (!res) throw SW_EXCEPTION(SWNoSuchElementException);

		return res;
		}

bool
MySqlIterator::remove(void * /*pOldData*/)
		{
		bool	res=false;

		if (!res) throw SW_EXCEPTION(SWUnsupportedOperationException);

		return res;
		}


bool
MySqlIterator::set(void * /*data*/, void * /*pOldData*/)
		{
		bool	res=false;

		if (!res) throw SW_EXCEPTION(SWUnsupportedOperationException);

		return res;
		}


bool
MySqlIterator::add(void * /*data*/, SWAbstractIterator::AddLocation /*where*/) 
		{
		bool	res=false;

		if (!res) throw SW_EXCEPTION(SWUnsupportedOperationException);

		return res;
		}


sw_status_t
DBMySqlDatabase::execute(const SWString &sql, DBRecordSet *pRecordSet)
		{
		SWGuard	guard(mySqlInitCount);
		sw_status_t	res=SW_STATUS_SUCCESS;
		const char	*s=sql;
		int			slen=(int)strlen(s);
		int			r;

		r = fn_mysql_real_query(MYPDB, s, slen);
		if (r != 0)
			{
			int		suberr=fn_mysql_errno(MYPDB);

			if (suberr == 2006 || suberr == 2013)
				{
				// Try a reconnect
				if (reconnect())
					{
					r = fn_mysql_real_query(MYPDB, s, slen);
					}
				}
			}

		if (r != 0)
			{
			res = SW_STATUS_DB_ERROR;
			}
		else if (pRecordSet != NULL)
			{
			MYSQL_RES		*pResult;
			pResult = fn_mysql_store_result(MYPDB);
			loadRecordSet(pRecordSet,  new MySqlIterator(pRecordSet, pResult));
			}

		return res;
		}


bool
DBMySqlDatabase::tableExists(const SWString &table)
		{
		SWGuard	guard(mySqlInitCount);
		bool				res=false;
		MySqlDB				mdb(this, MYPDB);
		MySqlResult			mr;
		SWString			sql;
		
		sql = "desc ";
		sql += table;

		res = (mdb.execute(sql, mr) == SW_STATUS_SUCCESS);

		return res;
		}

bool
DBMySqlDatabase::tableHasField(const SWString &table, const SWString &field)
		{
		SWGuard	guard(mySqlInitCount);
		bool				res=false;
		MySqlDB				mdb(this, MYPDB);
		MySqlResult			mr;
		SWString			sql;
		
		sql = "desc ";
		sql += table;

		if (mdb.execute(sql, mr) == SW_STATUS_SUCCESS)
			{
			MySqlRow	row;

			while (!res && mr.nextRow(row))
				{
				if (field == row.getField(0))
					{
					res = true;
					}
				}
			}

		return res;
		}


sw_status_t
DBMySqlDatabase::getTableInfo(const SWString &table, DBTableInfo &ti)
		{
		SWGuard	guard(mySqlInitCount);
		sw_status_t			res=SW_STATUS_SUCCESS;
		MySqlDB				mdb(this, MYPDB);
		MySqlResult			mr;
		SWString			sql;
		
		sql = "desc ";
		sql += table;

		if ((res = mdb.execute(sql, mr)) == SW_STATUS_SUCCESS)
			{
			MySqlRow	row;
			int			f=0;

			while (!res && mr.nextRow(row))
				{
				DBFieldInfo	dbfi;
				SWString	name=row.getField(0);
				SWString	tstr=row.getField(1);
				SWString	null=row.getField(2);
				SWString	key=row.getField(3);
				SWString	extra=row.getField(5);
				int			len=0;
				int			p;
				int			flags=0;

				if ((p = tstr.first('(')) > 0)
					{
					len = atoi(tstr.mid(p+1));
					tstr.remove(p);
					}

				dbfi.name(name);
				dbfi.size(len);

				if (null.compareNoCase("NO") == 0) flags |= DBA_NOTNULL;
				if (extra.contains("AUTO_INCREMENT", SWString::ignoreCase)) flags |= DBA_AUTOINC;

				dbfi.flags(flags);

				if (tstr.compareNoCase("int") == 0) dbfi.type(DBF_INT32);
				else if (tstr.compareNoCase("varchar") == 0) dbfi.type(DBF_TEXT);
				else if (tstr.compareNoCase("datetime") == 0) dbfi.type(DBF_DATETIME);
				else if (tstr.compareNoCase("tinyint") == 0) dbfi.type(DBF_INT8);
				else if (tstr.compareNoCase("smallint") == 0) dbfi.type(DBF_INT16);
				else if (tstr.compareNoCase("date") == 0) dbfi.type(DBF_DATE);
				else if (tstr.compareNoCase("double") == 0) dbfi.type(DBF_DOUBLE);
				else if (tstr.compareNoCase("mediumblob") == 0) dbfi.type(DBF_MEDIUM_BLOB);
				else if (tstr.compareNoCase("largeblob") == 0) dbfi.type(DBF_LARGE_BLOB);
				else if (tstr.compareNoCase("smallblob") == 0) dbfi.type(DBF_SMALL_BLOB);
				else if (tstr.compareNoCase("mediumtext") == 0) dbfi.type(DBF_TEXT);
				else dbfi.type(DBF_INVALID);


				/*
				switch (pField->type)
					{
					case MYSQL_TYPE_DECIMAL:		dbfi.type(DBF_DECIMAL);		break;
					case MYSQL_TYPE_NEWDECIMAL:		dbfi.type(DBF_DECIMAL);		break;
					case MYSQL_TYPE_TINY:			dbfi.type(DBF_INT8);		break;
					case MYSQL_TYPE_SHORT:			dbfi.type(DBF_INT16);		break;
					case MYSQL_TYPE_LONG:		
					case MYSQL_TYPE_FLOAT:			dbfi.type(DBF_DOUBLE);		break;
					case MYSQL_TYPE_DOUBLE:			dbfi.type(DBF_DOUBLE);		break;
					case MYSQL_TYPE_NULL:			dbfi.type(DBF_INVALID);		break;
					case MYSQL_TYPE_TIMESTAMP:		dbfi.type(DBF_TIMESTAMP);	break;
					case MYSQL_TYPE_LONGLONG:		dbfi.type(DBF_INT64);		break;
					case MYSQL_TYPE_INT24:			dbfi.type(DBF_INT32);		break;
					case MYSQL_TYPE_DATE:			dbfi.type(DBF_DATE);		break;
					case MYSQL_TYPE_DATETIME:		dbfi.type(DBF_DATETIME);	break;
					case MYSQL_TYPE_YEAR:			dbfi.type(DBF_INT16);		break;

					case MYSQL_TYPE_VARCHAR:
					case MYSQL_TYPE_ENUM:
					case MYSQL_TYPE_SET:
					case MYSQL_TYPE_VAR_STRING:
					case MYSQL_TYPE_STRING:		
						dbfi.type(DBF_TEXT);
						break;

					case MYSQL_TYPE_BIT:
					case MYSQL_TYPE_TINY_BLOB:
					case MYSQL_TYPE_MEDIUM_BLOB:
					case MYSQL_TYPE_LONG_BLOB:
					case MYSQL_TYPE_BLOB:
						dbfi.type(DBF_BLOB);
						break;

					// Unsupported types
					case MYSQL_TYPE_NEWDATE:
					case MYSQL_TYPE_TIME:
					case MYSQL_TYPE_GEOMETRY:
						dbfi.type(DBF_INVALID);
						break;
					}
				*/

				ti.setFieldInfo(f++, dbfi);
				}
			}

		return res;
		}



SWString		
DBMySqlDatabase::sqlBinaryValue(const void *pData, sw_size_t len) const
		{
		SWString	sql, ns;
		sw_size_t	pos;
		sw_byte_t	*p=(sw_byte_t *)pData;

		sql = "X'";
		for (pos=0;pos<len;pos++)
			{
			ns.format(_T("%02X"), (*p++) & 0xff);
			sql += ns;
			}
		sql += "'";

		/*
		char	*buf=(char *)sql.lockBuffer(10 + (len * 2), sizeof(char));

		buf[0] = '\'';
		fn_mysql_real_escape_string(MYPDB, buf+1, (char *)pData, len);
		sql.unlockBuffer();
		sql += "'";
		*/

		return sql;
		}



SWString
DBMySqlDatabase::sqlQuote(const SWString &s) const
		{
		SWString	res;
		
		const char	*sp=s.c_str();
		int			len=(int)strlen(sp);
		char		*buf=(char *)res.lockBuffer(10 + (len * 2), sizeof(char));

		buf[0] = '\'';
		fn_mysql_real_escape_string(MYPDB, buf+1, (char *)sp, len);
		res.unlockBuffer();
		res += "'";

		return res;
		}


sw_status_t	
DBMySqlDatabase::getTableList(SWStringArray &tlist)
		{
		tlist.clear();

		sw_status_t	res=SW_STATUS_SUCCESS;
		SWString	sql;
		SWString	dbname;
		MySqlDB		mdb(this, MYPDB);
		MySqlResult	mr;

		sql = "SELECT TABLE_NAME FROM information_schema.tables where TABLE_SCHEMA = ";
		sql += sqlQuote(dbname);

		if ((res = mdb.execute(sql, mr)) == SW_STATUS_SUCCESS)
			{
			MySqlRow	row;

			while (mr.nextRow(row))
				{
				tlist.add(row.getField(0));	// Name
				}
			}

		return res;
		}


#endif // SW_CONFIG_USE_MYSQL
