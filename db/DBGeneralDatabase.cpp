
/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"

#include <swift/SWTokenizer.h>
#include <swift/SWList.h>
#include <swift/sw_crypt.h>


#include <db/DBGeneralDatabase.h>
#include <db/DBOption.h>

SWList<DBDataObject *>					*DBGeneralDatabase::m_pDataObjectList=0;
SWList<DBGeneralDatabaseJob *>			*DBGeneralDatabase::m_pOpenJobList=0;
SWList<DBGeneralDatabaseJob *>			*DBGeneralDatabase::m_pCloseJobList=0;


static DBFieldDef	recordlock_fieldlist[] =
{
	{	"code",			DBF_TEXT,		16,	0	},
	{	"locktype",		DBF_INT32,		4,	0	},
	{	"locktime",		DBF_TIMESTAMP,	8,	0	},
	{	"username",		DBF_TEXT,		32,	0	},
	{	"hostname",		DBF_TEXT,		32,	0	},

	// End of list marker
	{	NULL,			DBF_MAX,			0,	0	}
};

static DBIndexDef recordlock_idxlist[] =
{
	{	"primary",		"code,locktype",	true	},

	// End of list marker
	{	NULL,			NULL,	true	}
};



static DBFieldDef	idctrl_fieldlist[] =
{
	{	"type",		DBF_INT32,		4,	0	},
	{	"code",		DBF_TEXT,		16,	0	},
	{	"nextid",	DBF_INT32,		4,	0	},

	// End of list marker
	{	NULL,			DBF_MAX,			0,	0	}
};

static DBIndexDef idctrl_idxlist[] =
{
	{	"primary",		"type,code",	true	},

	// End of list marker
	{	NULL,			NULL,	true	}
};


static DBFieldDef	idctrlbycode_fieldlist[] =
{
	{	"code",	DBF_TEXT,		64,	0	},
	{	"nextid",		DBF_INT32,		4,	0	},

	// End of list marker
	{	NULL,			DBF_MAX,		0,	0	}
};

static DBIndexDef idctrlbycode_idxlist[] =
{
	{	"primary",		"code",	true	},

	// End of list marker
	{	NULL,			NULL,	true	}
};

static bool
cacheNeedsUpdate(const SWTime &lastupdate, double cacheValidTime)
		{
		SWTime	now;
		
		now.update();
		
		return (((double)now - (double)lastupdate) > cacheValidTime);
		}


DBGeneralDatabase::DBGeneralDatabase() :
	m_optionsCacheValidTime(30.0)
		{
		}


DBGeneralDatabase::~DBGeneralDatabase()
		{
		// Make sure the database is actually closed
		close();
		}


sw_status_t
DBGeneralDatabase::open(int type, const SWString &params)
		{
		return open(type, params, OpenNormal);
		}


sw_status_t
DBGeneralDatabase::open(int type, const SWString &params, int openflags)
		{
		sw_status_t	res;

		res = DBDatabase::open(type, params);
		if (res == SW_STATUS_SUCCESS)
			{
			if ((openflags & OpenNoCheckTables) == 0) checkTables();
			if ((openflags & OpenNoRunJobs) == 0) runOpenJobs();
			}

		return res;
		}


sw_status_t
DBGeneralDatabase::close()
		{
		if (isOpen())
			{
			runCloseJobs();
			}

		return DBDatabase::close();
		}


bool
DBGeneralDatabase::lockRecord(int id, int type, bool force)
		{
		SWString	s;
		
		s.format(_T("%d"), id);
		
		return lockRecord(s, type, force);
		}


bool
DBGeneralDatabase::unlockRecord(int id, int type)
		{
		SWString	s;
		
		s.format(_T("%d"), id);
		
		return unlockRecord(s, type);
		}
		

bool
DBGeneralDatabase::lockRecord(const SWString &code, int type, bool /* force */)
		{
		SWString	sql;
		bool		res=false;
		
		sql = _T("select count(*) from recordlock where code = ");
		sql += sqlQuote(code);
		sql += _T(" and locktype = ");
		sql += SWString::valueOf(type);

		SWValue	v;

		if (execute(sql, v) == SW_STATUS_SUCCESS)
			{
			if (v.toInt32() == 0)
				{
				SWLocalTime	now;
			
				now.update();
				sql = _T("insert into recordlock (code, locktype, locktime) values(");
				sql += sqlQuote(code);
				sql += _T(", ");
				sql += SWString::valueOf(type);
				sql += _T(", '");
				sql += now.toString(_T("%Y-%m-%d %H:%M:%S"));
				sql += _T("')");

				res = (execute(sql) == SW_STATUS_SUCCESS);
				}
			}
			
		return res;
		}


bool
DBGeneralDatabase::unlockRecord(const SWString &code, int type)
		{
		SWString	sql;
		bool		res;
		
		sql = _T("delete from recordlock where code = ");
		sql += sqlQuote(code);
		sql += _T(" and locktype = ");
		sql += SWString::valueOf(type);

		res = (execute(sql) == SW_STATUS_SUCCESS);

		return res;
		}


int
DBGeneralDatabase::setNextID(int type, int code, int id)
		{
		SWString	s;
		
		s.format("%d", code);
		
		return setNextID(type, s, id);
		}



int
DBGeneralDatabase::setNextID(int type, const SWString &code, int id)
		{
		SWString	lkey, sql;
		SWValue		v;
		int			res=-1;
		
		lkey.format("%d_%s", type, code.c_str());
		
		int	tries=0;
		
		while (!lockRecord(lkey, LK_IDCTRL_RECORD))
			{
			sw_thread_sleep(10);
			if (++tries > 1000)
				{
				return -1;
				}
			}
		
		sql.format("select nextid from idctrl where type = %d and code = ", type);
		sql += sqlQuote(code);
		
		if (execute(sql, v) != SW_STATUS_SUCCESS)
			{
			sql = "insert into idctrl (type, code, nextid) values(";
			sql += SWString::valueOf(type);
			sql += ", ";
			sql += sqlQuote(code);
			sql += ", ";
			sql += SWString::valueOf(id);
			sql += ")";
			}
		else
			{
			sql = "update idctrl set nextid = ";
			sql += SWString::valueOf(id);
			sql += " where type = ";
			sql += SWString::valueOf(type);
			sql += " and code = ";
			sql += sqlQuote(code);
			}
		
		res = (execute(sql) == SW_STATUS_SUCCESS);
			
		unlockRecord(lkey, LK_IDCTRL_RECORD);
		
		return id;
		}


int
DBGeneralDatabase::getNextID(int type, int code, bool update)
		{
		SWString	s;
		
		s.format(_T("%d"), code);
		
		return getNextID(type, s, update);
		}


int
DBGeneralDatabase::getNextID(int type, const SWString &code, bool update)
		{
		SWString	lkey, sql;
		SWValue		v;
		int			res=-1;
		bool		insert=false;
		
		lkey.format(_T("%d_%s"), type, code.c_str());
		
		if (update)
			{
			int	tries=0;
			
			while (!lockRecord(lkey, LK_IDCTRL_RECORD))
				{
				sw_thread_sleep(10);
				if (++tries > 1000)
					{
					// Seomthing has probabbly gone wrong so ignore the problem and force an unlock
					break;
					// return -1;
					}
				}
			}
		
		sql.format(_T("select nextid from idctrl where type = %d and code = "), type);
		sql += sqlQuote(code);
		
		if (execute(sql, v) != SW_STATUS_SUCCESS)
			{
			res = 1;
			insert = true;
			}
		else
			{
			res = v.toInt32();
			}
			
		if (update)
			{
			if (insert)
				{
				sql = _T("insert into idctrl (type, code, nextid) values(");
				sql += SWString::valueOf(type);
				sql += _T(", ");
				sql += sqlQuote(code);
				sql += _T(", ");
				sql += SWString::valueOf(res+1);
				sql += _T(")");
				}
			else
				{
				sql = _T("update idctrl set nextid = ");
				sql += SWString::valueOf(res+1);
				sql += _T(" where type = ");
				sql += SWString::valueOf(type);
				sql += _T(" and code = ");
				sql += sqlQuote(code);
				}
			
			execute(sql);
				
			unlockRecord(lkey, LK_IDCTRL_RECORD);
			}
		
		return res;
		}



int
DBGeneralDatabase::setNextID(const SWString &code, int id)
		{
		SWString		sql;
		SWValue	v;
		int			res=-1;
		
		int	tries=0;
		
		while (!lockRecord(code, LK_IDCTRL_RECORD))
			{
			sw_thread_sleep(10);
			if (++tries > 1000)
				{
				return -1;
				}
			}
		
		sql = "select nextid from idctrlbycode where code = ";
		sql += sqlQuote(code);
		
		if (execute(sql, v) != SW_STATUS_SUCCESS)
			{
			sql = _T("insert into idctrlbycode (code, nextid) values(");
			sql += sqlQuote(code);
			sql += _T(", ");
			sql += SWString::valueOf(id);
			sql += _T(")");
			}
		else
			{
			sql = _T("update idctrlbycode set nextid = ");
			sql += SWString::valueOf(id);
			sql += _T(" where code = ");
			sql += sqlQuote(code);
			}
		
		res = (execute(sql) == SW_STATUS_SUCCESS);
			
		unlockRecord(code, LK_IDCTRL_RECORD);
		
		return id;
		}


int
DBGeneralDatabase::getNextID(const SWString &code, bool update)
		{
		SWString		sql;
		SWValue	v;
		int			res=-1;
		bool		insert=false;
		
		if (update)
			{
			int	tries=0;
			
			while (!lockRecord(code, LK_IDCTRL_RECORD))
				{
				sw_thread_sleep(10);
				if (++tries > 1000)
					{
					return -1;
					}
				}
			}
		
		sql = "select nextid from idctrlbycode where code = ";
		sql += sqlQuote(code);
		
		if (execute(sql, v) != SW_STATUS_SUCCESS)
			{
			res = 1;
			insert = true;
			}
		else
			{
			res = v.toInt32();
			}
			
		if (update)
			{
			if (insert)
				{
				sql = _T("insert into idctrlbycode (code, nextid) values(");
				sql += sqlQuote(code);
				sql += _T(", ");
				sql += SWString::valueOf(res+1);
				sql += _T(")");
				}
			else
				{
				sql = _T("update idctrlbycode set nextid = ");
				sql += SWString::valueOf(res+1);
				sql += _T(" where code = ");
				sql += sqlQuote(code);
				}
			
			execute(sql);
				
			unlockRecord(code, LK_IDCTRL_RECORD);
			}
		
		return res;
		}


sw_status_t
DBGeneralDatabase::checkTables()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		
		// First check the DBOption table since we use this for storing table version info
		DBOption	tmp;
		checkTable(tmp);

		// Next check special tables
		SWArray<DBTableField>	fieldlist;
		SWArray<DBTableIndex>	idxlist;

		fieldlist.clear();
		idxlist.clear();
		for (DBFieldDef *pfd=recordlock_fieldlist;pfd->name != NULL;pfd++) fieldlist.add(*pfd);
		for (DBIndexDef *pfd=recordlock_idxlist;pfd->name != NULL;pfd++) idxlist.add(*pfd);
		checkTable("recordlock",	fieldlist,	idxlist);

		fieldlist.clear();
		idxlist.clear();
		for (DBFieldDef *pfd=idctrl_fieldlist;pfd->name != NULL;pfd++) fieldlist.add(*pfd);
		for (DBIndexDef *pfd=idctrl_idxlist;pfd->name != NULL;pfd++) idxlist.add(*pfd);
		checkTable("idctrl",		fieldlist,	idxlist);

		fieldlist.clear();
		idxlist.clear();
		for (DBFieldDef *pfd=idctrlbycode_fieldlist;pfd->name != NULL;pfd++) fieldlist.add(*pfd);
		for (DBIndexDef *pfd=idctrlbycode_idxlist;pfd->name != NULL;pfd++) idxlist.add(*pfd);
		checkTable("idctrlbycode",		fieldlist,	idxlist);

		// Next check all of the self-registered DataObject tables		
		if (m_pDataObjectList != NULL)
			{
			DBDataObject	*pObject=NULL;
			
			while (m_pDataObjectList->removeHead(&pObject))
				{
				int		tv=pObject->tableVersion();
				int		dv=getOptionInteger("tableversion", pObject->tableName(), 0);

				// Only check the table if there is no table version no OR 
				if (tv == 0 || dv < tv)
					{
					if (checkTable(*pObject) == SW_STATUS_SUCCESS)
						{
						if (tv) setOptionInteger("tableversion", pObject->tableName(), tv);
						}
					}

				delete pObject;
				}
			
			delete m_pDataObjectList;
			m_pDataObjectList = NULL;
			}

		return res;
		}


sw_status_t
DBGeneralDatabase::runOpenJobs()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		
		// First check all of the self-registered DataObject tables		
		if (m_pOpenJobList != NULL)
			{
			DBGeneralDatabaseJob	*pJob;
			
			while (m_pOpenJobList->removeHead(&pJob))
				{
				pJob->run();
				delete pJob;
				}
			
			delete m_pOpenJobList;
			m_pOpenJobList = NULL;
			}

		return res;
		}


sw_status_t
DBGeneralDatabase::runCloseJobs()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		
		// First check all of the self-registered DataObject tables		
		if (m_pCloseJobList != NULL)
			{
			DBGeneralDatabaseJob	*pJob;
			
			while (m_pCloseJobList->removeHead(&pJob))
				{
				pJob->run();
				delete pJob;
				}
			
			delete m_pCloseJobList;
			m_pCloseJobList = NULL;
			}

		return res;
		}


int
DBGeneralDatabase::registerDataObject(const char * /*name*/, DBDataObject *pObj)
		{
		if (m_pDataObjectList == NULL) m_pDataObjectList = new SWList<DBDataObject *>;

		m_pDataObjectList->addTail(pObj);

		return 0;
		}


int
DBGeneralDatabase::registerOpenJob(const char * /*name*/, SWJob::JobProc proc, void *param)
		{
		if (m_pOpenJobList == NULL) m_pOpenJobList = new SWList<DBGeneralDatabaseJob *>;

		m_pOpenJobList->addTail(new DBGeneralDatabaseJob(proc, param));

		return 0;
		}


int
DBGeneralDatabase::registerCloseJob(const char * /*name*/, SWJob::JobProc proc, void *param)
		{
		if (m_pCloseJobList == NULL) m_pCloseJobList = new SWList<DBGeneralDatabaseJob *>;

		m_pCloseJobList->addTail(new DBGeneralDatabaseJob(proc, param));

		return 0;
		}



DBOption *
DBGeneralDatabase::findOption(DBGeneralDatabase &db, const SWString &section, const SWString &param, bool create)
		{
		DBOption	*pRecord=NULL;

		if (cacheNeedsUpdate(m_optionsUpdated, m_optionsCacheValidTime))
			{
			// We've not updated them for at least X seconds
			DBOption::loadRecordsFromDB(db, m_options);
			m_optionsUpdated.update();
			}

		for (int i=0;i<(int)m_options.size();i++)
			{
			DBOption	&rec=m_options[i];

			if (rec.section() == section && rec.param() == param)
				{
				pRecord = &rec;
				break;
				}
			}

		if (pRecord == NULL && create)
			{
			pRecord = &m_options[(int)m_options.size()];
			pRecord->section(section);
			pRecord->param(param);
			}

		return pRecord;
		}


void
DBGeneralDatabase::removeOption(const SWString &section, const SWString &param)
		{
		if (cacheNeedsUpdate(m_optionsUpdated, m_optionsCacheValidTime))
			{
			// We've not updated them for at least X seconds
			DBOption::loadRecordsFromDB(*this, m_options);
			m_optionsUpdated.update();
			}

		for (int i=0;i<(int)m_options.size();i++)
			{
			DBOption	&rec=m_options[i];

			if (rec.section() == section && rec.param() == param)
				{
				rec.removeFromDB(*this);
				m_options.remove(i);
				break;
				}
			}
		}
		

int
DBGeneralDatabase::getOptionInteger(const SWString &section, const SWString &param, int defval)
		{
		DBOption	*pRecord=findOption(*this, section, param, false);
		int			res=defval;

		if (pRecord != NULL)
			{
			res = pRecord->getIntegerValue();
			}

		return res;
		}


void
DBGeneralDatabase::setOptionInteger(const SWString &section, const SWString &param, int val)
		{
		DBOption	*pRecord=findOption(*this, section, param, true);

		pRecord->setIntegerValue(val);
		pRecord->save(*this);
		}


double
DBGeneralDatabase::getOptionDouble(const SWString &section, const SWString &param, double defval)
		{
		DBOption	*pRecord=findOption(*this, section, param, false);
		double		res=defval;

		if (pRecord != NULL)
			{
			res = pRecord->getDoubleValue();
			}

		return res;
		}


void
DBGeneralDatabase::setOptionDouble(const SWString &section, const SWString &param, double val)
		{
		DBOption	*pRecord=findOption(*this, section, param, true);

		pRecord->setDoubleValue(val);
		pRecord->save(*this);
		}


SWString
DBGeneralDatabase::getOptionString(const SWString &section, const SWString &param, const SWString &defval)
		{
		DBOption	*pRecord=findOption(*this, section, param, false);
		SWString	res=defval;

		if (pRecord != NULL)
			{
			res = pRecord->getStringValue();
			}

		return res;
		}


void
DBGeneralDatabase::setOptionString(const SWString &section, const SWString &param, const SWString &val)
		{
		DBOption	*pRecord=findOption(*this, section, param, true);

		pRecord->setStringValue(val);
		pRecord->save(*this);
		}


int
DBGeneralDatabase::getRecordCount(const SWString &table, const SWString &where)
		{
		SWValue		v;
		int			res=0;
		SWString	query;
		
		query = _T("select count(*) from ");
		query += table;
		if (!where.isEmpty())
			{
			query += _T(" ");
			query += where;
			}
		
		if (execute(query, v) == SW_STATUS_SUCCESS)
			{
			res = v.toInt32();
			}
		
		return res;
		}

bool
DBGeneralDatabase::getData(SWList<DBGenericDataObject> &dlist, const SWString &sql)
		{
		DBRecordSet		rset;
		bool			res=false;
		
		dlist.clear();

		
		if (execute(sql, rset) == SW_STATUS_SUCCESS)
			{
			SWIterator<DBRecord>	it;
			
			it = rset.iterator();
			while (it.hasNext())
				{
				it.next();
				
				DBGenericDataObject	gdo;
				
				gdo.loadCurrentRecord(this, rset);
				dlist.add(gdo);
				}

			res = true;
			}
		
		return res;
		}

		
		
bool
DBGeneralDatabase::getData(SWArray<DBGenericDataObject> &dlist, const SWString &sql)
		{
		DBRecordSet		rset;
		bool			res=false;
		
		dlist.clear();
		
		if (execute(sql, rset) == SW_STATUS_SUCCESS)
			{
			SWIterator<DBRecord>	it;
			int				pos=0;
			
			it = rset.iterator();
			while (it.hasNext())
				{
				it.next();
				
				DBGenericDataObject	&gdo=dlist[pos++];
				
				gdo.loadCurrentRecord(this, rset);
				}

			res = true;
			}
		
		return res;
		}

