/**
*** @file		DBOpenConnectivityDatabase.h
*** @brief		Basic database access object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Defines the DBOpenConnectivityDatabase class
***
*** <b>Copyright 2007 SwiftTec. All rights reserved.</b>
**/
/****************************************************************************
** $Date: 2012/05/18 21:43:15 $
** $Author: simon $
** $Revision: 1.2 $
*****************************************************************************
** $Log: DBOpenConnectivityDatabase.h,v $
** Revision 1.2  2012/05/18 21:43:15  simon
** WIP
**
** Revision 1.1  2008/10/09 14:49:39  simon
** Updated database model to include SqlServer and improved interface
**
****************************************************************************/


#ifndef __db_DBOpenConnectivityDatabase_h__
#define __db_DBOpenConnectivityDatabase_h__

#if defined(SW_PLATFORM_WINDOWS) && defined(SW_BUILD_MFC)

#include <db/DBConnection.h>

#include <swift/SWString.h>
#include <swift/SWInterlockedInt32.h>

#include <afxdb.h>

// Disable warning about deprecated objects
#pragma warning(disable: 4995)
#pragma warning(disable: 4251)


class DB_DLL_EXPORT DBOpenConnectivityDatabase : public DBConnection
		{
protected:
		// Can only be derived and not used directly
		DBOpenConnectivityDatabase();

		/// Open/connect to the database
		virtual sw_status_t		openodbc(const SWString &params);

public:
		virtual ~DBOpenConnectivityDatabase();

		/// Check if the database is supported on this system
		virtual bool			isSupported();

		/// Close the database
		virtual sw_status_t		close();

		/// Check a table against the definitions
		virtual sw_status_t		checkTable(const SWString &name, const SWArray<DBTableField> &fieldDefs, const SWArray<DBTableIndex> &indexDefs);

		/// Exectute the SQL statement
		virtual sw_status_t		execute(const SWString &sql, DBRecordSet *pRecordSet);

protected:
		CDatabase	m_db;		///< The actual database

private:
		static SWInterlockedInt32	m_initCount;
		};

#endif // defined(SW_PLATFORM_WINDOWS)

#endif // __db_DBOpenConnectivityDatabase_h__
