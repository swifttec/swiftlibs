/**
*** @file		DBMySqlDatabase.h
*** @brief		Basic database access object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Defines the DBMySqlDatabase class
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __db_DBMySqlDatabase_h__
#define __db_DBMySqlDatabase_h__

#include <config/config.h>

#ifdef SW_CONFIG_USE_MYSQL

#include <db/DBConnection.h>

#include <swift/SWString.h>
#include <swift/SWLoadableModule.h>



// Disable warning about deprecated objects
#pragma warning(disable: 4995)


class DB_DLL_EXPORT DBMySqlDatabase : public DBConnection
		{
public:
		DBMySqlDatabase();
		virtual ~DBMySqlDatabase();

		/// Open the databaseImportedVideo
		virtual sw_status_t		opendb(const SWArray<SWNamedValue> &va);

		/// Close the database
		virtual sw_status_t		close();

		/// Check if the database is supported on this system
		virtual bool			isSupported();

		/// Check a table against the definitions
		virtual sw_status_t		checkTable(const SWString &name, const SWArray<DBTableField> &fieldDefs, const SWArray<DBTableIndex> &indexDefs);

		/// Exectute the SQL statement
		virtual sw_status_t		execute(const SWString &sql, DBRecordSet *pRecordSet);

		/// Read the table list from the database
		virtual sw_status_t		getTableList(SWStringArray &tlist);

		virtual sw_status_t		getTableInfo(const SWString &table, DBTableInfo &tinfo);
		virtual bool			tableExists(const SWString &table);
		virtual bool			tableHasField(const SWString &table, const SWString &field);
		virtual SWString		sqlBinaryValue(const void *pData, sw_size_t len) const;
		virtual SWString		sqlQuote(const SWString &s) const;

		// Reconnect support
		void *					getMySQLDB()	{ return m_pDB; }
		bool					reconnect();

private:
		bool					startMySQL();
		bool					stopMySQL();

private:
		void		*m_pDB;		///< The actual database
		SWString	m_host;
		SWString	m_username;
		SWString	m_passwd;
		SWString	m_database;
		int			m_port;

private:
		static SWLoadableModule		m_dll;
		};

#endif // SW_CONFIG_USE_MYSQL

#endif // __db_DBMySqlDatabase_h__
