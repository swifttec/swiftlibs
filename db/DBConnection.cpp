/**
*** @file		DBConnection.cpp
*** @brief		Basic database access object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Implements the DBConnection class
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <swift/SWTokenizer.h>
#include <swift/sw_crypt.h>


#include <db/DBDatabase.h>

#include <db/DBConnection.h>

DBConnection::DBConnection()
		{
		}


DBConnection::~DBConnection()
		{
		}


sw_status_t
DBConnection::loadRecordSet(DBRecordSet *pRS, SWCollectionIterator *pCollectionIterator)
		{
		pRS->m_iterator.clear();
		SWCollection::initIterator(pRS->m_iterator, pCollectionIterator);
		return SW_STATUS_SUCCESS;
		}


sw_status_t
DBConnection::open(const SWString &params)
		{
		// We have a connection, now attempt to open the database using the parameters given
		SWArray<SWNamedValue>	va;
		SWTokenizer				tok(params, "&");
		SWString				s, n, v;
		int						p;
		
		while (tok.hasMoreTokens())
			{
			s = tok.nextToken();
			p = s.first('=');
			if (p < 0)
				{
				n = s;
				v.empty();
				}
			else
				{
				n = s.left(p);
				v = s.mid(p+1);
				}

			va.add(SWNamedValue(n, v));
			}

		m_params = params;

		return opendb(va);
		}


SWString
DBConnection::getConnectionString() const
		{
		return m_params;
		}


bool
DBConnection::tableExists(const SWString &table)
		{
		DBTableInfo	ti;

		return (getTableInfo(table, ti) == SW_STATUS_SUCCESS);
		}


sw_status_t
DBConnection::dropTable(const SWString &table)
		{
		SWString	sql;

		sql = "drop table ";
		sql += table;

		return execute(sql, NULL);
		}


sw_status_t
DBConnection::renameTable(const SWString &oldname, const SWString &newname)
		{
		SWString	sql;

		sql = "alter table ";
		sql += oldname;
		sql += " rename to ";
		sql += newname;

		return execute(sql, NULL);
		}


bool
DBConnection::tableHasField(const SWString &table, const SWString &field)
		{
		DBTableInfo	ti;
		bool		found=false;

		if (getTableInfo(table, ti) == SW_STATUS_SUCCESS)
			{
			SWString	fn;

			for (int i=0;i<(int)ti.getFieldCount();i++)
				{
				ti.getFieldName(i, fn);
				if (fn == field)
					{
					found = true;
					break;
					}
				}
			}

		return found;
		}


		
SWString		
DBConnection::sqlBinaryValue(const void *pData, sw_size_t len) const
		{
		SWString	sql, ns;
		sw_size_t	pos;
		sw_byte_t	*p=(sw_byte_t *)pData;

		sql = "0x";
		for (pos=0;pos<len;pos++)
			{
			ns.format(_T("%02X"), (*p++) & 0xff);
			sql += ns;
			}

		return sql;
		}


// Default implementation
sw_status_t
DBConnection::checkTable(const SWString &table, const SWArray<DBTableField> &fieldDefs, const SWArray<DBTableIndex> &indexDefs)
		{
		sw_status_t			res=SW_STATUS_SUCCESS;
		DBTableInfo			ti;
		bool				create=false;
		SWString			sql;

		// If we can't read it we must create it!
		create = (getTableInfo(table, ti) != SW_STATUS_SUCCESS);
		
		sql.Empty();
		
		for (int i=0;i<(int)fieldDefs.size();i++)
			{
			const DBTableField	&fd=fieldDefs.get(i);
			bool	found=false;
			
			if (!create)
				{
				SWString	fn;

				for (int i=0;!found&&i<(int)ti.getFieldCount();i++)
					{
					ti.getFieldName(i, fn);
					if (fn == fd.name)
						{
						found = true;
						}
					}
				}
			
			if (!found)
				{
				if (sql.IsEmpty())
					{
					if (create) sql = _T("create table ");
					else sql = _T("alter table ");
					sql += table;
					
					if (create) sql += _T(" (");
					}
				else
					{
					sql += _T(", ");
					}
				
				sql += " ";
				if (!create)
					{
					sql += _T("ADD COLUMN ");
					}
				
				sql += fd.name;
				sql += " ";
				
				switch (fd.type)
					{
					case DBF_BOOLEAN:	sql += _T("BOOL");		break;		// A boolean value
					case DBF_INT8:		sql += _T("TINYINT");	break;		// An 8-bit signed integer
					case DBF_INT16:		sql += _T("SMALLINT");	break;		// A 16-bit signed integer
					case DBF_FLOAT: 	sql += _T("FLOAT");		break;		// A float
					case DBF_DOUBLE:	sql += _T("DOUBLE");	break;		// A double
					case DBF_DATE:		sql += _T("DATE");		break;		// A date/time (SWTime) stored in local time format
					case DBF_DATETIME:	sql += _T("DATETIME");	break;		// A date/time (SWTime) stored in local time format
					
					case DBF_MONEY:
					case DBF_INT32:	
						sql += _T("INT");
						break;		// A 32-bit signed integer

					case DBF_TEXT:
					case DBF_PASSWORD:	// An encrypted text field
						{
						SWString	s;
						
						if (fd.size < 16384) s.format(_T("VARCHAR(%d)"), fd.size);
						else s = _T("MEDIUMTEXT");
						sql += s;
						}
						break;

					case DBF_SMALL_BLOB:	sql += _T("TINYBLOB");		break;
					case DBF_BLOB:			sql += _T("BLOB");			break;
					case DBF_MEDIUM_BLOB:	sql += _T("MEDIUMBLOB");	break;
					case DBF_LARGE_BLOB:	sql += _T("LONGBLOB");		break;
					default:
						throw 1;
						break;
					}
				}
			}
			
		if (!sql.IsEmpty())
			{
			if (create) sql += _T(")");
			execute(sql, NULL);
			
			if (create)
				{
				bool				primary=true;

				// Need to create the indexes
				for (int i=0;i<(int)indexDefs.size();i++)
					{
					const DBTableIndex	&idx=indexDefs.get(i);

					sql = _T("alter table ");
					sql += table;
					sql += _T(" add ");

					if (primary) sql += _T("primary ");
					else if (idx.unique) sql += _T("unique ");

					sql += _T("key ");
					sql += _T("idx_");
					sql += idx.name;
					sql += _T(" (");
					sql += idx.fields;
					sql += _T(" )");
					
					execute(sql, NULL);
					}
				}
			}
		
		return res;
		}



SWString
DBConnection::sqlQuote(const SWString &s) const
		{
		SWString	res;
		
		res = s;
		res.replaceAll(_T("'"), _T("''"));
		res.insert(0, _T("'"));
		res += _T("'");

		return res;
		}



SWString
DBConnection::sqlLikeWildcard() const
		{
		SWString	res="%";

		return res;
		}



SWString
DBConnection::sqlDateString(const SWDate &dt) const
		{
		SWString	s;

		if (dt.julian() == 0)
			{
			s = _T("null");
			}
		else
			{
			s = _T("'");
			s += dt.toString(_T("YYYY-MM-DD"));
			s += _T("'");
			}
		
		return s;
		}


SWString
DBConnection::sqlDateTimeString(const SWLocalTime &t) const
		{
		SWString	s;
		
		if (t.julian() == 0)
			{
			s = _T("null");
			}
		else
			{
			s = _T("'");
			s += t.toString(_T("%Y-%m-%d %H:%M:%S"));
			s += _T("'");
			}
		
		return s;
		}


SWString
DBConnection::sqlBooleanString(bool v) const
		{
		SWString	s;
		
		if (v) s = _T("true");
		else s = _T("false");
		
		return s;
		}

		

SWString
DBConnection::sqlFieldValueString(const SWValue &v, const DBTableField &fd) const
		{
		SWString	res;
		
		switch (fd.type)
			{
			case DBF_PASSWORD:
				res = sqlQuote(sw_crypt_encryptString(v.toString()));
				break;
				
			case DBF_TEXT:
				res = sqlQuote(v.toString());
				break;
				
			case DBF_INT32:
				res = SWString::valueOf(v.toInt32());
				break;

			case DBF_MONEY:
				res = v.toMoney().toString();
				break;
			
			case DBF_BOOLEAN:
				res = sqlBooleanString(v.toBoolean());
				break;
			
			case DBF_DATE:
				res = sqlDateString(v.toDate());
				break;
			
			case DBF_TIMESTAMP:
			case DBF_DATETIME:
				res = sqlDateTimeString(v.toTime());
				break;

			default:
				// Unhandled data type
				throw SW_EXCEPTION_TEXT(DBException, "Unexpected data type encountered in DBConnection::sqlFieldValueString");
				break;
			}
		
		return res;
		}

