/**
*** @file		DBJetDatabase.h
*** @brief		Basic database access object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Defines the DBJetDatabase class
***
*** <b>Copyright 2007 SwiftTec. All rights reserved.</b>
**/
/****************************************************************************
** $Date: 2008/10/09 14:49:39 $
** $Author: simon $
** $Revision: 1.1 $
*****************************************************************************
** $Log: DBJetDatabase.h,v $
****************************************************************************/


#ifndef __db_DBJetDatabase_h__
#define __db_DBJetDatabase_h__

#if defined(SW_PLATFORM_WINDOWS) && defined(SW_BUILD_MFC)

#include <db/DBOpenConnectivityDatabase.h>

// Disable warning about deprecated objects
#pragma warning(disable: 4995)
#pragma warning(disable: 4251)

class DB_DLL_EXPORT DBJetDatabase : public DBOpenConnectivityDatabase
		{
public:
		DBJetDatabase();
		virtual ~DBJetDatabase();

		/// Open/connect to the database
		virtual sw_status_t		opendb(const SWArray<SWNamedValue> &va);

		virtual sw_status_t		getTableInfo(const SWString &table, DBTableInfo &tinfo);

		/// Check a table against the definitions
		virtual sw_status_t		checkTable(const SWString &name, const SWArray<DBTableField> &fieldDefs, const SWArray<DBTableIndex> &indexDefs);

		/// Read the table list from the database
		virtual sw_status_t		getTableList(SWStringArray &tlist);

		virtual SWString		sqlDateString(const SWDate &v) const;
		virtual SWString		sqlLikeWildcard() const;

		/// Return the filename of the actual database file.
		const SWString &		filename() const	{ return m_filename; }

private:
		SWString	m_filename;
		};


#endif // defined(SW_PLATFORM_WINDOWS)

#endif // __db_DBJetDatabase_h__
