/**
*** @file		DBFieldInfo.h
*** @brief		Object to represent info on a single database field
*** @version	1.0
*** @author		Simon Sparkes
***
*** Object to represent info on a single database field
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __db_DBFieldInfo_h__
#define __db_DBFieldInfo_h__

#include <db/types.h>

#include <swift/SWArray.h>
#include <swift/SWValue.h>




#if defined(SW_PLATFORM_WINDOWS)
	
#endif

class DB_DLL_EXPORT DBFieldInfo
		{
public:
		DBFieldInfo();
		DBFieldInfo(const DBFieldInfo &other);
		virtual ~DBFieldInfo();

		DBFieldInfo &	operator=(const DBFieldInfo &other);
		bool		operator==(const DBFieldInfo &other);
		
		/// Clear the record
		void	clear();


		/// Set the field name
		void				name(const SWString &v)		{ m_name = v; }

		/// Return the field name
		const SWString &	name() const				{ return m_name; }


		/// Set the field size
		void				size(sw_uint32_t v)			{ m_size = v; }

		/// Return the field size
		sw_uint32_t	size() const						{ return m_size; }


		/// Set the field type
		void				type(sw_uint32_t v)			{ m_type = v; }

		/// Return the field type
		sw_uint32_t	type() const						{ return m_type; }

		SWString			typeAsString() const;

		/// Set the field type
		void				flags(sw_uint32_t v)		{ m_flags = v; }

		/// Return the field flags
		sw_uint32_t	flags() const						{ return m_flags; }

protected:
		SWString			m_name;
		sw_uint32_t			m_type;
		sw_uint32_t			m_size;
		sw_uint32_t			m_flags;
		};

#endif // __db_DBFieldInfo_h__
