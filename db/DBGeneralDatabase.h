/**
*** @file		DBGeneralDatabase.h
*** @brief		Basic general database access object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Defines the DBGeneralDatabase class
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __db_DBGeneralDatabase_h__
#define __db_DBGeneralDatabase_h__

#include <db/types.h>
#include <db/DBRecordSet.h>
#include <db/DBDataObject.h>
#include <db/DBDatabase.h>
#include <db/DBOption.h>

#include <swift/SWString.h>
#include <swift/SWLocalTime.h>
#include <swift/SWIterator.h>
#include <swift/SWJob.h>




class DBConnection;

// Internal class
class DBGeneralDatabaseJob : public SWJob
		{
public:
		DBGeneralDatabaseJob(SWJob::JobProc fp, void *param) : SWJob(fp, param, NULL)	{ }

		virtual sw_status_t	run()	{ return SWJob::run(); }
		};


class DB_DLL_EXPORT DBGeneralDatabase : public DBDatabase
		{
public:
		enum
			{
			OpenNormal=0,
			OpenNoCheckTables=1,
			OpenNoRunJobs=2
			};

public:
		DBGeneralDatabase();
		virtual ~DBGeneralDatabase();

		/// Open/connect to the database
		virtual sw_status_t		open(int type, const SWString &params, int openflags);

		/// Open/connect to the database
		virtual sw_status_t		open(int type, const SWString &params);

		/// Close the database
		virtual sw_status_t		close();

		// Check all the pre-defined and register tables are correct
		virtual sw_status_t		checkTables();

		// Run all the jobs schedule for open
		virtual sw_status_t		runOpenJobs();

		// Run all the jobs schedule for open
		virtual sw_status_t		runCloseJobs();

		bool			getData(SWList<DBGenericDataObject> &dlist, const SWString &sql);
		bool			getData(SWArray<DBGenericDataObject> &dlist, const SWString &sql);

public:
		// Record locking (via a dedicated pre-defined table)
		bool		lockRecord(int id, int type, bool force=false);
		bool		unlockRecord(int id, int type);
		bool		lockRecord(const SWString &code, int type, bool force=false);
		bool		unlockRecord(const SWString &code, int type);

		// New ID control (via a dedicated pre-defined table) - uses a single code (normally the table name)
		int			getNextID(const SWString &code, bool update=true);
		int			setNextID(const SWString &code, int id);

		// ID control (via a dedicated pre-defined table)
		int			getNextID(int type, int code, bool update);
		int			getNextID(int type, const SWString &code, bool update);
		int			setNextID(int type, int code, int id);
		int			setNextID(int type, const SWString &code, int id);

		int			getRecordCount(const SWString &table, const SWString &where="");
	
		int			getOptionInteger(const SWString &section, const SWString &param, int defval=0);
		void		setOptionInteger(const SWString &section, const SWString &param, int val);
			
		SWString	getOptionString(const SWString &section, const SWString &param, const SWString &defval="");
		void		setOptionString(const SWString &section, const SWString &param, const SWString &val);

		double		getOptionDouble(const SWString &section, const SWString &param, double defval=0.0);
		void		setOptionDouble(const SWString &section, const SWString &param, double val);

		void		removeOption(const SWString &section, const SWString &param);

private: // members
		DBOption *	findOption(DBGeneralDatabase &db, const SWString &section, const SWString &param, bool create);

public: // Static methods
		static		int		registerDataObject(const char *name, DBDataObject *pObject);
		static		int		registerOpenJob(const char *name, SWJob::JobProc, void *param);
		static		int		registerCloseJob(const char *name, SWJob::JobProc, void *param);

private: // Static members
		static SWList<DBDataObject *>			*m_pDataObjectList;
		static SWList<DBGeneralDatabaseJob *>	*m_pOpenJobList;
		static SWList<DBGeneralDatabaseJob *>	*m_pCloseJobList;

private:
		SWArray<DBOption>	m_options;			///< The user-defined flags
		SWTime				m_optionsUpdated;	///< The time the user flags were last updated from the database
		double				m_optionsCacheValidTime;
		};


#define DB_REGISTER_DATA_OBJECT(DOBJ) \
		static int	rdo_##DOBJ=DBGeneralDatabase::registerDataObject(#DOBJ, new DOBJ())

#define DB_REGISTER_OPEN_JOB(FUNC, PARAM) \
		static int	roj_##FUNC=DBGeneralDatabase::registerOpenJob(#FUNC, FUNC, PARAM)

#define DB_REGISTER_CLOSE_JOB(FUNC, PARAM) \
		static int	rcj_##FUNC=DBGeneralDatabase::registerCloseJob(#FUNC, FUNC, PARAM)

// Reserved record lock IDs all have bit 31 set.
#define LK_IDCTRL_RECORD			0x80000001

extern DB_DLL_EXPORT DBGeneralDatabase	theGeneralDB;

#endif // __db_DBGeneralDatabase_h__
