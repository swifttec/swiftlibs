/**
*** @file		DBGenericDataObject.h
*** @brief		Basic database access object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Defines the DBGenericDataObject class
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __db_DBGenericDataObject_h__
#define __db_DBGenericDataObject_h__

#include <db/types.h>
#include <db/DBDataObject.h>

#include <swift/SWArray.h>

class DB_DLL_EXPORT DBGenericDataObject : public DBDataObject
		{
public:
		DBGenericDataObject();
		virtual ~DBGenericDataObject();

		bool	operator==(const DBGenericDataObject &other);
		
protected:
		virtual SWString	tableName() const;
		virtual int			tableVersion() const;
		virtual bool		getTableFieldDefs(DBFieldDef *&pDefs) const;
		virtual bool		getTableIndexDefs(DBIndexDef *&pDefs) const;
		virtual bool		getTableFieldDefs(SWArray<DBTableField> &da) const;
		virtual bool		getTableIndexDefs(SWArray<DBTableIndex> &da) const;
		};

#endif // __db_DBGenericDataObject_h__
