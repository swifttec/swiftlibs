/**
*** @file		DBDaoDatabase.h
*** @brief		Basic database access object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Defines the DBDaoDatabase class
***
*** <b>Copyright 2007 SwiftTec. All rights reserved.</b>
**/
/****************************************************************************
** $Date: 2012/05/18 21:43:15 $
** $Author: simon $
** $Revision: 1.12 $
*****************************************************************************
** $Log: DBDaoDatabase.h,v $
** Revision 1.12  2012/05/18 21:43:15  simon
** WIP
**
** Revision 1.11  2012/02/27 23:55:58  simon
** Removed comment
** Committed on the Free edition of March Hare Software CVSNT Server.
** Upgrade to CVS Suite for more features and support:
** http://march-hare.com/cvsnt/
**
** Revision 1.10  2012/02/24 09:36:39  simon
** Added support for VC10 compiler and 64-bit native compilation
**
** Revision 1.9  2011/05/26 08:29:24  simon
** Added SQL Like Wildcard support which varies across database types
** 
** Revision 1.8  2009/12/02 07:49:14  simon
** Added support for more operations
**
** Revision 1.7  2008/11/14 08:41:25  simon
** WIP
**
** Revision 1.6  2008/10/09 14:49:39  simon
** Updated database model to include SqlServer and improved interface
**
** Revision 1.5  2007/09/15 06:30:28  simon
** WIP
**
** Revision 1.4  2007/07/17 07:02:28  simon
** no message
**
** Revision 1.3  2007/06/19 07:30:11  simon
** WIP
**
** Revision 1.2  2007/05/11 13:58:59  simon
** WIP
**
** Revision 1.1  2007/03/12 11:46:06  simon
** Work in progress
**
****************************************************************************/


#ifndef __db_DBDaoDatabase_h__
#define __db_DBDaoDatabase_h__

#if defined(SW_PLATFORM_WINDOWS) && defined(SW_BUILD_MFC) && !defined(_WIN64)

#include <db/DBConnection.h>

#include <swift/SWString.h>
#include <swift/SWInterlockedInt32.h>


// Disable warning about deprecated objects
#pragma warning(disable: 4995)
#pragma warning(disable: 4251)

class CDaoDatabase;


class DB_DLL_EXPORT DBDaoDatabase : public DBConnection
		{
public:
		DBDaoDatabase();
		virtual ~DBDaoDatabase();

		/// Open the database
		virtual sw_status_t		opendb(const SWArray<SWNamedValue> &va);

		/// Close the database
		virtual sw_status_t		close();

		/// Check if the database is supported on this system
		virtual bool			isSupported();

		/// Check a table against the definitions
		virtual sw_status_t		checkTable(const SWString &name, const SWArray<DBTableField> &fieldDefs, const SWArray<DBTableIndex> &indexDefs);

		/// Exectute the SQL statement
		virtual sw_status_t		execute(const SWString &sql, DBRecordSet *pRecordSet);

		/// Read the table list from the database
		virtual sw_status_t		getTableList(SWStringArray &tlist);

		virtual sw_status_t		getTableInfo(const SWString &table, DBTableInfo &tinfo);
		virtual bool			tableExists(const SWString &table);
		virtual bool			tableHasField(const SWString &table, const SWString &field);

		virtual SWString		sqlDateString(const SWDate &v) const;
		virtual SWString		sqlLikeWildcard() const;

		virtual sw_status_t		renameTable(const SWString &oldname, const SWString &newname);

		/// Return the filename of the actual database file.
		const SWString &		filename() const	{ return m_filename; }

private:
		void	getDaoFieldInfo(const DBTableField &fd, short &daotype, long &daolen);

private:
		CDaoDatabase	*m_pDB;		///< The actual database
		SWString		m_filename;	///< The filename of the database

private:
		static SWInterlockedInt32	m_daoInitCount;
		};

#endif // defined(SW_PLATFORM_WINDOWS) && !defined(_WIN64)

#endif // __db_DBDaoDatabase_h__
