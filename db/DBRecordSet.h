/**
*** @file		DBRecordSet.h
*** @brief		Object to represent a single database record (row)
*** @version	1.0
*** @author		Simon Sparkes
***
*** Object to represent a single database record (row)
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __db_DBRecordSet_h__
#define __db_DBRecordSet_h__

#include <db/DBRecord.h>
#include <db/DBFieldInfo.h>

#include <swift/SWList.h>
#include <swift/SWStringArray.h>
#include <swift/SWValue.h>




class DB_DLL_EXPORT DBRecordSet
		{
friend class DBDatabase;
friend class DBConnection;

public:
		DBRecordSet();
		virtual ~DBRecordSet();

		/// Clear the record
		void	clear();

		/// Return the number of fields (count)
		sw_size_t	getFieldCount() const						{ return m_fieldinfo.size(); }

		/// Return the nth field value
		void		getFieldName(int n, SWString &v) const;

		/// Return the nth field value
		void		getFieldInfo(int n, DBFieldInfo &v) const;

		// Return an iterator over the records
		SWIterator<DBRecord>	iterator()						{ return m_iterator; }

		/// Add a field
		void		setFieldInfo(int n, const DBFieldInfo &v)	{ m_fieldinfo[n] = v; }
		void		clearFieldInfo()							{ m_fieldinfo.clear(); }

		bool		hasNext()									{ return m_iterator.hasNext(); }
		bool		hasPrevious()								{ return m_iterator.hasPrevious(); }

protected:

protected:
		SWArray<DBFieldInfo>	m_fieldinfo;
		SWIterator<DBRecord>	m_iterator;
		};


#endif // __db_DBRecordSet_h__
