
/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#pragma once

#include <db/types.h>
#include <db/DBDataObject.h>

class DBDatabase;

class DB_DLL_EXPORT DBOption : public DBDataObject
		{
public:
		DBOption();
		DBOption(DBDatabase &db, const SWString &section, const SWString &param);
		DBOption(const SWString &section, const SWString &param, int v);
		DBOption(const SWString &section, const SWString &param, const SWString &v);
		virtual ~DBOption();

		DBOption(const DBOption &other);
		DBOption &	operator=(const DBOption &other);
		bool	operator==(const DBOption &other);

		// Load this record from the database
		bool				load(DBDatabase &db, const SWString &section, const SWString &param);

		// Save this record into the database
		virtual bool		save(DBDatabase &db, bool forceinsert=false);
		
		// Save this record into the database
		virtual bool		removeFromDB(DBDatabase &db);
		
		SWString			section() const						{ return getString("section"); }
		void				section(const SWString &v)			{ set("section", v); }

		SWString			param() const						{ return getString("param"); }
		void				param(const SWString &v)			{ set("param", v); }

		SWString			value() const						{ return getString("sv"); }
		void				value(const SWString &v)			{ set("sv", v); }

		SWString			getStringValue() const				{ return getString("sv"); }
		void				setStringValue(const SWString &v)	{ set("sv", v); }

		SWInt32				getIntegerValue() const				{ return getInt32("sv"); }
		void				setIntegerValue(int v)				{ set("sv", v); }

		double				getDoubleValue() const				{ return getDouble("sv"); }
		void				setDoubleValue(double v)			{ set("sv", v); }

public: // Overrides
		DB_DECLARE_REGISTER_TABLE(DBOption);

public: // Static methods
		DB_DECLARE_LOADRECORDS_ARRAY(DBOption);
		DB_DECLARE_LOADRECORDS_LIST(DBOption);
		};

