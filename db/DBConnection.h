/**
*** @file		DBConnection.h
*** @brief		Basic database access object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Defines the DBConnection class
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __db_DBConnection_h__
#define __db_DBConnection_h__

#include <db/types.h>
#include <db/DBRecordSet.h>
#include <db/DBTableInfo.h>
#include <db/DBTableField.h>
#include <db/DBTableIndex.h>

#include <swift/SWString.h>
#include <swift/SWArray.h>
#include <swift/SWNamedValue.h>
#include <swift/SWLocalTime.h>




class DB_DLL_EXPORT DBConnection
		{
protected:
		DBConnection();

		/// Internal method to load a record set
		sw_status_t				loadRecordSet(DBRecordSet *pRS, SWCollectionIterator *pCollectionIterator);

public:
		virtual ~DBConnection();

		/// Open/connect to the database
		virtual sw_status_t		open(const SWString &params);

		/// Open/connect to the database
		virtual sw_status_t		opendb(const SWArray<SWNamedValue> &va)=0;

		/// Close the database
		virtual sw_status_t		close()=0;

		/// Close the database
		virtual bool			isSupported()=0;

		/// Check a table against the definitions
		virtual sw_status_t		checkTable(const SWString &name, const SWArray<DBTableField> &fieldDefs, const SWArray<DBTableIndex> &indexDefs);

		/// Exectute the SQL statement
		virtual sw_status_t		execute(const SWString &sql, DBRecordSet *pRecordSet)=0;

		/// Read the info for the specified table
		virtual sw_status_t		getTableInfo(const SWString &table, DBTableInfo &tinfo)=0;

		/// Read the table list from the database
		virtual sw_status_t		getTableList(SWStringArray &tlist)=0;

		virtual bool			tableExists(const SWString &table);

		virtual bool			tableHasField(const SWString &table, const SWString &field);

		virtual SWString		sqlBinaryValue(const void *pData, sw_size_t len) const;
		virtual SWString		sqlQuote(const SWString &s) const;
		virtual SWString		sqlLikeWildcard() const;
		virtual SWString		sqlDateString(const SWDate &v) const;
		virtual SWString		sqlDateTimeString(const SWLocalTime &v) const;
		virtual SWString		sqlFieldValueString(const SWValue &v, const DBTableField &fd) const;
		virtual SWString		sqlBooleanString(bool v) const;

		virtual sw_status_t		dropTable(const SWString &table);
		virtual sw_status_t		renameTable(const SWString &oldname, const SWString &newname);


		virtual SWString		getConnectionString() const;

protected:
		SWString	m_params;	//< The opening parameters
		};


#endif // __db_DBConnection_h__
