
/*********************************************************************************
Copyright (c) 2006-2016 SwiftTec Ltd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "StdAfx.h"

#include <db/DBSqlStatement.h>
#include <db/DBGeneralDatabase.h>

using namespace DB;

DB_REGISTER_DATA_OBJECT(DBSqlStatement);


static DBFieldDef	table_fieldlist[] =
{
	{	"section",		DBF_TEXT,		64,	0	},
	{	"param",		DBF_TEXT,		64,	0	},
	{	"sv",			DBF_TEXT,		64,	0	},

	// End of list marker
	{	NULL,			DBF_MAX,		0,	0	}
};

static DBIndexDef table_indexlist[] =
{
	{	"primary",		"section,param",	true	},

	// End of list marker
	{	NULL,			NULL,	true	}
};


DBSqlStatement::DBSqlStatement()
		{
		}


DBSqlStatement::DBSqlStatement(const DBSqlStatement &other)
		{
		*this = other;
		}


DBSqlStatement::DBSqlStatement(DBDatabase &db, const SWString &s, const SWString &p)
		{
		if (!load(db, s, p))
			{
			section(s);
			param(p);
			}
		}


DBSqlStatement::DBSqlStatement(const SWString &s, const SWString &p, int v)
		{
		section(s);
		param(p);
		setIntegerValue(v);
		}


DBSqlStatement::DBSqlStatement(const SWString &s, const SWString &p, const SWString &v)
		{
		section(s);
		param(p);
		setStringValue(v);
		}


DBSqlStatement::~DBSqlStatement()
		{
		}



DBSqlStatement &
DBSqlStatement::operator=(const DBSqlStatement &other)
		{
		DBDataObject::operator=(other);

#define COPY(x)	x = other.x
//		COPY(m_ownerString);
#undef COPY

		return *this;
		}


bool
DBSqlStatement::operator==(const DBSqlStatement &other)
		{
		SWValue	s1, s2, p1, p2;
		
		get("section", s1);
		other.get("section", s2);
		get("param", p1);
		other.get("param", p2);
		
		return (s1 == s2 && p1 == p2);
		}


SWString
DBSqlStatement::table() const
		{
		return "dboption";
		}


bool
DBSqlStatement::getTableFieldDefs(DBFieldDef *&pDefs) const
		{
		pDefs = table_fieldlist;
		return true;
		}


bool
DBSqlStatement::getTableIndexDefs(DBIndexDef *&pDefs) const
		{
		pDefs = table_indexlist;
		return true;
		}		


bool
DBSqlStatement::load(DBDatabase &db, const SWString &section, const SWString &param)
		{
		SWString	where;
		
		where = _T("where section = ");
		where += db.sqlQuote(section);
		where += _T(" and param = ");
		where += db.sqlQuote(param);
		
		m_loaded = (db.getRecord(*this, where) == SW_STATUS_SUCCESS);
		
		return m_loaded;
		}


bool
DBSqlStatement::save(DBDatabase &db, bool forceinsert)
		{
		bool		res;
		
		if (isNewRecord() || forceinsert)
			{
			res = (db.insertRecord(*this) == SW_STATUS_SUCCESS);
			}
		else
			{
			SWString	sql;

			sql = _T("where section = ");
			sql += db.sqlQuote(section());
			sql += _T(" and param = ");
			sql += db.sqlQuote(param());
			res = (db.updateRecord(*this, sql) == SW_STATUS_SUCCESS);
			}
		
		return res;
		}

