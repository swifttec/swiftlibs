/**
*** @file		DBSqlServerDatabase.h
*** @brief		Basic database access object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Defines the DBSqlServerDatabase class
***
*** <b>Copyright 2007 SwiftTec. All rights reserved.</b>
**/
/****************************************************************************
** $Date: 2009/02/03 09:48:08 $
** $Author: simon $
** $Revision: 1.3 $
*****************************************************************************
** $Log: DBSqlServerDatabase.cpp,v $
** Revision 1.3  2009/02/03 09:48:08  simon
** Updated to allow UNICODE builds
**
** Revision 1.2  2008/11/14 08:41:25  simon
** WIP
**
** Revision 1.1  2008/10/09 14:49:39  simon
** Updated database model to include SqlServer and improved interface
**
****************************************************************************/

#include "stdafx.h"

#if defined(SW_PLATFORM_WINDOWS) && defined(SW_BUILD_MFC)

// Swift includes
#include <swift/SWTokenizer.h>
#include <swift/SWIntegerArray.h>
#include <swift/SWStringArray.h>
#include <swift/SWFileInfo.h>
#include <swift/SWCollectionIterator.h>
#include <swift/SWGuard.h>
#include <swift/SWLocalTime.h>

// DB includes
#include <db/DBSqlServerDatabase.h>
#include <db/DBRecord.h>
#include <db/DBDatabase.h>

// Disable warning about deprecated objects
#pragma warning(disable: 4995)
#pragma warning(disable: 4251)


DBSqlServerDatabase::DBSqlServerDatabase()
		{
		}


DBSqlServerDatabase::~DBSqlServerDatabase()
		{
		// Make sure the database is actually closed
		close();
		}


SWString
DBSqlServerDatabase::sqlBooleanString(bool v) const
		{
		SWString	s;
		
		if (v) s = _T("1");
		else s = _T("0");
		
		return s;
		}


sw_status_t
DBSqlServerDatabase::opendb(const SWArray<SWNamedValue> &va)
		{
		bool		createflag=true;
		sw_status_t	res=SW_STATUS_SUCCESS;
		SWString	connection;
		SWString	host, username, passwd, database;
		
		for (int i=0;i<(int)va.size();i++)
			{
			if (va.get(i).name() == "host") host = va.get(i).toString();
			else if (va.get(i).name() == "username") username = va.get(i).toString();
			else if (va.get(i).name() == "passwd") passwd = va.get(i).toString();
			else if (va.get(i).name() == "database") database = va.get(i).toString();
			else if (va.get(i).name() == "create") createflag = va.get(i).toBoolean();
			}

		connection = "Driver={SQL Server};Server=";
		connection += host;
		connection += ";Uid=";
		connection += username;
		connection += ";Pwd=";
		connection += passwd;
		connection += ";";

		res = openodbc(connection);
		if (res == SW_STATUS_SUCCESS)
			{
			if (!database.isEmpty())
				{
				SWString	sql;
				
				sql = _T("use ");
				sql += database;

				if ((res = execute(sql, NULL)) != SW_STATUS_SUCCESS && createflag)
					{
					sql = _T("create database ");
					sql += database;
					res = execute(sql, NULL);

					if (res == SW_STATUS_SUCCESS)
						{
						sql = _T("use ");
						sql += database;
						res = execute(sql, NULL);
						}
					}
				}
			}
		
		return res;
		}


sw_status_t
DBSqlServerDatabase::getTableInfo(const SWString &table, DBTableInfo &ti)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		SWString	sql;
		DBRecordSet	rset;

		sql = _T("select column_name, data_type, character_maximum_length, is_nullable from information_schema.columns where table_name=");
		sql += sqlQuote(table);

		if ((res = execute(sql, &rset)) == SW_STATUS_SUCCESS)
			{
			SWIterator<DBRecord>	it=rset.iterator();
			int						f=0;

			while (rset.hasNext())
				{
				DBRecord	&rec=*it.next();
				SWValue		v;
				DBFieldInfo	dbfi;

				rec.getValue(0, v);
				dbfi.name(v.toString());

				rec.getValue(2, v);
				dbfi.size(v.toInt32());

				dbfi.flags(0);

				rec.getValue(1, v);
				SWString	tstr=v.toString();

				if (tstr.compareNoCase("int") == 0) dbfi.type(DBF_INT32);
				else if (tstr.compareNoCase("varchar") == 0) dbfi.type(DBF_TEXT);
				else if (tstr.compareNoCase("datetime") == 0) dbfi.type(DBF_DATETIME);
				else if (tstr.compareNoCase("tinyint") == 0) dbfi.type(DBF_INT8);
				else if (tstr.compareNoCase("tinyint") == 0) dbfi.type(DBF_INT8);
				else dbfi.type(DBF_INVALID);

				ti.setFieldInfo(f++, dbfi);
				}
			}

		return res;
		}




sw_status_t
DBSqlServerDatabase::checkTable(const SWString &table, const SWArray<DBTableField> &fieldDefs, const SWArray<DBTableIndex> &indexDefs)
		{
		sw_status_t		res=SW_STATUS_SUCCESS;
		SWString		sql;

		sql = "SELECT a.[name] as 'Table',"
				" b.[name] as 'Column',"
				" c.[name] as 'Datatype',"
				" b.[length] as 'Length',"
				" CASE"
				" WHEN b.[cdefault] > 0 THEN d.[text]"
				" ELSE NULL"
				" END as 'Default',"
				" CASE"
				" WHEN b.[isnullable] = 0 THEN 'No'"
				" ELSE 'Yes'"
				" END as 'Nullable'"
				" FROM  sysobjects a"
				" INNER JOIN syscolumns b"
				" ON  a.[id] = b.[id]"
				" INNER JOIN systypes c"
				" ON  b.[xtype] = c.[xtype]"
				" LEFT JOIN syscomments d"
				" ON  b.[cdefault] = d.[id]"
				" WHERE a.[xtype] = 'u'"  // -- 'u' for user tables, 'v' for views.
				" AND a.[name]=";
		sql += sqlQuote(table);
		sql += " AND  a.[name] <> 'dtproperties'"
				" ORDER BY a.[name],b.[colorder]";

		DBRecordSet				rset;
		SWStringArray			fa;		// Field names
		SWStringArray			ta;		// Type strings
		SWIntegerArray			la;		// Field lengths
		SWValue					v;
		bool					create=true;
		int						r;

		res = execute(sql, &rset);
		if (res == SW_STATUS_SUCCESS)
			{
			SWIterator<DBRecord>	it=rset.iterator();

			while (it.hasNext())
				{
				DBRecord	*pRecord;

				pRecord = it.next();

				// column
				pRecord->getValue(1, v);
				fa.add(v.toString());

				// type
				pRecord->getValue(2, v);
				ta.add(v.toString());

				// column
				pRecord->getValue(3, v);
				la.add(v.toInt32());

				create = false;
				}
			}

			
		sql.Empty();
			
		SWString	extra;
		bool		autoincr=false;
		bool		addkeyword=false;

		for (int i=0;i<(int)fieldDefs.size();i++)
			{
			const DBTableField	&fd=fieldDefs.get(i);
			bool	found=false;
			
			if (!create)
				{
				for (int i=0;!found&&i<(int)fa.size();i++)
					{
					if (fa[i] == fd.name)
						{
						found = true;

						// If a test field make sure that if needed we increase the size to match
						// but never reduce size
						if (fd.type == DBF_TEXT || fd.type == DBF_PASSWORD)
							{
							SWString	ts=ta[i];

							if (ts.compareNoCase("NVARCHAR") == 0)
								{
								int	len;

								len = la[i];

								if (len > 0 && len < fd.size)
									{
									SWString	sqlGrow;

									sqlGrow = "alter table ";
									sqlGrow += table;
									sqlGrow += " modify ";
									sqlGrow += fd.name;
									sqlGrow += " ";

									SWString	s;
						
									if (fd.size < 4000) s.format("NVARCHAR(%d)", fd.size);
									else s = "NTEXT";

									sqlGrow += s;
									r = execute(sqlGrow, NULL);
									}
								}
							}
						}
					}
				}
			
			if (!found)
				{
				if (sql.IsEmpty())
					{
					if (create) sql = "CREATE TABLE ";
					else sql = "ALTER TABLE ";
					sql += table;
					
					if (create) sql += " (";
					}
				else
					{
					sql += ", ";
					}
				
				sql += " ";
				if (!create && !addkeyword)
					{
					sql += "ADD ";
					addkeyword = true;
					}
				
				sql += fd.name;
				sql += " ";
				
				switch (fd.type)
					{
					case DBF_BOOLEAN:	sql += "TINYINT";	break;		// A boolean value
					case DBF_INT8:		sql += "TINYINT";	break;		// An 8-bit signed integer
					case DBF_INT16:		sql += "SMALLINT";	break;		// A 16-bit signed integer
					case DBF_FLOAT: 	sql += "FLOAT";		break;		// A float
					case DBF_DOUBLE:	sql += "FLOAT";		break;		// A double
					case DBF_DATE:		sql += "DATE";		break;		// A date/time (SWTime) stored in local time format
					case DBF_DATETIME:	sql += "DATETIME";	break;		// A date/time (SWTime) stored in local time format
					case DBF_TIMESTAMP:	sql += "DATETIME";	break;		// A date/time (SWTime) stored in local time format
					
					case DBF_MONEY:
					case DBF_INT32:	
						sql += "INT";
						break;		// A 32-bit signed integer

					case DBF_INT64:	
						sql += "BIGINT";
						break;		// A 64-bit signed integer

					case DBF_TEXT:
					case DBF_PASSWORD:	// An encrypted text field
						{
						SWString	s;
						
						if (fd.size < 4000) s.format("NVARCHAR(%d)", fd.size);
						else s = "NTEXT";
						sql += s;
						}
						break;

					case DBF_SMALL_BLOB:	sql += "BINARY";		break;
					case DBF_BLOB:			sql += "VARBINARY(max)";			break;
					case DBF_MEDIUM_BLOB:	sql += "VARBINARY(max)";	break;
					case DBF_LARGE_BLOB:	sql += "VARBINARY(max)";		break;
					default:
						throw 1;
						break;
					}

				if ((fd.attributes & DBA_NOTNULL) != 0) sql += " NOT NULL";
				if ((fd.attributes & DBA_AUTOINC) != 0)
					{
					if (autoincr)
						{
						// We can only have one auto-inc field
						throw SW_EXCEPTION_TEXT(DBException, "Only one auto-increment field allowed per table");
						}

					// Enforces primary key
					sql += " PRIMARY KEY IDENTITY";
					}
				}
			}
			
		sql += extra;

		if (!sql.IsEmpty())
			{
			if (create) sql += ")";

			r = execute(sql, NULL);
			
			if (create)
				{
				bool				primary=true;

				// Need to create the indexes
				for (int i=0;i<(int)indexDefs.size();i++)
					{
					const DBTableIndex	&fd=indexDefs.get(i);

					sql = "alter table ";
					sql += table;
					sql += " add ";

					
					if (fd.unique)
						{
						if (primary)
							{
							sql += "primary ";
							primary = false;
							}
						else
							{
							sql += "unique ";
							}
						}
						
					sql += "key ";
					sql += "idx_";
					sql += fd.name;
					sql += " (";
					sql += fd.fields;
					sql += " )";
					
					r = execute(sql, NULL);

					}
				}
			}

		return res;
		}



bool
DBSqlServerDatabase::tableExists(const SWString &table)
		{
		SWString		sql;

		sql = "SELECT a.[name] as 'Table',"
				" b.[name] as 'Column',"
				" c.[name] as 'Datatype',"
				" b.[length] as 'Length',"
				" CASE"
				" WHEN b.[cdefault] > 0 THEN d.[text]"
				" ELSE NULL"
				" END as 'Default',"
				" CASE"
				" WHEN b.[isnullable] = 0 THEN 'No'"
				" ELSE 'Yes'"
				" END as 'Nullable'"
				" FROM  sysobjects a"
				" INNER JOIN syscolumns b"
				" ON  a.[id] = b.[id]"
				" INNER JOIN systypes c"
				" ON  b.[xtype] = c.[xtype]"
				" LEFT JOIN syscomments d"
				" ON  b.[cdefault] = d.[id]"
				" WHERE a.[xtype] = 'u'"  // -- 'u' for user tables, 'v' for views.
				" AND a.[name]=";
		sql += sqlQuote(table);
		sql += " AND  a.[name] <> 'dtproperties'"
				" ORDER BY a.[name],b.[colorder]";

		DBRecordSet				rset;
		bool					res=false;

		if (execute(sql, &rset) == SW_STATUS_SUCCESS)
			{
			SWIterator<DBRecord>	it=rset.iterator();
			SWValue					v;

			while (it.hasNext())
				{
				// We don't care what we got, just that we got something!
				res = true;
				break;
				}
			}

		return res;

		}


bool
DBSqlServerDatabase::tableHasField(const SWString &table, const SWString &field)
		{
		SWString		sql;

		sql = "SELECT a.[name] as 'Table',"
				" b.[name] as 'Column',"
				" c.[name] as 'Datatype',"
				" b.[length] as 'Length',"
				" CASE"
				" WHEN b.[cdefault] > 0 THEN d.[text]"
				" ELSE NULL"
				" END as 'Default',"
				" CASE"
				" WHEN b.[isnullable] = 0 THEN 'No'"
				" ELSE 'Yes'"
				" END as 'Nullable'"
				" FROM  sysobjects a"
				" INNER JOIN syscolumns b"
				" ON  a.[id] = b.[id]"
				" INNER JOIN systypes c"
				" ON  b.[xtype] = c.[xtype]"
				" LEFT JOIN syscomments d"
				" ON  b.[cdefault] = d.[id]"
				" WHERE a.[xtype] = 'u'"  // -- 'u' for user tables, 'v' for views.
				" AND a.[name]=";
		sql += sqlQuote(table);
		sql += " AND  a.[name] <> 'dtproperties'"
				" ORDER BY a.[name],b.[colorder]";

		DBRecordSet				rset;
		bool					res=false;

		if (execute(sql, &rset) == SW_STATUS_SUCCESS)
			{
			SWIterator<DBRecord>	it=rset.iterator();
			SWValue					v;

			while (it.hasNext())
				{
				DBRecord	*pRecord;

				pRecord = it.next();

				// column
				pRecord->getValue(1, v);
				if (field == v.toString())
					{
					res = true;
					break;
					}
				}
			}

		return res;
		}



sw_status_t	
DBSqlServerDatabase::getTableList(SWStringArray &tlist)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		return res;
		}
		
#endif //  defined(SW_PLATFORM_WINDOWS)
