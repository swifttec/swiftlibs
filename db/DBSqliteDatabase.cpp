
/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"

// Swift includes
#include <swift/SWTokenizer.h>
#include <swift/SWStringArray.h>
#include <swift/SWFileInfo.h>
#include <swift/SWCollectionIterator.h>
#include <swift/SWGuard.h>
#include <swift/SWLocalTime.h>
#include <swift/SWInterlockedInt32.h>
#include <swift/SWFolder.h>
#include <swift/SWFilename.h>



// DB includes
#include <db/DBSqliteDatabase.h>
#include <db/DBRecord.h>
#include <db/DBDatabase.h>

#include "sqlite3.h"

// Disable warning about deprecated objects
#pragma warning(disable: 4995)



// Internal (hidden) helper classes

DBSqliteDatabase::DBSqliteDatabase() :
	m_pDB(NULL)
		{
		}


DBSqliteDatabase::~DBSqliteDatabase()
		{
		// Make sure the database is actually closed
		close();
		}
		

SWString
DBSqliteDatabase::sqlBooleanString(bool v) const
		{
		SWString	s;
		
		if (v) s = _T("1");
		else s = _T("0");
		
		return s;
		}




SWString
DBSqliteDatabase::sqlDateString(const SWDate &dt) const
		{
		SWString	s;

		if (dt.julian() == 0)
			{
			s = _T("null");
			}
		else
			{
			s = SWString::valueOf(dt.julian());
			}
		
		return s;
		}


SWString
DBSqliteDatabase::sqlDateTimeString(const SWLocalTime &t) const
		{
		SWString	s;
		
		if (t.julian() == 0)
			{
			s = _T("null");
			}
		else
			{
			s += SWString::valueOf(t.toDouble());
			}
		
		return s;
		}



bool
DBSqliteDatabase::isSupported()
		{
		return true;
		}


sw_status_t
DBSqliteDatabase::opendb(const SWArray<SWNamedValue> &va)
		{
		SWString	filename;
		bool		createflag=true;
		sw_status_t	res=SW_STATUS_SUCCESS;
		SWString	journalmode="";
		SWString	synchronous="NORMAL";

		for (int i=0;i<(int)va.size();i++)
			{
			if (va.get(i).name() == "filename") filename = va.get(i).toString();
			else if (va.get(i).name() == "create") createflag = va.get(i).toBoolean();
			else if (va.get(i).name() == "journalmode") journalmode = va.get(i).toString();
			else if (va.get(i).name() == "synchronous") synchronous = va.get(i).toString();
			}

		if (m_pDB != NULL)
			{
			res = SW_STATUS_ALREADY_OPEN;
			}
		else if (!filename.isEmpty())
			{
			bool	created=false;

			if (SWFileInfo::isFile(filename))
				{
				if (!SWFileInfo::isWriteable(filename))
					{
					res = SW_STATUS_READONLY;
					}
				}

			m_filename.empty();

			if (res == SW_STATUS_SUCCESS)
				{
				if (sqlite3_open(filename, &m_pDB) == 0)
					{
					m_filename = filename;

					// Set up database for performance
					if (!journalmode.isEmpty()) execute("PRAGMA journal_mode = " + journalmode, NULL);
					if (!synchronous.isEmpty()) execute("PRAGMA synchronous = " + synchronous, NULL);
					}
				else
					{
					res = SW_STATUS_DB_ERROR;
					}
				}
			}
		else
			{
			res = SW_STATUS_DB_ERROR;
			m_filename.empty();
			}
		
		return res;
		}
		
		
sw_status_t
DBSqliteDatabase::close()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		
		if (m_pDB != NULL)
			{
			sqlite3_close(m_pDB);
			m_pDB = NULL;
			}
		
		return res;
		}



sw_status_t
DBSqliteDatabase::checkTable(const SWString &table, const SWArray<DBTableField> &fieldDefs, const SWArray<DBTableIndex> &indexDefs)
		{
		sw_status_t			res=SW_STATUS_SUCCESS;
		SWString			sql;
		DBRecordSet			rs;
		bool				create=true;
		SWStringArray		fa;		// Field names
		SWStringArray		ta;		// Type strings
		int					r;
		
		sql = "pragma table_info(";
		sql += table;
		sql += ")";

		if ((res = execute(sql, &rs)) == SW_STATUS_SUCCESS)
			{
			SWIterator<DBRecord>	it=rs.iterator();

			while (it.hasNext())
				{
				DBRecord	*pRecord=it.next();
				SWValue		v;

				pRecord->getValue(1, v);
				fa.add(v.toString());	// Name
				pRecord->getValue(2, v);
				ta.add(v.toString());	// type
				create = false;
				}
			}

		sql.Empty();
		
		SWString	extra;
		bool		autoincr=false;

		for (int i=0;i<(int)fieldDefs.size();i++)
			{
			const DBTableField	&fd=fieldDefs.get(i);
			bool	found=false;
			
			if (!create)
				{
				for (int i=0;!found&&i<(int)fa.size();i++)
					{
					if (fa[i] == fd.name)
						{
						found = true;

						// If a test field make sure that if needed we increase the size to match
						// but never reduce size
						if (fd.type == DBF_TEXT || fd.type == DBF_PASSWORD)
							{
							SWString	ts=ta[i];

							if (ts.startsWith("varchar(", SWString::ignoreCase))
								{
								int	len;

								ts.remove(0, 8);
								len = ts.toInt32(10);

								if (len > 0 && len < fd.size)
									{
									SWString	sqlGrow;

									sqlGrow = "alter table ";
									sqlGrow += table;
									sqlGrow += " modify ";
									sqlGrow += fd.name;
									sqlGrow += " ";

									SWString	s;
						
									if (fd.size < 16384) s.format("VARCHAR(%d)", fd.size);
									else s = "MEDIUMTEXT";

									sqlGrow += s;
									r = execute(sqlGrow, NULL);
									}
								}
							}
						}
					}
				}
			
			if (!found)
				{
				if (sql.IsEmpty())
					{
					if (create)
						{
						sql = "create table ";
						sql += table;
						sql += " (";
						}
					}
				else
					{
					if (create) sql += ", ";
					else sql += "; ";
					}
				
				sql += " ";
				if (!create)
					{
					sql = "alter table ";
					sql += table;
					sql += " ADD COLUMN ";
					}
				
				sql += fd.name;
				sql += " ";
				
				switch (fd.type)
					{
					case DBF_BOOLEAN:	sql += "BOOLEAN";		break;		// A boolean value
					case DBF_INT8:		sql += "INTEGER";	break;		// An 8-bit signed integer
					case DBF_INT16:		sql += "INTEGER";	break;		// A 16-bit signed integer
					case DBF_FLOAT: 	sql += "FLOAT";		break;		// A float
					case DBF_DOUBLE:	sql += "DOUBLE";	break;		// A double
					case DBF_DATE:		sql += "TIMESTAMP";		break;		// A date/time (SWTime) stored in local time format
					case DBF_DATETIME:	sql += "TIMESTAMP";	break;		// A date/time (SWTime) stored in local time format
					case DBF_TIMESTAMP:	sql += "TIMESTAMP";	break;		// A date/time (SWTime) stored in local time format
					
					case DBF_MONEY:
					case DBF_INT32:	
						sql += "INTEGER";
						break;		// A 32-bit signed integer

					case DBF_INT64:	
						sql += "INTEGER";
						break;		// A 64-bit signed integer

					case DBF_TEXT:
					case DBF_PASSWORD:	// An encrypted text field
						{
						SWString	s;
						
						if (fd.size < 16384) s.format("VARCHAR(%d)", fd.size);
						else s = "MEDIUMTEXT";
						sql += s;
						}
						break;

					case DBF_SMALL_BLOB:	sql += "TINYBLOB";		break;
					case DBF_BLOB:			sql += "BLOB";			break;
					case DBF_MEDIUM_BLOB:	sql += "MEDIUMBLOB";	break;
					case DBF_LARGE_BLOB:	sql += "LONGBLOB";		break;
					default:
						throw 1;
						break;
					}

				if ((fd.attributes & DBA_NOTNULL) != 0) sql += " NOT NULL";
				if ((fd.attributes & DBA_AUTOINC) != 0)
					{
					if (autoincr)
						{
						// We can only have one auto-inc field
						throw SW_EXCEPTION_TEXT(DBException, "Only one auto-increment field allowed per table");
						}

					// Enforces primary key
					sql += " PRIMARY KEY AUTOINCREMENT";
					}
				
				if (!create)
					{
					// We must alter the table one column at a time
					r = execute(sql, NULL);
					sql.empty();
					}
				}
			}
			
		sql += extra;

		if (!sql.IsEmpty())
			{
			if (create) sql += ")";
			r = execute(sql, NULL);
			
			if (create)
				{
				// Need to create the indexes
				for (int i=0;i<(int)indexDefs.size();i++)
					{
					const DBTableIndex	&fd=indexDefs.get(i);

					sql = "create ";
					if (fd.unique) sql += "unique ";

					sql += "index ";
					sql += "idx_";
					sql += table;
					sql += "_";
					sql += fd.name;
					sql += " on ";
					sql += table;
					sql += " (";
					sql += fd.fields;
					sql += " )";
					
					r = execute(sql, NULL);
					}
				}
			}

		return res;
		}

//--------------------------------------------------------------------------------
// SWList Iterator
//--------------------------------------------------------------------------------


class SqliteIterator : public SWCollectionIterator
		{
		//friend class SWList;

public:
		SqliteIterator(DBRecordSet *pRecordSet, sqlite3 *pDB, const SWString &sql);
		~SqliteIterator();

		virtual bool		hasCurrent();
		virtual bool		hasNext();
		virtual bool		hasPrevious();

		virtual void *		current();
		virtual void *		next();
		virtual void *		previous();

		virtual bool		remove(void *pOldData);
		virtual bool		add(void *data, SWAbstractIterator::AddLocation where);
		virtual bool		set(void *data, void *pOldData);

private:
		void				filldata();

private:
		sqlite3			*m_pDB;
		sqlite3_stmt	*m_pStatement;
		DBRecordSet		*m_pRecordSet;
		DBRecord		m_data;
		sw_int32_t		m_fieldCount;
		int				m_status;
		bool			m_datavalid;
		bool			m_dataready;
		bool			m_eof;
		};


#define FLAG_INIT		0x1
#define FLAG_NEXT		0x2
#define FLAG_PREV		0x4
#define FLAG_CURR		0x8


SqliteIterator::SqliteIterator(DBRecordSet *pRecordSet, sqlite3 *pDB, const SWString &sql) :
	SWCollectionIterator(NULL),
	m_pStatement(NULL),
	m_pRecordSet(pRecordSet),
	m_eof(false),
	m_datavalid(false),
	m_dataready(false),
	m_fieldCount(0),
	m_pDB(pDB)
		{
		m_status = sqlite3_prepare_v2(m_pDB, sql, -1, &m_pStatement, NULL);
		if (m_status == SQLITE_OK)
			{
			m_fieldCount = sqlite3_column_count(m_pStatement);

			m_pRecordSet->clearFieldInfo();
			for (sw_int32_t f=0;f<m_fieldCount;f++)
				{
				DBFieldInfo	dbfi;
				int			fieldlen=0;

				dbfi.name(sqlite3_column_name(m_pStatement, f));
				switch (sqlite3_column_type(m_pStatement, f))
					{
					case SQLITE_INTEGER:	
						fieldlen = 8;
						dbfi.type(DBF_INT64);
						break;

					case SQLITE_FLOAT:		
						fieldlen = 8;
						dbfi.type(DBF_DOUBLE);
						break;

					case SQLITE_BLOB:		
						fieldlen = 0;
						dbfi.type(DBF_TEXT);
						break;

					case SQLITE_NULL:		
						fieldlen = 0;
						dbfi.type(DBF_INVALID);
						break;

					case SQLITE3_TEXT:		
						fieldlen = 0;
						dbfi.type(DBF_TEXT);
						break;
					}

				dbfi.size(fieldlen);
				dbfi.flags(0);

				m_pRecordSet->setFieldInfo(f, dbfi);
				}
			}
		}


SqliteIterator::~SqliteIterator()
		{
		if (m_pStatement != NULL)
			{
			sqlite3_finalize(m_pStatement);
			m_pStatement = NULL;
			m_datavalid = false;
			m_dataready = false;
			m_eof = true;
			m_fieldCount = 0;
			}
		}


void
SqliteIterator::filldata()
		{
		m_dataready = false;
		m_data.clear();

		for (sw_int32_t f=0;f<m_fieldCount;f++)
			{
			SWValue	v;

			switch (sqlite3_column_type(m_pStatement, f))
				{
				case SQLITE_INTEGER:	
					v = sqlite3_column_int64(m_pStatement, f);
					break;

				case SQLITE_FLOAT:		
					v = sqlite3_column_double(m_pStatement, f);
					break;

				case SQLITE_BLOB:		
					{
					const void	*pdata=sqlite3_column_blob(m_pStatement, f);
					int			datalen=sqlite3_column_bytes(m_pStatement, f);

					v.setValue(pdata, datalen, true);
					}
					break;

				case SQLITE_NULL:		
					// Do nothing
					break;

				case SQLITE3_TEXT:		
					v = (const char *)sqlite3_column_text(m_pStatement, f);
					break;
				}

			m_data.setValue(f, v);
			}
		
		m_datavalid = true;
		}


bool
SqliteIterator::hasCurrent()
		{
		return m_datavalid;
		}


bool
SqliteIterator::hasNext()
		{
		if (!m_dataready && !m_eof)
			{
			int		r;

			r = sqlite3_step(m_pStatement);
			if (r == SQLITE_ROW)
				{
				m_dataready = true;
				}
			else
				{
				m_eof = true;
				}
			}

		return m_dataready;
		}


bool
SqliteIterator::hasPrevious() 
		{
		// Currently not supported
		return false;
		}


void *
SqliteIterator::current() 
		{
		if (!hasCurrent()) throw SW_EXCEPTION(SWNoSuchElementException);

		return &m_data;
		}

void *
SqliteIterator::next() 
		{
		if (!hasNext()) throw SW_EXCEPTION(SWNoSuchElementException);

		if (m_dataready)
			{
			filldata();
			}
		else
			{
			throw SW_EXCEPTION(SWNoSuchElementException);
			}


		return &m_data;
		}

void *
SqliteIterator::previous() 
		{
		void	*res=NULL;

		if (!res) throw SW_EXCEPTION(SWNoSuchElementException);

		return res;
		}

bool
SqliteIterator::remove(void * /*pOldData*/)
		{
		bool	res=false;

		if (!res) throw SW_EXCEPTION(SWUnsupportedOperationException);

		return res;
		}


bool
SqliteIterator::set(void * /*data*/, void * /*pOldData*/)
		{
		bool	res=false;

		if (!res) throw SW_EXCEPTION(SWUnsupportedOperationException);

		return res;
		}


bool
SqliteIterator::add(void * /*data*/, SWAbstractIterator::AddLocation /*where*/) 
		{
		bool	res=false;

		if (!res) throw SW_EXCEPTION(SWUnsupportedOperationException);

		return res;
		}


sw_status_t
DBSqliteDatabase::execute(const SWString &sql, DBRecordSet *pRecordSet)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (pRecordSet == NULL)
			{
			// No result - just execute
			if (sqlite3_exec(m_pDB, sql, NULL, NULL, NULL) != 0)
				{
				res = SW_STATUS_DB_ERROR;
				}
			}
		else
			{
			loadRecordSet(pRecordSet,  new SqliteIterator(pRecordSet, m_pDB, sql));
			}

		return res;
		}


bool
DBSqliteDatabase::tableExists(const SWString &table)
		{
		bool			res=false;
		SWString		sql;
		DBRecordSet		rs;
		
		sql = "pragma table_info(";
		sql += table;
		sql += ")";

		res = (execute(sql, &rs) == SW_STATUS_SUCCESS);
		if (res)
			{
			SWIterator<DBRecord>	it=rs.iterator();

			res = it.hasNext();
			}

		return res;
		}

bool
DBSqliteDatabase::tableHasField(const SWString &table, const SWString &field)
		{
		bool			res=false;
		DBRecordSet		rs;
		SWString		sql;
		
		sql = "pragma table_info(";
		sql += table;
		sql += ")";

		if (execute(sql, &rs) == SW_STATUS_SUCCESS)
			{
			SWIterator<DBRecord>	it=rs.iterator();

			while (!res && it.hasNext())
				{
				SWString	fn;
				DBRecord	*pRow=it.next();

				SWValue		v;

				pRow->getValue(1, v);

				if (field == v.toString())
					{
					res = true;
					}
				}
			}

		return res;
		}


sw_status_t
DBSqliteDatabase::getTableInfo(const SWString &table, DBTableInfo &ti)
		{
		sw_status_t			res=SW_STATUS_SUCCESS;
		SWString			sql;
		DBRecordSet			rs;
		
		sql = "pragma table_info(";
		sql += table;
		sql += ")";

		if ((res = execute(sql, &rs)) == SW_STATUS_SUCCESS)
			{
			SWIterator<DBRecord>	it=rs.iterator();

			int			f=0;

			while (!res && it.hasNext())
				{
				DBRecord	*pRow=it.next();
				DBFieldInfo	dbfi;
				SWValue		v;
				SWString	name, tstr, nullstr, key, extra;

				pRow->getValue(1, v);
				name = v.toString();

				pRow->getValue(2, v);
				tstr = v.toString();

				int			len=0;
				int			p;
				int			flags=0;

				if ((p = tstr.first('(')) > 0)
					{
					len = atoi(tstr.mid(p+1));
					tstr.remove(p);
					}

				dbfi.name(name);
				dbfi.size(len);

				if (nullstr.compareNoCase("NO") == 0) flags |= DBA_NOTNULL;
				if (extra.contains("AUTOINCREMENT", SWString::ignoreCase)) flags |= DBA_AUTOINC;

				dbfi.flags(flags);

				if (tstr.compareNoCase("integer") == 0) dbfi.type(DBF_INT32);
				else if (tstr.compareNoCase("varchar") == 0) dbfi.type(DBF_TEXT);
				else if (tstr.compareNoCase("timestamp") == 0) dbfi.type(DBF_DATETIME);
				else dbfi.type(DBF_INVALID);


				ti.setFieldInfo(f++, dbfi);
				}
			}

		return res;
		}




sw_status_t	
DBSqliteDatabase::getTableList(SWStringArray &tlist)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		return res;
		}
