/**
*** @file		DBJetDatabase.h
*** @brief		Basic database access object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Defines the DBJetDatabase class
***
*** <b>Copyright 2007 SwiftTec. All rights reserved.</b>
**/
/****************************************************************************
** $Date: 2009/02/03 09:48:08 $
** $Author: simon $
** $Revision: 1.3 $
*****************************************************************************
** $Log: DBJetDatabase.cpp,v $
****************************************************************************/

#include "stdafx.h"

#if defined(SW_PLATFORM_WINDOWS) && defined(SW_BUILD_MFC)

// Swift includes
#include <swift/SWTokenizer.h>
#include <swift/SWIntegerArray.h>
#include <swift/SWStringArray.h>
#include <swift/SWFileInfo.h>
#include <swift/SWCollectionIterator.h>
#include <swift/SWGuard.h>
#include <swift/SWLocalTime.h>

// DB includes
#include <db/DBJetDatabase.h>
#include <db/DBRecord.h>
#include <db/DBDatabase.h>

// Disable warning about deprecated objects
#pragma warning(disable: 4995)
#pragma warning(disable: 4251)


DBJetDatabase::DBJetDatabase()
		{
		}


DBJetDatabase::~DBJetDatabase()
		{
		// Make sure the database is actually closed
		close();
		}


SWString
DBJetDatabase::sqlDateString(const SWDate &dt) const
		{
		SWString	s;

		if (dt.julian() == 0)
			{
			s = _T("null");
			}
		else
			{
			s = _T("#");
			s += dt.toString(_T("MM/DD/YYYY"));
			s += _T("#");
			}
		
		return s;
		}


SWString
DBJetDatabase::sqlLikeWildcard() const
		{
		SWString	res="*";

		return res;
		}


sw_status_t
DBJetDatabase::opendb(const SWArray<SWNamedValue> &va)
		{
		bool		createflag=true;
		sw_status_t	res=SW_STATUS_SUCCESS;
		SWString	connection;
		SWString	filename;
		
		for (int i=0;i<(int)va.size();i++)
			{
			if (va.get(i).name() == "filename") filename = va.get(i).toString();
			else if (va.get(i).name() == "create") createflag = va.get(i).toBoolean();
			}

		connection = "Driver={Microsoft Access Driver (*.mdb)};Dbq=";
		connection += filename;
		connection += ";Uid=Admin;Pwd=;";

		res = openodbc(connection);

		if (res != SW_STATUS_SUCCESS)
			{
			connection = "Driver={Microsoft Access Driver (*.mdb, *.accdb)};Dbq=";
			connection += filename;
			connection += ";Uid=Admin;Pwd=;";

			res = openodbc(connection);
			}
		
		if (res == SW_STATUS_SUCCESS) m_filename = filename;
		else m_filename.empty();

		return res;
		}


sw_status_t
DBJetDatabase::getTableInfo(const SWString &table, DBTableInfo &ti)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		SWString	sql;
		DBRecordSet	rset;

		sql = _T("SELECT LvProp from MSysObjects where Name =");
		sql += sqlQuote(table);

		if ((res = execute(sql, &rset)) == SW_STATUS_SUCCESS)
			{
			SWIterator<DBRecord>	it=rset.iterator();

			if (it.hasNext())
				{
				DBRecord	*pRecord;
				SWValue		v;

				pRecord = it.next();
				pRecord->getValue(0, v);

				sw_byte_t	*p=(sw_byte_t *)v.data();
				sw_byte_t	jet3[4] = { 'K', 'K', 'D', 0 };
				sw_byte_t	jet4[4] = { 'M', 'R', '2', 0 };

				if (memcmp(p, jet3, 4) == 0 || memcmp(p, jet4, 4) == 0)
					{
					int		nbytes=(int)v.length();

//					printf("Valid table info %d bytes...\n");
					p += 4;
					nbytes -= 4;

					while (nbytes > 0)
						{
						sw_int32_t	blocklen;
						sw_int16_t	blocktype;

						memcpy(&blocklen, p, 4);
						memcpy(&blocktype, p+4, 2);

//						printf("blocklen = %d, blocktype = %d\n");

						if (blocklen > 0)
							{
							p += blocklen;
							nbytes -= blocklen;
							}
						else
							{
							break;
							}
						}

					/*
					DBRecord	&rec=*it.next();
					SWValue		v;
					DBFieldInfo	dbfi;

					rec.getValue(0, v);
					dbfi.name(v.toString());

					rec.getValue(2, v);
					dbfi.size(v.toInt32());

					dbfi.flags(0);

					rec.getValue(1, v);
					SWString	tstr=v.toString();

					if (tstr.compareNoCase("int") == 0) dbfi.type(DBF_INT32);
					else if (tstr.compareNoCase("varchar") == 0) dbfi.type(DBF_TEXT);
					else if (tstr.compareNoCase("datetime") == 0) dbfi.type(DBF_DATETIME);
					else if (tstr.compareNoCase("tinyint") == 0) dbfi.type(DBF_INT8);
					else if (tstr.compareNoCase("tinyint") == 0) dbfi.type(DBF_INT8);
					else dbfi.type(DBF_INVALID);

					ti.setFieldInfo(f++, dbfi);
					*/
					}
				}
			else
				{
				res = SW_STATUS_INVALID_DATA;
				}
			}

		return res;
		}




sw_status_t
DBJetDatabase::checkTable(const SWString &table, const SWArray<DBTableField> &fieldDefs, const SWArray<DBTableIndex> &indexDefs)
		{
		sw_status_t		res=SW_STATUS_SUCCESS;
		SWString		sql;

		sql = _T("SELECT TOP 1 * FROM ");
		sql += sqlQuote(table);

		DBRecordSet				rset;
		SWStringArray			fa;		// Field names
		SWIntegerArray			ta;		// Type Ids
		SWIntegerArray			la;		// Field lengths
		SWValue					v;
		bool					create=true;
		int						r;

		res = execute(sql, &rset);
		if (res == SW_STATUS_SUCCESS)
			{
			int		nfields=(int)rset.getFieldCount();

			for (int i=0;i<nfields;i++)
				{
				DBFieldInfo	fi;

				rset.getFieldInfo(i, fi);

				fa.add(fi.name());
				ta.add(fi.type());
				la.add(fi.size());
				/*
				// column
				pRecord->getValue(1, v);
				fa.add(v.toString());

				// type
				pRecord->getValue(2, v);
				ta.add(v.toString());

				// column
				pRecord->getValue(3, v);
				la.add(v.toInt32());
				*/

				create = false;
				}
			}

		sql.Empty();
			
		SWString	extra;
		bool		autoincr=false;

		for (int i=0;i<(int)fieldDefs.size();i++)
			{
			const DBTableField	&fd=fieldDefs.get(i);
			bool	found=false;
			
			if (!create)
				{
				for (int i=0;!found&&i<(int)fa.size();i++)
					{
					if (fa[i] == fd.name)
						{
						found = true;

						// If a test field make sure that if needed we increase the size to match
						// but never reduce size
						if (fd.type == DBF_TEXT || fd.type == DBF_PASSWORD)
							{
							// SWString	ts=ta[i];

							// if (ts.compareNoCase("NVARCHAR") == 0)
								{
								int	len;

								len = la[i];

								if (len > 0 && len < fd.size)
									{
									SWString	sqlGrow;

									sqlGrow = "alter table ";
									sqlGrow += table;
									sqlGrow += " modify ";
									sqlGrow += fd.name;
									sqlGrow += " ";

									SWString	s;
						
									if (fd.size < 255) s.format("CHAR(%d)", fd.size);
									else s = "MEMO";

									sqlGrow += s;
									r = execute(sqlGrow, NULL);
									}
								}
							}
						}
					}
				}
			
			if (!found)
				{
				if (sql.IsEmpty())
					{
					if (create) sql = "CREATE TABLE ";
					else sql = "ALTER TABLE ";
					sql += table;
					
					if (create) sql += " (";
					}
				else
					{
					sql += ", ";
					}
				
				sql += " ";
				if (!create)
					{
					sql += "ADD COLUMN ";
					}
				
				sql += fd.name;
				sql += " ";
				
				switch (fd.type)
					{
					case DBF_BOOLEAN:	sql += "YESNO";		break;		// A boolean value
					case DBF_INT8:		sql += "BYTE";		break;		// An 8-bit signed integer
					case DBF_INT16:		sql += "SMALLINT";	break;		// A 16-bit signed integer
					case DBF_FLOAT: 	sql += "FLOAT";		break;		// A float
					case DBF_DOUBLE:	sql += "FLOAT";		break;		// A double
					case DBF_DATE:		sql += "DATE";		break;		// A date/time (SWTime) stored in local time format
					case DBF_DATETIME:	sql += "DATE";		break;		// A date/time (SWTime) stored in local time format
					case DBF_TIMESTAMP:	sql += "DATE";		break;		// A date/time (SWTime) stored in local time format
					
					case DBF_MONEY:
					case DBF_INT32:	
						if ((fd.attributes & DBA_AUTOINC) != 0) sql += "COUNTER";
						else sql += "LONG";
						break;		// A 32-bit signed integer

					case DBF_INT64:	
						sql += "LONG";
						break;		// A 64-bit signed integer

					case DBF_TEXT:
					case DBF_PASSWORD:	// An encrypted text field
						{
						SWString	s;
						
						if (fd.size < 255) s.format("CHAR(%d)", fd.size);
						else s = "MEMO";
						sql += s;
						}
						break;

					case DBF_SMALL_BLOB:	sql += "OLEOBJECT";	break;
					case DBF_BLOB:			sql += "OLEOBJECT";	break;
					case DBF_MEDIUM_BLOB:	sql += "OLEOBJECT";	break;
					case DBF_LARGE_BLOB:	sql += "OLEOBJECT";	break;
					default:
						throw 1;
						break;
					}

				if ((fd.attributes & DBA_NOTNULL) != 0) sql += " NOT NULL";
				if ((fd.attributes & DBA_AUTOINC) != 0)
					{
					if (autoincr)
						{
						// We can only have one auto-inc field
						throw SW_EXCEPTION_TEXT(DBException, "Only one auto-increment field allowed per table");
						}

					// Enforces primary key
					sql += " PRIMARY KEY IDENTITY";
					}
				}
			}
			
		sql += extra;

		if (!sql.IsEmpty())
			{
			if (create) sql += ")";

			r = execute(sql, NULL);
			
			if (create)
				{
				bool				primary=true;
				for (int i=0;i<(int)indexDefs.size();i++)
					{
					const DBTableIndex	&fd=indexDefs.get(i);

					// Need to create the indexes
					sql = "alter table ";
					sql += table;
					sql += " add ";

					
					if (fd.unique)
						{
						if (primary)
							{
							sql += "primary ";
							primary = false;
							}
						else
							{
							sql += "unique ";
							}
						}
						
					sql += "key ";
					sql += "idx_";
					sql += fd.name;
					sql += " (";
					sql += fd.fields;
					sql += " )";
					
					r = execute(sql, NULL);
					}
				}
			}

		return res;
		}


sw_status_t	
DBJetDatabase::getTableList(SWStringArray &tlist)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		return res;
		}
		
#endif // defined(SW_PLATFORM_WINDOWS)
