
/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"

#include <swift/SWTokenizer.h>


#include <db/DBFieldInfo.h>

DBFieldInfo::DBFieldInfo()
		{
		}


DBFieldInfo::DBFieldInfo(const DBFieldInfo &other)
		{
		*this = other;
		}


DBFieldInfo::~DBFieldInfo()
		{
		clear();
		}


DBFieldInfo &
DBFieldInfo::operator=(const DBFieldInfo &other)
		{
#define COPY(x) x = other.x;
		COPY(m_name);
		COPY(m_type);
		COPY(m_size);
		COPY(m_flags);
#undef COPY

		return *this;
		}


bool
DBFieldInfo::operator==(const DBFieldInfo &other)
		{
		return (this == &other);
		}


void
DBFieldInfo::clear()
		{
		m_name.empty();
		m_size = 0;
		m_type = 0;
		m_flags = 0;
		}


SWString
DBFieldInfo::typeAsString() const
		{
		SWString	s;

		switch (m_type)
			{
			case DBF_BOOLEAN:		s = _T("boolean");		break;
			case DBF_INT8:			s = _T("int8");			break;
			case DBF_INT16:			s = _T("int16");		break;
			case DBF_INT32:			s = _T("int32");		break;
			case DBF_INT64:			s = _T("int64");		break;
			case DBF_FLOAT:			s = _T("float");		break;
			case DBF_DOUBLE:		s = _T("double");		break;
			case DBF_DATE:			s = _T("date");			break;
			case DBF_DATETIME:		s = _T("datetime");		break;
			case DBF_TIMESTAMP:		s = _T("timestamp");	break;
			case DBF_TEXT:			s = _T("text");			break;
			case DBF_PASSWORD:		s = _T("password");		break;
			case DBF_MONEY:			s = _T("money");		break;
			case DBF_DECIMAL:		s = _T("decimal");		break;
			case DBF_BLOB:			s = _T("blob");			break;
			case DBF_SMALL_BLOB:	s = _T("smallblob");	break;
			case DBF_MEDIUM_BLOB:	s = _T("mediumblob");	break;
			case DBF_LARGE_BLOB:	s = _T("largeblob");	break;
			
			default:				s.format("type%d", m_type);	break;
			// = _T("????");	break;
			}
		
		return s;
		}

