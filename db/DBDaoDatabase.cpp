#include "stdafx.h"

#if defined(SW_PLATFORM_WINDOWS) && defined(SW_BUILD_MFC) && !defined(_WIN64)

// Swift includes
#include <swift/SWTokenizer.h>
#include <swift/SWStringArray.h>
#include <swift/SWFileInfo.h>
#include <swift/SWCollectionIterator.h>
#include <swift/SWGuard.h>
#include <swift/SWLocalTime.h>

// DB includes
#include <db/DBDaoDatabase.h>
#include <db/DBRecord.h>

// For DAO objects
#include <afxdao.h>

// Disable warning about deprecated objects
#pragma warning(disable: 4995)
#pragma warning(disable: 4251)


// An interlocked count so we only init and term the AfxDao layer once
SWInterlockedInt32	DBDaoDatabase::m_daoInitCount(0);


DBDaoDatabase::DBDaoDatabase()
		{
		SWGuard	guard(m_daoInitCount);

		if (m_daoInitCount == 0)
			{
			AfxDaoInit();
			}

		m_daoInitCount++;

		m_pDB = new CDaoDatabase();
		}


DBDaoDatabase::~DBDaoDatabase()
		{
		// Make sure the database is actually closed
		close();

		SWGuard	guard(m_daoInitCount);

		if (--m_daoInitCount == 0)
			{
			AfxDaoTerm();
			}

		delete m_pDB;
		}


		

bool
DBDaoDatabase::isSupported()
		{
		return true;
		}



sw_status_t
DBDaoDatabase::opendb(const SWArray<SWNamedValue> &va)
		{
		SWString	filename;
		bool		createflag=true;
		sw_status_t	res=SW_STATUS_SUCCESS;
		
		for (int i=0;i<(int)va.size();i++)
			{
			if (va.get(i).name() == "filename") filename = va.get(i).toString();
			else if (va.get(i).name() == "create") createflag = va.get(i).toBoolean();
			}

		if (m_pDB->IsOpen())
			{
			res = SW_STATUS_ALREADY_OPEN;
			}
		else
			{
			m_filename.empty();

			try
				{
				if (SWFileInfo::isFile(filename))
					{
					m_pDB->Open(filename);
					m_filename = filename;
					}
				else if (createflag)
					{
					m_pDB->Create(filename);
					m_pDB->Close();
					m_pDB->Open(filename);
					m_filename = filename;
					}
				}
			catch (CDaoException *)
				{
				res = SW_STATUS_DB_ERROR;
				m_filename.empty();
				}
			}
		
		return res;
		}
		
		
sw_status_t
DBDaoDatabase::close()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		
		if (m_pDB->IsOpen())
			{
			try
				{
				m_pDB->Close();
				}
			catch (CDaoException *)
				{
				res = SW_STATUS_DB_ERROR;
				}
			}
		
		m_filename.empty();

		return res;
		}


void
DBDaoDatabase::getDaoFieldInfo(const DBTableField &fd, short &daotype, long &daolen)
		{
		switch (fd.type)
			{
			case DBF_BOOLEAN:
				daotype = dbBoolean;
				daolen = 1;
				break;
					
			case DBF_INT8:
				daotype = dbByte;
				daolen = 1;
				break;
					
			case DBF_INT16:
				daotype = dbInteger;
				daolen = 2;
				break;
					
			case DBF_MONEY:
			case DBF_INT32:
				daotype = dbLong;
				daolen = 4;
				break;
					
			case DBF_INT64:
				daotype = dbText;
				daolen = 24;
				break;
					
			case DBF_DOUBLE:
				daotype = dbDouble;
				daolen = 8;
				break;
					
			case DBF_DATE:
				daotype = dbDate;
				daolen = 8;
				break;
					
			case DBF_TIMESTAMP:
			case DBF_DATETIME:
				daotype = dbDate;
				daolen = 8;
				break;
					
			case DBF_PASSWORD:
			case DBF_TEXT:
				if (fd.size > 255)
					{
					daotype = dbMemo;
					daolen = 0;
					}
				else
					{
					daotype = dbText;
					daolen = fd.size;
					}
				break;
					
			case DBF_BLOB:
			case DBF_SMALL_BLOB:
			case DBF_MEDIUM_BLOB:
			case DBF_LARGE_BLOB:
					daotype = dbLongBinary;
					daolen = 0;
				break;

			default:
				// ERROR - unsupported type
				throw 1;
			}
		}


sw_status_t
DBDaoDatabase::checkTable(const SWString &table, const SWArray<DBTableField> &fieldDefs, const SWArray<DBTableIndex> &indexDefs)

		{
		sw_status_t			res=SW_STATUS_SUCCESS;
		CDaoTableDef		tdef(m_pDB);
		CDaoFieldInfo		field;
		bool				created=false;

		// Member Table
		try
			{
			tdef.Open(table);
			created = false;
			}
		catch (CDaoException *)
			{
			tdef.Close();
			tdef.Create(table);
			created = true;
			}

		// Verify the fields
		if ((created || tdef.IsOpen()))
			{
			for (int i=0;i<(int)fieldDefs.size();i++)
				{
				const DBTableField	&fd=fieldDefs.get(i);
				SWString	fieldname(fd.name);
				bool		createfield=created;
				
				if (!createfield)
					{
					try
						{
						createfield = true;

						int		i, n;
						
						n = tdef.GetFieldCount();
						for (i=0;i<n;i++)
							{
							tdef.GetFieldInfo(i, field);
							if (fieldname == field.m_strName)
								{
								createfield = false;

								// If a test field make sure that if needed we increase the size to match
								// but never reduce size
								if (fd.type == DBF_TEXT || fd.type == DBF_PASSWORD)
									{
									if (field.m_nType == dbText && field.m_lSize < fd.size)
										{
										SWString	sqlGrow;

										sqlGrow = "alter table ";
										sqlGrow += table;
										sqlGrow += " alter ";
										sqlGrow += fd.name;
										sqlGrow += " ";

										SWString	s;
							
										if (fd.size < 256) s.format("VARCHAR(%d)", fd.size);
										else s = "MEMO";

										sqlGrow += s;
										execute(sqlGrow, NULL);
										}
									}

								break;
								}
							}
						// tdef.GetFieldInfo(fieldname, field);
						}
					catch (CDaoException *)
						{
						}
					}
					
				if (createfield)
					{
					getDaoFieldInfo(fd, field.m_nType, field.m_lSize);

					// The field does not exist - create it
					field.m_strName = fd.name.t_str();
					
					field.m_lAttributes = dbUpdatableField;
					if (fd.type == DBF_TEXT || fd.type == DBF_PASSWORD)
						{
						field.m_lAttributes |= dbVariableField;
						}
					else
						{
						field.m_lAttributes |= dbFixedField;
						}
						
					if ((fd.attributes & DBA_AUTOINC) != 0) field.m_lAttributes |= dbAutoIncrField;

					field.m_nOrdinalPosition = 0;
					field.m_bRequired = ((fd.attributes & DBA_NOTNULL) != 0);
					field.m_bAllowZeroLength = (fd.type == DBF_TEXT || fd.type == DBF_PASSWORD);
					field.m_lCollatingOrder = 0;
					field.m_strForeignName.Empty();
					field.m_strSourceField.Empty();
					field.m_strSourceTable.Empty();
					field.m_strValidationRule.Empty();
					field.m_strValidationText.Empty();
					field.m_strDefaultValue.Empty();
					
					try
						{
						tdef.CreateField(field);
						}
					catch (CDaoException *)
						{
						TRACE("ERROR - exception in DAO CreateField");
						}
					}
				}

			if (created)
				{
				CDaoIndexInfo		idx;
				CDaoIndexFieldInfo	idxfields[10];
				BOOL				primary=TRUE;

				for (int i=0;i<(int)indexDefs.size();i++)
					{
					const DBTableIndex	&fd=indexDefs.get(i);
					SWTokenizer		tok(fd.fields, ",");
					SWStringArray	sa;
					
					while (tok.hasMoreTokens())
						{
						sa.add(tok.nextToken());
						}
					
					for (int j=0;j<(int)sa.size();j++)
						{
						// The index fields
						idxfields[j].m_strName = sa[j].t_str();
						idxfields[j].m_bDescending = FALSE;
						}
					
					// The index
					idx.m_strName = fd.name.t_str();
					idx.m_pFieldInfos = idxfields;
					idx.m_nFields = (short)sa.size();
					if (fd.unique)
						{
						idx.m_bPrimary = primary;
						idx.m_bUnique = TRUE;
						primary = FALSE;
						}
					else
						{
						idx.m_bPrimary = FALSE;
						idx.m_bUnique = FALSE;
						}
					idx.m_bIgnoreNulls = FALSE;
					idx.m_bRequired = TRUE;
					
					try
						{
						tdef.CreateIndex(idx);
						}
					catch (CDaoException *)
						{
						TRACE("ERROR - exception in DAO CreateIndex");
						}
					}

				// Add the table to the database
				try
					{
					tdef.Append();
					}
				catch (CDaoException *)
					{
					TRACE("ERROR - exception in DAO Append");
					}
				res = SW_STATUS_SUCCESS;
				}
			else
				{
				res = SW_STATUS_SUCCESS;
				tdef.Close();
				}
			}
		else
			{
			res = SW_STATUS_DB_ERROR;
			}
		
		return res;
		}

//--------------------------------------------------------------------------------
// SWList Iterator
//--------------------------------------------------------------------------------


class DaoIterator : public SWCollectionIterator
		{
		// friend class SWList;

public:
		DaoIterator(DBRecordSet *pRecordSet, CDaoDatabase *pDB, const SWString &query);
		~DaoIterator();

		virtual bool		hasCurrent();
		virtual bool		hasNext();
		virtual bool		hasPrevious();

		virtual void *		current();
		virtual void *		next();
		virtual void *		previous();

		virtual bool		remove(void *pOldData);
		virtual bool		add(void *data, SWAbstractIterator::AddLocation where);
		virtual bool		set(void *data, void *pOldData);

private:
		void				filldata();

private:
		CDaoRecordset	m_rset;
		DBRecordSet		*m_pRecordSet;
		DBRecord		m_data;
		sw_uint32_t		m_flags;
		};


#define FLAG_INIT		0x1
#define FLAG_NEXT		0x2
#define FLAG_PREV		0x4
#define FLAG_CURR		0x8


DaoIterator::DaoIterator(DBRecordSet *pRecordSet, CDaoDatabase *pDB, const SWString &query) :
	SWCollectionIterator(NULL),
	m_pRecordSet(pRecordSet),
	m_rset(pDB),
	m_flags(0)
		{
		try
			{
			m_rset.Open(AFX_DAO_USE_DEFAULT_TYPE, query.t_str());

			CDaoFieldInfo	fi;
			int				f, fc = m_rset.GetFieldCount();

			m_pRecordSet->clearFieldInfo();
			for (f=0;f<fc;f++)
				{
				DBFieldInfo	dbfi;

				m_rset.GetFieldInfo(f, fi, AFX_DAO_PRIMARY_INFO);

				dbfi.name(fi.m_strName);
				switch (fi.m_nType)
					{
					case dbBoolean:		dbfi.type(DBF_BOOLEAN);		break;
					case dbByte:		dbfi.type(DBF_INT8);		break;
					case dbInteger:		dbfi.type(DBF_INT16);		break;
					case dbLong:		dbfi.type(DBF_INT32);		break;
					case dbSingle:		dbfi.type(DBF_DOUBLE);		break;
					case dbDouble:		dbfi.type(DBF_DOUBLE);		break;
					case dbDate:		dbfi.type(DBF_DATETIME);	break;
					case dbText:		dbfi.type(DBF_TEXT);		break;
					case dbMemo:		dbfi.type(DBF_TEXT);		break;
					case dbLongBinary:	dbfi.type(DBF_BLOB);		break;
					
					// Unsupported types
					case dbCurrency:	dbfi.type(0);				break;
					case dbGUID:		dbfi.type(0);		break;
					}

				dbfi.size(fi.m_lSize);
				dbfi.flags(0);

				m_pRecordSet->setFieldInfo(f, dbfi);
				}
			
			// If successful we are pointing at the first data record
			// So we must move to the BOF to be consistant with Iterators
			// Data record sequence is BOF REC REC EOF
			// or BOF EOF for no data
			if (!m_rset.IsBOF())
				{
				m_rset.MovePrev();
				}
			}
		catch (CDaoException *)
			{
			if (m_rset.IsOpen())
				{
				m_rset.Close();
				}
			}
		}


DaoIterator::~DaoIterator()
		{
		try
			{
			if (m_rset.IsOpen())
				{
				m_rset.Close();
				}
			}
		catch (CDaoException *)
			{
			}
		}

void
DaoIterator::filldata()
		{
		m_data.clear();

		CDaoFieldInfo	fi;
		COleVariant		v;
		int				f, fc;
		SWString		fname;
		
		try
			{
			fc = m_rset.GetFieldCount();
			for (f=0;f<fc;f++)
				{
				SWValue	nv;

				m_rset.GetFieldValue(f, v);
				
				if (v.vt == VT_DATE)
					{
					// Must convert this to a local date
					SWLocalTime	lt;

					lt.setFromLocalVariantTime(v.date);
					nv = lt;
					}
				else
					{
					nv = v;
					}

				// Set the data but force the changed flag to be cleared
				m_data.setValue(f, nv, true);
				}
			}
		catch (CDaoException *)
			{
			}
		}


bool
DaoIterator::hasCurrent()
		{
		return (m_data.size() > 0);
		}


bool
DaoIterator::hasNext()
		{
		bool	res=false;

		try
			{
			if (m_rset.IsOpen() && !m_rset.IsEOF())
				{
				m_rset.MoveNext();
				res = (m_rset.IsEOF() == FALSE);
				m_rset.MovePrev();
				}
			}
		catch (CDaoException *)
			{
			res = false;
			}

		return res;
		}


bool
DaoIterator::hasPrevious() 
		{
		bool	res=false;

		try
			{
			if (m_rset.IsOpen() && !m_rset.IsBOF())
				{
				m_rset.MovePrev();
				res = (m_rset.IsBOF() == FALSE);
				m_rset.MoveNext();
				}
			}
		catch (CDaoException *)
			{
			res = false;
			}

		return res;
		}


void *
DaoIterator::current() 
		{
		if (!hasCurrent()) throw SW_EXCEPTION(SWNoSuchElementException);

		return &m_data;
		}

void *
DaoIterator::next() 
		{
		if (!hasNext()) throw SW_EXCEPTION(SWNoSuchElementException);

		m_rset.MoveNext();
		filldata();

		if (!hasCurrent()) throw SW_EXCEPTION(SWNoSuchElementException);

		return &m_data;
		}

void *
DaoIterator::previous() 
		{
		if (!hasPrevious()) throw SW_EXCEPTION(SWNoSuchElementException);

		// Move to the next record and fill
		m_rset.MovePrev();
		filldata();

		if (!hasCurrent()) throw SW_EXCEPTION(SWNoSuchElementException);

		return &m_data;
		}

bool
DaoIterator::remove(void * /*pOldData*/)
		{
		bool	res=false;

		if (!res) throw SW_EXCEPTION(SWUnsupportedOperationException);

		return res;
		}


bool
DaoIterator::set(void * /*data*/, void * /*pOldData*/)
		{
		bool	res=false;

		if (!res) throw SW_EXCEPTION(SWUnsupportedOperationException);

		return res;
		}


bool
DaoIterator::add(void * /*data*/, SWAbstractIterator::AddLocation /*where*/) 
		{
		bool	res=false;

		if (!res) throw SW_EXCEPTION(SWUnsupportedOperationException);

		return res;
		}


sw_status_t
DBDaoDatabase::execute(const SWString &sql, DBRecordSet *pRecordSet)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (pRecordSet == NULL)
			{
			// No result set - just do the execute
			try
				{
				m_pDB->Execute(sql);
				}
			catch (CDaoException *)
				{
				res = SW_STATUS_ILLEGAL_OPERATION;
				}
			}
		else
			{
			loadRecordSet(pRecordSet,  new DaoIterator(pRecordSet, m_pDB, sql));
			// SWCollection::initIterator(*pIter, new DaoIterator(m_pDB, sql));
			}

		return res;
		}


sw_status_t
DBDaoDatabase::getTableInfo(const SWString &table, DBTableInfo &ti)
		{
		sw_status_t		res=SW_STATUS_SUCCESS;
		CDaoTableDef	tdef(m_pDB);
		CDaoFieldInfo	fi;
		
		ti.clear();
		if (tableExists(table))
			{
			try
				{
				tdef.Open(table);
				
				int		i, n;
				
				n = tdef.GetFieldCount();
				for (i=0;i<n;i++)
					{
					DBFieldInfo	dbfi;

					tdef.GetFieldInfo(i, fi);
					dbfi.name(fi.m_strName);
					switch (fi.m_nType)
						{
						case dbBoolean:		dbfi.type(DBF_BOOLEAN);		break;
						case dbByte:		dbfi.type(DBF_INT8);		break;
						case dbInteger:		dbfi.type(DBF_INT16);		break;
						case dbLong:		dbfi.type(DBF_INT32);		break;
						case dbSingle:		dbfi.type(DBF_DOUBLE);		break;
						case dbDouble:		dbfi.type(DBF_DOUBLE);		break;
						case dbDate:		dbfi.type(DBF_DATETIME);	break;
						case dbText:		dbfi.type(DBF_TEXT);		break;
						case dbMemo:		dbfi.type(DBF_TEXT);		break;
						case dbLongBinary:	dbfi.type(DBF_BLOB);		break;

						// Unsupported types
						case dbCurrency:	dbfi.type(0);				break;
						case dbGUID:		dbfi.type(0);		break;
						}

					dbfi.size(fi.m_lSize);

					int	flags=0;

					if (fi.m_lAttributes & dbAutoIncrField) flags |= DBA_AUTOINC;

					dbfi.flags(flags);

					ti.setFieldInfo(i, dbfi);
					}
				}
			catch (CDaoException *)
				{
				res = SW_STATUS_DB_ERROR;
				tdef.Close();
				}
			}
		
		return res;
		}


bool
DBDaoDatabase::tableExists(const SWString &table)
		{
		bool				res=false;
		CDaoTableDefInfo	ti;
		int					i, n;
		
		n = m_pDB->GetTableDefCount();
		for (i=0;i<n;i++)
			{
			m_pDB->GetTableDefInfo(i, ti);
			if (table == ti.m_strName)
				{
				res = true;
				break;
				}
			}

		return res;
		}

bool
DBDaoDatabase::tableHasField(const SWString &table, const SWString &field)
		{
		bool				res=false;
		CDaoTableDef		tdef(m_pDB);
		CDaoFieldInfo		fi;
		
		if (tableExists(table))
			{
			try
				{
				tdef.Open(table);
				
				int		i, n;
				
				n = tdef.GetFieldCount();
				for (i=0;i<n;i++)
					{
					tdef.GetFieldInfo(i, fi);
					if (field == fi.m_strName)
						{
						res = true;
						break;
						}
					}
				}
			catch (CDaoException *)
				{
				tdef.Close();
				}

			// Verify the fields
			if (tdef.IsOpen())
				{
				tdef.Close();
				}
			}
		
		return res;
		}

/*
SWCollectionIterator *
SWList::getIterator(bool iterateFromEnd) const
		{
		return new DaoIterator((SWList *)this, iterateFromEnd);
		}
*/


SWString
DBDaoDatabase::sqlDateString(const SWDate &dt) const
		{
		SWString	s;

		if (dt.julian() == 0)
			{
			s = _T("null");
			}
		else
			{
			s = _T("#");
			s += dt.toString(_T("MM/DD/YYYY"));
			s += _T("#");
			}
		
		return s;
		}


sw_status_t
DBDaoDatabase::renameTable(const SWString &oldname, const SWString &newname)
		{
		sw_status_t			res=SW_STATUS_DB_ERROR;
		CDaoTableDef		tdef(m_pDB);
		CDaoFieldInfo		fi;
		
		if (tableExists(oldname))
			{
			try
				{
				tdef.Open(oldname);
				tdef.SetName(newname);
				}
			catch (CDaoException *)
				{
				tdef.Close();
				}

			// Verify the fields
			if (tdef.IsOpen())
				{
				tdef.Close();
				}
			}
		
		return res;
		}



SWString
DBDaoDatabase::sqlLikeWildcard() const
		{
		SWString	res="*";

		return res;
		}



sw_status_t	
DBDaoDatabase::getTableList(SWStringArray &tlist)
		{
		sw_status_t			res=SW_STATUS_SUCCESS;
		CDaoTableDefInfo	ti;
		
		tlist.clear();
		
		for (int i=0;i<m_pDB->GetTableDefCount();i++)
			{
			try
				{
				m_pDB->GetTableDefInfo(i, ti);
				if (ti.m_strName.Left(4).CompareNoCase(_T("MSys")) == 0) continue;

				tlist.add(ti.m_strName);
				}
			catch (CDaoException *)
				{
				}
			}

		return res;
		}
		
#endif // defined(SW_PLATFORM_WINDOWS) && !defined(_WIN64)

