/**
*** @file		DBDatabase.h
*** @brief		Basic database access object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Defines the DBDatabase class
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __db_DBDatabase_h__
#define __db_DBDatabase_h__

#include <db/types.h>
#include <db/DBTableInfo.h>
#include <db/DBRecordSet.h>
#include <db/DBGenericDataObject.h>
#include <db/DBTableField.h>
#include <db/DBTableIndex.h>

#include <swift/SWString.h>
#include <swift/SWLocalTime.h>
#include <swift/SWIterator.h>
#include <swift/SWException.h>
#include <swift/SWList.h>
#include <swift/SWArray.h>

#include <config/config.h>


class DBConnection;

class DB_DLL_EXPORT DBDatabase
		{
public:
		enum DBType
			{
			DB_TYPE_NONE		= 0,
			DB_TYPE_DAO			= 1,
#ifdef SW_CONFIG_USE_MYSQL
			DB_TYPE_MYSQL		= 2,
#endif
			DB_TYPE_SQLSERVER	= 3,
			DB_TYPE_JET			= 4,
			DB_TYPE_SQLITE3		= 5,

			// Always the final type in the list
			DB_TYPE_COUNT
			};

public:
		DBDatabase();
		virtual ~DBDatabase();

		/// Open/connect to the database
		virtual sw_status_t		open(int type, const SWString &params);

		/// Close the database
		virtual sw_status_t		close();

		/// Check to see if a table has already been checked
		bool					isTableChecked(const SWString &name) const;

		/// Check a table against the definitions
		sw_status_t				checkTable(const SWString &name, const SWArray<DBTableField> &fieldDefs, const SWArray<DBTableIndex> &indexDefs, bool force=false);

		/// Check a table against the definitions
		sw_status_t				checkTable(const DBDataObject &rec, bool force=false, const SWString &tablename="");

		/// Verify the database open status
		bool					isOpen() const;

		/// Return the database type
		int						type() const;

		/// Return the connection parameters
		SWString				params() const;

		/// Return the DBConnection object
		DBConnection *			connection() const	{ return m_pDB; }

		/// Exectute the SQL statement
		sw_status_t				execute(const SWString &sql);

		/// Exectute the SQL statement
		sw_status_t				execute(const SWString &sql, SWValue &v);

		/// Exectute the SQL statement and return an iterator of the results
		sw_status_t				execute(const SWString &sql, DBRecordSet &rset);

		/// Exectute the SQL statement and return an iterator of the results
		sw_status_t				execute(const SWString &sql, DBRecordSet *pRecordSet);

		/// Exectute the SQL statement and return a list of the results
		sw_status_t				execute(const SWString &sql, SWList<DBGenericDataObject> &dlist);

		/// Exectute the SQL statement and return aan array of the results
		sw_status_t				execute(const SWString &sql, SWArray<DBGenericDataObject> &dlist);

		sw_status_t				insertRecord(const SWString &table, DBDataObject &rec, const SWArray<DBTableField> &fieldDefs);
		sw_status_t				updateRecord(const SWString &table, DBDataObject &rec, const SWArray<DBTableField> &fieldDefs, const SWString &where);

		sw_status_t				getRecord(DBDataObject &rec, const SWString &where);
		sw_status_t				updateRecord(DBDataObject &rec, const SWString &where);
		sw_status_t				insertRecord(DBDataObject &rec);


		sw_status_t				getTableList(SWStringArray &tlist);
		sw_status_t				getTableInfo(const SWString &table, DBTableInfo &ti);
		sw_status_t				getFieldInfo(const SWString &table, const SWString &field, DBFieldInfo &ti);
		bool					tableExists(const SWString &table);
		bool					tableHasField(const SWString &table, const SWString &field);

		SWString				sqlBinaryValue(void *pData, sw_size_t len) const;

		sw_status_t				dropTable(const SWString &table);
		sw_status_t				renameTable(const SWString &oldname, const SWString &newname);

public:
		SWString				sqlDateString(const SWDate &v);
		SWString				sqlDateTimeString(const SWLocalTime &v);
		SWString				sqlQuote(const SWString &s);
		SWString				sqlLikeWildcard();
		SWString				sqlBooleanString(bool v);

		//SWString				getDateString(const SWDate &v);
		//SWString				getDateTimeString(const SWLocalTime &v);
		//SWString				getFieldValueString(const SWValue &v, DBFieldDef *pFieldDef);
		//SWString				getBooleanString(bool v);
	
		SWString				sqlGetRecord(DBDataObject &rec, const SWString &where);
		SWString				sqlInsertRecord(DBDataObject &rec);
		SWString				sqlInsertRecord(const SWString &table, DBDataObject &rec, const SWArray<DBTableField> &fieldDefs);
		SWString				sqlUpdateRecord(DBDataObject &rec, const SWString &where);
		SWString				sqlUpdateRecord(const SWString &table, DBDataObject &rec, const SWArray<DBTableField> &fieldDefs, const SWString &where);
		void					sqlAddValueToString(SWValue &v, const DBTableField &fd, SWString &sql);

		/// Return true if this database is based on a file/folder rather than a server connection
		bool					isFileBased() const				{ return isFileBased(type()); }

		/// Return true if this database is based on a server connection rather than a file/folder
		bool					isConnectionBased() const		{ return isConnectionBased(type()); }

public:
		static bool				isSupported(int type);

		/// Return true if the given database type is based on a file/folder rather than a server connection
		static bool				isFileBased(int type);

		/// Return true if the given database type is based on a server connection rather than a file/folder
		static bool				isConnectionBased(int type);

private:
		DBConnection		*m_pDB;
		int					m_type;
		SWValueAssocArray	m_tableCheckList;
		};


SW_DEFINE_EXCEPTION(DBException, SWException);

#endif // __db_DBDatabase_h__
