/**
*** @file		DBDataObject.cpp
*** @brief		Object to represent a single database record (row)
*** @version	1.0
*** @author		Simon Sparkes
***
*** Object to represent a single database record (row)
***
*** <b>Copyright 2007-2009 SwiftTec. All rights reserved.</b>
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/


#include "stdafx.h"

#include <swift/SWFlags32.h>
#include <swift/SWFilename.h>
#include <swift/SWFileInfo.h>
#include <swift/SWFile.h>
#include <swift/SWFolder.h>
#include <swift/SWLocalTime.h>
#include <swift/sw_crypt.h>



#include <db/DBDataObject.h>
#include <db/DBDatabase.h>


DBDataObject::DBDataObject() :
	m_loaded(false),
	m_pDB(NULL)
		{
		m_keysAreCaseInsensitive = true;
		}


DBDataObject::~DBDataObject()
		{
		}


DBDataObject::DBDataObject(const DBDataObject &other) :
	SWDataObject(other),
	m_pDB(NULL)
		{
		m_keysAreCaseInsensitive = true;
		*this = other;
		}


DBDataObject &
DBDataObject::operator=(const DBDataObject &other)
		{
		SWDataObject::operator=(other);

		m_keysAreCaseInsensitive = true;

#define	COPY(x)	x = other.x
		COPY(m_loaded);
		COPY(m_pDB);
#undef COPY

		return *this;
		}


bool
DBDataObject::loadCurrentRecord(DBDatabase *pDB, DBRecordSet &rs)
		{
		DBRecord	*pRecord=NULL;

		if (rs.iterator().hasCurrent())
			{
			pRecord = rs.iterator().current();

			SWValue		v;
			SWString	s;
			int			n;

			clear();

			n = (int)rs.getFieldCount();
			for (int i=0;i<n;i++)
				{
				rs.getFieldName(i, s);
				pRecord->getValue(i, v);
				set(s, v);
				}

		
			// Decrypt any password fields
			SWArray<DBTableField> da;
			
			getTableFieldArray(da);
			
			for (int i=0;i<(int)da.size();i++)
				{
				DBTableField	&fd=da[i];

				if (fd.type == DBF_PASSWORD && get(fd.name, v))
					{
					set(fd.name, sw_crypt_decryptString(v.toString()));
					}
				}

			m_loaded = true;
			m_changed = false;

			saveIdentifier();
			saveDatabase(pDB);
			}

		return m_loaded;
		}


void
DBDataObject::saveIdentifier()
		{
		// Default is to do nothing
		}


void
DBDataObject::saveDatabase(DBDatabase *pDB)
		{
		// Default is to save in m_pDB
		m_pDB = pDB;
		}

void
DBDataObject::clear()
		{
		SWDataObject::clear();
		m_loaded = false;
		}


int
DBDataObject::getFieldType(const SWString &name) const
		{
		int					res=DBF_INVALID;
		SWArray<DBTableField> da;
		
		getTableFieldArray(da);
		for (int i=0;i<(int)da.size();i++)
			{
			if (name == da[i].name)
				{
				res = da[i].type;
				break;
				}
			}
			
		return res;
		}


bool
DBDataObject::isTableField(const SWString &key) const
		{
		bool				res=false;
		SWArray<DBTableField> da;
		
		getTableFieldArray(da);
		for (int i=0;i<(int)da.size();i++)
			{
			if (key == da[i].name)
				{
				res = true;
				break;
				}
			}
			
		return res;
		}


bool
DBDataObject::getTableFieldArray(SWArray<DBTableField> &da) const
		{
		bool		res=false;
		DBFieldDef	*pDF;
		
		da.clear();
		res = getTableFieldDefs(pDF);

		while (pDF != NULL && pDF->name != NULL)
			{
			da.add(*pDF);
			pDF++;
			}
			
		return res;
		}


bool
DBDataObject::getTableIndexArray(SWArray<DBTableIndex> &da) const
		{
		bool		res=false;
		DBIndexDef	*pDF;
		
		da.clear();
		res = getTableIndexDefs(pDF);

		while (pDF != NULL && pDF->name != NULL)
			{
			da.add(*pDF);
			pDF++;
			}
			
		return res;
		}

		
bool
DBDataObject::loadFromDBWhere(DBDatabase &db, const SWString &sqlWhere)
		{
		db.checkTable(*this);

		m_loaded = (db.getRecord(*this, sqlWhere) == SW_STATUS_SUCCESS);
		if (m_loaded) saveIdentifier();
		
		return m_loaded;
		}


		
bool
DBDataObject::saveToDBWhere(DBDatabase &db, const SWString &sqlWhere)
		{
		bool	res;

		db.checkTable(*this);

		res = (db.updateRecord(*this, sqlWhere) == SW_STATUS_SUCCESS);
		if (res) saveIdentifier();

		return res;
		}


		
bool
DBDataObject::removeFromDBWhere(DBDatabase &db, const SWString &sqlWhere)
		{
		db.checkTable(*this);

		bool	res=false;

		if (m_loaded)
			{
			res = (db.execute("delete from " + tableName() + " " + sqlWhere) == SW_STATUS_SUCCESS);
			}
		
		return res;
		}