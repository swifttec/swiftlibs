
/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"

#include <swift/SWTokenizer.h>


#include <db/DBTableField.h>


DBTableField::DBTableField()
		{
		clear();
		}


DBTableField::DBTableField(const DBTableField &other)
		{
		*this = other;
		}


DBTableField::DBTableField(const DBFieldDef &other)
		{
		*this = other;
		}


DBTableField::~DBTableField()
		{
		clear();
		}


DBTableField &
DBTableField::operator=(const DBTableField &other)
		{
#define COPY(x) x = other.x;
		COPY(name);
		COPY(type);
		COPY(size);
		COPY(attributes);
#undef COPY

		return *this;
		}


DBTableField &
DBTableField::operator=(const DBFieldDef &other)
		{
		name = other.name;
		type = other.type;
		size = other.size;
		attributes = other.attributes;

		return *this;
		}


bool
DBTableField::operator==(const DBTableField &other)
		{
		return (this == &other);
		}


void
DBTableField::clear()
		{
		name.empty();
		size = 0;
		type = 0;
		attributes = 0;
		}

