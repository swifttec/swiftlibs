/**
*** @file		DBSqlServerDatabase.h
*** @brief		Basic database access object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Defines the DBSqlServerDatabase class
***
*** <b>Copyright 2007 SwiftTec. All rights reserved.</b>
**/
/****************************************************************************
** $Date: 2008/10/09 14:49:39 $
** $Author: simon $
** $Revision: 1.1 $
*****************************************************************************
** $Log: DBSqlServerDatabase.h,v $
** Revision 1.1  2008/10/09 14:49:39  simon
** Updated database model to include SqlServer and improved interface
**
****************************************************************************/


#ifndef __db_DBSqlServerDatabase_h__
#define __db_DBSqlServerDatabase_h__

#if defined(SW_PLATFORM_WINDOWS) && defined(SW_BUILD_MFC)

#include <db/DBOpenConnectivityDatabase.h>

// Disable warning about deprecated objects
#pragma warning(disable: 4995)
#pragma warning(disable: 4251)

class DB_DLL_EXPORT DBSqlServerDatabase : public DBOpenConnectivityDatabase
		{
public:
		DBSqlServerDatabase();
		virtual ~DBSqlServerDatabase();

		/// Open/connect to the database
		virtual sw_status_t		opendb(const SWArray<SWNamedValue> &va);

		virtual sw_status_t		getTableInfo(const SWString &table, DBTableInfo &tinfo);

		/// Check a table against the definitions
		virtual sw_status_t		checkTable(const SWString &name, const SWArray<DBTableField> &fieldDefs, const SWArray<DBTableIndex> &indexDefs);

		/// Read the table list from the database
		virtual sw_status_t		getTableList(SWStringArray &tlist);

		virtual SWString		sqlBooleanString(bool v) const;
		virtual bool			tableExists(const SWString &table);
		virtual bool			tableHasField(const SWString &table, const SWString &field);

		};


#endif // defined(SW_PLATFORM_WINDOWS)

#endif // __db_DBSqlServerDatabase_h__
