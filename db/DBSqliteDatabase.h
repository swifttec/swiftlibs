/**
*** @file		DBSqliteDatabase.h
*** @brief		Basic database access object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Defines the DBSqliteDatabase class
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __db_DBSqliteDatabase_h__
#define __db_DBSqliteDatabase_h__

#include <db/DBConnection.h>

#include <swift/SWString.h>



// Disable warning about deprecated objects
#pragma warning(disable: 4995)


// Forward declaration
typedef struct sqlite3 sqlite3;

class DB_DLL_EXPORT DBSqliteDatabase : public DBConnection
		{
public:
		DBSqliteDatabase();
		virtual ~DBSqliteDatabase();

		/// Open the database
		virtual sw_status_t		opendb(const SWArray<SWNamedValue> &va);

		/// Close the database
		virtual sw_status_t		close();

		/// Check if the database is supported on this system
		virtual bool			isSupported();

		/// Check a table against the definitions
		virtual sw_status_t		checkTable(const SWString &name, const SWArray<DBTableField> &fieldDefs, const SWArray<DBTableIndex> &indexDefs);

		/// Exectute the SQL statement
		virtual sw_status_t		execute(const SWString &sql, DBRecordSet *pRecordSet);

		/// Read the table list from the database
		virtual sw_status_t		getTableList(SWStringArray &tlist);

		virtual SWString		sqlBooleanString(bool v) const;
		virtual SWString		sqlDateString(const SWDate &v) const;
		virtual SWString		sqlDateTimeString(const SWLocalTime &v) const;

		virtual sw_status_t		getTableInfo(const SWString &table, DBTableInfo &tinfo);
		virtual bool			tableExists(const SWString &table);
		virtual bool			tableHasField(const SWString &table, const SWString &field);

		/// Return the filename of the actual database file.
		const SWString &		filename() const	{ return m_filename; }

private:
		sqlite3		*m_pDB;		///< The actual database
		SWString	m_filename;
		};


#endif // __db_DBSqliteDatabase_h__
