/**
*** @file		DBDataObject.h
*** @brief		Object to represent a single database record (row)
*** @version	1.0
*** @author		Simon Sparkes
***
*** Object to represent a single database record (row)
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __db_DBDataObject_h__
#define __db_DBDataObject_h__

#include <db/types.h>
#include <db/DBRecordSet.h>
#include <db/DBTableField.h>
#include <db/DBTableIndex.h>

#include <swift/SWDate.h>
#include <swift/SWLocalTime.h>
#include <swift/SWValue.h>
#include <swift/SWString.h>
#include <swift/SWHashMap.h>
#include <swift/SWDataObject.h>
#include <swift/SWArray.h>
#include <swift/SWConfigKey.h>


	
// forward declarations
class DBDatabase;


class DB_DLL_EXPORT DBDataObject : public SWDataObject
		{
friend class DBDatabase;

public:
		DBDataObject();
		DBDataObject(const DBDataObject &other);
		virtual ~DBDataObject();
		
		DBDataObject &		operator=(const DBDataObject &other);

		bool				loadCurrentRecord(DBDatabase *pDB, DBRecordSet &rs);
		
		/// Clear this record
		virtual void		clear();

		/// Load this record from the database
		virtual bool		loadFromDBWhere(DBDatabase &db, const SWString &sqlWhere);

		/// Save this record into the database
		virtual bool		saveToDBWhere(DBDatabase &db, const SWString &sqlWhere);
		
		/// Remove this record from the given database - use carefully as can remove multiple records.
		virtual bool		removeFromDBWhere(DBDatabase &db, const SWString &sqlWhere);

		// Called by loadCurrentRecord to save the current identifier which can be used for updates
		virtual void		saveIdentifier();

		// Called by loadCurrentRecord to save the current identifier which can be used for updates
		virtual void		saveDatabase(DBDatabase *pDB);

		/// Test if this object has been loaded from a database
		bool				loaded() const			{ return m_loaded; }

		/// Test if this object has been loaded from a database (alternative name)
		bool				isLoaded() const		{ return m_loaded; }

		/// Test this object is a new record (i.e. not loaded from a database)
		bool				isNewRecord() const		{ return !m_loaded; }

		/// Test if the given name is a field in the table used by this object
		bool				isTableField(const SWString &key) const;
		
		/// Get the type of the field in the table used by this object
		virtual int			getFieldType(const SWString &name) const;

public: // Overrides
		virtual SWString	tableName() const=0;
		virtual int			tableVersion() const=0;
		virtual bool		getTableFieldDefs(DBFieldDef *&pDefs) const=0;
		virtual bool		getTableIndexDefs(DBIndexDef *&pDefs) const=0;
		virtual bool		getTableFieldArray(SWArray<DBTableField> &da) const;
		virtual bool		getTableIndexArray(SWArray<DBTableIndex> &da) const;

protected:
		void				setAllValues();

protected:
		bool				m_loaded;
		DBDatabase			*m_pDB;
		};


#define DB_DECLARE_LOADRECORDS_ARRAY(type)	static bool	loadRecordsFromDB(DBDatabase &db, SWArray<type> &data, const SWString &where="", const SWString &order="")
#define DB_DECLARE_LOADRECORDS_LIST(type)	static bool	loadRecordsFromDB(DBDatabase &db, SWList<type> &data, const SWString &where="", const SWString &order="")


#define DB_IMPLEMENT_LOADRECORDS_ARRAY(type, deforderfields) \
bool type::loadRecordsFromDB(DBDatabase &db, SWArray<type> &results, const SWString &where, const SWString &order) \
		{ \
		type tmprec; \
		db.checkTable(tmprec); \
		SWString	sql; \
		bool		res=false; \
		results.clear(); \
		sql = "select * from "; \
		sql += TABLE_NAME; \
		if (!where.isEmpty()) \
			{ \
			sql += " "; \
			sql += where; \
			} \
		if (!order.isEmpty()) \
			{ \
			sql += " "; \
			sql += order; \
			} \
		else \
			{ \
			sql += " order by " deforderfields; \
			} \
		DBRecordSet		rset; \
		int				pos=0; \
		if (db.execute(sql, rset)==SW_STATUS_SUCCESS) \
			{ \
			SWIterator<DBRecord>	it; \
			it = rset.iterator(); \
			while (it.hasNext()) \
				{ \
				it.next(); \
				results[pos++].loadCurrentRecord(&db, rset); \
				} \
			res = true; \
			} \
		return res; \
		}

#define DB_IMPLEMENT_LOADRECORDS_LIST(type, deforderfields) \
bool type::loadRecordsFromDB(DBDatabase &db, SWList<type> &results, const SWString &where, const SWString &order) \
		{ \
		type tmprec; \
		db.checkTable(tmprec); \
		SWString	sql; \
		bool		res=false; \
		results.clear(); \
		sql = "select * from "; \
		sql += TABLE_NAME; \
		if (!where.isEmpty()) \
			{ \
			sql += " "; \
			sql += where; \
			} \
		if (!order.isEmpty()) \
			{ \
			sql += " "; \
			sql += order; \
			} \
		else \
			{ \
			sql += " order by " deforderfields; \
			} \
		DBRecordSet		rset; \
		if (db.execute(sql, rset)==SW_STATUS_SUCCESS) \
			{ \
			SWIterator<DBRecord>	it; \
			type					rec; \
			it = rset.iterator(); \
			while (it.hasNext()) \
				{ \
				it.next(); \
				rec.loadCurrentRecord(&db, rset); \
				results.add(rec); \
				} \
			res = true; \
			} \
		return res; \
		}

#define DB_REGISTER_TABLE(DOBJ, TNAME, TVER, TDEF, IDEF) \
		static int	rdo_##DOBJ=DBGeneralDatabase::registerDataObject(#DOBJ, new DOBJ()); \
		SWString	DOBJ::tableName() const			{ return TNAME; } \
		SWString	DOBJ::getTableName()			{ return TNAME; } \
		int			DOBJ::tableVersion() const	{ return TVER; } \
		int			DOBJ::getTableVersion()		{ return TVER; } \
		bool		DOBJ::getTableFieldDefs(DBFieldDef *&pDefs) const { pDefs = TDEF; return true; } \
		bool		DOBJ::getTableIndexDefs(DBIndexDef *&pDefs) const { pDefs = IDEF; return true; }

#define DB_REGISTER_TABLE_NO_DEFS(DOBJ, TNAME, TVER, TDEF, IDEF) \
		static int	rdo_##DOBJ=DBGeneralDatabase::registerDataObject(#DOBJ, new DOBJ()); \
		SWString	DOBJ::tableName() const			{ return TNAME; } \
		SWString	DOBJ::getTableName()			{ return TNAME; } \
		int			DOBJ::tableVersion() const	{ return TVER; } \
		int			DOBJ::getTableVersion()		{ return TVER; }

#define DB_DECLARE_REGISTER_TABLE(type) \
public: \
		virtual SWString	tableName() const; \
		virtual int			tableVersion() const; \
		virtual bool		getTableFieldDefs(DBFieldDef *&pDefs) const; \
		virtual bool		getTableIndexDefs(DBIndexDef *&pDefs) const; \
		static SWString		getTableName(); \
		static int			getTableVersion();



#define DB_DECLARE_TABLE_METHODS(type) \
		DB_DECLARE_REGISTER_TABLE(type) \
		DB_DECLARE_LOADRECORDS_ARRAY(type); \
		DB_DECLARE_LOADRECORDS_LIST(type);	

#define DB_DECLARE_TABLE_METHODS_NOLOADRECORDS(type) \
		DB_DECLARE_REGISTER_TABLE(type)

#define DB_IMPLEMENT_TABLE_METHODS(DOBJ, TNAME, TVER, TDEF, IDEF, ORDERFIELDS) \
		DB_REGISTER_TABLE(DOBJ, TNAME, TVER, TDEF, IDEF) \
		DB_IMPLEMENT_LOADRECORDS_ARRAY(DOBJ, ORDERFIELDS) \
		DB_IMPLEMENT_LOADRECORDS_LIST(DOBJ, ORDERFIELDS)

#define DB_IMPLEMENT_TABLE_METHODS_NOLOADRECORDS(DOBJ, TNAME, TVER, TDEF, IDEF, ORDERFIELDS) \
		DB_REGISTER_TABLE(DOBJ, TNAME, TVER, TDEF, IDEF)

#define DB_IMPLEMENT_TABLE_METHODS_NOREGISTER(DOBJ, TNAME, TVER, TDEF, IDEF, ORDERFIELDS) \
		SWString		DOBJ::tableName() const			{ return TNAME; } \
		SWString		DOBJ::getTableName()			{ return TNAME; } \
		int			DOBJ::tableVersion() const	{ return TVER; } \
		int			DOBJ::getTableVersion()		{ return TVER; } \
		bool		DOBJ::getTableFieldDefs(DBFieldDef *&pDefs) const { pDefs = TDEF; return true; } \
		bool		DOBJ::getTableIndexDefs(DBIndexDef *&pDefs) const { pDefs = IDEF; return true; }


#endif // __db_DBRecord_h__
