/**
*** @file		types.h
*** @brief		DB-specific types
*** @version	1.0
*** @author		Simon Sparkes
***
*** Defines the types used by the DB library.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __db_types_h__
#define __db_types_h__

#include <db/db.h>

enum DBFieldType
		{
		DBF_INVALID=0,		// An invalid type
		DBF_BOOLEAN=1,		// A boolean value
		DBF_INT8=2,			// An 8-bit signed integer
		DBF_INT16=3,		// A 16-bit signed integer
		DBF_INT32=4,		// A 32-bit signed integer
		DBF_INT64=5,		// A 64-bit signed integer
		DBF_FLOAT=6,		// A double
		DBF_DOUBLE=7,		// A double
		DBF_DATE=8,			// A date (SWDate) stored in local time format
		DBF_DATETIME=9,		// A date/time (SWTime) stored in local time format
		DBF_TIMESTAMP=10,	// A timestamp (date/time) stored in local time format
		DBF_TEXT=11,		// A text field (must specify length)
		DBF_PASSWORD=12,	// An encrypted text field
		DBF_MONEY=13,		// A money field (32-bit integer)
		DBF_DECIMAL=14,		// A packed decimal field with known number of places after the decimal point
		DBF_BLOB=15,		// A binary data field
		DBF_SMALL_BLOB=16,	// A binary data field
		DBF_MEDIUM_BLOB=17,	// A binary data field
		DBF_LARGE_BLOB=18,	// A binary data field
		
		// The last in the list
		DBF_MAX
		};


enum DBFieldAttribute
		{
		DBA_NORMAL=0x0000,		// Normal attibutes
		DBA_NOTNULL=0x0001,		// Field may not be null
		DBA_AUTOINC=0x0002,		// An auto-increment value
		};


typedef struct db_fielddef
		{
		const char	*name;		// The field name
		DBFieldType	type;		// The field type
 		long		size;		// The field length (strings)
 		long		attributes;	// The field attributes
		} DBFieldDef;
		

typedef struct db_indexdef
		{
		const char	*name;		// The index name
		const char	*fields;	// A comma-separated list of fields
		bool		unique;		// If true no duplicates allowed
		} DBIndexDef;


// The max size for a data blob without encountering SQL issues
#define DB_MAX_BLOB_SIZE	512000

#endif // __db_types_h__
