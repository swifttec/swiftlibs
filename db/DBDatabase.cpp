
/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "stdafx.h"

#include <swift/SWTokenizer.h>
#include <swift/sw_crypt.h>


#include <db/DBDatabase.h>
#include <db/DBMySqlDatabase.h>
#include <db/DBSqliteDatabase.h>
#include <db/DBSqlServerDatabase.h>
#include <db/DBJetDatabase.h>
#include <db/DBDaoDatabase.h>

DBDatabase::DBDatabase() :
	m_pDB(NULL),
	m_type(DB_TYPE_NONE)
		{
		}


DBDatabase::~DBDatabase()
		{
		// Make sure the database is actually closed
		close();
		}

bool
DBDatabase::isFileBased(int type)
		{
		bool	res=false;

		switch (type)
			{
#ifdef SW_CONFIG_USE_MYSQL
			case DB_TYPE_MYSQL:
#endif // SW_CONFIG_USE_MYSQL
			case DB_TYPE_SQLSERVER:
				res = false;
				break;

			case DB_TYPE_SQLITE3:
			case DB_TYPE_DAO:
			case DB_TYPE_JET:
				res = true;
				break;
			}

		return res;
		}


bool
DBDatabase::isConnectionBased(int type)
		{
		return !isFileBased(type);
		}


bool
DBDatabase::isSupported(int type)
		{
		bool	res=false;

		switch (type)
			{
#ifdef SW_CONFIG_USE_MYSQL
			case DB_TYPE_MYSQL:
				{
				DBMySqlDatabase	db;

				res = db.isSupported();
				}
				break;
#endif // SW_CONFIG_USE_MYSQL

			case DB_TYPE_SQLITE3:
				{
				DBSqliteDatabase	db;

				res = db.isSupported();
				}
				break;

#if defined(SW_PLATFORM_WINDOWS) && defined(SW_BUILD_MFC)

			case DB_TYPE_DAO:
#if defined(_WIN64)
				{
				DBJetDatabase	db;

				res = db.isSupported();
				}
#else
				res = true;
#endif
				break;


			case DB_TYPE_SQLSERVER:
				{
				DBSqlServerDatabase	db;

				res = db.isSupported();
				}
				break;

			case DB_TYPE_JET:
				{
				DBJetDatabase	db;

				res = db.isSupported();
				}
				break;
#endif // defined(SW_PLATFORM_WINDOWS) && defined(SW_BUILD_MFC)
			}

		return res;
		}


sw_status_t
DBDatabase::open(int type, const SWString &params)
		{
		sw_status_t		res=SW_STATUS_SUCCESS;
		DBConnection	*pDB=NULL;

		if (m_pDB != NULL)
			{
			res = SW_STATUS_ALREADY_OPEN;
			}
		else
			{
			switch (type)
				{
#ifdef SW_CONFIG_USE_MYSQL
				case DB_TYPE_MYSQL:
					pDB = new DBMySqlDatabase();
					break;
#endif // SW_CONFIG_USE_MYSQL

				case DB_TYPE_SQLITE3:
					pDB = new DBSqliteDatabase();
					break;

#if defined(SW_PLATFORM_WINDOWS) && defined(SW_BUILD_MFC)
				case DB_TYPE_DAO:
#if !defined(_WIN64)
					pDB = new DBDaoDatabase();
#else
					// 64-bit windows does not have DAO, but Jet support is close.
					// but not as good for table defs.
					pDB = new DBJetDatabase();
#endif
					break;

				case DB_TYPE_SQLSERVER:
					pDB = new DBSqlServerDatabase();
					break;

				case DB_TYPE_JET:
					pDB = new DBJetDatabase();
					break;
#endif
				}

			if (pDB == NULL)
				{
				res = SW_STATUS_INVALID_DATA;
				}
			}

		if (res == SW_STATUS_SUCCESS)
			{
			res = pDB->open(params);
			}

		if (res == SW_STATUS_SUCCESS || res == SW_STATUS_DB_TABLE_CREATED)
			{
			// Record the type if successful
			m_type = type;
			m_pDB = pDB;
			}
		else if (pDB != NULL)
			{
			pDB->close();
			delete pDB;
			pDB = NULL;
			m_type = DB_TYPE_NONE;
			}

		m_tableCheckList.clear();

		return res;
		}


sw_status_t
DBDatabase::close()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (m_pDB != NULL)
			{
			m_pDB->close();
			delete m_pDB;
			m_pDB = NULL;
			m_type = DB_TYPE_NONE;
			}

		m_tableCheckList.clear();

		return res;
		}


bool
DBDatabase::isOpen() const
		{
		return (m_pDB != NULL);
		}


sw_status_t
DBDatabase::execute(const SWString &sql)
		{
		return execute(sql, NULL);
		}


sw_status_t
DBDatabase::execute(const SWString &sql, DBRecordSet &rset)
		{
		return execute(sql, &rset);
		}


sw_status_t
DBDatabase::execute(const SWString &sql, DBRecordSet *pRecordSet)
		{
		sw_status_t	res=SW_STATUS_DB_ERROR;

		if (sql.isEmpty())
			{
			res = SW_STATUS_INVALID_PARAMETER;
			}
		else
			{
//			TRACE("SQL EXECUTE: %s\n", sql.c_str());
			if (m_pDB != NULL)
				{
				res = m_pDB->execute(sql, pRecordSet);
				}
			}
			
		return res;
		}


sw_status_t
DBDatabase::execute(const SWString &sql, SWValue &v)
		{
		sw_status_t	res=SW_STATUS_DB_ERROR;

		if (sql.isEmpty())
			{
			res = SW_STATUS_INVALID_PARAMETER;
			}
		else
			{
			if (m_pDB != NULL)
				{
				DBRecordSet		rset;
				DBRecord		*pRecord;

				res = m_pDB->execute(sql, &rset);
				if (res == SW_STATUS_SUCCESS)
					{
					SWIterator<DBRecord>	it=rset.iterator();

					if (it.hasNext())
						{
						pRecord = it.next();

						// ID
						pRecord->getValue(0, v);
						res = SW_STATUS_SUCCESS;
						}
					else
						{
						res = SW_STATUS_NOT_FOUND;
						}
					}
				}
			}
			
		return res;
		}


sw_status_t
DBDatabase::execute(const SWString &sql, SWList<DBGenericDataObject> &dlist)
		{
		DBRecordSet		rset;
		sw_status_t		res=SW_STATUS_SUCCESS;
		
		dlist.clear();
		
		if ((res = execute(sql, rset)) == SW_STATUS_SUCCESS)
			{
			SWIterator<DBRecord>	it;
			
			it = rset.iterator();
			while (it.hasNext())
				{
				it.next();
				
				DBGenericDataObject	gdo;
				
				gdo.loadCurrentRecord(this, rset);
				dlist.add(gdo);
				}
			}
		
		return res;
		}

		
		
sw_status_t
DBDatabase::execute(const SWString &sql, SWArray<DBGenericDataObject> &dlist)
		{
		DBRecordSet		rset;
		sw_status_t		res=SW_STATUS_SUCCESS;
		
		dlist.clear();
		
		if ((res = execute(sql, rset)) == SW_STATUS_SUCCESS)
			{
			SWIterator<DBRecord>	it;
			int						pos=0;
			
			it = rset.iterator();
			while (it.hasNext())
				{
				it.next();
				
				DBGenericDataObject	&gdo=dlist[pos++];
				
				gdo.loadCurrentRecord(this, rset);
				}
			}
		
		return res;
		}



sw_status_t
DBDatabase::checkTable(const SWString &name, const SWArray<DBTableField> &fieldDefs, const SWArray<DBTableIndex> &indexDefs, bool force)
		{
		sw_status_t	res=SW_STATUS_DB_ERROR;

		if (!isTableChecked(name) || force)
			{
			if (m_pDB != NULL)
				{
	//			TRACE("DBDatabase: Checking table %s\n", name.c_str());
				res = m_pDB->checkTable(name, fieldDefs, indexDefs);
				if (res == SW_STATUS_SUCCESS || res == SW_STATUS_DB_TABLE_CREATED)
					{
					m_tableCheckList[name] = 1;
					}
				}
			}
		else
			{
			res = SW_STATUS_SUCCESS;
			}

		return res;
		}


sw_status_t
DBDatabase::getTableInfo(const SWString &name, DBTableInfo &ti)
		{
		sw_status_t	res=SW_STATUS_DB_ERROR;

		if (m_pDB != NULL)
			{
			res = m_pDB->getTableInfo(name, ti);
			}

		return res;
		}


sw_status_t
DBDatabase::getFieldInfo(const SWString &table, const SWString &field, DBFieldInfo &fi)
		{
		sw_status_t	res=SW_STATUS_DB_ERROR;

		if (m_pDB != NULL)
			{
			DBTableInfo	ti;

			res = m_pDB->getTableInfo(table, ti);
			if (res == SW_STATUS_SUCCESS)
				{
				int		nfields=(int)ti.getFieldCount();

				res = SW_STATUS_NOT_FOUND;
				for (int i=0;i<nfields;i++)
					{
					DBFieldInfo	info;

					ti.getFieldInfo(i, info);
					if (info.name() == field)
						{
						res = SW_STATUS_SUCCESS;
						fi = info;
						}
					}
				}
			}

		return res;
		}


sw_status_t
DBDatabase::getTableList(SWStringArray &tlist)
		{
		sw_status_t	res=SW_STATUS_DB_ERROR;

		if (m_pDB != NULL)
			{
			res = m_pDB->getTableList(tlist);
			}

		return res;
		}


bool
DBDatabase::tableExists(const SWString &name)
		{
		bool	res=false;

		if (m_pDB != NULL)
			{
			res = m_pDB->tableExists(name);
			}

		return res;
		}

SWString
DBDatabase::sqlBinaryValue(void *pData, sw_size_t len) const
		{
		SWString	res;

		if (m_pDB != NULL)
			{
			res = m_pDB->sqlBinaryValue(pData, len);
			}

		return res;
		}


bool
DBDatabase::tableHasField(const SWString &table, const SWString &field)
		{
		bool	res=false;

		if (m_pDB != NULL)
			{
			res = m_pDB->tableHasField(table, field);
			}

		return res;
		}

bool
DBDatabase::isTableChecked(const SWString &name) const
		{
		return m_tableCheckList.defined(name);
		}


sw_status_t
DBDatabase::checkTable(const DBDataObject &rec, bool force, const SWString &tablename)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (!isTableChecked(rec.tableName()) || force)
			{
			SWArray<DBTableField>	fieldDefs;
			SWArray<DBTableIndex>	indexDefs;
			SWString				name(tablename);

			if (name.isEmpty()) name = rec.tableName();

			rec.getTableFieldArray(fieldDefs);
			rec.getTableIndexArray(indexDefs);

			res = checkTable(name, fieldDefs, indexDefs, force);
			}

		return res;
		}



sw_status_t
DBDatabase::updateRecord(DBDataObject &rec, const SWString &where)
		{
		SWArray<DBTableField>	fieldDefs;
		
		rec.getTableFieldArray(fieldDefs);

		return updateRecord(rec.tableName(), rec, fieldDefs, where);
		}


sw_status_t
DBDatabase::updateRecord(const SWString &table, DBDataObject &rec, const SWArray<DBTableField> &fieldDefs, const SWString &where)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		SWString	sql;

		sql = sqlUpdateRecord(table, rec, fieldDefs, where);
		
		if (!sql.isEmpty())
			{
			res = execute(sql);
			}
		
		if (res == SW_STATUS_SUCCESS)
			{
			rec.m_loaded = true;
			rec.m_changed = false;
			}
		
		return res;
		}


SWString
DBDatabase::sqlUpdateRecord(DBDataObject &rec, const SWString &where)
		{
		SWArray<DBTableField>	fieldDefs;
		
		rec.getTableFieldArray(fieldDefs);

		return sqlUpdateRecord(rec.tableName(), rec, fieldDefs, where);
		}


void
DBDatabase::sqlAddValueToString(SWValue &v, const DBTableField &fd, SWString &sql)
		{
		if (v.type() == SWValue::VtNone)
			{
			sql += _T("null");
			}
		else
			{
			switch (fd.type)
				{
				case DBF_PASSWORD:
					sql += m_pDB->sqlQuote(sw_crypt_encryptString(v.toString()));
					break;
			
				case DBF_TEXT:
					{
					SWString	vs=v.toString();

					if ((long)vs.length() > fd.size)
						{
						// truncate so that SQL does no fail - may still be a problem with UTF encoding.
						vs.remove(fd.size, vs.length()-fd.size);
						}

					sql += m_pDB->sqlQuote(vs);
					}
					break;
					
				case DBF_INT8:
					sql += SWString::valueOf(v.toInt8());
					break;

				case DBF_INT16:
					sql += SWString::valueOf(v.toInt16());
					break;

				case DBF_MONEY:
				case DBF_INT32:
					sql += SWString::valueOf(v.toInt32());
					break;

				case DBF_INT64:
					sql += SWString::valueOf(v.toInt64());
					break;
				
				case DBF_BOOLEAN:
					sql += m_pDB->sqlBooleanString(v.toBoolean());
					break;
				
				case DBF_DATE:
					sql += m_pDB->sqlDateString(v.toDate());
					break;
				
				case DBF_TIMESTAMP:
				case DBF_DATETIME:
					sql += m_pDB->sqlDateTimeString(v.toTime());
					break;
					
				case DBF_DOUBLE:
					sql += SWString::valueOf(v.toDouble());
					break;

				case DBF_BLOB:
				case DBF_SMALL_BLOB:
				case DBF_MEDIUM_BLOB:
				case DBF_LARGE_BLOB:
					{
					SWString		nstr;
					const sw_byte_t *p;
					sw_size_t		len;

					p = (const sw_byte_t *)v.data();
					len = v.length();

					sql += m_pDB->sqlBinaryValue(p, len);
					}
					break;

				default:
					// Unhandled data type
					throw SW_EXCEPTION_TEXT(DBException, "Unexpected data type encountered in sqlAddValueToString");
					break;
				}
			}
		}


SWString
DBDatabase::sqlUpdateRecord(const SWString &table, DBDataObject &rec, const SWArray<DBTableField> &fieldDefs, const SWString &where)
		{
		SWString	sql;
		SWValue		v;
		int			fieldcount=0;

		for (int i=0;i<(int)fieldDefs.size();i++)
			{
			const DBTableField	&fd=fieldDefs.get(i);

			// Don't update auto-increment fields
			if ((fd.attributes & DBA_AUTOINC) == 0)
				{
				rec.get(fd.name, v);
				if (v.changed())
					{
					if (fieldcount)
						{
						sql += _T(",");
						}
					else
						{
						if (
#ifdef SW_CONFIG_USE_MYSQL
							m_type == DB_TYPE_MYSQL ||
#endif // SW_CONFIG_USE_MYSQL
							m_type == DB_TYPE_SQLITE3)
							{
							// MySQL requires ASCII
							sql = "update ";
							}
						else
							{
							sql = _T("update ");
							}

						sql += table;
						sql += _T(" set");
						}
					
					sql += _T(" ");
					sql += fd.name;
					sql += _T(" = ");
				
					sqlAddValueToString(v, fd, sql);
					fieldcount++;
					}
				}
			}
		
		if (!sql.isEmpty())
			{
			sql += _T(" ");
			sql += where;
			}
		
		return sql;
		}		


sw_status_t
DBDatabase::insertRecord(DBDataObject &rec)
		{
		SWArray<DBTableField>	fieldDefs;
		
		rec.getTableFieldArray(fieldDefs);

		return insertRecord(rec.tableName(), rec, fieldDefs);
		}


sw_status_t
DBDatabase::insertRecord(const SWString &table, DBDataObject &rec, const SWArray<DBTableField> &fieldDefs)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		SWString	sql;

		sql = sqlInsertRecord(table, rec, fieldDefs);
		
		res = execute(sql);
		if (res == SW_STATUS_SUCCESS)
			{
			rec.m_loaded = true;
			rec.m_changed = false;
			}

		return res;
		}


SWString
DBDatabase::sqlInsertRecord(DBDataObject &rec)
		{
		SWArray<DBTableField>	fieldDefs;
		
		rec.getTableFieldArray(fieldDefs);

		return sqlInsertRecord(rec.tableName(), rec, fieldDefs);
		}


SWString
DBDatabase::sqlInsertRecord(const SWString &table, DBDataObject &rec, const SWArray<DBTableField> &fieldDefs)
		{
		SWString	sql;
		SWValue		v;
		bool		comma;
		
		if (
#ifdef SW_CONFIG_USE_MYSQL
		m_type == DB_TYPE_MYSQL || 
#endif // SW_CONFIG_USE_MYSQL
		m_type == DB_TYPE_SQLITE3)
			{
			// MySQL requires ASCII
			sql = "insert into ";
			}
		else
			{
			sql = _T("insert into ");
			}

		sql += table;
		sql += _T(" (");
		
		comma = false;
		for (int i=0;i<(int)fieldDefs.size();i++)
			{
			const DBTableField	&fd=fieldDefs.get(i);

			rec.get(fd.name, v);
			if (v.type() == SWValue::VtNone && (fd.attributes & DBA_AUTOINC) != 0)
				{
				// Skip this field - it is automatic
				continue;
				}

			if (comma) sql += _T(", ");

			sql	+= fd.name;
			comma = true;
			}
					
		sql += _T(") values(");

		comma = false;

		for (int i=0;i<(int)fieldDefs.size();i++)
			{
			const DBTableField	&fd=fieldDefs.get(i);

			rec.get(fd.name, v);
			if (v.type() == SWValue::VtNone && (fd.attributes & DBA_AUTOINC) != 0)
				{
				// Skip this field - it is automatic
				continue;
				}

			if (comma) sql += _T(", ");

			sqlAddValueToString(v, fd, sql);
			comma = true;
			}
		
		sql += _T(")");

		return sql;
		}


sw_status_t
DBDatabase::getRecord(DBDataObject &rec, const SWString &where)
		{
		sw_status_t		res=SW_STATUS_SUCCESS;
		DBRecordSet		rset;
		SWString		sql;

		sql = sqlGetRecord(rec, where);
		
		res = execute(sql, rset);
		if (res == SW_STATUS_SUCCESS)
			{
			SWIterator<DBRecord>	it;
			
			it = rset.iterator();
			if (it.hasNext())
				{
				it.next();
				rec.loadCurrentRecord(this, rset);
				}
			else
				{
				res = SW_STATUS_NOT_FOUND;
				}
			}
		
		return res;
		}





SWString
DBDatabase::sqlGetRecord(DBDataObject &rec, const SWString &where)
		{
		SWString		sql;
		
		if (
#ifdef SW_CONFIG_USE_MYSQL
		m_type == DB_TYPE_MYSQL || 
#endif // SW_CONFIG_USE_MYSQL
		m_type == DB_TYPE_SQLITE3)
			{
			// MySQL requires ASCII
			sql = "select * from ";
			}
		else
			{
			sql = _T("select * from ");
			}

		sql += rec.tableName();
		if (!where.isEmpty())
			{
			sql += _T(" ");
			sql += where;
			}

		return sql;
		}



int
DBDatabase::type() const
		{
		return m_type;
		}


SWString
DBDatabase::params() const
		{
		SWString	res;

		if (m_pDB != NULL)
			{
			res = m_pDB->getConnectionString();
			}

		return res;
		}


SWString
DBDatabase::sqlBooleanString(bool v)
		{
		SWString	s;

		if (m_pDB != NULL)
			{
			s = m_pDB->sqlBooleanString(v);
			}
		
		return s;
		}


SWString
DBDatabase::sqlQuote(const SWString &v)
		{
		SWString	s;

		if (m_pDB != NULL)
			{
			s = m_pDB->sqlQuote(v);
			}
		
		return s;
		}


SWString
DBDatabase::sqlLikeWildcard()
		{
		SWString	s;

		if (m_pDB != NULL)
			{
			s = m_pDB->sqlLikeWildcard();
			}
		
		return s;
		}


SWString
DBDatabase::sqlDateString(const SWDate &v)
		{
		SWString	s;

		if (m_pDB != NULL)
			{
			s = m_pDB->sqlDateString(v);
			}
		
		return s;
		}


SWString
DBDatabase::sqlDateTimeString(const SWLocalTime &v)
		{
		SWString	s;

		if (m_pDB != NULL)
			{
			s = m_pDB->sqlDateTimeString(v);
			}
		
		return s;
		}


sw_status_t
DBDatabase::dropTable(const SWString &table)
		{
		sw_status_t	res=SW_STATUS_DB_ERROR;

		if (m_pDB != NULL)
			{
			res = m_pDB->dropTable(table);
			}
		
		return res;
		}


sw_status_t
DBDatabase::renameTable(const SWString &oldname, const SWString &newname)
		{
		sw_status_t	res=SW_STATUS_DB_ERROR;

		if (m_pDB != NULL)
			{
			res = m_pDB->renameTable(oldname, newname);
			}
		
		return res;
		}
