/**
*** @file		DBRecord.h
*** @brief		Object to represent a single database record (row)
*** @version	1.0
*** @author		Simon Sparkes
***
*** Object to represent a single database record (row)
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __db_DBRecord_h__
#define __db_DBRecord_h__

#include <db/types.h>

#include <swift/SWArray.h>
#include <swift/SWValue.h>




#if defined(SW_PLATFORM_WINDOWS)
	
#endif

class DB_DLL_EXPORT DBRecord
		{
public:
		DBRecord();
		DBRecord(const DBRecord &other);
		virtual ~DBRecord();

		DBRecord &	operator=(const DBRecord &other);
		bool		operator==(const DBRecord &other);
		
		/// Clear the record
		void	clear();

		/// Return the number of fields (count)
		sw_size_t	size() const						{ return m_values.size(); }

		/// Return the nth field
		void		getValue(int n, SWValue &v) const;

		/// Add a field
		void		setValue(int n, const SWValue &v, bool clearChangedFlag=false);

protected:
		SWArray<SWValue>	m_values;
		};


#endif // __db_DBRecord_h__
