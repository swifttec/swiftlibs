/**
*** @file		DBOpenConnectivityDatabase.h
*** @brief		Basic database access object
*** @version	1.0
*** @author		Simon Sparkes
***
*** Defines the DBOpenConnectivityDatabase class
***
*** <b>Copyright 2007 SwiftTec. All rights reserved.</b>
**/
/****************************************************************************
** $Date: 2012/05/18 21:43:15 $
** $Author: simon $
** $Revision: 1.3 $
*****************************************************************************
** $Log: DBOpenConnectivityDatabase.cpp,v $
** Revision 1.3  2012/05/18 21:43:15  simon
** WIP
**
** Revision 1.2  2009/08/10 07:06:58  simon
** Minor changes for VC9
**
** Revision 1.1  2008/10/09 14:49:39  simon
** Updated database model to include SqlServer and improved interface
**
****************************************************************************/

#include "stdafx.h"

#if defined(SW_PLATFORM_WINDOWS) && defined(SW_BUILD_MFC)

// Swift includes
#include <swift/SWTokenizer.h>
#include <swift/SWStringArray.h>
#include <swift/SWRegularExpression.h>
#include <swift/SWFileInfo.h>
#include <swift/SWCollectionIterator.h>
#include <swift/SWGuard.h>
#include <swift/SWLocalTime.h>

// DB includes
#include <db/DBOpenConnectivityDatabase.h>
#include <db/DBRecord.h>

// Disable warning about deprecated objects
#pragma warning(disable: 4995)
#pragma warning(disable: 4251)


// An interlocked count so we only init and term the AfxDao layer once
SWInterlockedInt32	DBOpenConnectivityDatabase::m_initCount(0);


DBOpenConnectivityDatabase::DBOpenConnectivityDatabase()
		{
		SWGuard	guard(m_initCount);

		if (m_initCount == 0)
			{
			}

		m_initCount++;
		}


DBOpenConnectivityDatabase::~DBOpenConnectivityDatabase()
		{
		// Make sure the database is actually closed
		close();

		SWGuard	guard(m_initCount);

		if (--m_initCount == 0)
			{
			}
		}


sw_status_t
DBOpenConnectivityDatabase::openodbc(const SWString &connection)
		{
		bool		createflag=true;
		sw_status_t	res=SW_STATUS_SUCCESS;
		DWORD		flags=CDatabase::noOdbcDialog;
		
		if (m_db.IsOpen())
			{
			res = SW_STATUS_ALREADY_OPEN;
			}
		else
			{
			try
				{
				if (!m_db.OpenEx(connection, CDatabase::noOdbcDialog))
					{
					res = SW_STATUS_DB_ERROR;
					}
				/*
				else
					{
					m_params = m_db.GetConnect();
					}
				*/
				}
			catch (CDBException *)
				{
				res = SW_STATUS_DB_ERROR;
				}
			}
		
		return res;
		}
		

bool
DBOpenConnectivityDatabase::isSupported()
		{
		return true;
		}


sw_status_t
DBOpenConnectivityDatabase::close()
		{
		sw_status_t	res=SW_STATUS_SUCCESS;
		
		if (m_db.IsOpen())
			{
			try
				{
				m_db.Close();
				}
			catch (CDBException *)
				{
				res = SW_STATUS_DB_ERROR;
				}
			}
		
		return res;
		}


/*
void
DBOpenConnectivityDatabase::getDaoFieldInfo(DBFieldDef *pFD, short &daotype, long &daolen)
		{
		switch (pFD->type)
			{
			case DBF_BOOLEAN:
				daotype = dbBoolean;
				daolen = 1;
				break;
					
			case DBF_INT8:
				daotype = dbByte;
				daolen = 1;
				break;
					
			case DBF_INT16:
				daotype = dbInteger;
				daolen = 2;
				break;
					
			case DBF_MONEY:
			case DBF_INT32:
				daotype = dbLong;
				daolen = 4;
				break;
					
			case DBF_DOUBLE:
				daotype = dbDouble;
				daolen = 8;
				break;
					
			case DBF_DATE:
				daotype = dbDate;
				daolen = 8;
				break;
					
			case DBF_TIMESTAMP:
			case DBF_DATETIME:
				daotype = dbDate;
				daolen = 8;
				break;
					
			case DBF_PASSWORD:
			case DBF_TEXT:
				if (pFD->size > 255)
					{
					daotype = dbMemo;
					daolen = 0;
					}
				else
					{
					daotype = dbText;
					daolen = pFD->size;
					}
				break;
					
			case DBF_BLOB:
			case DBF_SMALL_BLOB:
			case DBF_MEDIUM_BLOB:
			case DBF_LARGE_BLOB:
					daotype = dbLongBinary;
					daolen = 0;
				break;

			default:
				// ERROR - unsupported type
				throw 1;
			}
		}
*/


sw_status_t
DBOpenConnectivityDatabase::checkTable(const SWString &table, const SWArray<DBTableField> &fieldDefs, const SWArray<DBTableIndex> &indexDefs)
		{
		sw_status_t			res=SW_STATUS_SUCCESS;
		/*
		CDaoTableDef		tdef(&m_db);
		DBFieldDef			*pfd=NULL;
		CDaoFieldInfo		field;
		bool				created=false;

		// Member Table
		try
			{
			tdef.Open(table);
			created = false;
			}
		catch (CDBException *)
			{
			tdef.Close();
			tdef.Create(table);
			created = true;
			}

		// Verify the fields
		if (created || tdef.IsOpen())
			{
			pfd = pFieldDef;
			while (pfd->name != NULL)
				{
				bool	createfield=created;
				
				if (!createfield)
					{
					try
						{
						tdef.GetODBCFieldInfo(pfd->name, field);
						}
					catch (CDBException *)
						{
						createfield = true;
						}
					}
					
				if (createfield)
					{
					getDaoFieldInfo(pfd, field.m_nType, field.m_lSize);

					// The field does not exist - create it
					field.m_strName = pfd->name;
					
					if (pfd->attributes == 0)
						{
						field.m_lAttributes = dbUpdatableField;
						if (pfd->type == DBF_TEXT || pfd->type == DBF_PASSWORD)
							{
							field.m_lAttributes |= dbVariableField;
							}
						else
							{
							field.m_lAttributes |= dbFixedField;
							}
						}
					else
						{
						field.m_lAttributes = pfd->attributes;
						}
						
					field.m_nOrdinalPosition = 0;
					field.m_bRequired = FALSE;
					field.m_bAllowZeroLength = (pfd->type == DBF_TEXT || pfd->type == DBF_PASSWORD);
					field.m_lCollatingOrder = 0;
					field.m_strForeignName.Empty();
					field.m_strSourceField.Empty();
					field.m_strSourceTable.Empty();
					field.m_strValidationRule.Empty();
					field.m_strValidationText.Empty();
					field.m_strDefaultValue.Empty();
					
					tdef.CreateField(field);
					}

				pfd++;
				}

			if (created)
				{
				if (pIndexDef != NULL)
					{
					CDaoIndexInfo		idx;
					CDaoIndexFieldInfo	idxfields[10];
					DBIndexDef			*pidx=pIndexDef;
					BOOL				primary=TRUE;

					while (fd.name != NULL)
						{
						SWTokenizer		tok(fd.fields, ",");
						SWStringArray	sa;
						int				i=0;
						
						while (tok.hasMoreTokens())
							{
							sa.add(tok.nextToken());
							}
						
						for (i=0;i<(int)sa.size();i++)
							{
							// The index fields
							idxfields[i].m_strName = sa[i].t_str();
							idxfields[i].m_bDescending = FALSE;
							}
						
						// The index
						idx.m_strName = fd.name;
						idx.m_pFieldInfos = idxfields;
						idx.m_nFields = (short)sa.size();
						if (fd.unique)
							{
							idx.m_bPrimary = primary;
							idx.m_bUnique = TRUE;
							primary = FALSE;
							}
						else
							{
							idx.m_bPrimary = FALSE;
							idx.m_bUnique = FALSE;
							}
						idx.m_bIgnoreNulls = FALSE;
						idx.m_bRequired = TRUE;
						
						tdef.CreateIndex(idx);
						pidx++;
						}
					}

				// Add the table to the database
				tdef.Append();
				res = SW_STATUS_SUCCESS;
				}
			else
				{
				res = SW_STATUS_SUCCESS;
				tdef.Close();
				}
			}
		else
			{
			res = SW_STATUS_DB_ERROR;
			}
		*/
		res = SW_STATUS_DB_ERROR;
		
		return res;
		}

//--------------------------------------------------------------------------------
// SWList Iterator
//--------------------------------------------------------------------------------


class ODBCIterator : public SWCollectionIterator
		{
		// friend class SWList;

public:
		ODBCIterator(DBRecordSet *pRecordSet, CDatabase *pDB, const SWString &query);
		~ODBCIterator();

		virtual bool		hasCurrent();
		virtual bool		hasNext();
		virtual bool		hasPrevious();

		virtual void *		current();
		virtual void *		next();
		virtual void *		previous();

		virtual bool		remove(void *pOldData);
		virtual bool		add(void *data, SWAbstractIterator::AddLocation where);
		virtual bool		set(void *data, void *pOldData);

private:
		void				filldata();

private:
		CRecordset		m_rset;
		DBRecordSet		*m_pRecordSet;
		DBRecord		m_data;
		sw_uint32_t		m_flags;
		};


#define FLAG_INIT		0x1
#define FLAG_NEXT		0x2
#define FLAG_PREV		0x4
#define FLAG_CURR		0x8


ODBCIterator::ODBCIterator(DBRecordSet *pRecordSet, CDatabase *pDB, const SWString &query) :
	SWCollectionIterator(NULL),
	m_pRecordSet(pRecordSet),
	m_rset(pDB),
	m_flags(0)
		{
		try
			{
			m_rset.Open(CRecordset::dynaset, query);

			CODBCFieldInfo	fi;
			int				f, fc = m_rset.GetODBCFieldCount();

			m_pRecordSet->clearFieldInfo();
			for (f=0;f<fc;f++)
				{
				DBFieldInfo	dbfi;

				m_rset.GetODBCFieldInfo((short)f, fi);

				dbfi.name(fi.m_strName);
				switch (fi.m_nSQLType)
					{
					case SQL_CHAR:
					case SQL_VARCHAR:
					case SQL_LONGVARCHAR:
					case SQL_WCHAR:
					case SQL_WVARCHAR:
					case SQL_WLONGVARCHAR:
						dbfi.type(DBF_TEXT);
						break;

					case SQL_SMALLINT: //  SMALLINT Exact numeric value with precision 5 and scale 0 (signed: �32,768 <= n <= 32,767, unsigned: 0 <= n <= 65,535)[3]. 
						dbfi.type(DBF_INT16);
						break;

					case SQL_INTEGER: //  INTEGER Exact numeric value with precision 10 and scale 0 (signed: �2[31] <= n <= 2[31] � 1, unsigned: 0 <= n <= 2[32] � 1)[3]. 
						dbfi.type(DBF_INT32);
						break;

					case SQL_REAL: //  REAL Signed, approximate, numeric value with a binary precision 24 (zero or absolute value 10[�38] to 10[38]). 
					case SQL_FLOAT: //  FLOAT(p) Signed, approximate, numeric value with a binary precision of at least p. (The maximum precision is driver-defined.)[5] 
						dbfi.type(DBF_FLOAT);
						break;

					case SQL_DOUBLE: //  DOUBLE PRECISION Signed, approximate, numeric value with a binary precision 53 (zero or absolute value 10[�308] to 10[308]). 
						dbfi.type(DBF_DOUBLE);
						break;

					case SQL_BIT: //  BIT Single bit binary data.[8] 
						dbfi.type(DBF_BOOLEAN);
						break;

					case SQL_TINYINT: //  TINYINT Exact numeric value with precision 3 and scale 0 (signed: �128 <= n <= 127, unsigned: 0 <= n <= 255)[3]. 
						dbfi.type(DBF_INT8);
						break;

					case SQL_BINARY: //  BINARY(n) Binary data of fixed length n.[9] 
					case SQL_VARBINARY: //  VARBINARY(n) Variable length binary data of maximum length n. The maximum is set by the user.[9] 
					case SQL_LONGVARBINARY: //  LONG VARBINARY Variable length binary data. Maximum length is data source�dependent.[9] 
						dbfi.type(DBF_BLOB);
						break;

					case SQL_TYPE_DATE: // [6] DATE Year, month, and day fields, conforming to the rules of the Gregorian calendar. (See "Constraints of the Gregorian Calendar," later in this appendix.) 
						dbfi.type(DBF_DATE);
						break;

					//case SQL_TYPE_UTCTIME: //  UTCTIME Hour, minute, second, utchour, and utcminute fields. The utchour and utcminute fields have 1/10th microsecond precision.. 

					//case SQL_TYPE_UTCDATETIME: //  UTCDATETIME Year, month, day, hour, minute, second, utchour, and utcminute fields. The utchour and utcminute fields have 1/10th microsecond precision. 
					case SQL_TYPE_TIMESTAMP: // [6] TIMESTAMP(p) Year, month, day, hour, minute, and second fields, with valid values as defined for the DATE and TIME data types. 
						dbfi.type(DBF_DATETIME);
						break;

					// Unsupported types
					case SQL_TYPE_TIME: // [6] TIME(p) Hour, minute, and second fields, with valid values for hours of 00 to 23, valid values for minutes of 00 to 59, and valid values for seconds of 00 to 61. Precision p indicates the seconds precision. 
					//case SQL_TYPE_UTCTIME: //  UTCTIME Hour, minute, second, utchour, and utcminute fields. The utchour and utcminute fields have 1/10th microsecond precision.. 
					case SQL_INTERVAL_MONTH: // [7] INTERVAL MONTH(p) Number of months between two dates; p is the interval leading precision. 
					case SQL_INTERVAL_YEAR: // [7] INTERVAL YEAR(p) Number of years between two dates; p is the interval leading precision. 
					case SQL_INTERVAL_YEAR_TO_MONTH: // [7] INTERVAL YEAR(p) TO MONTH Number of years and months between two dates; p is the interval leading precision. 
					case SQL_INTERVAL_DAY: // [7] INTERVAL DAY(p) Number of days between two dates; p is the interval leading precision. 
					case SQL_INTERVAL_HOUR: // [7] INTERVAL HOUR(p) Number of hours between two date/times; p is the interval leading precision. 
					case SQL_INTERVAL_MINUTE: // [7] INTERVAL MINUTE(p) Number of minutes between two date/times; p is the interval leading precision. 
					case SQL_INTERVAL_SECOND: // [7] INTERVAL SECOND(p,q) Number of seconds between two date/times; p is the interval leading precision and q is the interval seconds precision. 
					case SQL_INTERVAL_DAY_TO_HOUR: // [7] INTERVAL DAY(p) TO HOUR Number of days/hours between two date/times; p is the interval leading precision. 
					case SQL_INTERVAL_DAY_TO_MINUTE: // [7] INTERVAL DAY(p) TO MINUTE Number of days/hours/minutes between two date/times; p is the interval leading precision. 
					case SQL_INTERVAL_DAY_TO_SECOND: // [7] INTERVAL DAY(p) TO SECOND(q) Number of days/hours/minutes/seconds between two date/times; p is the interval leading precision and q is the interval seconds precision. 
					case SQL_INTERVAL_HOUR_TO_MINUTE: // [7] INTERVAL HOUR(p) TO MINUTE Number of hours/minutes between two date/times; p is the interval leading precision. 
					case SQL_INTERVAL_HOUR_TO_SECOND: // [7] INTERVAL HOUR(p) TO SECOND(q) Number of hours/minutes/seconds between two date/times; p is the interval leading precision and q is the interval seconds precision. 
					case SQL_INTERVAL_MINUTE_TO_SECOND: // [7] INTERVAL MINUTE(p) TO SECOND(q) Number of minutes/seconds between two date/times; p is the interval leading precision and q is the interval seconds precision. 
					case SQL_DECIMAL: //  DECIMAL(p,s) Signed, exact, numeric value with a precision of at least p and scale s. (The maximum precision is driver-defined.) (1 <= p <= 15; s <= p).[4] 
					case SQL_NUMERIC: //  NUMERIC(p,s) Signed, exact, numeric value with a precision p and scale s (1 <= p <= 15; s <= p).[4] 
					case SQL_GUID: //  GUID Fixed length Globally Unique Identifier. 
						dbfi.type(0);
						break;

					case SQL_BIGINT: //  BIGINT Exact numeric value with precision 19 (if signed) or 20 (if unsigned) and scale 0 (signed: �2[63] <= n <= 2[63] � 1, unsigned: 0 <= n <= 2[64] � 1)[3],[9]. 
						dbfi.type(DBF_INT64);
						break;
					}

				dbfi.size(fi.m_nScale);
				dbfi.flags(0);

				m_pRecordSet->setFieldInfo(f, dbfi);
				}
			
			// If successful we are pointing at the first data record
			// So we must move to the BOF to be consistant with Iterators
			// Data record sequence is BOF REC REC EOF
			// or BOF EOF for no data
			if (!m_rset.IsBOF())
				{
				m_rset.MovePrev();
				}
			}
		catch (CDBException *)
			{
			if (m_rset.IsOpen())
				{
				m_rset.Close();
				}
			}
		}


ODBCIterator::~ODBCIterator()
		{
		try
			{
			if (m_rset.IsOpen())
				{
				m_rset.Close();
				}
			}
		catch (CDBException *)
			{
			}
		}

void
ODBCIterator::filldata()
		{
		m_data.clear();

		SWRegularExpression	reYMD("^(\\d{4})-(\\d{2})-(\\d{2})");
		CDBVariant			v;
		int					f, fc;
		SWString			fname;
		
		try
			{
			fc = m_rset.GetODBCFieldCount();
			for (f=0;f<fc;f++)
				{
				SWValue	nv;

				m_rset.GetFieldValue((short)f, v);
				
				if (v.m_dwType == DBVT_DATE)
					{
					// Must convert this to a local date
					SWLocalTime	lt;

					lt.set(
						v.m_pdate->year,
						v.m_pdate->month,
						v.m_pdate->day,
						v.m_pdate->hour,
						v.m_pdate->minute,
						v.m_pdate->second,
						v.m_pdate->fraction
						);

					nv = lt;
					}
				else if (v.m_dwType == DBVT_STRING || v.m_dwType == DBVT_ASTRING || v.m_dwType == DBVT_WSTRING)
					{
					nv = v;

					// Some dates actually come in as strings - aaaarrrrgggghhhh!!!!!!
					SWString	s=nv.toString();

					if (s.length() == 10 && reYMD.matches(s))
						{
						SWDate	dt;

						int		y=reYMD.lastMatchString(0).toInt32(10);
						int		m=reYMD.lastMatchString(1).toInt32(10);
						int		d=reYMD.lastMatchString(2).toInt32(10);

						if (dt.set(d, m, y, false))
							{
							nv = dt;
							}
						}
					}
				else
					{
					nv = v;
					}

				m_data.setValue(f, nv);
				}
			}
		catch (CDBException *)
			{
			}
		}


bool
ODBCIterator::hasCurrent()
		{
		return (m_data.size() > 0);
		}


bool
ODBCIterator::hasNext()
		{
		bool	res=false;

		try
			{
			if (m_rset.IsOpen() && !m_rset.IsEOF())
				{
				m_rset.MoveNext();
				res = (m_rset.IsEOF() == FALSE);
				m_rset.MovePrev();
				}
			}
		catch (CDBException *)
			{
			res = false;
			}

		return res;
		}


bool
ODBCIterator::hasPrevious() 
		{
		bool	res=false;

		try
			{
			if (m_rset.IsOpen() && !m_rset.IsBOF())
				{
				m_rset.MovePrev();
				res = (m_rset.IsBOF() == FALSE);
				m_rset.MoveNext();
				}
			}
		catch (CDBException *)
			{
			res = false;
			}

		return res;
		}


void *
ODBCIterator::current() 
		{
		if (!hasCurrent()) throw SW_EXCEPTION(SWNoSuchElementException);

		return &m_data;
		}

void *
ODBCIterator::next() 
		{
		if (!hasNext()) throw SW_EXCEPTION(SWNoSuchElementException);

		m_rset.MoveNext();
		filldata();

		if (!hasCurrent()) throw SW_EXCEPTION(SWNoSuchElementException);

		return &m_data;
		}

void *
ODBCIterator::previous() 
		{
		if (!hasPrevious()) throw SW_EXCEPTION(SWNoSuchElementException);

		// Move to the next record and fill
		m_rset.MovePrev();
		filldata();

		if (!hasCurrent()) throw SW_EXCEPTION(SWNoSuchElementException);

		return &m_data;
		}

bool
ODBCIterator::remove(void * /*pOldData*/)
		{
		bool	res=false;

		if (!res) throw SW_EXCEPTION(SWUnsupportedOperationException);

		return res;
		}


bool
ODBCIterator::set(void * /*data*/, void * /*pOldData*/)
		{
		bool	res=false;

		if (!res) throw SW_EXCEPTION(SWUnsupportedOperationException);

		return res;
		}


bool
ODBCIterator::add(void * /*data*/, SWAbstractIterator::AddLocation /*where*/) 
		{
		bool	res=false;

		if (!res) throw SW_EXCEPTION(SWUnsupportedOperationException);

		return res;
		}


sw_status_t
DBOpenConnectivityDatabase::execute(const SWString &sql, DBRecordSet *pRecordSet)
		{
		sw_status_t	res=SW_STATUS_SUCCESS;

		if (pRecordSet == NULL)
			{
			// No result set - just do the execute
			try
				{
				m_db.ExecuteSQL(sql);
				}
			catch (CDBException *)
				{
				res = SW_STATUS_ILLEGAL_OPERATION;
				}
			}
		else
			{
			loadRecordSet(pRecordSet,  new ODBCIterator(pRecordSet, &m_db, sql));
			}

		return res;
		}


#endif // defined(SW_PLATFORM_WINDOWS)
